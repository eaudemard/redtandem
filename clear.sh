
#!/bin/tcsh
# Insruction for made all files


rm -r ./build

echo "############################   delete ReD   ####################################################"
cd ./src/ReD
rm -r build/

echo "############################   delete dataNucleiclevel   #######################################"
cd ../dataNucleicLevel/
rm -r build/

echo "############################   delete glintAddChrom   ##########################################"
cd ../glintAddChrom/
rm -r build/

echo "############################   delete postTraitement   #########################################"
cd ../postTraitement
rm -r build/

echo "############################   delete regionTandem   ###########################################"
cd ../regionTandem
rm -r build/

echo "############################   delete extendUnit   ###########################################"
cd ../extendUnit
rm -r build/

echo "############################   delete glint   ##################################################"
cd ../glint/src/
rm *.o
rm glint
rm glintindex

echo "############################   delete mdust   ##################################################"
cd ../../mdust
rm *.o
rm mdust





