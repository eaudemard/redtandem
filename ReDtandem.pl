#!/usr/bin/perl

=pod

=head1 NAME

Regiontandem.pl : Identifying tandem duplicated regions in genomic sequences

=head1 SYNOPSIS

 perl ReDtandem.pl

 @args:
  --species the name of the species
  --dnafile the dna file 
  --maxchaindist maximum distance between chains [150000]
  --maxanchordist maximum distance between anchors [40000]
  --centro a file describing the position of centromeres (optional)
  
=head1 DESCRIPTION

Perl Wrapper for the ReD tandem software
 
=head1 Authors

 Eric.Audemard@umontreal.ca
 Thomas.Faraut@toulouse.inra.fr
 Thomas.Schiex@toulouse.inra.fr

=head1 Version

 Created: July 2011
 
=cut

use strict;
use warnings;

use Cwd qw(cwd abs_path);
use Pod::Usage;
use Data::Dumper;
use IO::File;
use File::Temp;
use FindBin qw( $Bin );

use Bio::SeqIO;
use Bio::DB::Fasta;

BEGIN
{	# pathway to our modules
	use File::Basename;
	my $dirprg=dirname($0);

	unshift @INC, "$dirprg/lib";
}
# our modules
use ParamParser;
use General;

use constant BIN_DIR => "$Bin/build";

use constant SPECIES  => 'species';
use constant DNA_FILE => 'dnafile';

use constant NO_CLEAN    => 'noclean';
use constant SINGLE   => 'single';
use constant MAXCHAINDIST => 'maxchaindist';
use constant MAXANCHORDIST => 'maxanchordist';
use constant WORK => 'work';
use constant CENTRO  => 'centro';

use constant DEFAULT_MAXCHAINDIST => 150_000;
use constant DEFAULT_MAXANCHORDIST => 40_000;

use constant REDTANDEM => "ReDTandem.tmp";
use constant REGIONTANDEM => "regions.tmp";


MAIN:
{	
	Main();
}

sub Main
{	
	my $rh_params = &GetParameters();
	
	&InitGlobalParameters( $rh_params );
	
	my $working_dir;
	
	if ( defined $rh_params->{ &WORK } )
	{
		$working_dir  = $rh_params->{ &WORK };
	}
	else
	{			
		$working_dir  = &MakeTempDir( clean => !($rh_params->{&NO_CLEAN}));
	}
		
	my $save_dir = cwd;
	chdir $working_dir;
	print STDERR "---- ReD Tandem wrapper ----\n";

	$rh_params->{ rh_chrominfos } = &GetChromRegionInfo( $rh_params );
		
	print STDERR  "1- Low complexity region detection with mdust...";
	&RunProgram( 'mdust', $rh_params );
	print STDERR  "done.\n";
										
	print STDERR  "2- DNA anchor detection with glint...";	
	&RunProgram( 'glint', $rh_params );								
	print STDERR  "done.\n";	
							
	print STDERR  "3- Reformatting output...";
	&RunProgram( 'AddChrom', $rh_params );		
	print STDERR  "done.\n";		
			
	print STDERR  "4- Filtering low complexity and centromeric anchors...";
	&RunProgram( 'dataNucleicLevel', $rh_params );		
	print STDERR  "done.\n";
	
	print STDERR  "5- First anchors chaining...";
	&RunProgram( 'red', $rh_params );
	print STDERR  "done.\n";
	
	print STDERR  "6- Detecting Tandem arrays...";
	&RunProgram( 'postTraitement', $rh_params );
	print STDERR  "done.\n";
	
	print STDERR  "7- Selecting reference units...";
	&DetectReferenceUnits( $rh_params );
	print STDERR  "done.\n";
	
	print STDERR  "8- Tandem array structure analysis...\n";
	my $ReDtandem_output = &DetectRegionStructure( $rh_params );
	print STDERR  "done.\n";
																
	chdir $save_dir;		
	
	my	$output_file = File::Temp->new( TEMPLATE => "redtandem.outXXXX", DIR => $save_dir,  UNLINK => 0 );
	system("cp $ReDtandem_output $output_file");
	my $results_basename = basename( $output_file );
	print STDERR "Results available in $results_basename \n";
}

=head RunProgram
 Usage        : RunProgram( $program, $rh_params );
 Function     : Runs the $program: general wrapper for running a program, 
                all the program specific arguments are given by _GetProgramArguments
 Returns      : none
 Args         : $program   : the program name
				$rh_params  : the general parameters                  
=cut
sub RunProgram
{
	my ( $program, $rh_params ) = @_;	
	
	my $exe = _GetCmdExePath( $rh_params, $program );
	my $log =  $program.".log";
	
	my $rh_args = _GetProgramArguments( cmd => $program, params => $rh_params );	
			
	my $cmd = join( " ", ($exe, $rh_args->{ parameters }, $rh_args->{ input }));
	
	if (defined $rh_args->{output} )
	{
		$cmd.= "> $rh_args->{output} 2>$log";
	}
	else
	{
		$cmd.= " 1>$log 2>&1";	
	}
	_LaunchShellCommand( $cmd );	
}

#-----------------------------------------------------------------------------------------------------
# Tandem region structure
#-----------------------------------------------------------------------------------------------------

=head DetectRegionStructure
 Usage        : DetectRegionStructure( $rh_params );
 Function     : Runs IdentifyTUinRegion on all regions
 Returns      : none
 Args         :  $rh_params  : the general parameters  
                   
=cut
sub DetectRegionStructure
{
	my ( $rh_params ) = @_;	
	
	my $unit_file   = "munit.out";
	
	my $ra_regions = &GetRedRegions( $unit_file );
	
	my $save_dir = cwd;
	chdir &REGIONTANDEM;
	
	&IdentifyTUinRegions( $ra_regions, $rh_params );
	my $redtandem = &MergeDuplitOutputFiles( $ra_regions, $rh_params  );
	
	my $redtandemExtend = "redtandemExtend.out";
	
	&LaunchExtendUnit( $redtandem, $redtandemExtend,  $rh_params );
		
	my $ReDtandem_output = abs_path( $redtandemExtend );
			
	chdir $save_dir;
		
	return 	$ReDtandem_output;
}

=head IdentifyTUinRegions
 Usage        : IdentifyTUinRegions( $a_regions, $rh_params );
 Function     : Runs IdentifyTUinRegion on all regions
 Returns      : none
 Args         :   $ra_region   : a reference on an array of regions
                  $rh_params  : the general parameters   
=cut
sub IdentifyTUinRegions
{
	my ( $ra_regions, $rh_params ) = @_;	
	
	my $dna_db = Bio::DB::Fasta->new( $rh_params->{ dna_file } );
	
	$rh_params->{ dna_db } = $dna_db;
	
	my $nb_regions = scalar @$ra_regions;
	
	my $num_region = 0;
	foreach my $r_region ( @$ra_regions )
	{
		$num_region++;
		my $b_IsMinimized = &MinimizeTUinRegion( $r_region, $rh_params );
		
		#When the unit has been minimized a single unit will be used
		if ( $b_IsMinimized || $rh_params->{ &SINGLE } )
		{	
			&SearchSingleTUinRegion( $r_region, $rh_params );		
		}	
		else
		{		
			&SearchTUinRegion( $r_region, $rh_params );	
		}	
		
		print STDERR "$num_region/$nb_regions regions \r";
	}	
	print STDERR "\n";
}

=head SearchTUinRegion
 Usage        : &SearchTUinRegion( $r_region,  $rh_params )
 Function     : Search, for each reference unit, the matching units in the region
 Returns      : none
 Args         :   $r_region   : a reference on a region
                  $rh_params  : the general parameters 
=cut
sub SearchTUinRegion
{
	my ( $r_region, $rh_params ) = @_;
	
	my $tmpregion_fa = "region.tmp." . $r_region->{ r_num };
    &ExtractRegion( $r_region, $tmpregion_fa, $rh_params );	
	my $database = $tmpregion_fa;
	&FormatDB( $database, $rh_params );
		
	my @a_red_dupli_ouputfiles = ();		
	my @a_red_longDupli_ouputfiles = ();	
		
	my @a_units = @{$r_region->{ ra_units }};
	foreach my $r_unit ( @a_units )
	{
		my $tmpunit_fa = join(".","unit.tmp",$r_region->{ r_num },$r_unit->{ u_num });	
		&ExtractUnit( $r_unit, $tmpunit_fa, $rh_params );	
			
		my $query    = $tmpunit_fa;
		my $tblastx_output = join(".","tblastx.tandem.out",$r_region->{ r_num },$r_unit->{ u_num });		
		&Launchtblastx( $query, $database, $tblastx_output, $rh_params  );	
		my $glintx_output = join(".","glintx.tandem.out", $r_region->{ r_num },$r_unit->{ u_num });
		&LaunchGlintx( $query, $database, $glintx_output, $rh_params );
		
		my $output = join(".","tblastx.tandem.forRed", $r_region->{ r_num },$r_unit->{ u_num });
		
		&ConcatenateOutput( $output, $glintx_output, $tblastx_output );
		
		my $red_input = $output;
		my $red_output = join(".","tandem",$r_region->{ r_num },$r_unit->{ u_num });
		
		my $red_dupli_outputfile = join(".",$red_output,"dupli","out");
		my $red_longDupli_outputfile = join(".",$red_output,"longDupli","out");
		
		push @a_red_dupli_ouputfiles, $red_dupli_outputfile;
		push @a_red_longDupli_ouputfiles, $red_longDupli_outputfile;
		
		&LaunchRed( $red_input, $red_output, $rh_params );
		
		unless ( $rh_params->{ &NO_CLEAN } )
		{
			unlink( $tmpunit_fa );
			unlink( $tblastx_output );
			unlink( $glintx_output );	
			unlink( $output );
		}	
	}
		
	my $final_longDupli_redoutput = join(".","tandem",$r_region->{ r_num },"longDupli","out");
	my $final_dupli_redoutput = join(".","tandem",$r_region->{ r_num },"dupli","out");
	
	#Now merging the output for all the intermediate redtandem output
	&MergeRedOutput( $r_region, \@a_red_longDupli_ouputfiles, $final_longDupli_redoutput );
	&MergeRedOutput( $r_region, \@a_red_dupli_ouputfiles, $final_dupli_redoutput );
	
	unless ( $rh_params->{ &NO_CLEAN } )
	{
		unlink( $tmpregion_fa );
		unlink( "formatdb.log" );
		system(	"rm -f $tmpregion_fa.*");
		foreach my $r_unit ( @a_units )
		{	
			my $file = join(".","tandem",$r_region->{ r_num },$r_unit->{ u_num });
			system(	"rm -f $file.*");
		}
		
	}		
}

=head MergeRedOutput
 Usage        : &MergeRedOutput( $r_region, $ra_ouputfiles, $red_output )
 Function     : For each file, merge the different units (keeking only a set of non-overlapping units)
 Returns      : none
 Args         :   $r_region       : a reference on a region
                  $ra_ouputfiles  : a reference on a list of files with the redtandem output 
=cut
sub MergeRedOutput
{
	my ( $r_region, $ra_ouputfiles, $red_output ) = @_;	
	
	my @a_tandemunits = ();
	
	foreach my $file ( @$ra_ouputfiles )
	{
		next unless (-e $file );
		my $ra_redoutput = &ReadTabFile( file => $file );
		push @a_tandemunits, $ra_redoutput->[0]{ dupli };
	}
	
	return unless ( scalar @a_tandemunits > 0);
	
	my @a_tunits = ();
	foreach my $str_duplis ( @a_tandemunits )
	{
		my @a_temp = ();
		foreach my $int ( split(",",$str_duplis ) )
		{
			my ($start, $end) = split(/\.\./,$int);
			push @a_temp, { start => $start, end => $end };
		}
		push @a_tunits, \@a_temp;
	}
	
	my $ref_units = shift @a_tunits;
	my @a_tandemduplis =(@{$ref_units});
	
	foreach my $alt_ref_unit ( @a_tunits )
	{
		foreach my $r_tu ( @{$alt_ref_unit} )
		{
			if ( !OverlapIntervals( \@a_tandemduplis, $r_tu ) )
			{			
				push @a_tandemduplis, $r_tu;
			}
		}	
	}
	
	my @a_sorted_tu = sort { $a->{start} <=> $b->{start} } @a_tandemduplis;
	
	my $chrom   =  $r_region->{ r_chrom };
	my $r_start =  $a_sorted_tu[0]->{start};
	my $r_end   =  $a_sorted_tu[$#a_sorted_tu]->{end};
	

	my $str_dupli = _SprintDupli( \@a_sorted_tu );
	
	$r_region->{ dupli } = $str_dupli;
	$r_region->{ numDupli } = scalar @a_sorted_tu;
	
	my $fh = new IO::File( $red_output, "w" );
	
	print $fh "#".join("\t",qw( chrom start end u_start u_end numDupli dupli ) )."\n";
	print $fh join("\t",$chrom, $r_start, $r_end, @{$r_region}{qw( u_start u_end numDupli dupli )})."\n";
	
	$fh->close();	
		
}

=head OverlapIntervals
 Usage        : if ( OverlapIntervals( $ra_int , $r_tu ) ) { do something }
 Function     : Test the overlap bewteen $r_tu and all the intervals in @$ra_dupli
 Returns      : a boolean
 Args         :   $ra_int     : a reference on a list of intervals
                  $r_tu      : an interval
=cut
sub OverlapIntervals
{
	my ( $ra_refint, $r_int ) = @_;	
		
	foreach my $r_refint ( @$ra_refint )
	{
		return &TRUE if ( IntOverlap( $r_refint, $r_int ) );	
	}
	
	return 	&FALSE;	
}

=head IntOverlap
 Usage        : if ( IntOverlap( $r_inta, $r_intb ) ) { do something }
 Function     : Test the overlap bewteen $r_inta and $r_intb
 Returns      : a boolean
 Args         :   $r_inta    : an interval
                  $r_intb    : an interval
=cut
sub IntOverlap
{	
	my ( $r_inta, $r_intb ) = @_;
		
	return &StrictIntervalOverlap( $r_inta->{ start } , $r_inta->{ end }, $r_intb->{ start }, $r_intb->{ end }  );
	
}

=head IntOverlap
 Usage        : if ( IntervalOverlap( $a_start, $a_end, $b_start, $b_end ) ) { do something }
 Function     : Do they overlap ?
 Returns      : a boolean
=cut
sub StrictIntervalOverlap
{
	my ( $starta, $enda, $startb, $endb ) = @_;
	
	return 	( $startb < $enda && $endb > $starta );		
}

=head _SprintDupli
 Usage        : my $str = _SprintDupli( $ra_duplis )
 Function     : An sprintf like function for an array of intervals
 Returns      : a string with intervals concatenated, start1..end1,start2..end2,start3..end3,start1..end4,
 Args         : a reference on an array of intervals
=cut
sub _SprintDupli
{
	my ( $ra_duplis ) = @_;	
	
	my $str = "";
	foreach my $tu ( @$ra_duplis )
	{
		$str.= join("..",@{$tu}{qw( start end )}).",";	
	}
	
	return $str;
}

=head SearchSingleTUinRegion
 Usage        : &SearchSingleTUinRegion( $r_region,  $rh_params )
 Function     : Equivalent to SearchTUinRegion but using a uniq reference unit
 Returns      : none
 Args         :   $r_region   : a reference on a region
                  $rh_params  : the general parameters 
=cut
sub SearchSingleTUinRegion
{
	my ( $r_region, $rh_params ) = @_;
	
	my $tmpunit_fa = "unit.tmp" . $r_region->{ r_num };
		
	&ExtractUnit( $r_region, $tmpunit_fa, $rh_params );	
	my $tmpregion_fa = "region.tmp" . $r_region->{ r_num };
    &ExtractRegion( $r_region, $tmpregion_fa, $rh_params );	
		
	my $query    = $tmpunit_fa;
	my $database = $tmpregion_fa;	
	
	&FormatDB( $database, $rh_params );
	
	my $tblastx_output = "tblastx.tandem.out" . $r_region->{ r_num };		
	&Launchtblastx( $query, $database, $tblastx_output, $rh_params  );	
	my $glintx_output = "glintx.tandem.out". $r_region->{ r_num };
	&LaunchGlintx( $query, $database, $glintx_output, $rh_params );
	
	my $output = "tblastx.tandem.forRed". $r_region->{ r_num };
	&ConcatenateOutput( $output, $glintx_output, $tblastx_output );
	
	my $red_input = $output;
	my $red_output = "tandem.". $r_region->{ r_num };
	
	&LaunchRed( $red_input, $red_output, $rh_params );
	
	unless ( $rh_params->{ &NO_CLEAN } )
	{
		unlink( $tmpunit_fa );
		unlink( $tmpregion_fa );
		unlink( $tblastx_output );
		unlink( $glintx_output );
		unlink( $output );	
		unlink( "formatdb.log" );
		system(	"rm -f $tmpregion_fa.*");
	}
	
}

=head ConcatenateOutput
 Usage        : &ConcatenateOutput( $output_file,  @a_files )
 Function     : Concatenate all the tblastx outputfiles in @a_files into a single file : $output_file
 Returns      : none
 Args         :   $output_file  : the name of the output file
                  @a_files      : an array of files 
=cut
sub ConcatenateOutput
{
	my ( $output, @a_files ) = @_;
	
	my @a_redfields = ("Query id","Subject id","pIdentity","length","mismatches","gaps","q_start","q_end","s_start","s_end","e-value","score" );	
	
	my $fh = new IO::File ( ">$output"  )
		or die "Impossible to open file $output for writing";
	print $fh join("\t",@a_redfields)."\n";
	$fh->close();
	
	foreach my $file ( @a_files )
	{
		system("cat $file >> $output");	
	}
	
}

=head GetRedRegions
 Usage        : GetRedRegions( $file );
 Function     : Read the unit.out file and parse the units
 Returns      : none
 Args         :   $file   : the unit.out file
                 
=cut
sub GetRedRegions
{	
	my $file = shift;
	
	my $ra_redoutput   = &ReadTabFile( file => $file );
	
	my $num_region = 0;
	foreach my $r_region ( @$ra_redoutput )
	{
		$r_region->{ r_num } = ++$num_region;
		my $units = $r_region->{ units };
		$units =~ s/\,$//;
		my @a_units = split(",", $units );
		my @a_chains = ();
		my $num_unit = 0;
		foreach my $unit ( @a_units )
		{
			my %h_chain = ( u_num => ++$num_unit, r_chrom => $r_region->{ r_chrom }  );
			@h_chain{ qw( u_start u_end ) }= split(/\.\./, $unit );		
			push @a_chains, \%h_chain;
		}
		$r_region->{ ra_units } = \@a_chains; 	
	}
	
	return $ra_redoutput;
}

=head MinimizeTUinRegion
 Usage        : MinimizeTUinRegion( $r_region, $num_region, $rh_params );
 Function     : Atomic process on a specific region
 Returns      : none
 Args         :   $r_region   : a reference on a region
                  $rh_params  : the general parameters   
=cut
sub MinimizeTUinRegion
{
	my ( $r_region, $rh_params ) = @_;
	
	my $tmpunit_fa = "unit.tmp" . $r_region->{ r_num };	
	
	&ExtractUnit( $r_region, $tmpunit_fa, $rh_params );	
	my $bool_minimize = &MinimizeUnit( $r_region, $tmpunit_fa, $rh_params );
	
	unless ( $rh_params->{ &NO_CLEAN } )
	{
		unlink( $tmpunit_fa );
	}
	
	return $bool_minimize;
}

=head MiniMizeUnit
 Usage        : MiniMizeUnit( $tmpunit_fa, $rh_params );
 Function     : Minimisze the unit given by the $tmpunit_fa fasta file
 Returns      : none
 Args         :  $tmpunit_fa : the fasta file of the unit
				 $rh_params  : the general parameters 					
=cut
sub MinimizeUnit
{
	my 	( $r_region, $tmpunit_fa, $rh_params ) = @_;
		
	my $glint_input = $tmpunit_fa;	
	my $glint_output = 	"unite.glint.tmp".$r_region->{ r_num };
	
	my $orig_start = $r_region->{ u_start };
	my $orig_end   = $r_region->{ u_end };
	
	do
	{
		&LaunchGlint( $glint_input, $glint_output, $rh_params );
	}
	while ( &IsSelfOverlapping(  $r_region, $glint_input, $glint_output, $rh_params ) );
	
	unless ( $rh_params->{ &NO_CLEAN } )
	{
		unlink( $glint_output );
	}
	
	my $bool_minimized = ( $r_region->{ u_start } != $orig_start || $r_region->{ u_end } != $orig_end );
		
	return $bool_minimized;		
}

=head IsSelfOverlapping
 Usage        :  $bool_selfoverap = &IsSelfOverlapping(  $r_region, $glint_input, $glint_output, $rh_params );
 Function     : Check if the glint self comparison indicate a substantial overlap, returns true if it does
 Returns      : a boolean value
 SideEffect   : If return value is true the file $glint_input is modified to be the new candidate unit
 Args         :   $r_region    : a reference on a region
				  $glint_input : the unit fasta sequence
				  $glint_output: the result of comparing the unit sequence to itself 
                  $rh_params   : the parameters 
=cut
sub IsSelfOverlapping
{
	my ( $r_region, $glint_input, $glint_output, $rh_params ) = @_;
	
	my $b_selfoverlap = &FALSE;
	
	my $ra_glintout = &ReadGlintOutputFile( file => $glint_output,  mode => "m9" );
			
	my $unit_size = $r_region->{ u_end } - $r_region->{ u_start } + 1;
	my @a_unit_coverage = split( "", 0 x ($unit_size+1) ); #we start at position 1 and don't care about the 0 at position 0
	my @a_array_of_ones = split( "", 1 x ($unit_size+1) ); #we start at position 1
	
	my $minscore_hsp = $ra_glintout->[0];
	foreach my $r_hsp ( @$ra_glintout )
	{
		my ( $q_start, $q_end, $s_start, $s_end ) = @{$r_hsp}{ qw( q_start q_end s_start s_end ) };
		@a_unit_coverage[$q_start...$q_end] =  @a_array_of_ones[$q_start...$q_end];
		@a_unit_coverage[$s_start...$s_end] =  @a_array_of_ones[$s_start...$s_end];
		$minscore_hsp = $r_hsp if ( $r_hsp->{ score } < $minscore_hsp->{ score } );
	}
	my $total = 0;
	($total+=$_) for @a_unit_coverage; 
	
	my $coverage = $total/$unit_size; 
	if ( $coverage > $rh_params->{ max_overlap } )
	{
		&TrimUnit( 	$r_region, $minscore_hsp, $glint_input, $rh_params );
		$b_selfoverlap = &TRUE;
	}	
	return $b_selfoverlap;
}

=head TrimUnit
 Usage        : &TrimUnit( 	$r_region, $minscore_hsp, $glint_input, $rh_params );	
 Function     : For a given tandem duplication unit, removes the self-overlapping part defined by the hsp $minscore_hsp
 Returns      : none
 SideEffect   : The file $glint_input is modified to be the new candidate unit
 Args         :   $r_region    : a reference on a region
				  $minscore_hsp : the hsp of self-overlapping alignment with minimum score
				  $glint_input : the unit fasta sequence				  
                  $rh_params   : the parameters 
=cut
sub TrimUnit
{
	my ( $r_region, $minscore_hsp, $glint_input, $rh_params ) = @_;
	
	my $unit_size  = $r_region->{ u_end } - $r_region->{ u_start } +1;
	my $left_size  = $minscore_hsp->{ q_end } - 1; 
	my $right_size =  $unit_size -  $minscore_hsp->{ s_start };
	
	my $u_new_start =  $r_region->{ u_start };
	my $u_new_end   =  $r_region->{ u_end };
		
	if ( $left_size >= $right_size )   
	#trimming on the right
	{
		$u_new_end = 	$r_region->{ u_start } + $minscore_hsp->{ s_start } - 1;	
	}
	else  
	#trimming on the left
	{
		$u_new_start = $r_region->{ u_start } + $minscore_hsp->{ q_end } + 1 ;
	}
						
	#Correct the Unit information and dump the fasta sequence				
	$r_region->{ u_start } =  $u_new_start;
	$r_region->{ u_end   } =  $u_new_end;
	
	&ExtractUnit( $r_region, $glint_input, $rh_params );									
}

#-----------------------------------------------------------------------------------------------------
# Merging tandem duplicate infos in one file
#-----------------------------------------------------------------------------------------------------
=head MergeDuplitOutputFiles
 Usage        : &MergeDuplitOutputFiles( $ra_region )
 Function     : Merge all the files into one file
 Returns      : The name of the ReDtandem ouput file
 Args         :   $ra_regions      : a reference on the array of regions
                  
=cut
sub MergeDuplitOutputFiles
{
	my ( $ra_regions, $rh_params ) = @_;
		
	my $tandem_Dupli = &MergeOuput( $ra_regions, "dupli",  $rh_params->{ rh_chrominfos });
		
	Uniq( $tandem_Dupli );
	
	my $rh_region_info = $rh_params->{ rh_chrominfos };
			
	unless ( $rh_params->{ &NO_CLEAN } )
	{
		system("rm -f tandem.*.longDupli.out");
		system("rm -f tandem.*.dupli.out");
		system("rm -f tandem.*.chain.out");
		system("rm -f tandem.*.align.out");
		system("rm -f tandem.*.alignUse.out");
	}
	
	return 	$tandem_Dupli;
}

=head MergeOuput
 Usage        : &MergeOuput( $ra_region, type )
 Function     : Merge all the files if a particular $type into one file
 Returns      : none
 Args         :   $ra_regions      : a reference on the array of regions
                  
=cut
sub MergeOuput
{	
	my ( $ra_regions, $type, $rh_region_info ) = @_;
	
	my @a_lines = ();	
		
	foreach my $r_region ( @$ra_regions )
	{
		my $file = 	join(".","tandem",$r_region->{ r_num },$type,"out");	
		if ( -e $file )
		{
			my $ra_data =  &ReadTabFile( file => $file );
			foreach my $r_line ( @$ra_data )
			{
				push @a_lines, $r_line;			
			}
		}		
	}	
	
	my @a_sorted_lines = sort { $a->{ chrom } <=> $b->{ chrom } || $a->{ start } <=> $b->{ start } } @a_lines;
	
	my $output_file = "tandem.$type.out";
	my $out_fh = new IO::File( $output_file, "w" )
		or die "Impossible to open file $output_file for writing";
	
	my @a_fields = qw(  chrom start end u_start u_end numDupli dupli );
	
	print $out_fh "#".join("\t",@a_fields)."\n";	
	foreach my $r_line ( @a_sorted_lines )
	{
		my $chrom = $r_line->{ chrom };
		&AddRegionOffset( $r_line, 	$rh_region_info->{ $chrom } );
		print $out_fh join("\t",@{$r_line}{ @a_fields })."\n";		
	}
	
	return 	$output_file;		
}

=head AddRegionOffset
 Usage        : &AddRegionOffset( $r_ta, $rh_chrregion )
 Function     : Coorect the start and end coordinate of the TA and TUs according to the chrom information
 Returns      : none
 Args         :   $r_region      : a reference on a TA
                  $rh_chrregion  : a reference on the hash with chrom infos as provided by GetChromRegionInfo
                  
=cut
sub AddRegionOffset
{
	my ( $tr, $rh_chrregion ) = @_;	
	
	$tr->{ chrom } = $rh_chrregion->{ chrom };
	$tr->{ start } += $rh_chrregion->{ start } - 1 ;
	$tr->{ end } += $rh_chrregion->{ start } - 1 ;	
	$tr->{ u_start } += $rh_chrregion->{ start } - 1 ;
	$tr->{ u_end } += $rh_chrregion->{ start } - 1 ;
	
	
	my @a_units = split(",", $tr->{ dupli } );
	my $new_dupli = "";
	foreach my $unit ( @a_units )
	{
		my %h_unit = ();
		@h_unit{ qw( start end ) }= split(/\.\./, $unit );
		$h_unit{ start } += $rh_chrregion->{ start } - 1 ;
		$h_unit{ end } += $rh_chrregion->{ start } - 1 ;	
		$new_dupli .= $h_unit{ start }."..".$h_unit{ end }.",";
	}
	$tr->{ dupli } = $new_dupli;
}

#-----------------------------------------------------------------------------------------------------
# Merging overlapping TAs
#-----------------------------------------------------------------------------------------------------

use constant MIN_OVERLAP => 0.4;

=head Uniq
 Usage        : &Uniq( $red_file )
 Function     : Merge overlapping TAs
 Returns      : 
 Args         :  
                  
=cut
sub Uniq
{
	my $red_file = shift;
	
	my $temp_red = $red_file.".raw";
	
	system("cp $red_file $temp_red");
		
	my $ra_redta = &ReadTAs( $red_file );
	my ( $rh_coco, $ra_tas ) = &GetConnectComp( $ra_redta );
	
	&PrintMergedCoco( $red_file,  $rh_coco, $ra_tas );
		
}

sub PrintMergedCoco
{
	my (  $file, $rh_cocos, $ra_tas ) = @_;
	
	my $fh =new IO::File( $file, "w" )
		or die "Impossible to open $file for writing";
	
	print $fh "#".join("\t",qw( chrom start end u_start u_end numDupli	dupli))."\n";
	foreach my $coco ( sort { $a <=> $b } keys %$rh_cocos )
	{
		my $r_ta = $ra_tas->[ $coco ];
		print $fh join("\t",@{$r_ta}{qw( chrom start end u_start u_end numDupli	dupli)})."\n";
	}	
	
	$fh->close();
}

sub GetConnectComp
{
	my ( $ra_redta ) = @_;	
	
	my $rh_graph = &OverlapGraph( $ra_redta );
	
	my $rh_cocos = &ConnectedComponents( $rh_graph );
	
	&MergeOverlapping( $rh_cocos, $rh_graph );
		
	return ( $rh_cocos, $rh_graph->{ vertices } );
}

sub OverlapGraph
{
	my ( $ra_ta ) = @_;	
			
	my $ra_vertices = &CreateAndSortVertices( $ra_ta );
	my $num_vertices = 	scalar @$ra_vertices;	
		
	my %h_edges = ();
	FIRST : for ( my $i=0 ; $i< ($num_vertices-1) ; $i++)
	{
		SECOND : for ( my $j=$i+1; $j<$num_vertices ; $j++)
		{
			last SECOND if ( &TooFar( $ra_vertices->[$i], $ra_vertices->[$j] ) );
				
			if ( &Overlap( $ra_vertices->[$i], $ra_vertices->[$j] ) )
			{	
				push @{$h_edges{$ra_vertices->[$i]{ num }}}, $ra_vertices->[$j]{ num };		
			}
		} 
	}
	
	my %h_graph = ( edges => \%h_edges, vertices => $ra_vertices );
		
	return \%h_graph;			
}

sub CreateAndSortVertices
{
	my 	$ra_ta = shift;
	
	my @a_sorted_vertices =	sort SortTAs @$ra_ta;
	
	my $num = 0;
	foreach my $r_vertex ( @a_sorted_vertices )
	{ 
	   	$r_vertex->{ num } = $num++;
	}
	
	return \@a_sorted_vertices;
}

sub SortTAs
{
	return &CmpTAs( $a,  $b );
}

sub CmpTAs
{
	my ( $ra, $rb ) = @_;
	
	return ( $ra->{ chrom } <=> $rb->{ chrom } || 
	         $ra->{ start } <=> $rb->{ start } ||
	         $ra->{ end }   <=> $rb->{ end } );	
}

sub TooFar
{
	my ( $r_a, $r_b ) = @_;	
	
	my $bool_toofar = 0;
	
	$bool_toofar |= ( $r_a->{ chrom } != $r_b->{ chrom } );
	$bool_toofar |= ( $r_b->{ start } > $r_a->{ end } );
	
	return $bool_toofar;
}

sub Overlap
{
	my ( $r_taa, $r_tab ) = @_;
	
	my @a_aunits = @{$r_taa->{ ra_units}};
	my @a_bunits = @{$r_tab->{ ra_units}};
	
	my $num_a = scalar @a_aunits;
	my $num_b = scalar @a_bunits;
	
	my $bool_overlap = 0;
	for ( my $i=0 ; $i< $num_a ; $i++ )
	{
		for ( my $j=0 ; $j< $num_b ; $j++ )
		{
			$bool_overlap |= &IntervalOverlap( @{$a_aunits[$i]}{qw( start end )},@{$a_bunits[$j]}{qw( start end )} );
		}
	}
		
	return $bool_overlap;	
}

sub IntervalOverlap
{
	my ( $starta, $enda, $startb, $endb ) = @_;
	
	my $size_a = $enda - $starta + 1;
	my $size_b = $endb - $startb + 1;
	
	my $bool_overlap = 0;
	
	if ( $startb < $enda && $endb > $starta )
	{
 		
		my $overlap_size;
		if ( $startb > $starta )
		{
			$overlap_size = $enda - $startb;
		}
		else
		{
			$overlap_size = $endb - $starta;	
		}
		
		$bool_overlap = ( $overlap_size > $size_a*&MIN_OVERLAP || $overlap_size > $size_b*&MIN_OVERLAP );		
	}

	return $bool_overlap;	
}	

sub MergeOverlapping
{
	my ( $rh_cocos, $rh_graph ) = @_;
		
	foreach my $coco ( sort { $a <=> $b } keys %$rh_cocos )
	{
		if ( scalar @{$rh_cocos->{ $coco }} > 1 )
		{
			&MergeCoco( $coco, $rh_cocos, $rh_graph );	
		}	
	}			
}	

sub MergeCoco
{
	my ( $coco, $rh_cocos, $rh_graph ) = @_;
	
	my $rh_coco = $rh_cocos->{ $coco };
	
	my @a_tunits = ();
	foreach my $tus ( @$rh_coco )
	{
		push @a_tunits, $rh_graph->{ vertices }[ $tus ]{ ra_units}; 
	}	
	
	my $ref_units = shift @a_tunits;
	my @a_tandemduplis =(@{$ref_units});
	
	foreach my $alt_ref_unit ( @a_tunits )
	{
		foreach my $r_tu ( @{$alt_ref_unit} )
		{
			if ( !OverlapIntervals( \@a_tandemduplis, $r_tu ) )
			{			
				push @a_tandemduplis, $r_tu;
			}
		}	
	}
	
	my @a_sorted_tu = sort { $a->{start} <=> $b->{start} } @a_tandemduplis;
	
	my $ref_ta = $rh_graph->{ vertices }[ $coco ];
		
	my $start    =  $a_sorted_tu[0]->{start};
	my $end      =  $a_sorted_tu[$#a_sorted_tu]->{end};
	my $numdupli =  scalar @a_sorted_tu;
	my $dupli    = _SprintDupli( \@a_sorted_tu );	
	
	@{$ref_ta}{ qw( start end numDupli dupli) } = ( $start, $end, $numdupli, $dupli );		
}

=head ConnectedComponents
 Usage        : my $rh_coc = &ConnectedComponents( $rh_graph )
 Function     : Creates connected components from the overlap graph
 Returns      : $rh_cocos, the connected components in a hashstructure
 Args         : $r_graph a reference on the graph
                
=cut
sub ConnectedComponents
{
	my  ( $r_graph ) = @_;
			
	my $rh_edges = $r_graph->{ edges };
	my $ra_vertices =  $r_graph->{ vertices };
	
	my @a_edges = ();
	
	foreach my $first ( keys %$rh_edges )
	{
		foreach my $second ( @{$rh_edges->{$first}} )
		{
			push @a_edges, { a => $first, b => $second };	
		}
	}
	
	my %h_forest = ();
	
	foreach my $vertex ( @$ra_vertices )
	{
		&NewComponent( \%h_forest, $vertex->{num} );
	}	
	
	foreach my $r_edge ( @a_edges )
	{
		if ( &FindComponent( \%h_forest, $r_edge->{ a } ) != &FindComponent( \%h_forest, $r_edge->{ b } )  )	
		{
			&Union( \%h_forest, $r_edge->{ a }, $r_edge->{ b } );
		}			
	}
		
	my %h_coco ;
	foreach my $elt (keys %{$h_forest{ index }} )
	{
		push @{$h_coco{ $h_forest{ index }{ $elt } } }, $elt;
	}
					
	return \%h_coco;	
}
		
sub NewComponent
{
	my ( $rh_components, $elt ) = @_;	
	
	$rh_components->{ index } { $elt } = $elt;	
	$rh_components->{ set } { $elt }{ $elt }++;		
}

sub FindComponent
{
	my ( $rh_components, $elt ) = @_;		
	
	return 	$rh_components->{ index }{ $elt };	
}

sub Union
{
	my ( $rh_components, $e_a, $e_b ) = @_;		
		
	my $a_coco = &FindComponent( $rh_components, $e_a );
	my $b_coco = &FindComponent( $rh_components, $e_b );
		
	foreach my $elt ( keys %{$rh_components->{ set }{ $b_coco }} )
	{
		$rh_components->{ set }{ $a_coco }{ $elt }++;
		$rh_components->{ index }{ $elt } = $a_coco;
	}
	delete 	$rh_components->{ set }{ $b_coco };	
}	

=head ReadTAs
 Usage        : my $ra_data = ReadTAs()
 Function     : Reading the tandem arrays from the red output file
 Returns      : $ra_data, a reference on an array with the tandem arrays
 Args         : 
                $file : the name of the red output file
=cut
sub ReadTAs
{
	my ( $file ) = @_;
	
	my $ra_data = &ReadTabFile( file => $file );		
	
	foreach my $r_ta ( @$ra_data )
	{
		my @a_units = split(",", $r_ta->{ dupli } );
		my @a_tus = ();
		foreach my $unit ( @a_units )
		{
			my %h_unit = ();
			@h_unit{ qw( start end ) }= split(/\.\./, $unit );				
			push @a_tus, \%h_unit;	
		}
		$r_ta->{ ra_units } = \@a_tus; 				
	}
	
	return $ra_data;	
}

#-----------------------------------------------------------------------------------------------------
# Selecting reference units
#-----------------------------------------------------------------------------------------------------

use constant MAX_OVERLAP => 0.05;

use constant LEFT => 0;
use constant RIGTH => 1;
use constant OUTSIDE => -1;

use constant NON_OVERLAPPING => 0;
use constant LEFT_LEFT => 1;
use constant LEFT_RIGTH => 2;
use constant RIGTH_LEFT => 3;
use constant RIGTH_RIGTH => 4;

=head DetectReferenceUnits
 Usage        : DetectReferenceUnits( $rh_params );
 Function     : From each connected component, select a set of reference units
 Returns      : none
 Args         : $rh_params the general parameters
                   
=cut
sub DetectReferenceUnits
{
	my $rh_params = shift;
	
	my $red_unitfile      = "unit.out";	
	my $red_multiunitfile = "munit.out";
	my $ra_redunits = &GetRedUnits( $red_unitfile );
	
	&SelectUnits( $ra_redunits );
	&PrintUnits( $red_multiunitfile, $ra_redunits );
}

sub PrintUnits
{
	my 	( $output_file, $ra_redoutput )= @_;
	
	my $fh_out = new IO::File( $output_file, "w" )
		or die "Impossible to open file $output_file for writing";
		
	my @fields = qw( r_chrom r_start r_end u_start u_end );
	print $fh_out join("\t",(@fields,"units"))."\n";
	foreach my $r_region ( @$ra_redoutput )
	{	
		print $fh_out join("\t",@{$r_region}{@fields})."\t"; 	
		foreach my $r_int ( @{$r_region->{ra_refint}} )
		{
			print $fh_out join("..",@{$r_int}{ qw(start end) }).","
		}
		print $fh_out "\n";
	}	
	$fh_out->close();		
}

sub SelectUnits
{
	my 	$ra_redoutput = shift;
	
	my $i = 0;
	foreach my $r_units ( @$ra_redoutput )
	{	
		#print STDERR "region $r_units->{ r_chrom }:$r_units->{ r_start }-$r_units->{ r_end }\n";
		&GenomeOverlapGraph( $r_units );	
		&CandidateChains( $r_units );			
	}	
}

sub CandidateChains
{
	my $r_units = shift;	
	
	my @a_chains = @{$r_units->{ ra_chains }};
	
	my $startside = &ChainSide( $a_chains[0], $r_units->{ u_start }, $r_units->{ u_end });
		
	my @a_candidates = ( { start => $r_units->{ u_start }, end =>  $r_units->{ u_end } } );
	
	foreach my $neighbour ( @{$a_chains[0]->{ a_neighbors }[OppositeSide($startside)]}  )
	{
		if ( $neighbour->{ chain }->{ score } > $a_chains[0]->{ score }/2 )
		{			
			push @a_candidates, _GetInt( $neighbour->{ chain }, OppositeSide( $neighbour->{ side } ) );	
		}
	}		
	
	$r_units->{ ra_refint } = \@a_candidates;
}

sub ChainSide
{
	my 	($r_chaina, $start, $end ) = @_;
	
	if ( &GenomeIntervalOverlap(@{$r_chaina}{qw( q_start q_end )},$start, $end) )
	{
		return 	&LEFT;
	}
	if ( &GenomeIntervalOverlap(@{$r_chaina}{qw( s_start s_end )},$start, $end) )
	{
		return 	&RIGTH;	
	}
	
	return OUTSIDE;
}


sub _GetInt
{
	my ($r_chain, $side ) = @_;
	
	if ( $side == &LEFT	)
	{
		return { start => $r_chain->{q_start}, end => $r_chain->{q_end} };	
	}
	else
	{
		return { start => $r_chain->{s_start}, end => $r_chain->{s_end} };		
	}	
}

sub OppositeSide
{
	my $side = shift;	
	
	return 1-$side;	
}

sub GenomeOverlapGraph
{
	my $r_units = shift;	
				
	my @a_chains = @{$r_units->{ ra_chains }};	
	my $num_chain = scalar @a_chains;
		
	my %h_category;
	for (my $i = 1; $i<= 4; $i++ )
	{
		@{$h_category{ $i }} =( (($i >2) ? 1 : 0), (($i%2)? 0 : 1) );
	}
		
	for ( my $i=0 ; $i< ($num_chain-1) ; $i++)
	{
		for ( my $j=$i+1; $j<$num_chain ; $j++)
		{
			my $overlap_type = &NON_OVERLAPPING;
			if ( ($overlap_type = &ChainOverlap( $a_chains[$i], $a_chains[$j] )) != &NON_OVERLAPPING )
			{
				my ( $first,$second ) = @{$h_category{ $overlap_type }};
				push @{$a_chains[$i]->{ a_neighbors }[$first]}, { chain => $a_chains[$j], side => $second };
				push @{$a_chains[$j]->{ a_neighbors }[$second]},{ chain =>  $a_chains[$i], side => $first };
				push @{$a_chains[$i]->{ a_neighbourhood }}, $a_chains[$j];
			}
		}
	}				
}

sub ChainOverlap
{
	my ( $r_chaina, $r_chainb ) = @_;
	
	if ( &GenomeIntervalOverlap(@{$r_chaina}{qw( q_start q_end )},@{$r_chainb}{qw( q_start q_end )}) )
	{
		return 	&LEFT_LEFT;
	}
	if ( &GenomeIntervalOverlap(@{$r_chaina}{qw( s_start s_end )},@{$r_chainb}{qw( q_start q_end )}) )
	{
		return 	&RIGTH_LEFT;
	}	
	if ( &GenomeIntervalOverlap(@{$r_chaina}{qw( q_start q_end )},@{$r_chainb}{qw( s_start s_end )}) )
	{
		return 	&LEFT_RIGTH;
	}
	if ( &GenomeIntervalOverlap(@{$r_chaina}{qw( s_start s_end )},@{$r_chainb}{qw( s_start s_end )}) )
	{
		return 	&RIGTH_RIGTH;
	}
	
	return NON_OVERLAPPING;
}
	
sub GenomeIntervalOverlap
{
	my ( $starta, $enda, $startb, $endb ) = @_;
	
	my $size_a = $enda - $starta + 1;
	my $size_b = $endb - $startb + 1;
	
	my $bool_overlap = 0;
	
	if ( $startb < $enda && $endb > $starta )
	{
 		
		my $overlap_size;
		if ( $startb > $starta )
		{
			$overlap_size = $enda - $startb;
		}
		else
		{
			$overlap_size = $endb - $starta;	
		}
		
		$bool_overlap = ( $overlap_size > $size_a*&MAX_OVERLAP || $overlap_size > $size_b*&MAX_OVERLAP );				
	}

	return $bool_overlap;
		
}	
=head GetRedUnits
 Usage        : GetRedUnits( $file );
 Function     : Reading red units as provided by the postTraitement routine
 Returns      : none
 Args         : $file : the unit file
                   
=cut
sub GetRedUnits
{	
	my $file = shift;
	
	my $ra_redoutput   = &ReadTabFile( file => $file );
	
	foreach my $r_unit ( @$ra_redoutput )
	{
		my $units = $r_unit->{ units };
		$units =~ s/\,$//;
		my @a_units = split(",", $units );
		my @a_chains = ();
		foreach my $unit ( @a_units )
		{
			my %h_chain = ();
			@h_chain{ qw( q_start q_end s_start s_end score ) } = ( $unit =~ /(\d+)\-(\d+)\:(\d+)\-(\d+)\(([\d\.]+)\)/ );
			push @a_chains, \%h_chain;
		}
		$r_unit->{ ra_chains } = \@a_chains; 	
	}
	
	return $ra_redoutput;
}

#-----------------------------------------------------------------------------------------------------
# Managing external calls
#-----------------------------------------------------------------------------------------------------

=head _GetCmdExePath
 Usage        : &_GetCmdExePath( $rh_params, $cmd_name )
 Function     : Construct the cmd complete path
 Returns      : none
 Args         :  $rh_params
=cut
sub _GetCmdExePath
{
	my ( $rh_params, $cmd ) = @_;	
	
	my $cmd_key = $cmd."_exe";
	
	(defined $rh_params->{ $cmd_key } )
		or die "Unspecified $cmd_key key in global parameters hash $rh_params !";
	
	(-x $rh_params->{ $cmd_key })
		or die "There is a problem with your red installation, $rh_params->{$cmd_key} ($cmd_key executable) is not an existing executable file";
	
	return $rh_params->{ $cmd_key };
}

sub _GetProgramArguments
{
	my %h_args = @_;
	
	my @a_args = ();
	my %h_arguments = ();
	
	my $rh_params = $h_args{ params }; 
	my $cmd = $h_args{ cmd }; 
		
	my $species        = $rh_params->{ &SPECIES };	
	my $maxchaindist   = $rh_params->{ &MAXCHAINDIST };	
	my $maxanchordist  =  $rh_params->{ &MAXANCHORDIST };
			
	if ( $cmd eq "mdust" )
	{
		my $input  =  $rh_params->{ dna_file };
		my $output =  "mdust.out";	
		
		$h_arguments{ parameters }  = "";
		$h_arguments{ input }       = $input;
		$h_arguments{ output }      = $output;

	}
	elsif ( $cmd eq "glint" )
	{
		my $input  = $rh_params->{ dna_file }; #We are working on the dna file and not the mdust file ......
		my $output = "glint.out";
		
		my $parameters = _ProgramParameters( cmd => "glint", '--tandem' => $maxchaindist, '-o' => $output );
		
		$h_arguments{ parameters }  = $parameters;
		$h_arguments{ input }       = $input;
		$h_arguments{ output }      = undef;
		
	}
	elsif ( $cmd eq "AddChrom" )
	{
		my $input      = "glint.out";
		my $output     = "glint_chrom.out";
		my $log        = "AddChrom.log";
			
		my $parameters    = _ProgramParameters( -G => $input, -S => $species, -O => $output  );
		
		$h_arguments{ parameters }  = $parameters;
		$h_arguments{ input }       = "";
		$h_arguments{ output }      = undef;
					
	}
	elsif ( $cmd eq "dataNucleicLevel" )
	{	
		my $mdust_output      = "mdust.out";
		my $glint_output      = "glint_chrom.out";
		my $output            = "glint_chrom.red";
		my $log               = "dataNucleicLevel.log";
	
		my %h_progargs = ( -L => $mdust_output, -G => $glint_output, -S => $species, -D => $maxanchordist, -O => $output );
		
		if (defined $rh_params->{ centro_file } )
		{
			$h_progargs{ -C } = $rh_params->{ centro_file };
		}
		
		my $parameters    = _ProgramParameters( %h_progargs );
				
		$h_arguments{ parameters }  = $parameters;
		$h_arguments{ input }       = "";
		$h_arguments{ output }      = undef;		
						
	}	
	elsif ( $cmd eq "red" )
	{		
		my $input      = "glint_chrom.red";
		my $output     = "red";
			
		my $parameters    = _ProgramParameters( cmd => "red", -I => $input, -S => $species, -t => $maxanchordist, -G => $maxchaindist, -O => $output  );
		
		$h_arguments{ parameters }  = $parameters;
		$h_arguments{ input }       = "";
		$h_arguments{ output }      = undef;				
	}
	elsif ( $cmd eq "red2" )
	{			
		my $parameters    = _ProgramParameters( cmd => $cmd , -S => $species );
	     
	     
		$parameters="-E 7 -A 3 -F 4 -C 0 -H 0 -R 1.5 -M -1 -T 3 -u -v -m -n -s -p -L 1"." -S $species";
					
		$h_arguments{ parameters }  = $parameters;
		$h_arguments{ input }       = "";
		$h_arguments{ output }      = undef;				
	}
	elsif ( $cmd eq "postTraitement" )
	{	
		my $chains   = "red.chain.out";
		my $anchors  = "red.align.out";
		my $output   = "unit.out";
		
		my $parameters    = _ProgramParameters( -R => $chains, -G => $anchors, -D => $maxchaindist, -U => 3, -L => 5, -O => $output  );
	
		$h_arguments{ parameters }  = $parameters;
		$h_arguments{ input }       = "";
		$h_arguments{ output }      = undef;
					
	}
	elsif ( $cmd eq "regionTandem" )
	{	
		my $unit   = "unit.out";
		my $dna    = $rh_params->{ dna_file };
		my $output   = "regionTandem.out";
		
		my $parameters    = _ProgramParameters( -R => $unit, -D => $dna, -W => &REGIONTANDEM, -S => $species, -M => $maxchaindist );
		
		$h_arguments{ parameters }  = $parameters;
		$h_arguments{ input }       = "";
		$h_arguments{ output }      = $output;			
	}
	elsif ( $cmd eq "glintminimize" )
	{	
		my $parameters    = _ProgramParameters( cmd =>   $cmd, '--tandem' => $maxchaindist ); 
		
		$h_arguments{ parameters }  = $parameters;
		$h_arguments{ input }       = "";
		$h_arguments{ output }      = undef;			
	}
	else
	{
		my $parameters   = _ProgramParameters( cmd =>  $cmd, %h_args ); 
		
		$h_arguments{ parameters }  = $parameters;
		$h_arguments{ input }       = "";
		$h_arguments{ output }      = undef;			
	}
		
	return \%h_arguments;	
}

sub _ProgramParameters
{	
	my %h_param = @_;
	
	my $cmd_name = "none";
	
	if ( exists $h_param{ cmd } )
	{
		$cmd_name = $h_param{ cmd };	
		delete $h_param{ cmd };
	}
	
	if ( $cmd_name eq "glint" )  #Glint specific parameters
	{
		%h_param = ( %h_param, 
					-S => "", 
					'--strict' => "", 
					-C => 2, 
					-w => 2, 
					-d => 2, 
					-c => 15, 
					-X => 20, 
					-s => 25, 
					-m =>8 
					);	
	}
	elsif  ( $cmd_name eq "red" ) #Red specific parameters
	# -Y 0 -E 6 -R 1.2 -H 1 -A 4 -m -g -l -t 40000 -G 50000 -u -o -p -n -s -v -F 2 -M 1 -h 0 -T 3 -S ath -L 1 -N 500 
	{
		%h_param = ( %h_param, 
					 -Y => 0,
					 -E => 6, 
					 -R => 1.2,
					 -H => 1,
					 -A => 4, 
					 -m => "",
					 -g => "",
					 -l => "",
					 -u => "",
					 -o => "",
					 -p => "",
					 -n => "",
					 -s => "",
					 -v => "",					 
					 -F => 2,
					 -M => 1,
					 -h => 0,
					 -T => 3,
					 -L => 1,
					 -N => 500,									 
					);
	}
	elsif ( $cmd_name eq "red2" ) #Red2 specific parameters
	# -E 7 -A 3 -F 4 -C 0 -H 0 -R 1.5 -M 1 -T 3 -u -v -m -n -s -p -L 1 -I %s/tblastx.tandem.forRed%d  -O %s/tandem -S %s
	{
		%h_param = ( %h_param,
					  -E => 7,
					  -A => 3,
					  -F => 4,
					  -C => 0,
					  -H => 0,
					  -R => 1.5,
					  -M => 1,
					  -T => 3,
					  -u => "",
					  -v => "",
					  -m => "",
					  -n => "",
					  -s => "",
					  -p => "",	
					  -L => 1,		
					 );	
	}
	elsif ( $cmd_name eq "glintminimize" ) #glint minimize unit specific parameters
	{	
		%h_param = ( %h_param, 
			 '--strict' => "",
			 -C => 2, 
			 -w => 2,
			 -d => 2,
			 -c=> 15,
			 -X => 20,
			 -s => 25,
			 -m => 8,
			 '--silent' => "",
			 -S => ""
			 );	
	}
	elsif ( $cmd_name eq "glintx" ) #glintx specific parameters
	{
		%h_param = ( %h_param,
				'--mismatch=-2' => "",
				'--gapop=-4'    => "",
				'--gapext=-1'   => "",
				'-M'        => '1301102110121',
				'--strict'   => "",
				'-m'         => 8 
				 );
	}
	elsif ( $cmd_name eq "tblastx" ) #tblastx specific parameters
	{
		%h_param = (  
				%h_param,
				-evalue => '1e-5',
				-seg => 'no',
				-outfmt => 6
				);		
	}
	
	return join(" ",%h_param);
}


sub LaunchRed
{
	my ( $red_input, $red_output, $rh_params ) = @_;	
	
	my $log = "red.log";
		
	my $exe = _GetCmdExePath( $rh_params, "red" );
	my $rh_args = _GetProgramArguments( cmd => "red2", params => $rh_params );	
	my $cmd = join(" ",( $exe, $rh_args->{ parameters }, "-I $red_input", "-O $red_output" , "1>$log 2>&1") );
			
	_LaunchShellCommand( $cmd );	

}

sub LaunchGlint
{
	my ( $glint_input, $glint_output, $rh_params ) = @_;
	
	my $log = "glint.log";
		
	my $exe = _GetCmdExePath( $rh_params, "glint" );
	my $rh_args   = _GetProgramArguments( cmd => "glintminimize", params => $rh_params );	
	my $cmd = join(" ",( $exe, $rh_args->{ parameters }, "-o $glint_output", $glint_input , "1>$log 2>&1") );
		
	_LaunchShellCommand( $cmd );	
}

sub LaunchGlintx
{
	my ( $query, $database, $glintx_output, $rh_params ) = @_;
	
	my $log = "glintx.log";
		
	my $exe       = _GetCmdExePath( $rh_params, "glint" );
	my $rh_args   =  _GetProgramArguments( cmd => "glintx" );
	my $cmd = join(" ",( $exe, $rh_args->{ parameters }, "-o $glintx_output", $database, $query, "1>$log 2>&1") );
		
	_LaunchShellCommand( $cmd );	
}

sub FormatDB
{
	my ( $database, $rh_params ) = @_;	
	my $exe  = _GetCmdExePath( $rh_params, "formatdb" );		
	#my $formatdb = $exe . " -p F -i $database";	
	my $formatdb = $exe . " -dbtype nucl -in $database";
	_LaunchShellCommand( $formatdb );
		
}

sub Launchtblastx
{
	my ( $query, $database, $glint_output, $rh_params ) = @_;
	
	my $exe = _GetCmdExePath( $rh_params, "tblastx" );
	my $rh_args   =  _GetProgramArguments( cmd => "tblastx", -out => $glint_output, -query => $query, -db => $database );
	
	my $cmd = join(" ",( $exe, $rh_args->{ parameters }) );
	_LaunchShellCommand( $cmd );	
}

sub LaunchExtendUnit
{
	my ( $input, $output, $rh_params ) = @_;
	
	my $log = "extend.log";
		
	my $exe = _GetCmdExePath( $rh_params, "extendUnit" );
	my $rh_args   =  _GetProgramArguments( cmd => "extendUnit", -I => $input, -O => $output );
	
	my $cmd = join(" ",( $exe, $rh_args->{ parameters }, "1>$log 2>&1") );
	
	_LaunchShellCommand( $cmd );	
	
}

#-----------------------------------------------------------------------------------------------------
# General purpose methods
#-----------------------------------------------------------------------------------------------------

use constant DEFAULT_ENLARGE => 50_000;

=head ExtractRegion
 Usage        : my $output_fa = ExtractRegion( $r_region, $num_region, $rh_params );
 Function     : extract the subseq region from the dna sequence
 Returns      : the name of the resulting fasta file
 Args         :   $r_region   : a reference on a region
                  $rh_params  : the parameters 
=cut
sub ExtractRegion
{
	my ( $r_region, $output_filename, $rh_params ) = @_;	
		
	my ( $species, $chrom, $start, $end ) = ($rh_params->{ &SPECIES}, @{$r_region}{ ('r_chrom', 'r_start', 'r_end')});
		
	$start -= 	&DEFAULT_ENLARGE; 
	$start= 1 if ( $start <= 0 );
	 
	my $chrom_length = $rh_params->{ rh_chrominfos }{ $chrom }{ length };
	$end += &DEFAULT_ENLARGE; 
	$end = $chrom_length if ( $end > $chrom_length );
	
	my %h_region = ( species => $species, chrom => $chrom, start => $start, end => $end, 
					  masked => { start => ($r_region->{ u_start } - $start + 1), end => ($r_region->{ u_end } - $start + 1)}
				   );
	
	&ExtractSubseq( $rh_params->{ dna_db }, \%h_region, $output_filename, $rh_params  ); 	
	
	return $output_filename;
}

=head ExtractUnit
 Usage        : my $output_fa = ExtractUnit( $r_region, $num_region, $rh_params );
 Function     : extract the subseq unit from the dna sequence
 Returns      : the name of the resulting fasta file
 Args         :   $r_region   : a reference on a region
                  $rh_params  : the parameters 
=cut
sub ExtractUnit
{
	my ( $r_unit, $output_filename, $rh_params ) = @_;	
		
	my ( $species, $chrom, $start, $end ) = ($rh_params->{ &SPECIES}, @{$r_unit}{ ('r_chrom', 'u_start', 'u_end')});
		
	my %h_region = ( species => $species, chrom => $chrom, start => $start, end => $end );
		
	&ExtractSubseq( $rh_params->{ dna_db }, \%h_region, $output_filename, $rh_params ); 	
	
	return $output_filename;
}

=head ExtractSubseq
 Usage        : ExtractSubseq( $dna_db, $seq_name, $chrom_id, $start, $end, $output_filename  ); 	
 Function     : Extract from the Bio::DB::Fasta-indexed fasta file the corresponding subseq
 Returns      : none
 Args         :   $dna_db : the Bio::DB::Fasta object
                  $seq_id : the display_id of the resulting subseq
                  $chrom : the $seq_id of the multifasta dna.file
                  $start, $end : start and end of the region
                  $output_file : the name of the outputfile 
=cut
sub ExtractSubseq
{
	my ( $dna_db, $h_region, $output_file, $rh_params ) = @_; 	
	
	my ( $species, $chrom, $start, $end, $offset ) =  @{$h_region}{ qw( species chrom start end offset ) };
	
	my $seq_id = $species . $chrom. "_" . $start . "-" . $end;
	
	my $chrom_id = &RedChrom2ChromId( $chrom, $rh_params );
		
	my $seq = $dna_db->seq( $chrom_id, $start  => $end );
	
	if (defined $h_region->{ masked })
	{	
		my ( $substart, $subend ) =   @{$h_region->{ masked }}{qw( start end )};
		my $subseqlength =  $subend - $substart + 1;
		substr( $seq, $substart -1 ,  $subseqlength, 'N' x $subseqlength );
	}
	
	my $seq_out = Bio::SeqIO->new('-file' => ">$output_file",
                                       '-format' => 'fasta');
	my $seq_obj = Bio::Seq->new(-seq => $seq, -display_id => $seq_id );
	$seq_out->write_seq( $seq_obj );
}

=head ReadGlintOutputFile
 Usage        : my $ra_data = &ReadGlintOutputFile( file => $file, mode => $mode)
 Function     : Read a glint output file
 Returns      : $ra_data, a reference on an array with reference on a hash per line
 Args         : 
                --file : the name of the file
                --mode : the glint output format
=cut
sub ReadGlintOutputFile
{
	my %h_args = (  file => undef,
					mode => "m9",
					@_
					);
	
	my $glint_output = $h_args{ file };
	my $mode = $h_args{ mode };
		
	if ( $mode ne "m9" )
	{
		die "output format $mode not implemented yet in ReadGlintOuputFile"; 
	}	
		
	my @a_glintfields = ("query","subject","pident","length","mismatch","gaps","q_start","q_end","s_start","s_end","e-value","score");	
	my $ra_data = &ReadTabFile( file => $glint_output,  ra_fields => \@a_glintfields );
	
	foreach my $r_line ( @$ra_data )
	{
		$r_line->{ strand } = 1;
		if ( $r_line->{ s_end }	< $r_line->{ s_start }	)
		{
			my $true_s_start = 	$r_line->{ s_end };
			my $true_s_end = 	$r_line->{ s_start };
			$r_line->{ s_end }   = $true_s_end;
			$r_line->{ s_start } = $true_s_start;
			$r_line->{ strand } = -1;
		}
	}
	return $ra_data;
}

=head ReadTabFile
 Usage        : my $ra_data = ReadTabFile()
 Function     : Read a tab-separated file
 Returns      : $ra_data, a reference on an array with reference on a hash per line
 Args         : 
                --file : the name of the file
                --ra_fields :  a reference on an array of fieldnames 
                (if not provided field names are assumed to be specified by the first line)
=cut
sub ReadTabFile
{
	my %h_args = (  file => undef,
					ra_fields => undef,
					@_
					);
	my $file  = $h_args{ file };
	
	my $fh = new IO::File ( $file )
		or die "Impossible to open file $file for reading";
		
	my @a_data = ();
		
	#Small file, we read all the lines at once
	my @lines = <$fh>;
	
	my @field_names = ();
	if (defined $h_args{ ra_fields })
	{
		@field_names = @{$h_args{ ra_fields }};
	}
	else
	{		
		my $header = shift 	@lines;
		chomp($header);
		$header =~ s/^\#//;
		@field_names =  split(/\t/, $header);
	}

	foreach my $line ( @lines )
	{
		next unless ( $line =~ /\S/);
		chomp( $line );
		my @fields = split (/\t/, $line );
		my %h_line = ();
		@h_line{ @field_names } =  @fields;
		push @a_data, \%h_line;
	}	
	
	return \@a_data;					
}

=head GetChromRegionInfo
 Usage        : my $rh_chrominfo = GetRegionInfo()
 Function     : Read dna fasta file and extract the regions information (chrom start end for each chromosome sequence)
 Returns      : $rh_regioninfo, hash ref with keys ...
 Args         : the name of the dna multifasta file
=cut
sub GetChromRegionInfo
{
	my ( $rh_params ) = @_;
	
	my $species = $rh_params->{ &SPECIES };
	my $file = $rh_params->{ dna_file };
	
	my $pipe = "grep '^>' $file|";
	my $fh = new IO::File( $pipe )
		or die "Impossible to open file $pipe for reading";
	
	my %h_regions = ();
	
	while ( my $line =<$fh> )
	{
		if ( $line =~/$species(\w+)\_(\d+)\-(\d+)/ )
		{
			my ( $chrom, $start, $end ) = ($1, $2, $3 );	
			$h_regions{ $chrom } ={ chrom => $chrom, start => $start, end => $end, length => ($end-$start+1) };
		}
	}
	
	return \%h_regions;
}

=head RedChrom2ChromId
 Usage        : my $chrom_id = RedChrom2ChromId()
 Function     : Using the correspondance given by GetChromRegionInfo convert the red "chromosome" to the corresponding dna.fa-like seq_id
 Returns      : a valid chrom id
 Args         : the name the red chromosome, the rh_params

=cut
sub RedChrom2ChromId
{
	my  ( $rednum,	$rh_params ) = @_;
	
	my $species = $rh_params->{ &SPECIES };
	my $rh_chrominfos = $rh_params->{ rh_chrominfos };
	
	my $name = $species.$rh_chrominfos->{ $rednum }{ chrom } . "_" . $rh_chrominfos->{ $rednum }{ start } . "-" . $rh_chrominfos->{ $rednum }{ end };
    		
	return $name;
}


=head _LaunchShellCommand
 Usage        : _LaunchShellCommand( $command ) 
 Function     : Execute the shell command $command
 Returns      : none
 Args         :  $command : the command to be executed
=cut
sub _LaunchShellCommand
{
	my $command = shift;
	#print "Executing $command \n";
	system($command);
}

=head MakeTempDir
 Usage        : &MakeTempDir( clean => 0 )
 Function     : Initialize some global parameters
 Returns      : the newly created temp dir
 Args         : 
=cut
sub MakeTempDir
{
	my %h_args = ( clean => &TRUE,
	               @_ );
	my	$temp_dir = File::Temp->newdir( &REDTANDEM."XXXX", CLEANUP => $h_args{ clean } );	
	mkdir( $temp_dir."/".&REGIONTANDEM );
	
	return $temp_dir;
}

=head InitGlobalParameters
 Usage        : &InitGlobalParameters( $rh_params )
 Function     : Initialize some global parameters
 Returns      : none
 Args         :  $rh_params
=cut
sub InitGlobalParameters
{
	my $rh_params = shift;
			
	my $dna_file = abs_path( $rh_params->{ &DNA_FILE } );
	my $centro_file = (defined $rh_params->{ &CENTRO } ) ? abs_path( $rh_params->{ &CENTRO } ) : undef;
		
	@{$rh_params}{qw( dna_file centro_file )} = ( $dna_file, $centro_file );
		
	#The executable absolute paths
	my $mdust_exe            = &BIN_DIR."/mdust";
	my $glint_exe            = &BIN_DIR."/glint";
	my $AddChrom_exe         = &BIN_DIR."/glintAddChrom_exec";
	my $dataNucleicLevel_exe = &BIN_DIR."/dataNucleicLevel_exec";
	my $red_exe              = &BIN_DIR."/ReD_exec";
	my $postTraitement_exe   = &BIN_DIR."/postTraitement_exec";
	my $regionTandem_exe     = &BIN_DIR."/regionTandem_exec";
	my $extendUnit_exe       = &BIN_DIR."/extendUnit_exec";
	#my $tblastx_exe          = `which blastall`;
	my $tblastx_exe          = `which tblastx`;
	#my $formatdb_exe         = `which formatdb`;
	my $formatdb_exe         = `which makeblastdb`;
	
	chomp($tblastx_exe);
	chomp($formatdb_exe);
	
	@{$rh_params}{qw( mdust_exe glint_exe ) } = ( $mdust_exe, $glint_exe  );
	@{$rh_params}{qw( red_exe AddChrom_exe dataNucleicLevel_exe postTraitement_exe regionTandem_exe extendUnit_exe ) }= ( $red_exe, $AddChrom_exe, $dataNucleicLevel_exe, $postTraitement_exe, $regionTandem_exe, $extendUnit_exe);
	@{$rh_params}{qw( tblastx_exe formatdb_exe )} = ( $tblastx_exe,  $formatdb_exe );
		
	$rh_params->{ max_overlap } = 0.5;
		
}
		
=head GetParameters
 Usage        : my $rh_params = $o_x->GetParameters()
 Function     : Read parameters from command line and return them formatted as a hash ref.
 Returns      : $rh_params, hash ref with keys ...
 Args         : none

=cut
sub GetParameters
{
	ParamParser::SetDefaultUsage( sub { pod2usage(1) } );

	my @a_keys   = qw( help );
	push( @a_keys,  &SPECIES . '=s', &DNA_FILE . '=s', &WORK .'=s', &CENTRO . '=s', &MAXCHAINDIST .'=s', &MAXANCHORDIST .'=s', &NO_CLEAN, &SINGLE );
	my $o_param  = New ParamParser( 'GETOPTLONG', @a_keys );

	$o_param->AssertDefined( &DNA_FILE, &SPECIES );
	$o_param->AssertFileExists( &DNA_FILE );
		
	$o_param->SetUnlessDefined( &MAXCHAINDIST, 150_000 );
	$o_param->SetUnlessDefined( &MAXANCHORDIST, 40_000 );
			
	$o_param->SetUnlessDefined( &NO_CLEAN, &FALSE );
	$o_param->SetUnlessDefined( &SINGLE, &FALSE );
	
	my $rh_params = {};
	$o_param->Dump( 'HASH', $rh_params );

	return $rh_params;
}
