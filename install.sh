#!/bin/tcsh
# Insruction for made all files


echo "############################   install ReD   ####################################################"
cd ./src/ReD
mkdir ./build
cd ./build
cmake -DCMAKE_BUILD_TYPE=Release ../
make

echo "############################   install dataNucleiclevel   #######################################"
cd ../../dataNucleicLevel/
mkdir ./build
cd ./build
cmake -DCMAKE_BUILD_TYPE=Release ../
make

echo "############################   install glintAddChrom   ##########################################"
cd ../../glintAddChrom/
mkdir ./build
cd ./build
cmake -DCMAKE_BUILD_TYPE=Release ../
make

echo "############################   install postTraitement   #########################################"
cd ../../postTraitement
mkdir ./build
cd ./build
cmake -DCMAKE_BUILD_TYPE=Release ../
make

#It's no longer necessary to compile this
#echo "############################   install regionTandem   ###########################################"
#cd ../../regionTandem
#mkdir ./build
#cd ./build
#cmake -DCMAKE_BUILD_TYPE=Release ../
#make

echo "############################   install extendUnit   ###########################################"
cd ../../extendUnit
mkdir ./build
cd ./build
cmake -DCMAKE_BUILD_TYPE=Release ../
make

echo "############################   install glint   ##################################################"
cd ../../glint
make

echo "############################   install mdust   ##################################################"
cd ../mdust
make


cd ../../
mkdir ./build 
cd ./build
cp ../src/ReD/build/src/ReD_exec ./
cp ../src/dataNucleicLevel/build/src/dataNucleicLevel_exec ./
cp ../src/glintAddChrom/build/src/glintAddChrom_exec ./
cp ../src/postTraitement/build/src/postTraitement_exec ./
cp ../src/extendUnit/build/src/extendUnit_exec ./
cp ../src/glint/src/glint ./
cp ../src/mdust/mdust ./







