
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <valarray>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>

#include <boost/lexical_cast.hpp>
#include <boost/numeric/conversion/cast.hpp>
#include <boost/algorithm/string.hpp>

using namespace boost;
using namespace std;

int castStringToInt(string text){

	int i = 0;

	while (text.at(0)==' '){
		text.erase(0,1);	
	}

	try 
	{
		i = lexical_cast<int>(text);
	} 
	catch(boost::bad_lexical_cast &) {
		cout << "dataNucleicLevel -> main.cpp -> castStringToInt()" << endl;
		cout << text << endl;
		exit(EXIT_FAILURE);		
	}
	
	return i;
			
}

double castStringToDouble(string text){

	double d = 0;
	
	while (text.at(0)==' '){
		text.erase(0,1);	
	}

	try 
	{
		d = lexical_cast<double>(text);
	} 
	catch(boost::bad_lexical_cast &) {
		cout << "dataNucleicLevel -> main.cpp -> castStringToDouble()" << endl;
		cout << text << endl;
		exit(EXIT_FAILURE);		
	}
	
	return d;
			
}

void initIndiceCol(vector<int>& indiceCol, int nbCol){

	indiceCol.clear();
	for (int i=0; i<nbCol; ++i){
		indiceCol.push_back(-1);	
	} 
	
}


void headerGlint (vector<string>& col, vector<int>& indiceCol){
	initIndiceCol(indiceCol,6);
	//"#chrom\tstart\tend\tu_start\tu_end\tnumDupli\tdupli\n";
	for (int i=0; i<lexical_cast<int>(col.size()); i++){
			if (col.at(i) == "q_chrom"){ 
				indiceCol.at(0) = i;
			} else if (col.at(i) == "q_start"){ 
				indiceCol.at(1) = i;
			} else if (col.at(i) == "q_end"){ 
				indiceCol.at(2) = i;
			} else if (col.at(i) == "s_chrom"){ 
				indiceCol.at(3) = i;
			} else if (col.at(i) == "s_start"){ 
				indiceCol.at(4) = i;
			} else if (col.at(i) == "s_end"){ 
				indiceCol.at(5) = i;
			} 
		}
}

void SupprimeTousLesCaracteres(std::string& Str, char C )
{ 
	size_t espace = Str.find_first_of( C );
	while (string::npos!=espace) {
    	Str.erase(espace);
    	espace = Str.find_first_of( C );
	} 
}  


void filterGlintOut (char* fileLow, char* fileCentro, char* fileGlint, ofstream& ofOut, char* espece, double seuil, int maxOverlap, int distMaxbetweenAnchor, bool booFiltreCentro) {
	
	map< string,vector<int> > mapOverlap;
	map< string,vector<int> >::iterator itOverlap;
	
	map< string,vector<int> > mapFilter;
	map< string,vector<int> >::iterator itFilter;
	
	vector<string> col;
	vector<int> indiceCol;
	
	string chrom;
	size_t foundBegin;
	size_t foundEnd;
	int cptN = 0;
	string line;
	
	ifstream ifLow(fileLow, ios::in);
	while (getline(ifLow, line)){
		if (!line.empty()){
			if(line.at(0)=='>'){
				foundBegin = line.find(espece);
				foundEnd = line.find('_');
				if (foundBegin!=string::npos && foundEnd!=string::npos) {
					
				} else {
					foundEnd = line.length();
				}
				
				if (foundBegin!=string::npos && foundEnd!=string::npos) {
					foundBegin += strlen(espece);
					
					chrom = line.substr(foundBegin,foundEnd-foundBegin);
					if (chrom.at(0)=='M' || chrom.at(0)=='m') chrom = "100";
					if (chrom.at(0)=='C' || chrom.at(0)=='c') chrom = "101";
					
					mapFilter.insert(pair< string,vector<int> > (chrom, vector<int>() ) );
				} else {
					cerr << "dataNucleicLevel -> main.cpp -> filterGlintOut() 1 : name of chrom not found" << endl;
					cerr << line << endl;
					cerr << chrom << endl;
					cerr << espece << endl;
					cerr << foundBegin << " # " << foundEnd << endl;
					exit(EXIT_FAILURE);	
				}
				
				itFilter = mapFilter.find(chrom);
				if (itFilter == mapFilter.end()){
					cerr << "dataNucleicLevel -> main.cpp -> filterGlintOut() 2 : name of chrom not found" << endl;
					cerr << chrom << endl;
					exit(EXIT_FAILURE);	
				}
			} else {
				for(unsigned int i=0 ;i<line.length(); ++i){
					if (line.at(i)=='n' || line.at(i)=='N') {
						itFilter->second.push_back(1);
						++cptN;
					}	else {
						itFilter->second.push_back(0);
					}
				}	
			}
		}
	}
	ifLow.close();
	
	
	
	if (fileCentro != NULL ) {;
		cout << "\tread file centromere" << endl;
			
		ifstream ifCentro(fileCentro, ios::in);
		while (getline(ifCentro, line)){
			if (!line.empty()){
				
				split(col, line, is_any_of("\t"),algorithm::token_compress_off);
				for (unsigned int i=0; i<col.size(); ++i){
					SupprimeTousLesCaracteres(col.at(i), ' ');
				}
				
				itFilter = mapFilter.find(col.at(0));
				if (itFilter==mapFilter.end()){
					cerr << "dataNucleicLevel -> main.cpp -> filterGlintOut() 3 : name of chrom not found" << endl;
					cerr << line << endl;
					cerr << col.at(0) << endl;
					//exit(EXIT_FAILURE);
				} else {
					for(int i=castStringToInt(col.at(1)) ;i<=castStringToInt(col.at(2)); ++i){
						itFilter->second.at(i) = 1;
					}
				}
			}
		}
		ifCentro.close();
	}
	
	cout << "\tread, filtre and write file glint" << endl;
	
	for (map< string,vector<int> >::iterator it = mapFilter.begin(); it != mapFilter.end(); ++it){
		mapOverlap.insert(pair< string,vector<int> > (it->first, vector<int>(it->second.size(),0) ) );
	}
	
	int qStart = 0;
	int qEnd = 0;
	int sStart = 0;
	int sEnd = 0;
	int qMin = 0;
	int sMin = 0;
	
	ifstream ifGlint(fileGlint, ios::in);
	getline(ifGlint, line);
	split(col, line, is_any_of("\t"),algorithm::token_compress_off);
	for (unsigned int i=0; i<col.size(); ++i){
		SupprimeTousLesCaracteres(col.at(i), ' ');
	}
	if (!line.empty()){
		headerGlint(col, indiceCol);
	} else {
		cerr << "dataNucleicLevel -> main.cpp -> filterGlintOut() 4 : no line in glint file" << endl;
		exit(EXIT_FAILURE);
	}
	
	while (getline(ifGlint, line)){
		if (!line.empty()){
			split(col, line, is_any_of("\t"),algorithm::token_compress_off);
			for (unsigned int i=0; i<col.size(); ++i){
				SupprimeTousLesCaracteres(col.at(i), ' ');
			}
			
			if (col.at(0).compare(col.at(3)) == 0) {
				itOverlap = mapOverlap.find(col.at(0));
				
				qStart = castStringToInt(col.at(indiceCol.at(1)))-1;
				qEnd = castStringToInt(col.at(indiceCol.at(2)))-1;
				qMin = (qStart<qEnd)?	qStart:qEnd; 
				
				sStart = castStringToInt(col.at(indiceCol.at(4)))-1;
				sEnd = castStringToInt(col.at(indiceCol.at(5)))-1;
				sMin = (sStart<sEnd)?	sStart:sEnd; 
				
				if(abs(qMin-sMin)<=distMaxbetweenAnchor) {
					for(int i=qStart ;i<=qEnd; ++i){
						++itOverlap->second.at(i);
					}
					
					for(int i=sStart ;i<=sEnd; ++i){
						++itOverlap->second.at(i);
					}
				}
			}
		}
	}
	ifGlint.close();
	
	if (booFiltreCentro) {
		for (map< string,vector<int> >::iterator it = mapOverlap.begin(); it != mapOverlap.end(); ++it){
			itFilter = mapFilter.find(it->first);
			
			if (itFilter==mapFilter.end()){
				cerr << "dataNucleicLevel -> main.cpp -> filterGlintOut() 5 : name of chrom not found" << endl;
				cerr << it->first << endl;
				exit(EXIT_FAILURE);
			}
			
			
			for(unsigned int i=0; i<it->second.size(); ++i){
				if(	it->second.at(i)>maxOverlap ){
					itFilter->second.at(i)=1;
				}
			}
			
			cout << it->first << " : " << *max_element(it->second.begin(), it->second.end()) << endl;
		}
	}
	
	
	
	ofOut << "q_chrom\tq_name\tq_start\tq_end\ts_chrom\ts_name\ts_start\ts_end\tpIdentity\tevalue\tscore\n";
	
	
	qStart = 0;
	qEnd = 0;
	sStart = 0;
	sEnd = 0;
	qMin = 0;
	sMin = 0;
	int sumBadNuc = 0;
	int length = 0;
	
	ifstream ifGlint2(fileGlint, ios::in);
	getline(ifGlint2, line);
	split(col, line, is_any_of("\t"),algorithm::token_compress_off);
	for (unsigned int i=0; i<col.size(); ++i){
		SupprimeTousLesCaracteres(col.at(i), ' ');
	}
	
	if (!line.empty()){
		headerGlint(col, indiceCol);
	} else {
		cerr << "dataNucleicLevel -> main.cpp -> filterGlintOut() 6 : no line in glint file" << endl;
		exit(EXIT_FAILURE);
	}
		
	while (getline(ifGlint2, line)){
		if (!line.empty()){
			split(col, line, is_any_of("\t"),algorithm::token_compress_off);
			for (unsigned int i=0; i<col.size(); ++i){
				SupprimeTousLesCaracteres(col.at(i), ' ');
			}
			
			qStart = castStringToInt(col.at(indiceCol.at(1)))-1;
			qEnd = castStringToInt(col.at(indiceCol.at(2)))-1;
			qMin = (qStart<qEnd)?	qStart:qEnd; 
			
			sStart = castStringToInt(col.at(indiceCol.at(4)))-1;
			sEnd = castStringToInt(col.at(indiceCol.at(5)))-1;
			sMin = (sStart<sEnd)?	sStart:sEnd;
			
			if(col.at(indiceCol.at(0)).compare(col.at(indiceCol.at(3))) != 0) continue;
			if(abs(qMin-sMin)>distMaxbetweenAnchor) continue;
			
			itFilter = mapFilter.find(col.at(indiceCol.at(0)));
			if (itFilter==mapFilter.end()){
				cerr << "dataNucleicLevel -> main.cpp -> filterGlintOut() 7 : name of chrom not found" << endl;
				cerr << line << endl;
				cerr << indiceCol.at(0) << endl;
				cerr << col.at(indiceCol.at(0)) << endl;
				exit(EXIT_FAILURE);
			}
			
			if (qEnd >= numeric_cast<int>(itFilter->second.size())){
				cout << line << endl;
				cout << itFilter->second.size() << endl;
				cout << qEnd << endl;
				exit(EXIT_FAILURE);
			}
			
			for(int i=qStart ;i<=qEnd; ++i){
				sumBadNuc += itFilter->second.at(i);
			}
			
			length += qEnd-qStart+1;
			
			
			
			itFilter = mapFilter.find(col.at(indiceCol.at(3)));
			if (itFilter==mapFilter.end()){
				cerr << "dataNucleicLevel -> main.cpp -> filterGlintOut() 8 : name of chrom not found" << endl;
				cerr << line << endl;
				cerr << indiceCol.at(3) << endl;
				cerr << col.at(indiceCol.at(3)) << endl;
				exit(EXIT_FAILURE);
			}
			
			if (sEnd >= numeric_cast<int>(itFilter->second.size())){
				cout << line << endl;
				cout << itFilter->second.size() << endl;
				cout << sEnd << endl;
				exit(EXIT_FAILURE);
			}
						
			for(int i=sStart ;i<=sEnd; ++i){
				sumBadNuc += itFilter->second.at(i);
			}
			
			length += sEnd-sStart+1;
			
			if ( col.at(indiceCol.at(0)).compare("100") != 0 && col.at(indiceCol.at(0)).compare("101") ) {
				if ( numeric_cast<double>(sumBadNuc)/length < seuil) {
					ofOut << line << "\n";	
				}
			}
		}
		
		sumBadNuc = 0;
		length = 0;
	}
	ifGlint2.close();
	
}


int main(int argc, char **argv) {

	char* fileLow = 0;
	char* fileCentro = 0;
	char* fileGlint = 0;
	char* fileOut = 0;
	char* espece = 0;
	
	double seuil = 0.50;
	int distMaxbetweenAnchor = 150000;
	bool booFiltreCentro = false;
	
	cout << "read param" << endl;
	
	int opt;
	while ((opt = getopt(argc, argv, "L:C:G:O:S:D:c")) != -1) {
		switch (opt) {
 		case 'L': // file in
			fileLow = optarg;
			break;
		case 'C': // file cov
			fileCentro = optarg;
			break;
		case 'G': // file cov
			fileGlint = optarg;
			break;
		case 'D': // file cov
			distMaxbetweenAnchor = lexical_cast<int>(optarg);
			break;
		case 'O': // file out
			fileOut = optarg;
			break;
		case 'c': // file out
			booFiltreCentro = true;
		case 'S': // file out
			espece = optarg;
			break;
		default: 
			cerr << "Usage: -L name -C name -G name -O name -S name -D int -c" << endl;
			cerr << "-L name : file low complexity in input" << endl;
			cerr << "-C name : file centromere in input " << endl;
			cerr << "-G name : file glint in input" << endl;
			cerr << "-O name : file glint filter in output" << endl;
			cerr << "-S name : nick name of specice. ex: arabidopsis thaliana is ath" << endl;
			cerr << "-D int : distance max between two region in anchor" << endl;
			cerr << "-c : Automatic filter centro" << endl;
			cerr << "./src/dataNucleicLevel_exec -L ~/workspace/red/donnee/ath_tair9/mdust.out -C  ~/workspace/red/donnee/ath_tair9/centro.out2 -G  ~/workspace/red/donnee/ath_tair9/glintChrom.out -O  ~/workspace/red/donnee/ath_tair9/glintChrom-filtered.out2 -S ath" << endl;
			exit(EXIT_FAILURE);
		}
	}
	
	cout << "end of read param" << endl;
	
	ofstream ofOut(fileOut);
	filterGlintOut(fileLow, fileCentro, fileGlint, ofOut, espece, seuil, 20, distMaxbetweenAnchor, booFiltreCentro);
	ofOut.close();
	
	cout << "end of program" << endl;
}
