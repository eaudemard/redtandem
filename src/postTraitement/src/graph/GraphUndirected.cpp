#include "GraphUndirected.h"

namespace graphe{

/**
 * \brief a sorting function for anchors
 * \param 
 */
bool CmpPtrAnchorScore( Anchor* i, Anchor* j)
{
	return ( i->getScore() > j->getScore() );
}


	GraphUndirected::GraphUndirected (map<int, Anchor*>& mAnchor, map<int, Region*>& mRegion) : g(mAnchor.size()) {
			mapAnchor = mAnchor;
			mapRegion = mRegion;
			
			idGraph=-1;
			
			start = std::numeric_limits<int>::max();
			end = std::numeric_limits<int>::min();
			chrom = "";
			
			anchorMax = NULL;
	}
	
	GraphUndirected::GraphUndirected (int id, int numberAnchor) : g(numeric_cast<unsigned int>(numberAnchor)) {
			idGraph=id;
			
			start = std::numeric_limits<int>::max();
			end = std::numeric_limits<int>::min();
			chrom = "";
			
			anchorMax = NULL;
	}
	
	void GraphUndirected::createGraph(double dPoverlap) {
		graph_traits<Graph>::vertex_descriptor u, v;
					
		double pOverlap1 = 0;
		double pOverlap2 = 0;
		double pOverlap3 = 0;
		double pOverlap4 = 0;
		double pOverlap5 = 0;
		double pOverlap6 = 0;
		double pOverlap7 = 0;
		double pOverlap8 = 0;
		int noOverlap = 0;
		int cptSommet = 0;
		
		
		for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor != mapAnchor.end(); ++itAnchor){
		
			u = vertex((*itAnchor).second->getNumVertexGraph(), g);
			noOverlap = 0;
			cptSommet = 0;
			
			map<int, Anchor*>::const_iterator itAnchorNext = itAnchor;
			++itAnchorNext;
			while ( itAnchorNext != mapAnchor.end() && (*itAnchor).second->getQueryChrom() == (*itAnchorNext).second->getQueryChrom() 
					&& (*itAnchor).second->getSubjectChrom() == (*itAnchorNext).second->getSubjectChrom()) {
			
				pOverlap1 = (*itAnchor).second->pOverlapSubject(*(*itAnchorNext).second);
				pOverlap2 = (*itAnchor).second->pOverlapQuery(*(*itAnchorNext).second);
				pOverlap3 = (*itAnchor).second->pOverlapQuerySubject(*(*itAnchorNext).second);
				pOverlap4 = (*itAnchor).second->pOverlapSubjectQuery(*(*itAnchorNext).second);
				pOverlap5 = (*itAnchorNext).second->pOverlapSubject(*(*itAnchor).second);
				pOverlap6 = (*itAnchorNext).second->pOverlapQuery(*(*itAnchor).second);
				pOverlap7 = (*itAnchorNext).second->pOverlapQuerySubject(*(*itAnchor).second);
				pOverlap8 = (*itAnchorNext).second->pOverlapSubjectQuery(*(*itAnchor).second);
				
				if (pOverlap1 >= dPoverlap || pOverlap2 >= dPoverlap || pOverlap3 >= dPoverlap || pOverlap4 >= dPoverlap || 
					pOverlap5 >= dPoverlap || pOverlap6 >= dPoverlap || pOverlap7 >= dPoverlap || pOverlap8 >= dPoverlap){
					v = vertex((*itAnchorNext).second->getNumVertexGraph(), g);
					add_edge(u, v, g);
				} else {
					++noOverlap;
				}
				
				pOverlap1 = 0;
				pOverlap2 = 0;
				pOverlap3 = 0;
				pOverlap4 = 0;
				pOverlap5 = 0;
				pOverlap6 = 0;
				pOverlap7 = 0;
				pOverlap8 = 0;
				
				++itAnchorNext;
				++cptSommet;
			}
		}
	}
	
	void GraphUndirected::printRegionTandem (ofstream& fileTandem) const {	
		fileTandem << anchorMax->getQueryChrom() << "\t" << start << "\t" << end << "\t";
		
		if (anchorMax->getQueryPosMax() - anchorMax->getQueryPosMin() > anchorMax->getSubjectPosMax() - anchorMax->getSubjectPosMin()){
			fileTandem << anchorMax->getQueryPosMin()	<< "\t" << anchorMax->getQueryPosMax();	
		} else {
			fileTandem << anchorMax->getSubjectPosMin()	<< "\t" << anchorMax->getSubjectPosMax();
		} 
		
		if ( anchors.size() > 0 )
		{
			fileTandem << "\t";
			for ( vector<Anchor*>::const_iterator it = anchors.begin(); it != anchors.end(); it ++ )
			{
				if ( (*it)->getScore() > 0 )
				{				
					fileTandem << (*it)->getQueryPosMin()<<"-"<<(*it)->getQueryPosMax()<<":"<<(*it)->getSubjectPosMin()<<"-"<<(*it)->getSubjectPosMax();
					fileTandem << "("<<(*it)->getScore()<<"),";
				}
			}
		}		
		fileTandem << endl;
	}
		
	void GraphUndirected::printSingletonRegion (ofstream& fileSingleton, int maxDistBetweenTandem) const {	
			
		map<int, Anchor*>::const_iterator saveIt = mapAnchor.end();
		for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor != mapAnchor.end(); ++itAnchor){
			if ((*itAnchor).second->getScore() > 0 ){
				saveIt = itAnchor;
			}
		}
		
		if (saveIt != mapAnchor.end()) {
			
			int start = (*saveIt).second->getQueryPosMin();
			int end = (*saveIt).second->getQueryPosMax();
			
			if (start>(*saveIt).second->getSubjectPosMin()) start = (*saveIt).second->getSubjectPosMin();
			if (end<(*saveIt).second->getSubjectPosMax()) end = (*saveIt).second->getSubjectPosMax();
			
			if (abs((*saveIt).second->getQueryPosMin()-(*saveIt).second->getSubjectPosMin())>maxDistBetweenTandem) return;
			
			fileSingleton << (*saveIt).second->getQueryChrom();
			fileSingleton << "\t" << start << "\t" << end;
			fileSingleton << "\t" << (*saveIt).second->getQueryPosMin() << "\t" << (*saveIt).second->getQueryPosMax();
			fileSingleton << "\t2";
			fileSingleton << "\t" << (*saveIt).second->getQueryPosMin() << ".." << (*saveIt).second->getQueryPosMax() << ",";
			fileSingleton << (*saveIt).second->getSubjectPosMin() << ".." << (*saveIt).second->getSubjectPosMax() << ",";
			fileSingleton << "\n";
		}
	}
	
	
	void GraphUndirected::connectedComponents(vector<GraphUndirected*>& vecGraph, double dPoverlap){
		vector<int> component(num_vertices(g));
		int num = connected_components(g, &component[0]);
		vecGraph.erase(vecGraph.begin(),vecGraph.end());
		
		map<int, Anchor*>::iterator it;
		int i;
		
		
		cout << "number of composante connexe " << num << endl;
		vector<int> numSommet(num,0);
		for (i = 0; i != lexical_cast<int>(component.size()); ++i){
			numSommet[component[i]]++;
		}
		
		int numComp = 0;
		for (int cpt = 0; cpt != num; ++cpt){
			if (numSommet.at(cpt)>1) ++numComp;
		}
		
		cout << "\tNumber of components connexes with size > 1 : " << numComp << endl; 
		
		for (int cpt = 0; cpt < num; ++cpt){
			vecGraph.push_back(new GraphUndirected(cpt, numSommet.at(cpt)));
		}
		vecGraph.resize(vecGraph.size());
		
		int numVertex = 0;
		for (i = 0; i < numeric_cast<int>(component.size()); ++i){
			it = mapAnchor.find(i);
			(vecGraph.at(component[i]))->addAnchor(*(*it).second, component[i]);
			numVertex = (vecGraph.at(component[i]))->getNumAnchor()-1;
			(vecGraph.at(component[i]))->addRegion(*((*it).second->getRegionQuery()), component[i], numVertex);
			(vecGraph.at(component[i]))->addRegion(*((*it).second->getRegionSubject()), component[i], numVertex);
		}
		
		int cptDelete = 0;
		
		cout << "fin for 3 : " << vecGraph.size() << " deleted : " << cptDelete << endl;
		
		for (int cpt = 0; cpt < lexical_cast<int>(vecGraph.size()); ++cpt){
		      vecGraph.at(cpt)->createGraph(dPoverlap);
		}
		    
		g.clear();
	}
	
	void GraphUndirected::addAnchor(Anchor& A, int idGraph) {
		A.setIdGraph(idGraph);
		A.setNumVertexGraph(numeric_cast<int>(mapAnchor.size()));
		mapAnchor.insert(pair<int, Anchor*>(numeric_cast<int>(A.getId()),&A));
		
		chrom = A.getQueryChrom();
		
		if (start>A.getQueryPosMin()) start = A.getQueryPosMin();
		if (start>A.getSubjectPosMin()) start = A.getSubjectPosMin();
		
		if (end<A.getQueryPosMax()) end = A.getQueryPosMax();
		if (end<A.getSubjectPosMax()) end = A.getSubjectPosMax();
		
		if (anchorMax == NULL || anchorMax->getScore() < A.getScore()) anchorMax = &A;
	}
	
	bool GraphUndirected::badLengthUnitRegion(){
		
		int unitLength = 0;
		map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin();
		
		int startQuery = (*itAnchor).second->getQueryPosMin();
		int endQuery = (*itAnchor).second->getQueryPosMax();
		int startSubject = (*itAnchor).second->getSubjectPosMin();
		int endSubject = (*itAnchor).second->getSubjectPosMax();
		
		int maxLength = 0;
		
		for (itAnchor = mapAnchor.begin(); itAnchor != mapAnchor.end(); ++itAnchor){
			if (startQuery > (*itAnchor).second->getQueryPosMin() )
				startQuery = (*itAnchor).second->getQueryPosMin();
				
			if (endQuery < (*itAnchor).second->getQueryPosMax() )
				endQuery = (*itAnchor).second->getQueryPosMax();
				
			if (startSubject > (*itAnchor).second->getSubjectPosMin() )
				startSubject = (*itAnchor).second->getSubjectPosMin();
				
			if (endSubject < (*itAnchor).second->getSubjectPosMax() )
				endSubject = (*itAnchor).second->getSubjectPosMax();
		}
		
		
		if ( (endQuery-startQuery) < (endSubject-startSubject) )	maxLength = (endSubject-startSubject);
		else 														maxLength = (endQuery-startQuery);
		
		for (itAnchor = mapAnchor.begin(); itAnchor != mapAnchor.end(); ++itAnchor){
			unitLength = (*itAnchor).second->getQueryPosMax() - (*itAnchor).second->getQueryPosMax();
			if (unitLength*2 > maxLength) return true;
			unitLength = (*itAnchor).second->getSubjectPosMax() - (*itAnchor).second->getSubjectPosMax();
			if (unitLength*2 > maxLength) return true;
		}
		
		return false;
	}
	
	bool GraphUndirected::badConnectedComponents(){
		
		int cpt = 0;
		for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor != mapAnchor.end(); ++itAnchor){
			if ((*itAnchor).second->getScore() > 0 ) ++cpt;
		}
		
		if (cpt<1) return true;				
		return false;
	}
	
	bool GraphUndirected::singletonConnectedComponents(){
		
		map<int, Anchor*>::const_iterator saveIt;
		int cpt = 0;
		for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor != mapAnchor.end(); ++itAnchor){
			if ((*itAnchor).second->getScore() > 0 ){
				++cpt;
				saveIt = itAnchor;
			}
		}
		
		if (cpt==1) {
			for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor != mapAnchor.end(); ++itAnchor){
				if ((*itAnchor).second->getScore() == 0 ){
					if ((*itAnchor).second->pOverlapQuery(*(*saveIt).second) != 1 )		return false;
					if ((*itAnchor).second->pOverlapSubject(*(*saveIt).second) != 1 )	return false;
				}
			}
		}			
				
		return true;
	}
	
	int GraphUndirected::getDistDiagDistOfFirstAnchor() const {
		if (mapAnchor.size()!= 0) {
			map<int, Anchor*>::const_iterator it = mapAnchor.begin();
			return (*it).second->getDistBetweenAnchor();
		} else return 0;
	}
	
	void GraphUndirected::affiche() const {
			cout << "############################" << endl;
			cout << "connexe " << idGraph;
			cout << " - start " << start;
			cout << " - end " << end;
			cout << endl;
			cout << "****************************" << endl;
			anchorMax->affiche();
			cout << "############################" << endl;
			cout << endl;
	}
	
	map<string,string> GraphUndirected::getMapTrueTandem () const {
		
		map<string,string> mapRes;
		
		for (map<int, Region*>::const_iterator itRegion = mapRegion.begin(); itRegion!=mapRegion.end(); ++itRegion){
			if (mapRes.find((*itRegion).second->getProt())==mapRes.end()) mapRes.insert(pair<string, string>((*itRegion).second->getName(), (*itRegion).second->getName()));
		}
		
		return mapRes;
	}
	
	map<string,int> GraphUndirected::getMapIdTrueTandem () const {
		
		map<string,int> mapRes;
		
		for (map<int, Region*>::const_iterator itRegion = mapRegion.begin(); itRegion!=mapRegion.end(); ++itRegion){
			if (mapRes.find((*itRegion).second->getName())==mapRes.end()) mapRes.insert(pair<string, int>((*itRegion).second->getName(), (*itRegion).second->getId()));
		}
		
		return mapRes;
	}
	
	double GraphUndirected::getAnchorEvalue () const {
		double evalue = 0;
		for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor!=mapAnchor.end(); ++itAnchor){
			evalue += (*itAnchor).second->getEvalue();
		}
		
		return evalue/mapAnchor.size();
	}
	double GraphUndirected::getAnchorIdentity () const {
		double identity = 0;
		for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor!=mapAnchor.end(); ++itAnchor){
			identity += (*itAnchor).second->getIdentity();
		}
		
		return identity/mapAnchor.size();
	}
	int GraphUndirected::getAnchorCoverage () const {
		int coverage = 0;
		for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor!=mapAnchor.end(); ++itAnchor){
			coverage += (*itAnchor).second->getCoverage();
		}
		
		return coverage/mapAnchor.size();
	}
	
	void GraphUndirected::findMaxAnchor(int typeUnit) {
	
		
		int lengthMax = (*(mapAnchor.begin())).second->getDistDiag();
		anchorMax = (*(mapAnchor.begin())).second;
		double scoreMax = 0;
		
		if (typeUnit == 0) {
			for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor != mapAnchor.end(); ++itAnchor){
				if ( (*itAnchor).second->getScore() != 0 && (*itAnchor).second->getDistDiag() < lengthMax ) {
					lengthMax = (*itAnchor).second->getDistDiag();
				}
			}
		
			for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor != mapAnchor.end(); ++itAnchor){
				if ( (*itAnchor).second->getLengthQuery() <= lengthMax && (*itAnchor).second->getLengthSubject() <= lengthMax ) {
					if (scoreMax < (*itAnchor).second->getScore()){
						anchorMax = (*itAnchor).second;
						scoreMax = (*itAnchor).second->getScore();
					}  
				}
			}
		} else if (typeUnit == 1) {
			for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor != mapAnchor.end(); ++itAnchor){
				if (scoreMax < (*itAnchor).second->getScore()){
					anchorMax = (*itAnchor).second;
					scoreMax = (*itAnchor).second->getScore();
				  
				}
			}
		} else if (typeUnit == 2) {
			lengthMax = (*(mapAnchor.begin())).second->getLengthQuery();
			for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor != mapAnchor.end(); ++itAnchor){
				if (lengthMax < (*itAnchor).second->getLengthQuery()){
					anchorMax = (*itAnchor).second;
					lengthMax = (*itAnchor).second->getLengthQuery();
				  
				}
				if (lengthMax < (*itAnchor).second->getLengthSubject()){
					anchorMax = (*itAnchor).second;
					lengthMax = (*itAnchor).second->getLengthSubject();
				  
				}
			}
		}  else if (typeUnit == 3) {
						
			for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor != mapAnchor.end(); ++itAnchor)
			{
				anchors.push_back( (*itAnchor).second );
			}
			sort(anchors.begin(),anchors.end(),CmpPtrAnchorScore);	
			vector<Anchor*>::const_iterator it = anchors.begin();
			anchorMax = *it;
		}
	}
		
	void GraphUndirected::printAnchor(ofstream& outAT) const {
	
		for (map<int, Anchor*>::const_iterator itAnchor = mapAnchor.begin(); itAnchor != mapAnchor.end(); ++itAnchor){
			outAT << (*itAnchor).second->getQueryChrom() << "\t";
			outAT << (*itAnchor).second->getQueryName() << "\t";
			outAT << (*itAnchor).second->getQueryStart() << "\t";
			outAT << (*itAnchor).second->getQueryEnd() << "\t";
			
			outAT << (*itAnchor).second->getSubjectChrom() << "\t";
			outAT << (*itAnchor).second->getSubjectName() << "\t";
			outAT << (*itAnchor).second->getSubjectStart() << "\t";
			outAT << (*itAnchor).second->getSubjectEnd() << "\t";
			
			outAT << (*itAnchor).second->getStrand() << "\t";
			
			outAT << (*itAnchor).second->getIdentity() << "\t";
			outAT << (*itAnchor).second->getEvalue() << "\t";
			outAT << (*itAnchor).second->getScore() << "\n";
			
		}
		
	}
	
	void GraphUndirected::printRegion(ofstream& outAR) const {
	
		for (map<int, Region*>::const_iterator itRegion = mapRegion.begin(); itRegion != mapRegion.end(); ++itRegion){
			outAR << (*itRegion).second->getChrom() << "\t";
			outAR << (*itRegion).second->getProt() << "\t";
			outAR << (*itRegion).second->getStart() << "\t";
			outAR << (*itRegion).second->getEnd() << "\n";	
		}
		
	}
	
	GraphUndirected::~GraphUndirected()
	{
	}
}
