#ifndef GRAPHUNDIRECTED_H_
#define GRAPHUNDIRECTED_H_

#include "../data/Region.h"
#include "../data/Anchor.h"
#include "../util/UtilRegion.h"

#include <vector>
#include <map>
#include <iostream>
#include <fstream>

//=============================================================
//lib boost
#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/numeric/conversion/cast.hpp>
//=============================================================

using namespace data;
using namespace std;
using namespace boost;
using namespace util;

namespace graphe {

class GraphUndirected
{
	public :
		int idGraph;
	
		map<int, Anchor*> mapAnchor;	//key = numVertexGraph
		map<int, Region*> mapRegion;	//key = idRegion
		
		typedef adjacency_list < vecS, vecS, undirectedS> Graph;
		Graph g;
		
		int start;
		int end;
		string chrom;
		
		Anchor* anchorMax;
		vector<	Anchor*> anchors;
		
		map<int, Anchor*>::iterator itAnchor;
		
	public:
		GraphUndirected(map<int, Anchor*>& mAnchor, map<int, Region*>& mRegion);
		GraphUndirected(int id, int numberAnchor);
		
		void createGraph(double dPoverlap);
		void connectedComponents(vector<GraphUndirected*>& vecGraph, double dPoverlap);
		void addAnchor(Anchor& A, int idGraph);
		
		bool badLengthUnitRegion();
		bool badConnectedComponents();
		bool singletonConnectedComponents();
		
		void affiche() const;
		
		inline Anchor* getAnchor(int key) {
			map<int,Anchor*>::iterator it;
			it = mapAnchor.find(key);
			if (it != mapAnchor.end()) return ((*it).second);
			return NULL;
		}
		inline Region* getRegion(int key) {
			map<int,Region*>::iterator it;
			it = mapRegion.find(key);
			if (it != mapRegion.end()) return ((*it).second);
			return NULL;
		}
		
		inline void addRegion(Region& R, int idGraph, int numVertex) {
				R.setIdGraph(idGraph);
				R.setNumVertexGraph(numVertex);
				mapRegion.insert(pair<int, Region*>(numeric_cast<int>(R.getId()),&R));
		}
		
		inline int getNumAnchor() const			{	return numeric_cast<int>(mapAnchor.size());		}
		inline int getNumRegion() const			{	return numeric_cast<int>(mapRegion.size());		}
		
		inline int getId() const				{	return idGraph;			}
		inline int getStart() const				{	return start;			}
		inline int getEnd() const				{	return end;				}
		inline string getChrom() const			{	return chrom;			}
		inline Anchor* getAnchorMax() const		{	return anchorMax;		}
		
		inline void setStart(int s)				{	start = s;				}
		inline void setEnd(int e)				{	end = e;				}
		inline void setChrom(string c)			{	chrom = c;				}
		
		inline void initAnchor()				{	itAnchor = mapAnchor.begin();			}
		inline bool nextAnchor()				{	++itAnchor; if(itAnchor == mapAnchor.end()) return false; return true;	}
		inline string getQueryNameAnchor()		{	return (*itAnchor).second->getQueryName();		}
		inline int getQueryStartAnchor()		{	return (*itAnchor).second->getQueryStart();		}
		inline int getQueryEndAnchor()			{	return (*itAnchor).second->getQueryEnd();		}
		inline string getQueryChromAnchor()		{	return (*itAnchor).second->getQueryChrom();		}
		
		inline string getSubjectNameAnchor()	{	return (*itAnchor).second->getSubjectName();	}
		inline int getSubjectStartAnchor()		{	return (*itAnchor).second->getSubjectStart();	}
		inline int getSubjectEndAnchor()		{	return (*itAnchor).second->getSubjectEnd();	}
		inline string getSubjectChromAnchor()	{	return (*itAnchor).second->getSubjectChrom();	}
		
		int getDistDiagDistOfFirstAnchor() const;	
		void printRegionTandem (ofstream& fileTandem) const;	
		void printSingletonRegion (ofstream& fileSingleton, int maxDistBetweenTandem) const;	
		map<string,string> getMapTrueTandem () const;
		map<string,int> getMapIdTrueTandem () const;
		double getAnchorEvalue () const;
		double getAnchorIdentity () const;
		int getAnchorCoverage () const;
		void printAnchor(ofstream& outAT) const;
		void printRegion(ofstream& outRT) const;
		void findMaxAnchor(int typeUnit);
		
		virtual ~GraphUndirected();
	};
}

#endif /*GRAPHUNDIRECTED_H_*/
