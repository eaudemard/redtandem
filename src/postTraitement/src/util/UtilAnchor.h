#ifndef UTILANCHOR_H_
#define UTILANCHOR_H_

#include "../data/Anchor.h"
//#include <boost/lexical_cast.hpp>

using namespace data;

namespace util {
	
	inline bool compareAnchor(const Anchor* A1, const Anchor* A2) {
		int A1_qChrom = -1;
		int A1_sChrom = -1;
		int A2_qChrom = -1;
		int A2_sChrom = -1;
		
		try {
        	A1_qChrom = boost::lexical_cast<int>(A1->getQueryChrom());
        } catch(boost::bad_lexical_cast &) { 
        	if (A1->getQueryChrom().compare("X") == 0) A1_qChrom = 50;
        	if (A1->getQueryChrom().compare("Y") == 0) A1_qChrom = 51;
        	if (A1->getQueryChrom().compare("MT") == 0) A1_qChrom = 52;
        	
        	if (A1_qChrom == -1) A1_qChrom = 100;
        }
        
        try {
        	A1_sChrom = boost::lexical_cast<int>(A1->getSubjectChrom());
        } catch(boost::bad_lexical_cast &) { 
        	if (A1->getSubjectChrom().compare("X") == 0) A1_sChrom = 50;
        	if (A1->getSubjectChrom().compare("Y") == 0) A1_sChrom = 51;
        	if (A1->getSubjectChrom().compare("MT") == 0) A1_sChrom = 52;
        	
        	if (A1_sChrom == -1) A1_sChrom = 100;
        }
        
        try {
        	A2_qChrom = boost::lexical_cast<int>(A2->getQueryChrom());
        } catch(boost::bad_lexical_cast &) {
        	if (A2->getQueryChrom().compare("X") == 0) A2_qChrom = 50;
        	if (A2->getQueryChrom().compare("Y") == 0) A2_qChrom = 51;
        	if (A2->getQueryChrom().compare("MT") == 0) A2_qChrom = 52;
        	
        	if (A2_qChrom == -1) A2_qChrom = 100;
        }
        
        try {
        	A2_sChrom = boost::lexical_cast<int>(A2->getSubjectChrom());
        } catch(boost::bad_lexical_cast &) {
        	if (A2->getSubjectChrom().compare("X") == 0) A2_sChrom = 50;
        	if (A2->getSubjectChrom().compare("Y") == 0) A2_sChrom = 51;
        	if (A2->getSubjectChrom().compare("MT") == 0) A2_sChrom = 52; 
        	
        	if (A2_sChrom == -1) A1_sChrom = 100;
        }
        
        
        
		if ( A1_qChrom < A2_qChrom )
			return true;
		if ( A1_qChrom > A2_qChrom )
			return false;
			
		if ( A1_sChrom < A2_sChrom )
			return true;
		if ( A1_sChrom > A2_sChrom )
			return false;
      
		if (A1->getQueryPosMin() < A2->getQueryPosMin())
			return true;
		if (A1->getQueryPosMin() > A2->getQueryPosMin())
			return false;
		
		if (A1->getQueryPosMax() < A2->getQueryPosMax())
			return true;
		if (A1->getQueryPosMax() > A2->getQueryPosMax())
			return false;
			
		if (A1->getSubjectPosMin() < A2->getSubjectPosMin())
			return true;
		if (A1->getSubjectPosMin() > A2->getSubjectPosMin())
			return false;
		
		if (A1->getSubjectPosMax() < A2->getSubjectPosMax())
			return true;
		if (A1->getSubjectPosMax() > A2->getSubjectPosMax())
			return false;
			
		if (A1->getId() < A2->getId())
			return true;
		if (A1->getId() > A2->getId())
			return false;
			
		return false;
	}
	
	
}

#endif /*UTILANCHOR_H_*/