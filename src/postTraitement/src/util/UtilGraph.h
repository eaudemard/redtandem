#ifndef UTILGRAPH_H_
#define UTILGRAPH_H_

#include "../graph/GraphUndirected.h"

using namespace graphe;

namespace util {
	inline bool compareGraphUndirected(const GraphUndirected* G1, const GraphUndirected* G2) {
		int G1_Chrom = -1;
		int G2_Chrom = -1;
		
		try {
        	G1_Chrom = boost::lexical_cast<int>(G1->getChrom());
        } catch(boost::bad_lexical_cast &) { 
        	if (G1->getChrom().compare("X") == 0) G1_Chrom = 50;
        	if (G1->getChrom().compare("Y") == 0) G1_Chrom = 51;
        	if (G1->getChrom().compare("MT") == 0) G1_Chrom = 52;
        	
        	if (G1_Chrom == -1) G1_Chrom = 100;
        }
        
        try {
        	G2_Chrom = boost::lexical_cast<int>(G2->getChrom());
        } catch(boost::bad_lexical_cast &) { 
        	if (G2->getChrom().compare("X") == 0) G2_Chrom = 50;
        	if (G2->getChrom().compare("Y") == 0) G2_Chrom = 51;
        	if (G2->getChrom().compare("MT") == 0) G2_Chrom = 52;
        	
        	if (G2_Chrom == -1) G2_Chrom = 100;
        }
		
		if ( G1_Chrom < G2_Chrom )
				return true;
		if ( G1_Chrom > G2_Chrom )
				return false;
		
		if (G1->getStart() < G2->getStart())
			return true;
		if (G1->getStart() > G2->getStart())
			return false;
		
		if (G1->getEnd() < G2->getEnd())
			return true;
		if (G1->getEnd() > G2->getEnd())
			return false;
			
		if (G1->getId() < G2->getId())
			return true;
		if (G1->getId() > G2->getId())
			return false;
			
		return false;
	}
	
	
}

#endif /*UTILGRAPH_H_*/
