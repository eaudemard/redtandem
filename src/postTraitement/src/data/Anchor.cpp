#include "Anchor.h"

namespace data{
	
	Anchor::Anchor(const string& l, const vector<int>& indice, const unsigned int idAnchor, unsigned int numVertex){
		
			vector<string> v;
			boost::split(v, l, boost::is_any_of("\t"),boost::algorithm::token_compress_off);
			
			id = idAnchor;	
			q_chrom = (indice[0]!=-1)?v.at(indice[0]):"-1";
			s_chrom = (indice[1]!=-1)?v.at(indice[1]):"-1";
			q_prot = (indice[7]!=-1)?v.at(indice[7]):"";
			s_prot = (indice[8]!=-1)?v.at(indice[8]):"";
			if (indice.size()>13) {
				q_name = (indice[13]!=-1)?v.at(indice[13]):"-1";
				s_name = (indice[14]!=-1)?v.at(indice[14]):"-1";
				
				if(indice[7]==-1)	q_prot = q_name;
				if(indice[8]==-1)	s_prot = s_name;
				
			} else {
				q_name = "-1";
				s_name = "-1";
			}
			
			try
	        {
				q_start = (indice[2]!=-1)? boost::lexical_cast<int>(v.at(indice[2])):-1;
				q_end = (indice[3]!=-1)? boost::lexical_cast<int>(v.at(indice[3])):-1;
				s_start = (indice[4]!=-1)? boost::lexical_cast<int>(v.at(indice[4])):-1;
				s_end = (indice[5]!=-1)? boost::lexical_cast<int>(v.at(indice[5])):-1;
				score = (indice[6]!=-1)? boost::lexical_cast<double>(v.at(indice[6])):-1;
				if (indice.size()>9) {
					identity = (indice[9]!=-1)? boost::lexical_cast<double>(v.at(indice[9])):-1;
					evalue = (indice[10]!=-1)? boost::lexical_cast<double>(v.at(indice[10])):-1;
					q_cov = (indice[11]!=-1)? boost::lexical_cast<double>(v.at(indice[11])):-1;
					s_cov = (indice[12]!=-1)? boost::lexical_cast<double>(v.at(indice[12])):-1;
				} else {
					identity = -1;
					evalue = -1;
				}
				
				if (indice.size()>15) {
					size = (indice[15]!=-1)?  boost::lexical_cast<int>(v.at(indice[15])):-1;
				} else {
					size = -1;
				}
			
				
	        }
	        catch(boost::bad_lexical_cast &) {
				cout << "le fichier est dans un mauvais format ce qui lève une exception sur un cast dans le constructeur des Regions" << endl;
				cout << l << endl;
				exit(EXIT_FAILURE);
						
			}
			
			if (s_start>s_end){
				std::swap(s_start,s_end);
				swapSubject = true;
			} else { swapSubject = false; }
			
			if (q_start>q_end){
				std::swap(q_start,q_end);
				swapQuery = true;
			} else { swapQuery = false; }
			
			
			
			
			numVertexGraph = numVertex;
			
			idGraph = -1;
			
		}
	
	Anchor::Anchor(const string& sQ_name, const string& sQ_chrom, int iQ_start, int iQ_end, double dQ_cov, 
				const string& sS_name, const string& sS_chrom, int iS_start, int iS_end, double dS_cov,
				double dIdentity, double dEvalue, int iScore, bool bSwap, int idAnchor) {
	
		q_name = sQ_name;
		q_chrom = sQ_chrom;
		q_start = iQ_start;
		q_end = iQ_end;
		q_cov = dQ_cov;
		
		s_name = sS_name;
		s_chrom = sS_chrom;
		s_start = iS_start;
		s_end = iS_end;
		s_cov = dS_cov;
		
		identity = dIdentity;
		evalue = dEvalue;
		score = iScore;
		
		swapQuery = false;
		swapSubject = bSwap;	
		
		id = idAnchor;
	}
	
	void Anchor::changeAnchor(const string& sQ_name, const string& sQ_chrom, int iQ_start, int iQ_end, double dQ_cov, 
				const string& sS_name, const string& sS_chrom, int iS_start, int iS_end, double dS_cov,
				double dIdentity, double dEvalue, int iScore, bool bSwap) {
	
		q_name = sQ_name;
		q_chrom = sQ_chrom;
		q_start = iQ_start;
		q_end = iQ_end;
		q_cov = dQ_cov;
		
		s_name = sS_name;
		s_chrom = sS_chrom;
		s_start = iS_start;
		s_end = iS_end;
		s_cov = dS_cov;
		
		identity = dIdentity;
		evalue = dEvalue;
		score = iScore;
		
		swapQuery = false;
		swapSubject = bSwap;
					
	}
	
	int Anchor::getDistBetweenAnchor() const {
		int start = (getQueryPosMax()<getSubjectPosMax())? getQueryPosMax():-getSubjectPosMax();
		int end = (getQueryPosMin()>getSubjectPosMin())? getQueryPosMin():-getSubjectPosMin();
		
		return end - start;
	}
	
	double Anchor::pOverlapQuery (const Anchor& A) const {
		
		int start = 0;
		int end = 0;
		
		if (A.getQueryChrom() == getQueryChrom()) {
			start = (A.getQueryStart()<getQueryStart()) ?  getQueryStart() : A.getQueryStart();
			end =  (A.getQueryEnd()>getQueryEnd()) ? getQueryEnd() : A.getQueryEnd();
			if( (getQueryEnd()-getQueryStart())!=0 )	return ((double)(end-start))/(getQueryEnd()-getQueryStart());
			else {
				cout << "1 div 0" << endl;
				affiche();
				A.affiche();
				return 0;
			}
		}
		
		return 0;	
	}
	
	double Anchor::pOverlapQuerySubject (const Anchor& A) const {
		
		int start = 0;
		int end = 0;
		
		if (A.getSubjectChrom() == getQueryChrom()) {
			start = (A.getSubjectStart()<getQueryStart()) ?  getQueryStart() : A.getSubjectStart();
			end =  (A.getSubjectEnd()>getQueryEnd()) ? getQueryEnd() : A.getSubjectEnd();
			if( (getQueryEnd()-getQueryStart())!=0 )	return ((double)(end-start))/(getQueryEnd()-getQueryStart());
			else {
				cout << "1 div 0" << endl;
				affiche();
				A.affiche();
				return 0;
			}
		}
		
		return 0;	
	}
	
	double Anchor::pOverlapSubject (const Anchor& A) const {
		
		int start = 0;
		int end = 0;
	
		if (A.getSubjectChrom() == getSubjectChrom()) {
			start = (A.getSubjectStart()<getSubjectStart()) ? getSubjectStart() : A.getSubjectStart();
			end = (A.getSubjectEnd()>getSubjectEnd()) ? getSubjectEnd() : A.getSubjectEnd();
			if( (getSubjectEnd()-getSubjectStart())!=0 )	return ((double)(end-start))/(getSubjectEnd()-getSubjectStart());
			else {
				cout << "1 div 0" << endl;
				affiche();
				A.affiche();
				return 0;
			}
		}
		
		return 0;	
	}
	
	double Anchor::pOverlapSubjectQuery (const Anchor& A) const {
		
		int start = 0;
		int end = 0;
	
		if (A.getQueryChrom() == getSubjectChrom()) {
			start = (A.getQueryStart()<getSubjectStart()) ? getSubjectStart() : A.getQueryStart();
			end = (A.getQueryEnd()>getSubjectEnd()) ? getSubjectEnd() : A.getQueryEnd();
			if( (getSubjectEnd()-getSubjectStart())!=0 )	return ((double)(end-start))/(getSubjectEnd()-getSubjectStart());
			else {
				cout << "1 div 0" << endl;
				affiche();
				A.affiche();
				return 0;
			}
		}
		
		return 0;	
	}
	
	double Anchor::getPente() const {
		
			double pente1 = abs(q_start-s_start) / abs(q_end-s_end);
			double pente2 = abs(q_end-s_end) / abs(q_start-s_start);
			
			if (pente1 <= 1) return pente1;
			else return pente2;
	}
	
	
	void Anchor::affiche() const {
		cout << "Anchor " << id;
		cout << " - q_chrom " << q_chrom;
		if (!swapQuery) { 
			cout << " - q_start " << q_start;
			cout << " - q_end " << q_end;
		} else {
			cout << " - q_start " << q_end;
			cout << " - q_end " << q_start;
		}
		cout << " - s_chrom " << s_chrom;
		if (!swapSubject) { 
			cout << " - s_start " << s_start;
			cout << " - s_end " << s_end;
		} else {
			cout << " - s_start " << s_end;
			cout << " - s_end " << s_start;
		}
		cout << " - idGraph " << idGraph;
		cout << " - numVertexGraph " << numVertexGraph;
		cout << endl;
	}
	
	void Anchor::printAnchor(ofstream& ofBlastp) const {
		
		
		if (q_chrom.compare(s_chrom)==0) {
			if (q_start < s_start) {
				ofBlastp << q_chrom;
				ofBlastp << "\t" << q_name;
				ofBlastp << "\t" << q_start;
				ofBlastp << "\t" << q_end;
				ofBlastp << "\t" << s_chrom;
				ofBlastp << "\t" << s_name;
				ofBlastp << "\t" << s_start;
				ofBlastp << "\t" << s_end;
			} else {
				ofBlastp << s_chrom;
				ofBlastp << "\t" << s_name;
				ofBlastp << "\t" << s_start;
				ofBlastp << "\t" << s_end;
				ofBlastp << "\t" << q_chrom;
				ofBlastp << "\t" << q_name;
				ofBlastp << "\t" << q_start;
				ofBlastp << "\t" << q_end;
			}
		} else {
		
			try
	        {
				if ( boost::lexical_cast<int>(q_chrom) < boost::lexical_cast<int>(s_chrom) ){
					ofBlastp << q_chrom;
					ofBlastp << "\t" << q_name;
					ofBlastp << "\t" << q_start;
					ofBlastp << "\t" << q_end;
					ofBlastp << "\t" << s_chrom;
					ofBlastp << "\t" << s_name;
					ofBlastp << "\t" << s_start;
					ofBlastp << "\t" << s_end;
				} else {
					ofBlastp << s_chrom;
					ofBlastp << "\t" << s_name;
					ofBlastp << "\t" << s_start;
					ofBlastp << "\t" << s_end;
					ofBlastp << "\t" << q_chrom;
					ofBlastp << "\t" << q_name;
					ofBlastp << "\t" << q_start;
					ofBlastp << "\t" << q_end;
				}
	        }
	        catch(boost::bad_lexical_cast &) {
				cout << "Anchor.cpp -> printAnchor() : pb de cast to int with chrom" << endl;
				cout << q_chrom << endl;
				cout << s_chrom << endl;
				exit(EXIT_FAILURE);
						
			}
			
		}
		
		if (swapQuery || swapSubject)	ofBlastp << "\t-";
		else							ofBlastp << "\t+";
		ofBlastp << "\t" << identity;
		ofBlastp << "\t" << evalue;
		ofBlastp << "\t" << score;
		ofBlastp << "\n";
		
	}
	
	Anchor::~Anchor()
	{
	}
}