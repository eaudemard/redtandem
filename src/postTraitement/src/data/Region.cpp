#include "Region.h"

namespace data{
	
	Region::Region(const string& l, const vector<int>& indice, const unsigned int idRegion, const bool ancreSubject, unsigned int numVertex){

		niveau = 1;
		name = "-1";
		
		vector<string> v;
		boost::split(v, l, boost::is_any_of("\t"),boost::algorithm::token_compress_off);
		
		id = idRegion;
		if (!ancreSubject) {		
			chrom = (indice[0]!=-1)?v.at(indice[0]):"-1";
			prot = (indice[7]!=-1)?v.at(indice[7]):"";
		} else {
			chrom = (indice[1]!=-1)?v.at(indice[1]):"-1";
			prot = (indice[8]!=-1)?v.at(indice[8]):"";
		}
		try
        {
        	if (!ancreSubject) {
				start = (indice[2]!=-1)? boost::lexical_cast<int>(v.at(indice[2])):-1;
				end = (indice[3]!=-1)? boost::lexical_cast<int>(v.at(indice[3])):-1;
        	} else {
				start = (indice[4]!=-1)? boost::lexical_cast<int>(v.at(indice[4])):-1;
				end = (indice[5]!=-1)? boost::lexical_cast<int>(v.at(indice[5])):-1;
        	}
        }
        catch(boost::bad_lexical_cast &) {
			cout << "le fichier est dans un mauvais format ce qui lève une exception sur un cast dans le constructeur des Regions" << endl;
			cout << l << endl;
			exit(EXIT_FAILURE);
					
		}
		
		if (start>end){
			std::swap(start,end);
			swap = true;
		} else { swap = false;}
		
		lengthProt = 0;
		
		numVertexGraph = numVertex;
		
		idGraph = -1;
	}
	
	Region::Region(int iStart, int iEnd, const string& sChrom, const string& sProt, const string& sName, unsigned int idRegion, unsigned int numVertex, bool bSwap){

		niveau = 1;
		
		id = idRegion;

		chrom = sChrom;
		name = sName;
		prot = sProt;
		start = iStart;
		end = iEnd;
		lengthProt = 0;
		
		swap = bSwap;
				
		numVertexGraph = numVertex;
		
		idGraph = -1;	
	}
	
	Region::Region(int iStart, int iEnd, const string& sChrom, const string& sName, bool strand, int idRegion){

		niveau = 1;
		
		id = idRegion;

		chrom = sChrom;
		name = sName;
		prot = "-1";
		start = iStart;
		end = iEnd;
		lengthProt = 0;
		
		swap = !strand;
		
				
		numVertexGraph = -1;
		
		idGraph = -1;	
	}
	
	Region::Region(const string& iStart, const string& iEnd, const string& sChrom) {
		niveau = 1;
		
		id = -1;

		chrom = sChrom;
		name = "-1";
		prot = "-1";
		try
        {
        	start = boost::lexical_cast<int>(iStart);
			end = boost::lexical_cast<int>(iEnd);
        }
        catch(boost::bad_lexical_cast &) {
			cout << "Region.cpp -> Region::Region(int iStart, int iEnd, const string& sChrom) : pb de cast de string vers int" << endl;
			cout << iStart << endl;
			cout << iEnd << endl;
			exit(EXIT_FAILURE);	
		}
		lengthProt = 0;
		
		swap = false;
		
		numVertexGraph = -1;
		
		idGraph = -1;
	}
	
	Region::Region(int iStart, int iEnd, const string& sChrom, const string& sName) {
		niveau = 1;
		
		id = -1;

		chrom = sChrom;
		name = sName;
		prot = "-1";
		
		start = iStart;
		end = iEnd;
		lengthProt = 0;
		
		swap = false;
		
		numVertexGraph = -1;
		
		idGraph = -1;
	}
	
	
	Region::Region(int iStart, int iEnd, const string& sChrom) {
		niveau = 1;
		
		id = -1;

		chrom = sChrom;
		name = "-1";
		prot = "-1";
		
		start = iStart;
		end = iEnd;
        
        lengthProt = 0;
		
		swap = false;
		
		numVertexGraph = -1;
		
		idGraph = -1;
	}
	
	void Region::changeGene(int iStart, int iEnd, const string& sChrom, const string& sName, bool strand){

		chrom = sChrom;
		name = sName;
		prot = "-1";
		start = iStart;
		end = iEnd;
		lengthProt = 0;
		
		swap = !strand;
		
		numVertexGraph = -1;
		
		idGraph = -1;	
	}
	
	
	bool Region::overlap(const Region& R) const {
		if (chrom.compare(R.getChrom())!=0) return false;
		
		if (getPosMin()<=R.getPosMin() && R.getPosMin()<=getPosMax())	return true;
		if (getPosMin()<=R.getPosMax() && R.getPosMax()<=getPosMax())	return true;
		
		if (R.getPosMin()<=getPosMin() && getPosMin()<=R.getPosMax())	return true;
		if (R.getPosMin()<=getPosMax() && getPosMax()<=R.getPosMax())	return true;
		
		return false;
	}
	
	double Region::pOverlap(const Region& R) const {
		int start = 0;
		int end = 0;
		
		if (R.getChrom().compare(getChrom()) == 0) {
			start = (R.getStart()<getStart()) ?  getStart() : R.getStart();
			end =  (R.getEnd()>getEnd()) ? getEnd() : R.getEnd();
			if( (getEnd()-getStart())!=0 )	return ((double)(end-start))/(getEnd()-getStart());
			else {
				cout << "1 div 0" << endl;
				affiche();
				R.affiche();
				return 0;
			}
		}
		
		return 0;
	}

	void Region::addNiveau(const Region& R) {
		++niveau;
		
		if (start>R.getPosMin()) start = R.getPosMin();
		if (end<R.getPosMax()) end = R.getPosMax();
	}
	
	void Region::affiche() const {
		cout << chrom;
		cout << "\t" << start;
		cout << "\t" << end;
		cout << "\t" << niveau;
		cout << "\t" << name;
		cout << "\t" << prot;
		cout << endl; 	
	}
	
	void Region::printGene(ofstream& ofProt) const {
		ofProt << chrom;
		ofProt << "\t" << name;
		ofProt << "\t" << start;
		ofProt << "\t" << end;
		if (getStrand()) ofProt << "\t1";
		else			 ofProt << "\t-1";
		ofProt << "\n";
	}
	
	void Region::printGeneWithoutStrand(ofstream& ofProt) const {
		ofProt << chrom;
		ofProt << "\t" << name;
		ofProt << "\t" << start;
		ofProt << "\t" << end;
		ofProt << "\n";
	}
	
}