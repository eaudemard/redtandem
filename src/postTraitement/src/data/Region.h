#ifndef REGION_H_
#define REGION_H_

#include <string>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>

using namespace std;

namespace data{
	
	class Anchor;
	
	class Region
	{
	
	unsigned int id;
	string chrom;
	string prot;
	string name;
	
	int start;
	int end;
	int niveau;
	int lengthProt;
	bool swap;
	
	Anchor* ancre;
	
	unsigned int idGraph;
	unsigned int numVertexGraph;
		
	public:
		Region(const string& l, const vector<int>& indice, const unsigned int idRegion, const bool ancreSubject, unsigned int numVertex);
		Region(int iStart, int iEnd, const string& sChrom, const string& sProt, const string& sName, unsigned int idRegion, unsigned int numVertex, bool bSwap);
		Region(int iStart, int iEnd, const string& sChrom, const string& sName, bool strand, int idRegion);
		Region(const string& iStart, const string& iEnd, const string& sChrom);
		Region(int iStart, int iEnd, const string& sChrom, const string& sName);
		Region(int iStart, int iEnd, const string& sChrom);
		
		void changeGene(int iStart, int iEnd, const string& sChrom, const string& sName, bool strand);
		
		inline unsigned int getId() const		{	return id;					}
		inline const string& getChrom() const	{	return chrom;				}
		inline const string& getProt() const	{	return prot;				}
		inline const string& getName() const	{	return name;				}
		inline int getStart() const				{	return start;				}
		inline int getEnd() const				{	return end;					}
		inline int getNiveau() const			{	return niveau;				}
		inline int getLength() const			{	return end-start+1;			}
		inline int getLengthProt() const		{	return lengthProt;			}
		inline bool getStrand() const			{	return !swap;				}
		inline bool getSwap() const				{	return swap;				}
		inline int getPrintStart() const		{	return (swap)? end:start;	}
		inline int getPrintEnd() const			{	return (swap)? start:end;	}
		
		inline int getPosMin() const					{	return std::min(start, end); }
		inline int getPosMax() const					{	return std::max(start, end); }
		
		
		inline void setStart(int iStart)				{	start = iStart;			}
		inline void setEnd(int iEnd)					{	end = iEnd;				}
		inline void setNiveau(int iNiveau)				{	niveau = iNiveau;		}
		
		inline void addAcideAmine(int nbAcide)			{	lengthProt += nbAcide;	}
		
		
		inline Anchor* getAnchor() const				{	return ancre;			}
		inline void setAnchor(Anchor* anc)				{ 	ancre = anc;			}
		
		inline unsigned int getIdGraph() const			{	return idGraph;			}
		inline void setIdGraph(unsigned int id)			{	idGraph = id;			}
		
		inline int getNumVertexGraph() const			{	return numVertexGraph;		}
		inline void setNumVertexGraph(int numV) 		{	numVertexGraph = numV;		}
		
		bool overlap(const Region& R) const;
		double pOverlap(const Region& R) const;
		void addNiveau(const Region& R);
		
		void affiche() const;
		void printGene(ofstream& ofProt) const;
		void printGeneWithoutStrand(ofstream& ofProt) const;
		
		friend std::ostream& operator<<(std::ostream& out, const Region& r)
		{
			return out << r.id << " " << r.chrom << " " << r.start << " " << r.end << " " << r.niveau << "\n";
		} 		
	};
}

#endif /*REGION_H_*/