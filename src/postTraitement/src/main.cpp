#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <valarray>
#include <getopt.h>

#include "data/Region.h"
#include "data/Anchor.h"
#include "util/UtilAnchor.h"
#include "util/UtilRegion.h"
#include "util/UtilGraph.h"
#include "graph/GraphUndirected.h"

#include <boost/checked_delete.hpp>

using namespace data;
using namespace boost;
using namespace graphe;
using namespace util;
using namespace std;


void readFiles (ifstream& ifReg, ifstream& ifGlint, vector<Region*>& vecRegion, vector<Anchor*>& vecAnchor, map<int, Region*>& mapRegion, map<int, Anchor*>& mapAnchor, int maxDist) {

	string line;
	getline(ifReg, line);
	vector<string> col;
	vector<int> indiceCol (16,-1);
	unsigned int idReg = 0;
	unsigned int idAnchor = 0;
	
	
	split(col, line, is_any_of("\t"),algorithm::token_compress_off);
		
	if (!col.empty()){
		//q_chrom	s_chrom	pident	size	mismatch	gaps	q_start	q_end	s_start	s_end	evalue	score
		for (unsigned int i=0; i<col.size(); i++){
			if (col.at(i) == "q_chrom"){ 
				indiceCol.at(0) = i;
			} else if (col.at(i) == "s_chrom"){ 
				indiceCol.at(1) = i;
			} else if (col.at(i) == "q_start"){ 
				indiceCol.at(2) = i;
			} else if (col.at(i) == "q_end"){ 
				indiceCol.at(3) = i;
			} else if (col.at(i) == "s_start"){ 
				indiceCol.at(4) = i;
			} else if (col.at(i) == "s_end"){ 
				indiceCol.at(5) = i;
			} else if (col.at(i) == "qg_start"){ 
				indiceCol.at(2) = i;
			} else if (col.at(i) == "qg_end"){ 
				indiceCol.at(3) = i;
			} else if (col.at(i) == "sg_start"){ 
				indiceCol.at(4) = i;
			} else if (col.at(i) == "sg_end"){ 
				indiceCol.at(5) = i;
			} else if (col.at(i) == "score"){ 
				indiceCol.at(6) = i;
			} else if (col.at(i) == "q_prot"){ 
				indiceCol.at(7) = i;
			} else if (col.at(i) == "s_prot"){ 
				indiceCol.at(8) = i;
			} else if (col.at(i) == "size"){ 
				indiceCol.at(15) = i;
			} 
		}
		
		cout << "end read header region" << endl;	
		
		
		while(getline(ifReg, line)){
			if (!line.empty()) { 
				vecAnchor.push_back(new Anchor(line,indiceCol,idAnchor,idAnchor));
								
				if ( abs(vecAnchor.at(vecAnchor.size()-1)->getQueryPosMin()-vecAnchor.at(vecAnchor.size()-1)->getSubjectPosMin()) > maxDist ) {
					vecAnchor.erase(vecAnchor.end()-1);
				}
				
				++idAnchor;
				
			}
		}
	}
	
	cout << "end read and add region chain" << endl; 
	getline(ifGlint, line);
	while(getline(ifGlint, line)){
		if (!line.empty()) { 
			vecAnchor.push_back(new Anchor(line,indiceCol,idAnchor,idAnchor));
			if ( abs(vecAnchor.at(vecAnchor.size()-1)->getQueryPosMin()-vecAnchor.at(vecAnchor.size()-1)->getSubjectPosMin()) <= maxDist) {
				vecAnchor.at(vecAnchor.size()-1)->setScore(0);
			} else {
				vecAnchor.erase(vecAnchor.end()-1);
			}
			++idAnchor;
		}
	}
	
	
	cout << "end read and add region glint" << endl; 
	
	idReg = 0;
	idAnchor = 0;
	std::sort(vecAnchor.begin(), vecAnchor.end(), compareAnchor);
	for (vector<Anchor*>::const_iterator it = vecAnchor.begin(); it!=vecAnchor.end(); ++it){
		(*it)->setId(idAnchor);
		(*it)->setNumVertexGraph(idAnchor);
		mapAnchor.insert(pair<int, Anchor*>((*it)->getId(), (*it)));
		vecRegion.push_back(new Region((*it)->getQueryStart(), (*it)->getQueryEnd(), (*it)->getQueryChrom(), (*it)->getQueryProt(), "-1", idReg, (*it)->getId(),(*it)->getQuerySwap()));
		vecRegion.at(vecRegion.size()-1)->setAnchor((*it));
		(*it)->setRegionQuery(vecRegion.at(vecRegion.size()-1));
		++idReg;
		
		vecRegion.push_back(new Region((*it)->getSubjectStart(), (*it)->getSubjectEnd(), (*it)->getSubjectChrom(), (*it)->getSubjectProt(), "-1", idReg, (*it)->getId(),(*it)->getSubjectSwap()));
		vecRegion.at(vecRegion.size()-1)->setAnchor((*it));
		(*it)->setRegionSubject(vecRegion.at(vecRegion.size()-1));
		++idReg;
		++idAnchor;
	}
	cout << "end add anchor in map" << endl; 
	
	std::sort(vecRegion.begin(), vecRegion.end(), compareRegion);
	for (vector<Region*>::const_iterator it = vecRegion.begin(); it!=vecRegion.end(); ++it){
		mapRegion.insert(pair<int, Region*>((*it)->getId(), (*it)));
	}
	cout << "end add region in map" << endl; 
}

void printTandemDupli(char* fileOut, vector<GraphUndirected*>& vecGraph, int typeUnit)	{ 
	
	ofstream chain_file(fileOut);
	
	char fileOut2[2000];
	sprintf(fileOut2, "%s%s", fileOut, ".singleton.out");
	ofstream singleton_file(fileOut2);
	singleton_file << "#chrom\tstart\tend\tu_start\tu_end\tnumDupli\tdupli\n";
	
	int cptBadLengthRegion = 0;
	int cptSingletonRegion = 0;
	chain_file << "r_chrom\tr_start\tr_end\tu_start\tu_end\tunits\n";
	
	for(vector<GraphUndirected*>::const_iterator it=vecGraph.begin(); it<vecGraph.end(); ++it){
		if ((*it)->getNumAnchor()>1){
			if (!(*it)->badConnectedComponents()) {
				(*it)->findMaxAnchor(typeUnit);
				(*it)->printRegionTandem(chain_file);
			} 
		}
	}
	
	chain_file << "\n";
	chain_file.close();
	singleton_file.close();
	
	cout << "number of singleton region : " << cptSingletonRegion << endl; 
	cout << "number of bad length region : " << cptBadLengthRegion << endl; 
}


int main(int argc, char **argv) {

	char* fileReg = 0;
	char* fileGlint = 0;
	char* fileTandem = 0;
	
	vector<Region*> vecRegion;
	vector<Anchor*> vecAnchor;
	map<int, Region*> mapRegion;
	map<int, Anchor*> mapAnchor;
	
	int maxDist = 150000;
	int typeUnit = 0;
	
	vector<Region> vecRegionRes;
	vector<int> vecMaxRegion;
	double dPoverlap = 0.05; 

	int opt;
	while ((opt = getopt(argc, argv, "R:G:O:D:U:L:")) != -1) {
		switch (opt) {
 		case 'R': 
			fileReg = optarg;
			break;
		case 'G': 
			fileGlint = optarg;
			break;
		case 'O': 
			fileTandem = optarg;
			break;
		case 'D': 
			maxDist = lexical_cast<int>(optarg);
			break;
		case 'L': 
			dPoverlap = lexical_cast<double>(optarg)/100;
			break;
		case 'U': 
			typeUnit = lexical_cast<int>(optarg);
			break;
		default: 
			cerr << "Usage: -R fileName -G fileName -O fileName -D int -U int -L int" << endl;
			cerr << "-R fileName : file in region of chain" << endl;
			cerr << "-G fileName : file in region of glint" << endl;
			cerr << "-O fileName : file output" << endl;
			cerr << "-D int : Max distance " << endl;
			cerr << "-L int : pourcentage of overlap between to chain in a same connected component (-L 50 for 50%)" << endl;
			cerr << "-U int : type d'unité sélectionée " << endl;
			cerr << "\t 0 : la chaine de score maximum de taille inférieur à la distance de la diagonale " << endl;
			cerr << "\t 1 : la chaine de score maximum " << endl;
			cerr << "\t 2 : l'unité la plus grande " << endl;
			cerr << "\t 3 : l'unité de score max et les autres unités ordonnées par score" << endl;
			cerr << "./build/src/postTraitement_exec -R ./donnee/test-arabido-chr2 -O ./donnee/res.out" << endl;
			exit(EXIT_FAILURE);
		}
	}
	
	cout << "end of read param" << endl;
	ifstream ifReg(fileReg, ios::in);
	ifstream ifGlint(fileGlint, ios::in);
	readFiles(ifReg, ifGlint, vecRegion, vecAnchor, mapRegion, mapAnchor, maxDist);
	ifReg.close();
	ifGlint.close();
	
	cout << "debut de la création du graphe. number Anchor : " << vecAnchor.size() << endl;
	GraphUndirected g(mapAnchor, mapRegion);
	g.createGraph(dPoverlap);
	cout << "fin de la création du graphe" << endl;
	
	cout << "debut de la création des composantes conexes" << endl;
	vector<GraphUndirected*> vecGraph;
	g.connectedComponents(vecGraph, dPoverlap);
	cout << "fin de la création des composantes connexes" << endl;
	
	std::sort(vecGraph.begin(), vecGraph.end(), compareGraphUndirected);
	printTandemDupli(fileTandem, vecGraph, typeUnit);
	
	std::for_each(vecRegion.begin(), vecRegion.end(), boost::checked_deleter<Region>()); 
	std::for_each(vecAnchor.begin(), vecAnchor.end(), boost::checked_deleter<Anchor>()); 
}


