#ifndef CHAIN_H_
#define CHAIN_H_

#include <limits.h>
#include <boost/limits.hpp>

#include "../data/Alignement.h"
#include "../util/UtilAlign.h"
#include "../const.h"

#include <vector>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <cmath>
#include <cstdlib>

#include <boost/lexical_cast.hpp>
#include <boost/numeric/conversion/cast.hpp>

using namespace data;
using namespace util;
using namespace std;
using namespace boost;

namespace graphe{
	
	class DirectedGraph;
	
	class Chain
	{
		
		int q_start;
		int q_end;
		int s_start;
		int s_end;
		string q_chrom;
		string s_chrom;
		double scoreAlign;
		double scoreChain;
		
		double coefCorrelation;
		
		vector<Alignement*> vecAlign;
		map<string,Alignement*> mapAlignQuery;
		map<string,Alignement*> mapAlignSubject;
		
		int numDeletion;
		int numSubstitution;
		int numOpenGap;
		int size;
		int orderSubject;
		int orderQuery;
		
		int idGraph;
		
		double evalue;
		
		bool dupTandem;
		bool chevauche;
		bool reverse;
		
		long scoreStruct;
		
		vector<Chain*> vecChainTeeth;

	public:
		Chain();
		Chain(int start, int end, string chrom);
		Chain(Alignement& align, int* type, bool* booParam, double* param, bool bReverse, int idG, vector<double>& vecCost);
		Chain(Alignement& align, double score);

		inline bool getPolarite() const	{
			if (q_start<q_end && s_start<s_end) return true;
			else return false;
		}
		inline bool getDupTandem() const				{	return dupTandem;								}
		inline bool getChevauche() const				{	return chevauche;								}
		inline void setChevauche(bool booChevauche) 	{	 chevauche = booChevauche;						}
		inline void updateChevauche(bool booChevauche) 	{	 chevauche = chevauche || booChevauche;			}
		inline int getNbAlign() const					{	return numeric_cast<int>(vecAlign.size());		}
		inline Alignement* getAlign(int i) const		{	return vecAlign.at(i);							}
		inline int getQueryMin() const					{	return (q_start < q_end)? q_start:q_end;		}
		inline int getQueryMax() const					{	return (q_start > q_end)? q_start:q_end;		}
		inline int getQueryStart() const				{	return q_start;									}
		inline int getQueryEnd() const					{	return q_end;									}
		inline int getQueryLength() const				{	return q_end-q_start+1;							}
		inline int getSubjectMin() const				{	return (s_start < s_end)? s_start:s_end;		}
		inline int getSubjectMax() const				{	return (s_start > s_end)? s_start:s_end;		}
		inline int getSubjectStart() const				{	return s_start;									}
		inline int getSubjectEnd() const				{	return s_end;									}
		inline int getSubjectLength() const				{	return s_end-s_start+1;							}
		inline string getQueryChrom() const				{	return q_chrom;									}
		inline string getSubjectChrom() const			{	return s_chrom;									}
		inline Chain* getChainTeeth(int i) const		{	if (i==0) return vecChainTeeth.at(0);	
															if (i==1) return vecChainTeeth.at(1);
															return NULL;									}
		inline int getNumChainTeeth() const				{	return vecChainTeeth.size();					}
		inline int getNumAlign() const					{	return vecAlign.size();							}
		inline int getAlignsScore() const				{	return -scoreAlign;								}
		inline int getChainScore() const				{	return -scoreChain;								}
		inline int getSize() const						{	return size;									}
		inline int getOrderQuery() const				{	return orderQuery;								}
		inline int getOrdersubject() const				{	return orderSubject;							}
		inline int getNumDeletion() const				{	return numDeletion;								}
		inline int getNumSubstitution() const			{	return numSubstitution;							}
		inline int isTandem() const						{	return dupTandem;								}
		inline int getLengthQuery() const				{	return getQueryMax() - getQueryMin() + 1;		}
		inline int getLengthSubject() const				{	return getSubjectMax() - getSubjectMin() + 1;	}
		
		inline double getPente() const					{	if ( (((double)abs(q_end-q_start)) / abs(s_end-s_start)) <= 1 )	
																return ((double)abs(q_end-q_start)) / abs(s_end-s_start);
															return ((double)abs(s_end-s_start) / abs(q_end-q_start));
														}
																
		inline double getScoreChain(double pOpenGap, double pDeletion, double pSubstitution) const	{
			return scoreChain - pOpenGap*numOpenGap - pDeletion*numDeletion - pSubstitution*numSubstitution ;	}
		
		inline double getScoreStruct() const	{	return ((double)scoreStruct)/(((double)getSize())*getSize());	}
		inline double getCoefCorrelation() const	{	return coefCorrelation;	}
		
		inline bool seqOverlap() const	{	
			if (q_chrom != s_chrom) return false;
			if (q_start < s_start)	return (q_end > s_start);	
			else					return (s_end > q_start); 
		}
		
		double pOverlapQuery (const Chain& C) const;
		double pOverlapSubject (const Chain& C) const;
		bool chainOverlap(Chain& ch) ;
	
		inline void setOrderQuery(int OderQ)	{	orderQuery = OderQ;		}
		inline void setOrderSubject(int OderS)	{	orderSubject = OderS;	}
		
		int getNumMatch() const;
		
		void computeCoefCorrelation();
				
		void addAlignForward(Alignement& align, int* type, bool* booParam, double* param, vector<double>& vecCost);
		int chainTeeth3(int* type, bool* booParam, double* param, bool bReverse, vector<double>& vecCost);
		bool chainValide(int type, int numMinAlign, int scoreMin, double coefCorr, bool booOverlapGlobal, int lengthMin=0) const;
		
		bool deleteEdege(vector<DirectedGraph*>& vecGraph);
		
		void printResNarcisse(ofstream& chain_file, int* type, bool* booParam) const;
		void printResRed(ofstream& chain_file, int numChain, int* type, bool* booParam, double* param) const;
		void printRes(ofstream& chain_file, int numChain, int* type, bool* booParam, double* param) const;
		void afficheChain(int numChain, int* type, bool* booParam, double* param) const;
		void afficheChain() const;
		virtual ~Chain();
	};
}

#endif /*CHAIN_H_*/
