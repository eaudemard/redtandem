#ifndef ABSTRACTGRAPH_H_
#define ABSTRACTGRAPH_H_

#include <limits.h>
#include <boost/limits.hpp>

#include "../data/Alignement.h"
#include "../const.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <map>

#include <boost/lexical_cast.hpp>
#include <boost/numeric/conversion/cast.hpp>

using namespace data;
using namespace std;
using namespace boost;

namespace graphe{
	
	class AbstractGraph {
		
		
	public:
	
		//---------------- atribut -------------------
				
		map<int, Alignement*> mapAlign;
		
		//---------------- fonction -------------------	
	
	protected :
		AbstractGraph();
		AbstractGraph(map<int, Alignement*>& mAlign);
			
	public :
		
		void rebuildChain(int typeCostAlign, int typeDist);
		
		inline Alignement* getAlign(int key){
			map<int,Alignement*>::iterator it;
			it = mapAlign.find(key);
			if (it != mapAlign.end()) return ((*it).second);
			return NULL;
		}
		
		inline void addAlign(Alignement& A, int idGraph) {
			A.setIdInGraph(numeric_cast<int>(mapAlign.size()));
			A.setIdGraph(idGraph);
			mapAlign.insert(pair<int, Alignement*>(numeric_cast<int>(mapAlign.size()),&A));
			
		}
		
		void eraseChain();
		
		inline int getNumAlign() const		{	return numeric_cast<int>(mapAlign.size());		}
		inline int getNumVertex() const		{	return numeric_cast<int>(mapAlign.size())*2+2;	}
		
		void afficheAlign() const;
		
		double checkOrderAlign (const Alignement& Ai, const Alignement& Aj, int* type, bool* booParam, double* param) const;
		
		void printOutAlign() ;
		
		virtual ~AbstractGraph(){}
	};
	
}

#endif /*ABSTRACTGRAPH_H_*/
