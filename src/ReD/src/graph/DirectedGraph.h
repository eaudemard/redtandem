#ifndef DIRECTEDGRAPH_H_
#define DIRECTEDGRAPH_H_



#include "./AbstractGraph.h"
#include "./Chain.h"

//=============================================================
//lib boost
#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/bellman_ford_shortest_paths.hpp>
#include <boost/graph/dag_shortest_paths.hpp>
//#include <boost/numeric_cast.hpp>
#include <boost/numeric/conversion/cast.hpp>
//=============================================================

using namespace boost;
using namespace data;
using namespace std;

namespace graphe{

	class DirectedGraph : public AbstractGraph {

		//=============================================================
		//définition des variables nécessaire à la création d'un graphe
		struct EdgeWeight {
			long int weight;
		};
		struct EdgeDist {
			long int dist;
		};
		typedef adjacency_list < vecS, vecS, directedS, EdgeDist, EdgeWeight> Graph;
		Graph g;
		vector<std::size_t> parent;
		//=============================================================
	
		int idGraph;
		int costSource;
		int costSink;
				
		int numEdge;
		
		int idSource;
		int idSink;
		
		
		double numAlignUse;
		
		vector<Chain*> vecChain;
		
		bool setChainValide;
				
	public:
		DirectedGraph();
		DirectedGraph(map<int, Alignement*>& mAlign, int idG, int cSource, int cSink, int* type, bool* booParam, double* param, vector<double>& vecCost);
		DirectedGraph(bool sourceSink, map<int, Alignement*>& mAlign, int idG, int cSource, int cSink, int* type, bool* booParam, double* param, vector<double>& vecCost);
		DirectedGraph(int idG, int cSource, int cSink, int nbAlign);
		
		void initIdSourceSink();
		void initIdGraphe();
		
		vector<unsigned long long int> createGraph (bool sourceSink, int* type, bool* booParam, double* param, vector<double>& vecCost);
		
		void bellmanFord();
		void dagShortestPath();
		double reverseEdge(int start, int end);
		void updateScore(vector<double>& vecCost);
		void deleteEdge(int start, int end);
		double deleteVertex(int start, int end);
		double findBestChainAndUpdateGraph(bool updateGraph, bool glouton);
		
		void emptyAndDeleteVecChain();
		void emptyVecChain();
		int rebuildChain(int* type, bool* booParam, double* param, bool modeReverse, vector<double>& vecCost);
				
		inline int getNbChain() const				{	return numeric_cast<int>(vecChain.size());		}
		inline Chain* getChain(int index)	const	{	return vecChain.at(index);						}
		
		int getMinSizeChain() const;
		int getMaxSizeChain() const;
		double getMeanSizeChain() const;
		double getSumSizeChain() const; 
		
		double getMinScoreChain() const;
		double getMaxScoreChain() const;
		double getMeanScoreChain() const;
		
		double getMinPente() const;
		double getMaxPente() const;
		double getMeanPente() const;
		double getSumPente() const;
		
		int getMaxNumAlignForOneChain(int* type, bool* booParam, double* param) const;
		int getSumNumAlignForOneChain(int* type, bool* booParam, double* param) const;
		int getnumChainValide(int* type, bool* booParam, double* param) const;
		
		inline double getNumAlignUse() const 	{		return numAlignUse;		}
		inline bool getSetChainValide() const	{		return setChainValide;	}
		inline int getIdSource() const			{		return idSource;		}
		inline int getIdSink() const			{		return idSink;			}
		
		void testSingleton(ofstream& fileOut, double pIdent, double lengthMin, double distMaxbetweenAnchor) const;
		
		void afficheParent();
		void afficheScoreChain() const;
		
		void printChains(char* fileOut, int* type, bool* booParam, double* param) const;
		void printChains(ofstream& chain_file, int* type, bool* booParam, double* param) const;
		void afficheChains(int* type, bool* booParam, double* param) const;
		void afficheRes(const char* fileOut);
		void afficheRes();
		
		virtual ~DirectedGraph();
		
	};
}

#endif /*DIRECTEDGRAPH_H_*/
