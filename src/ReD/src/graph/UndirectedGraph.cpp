#include "UndirectedGraph.h"

namespace graphe{
	UndirectedGraph::UndirectedGraph() :  AbstractGraph() {
		
	}
	
	UndirectedGraph::UndirectedGraph(map<int, Alignement*>& mAlign) :  AbstractGraph(mAlign), g(mAlign.size()*2) {
	
	}
	
	vector<unsigned long long int> UndirectedGraph::createGraph (int* type, bool* booParam, double* param){
		graph_traits<Graph>::vertex_descriptor u, v;
		
		unsigned long long int costAlign = 0;
		unsigned long long int costTrans = 0;
		unsigned long long int numEdgeDist = 0;
		unsigned long long int numEdgeOverlap = 0;
		unsigned long long int numEdgePOverlap = 0;
		unsigned long long int numEdgeCost = 0;
		unsigned long long int numEdge = 0;
		unsigned long long int numEdgeTrans = 0;
		unsigned long long int numEdgeSlope = 0;
		
		unsigned long long int numEdgeSameName = 0;
		unsigned long long int numEdgeTandem = 0;
		unsigned long long int numEdgeOverMax = 0;
		
		
				
		for (map<int, Alignement*>::iterator itAlign = mapAlign.begin(); itAlign != mapAlign.end(); ++itAlign){
			u = vertex((*itAlign).second->getIdInGraph()*2, g);
			v = vertex((*itAlign).second->getIdInGraph()*2+1, g);
			add_edge(u, v, g);
			++numEdge;
			
			costAlign = costAlign + fabs((*itAlign).second->costAlign(type[1], param[1], booParam[10]));
						
			double weight = -1;
			list< vector<double> > listEdge;
			list< vector<double> >::iterator itList;
			
			map<int, Alignement*>::iterator itAlignNext = itAlign;
			while ( itAlignNext != mapAlign.end() && (*itAlign).second->getQueryChrom() == (*itAlignNext).second->getQueryChrom() 
					&& (*itAlign).second->getSubjectChrom() == (*itAlignNext).second->getSubjectChrom()) {
			
				if ((*itAlignNext).second->getId()!=(*itAlign).second->getId()){
					
					weight = checkOrderAlign(*(*itAlign).second, *(*itAlignNext).second, type, booParam, param);
					
					if (weight >= 0) {
						
						itList = listEdge.begin();
						while(itList!= listEdge.end() && itList->at(0)< weight){
							++itList;	
						}
						
						if (itList==listEdge.end()){
							listEdge.push_back(vector<double>(3,weight));
							itList=listEdge.end();
						} else {
							listEdge.insert(itList,vector<double>(3,weight));
							
						}
						--itList;
						itList->at(1) = (*itAlign).second->getIdInGraph()*2+1;
						itList->at(2) = (*itAlignNext).second->getIdInGraph()*2;
						
					}
					
					if (weight == -7) {
						++numEdgeDist;
						break;
					} 
					
					if (weight == -8) ++numEdgeOverlap;
					if (weight == -9) ++numEdgeCost;
					if (weight == -10) ++numEdgeSameName;
					if (weight == -11) ++numEdgeTandem;
					if (weight == -12) ++numEdgePOverlap;
					if (weight == -13) ++numEdgeSlope;
					
				}
				++itAlignNext;
			}
			
			double nbEdge = 0;
			if ( booParam[BPARAM_MAX_EDGE] )	nbEdge = param[PARAM_MAX_EDGE];
			else								nbEdge = listEdge.size();
			
			itList = listEdge.begin();
			for (int i=0; i<nbEdge; ++i){
				if (itList == listEdge.end()) break;
				
				u = vertex( itList->at(1), g );
				v = vertex( itList->at(2), g );
				
				add_edge(u, v, g);
				costTrans = costTrans + itList->at(0);
				
				++numEdgeTrans;
				++numEdge;
				++itList;
			}
			
			numEdgeOverMax+= (listEdge.size()-nbEdge);
			
		}
		
		cout << "nombre d'arcs : " << numEdge << endl;
		cout << "nombre d'arcs supprimés qui dépassent maxDist : " << numEdgeDist << endl;
		cout << "nombre d'arcs supprimés qui créaient des sous chaînes qui se chevauchent : " << numEdgeOverlap << endl;
		cout << "nombre d'arcs supprimés qui créaient des sous chaînes qui se chevauchent presque : " << numEdgePOverlap << endl;
		cout << "nombre d'arcs supprimés qui créaient des sous chaîne cout superieur au cout Max : " << numEdgeCost << endl;
		cout << "nombre d'arcs supprimés qui ajoutaient trop arc : " << numEdgeOverMax << endl;
		
		vector<unsigned long long int> vec;
		vec.push_back(costAlign);
		vec.push_back(costTrans);
		vec.push_back(numEdgeTrans);
		
		return vec;
	}
	
	void UndirectedGraph::connectedComponents(vector<DirectedGraph*>& vecDirGraph, int cSource, int cSink, int* type, bool* booParam, double* param, vector<double>& vecCost, ofstream* fileSingletonOut){
		vector<int> component(num_vertices(g));
		int num = connected_components(g, &component[0]);
		vecDirGraph.erase(vecDirGraph.begin(),vecDirGraph.end());

		map<int,Alignement*>::iterator it;
		int i;
		vector<int> numSommet(num,0);
		for (i = 0; i != lexical_cast<int>(component.size()); ++i){
			++numSommet.at(component[i]);
		}
		cout << endl;
		int numComp = 0;
		for (int cpt = 0; cpt != num; ++cpt){
			if (numSommet.at(cpt)!=1) ++numComp;
		}
		cout << "\tTotal number of components: " << numComp << endl; 
	    
		for (int cpt = 0; cpt < num; ++cpt){
			vecDirGraph.push_back(new DirectedGraph(cpt, cSource, cSink, numSommet.at(cpt)));
		}
		vecDirGraph.resize(vecDirGraph.size());
		
		int cptAlignAdd = 0;
		for (i = 0; i < numeric_cast<int>(component.size()); i+=2){
			it = mapAlign.find(cptAlignAdd);
			(vecDirGraph.at(component[i]))->addAlign(*(*it).second, component[i]);
			++cptAlignAdd;
		}
		
		vector<DirectedGraph*>::iterator itG = vecDirGraph.begin();
		int cptDelete = 0;
		while(itG!=vecDirGraph.end()){
			if ((*itG)->getNumAlign()>1){
				++itG;
			} else {
				if (type[TYPE_EXECUTION]==6 && fileSingletonOut!=NULL){
					(*itG)->testSingleton((*fileSingletonOut), param[PARAM_PIDENTITY_SINGLETON], param[PARAM_MIN_LENGTH_SINGLETON], param[PARAM_DIST_MAX_BETWEEN_ANCHOR]);
				}
	    		
				delete((*itG));
				vecDirGraph.erase(itG);
				itG = vecDirGraph.begin();
				++cptDelete;
			}
		}
		cout << "\tTotal number of components keep : " << vecDirGraph.size() << endl; 
	    
		for (int cpt = 0; cpt < numeric_cast<int>(vecDirGraph.size()); ++cpt){
			vecDirGraph.at(cpt)->createGraph (true, type, booParam, param, vecCost);
		}
 
		g.clear();
	}
	
	void UndirectedGraph::afficheRes(char* fileOut){
	
		graph_traits<Graph>::edge_iterator ei, ei_end;
		int taille = 15;
		
		ofstream dot_file(fileOut);
		dot_file << "digraph D {\n"
    		<< "  rankdir=LR\n"
    		<< "  size=\"" << taille << "," << taille << "\"\n"
    		<< "  ratio=\"fill\"\n"
    		<< "  edge[style=\"bold\"]\n" << "  node[shape=\"circle\"]\n";

  		{
    		for (tie(ei, ei_end) = edges(g); ei != ei_end; ++ei) {
      			graph_traits < Graph >::edge_descriptor e = *ei;
      			graph_traits < Graph >::vertex_descriptor u = source(e, g);
      			graph_traits < Graph >::vertex_descriptor v = target(e, g);
      			
      			dot_file << u << " -> " << v << "[label=\"0\", color=\"black\"]";
      			
    		}
  		}
  		dot_file << "}";
	}
	
	
	
	
	UndirectedGraph::~UndirectedGraph()
	{
	}
}
