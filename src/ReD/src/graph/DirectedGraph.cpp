#include "DirectedGraph.h"

namespace graphe{

	DirectedGraph::DirectedGraph() :  AbstractGraph(), idGraph(-1), costSource(-1), costSink(-1) {
		
		numEdge = -1;
		numAlignUse = 0;
		setChainValide = true;
	}
	
	DirectedGraph::DirectedGraph(map<int, Alignement*>& mAlign, int idG, int cSource, int cSink, int* type, bool* booParam, double* param, vector<double>& vecCost) : AbstractGraph(mAlign), g(mAlign.size()*2+2), idGraph(idG), costSource(cSource), costSink(cSink) {
						
		eraseChain();
		createGraph (true, type, booParam, param, vecCost);
		numAlignUse = 0;
		setChainValide = true;
	}

	DirectedGraph::DirectedGraph(bool sourceSink, map<int, Alignement*>& mAlign, int idG, int cSource, int cSink, int* type, bool* booParam, double* param, vector<double>& vecCost) : AbstractGraph(mAlign), g(mAlign.size()*2+2), idGraph(idG), costSource(cSource), costSink(cSink) {
		
		createGraph (sourceSink, type, booParam, param, vecCost);
		numAlignUse = 0;
		setChainValide = true;
	}
	
	DirectedGraph::DirectedGraph(int idG, int cSource, int cSink, int nbAlign) : AbstractGraph(), g(nbAlign*2+2), parent(nbAlign*2+2), idGraph(idG), costSource(cSource), costSink(cSink) {
		numAlignUse = 0;
		setChainValide = true;
	}
	
	void DirectedGraph::initIdSourceSink(){
		
		idSource = numeric_cast<int>(mapAlign.size())*2;
		idSink = numeric_cast<int>(mapAlign.size())*2 + 1;
		
		parent = vector<std::size_t>(getNumVertex());
	}
	
	void DirectedGraph::initIdGraphe(){
		int idGraph = 0;
		for (map<int, Alignement*>::iterator itAlign = mapAlign.begin(); itAlign!=mapAlign.end(); ++itAlign){
			(*itAlign).second->setIdInGraph(idGraph);
			++idGraph;
		}
	}
	
	vector<unsigned long long int> DirectedGraph::createGraph (bool booSourceSink, int* type, bool* booParam, double* param, vector<double>& vecCost){
		graph_traits<Graph>::vertex_descriptor u, v;
		
		initIdGraphe();		
		initIdSourceSink();
		
		unsigned long long int costAlign = 0;
		unsigned long long int costTrans = 0;
		unsigned long long int numEdgeNorm = 0;
		unsigned long long int numVertexNorm = 0;
		int cptSommet = 2;
		int cptArc = 0;
		int numArc = 0;
		EdgeWeight ew;
		double weight;
		
		for (map<int, Alignement*>::iterator itAlign = mapAlign.begin(); itAlign != mapAlign.end(); ++itAlign){
			
			u = vertex((*itAlign).second->getIdInGraph()*2, g);
			v = vertex((*itAlign).second->getIdInGraph()*2+1, g);
			if (vecCost.size() == 0){
				weight = (*itAlign).second->costAlign(type[TYPE_SCORE_ALIGN], param[PARAM_SCORE_MAX_ALIGN], booParam[BPARAM_OVERLAP_BONUS]);
			} else {
				weight = ((*itAlign).second->costAlign(type[TYPE_SCORE_ALIGN], param[PARAM_SCORE_MAX_ALIGN], booParam[BPARAM_OVERLAP_BONUS])*DOUBLEINT)/vecCost.at(0);
			}
			
			costAlign = costAlign + fabs(weight);
			
			ew.weight = numeric_cast<long int>(floor(weight));
			add_edge(u, v, ew, g);
			
			numArc = numArc + 1;
			++numVertexNorm;
			
			if (booSourceSink) {
				u = vertex(idSource, g);
				v = vertex((*itAlign).second->getIdInGraph()*2, g);
				ew.weight = costSource;
				add_edge(u, v, ew, g);
				
				u = vertex((*itAlign).second->getIdInGraph()*2+1, g);
				v = vertex(idSink, g);
				ew.weight = costSink;
				add_edge(u, v, ew, g);
				
				numArc = numArc + 2;
			}
			
			cptSommet = cptSommet + 2;
			cptArc = cptArc + 3;
			
			list< vector<double> > listEdge;
			list< vector<double> >::iterator itList;
			
			map<int, Alignement*>::iterator itAlignNext = itAlign;
			while ( itAlignNext != mapAlign.end() && (*itAlign).second->getQueryChrom() == (*itAlignNext).second->getQueryChrom() 
					&& (*itAlign).second->getSubjectChrom() == (*itAlignNext).second->getSubjectChrom()) {
			
				if ((*itAlignNext).second->getId()!=(*itAlign).second->getId()){
					
					weight = checkOrderAlign(*(*itAlign).second, *(*itAlignNext).second, type, booParam, param);
					
					if (weight >= 0) {
						if (vecCost.size() != 0){
							weight = (weight * DOUBLEINT)/vecCost.at(1);
						}
						
						itList = listEdge.begin();
						while(itList!= listEdge.end() && itList->at(0)< weight){
							++itList;	
						}
						
						if (itList==listEdge.end()){
							listEdge.push_back(vector<double>(3,weight));
							itList=listEdge.end();
						} else {
							listEdge.insert(itList,vector<double>(3,weight));
							
						}
						--itList;
						itList->at(1) = (*itAlign).second->getIdInGraph()*2+1;
						itList->at(2) = (*itAlignNext).second->getIdInGraph()*2;
					
					} else {
						if (weight == -7) {
							break;
						} 
					}
										
				}
				++itAlignNext;
			}
			
			double nbEdge = 0;
			if ( booParam[BPARAM_MAX_EDGE] )	nbEdge = param[PARAM_MAX_EDGE];
			else								nbEdge = listEdge.size();
			
			itList = listEdge.begin();
			for (int i=0; i<nbEdge; ++i){
				if (itList==listEdge.end()) break;
				
				u = vertex( itList->at(1), g );
				v = vertex( itList->at(2), g );
				
				costTrans = costTrans + fabs(itList->at(0));
				
				ew.weight = numeric_cast<long int>(floor(itList->at(0)));
				add_edge(u, v, ew, g);
								
				++numEdgeNorm;	
							
				++numArc;
				++cptArc;
				++itList;
			}
		}
		numEdge = numArc;
		
		vector<unsigned long long int> vec;
		vec.push_back(costAlign);
		vec.push_back(costTrans);
		vec.push_back(numEdgeNorm);
		vec.push_back(numVertexNorm);
		
		return vec;
	}
	
	void DirectedGraph::bellmanFord(){
		
		vector<long int> distance(getNumVertex(), (std::numeric_limits < long int >::max)());
		for (int i = 0; i<getNumVertex(); ++i)
    		parent[i] = lexical_cast<std::size_t>(i);
  		distance[idSource] = 0;
  		
  		bool r = bellman_ford_shortest_paths(g, lexical_cast<int>(getNumVertex()), get(&EdgeWeight::weight, g), &parent[0], &distance[0], closed_plus<long int>(), std::less<long int>(), default_bellman_visitor());
  		  		
  		if (!r){
  			cout << "negative cycle " << endl;
  			graph_traits<Graph>::edge_iterator ei, ei_end;
  			graph_traits<Graph>::vertex_descriptor viSource, viTarget;
  			property_map<Graph, long int EdgeWeight::*>::type weight_pmap = get(&EdgeWeight::weight, g);
  			for (tie(ei, ei_end) = edges(g); ei != ei_end; ++ei){
  				viSource = source(*ei, g);
  				viTarget = target(*ei, g);
    			if (weight_pmap[*ei]+distance[viSource] < distance[viTarget]){
    				cout << "source : " << viSource << " - " <<  distance[viSource] << " target : " << viTarget << " - " << distance[viTarget] << endl;
    			}
  			}
  			afficheRes();
  		}
	}
	
	void DirectedGraph::dagShortestPath(){
		std::vector<default_color_type> color(num_vertices(g));
		default_dijkstra_visitor vis;
		std::less<long int> compare;
		closed_plus<long int> combine;
		
		dag_shortest_paths(g, int(idSource), get(&EdgeDist::dist, g), get(&EdgeWeight::weight, g), &color[0], &parent[0], vis, compare, combine, (std::numeric_limits<long int>::max)(), 0);
	}
	
	double DirectedGraph::reverseEdge(int start, int end){
		
		graph_traits<Graph>::vertex_descriptor u, v;
		graph_traits<Graph>::edge_descriptor ei;
		bool findEdge;
		int weight = 0;
		EdgeWeight ew;
		
		u = vertex(start, g);
		v = vertex(end, g);
		
		tie(ei, findEdge) = edge(u, v, g);
		if (findEdge){
			weight = get(&EdgeWeight::weight, g, ei);
			remove_edge(ei, g);
			ew.weight = -weight;
			add_edge(v, u, ew, g);
		}
		else{
			cerr << "DirectedGraph::reverseEdge : pb Edge not found " << endl;
			cout << "idGraph : " << idGraph << endl;
			exit(EXIT_FAILURE);
		}
		
		return weight;
	}
	
	void DirectedGraph::updateScore(vector<double>& vecCost){
		
		graph_traits<Graph>::edge_iterator ei, eiend;
		
		property_map<Graph, long int EdgeWeight::*>::type weight_pmap = get(&EdgeWeight::weight, g);
				
		for (tie(ei, eiend) = boost::edges(g); ei != eiend; ++ei) { 
			if ( source(*ei,g) % 2 == 0) {
				weight_pmap[*ei] = (weight_pmap[*ei]*DOUBLEINT) / vecCost.at(0);
			} else {
				weight_pmap[*ei] = (weight_pmap[*ei]*DOUBLEINT) / vecCost.at(1);
			}
		}

	}
	
	void DirectedGraph::deleteEdge(int start, int end){
		graph_traits<Graph>::vertex_descriptor u, v;
		graph_traits<Graph>::edge_descriptor ei;
		bool findEdge;
		
		u = vertex(start, g);
		v = vertex(end, g);
		
		tie(ei, findEdge) = edge(u, v, g);
		if (findEdge) {
			remove_edge(ei, g);
		}
	}
	
	double DirectedGraph::deleteVertex(int start, int end){
		graph_traits<Graph>::vertex_descriptor u, v;
		graph_traits<Graph>::edge_descriptor ei;
		bool findEdge;
		int weight = 0;
		
		u = vertex(start, g);
		v = vertex(end, g);
		
		tie(ei, findEdge) = edge(u, v, g);
		if (findEdge){
			weight = get(&EdgeWeight::weight, g, ei);
		}
		
		clear_vertex(v, g);
		
		return weight;
	}
	
	double DirectedGraph::findBestChainAndUpdateGraph(bool updateGraph, bool glouton){
		int presentI = idSink;
		
		if (presentI==lexical_cast<int>(parent[presentI])) return std::numeric_limits<double>::max();
		
		map<int,Alignement*>::iterator itAlign;
		bool deleteSection = false;
		int lastI = -1;
		int weight = 0;
		
		if (presentI == lexical_cast<int>(parent[presentI]) ){
			return std::numeric_limits<double>::min(); 
		}
		
		while (presentI!=idSource){
			if (lexical_cast<int>(parent[presentI])!=idSource){
				itAlign = mapAlign.find(parent[presentI]/2);
				
				lastI = presentI;
				presentI = parent[presentI];
			}
			else{
				lastI = presentI;
				presentI = idSource;
			}
						
			if (lastI==idSink){ 
			
				if ((*itAlign).second->alreadyInChain()){
					deleteSection = true;
					numAlignUse = numAlignUse + 
						(*itAlign).second->updateChain((*itAlign).second->getAlignBefore(),-1,(*itAlign).second->isStartChain(),true);
					if (updateGraph){
						if (!glouton){
							weight = weight + reverseEdge(presentI,lastI);
						}
					}
					
					if ( (parent[presentI]-1) == parent[parent[presentI]] && parent[parent[presentI]]%2==0){
						deleteSection = false;
					}
					
				}
				else{
					numAlignUse = numAlignUse + 
						(*itAlign).second->updateChain(parent[parent[presentI]]/2,-1,(*itAlign).second->isStartChain(),true);
					if (updateGraph){
						if (glouton){
							lastI = presentI;
							presentI = parent[presentI];
							weight = weight + deleteVertex(presentI,lastI);
						}
						else{
							weight = weight + reverseEdge(presentI,lastI);
							lastI = presentI;
							presentI = parent[presentI];
							weight = weight + reverseEdge(presentI,lastI);
						}
					}
				}
			}
			else if (presentI==idSource){ 
				itAlign = mapAlign.find(lastI/2);
				numAlignUse = numAlignUse + 
						(*itAlign).second->updateChain(-1,(*itAlign).second->getAlignAfter(),true,(*itAlign).second->isEndChain());
				if (updateGraph){
					if (glouton){
						weight = weight + deleteVertex(presentI,lastI);
					}
					else{
						weight = weight + reverseEdge(presentI,lastI);
					}
				}
				
			}
			else {
				if ((*itAlign).second->alreadyInChain()){	//Alignement est déjà chainé 
					if (!deleteSection) {	//Alignement précédant n'était pas chainé
						deleteSection = true;
						numAlignUse = numAlignUse + 
							(*itAlign).second->updateChain((*itAlign).second->getAlignBefore(),lastI/2,(*itAlign).second->isStartChain(),(*itAlign).second->isEndChain());
						
						if (updateGraph){
							if (glouton){
								weight = weight + deleteVertex(presentI,lastI);
							}
							else{
								weight = weight + reverseEdge(presentI,lastI);
							}
						}
					}
					else{
						if ((presentI%2==0 && lexical_cast<int>(parent[presentI])== presentI+1) || (presentI%2==1 && lexical_cast<int>(parent[presentI])+1== presentI)){
							numAlignUse = numAlignUse + 
								(*itAlign).second->updateChain(-1,-1,(*itAlign).second->isStartChain(),(*itAlign).second->isEndChain());
							if (updateGraph){
								if (glouton){
									weight = weight + deleteVertex(presentI,lastI);
									lastI = presentI;
									presentI = parent[presentI];
									weight = weight + deleteVertex(presentI,lastI);
								}
								else{
									weight = weight + reverseEdge(presentI,lastI);
									lastI = presentI;
									presentI = parent[presentI];
									weight = weight + reverseEdge(presentI,lastI);
								}
							}
						}
						else{ //c'est le dernier à être déjà chainé
							if (presentI%2==1 && lastI%2==0){ //Alignement déjà utilisé mais qui emprunte un arc de trnasition non utilisé
								numAlignUse = numAlignUse + 
									(*itAlign).second->updateChain((*itAlign).second->getAlignBefore(),lastI/2,(*itAlign).second->isStartChain(),(*itAlign).second->isEndChain());
							}
							else{
								numAlignUse = numAlignUse + 
									(*itAlign).second->updateChain(parent[presentI]/2,(*itAlign).second->getAlignAfter(),(*itAlign).second->isStartChain(),(*itAlign).second->isEndChain());
							}
							
							if ( (parent[presentI]-1) == parent[parent[presentI]] && parent[parent[presentI]]%2==0){ //Si le suivant n'est pas un alignement déjà chainé
								deleteSection = false;
							}
							if (updateGraph){
								if (glouton){
									weight = weight + deleteVertex(presentI,lastI);
								}
								else{
									weight = weight + reverseEdge(presentI,lastI);
								}
							}
						}
					}
				}
				else {	//Alignement n'est pas encore chainé
					numAlignUse = numAlignUse + 
						(*itAlign).second->updateChain(parent[parent[presentI]]/2,lastI/2,(*itAlign).second->isStartChain(),(*itAlign).second->isEndChain());
					if (updateGraph){
						if (glouton){
							weight = weight + deleteVertex(presentI,lastI);
							lastI = presentI;
							presentI = parent[presentI];
							weight = weight + deleteVertex(presentI,lastI);
						}
						else{
							weight = weight + reverseEdge(presentI,lastI);
							lastI = presentI;
							presentI = parent[presentI];
							weight = weight + reverseEdge(presentI,lastI);
						}
					}
					
				}
			}
		}
		
		return lexical_cast<double>(weight/DOUBLEINT);	
	}
	
	void DirectedGraph::emptyAndDeleteVecChain() {
		for(vector<Chain*>::const_iterator it = vecChain.begin(); it<vecChain.end(); ++it){
			delete *it;
		}
		vecChain.erase(vecChain.begin(), vecChain.end());
	}
	
	void DirectedGraph::emptyVecChain() {
		vecChain.erase(vecChain.begin(), vecChain.end());
	}
	
	int DirectedGraph::rebuildChain(int* type, bool* booParam, double* param, bool modeReverse, vector<double>& vecCost){
		emptyAndDeleteVecChain();
		setChainValide = true;
		
		map<int,Alignement*>::iterator it2;
		int nbAlign = 0;
		Chain* chaine;
		bool prec = setChainValide;
		for(map<int,Alignement*>::iterator it = mapAlign.begin(); it != mapAlign.end(); ++it){
			
			if ((*it).second->getAlignBefore() != -1 || (*it).second->getAlignAfter() != -1 || (*it).second->isStartChain()){
				++nbAlign;
			}
			
			if ((*it).second->isStartChain()){
				chaine = new Chain(*(*it).second, type, booParam, param, modeReverse, idGraph, vecCost);
				
				if (!(*it).second->isEndChain ()){
					it2 = mapAlign.find((*it).second->getAlignAfter());
					while(!(*it2).second->isEndChain()){
						chaine->addAlignForward(*(*it2).second, type, booParam, param, vecCost);
						it2 = mapAlign.find((*it2).second->getAlignAfter());
					}
					
					chaine->addAlignForward(*(*it2).second, type, booParam, param, vecCost);
				}
				chaine->computeCoefCorrelation();
				
				if (chaine->getNbAlign()>= param[PARAM_NUMBER_MIN_ALIGN] && chaine->getChainScore() >= param[PARAM_SCORE_MIN_CHAIN] && abs(chaine->getCoefCorrelation()) >= param[PARAM_COEF_CORRELATION] && setChainValide) {
					if (chaine->getNbAlign()>1) {
						prec = setChainValide;
						setChainValide = setChainValide && chaine->chainValide(type[TYPE_FILTER], numeric_limits<int>::min(), numeric_limits<int>::min(), numeric_limits<double>::min(), booParam[BPARAM_GLOBAL_VALIDATION_OVERLAP]);
					}
					
					if (setChainValide && booParam[BPARAM_GLOBAL_VALIDATION_OVERLAP]){
						vector<Chain*>::iterator itChain = vecChain.begin();
						while ( itChain != vecChain.end()){
							if ((*itChain)->getNbAlign()>= param[PARAM_NUMBER_MIN_ALIGN] && (*itChain)->getChainScore() >= param[PARAM_SCORE_MIN_CHAIN] && abs(chaine->getCoefCorrelation()) >= param[PARAM_COEF_CORRELATION]) {
								if (chaine->getNbAlign()<=1 || (*itChain)->getNbAlign()<=1){
									chaine->chainOverlap(*(*itChain));
								} else {
									prec = setChainValide;
									setChainValide = setChainValide && !chaine->chainOverlap(*(*itChain));
								}
							}
							++itChain;
						}
					}
					
				}
				
				vecChain.push_back(chaine);
			}
		}
		return nbAlign;
	}
		
	int DirectedGraph::getMinSizeChain() const {
		int minSize = -1;
		if (!vecChain.empty()){
			vector<Chain*>::const_iterator it = vecChain.begin();
			minSize = (*it)->getSize();
			for(it = vecChain.begin(); it<vecChain.end(); it++){
				if(minSize > (*it)->getSize())	minSize = (*it)->getSize();
			}
		}
		return minSize;
	}
	
	int DirectedGraph::getMaxSizeChain() const {
		int maxSize = -1;
		if (!vecChain.empty()){
			vector<Chain*>::const_iterator it = vecChain.begin();
			maxSize = (*it)->getSize();
			for(it = vecChain.begin(); it<vecChain.end(); it++){
				if(maxSize < (*it)->getSize())	maxSize = (*it)->getSize();
			}
		}
		return maxSize;
	}
	
	double DirectedGraph::getMeanSizeChain() const {
		double size = 0;
		if(vecChain.size()>0){
			for(vector<Chain*>::const_iterator it = vecChain.begin(); it<vecChain.end(); it++){
				size = size + (*it)->getSize();
			}
			
			return size/lexical_cast<double>(vecChain.size());
		}
		return 0;
	}
	
	double DirectedGraph::getSumSizeChain() const {
		double size = 0;
		if(vecChain.size()>0){
			for(vector<Chain*>::const_iterator it = vecChain.begin(); it<vecChain.end(); ++it){
				size = size + (*it)->getSize();
			}
			
			return size;
		}
		return 0;
	}
	
	double DirectedGraph::getMinScoreChain() const {
		double minScore = std::numeric_limits<double>::max();
		if (!vecChain.empty()){
			vector<Chain*>::const_iterator it = vecChain.begin();
			minScore = (*it)->getChainScore();
			for(it = vecChain.begin(); it<vecChain.end(); ++it){
				if(minScore>(*it)->getChainScore())	minScore = (*it)->getChainScore();
			}
		}
		
		return minScore;
	}
	
	double DirectedGraph::getMaxScoreChain() const {
		double maxScore = std::numeric_limits<double>::min();
		if (!vecChain.empty()){
			vector<Chain*>::const_iterator it = vecChain.begin();
			maxScore = (*it)->getChainScore();
			for(it = vecChain.begin(); it<vecChain.end(); ++it){
				if(maxScore<(*it)->getChainScore())	maxScore = (*it)->getChainScore();
			}
		}
		
		return maxScore;
	}
	
	double DirectedGraph::getMeanScoreChain() const {
		double score = 0;
		if (!vecChain.empty()){
			vector<Chain*>::const_iterator it = vecChain.begin();
			for(it = vecChain.begin(); it<vecChain.end(); ++it){
				score = score + (*it)->getChainScore();
			}
			
			return score/lexical_cast<double>(vecChain.size());
		}
		return -1;
	}
	
	double DirectedGraph::getMinPente() const {
		double pente = std::numeric_limits<double>::max();
		if (!vecChain.empty()){
			vector<Chain*>::const_iterator it = vecChain.begin();
			pente = (*it)->getPente();
			for(it = vecChain.begin(); it<vecChain.end(); ++it){
				if((*it)->getPente() < pente) pente = (*it)->getPente();
			}
			
			return pente;
		}
		return pente;
	}
	
	double DirectedGraph::getMaxPente() const {
		double pente = std::numeric_limits<double>::min();
		if (!vecChain.empty()){
			vector<Chain*>::const_iterator it = vecChain.begin();
			pente = (*it)->getPente();
			for(it = vecChain.begin(); it<vecChain.end(); ++it){
				if((*it)->getPente() > pente) pente = (*it)->getPente();
			}
			
			return pente;
		}
		return pente;
	}
	
	double DirectedGraph::getMeanPente() const {
		double pente = 0;
		if (!vecChain.empty()){
			vector<Chain*>::const_iterator it = vecChain.begin();
			for(it = vecChain.begin(); it<vecChain.end(); ++it){
				pente = pente + (*it)->getPente();
			}
			
			return pente/lexical_cast<double>(vecChain.size());
		}
		return std::numeric_limits<double>::max();
	}
	
	double DirectedGraph::getSumPente() const {
		double pente = 0;
		if (!vecChain.empty()){
			vector<Chain*>::const_iterator it = vecChain.begin();
			for(it = vecChain.begin(); it<vecChain.end(); ++it){
				pente = pente + (*it)->getPente();
			}
			
			return pente;
		}
		return pente;
	}
	
	int DirectedGraph::getMaxNumAlignForOneChain(int* type, bool* booParam, double* param) const {
		int numAlignMax = numeric_limits<int>::min();
		for(vector<Chain*>::const_iterator it = vecChain.begin(); it<vecChain.end(); ++it){
			if ((*it)->chainValide(type[TYPE_FILTER], param[PARAM_NUMBER_MIN_ALIGN], param[PARAM_SCORE_MIN_CHAIN], param[PARAM_COEF_CORRELATION], booParam[BPARAM_GLOBAL_VALIDATION_OVERLAP])) {
				if ((*it)->getNumAlign()>numAlignMax){
					numAlignMax = (*it)->getNumAlign();
				}
			}
		}
		return numAlignMax;
	}
	
	int DirectedGraph::getSumNumAlignForOneChain(int* type, bool* booParam, double* param) const {
		int sumAlignMax = 0;
		for(vector<Chain*>::const_iterator it = vecChain.begin(); it<vecChain.end(); ++it){
			if ((*it)->chainValide(type[TYPE_FILTER], param[PARAM_NUMBER_MIN_ALIGN], param[PARAM_SCORE_MIN_CHAIN], param[PARAM_COEF_CORRELATION], booParam[BPARAM_GLOBAL_VALIDATION_OVERLAP])) {
				sumAlignMax += (*it)->getNumAlign();
			}
		}
		return sumAlignMax;
	}
	
	int DirectedGraph::getnumChainValide(int* type, bool* booParam, double* param) const {
		int nbChain = 0;
		for(vector<Chain*>::const_iterator it = vecChain.begin(); it<vecChain.end(); ++it){
			if ((*it)->chainValide(type[TYPE_FILTER], param[PARAM_NUMBER_MIN_ALIGN], param[PARAM_SCORE_MIN_CHAIN], param[PARAM_COEF_CORRELATION], booParam[BPARAM_GLOBAL_VALIDATION_OVERLAP])) {
				++nbChain;
			}
		}
		return nbChain;
	}
	
	void DirectedGraph::testSingleton(ofstream& fileOut, double pIden, double lengthMin, double distMaxbetweenAnchor) const {
		
		
		if (mapAlign.begin() != mapAlign.end()){
			if (mapAlign.begin()->second->getPidentity()>pIden 
					&& mapAlign.begin()->second->getLengthSubject()>=lengthMin 
					&& mapAlign.begin()->second->getLengthQuery()>=lengthMin) {
						
				int beginRegion = mapAlign.begin()->second->getQueryMin();
				if (beginRegion > mapAlign.begin()->second->getSubjectMin()) {
					beginRegion = mapAlign.begin()->second->getSubjectMin();
				}
				
				int endRegion = mapAlign.begin()->second->getQueryMax();
				if (endRegion < mapAlign.begin()->second->getSubjectMax()) {
					endRegion = mapAlign.begin()->second->getSubjectMax();
				}
				
				if ( abs(mapAlign.begin()->second->getQueryMin() - mapAlign.begin()->second->getSubjectMin()) > distMaxbetweenAnchor) return;
				
				fileOut << mapAlign.begin()->second->getQueryChrom();
				fileOut << "\t" << beginRegion;
				fileOut << "\t" << endRegion;
				fileOut << "\t" << mapAlign.begin()->second->getQueryMin();
				fileOut << "\t" << mapAlign.begin()->second->getQueryMax();
				fileOut << "\t2";
				fileOut << "\t" << mapAlign.begin()->second->getQueryMin() << ".." << mapAlign.begin()->second->getQueryMax() << ",";
				fileOut << mapAlign.begin()->second->getSubjectMin() << ".." << mapAlign.begin()->second->getSubjectMax() << ",";
				fileOut << "\n";
			}
		}
		
	}
	
	void DirectedGraph::afficheParent(){
		
		int presentI = idSink;
		
		while (presentI!=idSource){
			cout << presentI << "\t";
			presentI = parent[presentI];
		}
		cout << endl;
		
	}
	
	void DirectedGraph::afficheScoreChain() const {
		for(vector<Chain*>::const_iterator it = vecChain.begin(); it<vecChain.end(); ++it){
			cout << (*it)->getChainScore() << "\t";
		}
		if (vecChain.begin()!=vecChain.end()) cout << endl;
	}
	
	void DirectedGraph::printChains(char* fileOut, int* type, bool* booParam, double* param) const {
		ofstream chain_file(fileOut);
				
		chain_file << "numChain\tcomplete\ts_chrom\tq_chrom\ts_start\ts_end\tq_start\tq_end\tscore\n";
		
		int cptChain = 0;
		for(vector<Chain*>::const_iterator it = vecChain.begin(); it<vecChain.end(); ++it){
			(*it)->printRes(chain_file, cptChain, type, booParam, param);
			++cptChain;
		}
		
		chain_file << "\n";
		chain_file.close();
	}
	
	void DirectedGraph::afficheChains(int* type, bool* booParam, double* param) const {
						
		int cptChain = 0;
		for(vector<Chain*>::const_iterator it = vecChain.begin(); it<vecChain.end(); ++it){
			(*it)->afficheChain(cptChain, type, booParam, param);
			++cptChain;
		}
		
	}
	
	void DirectedGraph::printChains(ofstream& chain_file, int* type, bool* booParam, double* param) const {
				
		int cptChain = 0;
		for(vector<Chain*>::const_iterator it = vecChain.begin(); it<vecChain.end(); ++it){
			(*it)->printRes(chain_file, cptChain, type, booParam, param);
			++cptChain;
		}
	}
	
		
	void DirectedGraph::afficheRes(const char* fileOut){
	
		graph_traits<Graph>::edge_iterator ei, ei_end;
		int taille = 40;
		
		ofstream dot_file(fileOut);
		dot_file << "digraph D {\n"
    		<< "  rankdir=LR\n"
    		<< "  size=\"" << taille << "," << taille << "\"\n"
    		<< "  ratio=\"fill\"\n"
    		<< "  edge[style=\"bold\"]\n" << "  node[shape=\"circle\"]\n";

  		{
    		for (tie(ei, ei_end) = edges(g); ei != ei_end; ++ei) {
      			graph_traits < Graph >::edge_descriptor e = *ei;
      			graph_traits < Graph >::vertex_descriptor u = source(e, g);
      			graph_traits < Graph >::vertex_descriptor v = target(e, g);
      			
      			dot_file << u << " -> " << v
        			<< "[label=\"" << get(get(&EdgeWeight::weight, g), e) << "\"";
      			
      			if (parent[v] == u)	dot_file << ", color=\"black\"";
      			else	dot_file << ", color=\"grey\"";
      			dot_file << "]";
    		}
  		}
  		dot_file << "}";
	}
	
	void DirectedGraph::afficheRes(){
	
		graph_traits<Graph>::edge_iterator ei, ei_end;
		int taille = 40;
		
		cout << "digraph D {\n"
    		<< "  rankdir=LR\n"
    		<< "  size=\"" << taille << "," << taille << "\"\n"
    		<< "  ratio=\"fill\"\n"
    		<< "  edge[style=\"bold\"]\n" << "  node[shape=\"circle\"]\n";

  		{
    		for (tie(ei, ei_end) = edges(g); ei != ei_end; ++ei) {
      			graph_traits < Graph >::edge_descriptor e = *ei;
      			graph_traits < Graph >::vertex_descriptor u = source(e, g);
      			graph_traits < Graph >::vertex_descriptor v = target(e, g);
      			
      			cout << u << " -> " << v
        			<< "[label=\"" << get(get(&EdgeWeight::weight, g), e) << "\"";
      			
      			if (parent[v] == u)	cout << ", color=\"black\"";
      			else	cout << ", color=\"grey\"";
      			cout << "]";
    		}
  		}
  		cout << "}" << endl;
	}
	
	DirectedGraph::~DirectedGraph(){
		
	}

}
