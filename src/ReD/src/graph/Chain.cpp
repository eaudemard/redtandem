#include "Chain.h"
#include "./DirectedGraph.h"

namespace graphe{

	Chain::Chain(){
		q_start = -1;
		q_end = -1;
		s_start = -1;
		s_end = -1;
		
		scoreAlign = -1;
		scoreChain = -1;
		numOpenGap = -1;
		numDeletion = -1;
		numSubstitution = -1;
		size = -1;
		
		scoreStruct = -1;
		
		q_chrom="";
		s_chrom="";
		
		dupTandem = false;
		chevauche = false;
	}
	
	Chain::Chain(int start, int end, string chrom){
		q_start = start;
		q_end = end;
		s_start = start;
		s_end = end;
		
		scoreAlign = -1000;
		scoreChain = -1000;
		numOpenGap = -1;
		numDeletion = -1;
		numSubstitution = -1;
		size = -1;
		
		scoreStruct = -1;
		
		q_chrom=chrom;
		s_chrom=chrom;
		
		dupTandem = false;
		chevauche = false;
		coefCorrelation = 1;
	}

	Chain::Chain(Alignement& align, double score){
		
		bool reverseAlign = align.getReverse();
		align.setReverse(false);
		q_start = align.getQueryMin();
		q_end = align.getQueryMax();
		s_start = align.getSubjectMin();
		s_end = align.getSubjectMax();
		align.setReverse(reverseAlign);
		
		scoreAlign = -score;
		scoreChain = -score;
		numOpenGap = 0;
		numDeletion = 0;
		numSubstitution = 0;
		size = align.getSize();
		evalue = align.getEvalue();
		
		scoreStruct = size*size;
		
		q_chrom = align.getQueryChrom();
		s_chrom = align.getSubjectChrom();
		
		vecAlign.push_back(&align);
		mapAlignQuery.insert (pair<string,Alignement*>(align.getQueryName(),&align));
		mapAlignSubject.insert (pair<string,Alignement*>(align.getSubjectName(),&align));
		
		dupTandem = false;
		chevauche = false;
		reverse = align.getReverse();
		coefCorrelation = 1;
		
		idGraph = -1;
	}

	Chain::Chain(Alignement& align, int* type, bool* booParam, double* param, bool bReverse, int idG, vector<double>& vecCost){
		
		bool reverseAlign = align.getReverse();
		align.setReverse(false);
		q_start = align.getQueryMin();
		q_end = align.getQueryMax();
		s_start = align.getSubjectMin();
		s_end = align.getSubjectMax();
		align.setReverse(reverseAlign);
		
		scoreAlign = align.costAlign(type[1], param[1], booParam[10])/vecCost.at(0);
		scoreChain = scoreAlign;
		numOpenGap = 0;
		numDeletion = 0;
		numSubstitution = 0;
		size = align.getSize();
		evalue = align.getEvalue();
		
		scoreStruct = size*size;
		
		q_chrom = align.getQueryChrom();
		s_chrom = align.getSubjectChrom();
		
		vecAlign.push_back(&align);
		mapAlignQuery.insert (pair<string,Alignement*>(align.getQueryName(),&align));
		mapAlignSubject.insert (pair<string,Alignement*>(align.getSubjectName(),&align));
		
		dupTandem = false;
		chevauche = false;
		reverse = bReverse;
		
		idGraph = idG;
	}
	
	int Chain::getNumMatch() const {
		int num = 0;
		for(vector<Alignement*>::const_iterator it = vecAlign.begin(); it<vecAlign.end(); it++){
			num = num + (*it)->getSize();
		}
		return num;
	}
	
	void Chain::computeCoefCorrelation() {
		if (vecAlign.size()==1) {
			coefCorrelation = 1;
		} else {
			coefCorrelation = util::computeCoefCorrelation(vecAlign.begin(), vecAlign.end());
		}
				
	}
	
	void Chain::addAlignForward( Alignement& align, int* type, bool* booParam, double* param, vector<double>& vecCost){
		
		Alignement* align2;
		double costAlign;
		double weight;
		
		bool reverseAlign = align.getReverse();
		align.setReverse(false);
		if (align.getQueryMin() < q_start){
			q_start = align.getQueryMin();
		}
		if (align.getQueryMax() > q_end){
			q_end = align.getQueryMax();
		}
		
		if (align.getSubjectMin() < s_start){
			s_start = align.getSubjectMin();
		}
		if (align.getSubjectMax() > s_end){
			s_end = align.getSubjectMax();
		}
		align.setReverse(reverseAlign);
		
		
		align2 = vecAlign.at(vecAlign.size()-1);
		costAlign = align.costAlign(type[1], param[1], booParam[10])/vecCost.at(0);
		
		weight = align2->weight(align, type[2], booParam[0], param[2], booParam[4], booParam[12], param[PARAM_PENALITY_EXTEND_GAP])/vecCost.at(1);
		scoreAlign = scoreAlign + costAlign;
		scoreChain = scoreChain + costAlign + weight;
		evalue = evalue + align.getEvalue();
		
		int nbSubstitution = align2->getNumSubstitution(align, false, booParam[4]);
		numSubstitution = numSubstitution + nbSubstitution;
		numDeletion = numDeletion + align2->getNumDeletion(align, false, nbSubstitution, booParam[4]);
		++numOpenGap;
		
		size = size + numDeletion + align.getSize();
		
		scoreStruct = scoreStruct + align.getSize()*align.getSize() + numDeletion*numDeletion;
		
		vecAlign.push_back(&align);
		
		if (q_chrom == s_chrom){
			if (align.getQueryName()!="" && align.getSubjectName()!=""){
				if (mapAlignSubject.find(align.getQueryName()) == mapAlignSubject.end())		mapAlignQuery.insert (pair<string,Alignement*>(align.getQueryName(),&align));
				else	{
					dupTandem = true;
				}
				if (mapAlignQuery.find(align.getSubjectName()) == mapAlignQuery.end())		mapAlignSubject.insert (pair<string,Alignement*>(align.getSubjectName(),&align));
				else	{
					dupTandem = true;
				}
			} 
		}
	}
	
	int Chain::chainTeeth3(int* type, bool* booParam, double* param, bool bReverse, vector<double>& vecCost) {
		
		int nbChain = -1;
		
		bool boo = false;
		vector<Alignement*>::const_iterator itMiddle = vecAlign.begin();
		for(unsigned int i=0; i<(vecAlign.size()/2)+1; ++i){
			++itMiddle;
		}
		
		
		double coef1 = util::computeCoefCorrelation(vecAlign.begin(), itMiddle);
		double coef2 = util::computeCoefCorrelation(itMiddle, vecAlign.end());
		
		if (coef1<0 && coef2>0) boo = true;
		if (coef1>0 && coef2<0) boo = true;
		
		computeCoefCorrelation();
		
		if (boo){
			unsigned int indMin = 0;
			unsigned int indMax = 0;
			
			for(unsigned int i=0; i!= vecAlign.size(); ++i){
				if ( (vecAlign.at(i))->getQueryMin() == q_start ) indMin = i;
				if ( (vecAlign.at(i))->getQueryMax() == q_end ) indMax = i;
			}
			unsigned int ind = ( abs(((int)indMin)-((int)(vecAlign.size()/2))) < abs(((int)indMax)-((int)(vecAlign.size()/2))) )? indMin:indMax;
			
			if(ind==0 || ind==(vecAlign.size()-1)) return -1;
			
			double w1 = (vecAlign.at(ind-1))->weight(*(vecAlign.at(ind)), type[2], booParam[0], param[2], booParam[4], booParam[12], param[PARAM_PENALITY_EXTEND_GAP]);
			double w2 = (vecAlign.at(ind))->weight(*(vecAlign.at(ind+1)), type[2], booParam[0], param[2], booParam[4], booParam[12], param[PARAM_PENALITY_EXTEND_GAP]);
			
			bool cut = ( w1 < w2 )? false:true; 
			int idChain = 0;
			vecChainTeeth.push_back(new Chain(*vecAlign.at(0), type, booParam, param, bReverse, idGraph, vecCost));
			++nbChain;
			for(unsigned int i=1; i!= vecAlign.size(); ++i){
				if (cut) {
					if (i==ind+1) {
						vecChainTeeth.push_back(new Chain(*vecAlign.at(i), type, booParam, param, bReverse, idGraph, vecCost));
						++idChain;
						++nbChain;
				} else {
						vecChainTeeth.at(idChain)->addAlignForward(*vecAlign.at(i), type, booParam, param, vecCost);
					}
				} else {
					if (i==ind) {
						vecChainTeeth.push_back(new Chain(*vecAlign.at(i), type, booParam, param, bReverse, idGraph, vecCost));
						++idChain;
						++nbChain;
					} else {
						vecChainTeeth.at(idChain)->addAlignForward(*vecAlign.at(i), type, booParam, param, vecCost);
					}
				}
			}
		} else {
			return nbChain;	
		}
		
		return nbChain;	
	}
	
	double Chain::pOverlapQuery (const Chain& C) const {
		
		int start = 0;
		int end = 0;
		
		if (C.getQueryChrom() == getQueryChrom()) {
			start = (C.getQueryMin()<getQueryMin()) ?  getQueryMin() : C.getQueryMin();
			end =  (C.getQueryMax()>getQueryMax()) ? getQueryMax() : C.getQueryMax();
			if( (getQueryMax()-getQueryMin())!=0 )	return ((double)(end-start))/(getQueryMax()-getQueryMin());
			else {
				cout << "1 div 0" << endl;
				afficheChain();
				C.afficheChain();
				return 0;
			}
		}
		
		return 0;	
	}
	
	double Chain::pOverlapSubject (const Chain& C) const {
		
		int start = 0;
		int end = 0;
		
		if (C.getSubjectChrom() == getSubjectChrom()) {
			start = (C.getSubjectMin()<getSubjectMin()) ?  getSubjectMin() : C.getSubjectMin();
			end =  (C.getSubjectMax()>getSubjectMax()) ? getSubjectMax() : C.getSubjectMax();
			if( (getSubjectMax()-getSubjectMin())!=0 )	return ((double)(end-start))/(getSubjectMax()-getSubjectMin());
			else {
				cout << "1 div 0" << endl;
				afficheChain();
				C.afficheChain();
				return 0;
			}
		}
		
		return 0;	
	}
	
	bool Chain::chainOverlap(Chain& ch) {
		bool booChevauche = false;
		
		if (pOverlapSubject(ch)>0 && pOverlapQuery(ch)>0) booChevauche=true;
		
		if (getNbAlign()==1){
			chevauche = chevauche || booChevauche;
		} 
		if (ch.getNbAlign()==1){
			ch.updateChevauche(booChevauche);
		} 
		
		if (getNbAlign()>1 && ch.getNbAlign()>1){
			chevauche = chevauche || booChevauche;
			ch.updateChevauche(booChevauche);
		} else {
			return false;	
		}
		
		return booChevauche;
	}
	
	bool Chain::chainValide(int type, int numMinAlign, int scoreMin, double coefCorr, bool booOverlapGlobal, int lengthMin) const {
		bool boo = true;
		
		if (getNbAlign() < numMinAlign) return false;
		if (scoreMin > getChainScore()) return false;
		if (coefCorr > std::abs(getCoefCorrelation())) return false;
		if(booOverlapGlobal) boo = boo && !chevauche;
		
		switch (type){
			case 0 :
				 boo = boo && true;
				break;
			case 1 :
				 boo = boo && !dupTandem;
				break;
			case 2 :
				 boo = boo && !seqOverlap();
				break;
			case 3 : 
				 boo = boo && (!dupTandem && !seqOverlap());
				break;
			case 4 : 
				boo = boo && (getQueryLength()>= lengthMin || getSubjectLength()>= lengthMin);
				break;
			default :
				cout << "le type de filtre n'est pas correct" << endl;
				exit(EXIT_FAILURE);
		}
		
		return boo;
	}
	
	
	bool Chain::deleteEdege(vector<DirectedGraph*>& vecGraph) {
		
		vector<Alignement*>::const_iterator itPrec = vecAlign.begin();
		
		for(vector<Alignement*>::const_iterator it = vecAlign.begin(); it != vecAlign.end(); ++it){
			(vecGraph.at(idGraph))->deleteEdge( (*it)->getIdInGraph()*2,(*it)->getIdInGraph()*2+1 );
			(vecGraph.at(idGraph))->deleteEdge( (*it)->getIdInGraph()*2+1,(*it)->getIdInGraph()*2 );
			(vecGraph.at(idGraph))->deleteEdge( (vecGraph.at(idGraph))->getIdSource(), (*it)->getIdInGraph()*2 );
			(vecGraph.at(idGraph))->deleteEdge( (*it)->getIdInGraph()*2, (vecGraph.at(idGraph))->getIdSource() );
			(vecGraph.at(idGraph))->deleteEdge( (*it)->getIdInGraph()*2+1, (vecGraph.at(idGraph))->getIdSink() );
			(vecGraph.at(idGraph))->deleteEdge( (vecGraph.at(idGraph))->getIdSink(), (*it)->getIdInGraph()*2+1 );
			
			(vecGraph.at(idGraph))->deleteEdge( (*it)->getIdInGraph()*2,(*itPrec)->getIdInGraph()*2+1 );
			(vecGraph.at(idGraph))->deleteEdge( (*itPrec)->getIdInGraph()*2,(*it)->getIdInGraph()*2+1 );
			(vecGraph.at(idGraph))->deleteEdge( (*itPrec)->getIdInGraph()*2+1,(*it)->getIdInGraph()*2 );
			(vecGraph.at(idGraph))->deleteEdge( (*it)->getIdInGraph()*2+1,(*itPrec)->getIdInGraph()*2 );
		}
		
		return true;
	}
	
	void Chain::printResRed(ofstream& chain_file, int numChain, int* type, bool* booParam, double* param) const {
		bool reverseP = false;
		if (!booParam[BPARAM_REVERSE_MODE])	reverseP = (coefCorrelation<0)? true:false;
		else				reverseP = reverse;
		
		vector<Alignement*>::const_iterator it = vecAlign.begin();
		vector<Alignement*>::const_iterator itPrec = vecAlign.begin();
		double score = 0;
		double weight;
		if(it!=vecAlign.end()){
			
			(*itPrec)->setReverse(reverse);
			(*it)->setReverse(reverse);	
			score = (*it)->costAlign(type[1], param[1], booParam[10]);
			if(reverse) {
				(*itPrec)->setReverse(false);
				(*it)->setReverse(false);
			}
	
			chain_file << numChain << "\t0\t" << reverseP << "\t" << (*it)->getSubjectChrom() << 
				"\t" << (*it)->getQueryChrom() << "\t"<< (*it)->getSubjectName() << 
				"\t" << (*it)->getQueryName() << "\t" << (*it)->getSubjectStart() << 
				"\t" << (*it)->getSubjectEnd() << "\t" << (*it)->getQueryStart() << 
				"\t" << (*it)->getQueryEnd() << "\t" << -score <<  
				"\t" << (*it)->getSlope() << "\t" << 2 << "\t" << 0 << "\n";
			++it;
			
			int nbSub;
			int nbDel;
			
			while(it!=vecAlign.end()){
				nbSub = (*itPrec)->getNumSubstitution(**it, false, booParam[4]);
				nbDel = (*itPrec)->getNumDeletion(**it, false, nbSub, booParam[4]);
				
				(*itPrec)->setReverse(reverse);
				(*it)->setReverse(reverse);
				weight = (*itPrec)->weight(*(*it), type[2], booParam[0], param[2], booParam[4], booParam[12], param[PARAM_PENALITY_EXTEND_GAP]);
				score = score + (*it)->costAlign(type[1], param[1], booParam[10]);
				score = score + weight;
				if (reverse) {
					(*itPrec)->setReverse(false);
					(*it)->setReverse(false);
				}
				
				
				chain_file << numChain << "\t0\t" << reverseP << "\t" << (*it)->getSubjectChrom() << 
						"\t" << (*it)->getQueryChrom() << "\t"<< (*it)->getSubjectName() << 
						"\t" << (*it)->getQueryName() << "\t" << (*it)->getSubjectStart() << 
						"\t" << (*it)->getSubjectEnd() << "\t" << (*it)->getQueryStart() << 
						"\t" << (*it)->getQueryEnd() << "\t" << -score <<  
						"\t" << (*it)->getSlope() << "\t" << 2 << "\t" << 0 << "\n";
						
				++it;
				++itPrec;
			}
			it = vecAlign.begin();
			
			chain_file << numChain << "\t1\t" << reverseP << "\t" << s_chrom << 
					"\t" << q_chrom << "\t" << "aaaaaaaaa" << 
					"\t" <<  "aaaaaaaaa" << "\t" << s_start << 
					"\t" << s_end << "\t" << q_start << 
					"\t" << q_end << "\t" << -scoreChain <<  
					"\t" << getPente() << "\t" << vecAlign.size()*2 << "\t" << coefCorrelation << "\n";	
		}
	}
	
	void Chain::printResNarcisse(ofstream& chain_file, int* type, bool* booParam) const {
		char reverseP;
		if (!booParam[BPARAM_REVERSE_MODE])	reverseP = (coefCorrelation<0)? '-':'+';
		else				reverseP = (reverse)? '-':'+';
		
		chain_file << orderQuery << "\t" << orderQuery << "\t" << q_chrom << "\t" << q_start << 
			"\t" << q_end << "\t"<< reverseP << "\t" << s_chrom << 
			"\t" << s_start << "\t" << s_end << "\t" <<  orderSubject << 
			"\t" << -scoreChain << "\t" << vecAlign.size() <<"\t";
		for(vector<Alignement*>::const_iterator it = vecAlign.begin(); it != vecAlign.end(); ++it){
			chain_file << (*it)->getOrderQuery() << ":";
		}
		chain_file << "\t";
		for(vector<Alignement*>::const_iterator it = vecAlign.begin(); it != vecAlign.end(); ++it){
			chain_file << (*it)->getOrderSubject() << ":";
		}
		
		if (type[6]==0) {	chain_file << "\t" << "dna" <<"\n"; }
		else {
			if (type[6]==1) {	chain_file << "\t" << "prot" <<"\n"; }
			else {
				chain_file << "\t" << "unknow" <<"\n";
			}
		}
	}
		
	void Chain::printRes(ofstream& chain_file, int numChain, int* type, bool* booParam, double* param) const {
		if (type[5]==0)	{
			printResRed(chain_file, numChain, type, booParam, param);
		} else {
			if (type[5]==1) {
				printResNarcisse(chain_file, type, booParam);
			}
		}
	}
	
	void Chain::afficheChain(int numChain, int* type, bool* booParam, double* param) const {
		
		cout << "afficheChain" << endl;
		
		bool reverseP = false;
		if (!booParam[6])	reverseP = (coefCorrelation<0)? true:false;
		else				reverseP = reverse;
		
		
		vector<Alignement*>::const_iterator it = vecAlign.begin();
		vector<Alignement*>::const_iterator itPrec = vecAlign.begin();
		double score = 0;
		double weight;
		if(it!=vecAlign.end()){
			
			(*itPrec)->setReverse(reverse);
			(*it)->setReverse(reverse);	
			score = (*it)->costAlign(type[1], param[1], booParam[10]);
			if(reverse) {
				(*itPrec)->setReverse(false);
				(*it)->setReverse(false);
			}
			
			cout << numChain << "\t0\t" << reverseP << "\t" << (*it)->getSubjectChrom() << 
				"\t" << (*it)->getQueryChrom() << "\t"<< (*it)->getSubjectName() << 
				"\t" << (*it)->getQueryName() << "\t" << (*it)->getSubjectStart() << 
				"\t" << (*it)->getSubjectEnd() << "\t" << (*it)->getQueryStart() << 
				"\t" << (*it)->getQueryEnd() << "\t" << -score << 
				"\t" << -score << "\t" << (*it)->getEvalue() <<"\t" << (*it)->getSlope() << "\t" << 2 << "\n";
			++it;
			
			int nbSub;
			int nbDel;
			
			while(it!=vecAlign.end()){
				
				nbSub = (*itPrec)->getNumSubstitution(**it, false, booParam[4]);
				nbDel = (*itPrec)->getNumDeletion(**it, false, nbSub, booParam[4]);
				
				(*itPrec)->setReverse(reverse);
				(*it)->setReverse(reverse);
				weight = (*itPrec)->weight(*(*it), type[2], booParam[0], param[2], booParam[4], booParam[12], param[PARAM_PENALITY_EXTEND_GAP]);
				score = score + (*it)->costAlign(type[1], param[1], booParam[10]);
				score = score + weight;
				if (reverse) {
					(*itPrec)->setReverse(false);
					(*it)->setReverse(false);
				}
				
				
				cout << numChain << "\t0\t" << reverseP << "\t" << (*it)->getSubjectChrom() << 
						"\t" << (*it)->getQueryChrom() << "\t"<< (*it)->getSubjectName() << 
						"\t" << (*it)->getQueryName() << "\t" << (*it)->getSubjectStart() << 
						"\t" << (*it)->getSubjectEnd() << "\t" << (*it)->getQueryStart() << 
						"\t" << (*it)->getQueryEnd() << "\t" << -(*it)->costAlign(type[1], param[1], booParam[10]) <<  
						"\t" << -score << "\t" << (*it)->getEvalue() << (*it)->getSlope() << "\t" << 2 << "\n";
						
				++it;
				++itPrec;
			}
			it = vecAlign.begin();
			
			
			cout << numChain << "\t1\t" << reverseP  << "\t" << s_chrom << 
				"\t" << q_chrom << "\t" << "aaaaaaaaa" << 
				"\t" <<  "aaaaaaaaa" << "\t" << s_start << 
				"\t" << s_end << "\t" << q_start << 
				"\t" << q_end << "\t" << -scoreAlign << 
				"\t" << -scoreChain << "\t" << evalue/vecAlign.size() << 
				"\t" << getPente() << "\t" << vecAlign.size()*2 << "\n";	
		}
	}
	
	void Chain::afficheChain() const {
		
		vector<Alignement*>::const_iterator it = vecAlign.begin();
		
		if(it!=vecAlign.end()){
			cout << 0 << "\t1\t" << reverse  << "\t" << s_chrom << 
				"\t" << q_chrom << "\t" << "aaaaaaaaa" << 
				"\t" <<  "aaaaaaaaa" << "\t" << s_start << 
				"\t" << s_end << "\t" << q_start << 
				"\t" << q_end << "\t" << scoreAlign << 
				"\t" << -scoreChain << "\t" << evalue/vecAlign.size() << 
				"\t" << getPente() << "\t" << vecAlign.size()*2 << "\n";	
		}
	}
	
	Chain::~Chain(){
		
	}
	
}
