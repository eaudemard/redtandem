#ifndef UNDIRECTEDGRAPH_H_
#define UNDIRECTEDGRAPH_H_

#include "./AbstractGraph.h"
#include "./DirectedGraph.h"

//=============================================================
//lib boost
#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>
//=============================================================

using namespace boost;
using namespace data;
using namespace std;

namespace graphe{

	class UndirectedGraph : public AbstractGraph {
		
		typedef adjacency_list < vecS, vecS, undirectedS> Graph;
		Graph g;
		
	public:
		UndirectedGraph();
		UndirectedGraph(map<int, Alignement*>& mAlign);
		
		vector<unsigned long long int> createGraph (int* type, bool* booParam, double* param);
		
		
		void connectedComponents(vector<DirectedGraph*>& vecDirGraph, int cSource, int cSink, int* type, bool* booParam, double* param, vector<double>& vecCost, ofstream* fileSingletonOut=NULL);
		
		void afficheRes(char* fileOut);
		
		virtual ~UndirectedGraph();
	};
	
}

#endif /*UNDIRECTEDGRAPH_H_*/
	
