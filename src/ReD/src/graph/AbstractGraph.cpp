#include "AbstractGraph.h"

namespace graphe{
	AbstractGraph::AbstractGraph() {
	}
	
	AbstractGraph::AbstractGraph(map<int, Alignement*>& mAlign) {
		mapAlign = mAlign;
	}
	
	void AbstractGraph::eraseChain() {
		for(map<int,Alignement*>::iterator it = mapAlign.begin(); it != mapAlign.end(); ++it){
			((*it).second)->updateChain(-1,-1,false,false);
		}
	}
		
	void AbstractGraph::afficheAlign() const {
				
		for(map<int,Alignement*>::const_iterator it = mapAlign.begin(); it != mapAlign.end(); ++it){
			((*it).second)->affiche();
		}
		cout << endl;
		
	}
	
	double AbstractGraph::checkOrderAlign (const Alignement& Ai, const Alignement& Aj, int* type, bool* booParam, double* param) const {
	
		int distS = -1;
		int distQ = -1;
		
		if (!Ai.sameQueryChrom(Aj)) return -1;
		
		if (!Ai.sameSubjectChrom(Aj)) return -1;
		
		distS = Ai.getDistSubject(Aj,booParam[BPARAM_CUT_ALIGN], booParam[BPARAM_MIDDLE_ALIGN]);
		distQ = Ai.getDistQuery(Aj,booParam[BPARAM_CUT_ALIGN], booParam[BPARAM_MIDDLE_ALIGN]);
		
		double cost = Ai.costAlign(type[TYPE_SCORE_ALIGN], param[PARAM_SCORE_MAX_ALIGN], booParam[BPARAM_OVERLAP_BONUS])*DOUBLEINT;
		cost = cost + Aj.costAlign(type[TYPE_SCORE_ALIGN], param[PARAM_SCORE_MAX_ALIGN], booParam[BPARAM_OVERLAP_BONUS])*DOUBLEINT;
		
		if (booParam[BPARAM_SAME_POLARITY]) { if (Ai.getPolarite()!=Aj.getPolarite()) return -2; }
		
		
		if(type[TYPE_EXECUTION]==5){
			if (distQ<=0) return -3;
			
			if (booParam[BPARAM_ORDER]) {
				if (Ai.getPolarite()){
					if (distS<=0) return -4;
				} else {
					if (distS>=0) return -4;
				}
			}
		} else { 
			if (distQ<=0) return -3;
			
			if (booParam[BPARAM_ORDER]) {
				if (Ai.getPolarite()){
					if (distS<=0) return -4;
				} else {
					if (distS>=0) return -4;
				}
			} 
		}
		
		distS = abs(distS);
		distQ = abs(distQ);
		
		if (distS>param[PARAM_DIST_MAX_SUBJECT]) return -14;
		if (distQ>param[PARAM_DIST_MAX_QUERY]) return -7;
		if (booParam[BPARAM_SAME_SPECIE]){
			if (booParam[BPARAM_EDGE_OVERLAP] && Ai.overlapSeq(Aj, booParam[BPARAM_CUT_ALIGN])) return -8;
			if (booParam[BPARAM_EDGE_OVERLAP] && type[TYPE_EXECUTION]==6 && Ai.pOverlapSeq(Aj, booParam[BPARAM_CUT_ALIGN])) return -12;
		}
		
		int weight = Ai.weight(Aj, type[TYPE_SCORE_TRANSITION], booParam[BPARAM_CUT_ALIGN], param[PARAM_GAP_UNIT_LEN], booParam[BPARAM_MIDDLE_ALIGN], booParam[BPARAM_SLOPE_BONUS], param[PARAM_PENALITY_EXTEND_GAP])*DOUBLEINT;
		
		if (!booParam[BPARAM_EDGE_TANDEM]) {
			if (Ai.getSubjectName() != "" && (Ai.getSubjectName()).compare(Aj.getQueryName()) == 0 ) return -11;
			if (Ai.getQueryName() != "" && (Ai.getQueryName()).compare(Aj.getSubjectName()) == 0 ) return -11;
			if (Ai.getQueryName() != "" && (Ai.getQueryName()).compare(Aj.getQueryName()) == 0 ) return -10;
			if (Ai.getSubjectName() != "" && (Ai.getSubjectName()).compare(Aj.getSubjectName()) == 0 ) return -10;	
			cout << Ai.getQueryName() << " - " << Ai.getSubjectName() << " # "<< Aj.getQueryName() << " - " << Aj.getSubjectName() << endl;
		}
		
		return weight/DOUBLEINT;
	}
	
	
	
	void AbstractGraph::printOutAlign() {
		
		for(map<int,Alignement*>::iterator it = mapAlign.begin(); it != mapAlign.end(); ++it){
			(*it).second->affiche();
		}
		
	}
}
