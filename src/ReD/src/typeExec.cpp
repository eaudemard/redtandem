#ifndef TYPEEXEC_C_
#define TYPEEXEC_C_

#include "data/Alignement.h"
#include "graph/DirectedGraph.h"
#include "graph/AbstractGraph.h"
#include "graph/UndirectedGraph.h"
#include "util/UtilChain.h"
#include <stdlib.h> 
#include <cassert>
#include <getopt.h>

//=============================================================
//lib boost
#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/bellman_ford_shortest_paths.hpp>
#include <boost/array.hpp>
//=============================================================

#include <vector>
#include <valarray>
#include <map>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <limits>

using namespace boost;
using namespace data;
using namespace graphe;

int cptPrint = 0;

vector<Chain*>& addVectorChain(vector<Chain*>& vecT, vector<DirectedGraph*>& vecDirGraph){
	
	vecT.erase(vecT.begin(),vecT.end());
	int cpt =0;
	for(vector<DirectedGraph*>::iterator it = vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
		for (int index=0 ; index<(*it)->getNbChain(); ++index){
			vecT.push_back((*it)->getChain(index));
			++cpt;
		}
		(*it)->emptyVecChain();
	}
	
	return vecT;
}


void excecutionGloutonNumChain (map<int, Alignement*>& mapAlign, vector<Chain*>& vecT, vector<double>& vecCost, int cSource, int cSink, int* type, bool* booParam, double* param, bool modeReverse, int nbChainStop){
	
	cout << "debut de la création du graphe" << endl;
	UndirectedGraph g(mapAlign);
	vector<unsigned long long int> vecC = g.createGraph(type, booParam, param); 
	vecCost.push_back(vecC.at(0)/(param[PARAM_MEAN_SCORE_ANCHOR]*mapAlign.size()));
	vecCost.push_back(vecC.at(1)/(param[PARAM_MEAN_SCORE_TRANSITION]*vecC.at(2)));
	cout << "graphe créé" << endl;
	
	vector<DirectedGraph*> vecDirGraph;
	g.connectedComponents(vecDirGraph, cSource, cSink, type, booParam, param, vecCost);
	cout << "graphes connexes crées" << endl;
	
	int indGraphBestChain=-1;
	double bestScoreChain;
	double bestScoresChains [vecDirGraph.size()];
	vector<int> nbChainByGraphe(vecDirGraph.size(),0);
			
	int cpt = 0;
	
	
	for(vector<DirectedGraph*>::iterator itG = vecDirGraph.begin(); itG!=vecDirGraph.end(); ++itG){
		(*itG)->dagShortestPath();
		bestScoresChains[cpt] = - (*itG)->findBestChainAndUpdateGraph(true, true);
		++cpt;
		
	}
	
	int nbChain = 0;
	int cptExec = 0;
	
	while (nbChain != nbChainStop && indGraphBestChain!=-2) {
		if(cptExec%10==0){
			 cout << ".";
			 cout.flush();
		}
		++cptExec;
		
		bestScoreChain = std::numeric_limits<double>::min();
		indGraphBestChain = -1;	
		for(cpt=0; cpt < numeric_cast<int>(vecDirGraph.size()); ++cpt){
			if (bestScoreChain < bestScoresChains[cpt]){
				bestScoreChain = bestScoresChains[cpt];
				indGraphBestChain = cpt;
			}
		}
		
		
		if (indGraphBestChain == -1) {
			indGraphBestChain = -2;		
			cout << "number of chain is to high" << endl;
		}
		else {
			vecDirGraph.at(indGraphBestChain)->rebuildChain(type, booParam, param, modeReverse, vecCost);
			
			nbChain = 0;
			for(vector<DirectedGraph*>::const_iterator it= vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
				nbChain = nbChain + (*it)->getNbChain();
			}	
			vecDirGraph.at(indGraphBestChain)->dagShortestPath();
			bestScoresChains[indGraphBestChain] = - vecDirGraph.at(indGraphBestChain)->findBestChainAndUpdateGraph(true, true);
		}	
	}
	
	cout<<endl;
	addVectorChain(vecT, vecDirGraph);
	
	for (vector<DirectedGraph*>::iterator it=vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
		delete *it;
	}
		
}


void excecutionScore (map<int, Alignement*>& mapAlign, vector<Chain*>& vecT, vector<double>& vecCost, int cSource, int cSink, int* type, bool* booParam, double* param, bool modeReverse){
	cout << "excecutionScore" << endl;
	cout << "debut de la création du graphe" << endl;
	UndirectedGraph g(mapAlign);
	vector<unsigned long long int> vecC = g.createGraph(type, booParam, param); 
	
	cout << "taille graphe non dirigé : " << sizeof(UndirectedGraph) << endl;
	cout << "taille graphe dirigé : " << sizeof(DirectedGraph) << endl;
	vecCost.push_back(vecC.at(0)/(param[PARAM_MEAN_SCORE_ANCHOR]*mapAlign.size()));
	vecCost.push_back(vecC.at(1)/(param[PARAM_MEAN_SCORE_TRANSITION]*vecC.at(2)));
	
	cout << "Somme align : " << vecC.at(0) << " somme trans : " << vecC.at(1) << endl;
	cout << "graphe créé " << vecCost.at(0) << "\t" << vecCost.at(1) << endl;
	
	vector<DirectedGraph*> vecDirGraph;
	g.connectedComponents(vecDirGraph, cSource, cSink, type, booParam, param, vecCost);
	cout << "graphes connexes crées" << endl;
	
	int indGraphBestChain = -1;
	double bestScoreChain;
	double bestScoresChains [vecDirGraph.size()];
	vector<int> nbChainByGraphe(vecDirGraph.size(),0);
			
	int cpt = 0;
	
	int nbAlign = 0;
	
	for(vector<DirectedGraph*>::iterator itG = vecDirGraph.begin(); itG!=vecDirGraph.end(); ++itG){
		(*itG)->bellmanFord();
		
		bestScoresChains[cpt] = - (*itG)->findBestChainAndUpdateGraph(true, false);
		++cpt;
		
		nbAlign = nbAlign + (*itG)->getNumAlign();
	}
	
	int nbChain = 0;
	int cptExec = 0;
	
	while (nbChain != numeric_cast<int>(mapAlign.size()) && indGraphBestChain!=-2) {
		
		if(cptExec%10==0){
			 cout << ".";
			 cout.flush();
		}
		++cptExec;
		
		bestScoreChain = std::numeric_limits<double>::min();
		indGraphBestChain = -1;
		for(cpt=0; cpt<numeric_cast<int>(vecDirGraph.size()); ++cpt){
			if (bestScoreChain < bestScoresChains[cpt]){
				bestScoreChain = bestScoresChains[cpt];
				indGraphBestChain = cpt;
			}
		}
		
		if (indGraphBestChain == -1 || bestScoresChains[indGraphBestChain]<param[6])  {
			indGraphBestChain = -2;			
		}
		else {
		
			vecDirGraph.at(indGraphBestChain)->rebuildChain(type, booParam, param, modeReverse, vecCost);
			nbChain = 0;
			for(vector<DirectedGraph*>::const_iterator it= vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
				nbChain = nbChain + (*it)->getNbChain();
			}
			
			vecDirGraph.at(indGraphBestChain)->bellmanFord();
			bestScoresChains[indGraphBestChain] = - vecDirGraph.at(indGraphBestChain)->findBestChainAndUpdateGraph(true, false);
		}
	}
	
	cout << endl;
	
	cout << "phase of validation chain" << endl;
	bool setChainsValide [vecDirGraph.size()];
	cpt = 0;
	for(vector<DirectedGraph*>::const_iterator it= vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
		setChainsValide[cpt] = (*it)->getSetChainValide();
		++cpt;
	}
	
	nbAlign = 0;
	int score = 0;
	
	for(cpt=0; cpt < numeric_cast<int>(vecDirGraph.size()); ++cpt){
		nbChain = vecDirGraph.at(cpt)->getNbChain();
		nbAlign = vecDirGraph.at(cpt)->getNumAlign();
		score = -1;
		cptExec = 0;
		while (!setChainsValide[cpt] && nbChain!=(nbAlign-1) ){
			if(cptExec%10==0){
				  cout << ".";
				  cout.flush();
			}
			++cptExec;
			vecDirGraph.at(cpt)->rebuildChain(type, booParam, param, modeReverse, vecCost);
			vecDirGraph.at(cpt)->bellmanFord();
			score = vecDirGraph.at(cpt)->findBestChainAndUpdateGraph(true, false);
			setChainsValide[cpt] = vecDirGraph.at(cpt)->getSetChainValide();
			nbChain = vecDirGraph.at(cpt)->getNbChain();
		}
		if(score!=-1) {
			cout << endl;
			cout << "Set chain valide with score : " << score << endl;	
		}
	}
	
	cout << "phase of validation chain finish" << endl;
	
	cout<<endl;
	addVectorChain(vecT, vecDirGraph);
	
	for (vector<DirectedGraph*>::iterator it=vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
		delete *it;
	}	
}

void excecutionNumChain (map<int, Alignement*>& mapAlign, vector<Chain*>& vecT, vector<double>& vecCost, int cSource, int cSink, int* type, bool* booParam, double* param, bool modeReverse, int nbChainStop){
	cout << "excecutionScore" << endl;
	cout << "debut de la création du graphe" << endl;
	UndirectedGraph g(mapAlign);
	vector<unsigned long long int> vecC = g.createGraph(type, booParam, param); 
	
	cout << "taille graphe non dirigé : " << sizeof(UndirectedGraph) << endl;
	cout << "taille graphe dirigé : " << sizeof(DirectedGraph) << endl;
	vecCost.push_back(vecC.at(0)/(param[PARAM_MEAN_SCORE_ANCHOR]*mapAlign.size()));
	vecCost.push_back(vecC.at(1)/(param[PARAM_MEAN_SCORE_TRANSITION]*vecC.at(2)));
	
	cout << "Somme align : " << vecC.at(0) << " somme trans : " << vecC.at(1) << endl;
	cout << "graphe créé " << vecCost.at(0) << "\t" << vecCost.at(1) << endl;
	
	vector<DirectedGraph*> vecDirGraph;
	g.connectedComponents(vecDirGraph, cSource, cSink, type, booParam, param, vecCost);
	cout << "graphes connexes crées" << endl;
	
	int indGraphBestChain = -1;
	double bestScoreChain;
	double bestScoresChains [vecDirGraph.size()];
	vector<int> nbChainByGraphe(vecDirGraph.size(),0);
			
	int cpt = 0;
	
	int nbAlign = 0;
	
	for(vector<DirectedGraph*>::iterator itG = vecDirGraph.begin(); itG!=vecDirGraph.end(); ++itG){
		(*itG)->bellmanFord();
		
		bestScoresChains[cpt] = - (*itG)->findBestChainAndUpdateGraph(true, false);
		++cpt;
		
		nbAlign = nbAlign + (*itG)->getNumAlign();
	}
	
	int nbChain = 0;
	int cptExec = 0;
	
	while (nbChain != nbChainStop && indGraphBestChain!=-2) {
		
		if(cptExec%10==0){
			 cout << ".";
			 cout.flush();
		}
		++cptExec;
		
		bestScoreChain = std::numeric_limits<double>::min();
		indGraphBestChain = -1;
		for(cpt=0; cpt<numeric_cast<int>(vecDirGraph.size()); ++cpt){
			if (bestScoreChain < bestScoresChains[cpt]){
				bestScoreChain = bestScoresChains[cpt];
				indGraphBestChain = cpt;
			}
		}
		
		if (indGraphBestChain == -1)  {
			indGraphBestChain = -2;		
			cout << "number of chain is to high" << endl;	
		}
		else {
		
			vecDirGraph.at(indGraphBestChain)->rebuildChain(type, booParam, param, modeReverse, vecCost);
			nbChain = 0;
			for(vector<DirectedGraph*>::const_iterator it= vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
				nbChain = nbChain + (*it)->getNbChain();
			}
			
			vecDirGraph.at(indGraphBestChain)->bellmanFord();
			bestScoresChains[indGraphBestChain] = - vecDirGraph.at(indGraphBestChain)->findBestChainAndUpdateGraph(true, false);
		}
	}
		
	cout << endl;
	
	addVectorChain(vecT, vecDirGraph);
	
	for (vector<DirectedGraph*>::iterator it=vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
		delete *it;
	}	
}


void excecutionGloutonScore (map<int, Alignement*>& mapAlign, vector<Chain*>& vecT, vector<double>& vecCost, int cSource, int cSink, int* type, bool* booParam, double* param, double scoreMin, bool modeReverse){
	
	cout << "debut de la création du graphe" << endl;
	UndirectedGraph g(mapAlign);
	vector<unsigned long long int> vecC = g.createGraph(type, booParam, param); 
	vecCost.push_back(vecC.at(0)/(param[PARAM_MEAN_SCORE_ANCHOR]*mapAlign.size()));
	vecCost.push_back(vecC.at(1)/(param[PARAM_MEAN_SCORE_TRANSITION]*vecC.at(2)));
	cout << "graphe créé" << endl;
	
	vector<DirectedGraph*> vecDirGraph;
	g.connectedComponents(vecDirGraph, cSource, cSink, type, booParam, param, vecCost);
	cout << "graphes connexes crées" << endl;
	
	int indGraphBestChain=-1;
	double bestScoreChain;
	double bestScoresChains [vecDirGraph.size()];
	vector<int> nbChainByGraphe(vecDirGraph.size(),0);
			
	int cpt = 0;
	
	for(vector<DirectedGraph*>::iterator itG = vecDirGraph.begin(); itG!=vecDirGraph.end(); ++itG){
		(*itG)->dagShortestPath();
		bestScoresChains[cpt] = - (*itG)->findBestChainAndUpdateGraph(true, true);
		++cpt;
	}
	
	int nbChain = 0;
	int cptExec = 0;
	
	while (nbChain != numeric_cast<int>(mapAlign.size()) && indGraphBestChain!=-2) {
		if(cptExec%10==0){
			 cout << ".";
			 cout.flush();
		}
		++cptExec;
		
		bestScoreChain = std::numeric_limits<double>::min();
		indGraphBestChain = -1;	
		for(cpt=0; cpt < numeric_cast<int>(vecDirGraph.size()); ++cpt){
			if (bestScoreChain < bestScoresChains[cpt]){
				bestScoreChain = bestScoresChains[cpt];
				indGraphBestChain = cpt;
			}
		}
		
		if (indGraphBestChain == -1 || bestScoresChains[indGraphBestChain]<scoreMin) {
			indGraphBestChain = -2;
		}
		else {
			vecDirGraph.at(indGraphBestChain)->rebuildChain(type, booParam, param, modeReverse, vecCost);
			
			nbChain = 0;
			for(vector<DirectedGraph*>::const_iterator it= vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
				nbChain = nbChain + (*it)->getNbChain();
			}
			
			vecDirGraph.at(indGraphBestChain)->dagShortestPath();
			bestScoresChains[indGraphBestChain] = - vecDirGraph.at(indGraphBestChain)->findBestChainAndUpdateGraph(true, true);
		}	
	}
	
	cout<<endl;
	addVectorChain(vecT, vecDirGraph);
	
	for (vector<DirectedGraph*>::iterator it=vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
		delete *it;
	}
		
}

void excecutionDuplication (map<int, Alignement*>& mapAlign, vector<Chain*>& vecT, vector<double>& vecCost, int cSource, int cSink, int* type, bool* booParam, double* param, bool modeReverse){
	
	cout << "debut de la création du graphe" << endl;
	UndirectedGraph g(mapAlign);
	vector<unsigned long long int> vecC = g.createGraph(type, booParam, param); 
	vecCost.push_back(vecC.at(0)/(param[PARAM_MEAN_SCORE_ANCHOR]*mapAlign.size()));
	vecCost.push_back(vecC.at(1)/(param[PARAM_MEAN_SCORE_TRANSITION]*vecC.at(2)));
	cout << "graphe créé" << endl;
	
	vector<DirectedGraph*> vecDirGraph;
	g.connectedComponents(vecDirGraph, cSource, cSink, type, booParam, param, vecCost);
	cout << "graphes connexes crées" << endl;
	
	int indGraphBestChain=-1;
	double moyNumAlign = 0;
	int sumNumAlignForOneChain [vecDirGraph.size()];
	int numChainValide [vecDirGraph.size()];
	int numChain = 0; 
	double bestScoreChain;
	double bestScoresChains [vecDirGraph.size()];
	bool setChainsValide [vecDirGraph.size()];
	bool chainsValide = true;
	
	vector<int> nbChainByGraphe(vecDirGraph.size(),0);
			
	int cpt = 0;
	
	for(vector<DirectedGraph*>::iterator itG = vecDirGraph.begin(); itG!=vecDirGraph.end(); ++itG){
		(*itG)->bellmanFord();
		bestScoresChains[cpt] = - (*itG)->findBestChainAndUpdateGraph(true, false);
		(*itG)->rebuildChain(type, booParam, param, modeReverse, vecCost);
		sumNumAlignForOneChain[cpt] = (*itG)->getSumNumAlignForOneChain(type, booParam, param);
		numChainValide[cpt] = (*itG)->getnumChainValide(type, booParam, param);
		(*itG)->bellmanFord();
		bestScoresChains[cpt] = - (*itG)->findBestChainAndUpdateGraph(true, false);
		setChainsValide[cpt] = (*itG)->getSetChainValide();
		++cpt;		
	}
	
	int cptExec = 0;
	
	moyNumAlign = 0;
	numChain = 0;
	for(cpt=0; cpt<numeric_cast<int>(vecDirGraph.size()); ++cpt){
		
		moyNumAlign += sumNumAlignForOneChain[cpt];
		numChain += numChainValide[cpt];
		if (!setChainsValide[cpt]){
			chainsValide = false;
		}
	}
	
	int nbChain = 0;
	for(vector<DirectedGraph*>::const_iterator it= vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
		nbChain = nbChain + (*it)->getNbChain();
	}
			
	moyNumAlign = moyNumAlign / numChain;
	
	bool booScore = true;
	cout << "number max " << param[PARAM_NUMBER_MAX_ALIGN] << endl;
	while (nbChain != numeric_cast<int>(mapAlign.size()) && (moyNumAlign>param[PARAM_NUMBER_MAX_ALIGN] || booScore || !chainsValide) ) {
		if(cptExec%100==0){
			 cout << "." << moyNumAlign;
			 if (moyNumAlign>param[PARAM_NUMBER_MAX_ALIGN]) cout << "M";
			 if (booScore) cout << "S";
			 if (!chainsValide) cout << "V";
			 cout.flush();
		}
		++cptExec;
		
		bestScoreChain = std::numeric_limits<double>::min();
		indGraphBestChain = -1;
		for(cpt=0; cpt<numeric_cast<int>(vecDirGraph.size()); ++cpt){
			if (bestScoreChain < bestScoresChains[cpt]){
				bestScoreChain = bestScoresChains[cpt];
				indGraphBestChain = cpt;
			}
		}
		
		if (bestScoresChains[indGraphBestChain]<param[PARAM_SCORE_MIN_CHAIN]){
			booScore = false;	
		}
		
		if (indGraphBestChain == -1)  {
			indGraphBestChain = -2;			
		}
		else {
			vecDirGraph.at(indGraphBestChain)->rebuildChain(type, booParam, param, modeReverse, vecCost);
			setChainsValide[indGraphBestChain] = vecDirGraph.at(indGraphBestChain)->getSetChainValide();
			nbChain = 0;
			for(vector<DirectedGraph*>::const_iterator it= vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
				nbChain = nbChain + (*it)->getNbChain();
			}
			sumNumAlignForOneChain[indGraphBestChain] = vecDirGraph.at(indGraphBestChain)->getSumNumAlignForOneChain(type, booParam, param);
			numChainValide[indGraphBestChain] = vecDirGraph.at(indGraphBestChain)->getnumChainValide(type, booParam, param);
							
			vecDirGraph.at(indGraphBestChain)->bellmanFord();
			bestScoresChains[indGraphBestChain] = -vecDirGraph.at(indGraphBestChain)->findBestChainAndUpdateGraph(true, false);
					
			moyNumAlign = 0;
			numChain = 0;
			for(cpt=0; cpt<numeric_cast<int>(vecDirGraph.size()); ++cpt){
				moyNumAlign += sumNumAlignForOneChain[cpt];
				numChain += numChainValide[cpt];
				
				if (!setChainsValide[cpt]){
					chainsValide = false;
				}
			}
			moyNumAlign = moyNumAlign/numChain;
		}
	}
	
	cout << endl;
		
	addVectorChain(vecT, vecDirGraph);
	
	for (vector<DirectedGraph*>::iterator it=vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
		delete *it;
	}
}


void excecutionDuplicationByComposantConnexe (map<int, Alignement*>& mapAlign, vector<Chain*>& vecT, vector<double>& vecCost, int cSource, int cSink, int* type, bool* booParam, double* param, bool modeReverse){
	cout << "debut de la création du graphe" << endl;
	UndirectedGraph g(mapAlign);
	vector<unsigned long long int> vecC = g.createGraph(type, booParam, param); 
	vecCost.push_back(vecC.at(0)/(param[PARAM_MEAN_SCORE_ANCHOR]*mapAlign.size()));
	vecCost.push_back(vecC.at(1)/(param[PARAM_MEAN_SCORE_TRANSITION]*vecC.at(2)));
	cout << "graphe créé" << endl;
	
	vector<DirectedGraph*> vecDirGraph;
	g.connectedComponents(vecDirGraph, cSource, cSink, type, booParam, param, vecCost);
	cout << "graphes connexes crées" << endl;
	
	int nbChain = 0; 
	double scoresChains = 0;
	
	bool chainsValide = true;
	bool booScore = true;
	
	int cptExec = 0;
	
	
	for(vector<DirectedGraph*>::iterator itG = vecDirGraph.begin(); itG!=vecDirGraph.end(); ++itG){
		nbChain = 0;
		chainsValide = false;
		booScore = true;
		while (nbChain != (*itG)->getNumAlign() && (booScore || !chainsValide) ) {
			if(cptExec%100==0){
				cout << ".";
				if (booScore) cout << "S";
				if (!chainsValide) cout << "V";
				cout.flush();
			}
			++cptExec;
			
			(*itG)->bellmanFord();
			scoresChains = - (*itG)->findBestChainAndUpdateGraph(true, false);
			
			if (scoresChains<param[PARAM_SCORE_MIN_CHAIN]){
				booScore = false;	
			}
			
			if (booScore || !chainsValide) {
				(*itG)->rebuildChain(type, booParam, param, modeReverse, vecCost);	
				nbChain = (*itG)->getNbChain();
				chainsValide = (*itG)->getSetChainValide();
			}
			
		}	
	}
	
	cout << endl;
	
	vecT.clear();
	addVectorChain(vecT, vecDirGraph);
	
	for (vector<DirectedGraph*>::iterator it=vecDirGraph.begin(); it!=vecDirGraph.end(); ++it){
		delete *it;
	}
}

void excecutionDuplicationAfterTblastX (map<int, Alignement*>& mapAlign, vector<Chain*>& vecT, vector<double>& vecCost, int* type, bool* booParam, double* param){
	
	
	cout << "debut de la création du graphe" << endl;
	UndirectedGraph gU(mapAlign);
	vector<unsigned long long int> vecC = gU.createGraph(type, booParam, param); 
	vecCost.push_back(vecC.at(0)/(param[PARAM_MEAN_SCORE_ANCHOR]*mapAlign.size()));
	vecCost.push_back(vecC.at(1)/(param[PARAM_MEAN_SCORE_TRANSITION]*vecC.at(2)));
	
	
	cout << "debut de la création du graphe" << endl;
	DirectedGraph g(true, mapAlign, 0, 0, 0, type, booParam, param, vecCost);
	cout << "fin de la création du graphe" << endl;
	vector<DirectedGraph*> vecDirGraph;
	vecDirGraph.push_back(&g);
	int numAligneUse = 0;
	
	
	cout << "debut de la création des chaines" << endl;
	while( boost::numeric_cast<int>(mapAlign.size()) != numAligneUse){
		g.dagShortestPath();
		g.findBestChainAndUpdateGraph(true, true);
		g.rebuildChain(type, booParam, param, false, vecCost);
		
		addVectorChain(vecT, vecDirGraph);
		numAligneUse = 0;
		for (vector<Chain*>::iterator it=vecT.begin(); it!=vecT.end(); ++it){
			numAligneUse += (*it)->getNumAlign();
		}
	}
	
	
	
}

#endif 

