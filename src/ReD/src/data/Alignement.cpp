#include "Alignement.h"

namespace data{

	Alignement::Alignement(){
		
		id = -1;
		
		q_start = -1;
		q_end = -1;
		
		s_start = -1;
		s_end = -1;
		
		sign = false;
		
		score = -1;
		size = -1;
		
		nbMissMatch = -1;
		nbGap = -1;
		
		pIdentity = -1;
		evalue = -1;
		
		vertexIn = -1;
		vertexOut = -1;
		
		alignBefore = -1;
		alignAfter = -1;
		startChain = false;
		endChain = false;
		
		reverse = false;
		bUseInChain = false;
		
		percAlignOverlap = 0;
	}
	
	Alignement::Alignement(std::string l, vector<int> indice, int idAlign, int nbCutAlign, bool* booParam, string espece){

		vector<string> v;
		boost::split(v, l, boost::is_any_of("\t"),boost::algorithm::token_compress_off);
		
		id = idAlign;
		idInGraph = idAlign;
		bUseInChain = false;
				
		q_name = (indice[1]!=-1)? v.at(indice[1]):"";
		s_name = (indice[2]!=-1)? v.at(indice[2]):"";
		q_chrom = (indice[14]!=-1)?v.at(indice[14]):"-1";
		s_chrom = (indice[15]!=-1)?v.at(indice[15]):"-1";
				
		cutAlign = nbCutAlign;
		
		sign = (indice[7]!=-1)? ((v.at(indice[7])=="-")?false:true):false;
		
		try
        {
			q_start = (indice[3]!=-1)? boost::lexical_cast<int>(v.at(indice[3])):-1;
			q_end = (indice[4]!=-1)? boost::lexical_cast<int>(v.at(indice[4])):-1;
			
			s_start = (indice[5]!=-1)? boost::lexical_cast<int>(v.at(indice[5])):-1;
			s_end = (indice[6]!=-1)? boost::lexical_cast<int>(v.at(indice[6])):-1;
			
			while(v.at(indice[8]).at(0)==' '){
				v.at(indice[8]).erase(0,1);
			}
			score = (indice[8]!=-1)? boost::lexical_cast<double>(v.at(indice[8])):-1;
			size = (indice[9]!=-1)? boost::lexical_cast<double>(v.at(indice[9])):-1;
			
			pIdentity = (indice[10]!=-1)? boost::lexical_cast<double>(v.at(indice[10])):-1;
			if (pIdentity > 1) pIdentity = pIdentity/100;
			
			evalue = (indice[11]!=-1)? boost::lexical_cast<double>(v.at(indice[11])):-1;

			nbMissMatch = (indice[12]!=-1)? boost::lexical_cast<int>(v.at(indice[12])):-1;
			nbGap = (indice[13]!=-1)? boost::lexical_cast<int>(v.at(indice[13])):-1;
			
        }
        catch(boost::bad_lexical_cast& e) {
			cerr << "ReD -> Alignement.cpp -> Alignement() 1 : le fichier est dans un mauvais format. Exception sur un cast "<<endl;
			cerr << e.what() << endl;
			if (indice[3]!=-1)	cerr << "3:" << v.at(indice[3]) << " " ;
			if (indice[4]!=-1)	cerr << "4:" << v.at(indice[4]) << " " ;
			if (indice[5]!=-1)	cerr << "5:" << v.at(indice[5]) << " " ;
			if (indice[6]!=-1)	cerr << "6:" << v.at(indice[6]) << " " ;
			if (indice[8]!=-1)	cerr << "8:" << v.at(indice[8]) << " " ;
			if (indice[9]!=-1)	cerr << "9:" << v.at(indice[9]) << " " ;
			if (indice[10]!=-1)	cerr << "10:" << v.at(indice[10]) << " " ;
			if (indice[11]!=-1)	cerr << "11:" << v.at(indice[11]) << " " ;
			if (indice[12]!=-1)	cerr << "12:" << v.at(indice[12]) << " " ;
			if (indice[13]!=-1)	cerr << "13:" << v.at(indice[13]) << " " ;
			cerr << endl;
			
			exit(EXIT_FAILURE);
		}
		
		if (booParam[BPARAM_SAME_SPECIE]){
			try
       		{
				if (boost::lexical_cast<int>(q_chrom) > boost::lexical_cast<int>(s_chrom)){
					string sTemp = q_chrom;
					q_chrom = s_chrom;
					s_chrom = sTemp;
					
					int iTemp = q_start;
					q_start = s_start;
					s_start = iTemp;
					
					iTemp = q_end;
					q_end = s_end;
					s_end = iTemp;
				}
       		}
       		catch(boost::bad_lexical_cast &) {
       			cerr << "ReD -> Alignement.cpp -> Alignement() 2 : cast chrom"<<endl;
				exit(EXIT_FAILURE);
       		}
		}
		if (indice[16]!=-1) {
			if (v.at(indice[16])=="-") {
					int temp = s_start;
					s_start = s_end;
					s_end = temp;
			}
		}
		
		if (q_end<q_start) strand = true;
		else	strand = false;
		
		vertexIn = -1;
		vertexOut = -1;
		
		alignBefore = -1;
		alignAfter = -1;
		startChain = false;
		endChain = false;
		
		reverse = false;
		percAlignOverlap = 0;
		
		if (size==-1){
			size = getQueryMax()-getQueryMin();
			if(size > getSubjectMax()-getSubjectMin()){
				size = getSubjectMax()-getSubjectMin();
			}
		}
		
		if (indice[17]!=-1){
			try {
				size_t debut = v.at(indice[17]).find("_");
				if (debut == string::npos){
					q_chrom = boost::lexical_cast<string>(v.at(indice[17]).substr(espece.length(),string::npos));
				} else {
					q_chrom = boost::lexical_cast<string>(v.at(indice[17]).substr(espece.length(),debut-espece.length()));
					++debut; 
					size_t fin = v.at(indice[17]).find("-");
					q_start += boost::lexical_cast<int>(v.at(indice[17]).substr(debut,fin-debut))-1;
						q_end += boost::lexical_cast<int>(v.at(indice[17]).substr(debut,fin-debut))-1;
				}
		
			}
			catch(boost::bad_lexical_cast& e) {
				cerr << "ReD -> Alignement.cpp -> Alignement() 3 : le fichier est dans un mauvais format. Exception sur un cast "<<endl;
				cerr << e.what() << endl;
				if (indice[17]!=-1)	cerr << v.at(indice[17]) << " " ;
					cerr << endl;
				exit(EXIT_FAILURE);
			}
		}
		
		if (indice[18]!=-1){
			try {
				size_t debut = v.at(indice[18]).find("_");
				if (debut == string::npos){
					s_chrom = boost::lexical_cast<string>(v.at(indice[18]).substr(espece.length(),string::npos));
				} else {
					s_chrom = boost::lexical_cast<string>(v.at(indice[18]).substr(espece.length(),debut-espece.length()));
					++debut; 
					size_t fin = v.at(indice[18]).find("-");
					s_start += boost::lexical_cast<int>(v.at(indice[18]).substr(debut,fin-debut))-1;
						s_end += boost::lexical_cast<int>(v.at(indice[18]).substr(debut,fin-debut))-1;
				}
			}
			catch(boost::bad_lexical_cast& e) {
				cerr << "ReD -> Alignement.cpp -> Alignement() 4 : le fichier est dans un mauvais format. Exception sur un cast "<<endl;
				cerr << e.what() << endl;
				if (indice[18]!=-1)	cerr << v.at(indice[18]) << " " ;
					cerr << endl;
				exit(EXIT_FAILURE);
			}
		}
		
	}
	
	bool Alignement::overlapSeq (const Alignement& A, bool cut) const {
		return ( overlapSubject(A, cut) || overlapQuery(A, cut) );
	}
	
	bool Alignement::pOverlapSeq (const Alignement& A, bool cut) const {
		if (getQueryChrom() != A.getQueryChrom()) return false;
		if (getSubjectChrom() != A.getSubjectChrom()) return false;
		
		int start = 0;
		int end = 0;
		int startQuery = 0;
		int endQuery = 0;
		int startSubject = 0;
		int endSubject = 0;
		
		if (cut) {
			
			start = getQueryStartCut();
			if (start > getQueryEndCut()) 		start = getQueryEndCut();
			if (start > getSubjectStartCut()) 	start = getSubjectStartCut();
			if (start > getSubjectEndCut()) 	start = getSubjectEndCut();
			if (start > A.getQueryStartCut()) 	start = A.getQueryStartCut();
			if (start > A.getQueryEndCut()) 	start = A.getQueryEndCut();
			if (start > A.getSubjectStartCut()) start = A.getSubjectStartCut();
			if (start > A.getSubjectEndCut()) 	start = A.getSubjectEndCut();
			
			end = getQueryStartCut();
			if (end < getQueryEndCut()) 		end = getQueryEndCut();
			if (end < getSubjectStartCut()) 	end = getSubjectStartCut();
			if (end < getSubjectEndCut()) 		end = getSubjectEndCut();
			if (end < A.getQueryStartCut()) 	end = A.getQueryStartCut();
			if (end < A.getQueryEndCut()) 		end = A.getQueryEndCut();
			if (end < A.getSubjectStartCut()) 	end = A.getSubjectStartCut();
			if (end < A.getSubjectEndCut()) 	end = A.getSubjectEndCut();
			
			startQuery = getQueryStartCut();
			if (startQuery > getQueryEndCut()) 		startQuery = getQueryEndCut();
			if (startQuery > A.getQueryStartCut()) 	startQuery = A.getQueryStartCut();
			if (startQuery > A.getQueryEndCut()) 	startQuery = A.getQueryEndCut();
			
			endQuery = getQueryStartCut();
			if (endQuery < getQueryEndCut()) 		endQuery = getQueryEndCut();
			if (endQuery < A.getQueryStartCut()) 	endQuery = A.getQueryStartCut();
			if (endQuery < A.getQueryEndCut()) 		endQuery = A.getQueryEndCut();
			
			startSubject = getSubjectStartCut();
			if (startSubject > getSubjectEndCut()) 		startSubject = getSubjectEndCut();
			if (startSubject > A.getSubjectStartCut()) 	startSubject = A.getSubjectStartCut();
			if (startSubject > A.getSubjectEndCut()) 	startSubject = A.getSubjectEndCut();
			
			endSubject = getSubjectStartCut();
			if (endSubject < getSubjectEndCut()) 		endSubject = getSubjectEndCut();
			if (endSubject < A.getSubjectStartCut()) 	endSubject = A.getSubjectStartCut();
			if (endSubject < A.getSubjectEndCut()) 		endSubject = A.getSubjectEndCut();
				
		} 
		else {
			start = getQueryStart();
			if (start > getQueryEnd()) 		start = getQueryEnd();
			if (start > getSubjectStart()) 	start = getSubjectStart();
			if (start > getSubjectEnd()) 	start = getSubjectEnd();
			if (start > A.getQueryStart()) 	start = A.getQueryStart();
			if (start > A.getQueryEnd()) 	start = A.getQueryEnd();
			if (start > A.getSubjectStart()) start = A.getSubjectStart();
			if (start > A.getSubjectEnd()) 	start = A.getSubjectEnd();
			
			end = getQueryStartCut();
			if (end < getQueryEnd()) 		end = getQueryEnd();
			if (end < getSubjectStart()) 	end = getSubjectStart();
			if (end < getSubjectEnd()) 		end = getSubjectEnd();
			if (end < A.getQueryStart()) 	end = A.getQueryStart();
			if (end < A.getQueryEnd()) 		end = A.getQueryEnd();
			if (end < A.getSubjectStart()) 	end = A.getSubjectStart();
			if (end < A.getSubjectEnd()) 	end = A.getSubjectEnd();
			
			startQuery = getQueryStart();
			if (startQuery > getQueryEnd()) 		startQuery = getQueryEnd();
			if (startQuery > A.getQueryStart()) 	startQuery = A.getQueryStart();
			if (startQuery > A.getQueryEnd()) 		startQuery = A.getQueryEnd();
			
			endQuery = getQueryStart();
			if (endQuery < getQueryEnd()) 		endQuery = getQueryEnd();
			if (endQuery < A.getQueryStart()) 	endQuery = A.getQueryStart();
			if (endQuery < A.getQueryEnd()) 	endQuery = A.getQueryEnd();
			
			startSubject = getSubjectStart();
			if (startSubject > getSubjectEnd()) 		startSubject = getSubjectEnd();
			if (startSubject > A.getSubjectStart()) 	startSubject = A.getSubjectStart();
			if (startSubject > A.getSubjectEnd()) 		startSubject = A.getSubjectEnd();
			
			endSubject = getSubjectStart();
			if (endSubject < getSubjectEnd()) 		endSubject = getSubjectEnd();
			if (endSubject < A.getSubjectStart()) 	endSubject = A.getSubjectStart();
			if (endSubject < A.getSubjectEnd()) 	endSubject = A.getSubjectEnd();
		}
		
		int total = end-start;
		int totalQuery = endQuery-startQuery;
		int totalSubject = endSubject-startSubject;
		
		if( total*85/100 < (totalQuery+totalSubject)) return true;
		
		return false;
	}
	
	
	int Alignement::getNumDeletion(const Alignement& A, bool cut, int numSubstitution, bool middle) const {
		int numDeletion;
		
		if(middle) {
			if (abs(A.getQueryMiddle()) - getQueryMiddle() < abs(A.getSubjectMiddle()-getSubjectMiddle())){
				numDeletion = abs(A.getSubjectMiddle()-getSubjectMiddle() - numSubstitution);
			} else {
				numDeletion = abs(getQueryMiddle() - A.getQueryMiddle() - numSubstitution);
			}
			return numDeletion;
		}
		
		if (cut){
			switch (A.getPolarite()){
				case false : 
					if (abs(getQueryEndCut() - A.getQueryStartCut()) < abs(A.getSubjectStartCut()-getSubjectEndCut())){
						numDeletion = abs(A.getSubjectStartCut()-getSubjectEndCut() - numSubstitution);
					} else {
						numDeletion = abs(getQueryEndCut() - A.getQueryStartCut() - numSubstitution);
					}
					break;
				case true :
					if (abs(A.getQueryStartCut()-getQueryEndCut()) < abs(A.getSubjectStartCut()-getSubjectEndCut())){
						numDeletion = abs(A.getSubjectStartCut()-getSubjectEndCut() - numSubstitution);
					} else {
						numDeletion = abs(A.getQueryStartCut()-getQueryEndCut() - numSubstitution);
					}
			}
		}
		else {
			switch (A.getPolarite()){
				case false : 
					if (abs(getQueryEnd() - A.getQueryStart()) < abs(A.getSubjectStart()-getSubjectEnd())){
						numDeletion = abs(A.getSubjectStart()-getSubjectEnd() - numSubstitution);
					} else {
						numDeletion = abs(getQueryEnd() - A.getQueryStart() - numSubstitution);
					}
					break;
				case true :
					if (abs(A.getQueryStart()-getQueryEnd()) < abs(A.getSubjectStart()-getSubjectEnd())){
						numDeletion = abs(A.getSubjectStart()-getSubjectEnd() - numSubstitution);
					} else {
						numDeletion = abs(A.getQueryStart()-getQueryEnd() - numSubstitution);
					}
			}
		}
		return numDeletion;
	}
	int Alignement::getNumSubstitution(const Alignement& A, bool cut, bool middle) const {
		int numSubstitution;
		
		if(middle) {
			if (abs(getQueryMiddle() - A.getQueryMiddle()) < abs(A.getSubjectMiddle()-getSubjectMiddle())){
				numSubstitution = abs(getQueryMiddle() - A.getQueryMiddle());
			} else {
				numSubstitution = abs(A.getSubjectMiddle()-getSubjectMiddle());
			}
			return numSubstitution;
		}
		
		if (cut){
			switch (A.getPolarite()){
				case false : 
					if (abs(getQueryEndCut() - A.getQueryStartCut()) < abs(A.getSubjectStartCut()-getSubjectEndCut())){
						numSubstitution = abs(getQueryEndCut() - A.getQueryStartCut());
					} else {
						numSubstitution = abs(A.getSubjectStartCut()-getSubjectEndCut());
					}
					break;
				case true :
					if (abs(A.getQueryStartCut()-getQueryEndCut()) < abs(A.getSubjectStartCut()-getSubjectEndCut())){
						numSubstitution = abs(A.getQueryStartCut()-getQueryEndCut());
					} else {
						numSubstitution = abs(A.getSubjectStartCut()-getSubjectEndCut());
					}
			}
		}
		else {
			switch (A.getPolarite()){
				case false : 
					if (abs(getQueryEnd() - A.getQueryStart()) < abs(A.getSubjectStart()-getSubjectEnd())){
						numSubstitution = abs(getQueryEnd() - A.getQueryStart());
					} else {
						numSubstitution = abs(A.getSubjectStart()-getSubjectEnd());
					}
					break;
				case true :
					if (abs(A.getQueryStart()-getQueryEnd()) < abs(A.getSubjectStart()-getSubjectEnd())){
						numSubstitution = abs(A.getQueryStart()-getQueryEnd());
					} else {
						numSubstitution = abs(A.getSubjectStart()-getSubjectEnd());
					}
			}
		}
		return numSubstitution;
	}
	int Alignement::getDistQuery(const Alignement& A, bool cut, bool middle) const {
		if (middle) return A.getQueryMiddle() - getQueryMiddle();
		
		int thisAlign = 0;
		int nextAlign = 0;
		
		if (cut){
			thisAlign = getQueryMaxCut();
			nextAlign = A.getQueryMinCut();
		}
		else {
			thisAlign = getQueryMax();
			nextAlign = A.getQueryMin();
		}
		
		return (nextAlign-thisAlign);
	}
	int Alignement::getDistSubject(const Alignement& A, bool cut, bool middle) const {
		if (middle) return A.getSubjectMiddle() - getSubjectMiddle();
		
		int thisAlign = 0;
		int nextAlign = 0;
		
		if (cut){
			thisAlign = getSubjectMaxCut();
			nextAlign = A.getSubjectMinCut();
		}
		else {
			thisAlign = getSubjectMax();
			nextAlign = A.getSubjectMin();
		}
		
		return (nextAlign-thisAlign);
	}
	
	bool Alignement::overlap (const Alignement& A, bool cut) const {
	
		if (sameQueryChrom(A) && sameSubjectChrom(A)) {
			if (pOverlapQuery (A, cut)>0 && pOverlapSubject (A, cut)>0) return true;
		}
		
		return false;
	}
	
	bool Alignement::alignOverlap () const {
			if (q_chrom!=s_chrom) return false;
			if (getSubjectMin()<=getQueryMin() && getQueryMin()<=getSubjectMax()) return true;
			if (getSubjectMin()<=getQueryMax() && getQueryMax()<=getSubjectMax()) return true;
			if (getQueryMin()<=getSubjectMin() && getSubjectMin()<=getQueryMax()) return true;
			if (getQueryMin()<=getSubjectMax() && getSubjectMax()<=getQueryMax()) return true;
			
			return false;
		}
		
	
	bool Alignement::overlapSubject (const Alignement& A, bool cut) const {
		
		if (pOverlapSubject(A,cut) > 0) return true;
		
		return false; 
	}
	bool Alignement::overlapQuery (const Alignement& A, bool cut) const {
		
		if (pOverlapQuery(A,cut) > 0) return true;
		
		return false; 
	}
	bool Alignement::longest (const Alignement& A) const {
		int sizeThis = getQueryMax() - getQueryMin() + getSubjectMax() - getSubjectMin();
		int sizeOther = A.getQueryMax() - A.getQueryMin() + A.getSubjectMax() - A.getSubjectMin();
		
		return (sizeThis>=sizeOther);
	}
	
	double Alignement::weight(const Alignement& A, int type, bool cut, double gapUnitLen, bool middle, bool bonusSlope, double gapExtend) const {
		
		double weight = -1;
		if (type==0){
			weight = distManhattan(A, cut, middle);
		} else if (type == 1){
			weight = 30;
		} else if (type == 2){
			weight = distDAGchainer(A, cut, gapUnitLen, middle);
		} else if (type == 3){
			weight = distDiag(A, cut, middle);
		}
		
		if (bonusSlope){
			double fac=0;
			double pente = getSlope(A);
			
			if (pente==-1) return -1;
			
			if (getPolarite()==A.getPolarite()){
				fac=pente;
			} else {
				fac=(1-pente);
			}	
			
			if(fac<0.2) fac=0.2;
			fac = -fac/0.8 + 1.8/0.8;
			weight = (weight)*fac;
			
			
		}
		
		return weight*gapExtend;
	}
	
	double Alignement::getSlope() const	{
					
		if ( ((double)(getQueryMax()-getQueryMin())) / (getSubjectMax()-getSubjectMin()) <= 1 )	
			return ((double)(getQueryMax()-getQueryMin())) / (getSubjectMax()-getSubjectMin());
		return ((double)(getSubjectMax()-getSubjectMin())) / (getQueryMax()-getQueryMin());
	}
	
	bool Alignement::consitencySlope (const Alignement& A, bool cut) const {
		
		
		int id_qMax = 1;
		int id_qMin = 1;
		int id_sMax = 1;
		int id_sMin = 1;
		
		int queryMax = 0;
		int queryMin = 0;
		int subjectMax = 0;
		int subjectMin = 0;
		
		
		
		
		if (cut) {
			queryMax = A.getQueryMaxCut();
			queryMin = A.getQueryMinCut();
			subjectMax = A.getSubjectMaxCut();
			subjectMin = A.getSubjectMinCut();
			
			if (queryMax<getQueryMaxCut()){
				queryMax = getQueryMaxCut();
				id_qMax = 2;
			}
			if (queryMin>getQueryMinCut()){
				queryMin = getQueryMinCut();
				id_qMin = 2;
			}
			
			if (subjectMax<getSubjectMaxCut()){
				subjectMax = getSubjectMaxCut();
				id_sMax = 2;
			}
			if (subjectMin>getSubjectMinCut()){
				subjectMin = getSubjectMinCut();
				id_sMin = 2;
			}
		} else {
			queryMax = A.getQueryMax();
			queryMin = A.getQueryMin();
			subjectMax = A.getSubjectMax();
			subjectMin = A.getSubjectMin();
			
			if (queryMax<getQueryMax()){
				queryMax = getQueryMax();
				id_qMax = 2;
			}
			if (queryMin>getQueryMin()){
				queryMin = getQueryMin();
				id_qMin = 2;
			}
			
			if (subjectMax<getSubjectMax()){
				subjectMax = getSubjectMax();
				id_sMax = 2;
			}
			if (subjectMin>getSubjectMin()){
				subjectMin = getSubjectMin();
				id_sMin = 2;
			}
		}
		
		if (A.getQueryMax() == getQueryMax() && A.getQueryMin() == getQueryMin()){
			id_qMax = 1;
			id_qMin = 2;
		}
		
		if (A.getSubjectMax() == getSubjectMax() && A.getSubjectMin() == getSubjectMin()){
			id_sMax = 1;
			id_sMin = 2;
		}
		
		
		if (id_qMax==id_qMin) return false;
		if (id_sMax==id_sMin) return false;
		
		
		return true;
		
	}
		
	
	double Alignement::getSlope(const Alignement& A) const	{
		int queryMax = A.getQueryMax();
		int queryMin = A.getQueryMin();
		int subjectMax = A.getSubjectMax();
		int subjectMin = A.getSubjectMin();
		
		if (queryMax<getQueryMax()){
			queryMax = getQueryMax();
		}
		if (queryMin>getQueryMin()){
			queryMin = getQueryMin();
		}
		
		if (subjectMax<getSubjectMax()){
			subjectMax = getSubjectMax();
		}
		if (subjectMin>getSubjectMin()){
			subjectMin = getSubjectMin();
		}
				
		if ( ((double)(queryMax-queryMin)) / (subjectMax-subjectMin) <= 1 )	
			return ((double)(queryMax-queryMin)) / (subjectMax-subjectMin);
		return ((double)(subjectMax-subjectMin)) / (queryMax-queryMin);
	}	
		
	
	double Alignement::distManhattan(const Alignement& A, bool cut, bool middle) const {
		if(middle) {
			return (A.getQueryMiddle() - getQueryMiddle()) + (A.getSubjectMiddle() - getSubjectMiddle());
		}
		
		if (cut) {
			switch (A.getPolarite()){
				case false :return abs(getQueryEndCut() - A.getQueryStartCut()) + abs(A.getSubjectStartCut() - getSubjectEndCut());
				case true :	return abs(A.getQueryStartCut() - getQueryEndCut()) + abs(A.getSubjectStartCut() - getSubjectEndCut());
			}
		}
		else {
			switch (A.getPolarite()){
				case false :return abs(getQueryEnd() - A.getQueryStart()) + abs(A.getSubjectStart()-getSubjectEnd());
				case true :	return abs(A.getQueryStart()-getQueryEnd()) + abs(A.getSubjectStart()-getSubjectEnd());
			}
		}
		return -1;
	}
	double Alignement::distDAGchainer(const Alignement& A, bool cut, double gapUnitLen, bool middle) const {
		double un;
		double deux;
		
		if(middle) {
			un = abs((A.getQueryMiddle() - getQueryMiddle()-1)) + abs((A.getSubjectMiddle() - getSubjectMiddle()) -1);
			deux = abs( abs((A.getQueryMiddle() - getQueryMiddle()-1)) - abs((A.getSubjectMiddle() - getSubjectMiddle())-1) );
			return floor((((un + deux) / (2 * gapUnitLen))));
		}
		
		if (cut) {
			switch (A.getPolarite()){
				case false : 
					un = abs(getQueryEndCut() - A.getQueryStartCut()-1) + abs(A.getSubjectStartCut() - getSubjectEndCut()-1);
					deux = abs( abs(getQueryEndCut() - A.getQueryStartCut()-1) - abs(A.getSubjectStartCut() - getSubjectEndCut()-1) );
					return floor((((un + deux) / (2 * gapUnitLen))));
				case true :	
					un = abs(A.getQueryStartCut() - getQueryEndCut()-1) + abs(A.getSubjectStartCut() - getSubjectEndCut()-1);
					deux = abs( abs(A.getQueryStartCut() - getQueryEndCut()-1) - abs(A.getSubjectStartCut() - getSubjectEndCut()-1) );
					return floor((((un + deux) / (2 * gapUnitLen))));
			}
		}
		else {
			switch (A.getPolarite()){
				case false :
					un = abs(getQueryEnd() - A.getQueryStart()-1) + abs(A.getSubjectStart()-getSubjectEnd()-1);
					deux = abs( abs(getQueryEnd() - A.getQueryStart()-1) - abs(A.getSubjectStart()-getSubjectEnd()-1) );
					return floor((((un + deux) / (2 * gapUnitLen))));
				case true :	
					un = abs(A.getQueryStart()-getQueryEnd()-1) + abs(A.getSubjectStart()-getSubjectEnd()-1);
					deux = abs( abs(A.getQueryStart()-getQueryEnd()-1) - abs(A.getSubjectStart()-getSubjectEnd()-1) );
					return floor((((un + deux) / (2 * gapUnitLen))));
			}
		}
		return -1;
	}
	double Alignement::distDiag(const Alignement& A, bool cut, bool middle) const {
		double un;
		double deux;
		
		if(middle) {
			un = abs((A.getQueryMiddle() - getQueryMiddle()-1));
			deux = abs((A.getSubjectMiddle() - getSubjectMiddle())-1);
		}
		
		if (cut) {
			switch (A.getPolarite()){
				case false : 
					un = abs(getQueryEndCut() - A.getQueryStartCut()-1);
					deux = abs(A.getSubjectStartCut() - getSubjectEndCut()-1);
				case true :	
					un = abs(A.getQueryStartCut() - getQueryEndCut()-1);
					deux = abs(A.getSubjectStartCut() - getSubjectEndCut()-1);
			}
		}
		else {
			switch (A.getPolarite()){
				case false :
					un = abs(getQueryEnd() - A.getQueryStart()-1);
					deux = abs(A.getSubjectStart()-getSubjectEnd()-1);
				case true :	
					un = abs(A.getQueryStart()-getQueryEnd()-1);
					deux = abs(A.getSubjectStart()-getSubjectEnd()-1);
			}
		}
		
		if (un < deux) return deux * 2 - un;
		return un * 2 - deux;
	}
	
	double Alignement::costAlign(int type, double costMax, bool bonusOverlap) const {
		double cost = -1;
		
		if (type == 0){
			cost = -50;
		} else if (type == 1){
			cost = -50;
		} else if (type == 2){
			cost = -50;
		} else if (type == 3){
			cost = -1000;
		} else if (type == 4){
			cost = -score;
		} else if (type == 5){
			if (evalue==0)	cost = log10(1e-250);
			else			cost = log10(evalue);
		} else if (type == 6){
			if (evalue==0)	cost = log(1e-250);
			else			cost = log(evalue);
		} else return -1;
		
		if (type == 5 || type == 6 || type == 4) {
			if (costMax > cost)		cost = costMax;
		}
		
		if (bonusOverlap){
			cost = cost + cost * percAlignOverlap;
		}
		
		return cost;
		
	}
	
	double Alignement::coefDistEvol(int typeDistEvol, const Alignement& A) const{
		switch (typeDistEvol){
			case 0 : return 1 + fabs(distKimuraProt()-A.distKimuraProt()) ;
			default : return 1;
		}
	}
	
	double Alignement::pOverlapQuery (const Alignement& A, bool cut) const {
		
		double p = 0;
		
		if (A.getQueryChrom() == getQueryChrom()) {
				
			double start, end;
			
			if(cut) {
				(A.getQueryMinCut()<getQueryMinCut()) ? start = getQueryMinCut() : start = A.getQueryMinCut();
				(A.getQueryMaxCut()>getQueryMaxCut()) ? end = getQueryMaxCut() : end = A.getQueryMaxCut();
				if( (getQueryMaxCut()-getQueryMinCut())!=0 )	p = ((double)(end-start))/(getQueryMaxCut()-getQueryMinCut());
				else {
					cout << "1 div 0" << endl;
					affiche();
					A.affiche();
					return 0;
				}
			}
			else {
				(A.getQueryMin()<getQueryMin()) ? start = getQueryMin() : start = A.getQueryMin();
				(A.getQueryMax()>getQueryMax()) ? end = getQueryMax() : end = A.getQueryMax();
				if( (getQueryMax()-getQueryMin())!=0 )	p = ((double)(end-start))/(getQueryMax()-getQueryMin());
				else {
					cout << "1 div 0" << endl;
					affiche();
					A.affiche();
					return 0;
				}
			}
				
		}
		
		if (p < 0) p = 0;
		
		return p; 
	}
	
	double Alignement::pOverlapQuerySubject (const Alignement& A, bool cut) const {
		
		double p = 0;
		
		if (A.getSubjectChrom() == getQueryChrom()) {
				
			double start, end;
			
			if(cut) {
				(A.getSubjectMinCut()<getQueryMinCut()) ? start = getSubjectMinCut() : start = A.getQueryMinCut();
				(A.getSubjectMaxCut()>getQueryMaxCut()) ? end = getQueryMaxCut() : end = A.getSubjectMaxCut();
				if( (getQueryMaxCut()-getQueryMinCut())!=0 )	p = ((double)(end-start))/(getQueryMaxCut()-getQueryMinCut());
				else {
					cout << "pOverlapQuerySubject -> 1 div 0" << endl;
					affiche();
					A.affiche();
					return 0;
				}
			}
			else {
				(A.getSubjectMin()<getQueryMin()) ? start = getQueryMin() : start = A.getSubjectMin();
				(A.getSubjectMax()>getQueryMax()) ? end = getQueryMax() : end = A.getSubjectMax();
				if( (getQueryMax()-getQueryMin())!=0 )	p = ((double)(end-start))/(getQueryMax()-getQueryMin());
				else {
					cout << "pOverlapQuerySubject -> 2 div 0" << endl;
					affiche();
					A.affiche();
					return 0;
				}
			}
				
		}
		
		if (p < 0) p = 0;
		
		return p; 
	}
	
	double Alignement::pOverlapSubject (const Alignement& A, bool cut) const {
		
		double p = 0;
		
		if (A.getSubjectChrom() == getSubjectChrom()) {
			double start, end;
			if (!cut) {
				(A.getSubjectMin()<getSubjectMin()) ? start = getSubjectMin() : start = A.getSubjectMin();
				(A.getSubjectMax()>getSubjectMax()) ? end = getSubjectMax() : end = A.getSubjectMax();
				if (getSubjectMax()-getSubjectMin()!=0)	p = ((double)(end-start))/(getSubjectMax()-getSubjectMin());
				else {
					cout << "pOverlapSubject -> 1 div 0" << endl;
					affiche();
					A.affiche();
					return 0;
				}
			}
			else {
				(A.getSubjectMinCut()<getSubjectMinCut()) ? start = getSubjectMinCut() : start = A.getSubjectMinCut();
				(A.getSubjectMaxCut()>getSubjectMaxCut()) ? end = getSubjectMaxCut() : end = A.getSubjectMaxCut();
				if (getSubjectMaxCut()-getSubjectMinCut()!=0)	p = ((double)(end-start))/(getSubjectMaxCut()-getSubjectMinCut());
				else {
					cout << "pOverlapSubject -> 2 div 0" << endl;
					affiche();
					A.affiche();
					return 0;
				}
			}
		}
		
		if (p < 0) p = 0;
		
		return p; 
	}
	
	double Alignement::pOverlapSubjectQuery (const Alignement& A, bool cut) const {
		
		double p = 0;
		
		if (A.getSubjectChrom() == getSubjectChrom()) {
			double start, end;
			if (!cut) {
				(A.getQueryMin()<getSubjectMin()) ? start = getSubjectMin() : start = A.getQueryMin();
				(A.getQueryMax()>getSubjectMax()) ? end = getSubjectMax() : end = A.getQueryMax();
				if (getSubjectMax()-getSubjectMin()!=0)	p = ((double)(end-start))/(getSubjectMax()-getSubjectMin());
				else {
					cout << "pOverlapSubjectQuery -> 1 div 0" << endl;
					affiche();
					A.affiche();
					return 0;
				}
			}
			else {
				(A.getQueryMinCut()<getSubjectMinCut()) ? start = getSubjectMinCut() : start = A.getQueryMinCut();
				(A.getQueryMaxCut()>getSubjectMaxCut()) ? end = getSubjectMaxCut() : end = A.getQueryMaxCut();
				if (getSubjectMaxCut()-getSubjectMinCut()!=0)	p = ((double)(end-start))/(getSubjectMaxCut()-getSubjectMinCut());
				else {
					cout << "pOverlapSubjectQuery -> 2 div 0" << endl;
					affiche();
					A.affiche();
					return 0;
				}
			}
		}
		
		if (p < 0) p = 0;
		
		return p; 
	}
	
	void Alignement::addVertex(int in, int out){
		vertexIn = in;
		vertexOut = out;
	}
	
	int Alignement::updateChain (int alBefore, int alAfter, bool isStart, bool isEnd){
		
		bool begin = alreadyInChain();
		
		alignBefore = alBefore;
		alignAfter = alAfter;
		startChain = isStart;
		endChain = isEnd;
		
		bool end = alreadyInChain();
		
		if (!begin && !end) return 0;
		if (begin && end) return 0;
		if (begin && !end) return -1;
		if (!begin && end) return 1;
		return 0;
		
	}
		
	void Alignement::affiche() const {
		cout << "id: " << id <<"\t";
		
		if (!q_chrom.empty())	cout << "chrom : " << q_chrom <<"\t";
		if (q_start != -1)	cout << "q_s : " << q_start <<"\t";
		if (q_end != -1)	cout << "q_e : " << q_end <<"\t";
		if (s_start != -1)	cout << "s_s : " << s_start <<"\t";
		if (s_end != -1)	cout << "s_e : " << s_end <<"\t";
		if (score != -1)	cout << "sco : " << score <<"\t";
		if (pIdentity != -1) cout << "pIdentity : " << getPidentity() <<"\t";
		
		cout<<endl;
	}
	
	void Alignement::affiche(ofstream& file) const {
		
		cout << "affiche align" << endl; 
		if (!q_name.empty())	file << q_name <<"\t";
		else file << "pas de nom" <<"\t";
		if (!q_chrom.empty())	file << q_chrom <<"\t";
		if (!s_chrom.empty())	file << s_chrom <<"\t";
		if (q_start != -1)	file << q_start <<"\t";
		if (q_end != -1)	file << q_end <<"\t";
		if (s_start != -1)	file << s_start <<"\t";
		if (s_end != -1)	file << s_end <<"\t";
		file  << getPolarite() <<"\t";
		if (score != -1)	file << score <<"\t";
		if (evalue != -1)	file << evalue <<"\t";
		if (pIdentity != -1) file << getPidentity() <<"\t";
		
		file<<"\n";
	}
	
	void Alignement::printResNarcisse(ofstream& chain_file, int orderS, int orderQ, int* type, bool* booParam, double* param, bool use) const {
		char reverseP;
		reverseP = (getPolarite())? '+':'-';
			
		if (use) {
			chain_file << orderQ << "\t" << q_chrom << "\t" << getQueryMin() << 
				"\t" << getQueryMax() << "\t"<< reverseP << "\t" << s_chrom << 
				"\t" << getSubjectMin() << "\t" << getSubjectMax() << "\t" <<  orderS << 
				"\t" << -costAlign(type[1], param[1], booParam[10]) << "\t" << getOrderQuery() << ":";
		} else {
			 chain_file << orderQuery << "\t" << q_chrom << "\t" << getQueryMin() << 
				"\t" << getQueryMax() << "\t"<< reverseP << "\t" << s_chrom << 
				"\t" << getSubjectMin() << "\t" << getSubjectMax() << "\t" <<  orderSubject << 
				"\t" << -costAlign(type[1], param[1], booParam[10]) << "\t:";
		}
				
		if (type[6]==0) {	chain_file << "\t" << "dna" <<"\n"; }
		else {
			if (type[6]==1) {	chain_file << "\t" << "prot" <<"\n"; }
			else {
				chain_file << "\t" << "unknow" <<"\n";
			}
		}
	}
	void Alignement::printResNarcisse2(ofstream& chain_file, int orderS, int orderQ, int* type, bool* booParam, double* param, bool use) const {
		char reverseP;
		reverseP = (getPolarite())? '+':'-';
		
		if (use) {
			 chain_file << orderQ << "\t" << orderQ << "\t" << q_chrom << "\t" << getQueryMin() << 
				"\t" << getQueryMax() << "\t"<< reverseP << "\t" << s_chrom << "\t" << getSubjectMin() << 
				"\t" << getSubjectMax() << "\t" <<  orderS << "\t" << -costAlign(type[1], param[1], booParam[10]) << 
				"\t" << 1 << "\t" << getOrderQuery() << ":\t" << getOrderSubject() << ":";
		} else {
			 chain_file << orderQuery << "\t" << orderQuery << "\t" << q_chrom << "\t" << getQueryMin() << 
				"\t" << getQueryMax() << "\t"<< reverseP << "\t" << s_chrom << "\t" << getSubjectMin() << 
				"\t" << getSubjectMax() << "\t" <<  orderSubject << "\t" << -costAlign(type[1], param[1], booParam[10]) << 
				"\t" << 0 << "\t:\t:";
		}
				
		if (type[6]==0) {	chain_file << "\t" << "dna" <<"\n"; }
		else {
			if (type[6]==1) {	chain_file << "\t" << "prot" <<"\n"; }
			else {
				chain_file << "\t" << "unknow" <<"\n";
			}
		}
	}
	void Alignement::printRes(ofstream& chain_file, int orderS, int orderQ, int* type, bool* booParam, double* param, bool use) const {
		if (type[5]==1) {
			printResNarcisse2(chain_file, orderS, orderQ, type, booParam, param, use);
		}	
	}
	
	
	
	Alignement::~Alignement(){
	
	}
	
}
