#ifndef ALIGNEMENT_H_
#define ALIGNEMENT_H_

#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <fstream>
#include <math.h>

#include "../const.h"



using namespace std;

namespace data{
		
	class Alignement
	{
		int id;
		
		string q_name;
		string s_name;
		string q_chrom;
		string s_chrom;
		
		int q_start;
		int q_end;
		
		int s_start;
		int s_end;
		
		int s_max;
		
		int cutAlign;
		
		bool sign;
		bool bUseInChain;
		
		int score;
		int size;
		int nbMissMatch;
		int nbGap;
		
		double evalue;
		double pIdentity;
		
		int vertexIn;
		int vertexOut;
		
		int alignBefore;
		int alignAfter;
		bool startChain;
		bool endChain;
		
		bool strand; 
		bool reverse;
		
		vector<double> vecAlignOverlap;
		double percAlignOverlap;
		
		int idGraph;
		int idInGraph;
		int orderQuery;
		int orderSubject;
		
	public:
		Alignement();
		Alignement(string, vector<int>, int idAlign, int nbCutAlign, bool* booParam, string espece);
				
		inline int getId() const			{	return id;			} 
		inline int getIdInGraph() const			{	return idInGraph;		}
		inline int getIdGraph() const			{	return idGraph;			}
		inline int getOrderQuery() const		{	return orderQuery;		}
		inline int getOrderSubject() const		{	return orderSubject;		}
		
		inline bool getPolarite() const {
			if (getQueryStart()<getQueryEnd() && getSubjectStart()<getSubjectEnd()) return true;
			else return false;
		}
		
		inline int getQueryMiddle() const 		{
			return (double(q_start+q_end))/2;
		}
		inline int getSubjectMiddle() const 	{
			if (reverse) return (double(s_max-s_start+s_max-s_end))/2;
			return (double(s_start+s_end))/2;
		}
		inline int getQueryStart() const 		{	
			if (!strand)	return q_start;
			else return q_end;
		}
		inline int getQueryEnd() const 			{	
			if (!strand)	return q_end;
			else return q_start;
		}
		inline int getSubjectStart() const 		{
			if (reverse){
				if (!strand)	return s_max - s_start;
				else return s_max - s_end;
			}
			if (!strand)	return s_start;
			else return s_end;
		}
		inline int getSubjectEnd() const 		{	
			if (reverse){
				if (!strand)	return s_max - s_end;
				else return s_max - s_start;
			}
			if (!strand)	return s_end;
			else return s_start;
		}
		inline int getSubjectMin() const 		{	
			if (reverse){
				return  ( (s_max-s_start)<(s_max-s_end) )? (s_max-s_start):(s_max-s_end);
			}
			return (s_start<s_end)? s_start:s_end;
		}
		inline int getSubjectMinCut() const 		{	
			if (reverse){
				return  ( (s_max-getSubjectStartCut())<(s_max-getSubjectEndCut()) )? (s_max-getSubjectStartCut()):(s_max-getSubjectEndCut());
			}
			return (getSubjectStartCut()<getSubjectEndCut())? getSubjectStartCut():getSubjectEndCut();
		}
		inline int getQueryMin() const 		{	
			return (q_start<q_end)? q_start:q_end;
		}
		inline int getQueryMinCut() const 		{	
			return (getQueryStartCut()<getQueryEndCut())? getQueryStartCut():getQueryEndCut();
		}
		inline int getSubjectMax() const 		{	
			if (reverse){
				return  ( (s_max-s_start)>(s_max-s_end) )? (s_max-s_start):(s_max-s_end);
			}
			return (s_start>s_end)? s_start:s_end;
		}
		inline int getSubjectMaxCut() const 		{	
			if (reverse){
				return  ( (s_max-getSubjectStartCut())>(s_max-getSubjectEndCut()) )? (s_max-getSubjectStartCut()):(s_max-getSubjectEndCut());
			}
			return (getSubjectStartCut()>getSubjectEndCut())? getSubjectStartCut():getSubjectEndCut();
		}
		inline int getQueryMax() const 		{	
			return (q_start>q_end)? q_start:q_end;
		}
		inline int getQueryMaxCut() const 		{	
			return (getQueryStartCut()>getQueryEndCut())? getQueryStartCut():getQueryEndCut();
		}
		inline int getQueryStartCut() const 	{	
			if (!strand)	return q_start + cutAlign;
			else return q_end + cutAlign;
		}
		inline int getQueryEndCut() const 		{	
			if (!strand)	return q_end - cutAlign;
			else return q_start - cutAlign;
		}
		inline int getSubjectStartCut() const 	{
			if (reverse){
				if (!strand)	return s_max - s_start + cutAlign;
				else return s_max - s_end + cutAlign;
			}	
			if (!strand)	return s_start + cutAlign;
			else return s_end + cutAlign;
		}
		inline int getSubjectEndCut() const 	{
			if (reverse){	
				if (!strand)	return s_max - s_end - cutAlign;
				else return s_max - s_start - cutAlign;
			}	
			if (!strand)	return s_end - cutAlign;
			else return s_start - cutAlign;
		}
		
		inline int getLengthSubject() const 	{
			return getSubjectMax() - getSubjectMin() + 1;
		}
		inline int getLengthQuery() const 	{
			return getQueryMax() - getQueryMin() + 1;
		}
		
		inline void restartAlign(int maxY) {
			vertexIn = -1;
			vertexOut = -1;
			
			alignBefore = -1;
			alignAfter = -1;
			startChain = false;
			endChain = false;
			reverse = !reverse;
			s_max = maxY;
		}
		
		inline double getPercentOverlap() const {
			double bOverLap = 0;
			for (vector<double>::const_iterator it = vecAlignOverlap.begin(); it<vecAlignOverlap.end(); it++){
				bOverLap = bOverLap + size * (*it);
			}
			return bOverLap;
		}
		
		inline void setPercentOverlap(double pOverlap)	{	percAlignOverlap = pOverlap;			}
		
		inline void setReverse(bool bReverse)	{	reverse = bReverse;							}
		inline void setOrderQuery(int orderQ)	{	orderQuery = orderQ;						}
		inline void setOrderSubject(int orderS)	{	orderSubject = orderS;						}
		
		inline bool getReverse() const			{	return reverse;								}
		inline int getScore() const 			{	return score; 								}
		inline int getSize() const 				{	return size;								}
		inline int getDistDiag() const 			{	return abs(getQueryMin()-getSubjectMin());	}
		inline int getNbMissMatch() const		{	return nbMissMatch;							}
		inline int getNbGap() const				{	return nbGap;								}
		inline string getQueryChrom() const		{	return q_chrom;								}
		inline string getSubjectChrom() const	{	return s_chrom;								}
		inline string getQueryName() const		{	return q_name;								}
		inline string getSubjectName() const	{	return s_name;								}
		inline double getEvalue() const			{	return evalue;								}
		inline double getPidentity() const		{	return pIdentity;							}
		inline void setIdInGraph(int idInG)		{	idInGraph = idInG;		}
		inline void setIdGraph(int idG)			{	idGraph = idG;			}
				
		inline bool isEndChain() const 			{ 	return endChain;		}
		inline bool isStartChain() const 		{	return startChain;		}
		inline int getAlignBefore() const 		{	return alignBefore;		}
		inline int getAlignAfter() const 		{	return alignAfter;		}
		inline bool alreadyInChain() const 		{	return !(alignBefore == -1 && alignAfter == -1);				}
		
		inline bool sameQueryChrom (const Alignement& A) const 		{	return (A.getQueryChrom()== q_chrom);		}
		inline bool sameSubjectChrom (const Alignement& A) const 	{	return (A.getSubjectChrom() == s_chrom);	}
		
		inline bool useInChain () const 		{	return bUseInChain;	}
		inline void setUseInChain (bool use)  		{	bUseInChain = use;	}
		

		inline void addAlignOverlap (double pOverlap) 				{	vecAlignOverlap.push_back(pOverlap);		}
		
		inline bool beforeQuery (const Alignement& A, bool cut) const	{
			if (cut)	return (A.getQueryStartCut()>getQueryEndCut());
			else 		return (A.getQueryStart()>getQueryEnd());
		}
		inline bool afterQuery (const Alignement& A, bool cut) const 	{
			if (cut)	return (A.getQueryStartCut()<getQueryEndCut());
			else		return (A.getQueryStart()<getQueryEnd());
		}
		inline bool beforeSubject (const Alignement& A, bool cut) const	{
			if (cut)	return (A.getSubjectStartCut()>getSubjectEndCut());
			else		return (A.getSubjectStart()>getSubjectEnd());
		}
		inline bool afterSubject (const Alignement& A, bool cut) const	{
			if (cut)	return (A.getSubjectStartCut()<getSubjectEndCut());
			else		return (A.getSubjectStart()<getSubjectEnd());
		}
		inline double distKimuraProt () const {
			return -log(1 - pIdentity/100 - 0.2*pIdentity/100*pIdentity/100);
		}
		
		bool overlapSeq (const Alignement& A, bool cut) const;
		bool consitencySlope (const Alignement& A, bool cut) const;
		bool pOverlapSeq (const Alignement& A, bool cut) const;
		
		int getNumDeletion(const Alignement& A, bool cut, int numSubstitution, bool middle) const;
		int getNumSubstitution(const Alignement& A, bool cut, bool middle) const;
		int getDistQuery(const Alignement& A, bool cut, bool middle) const;
		int getDistSubject(const Alignement& A, bool cut, bool middle) const;
		
		bool overlap (const Alignement&, bool cut) const;
		bool overlapSubject (const Alignement&, bool cut) const;
		bool overlapQuery (const Alignement&, bool cut) const;
		bool alignOverlap () const;
		
		bool longest (const Alignement&) const;
		
		double getSlope() const;
		double getSlope(const Alignement& A) const;	
		
		
		double weight(const Alignement& A, int typeDist, bool cut, double gapUnitLen, bool middle, bool bonusSlope, double gapExtend) const;
		double distManhattan(const Alignement& A, bool cut, bool middle) const ;
		double distDAGchainer(const Alignement& A, bool cut, double gapUnitLen, bool middle) const ;
		double distDiag(const Alignement& A, bool cut, bool middle) const ;
		
		double costAlign(int type, double costMax, bool bonusOverlap) const ;
		double coefDistEvol(int typeDistEvol, const Alignement& A) const;
		
		double pOverlapQuery (const Alignement&, bool cut) const;
		double pOverlapSubject (const Alignement&, bool cut) const;
		double pOverlapQuerySubject (const Alignement&, bool cut) const;
		double pOverlapSubjectQuery (const Alignement&, bool cut) const;
		
		void addVertex(int, int);
		
		int updateChain (int alBefore, int alAfter, bool isStart, bool isEnd);
		
		void affiche() const;
		void affiche(ofstream& file) const ;
		
		void printResNarcisse(ofstream& chain_file, int orderS, int orderQ, int* type, bool* booParam, double* param, bool use) const;
		void printResNarcisse2(ofstream& chain_file, int orderS, int orderQ, int* type, bool* booParam, double* param, bool use) const;
		void printRes(ofstream& chain_file, int orderS, int orderQ, int* type, bool* booParam, double* param, bool use) const;
		
		virtual ~Alignement();
	};
	
}

#endif /*ALIGNEMENT_H_*/
