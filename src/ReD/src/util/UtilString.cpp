#include "UtilString.h"

namespace util{

	vector<string> decoupe(string text){
		
		vector<string> v;
		
		string::size_type iBegin = 0;
		string::size_type iEnd = text.find('\t',iBegin);
		while (iEnd != string::npos){
			v.push_back(text.substr(iBegin,iEnd));
			iBegin = iEnd;
			iEnd = text.find('\t',iBegin);
		}
				
		return v;
	}
	
}
