#ifndef UTILALIGN_H_
#define UTILALIGN_H_

#include "../data/Alignement.h"
#include <vector>

using namespace data;

namespace util {
	
	inline bool compareQueryAlign(const Alignement* A1, const Alignement* A2) {
		int A1_qChrom = -1;
		int A1_sChrom = -1;
		int A2_qChrom = -1;
		int A2_sChrom = -1;

		try {
        		A1_qChrom = boost::lexical_cast<int>(A1->getQueryChrom());
		} catch(boost::bad_lexical_cast &) { 
			if (A1->getQueryChrom().compare("X") == 0) A1_qChrom = 50;
			if (A1->getQueryChrom().compare("Y") == 0) A1_qChrom = 51;
			if (A1->getQueryChrom().compare("MT") == 0) A1_qChrom = 52;
			
			if (A1_qChrom == -1) A1_qChrom = 100;
		}

		try {
			A1_sChrom = boost::lexical_cast<int>(A1->getSubjectChrom());
		} catch(boost::bad_lexical_cast &) { 
			if (A1->getSubjectChrom().compare("X") == 0) A1_sChrom = 50;
			if (A1->getSubjectChrom().compare("Y") == 0) A1_sChrom = 51;
			if (A1->getSubjectChrom().compare("MT") == 0) A1_sChrom = 52;
			
			if (A1_sChrom == -1) A1_sChrom = 100;
		}
		
		try {
			A2_qChrom = boost::lexical_cast<int>(A2->getQueryChrom());
		} catch(boost::bad_lexical_cast &) {
			if (A2->getQueryChrom().compare("X") == 0) A2_qChrom = 50;
			if (A2->getQueryChrom().compare("Y") == 0) A2_qChrom = 51;
			if (A2->getQueryChrom().compare("MT") == 0) A2_qChrom = 52;
			
			if (A2_qChrom == -1) A2_qChrom = 100;
		}
		
		try {
			A2_sChrom = boost::lexical_cast<int>(A2->getSubjectChrom());
		} catch(boost::bad_lexical_cast &) {
			if (A2->getSubjectChrom().compare("X") == 0) A2_sChrom = 50;
			if (A2->getSubjectChrom().compare("Y") == 0) A2_sChrom = 51;
			if (A2->getSubjectChrom().compare("MT") == 0) A2_sChrom = 52; 
			
			if (A2_sChrom == -1) A1_sChrom = 100;
		}
		
		if ( A1_qChrom < A2_qChrom )
			return true;
		if ( A1_qChrom > A2_qChrom )
			return false;
		
		if ( A1_sChrom < A2_sChrom )
			return true;
		if ( A1_sChrom > A2_sChrom )
			return false;
      
	
		if (A1->getQueryMin() > A2->getQueryMin())
			return false;
		if (A1->getQueryMin() < A2->getQueryMin())
			return true;
		
		if (A1->getSubjectMin() > A2->getSubjectMin())
			return false;
		if (A1->getSubjectMin() < A2->getSubjectMin())
			return true;	
	
		return true;
	}
	
	inline bool compareQueryMaxAlign(const Alignement* A1, const Alignement* A2) {
				
		if (A1->getQueryMax() > A2->getQueryMax())
			return true;
		if (A1->getQueryMax() < A2->getQueryMax())
			return false;
			
		if (A1->getSubjectMin() > A2->getSubjectMin())
			return false;
		if (A1->getSubjectMin() < A2->getSubjectMin())
			return true;	
		
		return true;
	}
	
	inline bool compareSubjectAlign(const Alignement* A1, const Alignement* A2) {
		int A1_qChrom = -1;
		int A1_sChrom = -1;
		int A2_qChrom = -1;
		int A2_sChrom = -1;
		
		try {
        	A1_qChrom = boost::lexical_cast<int>(A1->getQueryChrom());
        } catch(boost::bad_lexical_cast &) { 
        	if (A1->getQueryChrom().compare("X") == 0) A1_qChrom = 50;
        	if (A1->getQueryChrom().compare("Y") == 0) A1_qChrom = 51;
        	if (A1->getQueryChrom().compare("MT") == 0) A1_qChrom = 52;
        	
        	if (A1_qChrom == -1) A1_qChrom = 100;
        }
        
        try {
        	A1_sChrom = boost::lexical_cast<int>(A1->getSubjectChrom());
        } catch(boost::bad_lexical_cast &) { 
        	if (A1->getSubjectChrom().compare("X") == 0) A1_sChrom = 50;
        	if (A1->getSubjectChrom().compare("Y") == 0) A1_sChrom = 51;
        	if (A1->getSubjectChrom().compare("MT") == 0) A1_sChrom = 52;
        	
        	if (A1_sChrom == -1) A1_sChrom = 100;
        }
        
        try {
        	A2_qChrom = boost::lexical_cast<int>(A2->getQueryChrom());
        } catch(boost::bad_lexical_cast &) {
        	if (A2->getQueryChrom().compare("X") == 0) A2_qChrom = 50;
        	if (A2->getQueryChrom().compare("Y") == 0) A2_qChrom = 51;
        	if (A2->getQueryChrom().compare("MT") == 0) A2_qChrom = 52;
        	
        	if (A2_qChrom == -1) A2_qChrom = 100;
        }
        
        try {
        	A2_sChrom = boost::lexical_cast<int>(A2->getSubjectChrom());
        } catch(boost::bad_lexical_cast &) {
        	if (A2->getSubjectChrom().compare("X") == 0) A2_sChrom = 50;
        	if (A2->getSubjectChrom().compare("Y") == 0) A2_sChrom = 51;
        	if (A2->getSubjectChrom().compare("MT") == 0) A2_sChrom = 52; 
        	
        	if (A2_sChrom == -1) A1_sChrom = 100;
        }
        
        if ( A1_sChrom < A2_sChrom )
			return true;
		if ( A1_sChrom > A2_sChrom )
			return false;
      	
      	if ( A1_qChrom < A2_qChrom )
			return true;
		if ( A1_qChrom > A2_qChrom )
			return false;
			
				
		if (A1->getSubjectMin() > A2->getSubjectMin())
			return false;
		if (A1->getSubjectMin() < A2->getSubjectMin())
			return true;
			
		if (A1->getQueryMin() > A2->getQueryMin())
			return false;
		if (A1->getQueryMin() < A2->getQueryMin())
			return true;
			
		return true;
	}
	
	inline bool compareScoreAlign(const Alignement* A1, const Alignement* A2) {
		if (A1->getScore() > A2->getScore())
			return false;
		if (A1->getScore() < A2->getScore())
			return true;
			
		if (A1->getId() < A2->getId())
			return true;
			
		return false;
	}
	
	inline bool compareOrderQueryAlign(const Alignement* A1, const Alignement* A2) {
		if (A1->getOrderQuery() > A2->getOrderQuery())
			return false;
		return true;
	}
	
	inline bool compareOrderSubjectAlign(const Alignement* A1, const Alignement* A2) {
		if (A1->getOrderSubject() > A2->getOrderSubject())
			return false;
		return true;
	}
	
	double computeCoefCorrelation(vector<Alignement*>::const_iterator Abegin, vector<Alignement*>::const_iterator Aend);
	
}

#endif /*UTILALIGN_H_*/
