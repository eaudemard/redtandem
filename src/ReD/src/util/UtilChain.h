#ifndef UTILCHAIN_H_
#define UTILCHAIN_H_

#include "../graph/Chain.h"
#include <vector>

using namespace graphe;

namespace util {
	
	inline bool compareQueryChain(const Chain* C1, const Chain* C2) {
		int C1_qChrom = -1;
		int C1_sChrom = -1;
		int C2_qChrom = -1;
		int C2_sChrom = -1;
		
		try {
        	C1_qChrom = boost::lexical_cast<int>(C1->getQueryChrom());
        } catch(boost::bad_lexical_cast &) { 
        	if (C1->getQueryChrom().compare("X") == 0) C1_qChrom = 50;
        	if (C1->getQueryChrom().compare("Y") == 0) C1_qChrom = 51;
        	if (C1->getQueryChrom().compare("MT") == 0) C1_qChrom = 52;
        	
        	if (C1_qChrom == -1) C1_qChrom = 100;
        }
        
        try {
        	C1_sChrom = boost::lexical_cast<int>(C1->getSubjectChrom());
        } catch(boost::bad_lexical_cast &) { 
        	if (C1->getSubjectChrom().compare("X") == 0) C1_sChrom = 50;
        	if (C1->getSubjectChrom().compare("Y") == 0) C1_sChrom = 51;
        	if (C1->getSubjectChrom().compare("MT") == 0) C1_sChrom = 52;
        	
        	if (C1_sChrom == -1) C1_sChrom = 100;
        }
        
        try {
        	C2_qChrom = boost::lexical_cast<int>(C2->getQueryChrom());
        } catch(boost::bad_lexical_cast &) {
        	if (C2->getQueryChrom().compare("X") == 0) C2_qChrom = 50;
        	if (C2->getQueryChrom().compare("Y") == 0) C2_qChrom = 51;
        	if (C2->getQueryChrom().compare("MT") == 0) C2_qChrom = 52;
        	
        	if (C2_qChrom == -1) C2_qChrom = 100;
        }
        
        try {
        	C2_sChrom = boost::lexical_cast<int>(C2->getSubjectChrom());
        } catch(boost::bad_lexical_cast &) {
        	if (C2->getSubjectChrom().compare("X") == 0) C2_sChrom = 50;
        	if (C2->getSubjectChrom().compare("Y") == 0) C2_sChrom = 51;
        	if (C2->getSubjectChrom().compare("MT") == 0) C2_sChrom = 52; 
        	
        	if (C2_sChrom == -1) C2_sChrom = 100;
        }
        
        if ( C1_qChrom < C2_qChrom )
			return true;
		if ( C1_qChrom > C2_qChrom )
			return false;
			
		if ( C1_sChrom < C2_sChrom )
			return true;
		if ( C1_sChrom > C2_sChrom )
			return false;
      
		
		if (C1->getQueryStart() > C2->getQueryStart())
			return false;
		if (C1->getQueryStart() < C2->getQueryStart())
			return true;	
			
		if (C1->getSubjectStart() > C2->getSubjectStart())
			return false;
		if (C1->getSubjectStart() < C2->getSubjectStart())
			return true;
			
		return true;
	}
	
	inline bool compareSubjectChain(const Chain* C1, const Chain* C2) {
		int C1_qChrom = -1;
		int C1_sChrom = -1;
		int C2_qChrom = -1;
		int C2_sChrom = -1;
		
		try {
        	C1_qChrom = boost::lexical_cast<int>(C1->getQueryChrom());
        } catch(boost::bad_lexical_cast &) { 
        	if (C1->getQueryChrom().compare("X") == 0) C1_qChrom = 50;
        	if (C1->getQueryChrom().compare("Y") == 0) C1_qChrom = 51;
        	if (C1->getQueryChrom().compare("MT") == 0) C1_qChrom = 52;
        	
        	if (C1_qChrom == -1) C1_qChrom = 100;
        }
        
        try {
        	C1_sChrom = boost::lexical_cast<int>(C1->getSubjectChrom());
        } catch(boost::bad_lexical_cast &) { 
        	if (C1->getSubjectChrom().compare("X") == 0) C1_sChrom = 50;
        	if (C1->getSubjectChrom().compare("Y") == 0) C1_sChrom = 51;
        	if (C1->getSubjectChrom().compare("MT") == 0) C1_sChrom = 52;
        	
        	if (C1_sChrom == -1) C1_sChrom = 100;
        }
        
        try {
        	C2_qChrom = boost::lexical_cast<int>(C2->getQueryChrom());
        } catch(boost::bad_lexical_cast &) {
        	if (C2->getQueryChrom().compare("X") == 0) C2_qChrom = 50;
        	if (C2->getQueryChrom().compare("Y") == 0) C2_qChrom = 51;
        	if (C2->getQueryChrom().compare("MT") == 0) C2_qChrom = 52;
        	
        	if (C2_qChrom == -1) C2_qChrom = 100;
        }
        
        try {
        	C2_sChrom = boost::lexical_cast<int>(C2->getSubjectChrom());
        } catch(boost::bad_lexical_cast &) {
        	if (C2->getSubjectChrom().compare("X") == 0) C2_sChrom = 50;
        	if (C2->getSubjectChrom().compare("Y") == 0) C2_sChrom = 51;
        	if (C2->getSubjectChrom().compare("MT") == 0) C2_sChrom = 52; 
        	
        	if (C2_sChrom == -1) C2_sChrom = 100;
        }
        
        if ( C1_sChrom < C2_sChrom )
			return true;
		if ( C1_sChrom > C2_sChrom )
			return false;
      	
      	if ( C1_qChrom < C2_qChrom )
			return true;
		if ( C1_qChrom > C2_qChrom )
			return false;
				
		
		if (C1->getSubjectStart() > C2->getSubjectStart())
			return false;
		if (C1->getSubjectStart() < C2->getSubjectStart())
			return true;
		
		if (C1->getQueryStart() > C2->getQueryStart())
			return false;
		if (C1->getQueryStart() < C2->getQueryStart())
			return true;
			
		return true;
	}
	
		
	inline bool compareOrderQueryChain(const Chain* C1, const Chain* C2) {
		if (C1->getOrderQuery() > C2->getOrderQuery())
			return false;
		if (C1->getOrderQuery() < C2->getOrderQuery())
			return true;	
				
		return true;
	}
}

#endif /*UTILCHAIN_H_*/
