#include "UtilAlign.h"

namespace util{

	double computeCoefCorrelation(vector<Alignement*>::const_iterator Abegin, vector<Alignement*>::const_iterator Aend) {
		long double moyQuery = 0.0;
		long double moySubject = 0.0;
		double cpt = 0.0;
				
		for(vector<Alignement*>::const_iterator it = Abegin ;  it!=Aend; ++it){
			moyQuery += ((double)(*it)->getQueryMiddle());
			moySubject += ((double)(*it)->getSubjectMiddle());
			++cpt;
		}
		
		
		moyQuery = moyQuery / cpt;
		moySubject = moySubject / cpt;
		
		double coefCorrelation = 0.0;
		long double un = 0.0;
		long double deux = 0.0;
		long double trois = 0.0;
		
		for(vector<Alignement*>::const_iterator it = Abegin ;  it!=Aend; ++it){
			un += (((double)((*it)->getQueryMiddle()))-moyQuery) * (((double)((*it)->getSubjectMiddle()))-moySubject);
			deux += (((double)((*it)->getQueryMiddle()))-moyQuery)*(((double)((*it)->getQueryMiddle()))-moyQuery);
			trois += (((double)((*it)->getSubjectMiddle()))-moySubject)*(((double)((*it)->getSubjectMiddle()))-moySubject);	
		}
		
		if (un==0){
			return 1;	
		}
		if (deux==0 || trois==0){
			return 0;	
		}
		
		coefCorrelation = un / (sqrt(deux)*sqrt(trois));
		return coefCorrelation;
	}
	
	
}
