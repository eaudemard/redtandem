
#include "data/Alignement.h"
#include "graph/DirectedGraph.h"
#include "graph/AbstractGraph.h"
#include "graph/UndirectedGraph.h"
#include "util/UtilChain.h"
#include "./typeExec.cpp"
#include <stdlib.h> 
#include <cassert>
#include <getopt.h>

//=============================================================
//lib boost
#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/bellman_ford_shortest_paths.hpp>
#include <boost/array.hpp>
//=============================================================

#include <vector>
#include <valarray>
#include <map>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <limits>

using namespace boost;
using namespace data;
using namespace graphe;

bool checkAlign(vector<Alignement*>* vecAlign, Alignement& align, ofstream& file_overlap, bool booPrint){
	bool boo = true;
	vector<Alignement*>::iterator it = vecAlign->begin();
	while (boo==true && it != vecAlign->end()){
	
		if ((*it)->overlap(align, true)){
			if (booPrint){
					align.affiche(file_overlap);
					(*it)->affiche(file_overlap);
					file_overlap << "\n";
			}
			if ((*it)->longest(align)) {
				return false;	
			}
			else {
				delete *it;
				vecAlign->erase(it);
				it = vecAlign->begin();
			}
		} else {
			++it;
		}	
	}
	return true;
}

void checkAlign2(vector<Alignement*>* vecAlign){
	vector<Alignement*>::iterator itSave = vecAlign->end();
	vector<Alignement*>::iterator it2 = vecAlign->end();
	double qOverlap = 0;
	double sOverlap = 0;
	int cptContient = 0;
	
	map<int, int> mapAnchorDelete;
	
	map<string, vector<Alignement*>::iterator> mapIt;
	mapIt.insert(pair<string, vector<Alignement*>::iterator>( (*(vecAlign->begin()))->getQueryChrom()+(*(vecAlign->begin()))->getSubjectChrom(), vecAlign->begin()));
	
	for (vector<Alignement*>::iterator it1 = vecAlign->begin(); it1 != vecAlign->end(); ++it1) {
	
		if (itSave == vecAlign->end()) itSave = it1;
		else {
			if 	( (*it1)->getQueryChrom().compare((*itSave)->getQueryChrom()) != 0 || (*it1)->getSubjectChrom().compare((*itSave)->getSubjectChrom()) ) {
				mapIt.insert(pair<string, vector<Alignement*>::iterator>((*it1)->getQueryChrom()+(*it1)->getSubjectChrom(), it1));
			}
			itSave = it1;
		}
		
	}
	
	cout << "mapIt finish" << endl;
	
	for (vector<Alignement*>::iterator it1 = vecAlign->begin(); it1 != vecAlign->end(); ++it1) {
		
		cptContient = 0;
		it2 = mapIt.find((*it1)->getQueryChrom()+(*it1)->getSubjectChrom())->second;
		
		for (; it2 != vecAlign->end(); ++it2) {
			
			if (it1!=it2 && (*it1)->getQueryChrom().compare((*it2)->getQueryChrom()) == 0 && (*it1)->getSubjectChrom().compare((*it2)->getSubjectChrom()) == 0) {
				
				qOverlap = (*it2)->pOverlapQuery(*(*it1), true);
				sOverlap = (*it2)->pOverlapSubject(*(*it1), true);
				
				if (qOverlap>0.999 && sOverlap>0.999) ++cptContient;
				
				if (cptContient>1) break;
				
			} else {
				if (it1!=it2) break;
			}
		
		} 
		
		if (cptContient>1) {
			mapAnchorDelete.insert( pair<int, int>((*it1)->getId(), (*it1)->getId()) );
		}
		
	} 
	
	cout << "number anchor : " << vecAlign->size() << endl;
	cout << "number anchor to long : " << mapAnchorDelete.size() << endl;
	
	for (vector<Alignement*>::iterator it1 = vecAlign->begin(); it1 != vecAlign->end(); ) {
		if ( mapAnchorDelete.find((*it1)->getId()) != mapAnchorDelete.end() ){
			delete *it1;
			it1 = vecAlign->erase(it1);
		} else {
			++it1;
		}
	}
	
	cout << "number anchor : " << vecAlign->size() << endl;
	
	mapIt.clear();
	
	for (vector<Alignement*>::iterator it1 = vecAlign->begin(); it1 != vecAlign->end(); ++it1) {
		
		it2 = it1;
		++it2;
		while (it2 != vecAlign->end()) {
			
			if ((*it1)->getQueryChrom().compare((*it2)->getQueryChrom()) == 0 && (*it1)->getSubjectChrom().compare((*it2)->getSubjectChrom()) == 0) {
				
				qOverlap = (*it2)->pOverlapQuery(*(*it1), true);
				sOverlap = (*it2)->pOverlapSubject(*(*it1), true);
				
				if (qOverlap>0.01 && sOverlap>0.01){
					int dist = std::distance(it1,it2);
					if ((*it1)->longest(*(*it2))) {
						delete *it2;
						it2 = vecAlign->erase(it2);
						it1 = it2 - dist;	
					} else {
						delete *it1;
						it1 = vecAlign->erase(it1);
						it2 = it1;
						++it2;
					}
				} else {
					++it2;	
				}
					
			} else {
				break;
			}
		} 
	} 
	cout << "checkAlign2 finish" << endl;
}


 int backAlign (vector<Alignement*>* vecAlign, ifstream& ifIn, int nbNucCut, ofstream& file_overlap, bool booPrint, bool verifOver, bool* booParam, int* type, double* param, string espece){
	
	string line; 
	vector<string> col;
	vector<int> indiceCol (19,-1);
	int idAlign = 0;
	Alignement* align;
	int maxY = 0;
	
	int cptAlignOverlap = 0;
	int cptTooFar = 0;
	int cptTooClose = 0;
			
	if (getline(ifIn, line)){
		if (type[TYPE_HEADER]==0){
			//q_name	s_name	pident	size	mismatch	gaps	q_start	q_end	s_start	s_end	evalue	score
			//#q_order	element	q_chrom	q_start	q_end	strand	s_chrom	s_start	s_end	s_order	score	size	q_chain	s_chain	mol
			split(col, line, is_any_of("\t"),algorithm::token_compress_off);
			for (int i=0; i<lexical_cast<int>(col.size()); i++){
				if (col.at(i) == "name"){ 
					indiceCol.at(0) = i;
				} else if (col.at(i) == "query"){ 
					indiceCol.at(1) = i;
				} else if (col.at(i) == "q_prot"){ 
					indiceCol.at(1) = i;
				} else if (col.at(i) == "q_name"){ 
					indiceCol.at(1) = i;
				} else if (col.at(i) == "q_chrom"){ 
					indiceCol.at(14) = i;
				} else if (col.at(i) == "q_name"){ 
					indiceCol.at(14) = i;
				} else if (col.at(i) == "subject"){ 
					indiceCol.at(2) = i;
				} else if (col.at(i) == "s_prot"){ 
					indiceCol.at(2) = i;
				} else if (col.at(i) == "s_name"){ 
					indiceCol.at(2) = i;
				} else if (col.at(i) == "s_chrom"){ 
					indiceCol.at(15) = i;
				} else if (col.at(i) == "s_name"){ 
					indiceCol.at(15) = i;
				} else if (col.at(i) == "q_start"){ 
					indiceCol.at(3) = i;
				} else if (col.at(i) == "qg_start"){ 
					indiceCol.at(3) = i;
				} else if (col.at(i) == "q. start"){ 
					indiceCol.at(3) = i;
				} else if (col.at(i) == "q..start"){ 
					indiceCol.at(3) = i;
				} else if (col.at(i) == "q_end"){ 
					indiceCol.at(4) = i;
				} else if (col.at(i) == "qg_end"){ 
					indiceCol.at(4) = i;
				} else if (col.at(i) == "q. end"){ 
					indiceCol.at(4) = i;
				} else if (col.at(i) == "q..end"){ 
					indiceCol.at(4) = i;
				} else if (col.at(i) == "s_start"){ 
					indiceCol.at(5) = i;
				} else if (col.at(i) == "sg_start"){ 
					indiceCol.at(5) = i;
				}  else if (col.at(i) == "s. start"){ 
					indiceCol.at(5) = i;
				} else if (col.at(i) == "s..start"){ 
					indiceCol.at(5) = i;
				} else if (col.at(i) == "s_end"){ 
					indiceCol.at(6) = i;
				} else if (col.at(i) == "sg_end"){ 
					indiceCol.at(6) = i;
				} else if (col.at(i) == "s. end"){ 
					indiceCol.at(6) = i;
				} else if (col.at(i) == "s..end"){ 
					indiceCol.at(6) = i;
				} else if (col.at(i) == "sign"){ 
					indiceCol.at(7) = i;
				} else if (col.at(i) == "score"){ 
					indiceCol.at(8) = i;
				} else if (col.at(i) == "bit score"){ 
					indiceCol.at(8) = i;
				} else if (col.at(i) == "bit.score"){ 
					indiceCol.at(8) = i;
				} else if (col.at(i) == "size"){ 
					indiceCol.at(9) = i;
				} else if (col.at(i) == "length"){ 
					indiceCol.at(9) = i;
				} else if (col.at(i) == "pIdentity"){ 
					indiceCol.at(10) = i;
				} else if (col.at(i) == "identity"){ 
					indiceCol.at(10) = i;
				} else if (col.at(i) == "pident"){ 
					indiceCol.at(10) = i;
				} else if (col.at(i) == "% identity alignment"){ 
					indiceCol.at(10) = i;
				} else if (col.at(i) == "X..identity.alignment"){ 
					indiceCol.at(10) = i;
				} else if (col.at(i) == "evalue"){ 
					indiceCol.at(11) = i;
				} else if (col.at(i) == "e-value"){ 
					indiceCol.at(11) = i;
				} else if (col.at(i) == "e.value"){ 
					indiceCol.at(11) = i;
				} else if (col.at(i) == "mismatch"){ 
					indiceCol.at(12) = i;
				} else if (col.at(i) == "gaps"){ 
					indiceCol.at(13) = i;
				} else if (col.at(i) == "strand"){ 
					indiceCol.at(16) = i;
				} else if (col.at(i) == "Query id"){ 
					indiceCol.at(17) = i;
				} else if (col.at(i) == "Subject id"){ 
					indiceCol.at(18) = i;
				}
			} 
		} else if (type[TYPE_HEADER]==1) {
			indiceCol.at(17) = 0;	//name query
			indiceCol.at(18) = 1;	//name subject
			indiceCol.at(10) = 2;	//pIdentity
			indiceCol.at(3) = 6;	//q_start
			indiceCol.at(4) = 7;	//q_end
			indiceCol.at(5) = 8;	//s_start
			indiceCol.at(6) = 9;	//s_end
			indiceCol.at(11) = 10;	//e_value
			indiceCol.at(8) = 11;	//score
		} else {
			cerr << "pb with the header"<<endl;
    		exit(EXIT_FAILURE);
		}
		
		bool boo = true;
		while (getline(ifIn, line)){
			align = new Alignement(line,indiceCol,idAlign,nbNucCut, booParam, espece);
			boo = true;
			
			if (align->alignOverlap()) {
				boo = false;
				++cptAlignOverlap;
			}
				
			if (type[TYPE_EXECUTION]==6 && boo){
				if (align->getDistDiag()>param[PARAM_DISTANCE_DIAG]){
					boo = false;
					++cptTooFar;
				}
				if (align->getDistDiag()<param[PARAM_MIN_LENGTH_SINGLETON] && boo){
					boo = false;
					++cptTooClose;
				}
				
			}
			
			
			if (verifOver && boo) {
				if (!checkAlign(vecAlign,*align,file_overlap, booPrint)){
					boo = false;  
				}
			}
			
			
			if (boo){
				vecAlign->push_back(align);
				if (maxY < align->getSubjectEnd()) maxY = align->getSubjectEnd();
				if (maxY < align->getSubjectStart()) maxY = align->getSubjectStart();
			} else {
				delete align;
			}
			++idAlign;
		}
		
	}
	else{	cout<< "pas de ligne dans le fichier" <<endl;	}
		
	cout << "number alignement in file : " << idAlign << endl;
	cout << "number alignement not too far : " << idAlign - cptTooFar << endl;
	cout << "number alignement not too far and not too close: " << idAlign - cptTooFar - cptTooClose << endl;
	cout << "number alignement keep : " << vecAlign->size() << endl;
	cout << "number alignement overlap : " << cptAlignOverlap << endl;
	
	cout << "maxY " << maxY << endl;
	return maxY;
}

void getUnitDupliQuery(ifstream& ifIn, vector<int>& vec) {
	string line; 
	vector<string> col;
	vector<int> indiceCol (19,-1);
	
	if (getline(ifIn, line)){
		//q_name	s_name	pident	size	mismatch	gaps	q_start	q_end	s_start	s_end	evalue	score
		split(col, line, is_any_of("\t"),algorithm::token_compress_off);
		for (int i=0; i<lexical_cast<int>(col.size()); i++){
			if (col.at(i) == "Query id"){ 
				indiceCol.at(0) = i;
			} else if (col.at(i) == "Subject id"){ 
				indiceCol.at(1) = i;
			}
		}
	}
	
	int s_start = 0;
	int s_end = 0;
	int q_start = 0;
	int q_end = 0;
	
	getline(ifIn, line);
	vector<string> v;
	boost::split(v, line, boost::is_any_of("\t"),boost::algorithm::token_compress_off);
	if (indiceCol[0]!=-1){
		try
	    	{
	    		int debut = v.at(indiceCol[0]).find("_")+1;
	    		int fin = v.at(indiceCol[0]).find("-");
	    		
	    		q_start = boost::lexical_cast<int>(v.at(indiceCol[0]).substr(debut,fin-debut))-1;
	    		
	    		debut = fin+1;
			
				q_end = boost::lexical_cast<int>(v.at(indiceCol[0]).substr(debut,string::npos));
	    	}
	    	catch(boost::bad_lexical_cast &) {
	    		cerr << "ReD->main.cpp->getUnitDupliQuery() : Missing Query id"<<endl;
		    	exit(EXIT_FAILURE);
	    	} 
	}
	if (indiceCol[1]!=-1){
		try
	    	{
	    		int debut = v.at(indiceCol[1]).find("_")+1;
	    		int fin = v.at(indiceCol[1]).find("-");
	    		
	    		s_start = boost::lexical_cast<int>(v.at(indiceCol[1]).substr(debut,fin-debut))-1;
	    		
				debut = fin+1;
			
				s_end = boost::lexical_cast<int>(v.at(indiceCol[1]).substr(debut,string::npos));
	    	}
	    	catch(boost::bad_lexical_cast &) {
				cerr << "ReD->main.cpp->getUnitDupliQuery() : Missing Subject id"<<endl;
		    	exit(EXIT_FAILURE);
	    	}
	}
	
	
	if ((q_end-q_start)<(s_end-s_start)){
		cout << q_end << " _ " << q_start << endl;
		vec.push_back(q_end-q_start+1);
		vec.push_back(q_start);
		vec.push_back(q_end);
		vec.push_back(1);
		
	}
	else {
		cout << s_end << " # " << s_start << endl;
		vec.push_back(s_end-s_start+1);
		vec.push_back(s_start);
		vec.push_back(s_end);
		vec.push_back(0);
	}
}

void optionOverlap(vector<Alignement*>& vecAlign, bool cut, int* type, bool* booParam, double distMaxQuery, double distMaxSubject){
	double pOverlap1 = 0;
	double pOverlap2 = 0;
	double pOverlap3 = 0;
	double pOverlap4 = 0;
	
	int dist = 0;
	bool testDist = false;
	bool boo = true;
	
	if (type[TYPE_EXECUTION] == 6 ){
		testDist = true;
	}	
	vector<Alignement*>::iterator it2;
	for (vector<Alignement*>::iterator it = vecAlign.begin(); it<vecAlign.end(); it++){
		it2 = it+1;
		boo = true;
		while ( it2!=vecAlign.end() && boo ){
			
			if (testDist){
				dist = abs((*it)->getDistSubject(**it2,booParam[BPARAM_CUT_ALIGN], booParam[BPARAM_MIDDLE_ALIGN]));
				if (dist>distMaxSubject){
					boo = false;
				} else {
					dist = abs((*it)->getDistQuery(**it2,booParam[BPARAM_CUT_ALIGN], booParam[BPARAM_MIDDLE_ALIGN]));
					if (dist>distMaxQuery){
						boo = false;
					}
				}
			}

			if (boo) {
				pOverlap1 = (*it)->pOverlapSubject(**it2, cut);
				pOverlap2 = (*it)->pOverlapQuery(**it2, cut);
				if (pOverlap1 > 0)	(*it)->addAlignOverlap(pOverlap1);
				if (pOverlap2 > 0)	(*it)->addAlignOverlap(pOverlap2);
				if (booParam[BPARAM_SAME_SPECIE]){
					pOverlap3 = (*it)->pOverlapQuerySubject(**it2, cut);
					pOverlap4 = (*it)->pOverlapSubjectQuery(**it2, cut);
					if (pOverlap3 > 0)	(*it)->addAlignOverlap(pOverlap3);
					if (pOverlap4 > 0)	(*it)->addAlignOverlap(pOverlap4);
				}
				
				pOverlap1 = (*it2)->pOverlapSubject(**it, cut);
				pOverlap2 = (*it2)->pOverlapQuery(**it, cut);
				if (pOverlap1 > 0)	(*it2)->addAlignOverlap(pOverlap1);
				if (pOverlap2 > 0)	(*it2)->addAlignOverlap(pOverlap2);
				if (booParam[BPARAM_SAME_SPECIE]){
					pOverlap3 = (*it2)->pOverlapQuerySubject(**it, cut);
					pOverlap4 = (*it2)->pOverlapSubjectQuery(**it, cut);
					if (pOverlap3 > 0)	(*it2)->addAlignOverlap(pOverlap3);
					if (pOverlap4 > 0)	(*it2)->addAlignOverlap(pOverlap4);
				}
			}
			++it2;
		}
	}
	
	pOverlap1 = 0;
	for (vector<Alignement*>::iterator it = vecAlign.begin(); it<vecAlign.end(); it++){
		if (pOverlap1 < (*it)->getPercentOverlap()) pOverlap1 = (*it)->getPercentOverlap();
	}
	
	
	
	int multiplicateur = 1;
	
	if (type[TYPE_EXECUTION] == 5 ){
		multiplicateur = 100;
	} else {
		if (type[TYPE_EXECUTION] == 6 ){
			multiplicateur = 100;
		}	
	}
	
	if ( boost::numeric_cast<int>(pOverlap1)!=0 )	{
		for (vector<Alignement*>::iterator it = vecAlign.begin(); it<vecAlign.end(); it++){
			(*it)->setPercentOverlap( ((*it)->getPercentOverlap()/pOverlap1)*multiplicateur );
		}
	}
	
}

void killInvalidChain (vector<Chain*>& vecT, int* type, bool* booParam, double* param) {
	cout << "kill invalid chain" << endl;
	cout << "num chain créé: " << vecT.size() << endl;
		
	vector<Chain*> vecChain;
	
	for(vector<Chain*>::const_iterator it = vecT.begin(); it != vecT.end(); ++it){
		if ((*it)->chainValide(type[TYPE_FILTER],param[PARAM_NUMBER_MIN_ALIGN],param[PARAM_SCORE_MIN_CHAIN], param[PARAM_COEF_CORRELATION], booParam[BPARAM_GLOBAL_VALIDATION_OVERLAP], param[PARAM_UNIT_LENGTH])) {
			vecChain.push_back((*it));
		} 
	}
	cout << "num chain keep: " << vecChain.size() << endl;
	
	vecT.clear();
	
	for(vector<Chain*>::const_iterator it = vecChain.begin(); it != vecChain.end(); ++it){
		vecT.push_back((*it));
	}
	vecChain.clear();
	
	if (type[TYPE_EXECUTION]==7){
		bool deleteChain = false;
		double p1 = 0;
		double p2 = 0;
		
		for(vector<Chain*>::const_iterator it = vecT.begin(); it != vecT.end();++it){
			vector<Chain*>::const_iterator itNext = vecT.begin();
			deleteChain = false;
			while(itNext!=vecT.end() && !deleteChain){
				if (booParam[BPARAM_UNIT_TANDEM_QUERY]) {
					p1 = (*it)->pOverlapSubject((**itNext));
					p2 = (*itNext)->pOverlapSubject((**it));
					
					if (it != itNext && (p1>0.05 || p2>0.05) ){
						if ( (*it)->getLengthQuery()==(*itNext)->getLengthQuery() ){
							if ( (*it)->getChainScore()==(*itNext)->getChainScore() ){
								if ( (*it)->getLengthSubject()==(*itNext)->getLengthSubject() ){
									deleteChain = true;
								} else {
									if ( (*it)->getLengthSubject()<(*itNext)->getLengthSubject() ){
										deleteChain = true;
									}
								}
							} else {
								if ( (*it)->getChainScore()<(*itNext)->getChainScore() ){
									deleteChain = true;
								}
							}
						}
						
						if ( (*it)->getLengthQuery()<(*itNext)->getLengthQuery()){
							deleteChain = true;
						}
					}
				} else {
					p1 = (*it)->pOverlapQuery((**itNext));
					p2 = (*itNext)->pOverlapQuery((**it));
					
					if (it != itNext && (p1>0.05 || p2>0.05) ){
						if ( (*it)->getLengthSubject()==(*itNext)->getLengthSubject()){
							if ( (*it)->getChainScore()==(*itNext)->getChainScore()){
								if ( (*it)->getLengthQuery()==(*itNext)->getLengthQuery()){
									deleteChain = true;
								} else {
									if ( (*it)->getLengthQuery()<(*itNext)->getLengthQuery()){
										deleteChain = true;
									}
								}
							} else {
								if ( (*it)->getChainScore()<(*itNext)->getChainScore()){
									deleteChain = true;
								}
							}
						}
						
						if ( (*it)->getLengthSubject()<(*itNext)->getLengthSubject()){
							deleteChain = true;
						}	
					}
				}
				++itNext;
			}
			
			if(!deleteChain){
				vecChain.push_back((*it));
			} 
		}
		
		vecT.clear();
	
		for(vector<Chain*>::const_iterator it = vecChain.begin(); it != vecChain.end(); ++it){
			vecT.push_back((*it));
		}
		
	}
	
	cout << "end kill invalid chain" << endl;
	cout << "num chain valid: " << vecT.size() << endl;
	
}

void selectAlignUse(vector<Alignement*>& vecA, vector<Chain*>& vecT) {

	for(vector<Alignement*>::iterator itAlign = vecA.begin(); itAlign!=vecA.end(); ++itAlign){
		(*itAlign)->setUseInChain(false);
	}

	vector<Alignement*> vecAlign;
	for(vector<Chain*>::const_iterator it = vecT.begin(); it!=vecT.end(); ++it){
		for (int i= 0; i<(*it)->getNbAlign(); ++i) {
			vecAlign.push_back((*it)->getAlign(i));
		}
	}

	for(vector<Alignement*>::iterator it = vecAlign.begin(); it!=vecAlign.end(); ++it){
		(*it)->setUseInChain(true);
	}

}

void addAlignChain(vector<Alignement*>& vecA, vector<Chain*>& vecT, int lengthMin, double* param) {
	cout << "begin addAlignChain" << endl;
	int cpt = 0;
	for(vector<Alignement*>::iterator itAlign = vecA.begin(); itAlign!=vecA.end(); ++itAlign){
		if ( !(*itAlign)->useInChain() ) {
			
			if ( (*itAlign)->getLengthSubject() >= lengthMin && (*itAlign)->getLengthQuery() >= lengthMin ) {
				vecT.push_back( new Chain(*(*itAlign), param[PARAM_SCORE_MIN_CHAIN]) );
				(*itAlign)->setUseInChain(true);
				++cpt ;
			}

		}
	}

	cout << "end addAlignChain : " <<  cpt << "chain" << endl;
}

void printAlign(char* fileOut, vector<Alignement*>& vecA, int* type, bool* booParam, double* param)	{ 
	cout << "print align" << endl;
	int numAlign = 0;
	
	bool existe = false;
	
	
	
	char fileOut2[2000];
	sprintf(fileOut2, "%s%s", fileOut, ".align.out");
	ofstream chain_file; 
	
	std::ifstream fichier(fileOut2); 
  	if(!fichier.fail()) existe = true;
  	else existe = false; 
  	fichier.close();
	
	string entete = "#q_order\telement\tq_chrom\tq_start\tq_end\tstrand\ts_chrom\ts_start\ts_end\ts_order\tscore\tsize\tq_chain\ts_chain\tmol\n";
	
	if (type[TYPE_EXECUTION]==7){
		chain_file.open(fileOut2, ios::app);
		if (existe) entete = "";
	} else {
		chain_file.open(fileOut2);
	}
	
	chain_file << entete;
	
	if (type[5]==1) {
		vector<Alignement*> vecAlignQuery(vecA);
		vector<Alignement*> vecAlignSubject(vecA);		
	
		
		sort(vecAlignQuery.begin(), vecAlignQuery.end(), compareQueryAlign);
		sort(vecAlignSubject.begin(), vecAlignSubject.end(), compareSubjectAlign);
		int order=0;
		for(vector<Alignement*>::const_iterator it = vecAlignQuery.begin(); it != vecAlignQuery.end(); ++it){
			(*it)->setOrderQuery(order);
			++order;
		}
		order=0;
		for(vector<Alignement*>::const_iterator it = vecAlignSubject.begin(); it != vecAlignSubject.end(); ++it){
			(*it)->setOrderSubject(order);
			++order;
		}
		
		for(vector<Alignement*>::const_iterator itWrite = vecAlignQuery.begin(); itWrite!=vecAlignQuery.end(); ++itWrite){
			(*itWrite)->printRes(chain_file, numAlign, numAlign, type, booParam, param, false);
			++numAlign;
		}
	} else {
		for(vector<Alignement*>::const_iterator itWrite = vecA.begin(); itWrite!=vecA.end(); ++itWrite){
			(*itWrite)->printRes(chain_file, numAlign, numAlign, type, booParam, param, false);
			++numAlign;
		}	
	}
	
	chain_file.close();
}

void printAlignUse(char* fileOut, vector<Chain*>& vecT, int* type, bool* booParam, double* param)	{ 
	cout << "print align Use" << endl;
	int numAlign = 0;
	
	bool existe = false;
	
	char fileOut2[2000];
	sprintf(fileOut2, "%s%s", fileOut, ".alignUse.out");
	ofstream chain_file; 
	
	std::ifstream fichier(fileOut2); 
  	if(!fichier.fail()) existe = true;
  	else existe = false; 
  	fichier.close();
	
	string entete = "#q_order\telement\tq_chrom\tq_start\tq_end\tstrand\ts_chrom\ts_start\ts_end\ts_order\tscore\tsize\tq_chain\ts_chain\tmol\n";
	
	if (type[TYPE_EXECUTION]==7){
		chain_file.open(fileOut2, ios::app);
		if (existe) entete = "";
	} else {
		chain_file.open(fileOut2);
	}
	
	chain_file << entete;
	
	
	vector<Alignement*> vecAlign;
	for(vector<Chain*>::const_iterator it = vecT.begin(); it!=vecT.end(); ++it){
		for (int i= 0; i<(*it)->getNbAlign(); ++i) {
			vecAlign.push_back((*it)->getAlign(i));
		}
	}
	
	map<string,int> vecOrderSubject;
	sort(vecAlign.begin(), vecAlign.end(), compareSubjectAlign);
	for(vector<Alignement*>::const_iterator it = vecAlign.begin(); it!=vecAlign.end(); ++it){
		vecOrderSubject.insert(pair<string, int>((*it)->getQueryName()+(*it)->getSubjectName(), numAlign));
		++numAlign;
	}
	
	sort(vecAlign.begin(), vecAlign.end(), compareQueryAlign);
	numAlign = 0;
	for(vector<Alignement*>::const_iterator itWrite = vecAlign.begin(); itWrite!=vecAlign.end(); ++itWrite){
		(*itWrite)->printRes(chain_file, vecOrderSubject.find((*itWrite)->getQueryName()+(*itWrite)->getSubjectName())->second, numAlign, type, booParam, param, true);
		(*itWrite)->setOrderSubject(vecOrderSubject.find((*itWrite)->getQueryName()+(*itWrite)->getSubjectName())->second);
		(*itWrite)->setOrderQuery(numAlign);
		++numAlign;
	}
	chain_file.close();
}

void printChain(char* fileOut, vector<Chain*>& vecT, int* type, bool* booParam, double* param, vector<double>& vecCost)	{ 
	vecCost.size();
	
	cout << "print chain" << endl;
	int numChain = 0;
	++cptPrint;
	
	char fileOut2[2000];
	if (type[TYPE_FILE_OUT]==0)	sprintf(fileOut2, "%s%s", fileOut, "");
	else {
		if (type[TYPE_FILE_OUT]==1)	sprintf(fileOut2, "%s%s", fileOut, ".chain.out");
	}
	ofstream chain_file;
	
	bool existe = false;
	
	std::ifstream fichier(fileOut2); 
  	if(!fichier.fail()) existe = true;
  	else existe = false; 
  	fichier.close();
	
	string entete ="";
	if (type[TYPE_FILE_OUT]==0)	{
		entete = "numChain\tchain\tstrand\ts_chrom\tq_chrom\ts_name\tq_name\ts_start\ts_end\tq_start\tq_end\tscoreChain\tpente\tnbHSP\tcoefCorrelation\n";
	} else {
		if (type[TYPE_FILE_OUT]==1) {
			entete = "#q_order\telement\tq_chrom\tq_start\tq_end\tstrand\ts_chrom\ts_start\ts_end\ts_order\tscore\tsize\tq_chain\ts_chain\tmol\n";
		}
	}
	
	if (type[TYPE_EXECUTION]==7){
		chain_file.open(fileOut2, ios::app);
		if (existe) entete = "";
	} else {
		chain_file.open(fileOut2);
	}
	
	chain_file << entete;
	
	if (type[TYPE_FILE_OUT]==1) {
		vector<Chain*> vecChainQuery;
		vector<Chain*> vecChainSubject;	
		
		for(vector<Chain*>::const_iterator it = vecT.begin(); it != vecT.end(); ++it){
			vecChainQuery.push_back((*it));
			vecChainSubject.push_back((*it));
		}
		
		sort(vecChainQuery.begin(), vecChainQuery.end(), compareQueryChain);
		if (type[TYPE_FILE_OUT]==1) {
			sort(vecChainSubject.begin(), vecChainSubject.end(), compareSubjectChain);
			int order=0;
			for(vector<Chain*>::const_iterator it = vecChainQuery.begin(); it != vecChainQuery.end(); ++it){
				(*it)->setOrderQuery(order);
				++order;
			}
			order=0;
			sort(vecChainSubject.begin(), vecChainSubject.end(), compareSubjectChain);
			for(vector<Chain*>::const_iterator it = vecChainSubject.begin(); it != vecChainSubject.end(); ++it){
				(*it)->setOrderSubject(order);
				++order;
			}
		}
		
		for(vector<Chain*>::const_iterator itWrite = vecChainQuery.begin(); itWrite!=vecChainQuery.end(); ++itWrite){
			(*itWrite)->computeCoefCorrelation();
			(*itWrite)->printRes(chain_file, numChain, type, booParam, param);
			++numChain;
		}
	} else {
		for(vector<Chain*>::const_iterator itWrite = vecT.begin(); itWrite!=vecT.end(); ++itWrite){
			if ((*itWrite)->chainValide(type[TYPE_FILTER],param[PARAM_NUMBER_MIN_ALIGN],param[PARAM_SCORE_MIN_CHAIN], param[PARAM_COEF_CORRELATION], booParam[BPARAM_GLOBAL_VALIDATION_OVERLAP], param[PARAM_UNIT_LENGTH])) {
				(*itWrite)->computeCoefCorrelation();
				(*itWrite)->printRes(chain_file, numChain, type, booParam, param);
				++numChain;
			}
		}
	}
		
	chain_file.close();
}

int printRegion(char* fileOut, vector<Chain*>& vecT, int* type, bool* booParam, double* param)	{ 
	cout << "print region" << endl;
	int dist = std::numeric_limits<int>::max();
	int longestUnit = 0;
	
	char fileOut2[2000];
	sprintf(fileOut2, "%s%s", fileOut, ".dupli.out");
	ofstream chain_file; 
	
	bool existe = false;
	
	std::ifstream fichier(fileOut2); 
  	if(!fichier.fail()) existe = true;
  	else existe = false; 
  	fichier.close();
	
	string entete = "#chrom\tstart\tend\tu_start\tu_end\tnumDupli\tdupli\n";
	
	if (type[TYPE_EXECUTION]==7){
		chain_file.open(fileOut2, ios::app);
		if (existe) entete = "";
	} else {
		chain_file.open(fileOut2);
	}
	
	chain_file << entete;
	
	if (booParam[BPARAM_UNIT_TANDEM_QUERY]) {
		sort(vecT.begin(), vecT.end(), compareSubjectChain);
		chain_file << vecT.at(0)->getQueryChrom() << "\t";
		chain_file << vecT.at(0)->getSubjectMin() << "\t";
		chain_file << vecT.at(vecT.size()-1)->getSubjectMax() << "\t";
		chain_file << numeric_cast<int>(param[PARAM_UNIT_START]) << "\t";
		chain_file << numeric_cast<int>(param[PARAM_UNIT_END]) << "\t";
	}
	else {
		sort(vecT.begin(), vecT.end(), compareQueryChain);
		chain_file << vecT.at(0)->getSubjectChrom() << "\t";
		chain_file << vecT.at(0)->getQueryMin() << "\t";
		chain_file << vecT.at(vecT.size()-1)->getQueryMax() << "\t";
		chain_file << numeric_cast<int>(param[PARAM_UNIT_START]) << "\t";
		chain_file << numeric_cast<int>(param[PARAM_UNIT_END]) << "\t";
	}
	
	chain_file << vecT.size() << "\t";
	int precCenter = 0;
	int center = 0;
	
		
	for(vector<Chain*>::const_iterator itWrite = vecT.begin(); itWrite!=vecT.end(); ++itWrite){
		if ((*itWrite)->getCoefCorrelation()>0){
			if (booParam[BPARAM_UNIT_TANDEM_QUERY]) {
				chain_file << (*itWrite)->getSubjectMin() << ".." << (*itWrite)->getSubjectMax() << ",";
				center = ((*itWrite)->getSubjectStart()+(*itWrite)->getSubjectEnd())/2;
				if (abs((*itWrite)->getSubjectStart()-(*itWrite)->getSubjectEnd())+1 > longestUnit) longestUnit = abs((*itWrite)->getSubjectStart()-(*itWrite)->getSubjectEnd())+1;
				if (precCenter!=0 && abs(center-precCenter)<dist) dist = abs(center-precCenter);
				precCenter = center;
			} else {
				chain_file << (*itWrite)->getQueryMin() << ".." << (*itWrite)->getQueryMax() << ",";
				center = ((*itWrite)->getQueryStart()+(*itWrite)->getQueryEnd())/2;
				if (abs((*itWrite)->getQueryStart()-(*itWrite)->getQueryEnd())+1 > longestUnit) longestUnit = abs((*itWrite)->getQueryStart()-(*itWrite)->getQueryEnd())+1;
				if (precCenter!=0 && abs(center-precCenter)<dist) dist = abs(center-precCenter);
				precCenter = center;
			}
		}else{
			if (booParam[BPARAM_UNIT_TANDEM_QUERY]) {
				chain_file << (*itWrite)->getSubjectMin() << ".." << (*itWrite)->getSubjectMax() << ",";
				center = ((*itWrite)->getSubjectEnd()+(*itWrite)->getSubjectStart())/2;
				if (abs((*itWrite)->getSubjectEnd()-(*itWrite)->getSubjectStart())+1 > longestUnit) longestUnit = abs((*itWrite)->getSubjectEnd()-(*itWrite)->getSubjectStart())+1;
				if (precCenter!=0 && abs(center-precCenter)<dist) dist = abs(center-precCenter);
				precCenter = center;
			} else {
				chain_file << (*itWrite)->getQueryMin() << ".." << (*itWrite)->getQueryMax() << ",";
				center = ((*itWrite)->getQueryEnd()+(*itWrite)->getQueryStart())/2;
				if (abs((*itWrite)->getQueryEnd()-(*itWrite)->getQueryStart())+1 > longestUnit) longestUnit = abs((*itWrite)->getQueryEnd()-(*itWrite)->getQueryStart())+1;
				if (precCenter!=0 && abs(center-precCenter)<dist) dist = abs(center-precCenter);
				precCenter = center;
			}	
		}
	}
	
	chain_file << "\n";
	chain_file.close();
	
	if (dist > longestUnit+longestUnit/2) {
		dist = longestUnit+longestUnit/2;
	} 
	return dist;
}

void printLongRegion(char* fileOut, vector<Chain*>& vecT, int* type, bool* booParam, double* param, int dist)	{ 
	cout << "print region" << endl;
	
	char fileOut2[2000];
	sprintf(fileOut2, "%s%s", fileOut, ".longDupli.out");
	ofstream chain_file; 
	
	bool existe = false;
	
	std::ifstream fichier(fileOut2); 
  	if(!fichier.fail()) existe = true;
  	else existe = false; 
  	fichier.close();
	
	string entete = "#chrom\tstart\tend\tu_start\tu_end\tnumDupli\tdupli\n";
	
	if (type[TYPE_EXECUTION]==7){
		chain_file.open(fileOut2, ios::app);
		if (existe) entete = "";
	} else {
		chain_file.open(fileOut2);
	}
	
	chain_file << entete;
	int center = 0;
	
	if (booParam[BPARAM_UNIT_TANDEM_QUERY]) {
		
		sort(vecT.begin(), vecT.end(), compareSubjectChain);
		chain_file << vecT.at(0)->getQueryChrom() << "\t";
		
		center = (vecT.at(0)->getSubjectStart()+vecT.at(0)->getSubjectEnd())/2;
		if (center-dist/2<1) center = 1;
		else center = center-dist/2;
		chain_file << center << "\t";
		
		center = (vecT.at(vecT.size()-1)->getSubjectStart()+vecT.at(vecT.size()-1)->getSubjectEnd())/2;
		center = center+dist/2;
		chain_file << center << "\t";
		
		chain_file << numeric_cast<int>(param[PARAM_UNIT_START]) << "\t";
		chain_file << numeric_cast<int>(param[PARAM_UNIT_END]) << "\t";
	}
	else {
		sort(vecT.begin(), vecT.end(), compareQueryChain);
		chain_file << vecT.at(0)->getSubjectChrom() << "\t";
		
		center = (vecT.at(0)->getQueryStart()+vecT.at(0)->getQueryEnd())/2;
		if (center-dist/2<1) center = 1;
		else center = center-dist/2;
		chain_file << center << "\t";
		
		center = (vecT.at(vecT.size()-1)->getQueryStart() + vecT.at(vecT.size()-1)->getQueryEnd())/2;
		center = center+dist/2;
		chain_file << center << "\t";
		
		chain_file << numeric_cast<int>(param[PARAM_UNIT_START]) << "\t";
		chain_file << numeric_cast<int>(param[PARAM_UNIT_END]) << "\t";
	}
	
	chain_file << vecT.size() << "\t";
	
	for(vector<Chain*>::const_iterator itWrite = vecT.begin(); itWrite!=vecT.end(); ++itWrite){
		if ((*itWrite)->getCoefCorrelation()>0){
			if (booParam[BPARAM_UNIT_TANDEM_QUERY]) {
				center = ((*itWrite)->getSubjectStart()+(*itWrite)->getSubjectEnd())/2;
			} else {
				center = ((*itWrite)->getQueryStart()+(*itWrite)->getQueryEnd())/2;
			}
		}else{
			if (booParam[BPARAM_UNIT_TANDEM_QUERY]) {
				center = ((*itWrite)->getSubjectEnd()+(*itWrite)->getSubjectStart())/2;
			} else {
				center = ((*itWrite)->getQueryEnd()+(*itWrite)->getQueryStart())/2;
			}	
		}
		if (center-dist/2<1) chain_file << 1 << ".." << center+dist/2 << ",";
		else chain_file << center-dist/2 << ".." << center+dist/2 << ",";
	}
	
	chain_file << "\n";
	chain_file.close();
		
}


void calcDistMoy(vector<Alignement*>& vecAlign, double* param, bool* booParam){
	unsigned long long int distQ = 0;
	unsigned long long int distS = 0;
	vector<Alignement*>::iterator itPrec;
	
	bool firstAlignChrom = true;
	sort(vecAlign.begin(), vecAlign.end(), compareSubjectAlign);
	for (vector<Alignement*>::iterator it=vecAlign.begin(); it!=vecAlign.end(); ++it){
		if (firstAlignChrom){
				distS += (*it)->getSubjectMin();
				firstAlignChrom = false;
				itPrec = it;
		} else {
			if ( ((*itPrec)->getSubjectChrom()).compare((*it)->getSubjectChrom())==0 ){
				if((*it)->getSubjectMin()>(*itPrec)->getSubjectMax()){
					distS += ((*it)->getSubjectMin() - (*itPrec)->getSubjectMax());
				}
			} else {
				itPrec = it;
			}
		}
		if ((*itPrec)->getSubjectMax()<(*it)->getSubjectMax()){
			itPrec = it;
		}
	}
	
	firstAlignChrom = true;
	sort(vecAlign.begin(), vecAlign.end(), compareQueryAlign);
	for (vector<Alignement*>::iterator it=vecAlign.begin(); it!=vecAlign.end(); ++it){
		if (firstAlignChrom){
				distQ += (*it)->getQueryMin();
				firstAlignChrom = false;
				itPrec = it;
		} else {
			if ( ((*itPrec)->getQueryChrom()).compare((*it)->getQueryChrom())==0 ){
				if((*it)->getQueryMin()>(*itPrec)->getQueryMax()){
					distQ += ((*it)->getQueryMin() - (*itPrec)->getQueryMax());
				}
			} else {
				itPrec = it;
			}
		}
		if ((*itPrec)->getQueryMax()<(*it)->getQueryMax()){
			itPrec = it;
		}
	}
	
	cout << distQ << endl;
	cout << distS << endl;
	
	param[PARAM_DIST_MAX_QUERY] = (distQ/sqrt(vecAlign.size()))/sqrt(2);
	if (booParam[BPARAM_SAME_SPECIE] == false) { param[PARAM_DIST_MAX_SUBJECT] = (distS/sqrt(vecAlign.size()))/sqrt(2); }
	else { param[PARAM_DIST_MAX_SUBJECT] = param[PARAM_DIST_MAX_QUERY]; }
}

void calcDistMed(vector<Alignement*>& vecAlign, double* param, bool* booParam){
	vector<int> vecDistQ;
	vector<int> vecDistS;
	vector<Alignement*>::iterator itPrec;
	
	bool firstAlignChrom = true;
	sort(vecAlign.begin(), vecAlign.end(), compareSubjectAlign);
	for (vector<Alignement*>::iterator it=vecAlign.begin(); it!=vecAlign.end(); ++it){
		if (firstAlignChrom){
				firstAlignChrom = false;
				itPrec = it;
		} else {
			if ( ((*itPrec)->getSubjectChrom()).compare((*it)->getSubjectChrom())==0 ){
				if((*it)->getSubjectMin()>(*itPrec)->getSubjectMax()){
					vecDistS.push_back((*it)->getSubjectMin() - (*itPrec)->getSubjectMax());
				}
			} else {
				itPrec = it;
			}
		}
		if ((*itPrec)->getSubjectMax()<(*it)->getSubjectMax()){
			itPrec = it;
		}
	}
	
	firstAlignChrom = true;
	sort(vecAlign.begin(), vecAlign.end(), compareQueryAlign);
	for (vector<Alignement*>::iterator it=vecAlign.begin(); it!=vecAlign.end(); ++it){
		if (firstAlignChrom){
				firstAlignChrom = false;
				itPrec = it;
		} else {
			if ( ((*itPrec)->getQueryChrom()).compare((*it)->getQueryChrom())==0 ){
				if((*it)->getQueryMin()>(*itPrec)->getQueryMax()){
					vecDistQ.push_back((*it)->getQueryMin() - (*itPrec)->getQueryMax());
				}
			} else {
				itPrec = it;
			}
		}
		if ((*itPrec)->getQueryMax()<(*it)->getQueryMax()){
			itPrec = it;
		}
	}
	
	sort(vecDistQ.begin(), vecDistQ.end());
	sort(vecDistS.begin(), vecDistS.end());
	int somme = 0;
	for (vector<int>::iterator it=vecDistQ.begin(); it!=vecDistQ.end(); ++it){
		somme += (*it);
	}
	cout << somme << endl;
	somme = 0;
	for (vector<int>::iterator it=vecDistS.begin(); it!=vecDistS.end(); ++it){
		somme += (*it);
	}
	cout << somme << endl;
	
	param[PARAM_DIST_MAX_QUERY] = vecDistQ[vecDistQ.size()/2];
	if (booParam[BPARAM_SAME_SPECIE] == false) { param[PARAM_DIST_MAX_SUBJECT] = vecDistS[vecDistS.size()/2]; }
	else { param[PARAM_DIST_MAX_SUBJECT] = param[PARAM_DIST_MAX_QUERY]; }
}

void calcDist(vector<Alignement*>& vecAlign, double* param){
	vector<int> vecDistQ;
	vector<int> vecDistS;
	vector<Alignement*>::iterator itPrec;
	
	bool firstAlignChrom = true;
	sort(vecAlign.begin(), vecAlign.end(), compareSubjectAlign);
	for (vector<Alignement*>::iterator it=vecAlign.begin(); it!=vecAlign.end(); ++it){
		if (firstAlignChrom){
				firstAlignChrom = false;
				itPrec = it;
		} else {
			if ( ((*itPrec)->getSubjectChrom()).compare((*it)->getSubjectChrom())==0 ){
				if((*it)->getSubjectMin()>(*itPrec)->getSubjectMax()){
					vecDistS.push_back((*it)->getSubjectMin() - (*itPrec)->getSubjectMax());
				}
			} else {
				itPrec = it;
			}
		}
		if ((*itPrec)->getSubjectMax()<(*it)->getSubjectMax()){
			itPrec = it;
		}
	}
	
	firstAlignChrom = true;
	sort(vecAlign.begin(), vecAlign.end(), compareQueryAlign);
	for (vector<Alignement*>::iterator it=vecAlign.begin(); it!=vecAlign.end(); ++it){
		if (firstAlignChrom){
				firstAlignChrom = false;
				itPrec = it;
		} else {
			if ( ((*itPrec)->getQueryChrom()).compare((*it)->getQueryChrom())==0 ){
				if((*it)->getQueryMin()>(*itPrec)->getQueryMax()){
					vecDistQ.push_back((*it)->getQueryMin() - (*itPrec)->getQueryMax());
				}
			} else {
				itPrec = it;
			}
		}
		if ((*itPrec)->getQueryMax()<(*it)->getQueryMax()){
			itPrec = it;
		}
	}
	
	sort(vecDistQ.begin(), vecDistQ.end());
	sort(vecDistS.begin(), vecDistS.end());
	int difQ = vecDistQ[vecDistQ.size()/4*3] - vecDistQ[vecDistQ.size()/4]*1.5;
	int difS = vecDistS[vecDistS.size()/4*3] - vecDistS[vecDistS.size()/4]*1.5;
	
	int somme = 0;
	int cpt = 0;
	for (vector<int>::iterator it=vecDistQ.begin(); it!=vecDistQ.end(); ++it){
		if ((*it)>= vecDistQ[vecDistQ.size()/2]-difQ/2 && (*it)<= vecDistQ[vecDistQ.size()/2]+difQ/2) {
			somme += (*it);
			++cpt;
		}
	}
	param[PARAM_DIST_MAX_QUERY] = (somme/sqrt(cpt))/sqrt(2);
	
	somme = 0;
	cpt = 0;
	for (vector<int>::iterator it=vecDistS.begin(); it!=vecDistS.end(); ++it){
		if ((*it)>= vecDistS[vecDistS.size()/2]-difS/2 && (*it)<= vecDistS[vecDistS.size()/2]+difS/2) {
			somme += (*it);
			++cpt;
		}
	}
	param[PARAM_DIST_MAX_SUBJECT] = (somme/sqrt(cpt))/sqrt(2);
	
}


int main(int argc, char **argv) {
	
	double ratioEdge = 1;
	double ratioScore = 1;
	
	//TYPE_EXECUTION,TYPE_SCORE_ALIGN, TYPE_SCORE_TRANSITION, TYPE_DISTANCE_EVOLUTION, TYPE_FILTER, TYPE_FILE_OUT, TYPE_FILE_IN, TYPE_HEADER
	int type[8] = {1, 5, 1, 0, 0, 0, 0, 0};
	//BPARAM_CUT_ALIGN; BPARAM_EDGE_OVERLAP; BPARAM_TRANSITION; BPARAM_PENALITY_EXTEND_GAP; BPARAM_MIDDLE_ALIGN; BPARAM_REVERSE_MODE; BPARAM_FORWARD_QUERY_SUBJECT; BPARAM_EDGE_TANDEM; BPARAM_NO_TEETH_CHAIN; BPARAM_FILTER_TEETH_CHAIN; BPARAM_OVERLAP_BONUS; BPARAM_SAME_POLARITY; BPARAM_SLOPE_BONUS; BPARAM_UNIT_TANDEM_QUERY; BPARAM_MAX_EDGE
	bool booParam[18] = {false, false, false, false, false, false, false, true, false, false, false, false, false, false, true, false, false, false}; 
	//PARAM_DIST_MAX, PARAM_SCORE_MAX_ALIGN,PARAM_GAP_UNIT_LEN,PARAM_PENALITY_EXTEND_GAP, PARAM_NUMBER_MIN_ALIGN, PARAM_SCORE_MIN_CHAIN, PARAM_SCORE_MIN_SUBCHAIN, PARAM_MEAN_SCORE_ANCHOR, PARAM_MEAN_SCORE_TRANSITION, PARAM_NUMBER_MAX_ALIGN, PARAM_DISTANCE_DIAG, PARAM_UNIT_LENGTH, PARAM_UNIT_START, PARAM_UNIT_END, PARAM_MAX_EDGE, PARAM_PIDENTITY_SINGLETON
	double param[20] = {200000,-250,1,1, 3, 300, 300, 1000, 1000, 200000, 0.8, 2, 150000, 0, 0, 0, 15, 0.9, 500, 50000}; 
	
	bool verifOver = false;
	
	string espece;
	
	int cSource = 0;
	int cSink = 0;
	int numChain = 0;
	int lengthMinAncorChain = -1;
	
	char* fileOut = 0;
	char* fileIn = 0;
	
	int opt;
  
	while ((opt = getopt(argc, argv, "I:Y:E:A:a:F:M:W:T:C:D:G:clogmnesrvupR:S:H:h:Z:t:L:N:O:")) != -1) {
		try {
			switch (opt) {
	 		case 'I': // fichier d'entré
				fileIn = optarg;
				break;
			case 'Y': // type fichier d'entré
				type[TYPE_FILE_IN] = lexical_cast<int>(optarg);
				break;
			case 'Z': // nombre de chaînes à créer
				numChain = lexical_cast<int>(optarg);
				break;
			case 'N': // longueur minimum pour une ancre non utilisée soit transformée en chaîne
				lengthMinAncorChain = lexical_cast<int>(optarg);
				break;
			case 'R': // ratio edge
				ratioEdge = lexical_cast<double>(optarg);
				break;
			case 'G': // dist max of diagonal
				param[PARAM_DISTANCE_DIAG] = lexical_cast<double>(optarg);
				break;
			case 'C': // le coeficient de corrélation.
				param[PARAM_COEF_CORRELATION] = lexical_cast<double>(optarg);
				break;
			case 'H': // ratio score
				ratioScore = lexical_cast<double>(optarg);
				break;
			case 'E': //type d'execution
				type[TYPE_EXECUTION] = lexical_cast<int>(optarg);
				break;
			case 'A'://type de calcul de score d'alignement
				type[TYPE_SCORE_ALIGN] = lexical_cast<int>(optarg);
				break;
			case 'h'://type de calcul de score d'alignement
				type[TYPE_HEADER] = lexical_cast<int>(optarg);
				break;
			case 'S'://type de calcul de score d'alignement
				espece = lexical_cast<string>(optarg);
				break;
			case 'a'://scoreMaxAlign
				param[PARAM_SCORE_MAX_ALIGN] = - lexical_cast<double>(optarg);
				break;
			case 'F'://type de filtre sur les chaines
				type[TYPE_FILTER] = lexical_cast<int>(optarg);
				if (type[TYPE_FILTER]==1 || type[TYPE_FILTER]==3) booParam[BPARAM_EDGE_TANDEM] = false;
				break;	
			case 'M'://minimum number of alignement in chain
				param[PARAM_NUMBER_MIN_ALIGN] = lexical_cast<double>(optarg);
				break;	
			case 'W'://minimum number of alignement in chain
				param[PARAM_NUMBER_MAX_ALIGN] = lexical_cast<double>(optarg);
				break;	
			case 'T'://type de calcul de score de transition
				type[TYPE_SCORE_TRANSITION] = lexical_cast<int>(optarg);
				break;
			case 'D'://type de distance evolutive
				booParam[BPARAM_TRANSITION] = true;
				type[TYPE_DISTANCE_EVOLUTION] = lexical_cast<int>(optarg);
				break;
			case 'c': //coupe ou pas les alignements
				booParam[BPARAM_CUT_ALIGN] = true;
				break;
			case 'l': //arc overlapp seqeunce ou pas
				booParam[BPARAM_EDGE_OVERLAP] = true;
				break;
			case 'o': //order on two sequence
				booParam[BPARAM_ORDER] = true;
				break;
			case 'g': //global validation overlap
				booParam[BPARAM_GLOBAL_VALIDATION_OVERLAP] = true;
				break;
			case 'm': //middle point for align
				booParam[BPARAM_MIDDLE_ALIGN] = true;
				break;
			case 'n': //border max adge to go out of a node
				booParam[BPARAM_MAX_EDGE] = true;
				break;
			case 'e': //bonus
				booParam[BPARAM_SLOPE_BONUS] = true;
				break;
			case 's': //même espèce
				booParam[BPARAM_SAME_SPECIE] = true;
				break;
			case 'r': //reverse mode
				booParam[BPARAM_REVERSE_MODE] = true;
				break;
			case 'p': //suprime les arcs qui relient deux alignement de polarité différentes
				booParam[BPARAM_SAME_POLARITY] = true;
				break;
			case 'u': //bonus pour les alignements qui se chevauchenet
				booParam[BPARAM_OVERLAP_BONUS] = true;
				break;
			case 't': // distance max de transition
				param[PARAM_DIST_MAX_QUERY] = lexical_cast<double>(optarg);
				param[PARAM_DIST_MAX_SUBJECT] = param[PARAM_DIST_MAX_QUERY];
				booParam[BPARAM_DIST_MAX_AUTO] = false;
				break;
			case 'v': // vérifie que les alignements ne ce chevauchent pas
				verifOver = true;
				break;
			case 'L': // type de fichier de sortie
				type[TYPE_FILE_OUT] = lexical_cast<int>(optarg);
				break;
			case 'O': // fichier de sortie
				fileOut = optarg;
				break;
			default: 
				cerr << "Usage: -I name -Y num -E num [-N num] [-H num] [-h num] [-n] [-A num] [-S num] [-a num] [-T num] [-D num] [-G num] [-c] [-e] [-o] [-l] [-m] [-r] [-g num] [-t num] [-v]  [-L num] [-N num] -O name" << endl;
				cerr << "-I name : file in input" << endl;
				cerr << "-Y num : type of data in input" << endl;
				cerr << "\t 0 : dna" << endl;
				cerr << "\t 1 : prot" << endl;
				cerr << "-E num : type of excecution" << endl;
				cerr << "\t 1 : flot de cout min - score" << endl;
				cerr << "\t 2 : flot de cout min - nbChain" << endl;
				cerr << "\t 3 : glouton - score" << endl;
				cerr << "\t 4 : glouton - nbChain" << endl;
				cerr << "\t 5 : genes mostly duplicated" << endl;
				cerr << "\t 6 : tandem duplication" << endl;
				cerr << "\t 7 : tandem duplication after tblastx" << endl;
				cerr << "[-R num] : Ratio for edge" << endl;
				cerr << "[-H num] : Ratio for chain min score" << endl;
				cerr << "[-S num] : specices" << endl;
				cerr << "[-A num] : type of cost Alignement" << endl;
				cerr << "\t 0 : 50" << endl;
				cerr << "\t 1 : 50" << endl;
				cerr << "\t 2 : 50" << endl;
				cerr << "\t 3 : 1000" << endl;
				cerr << "\t 4 : -score" << endl;
				cerr << "\t 5 : score DAGchainer : -log10(evalue)" << endl;
				cerr << "\t 6 : score : -log(evalue)" << endl;
				cerr << "[-a num] : score maximum for an alignment " << endl;
				cerr << "[-h num] : type of header" << endl;
				cerr << "\t 0 : with header" << endl;
				cerr << "\t 1 : without header format glint" << endl;
				cerr << "[-F num] : type of filter for chain" << endl;
				cerr << "\t 0 : minimum number of alignement" << endl;
				cerr << "\t 1 : minimum number of alignement + Tandem" << endl;
				cerr << "\t 2 : minimum number of alignement + Overlapping sequence" << endl;
				cerr << "\t 3 : minimum number of alignement + Tandem + Overlapping sequence" << endl;
				cerr << "\t 4 : 50% of length (for -E 7)" << endl;
				cerr << "[-M num] : minimum number of alignement in chain, default : 3" << endl;
				cerr << "[-W num] : maximum number of alignement in chain, default : 2" << endl;
				cerr << "[-C num] : coefficient de correlation minimum"<< endl;
				cerr << "[-T num] : type of cost transition" << endl;
				cerr << "\t 0 : Manhattan distance" << endl;
				cerr << "\t 1 : 30" << endl;
				cerr << "\t 2 : DAGchainer distance" << endl;
				cerr << "\t 3 : ADHoRe distance" << endl;
				cerr << "[-D num] : type of distance evolutive" << endl;
				cerr << "[-G num] : dist max od diagonale only for -T 6" << endl;
				cerr << "\t 0 : Kimura proteine distance" << endl;
				cerr << "[-c] : erode alignement -> true" << endl;
				cerr << "[-l] : delete edges making sequence overlapp" << endl;
				cerr << "[-f] : filtre use in search phase and not after" << endl;
				cerr << "[-o] : order" << endl;
				cerr << "[-n] : border max edge to go out of a node" << endl;
				cerr << "[-g] : global validation overlap" << endl;
				cerr << "[-m] : use the middle of alignement" << endl;
				cerr << "[-s] : query = subject" << endl;
				cerr << "[-e] : bonus transition function to slope" << endl;
				cerr << "[-p] : delete arc beteween two alignement with different polarity" << endl;
				cerr << "[-r] : reverse mode" << endl;
				cerr << "[-u] : bonus for alignment overlapp" << endl;
				cerr << "[-t num] : max of nucleotide for make a transition" << endl;
				cerr << "[-Z num] : When -E 2 or 4. number of chain find " << endl;
				cerr << "[-v] : delete overlapping alignement" << endl;
				cerr << "[-N num] : min length for transform a not used anchor in chain" << endl;
				cerr << "[-L num] : type of output file" << endl;
				cerr << "\t 0 : Red" << endl;
				cerr << "\t 1 : Narciss" << endl;
				cerr << "-O name : file in output" << endl;
			
				cerr << "~/workspace/red/src/ReD_exec -I ~/workspace/red/donnee/ath/data/blastp.strict50.out -Y 1 -E 1 -R 5 -H 4 -A 6 -m -g -l -s -F 3 -M 3 -h 0 -T 2 -S ath -L 1 -O ~/workspace/red/res/ath_ROADEF/dupSeg-strict50-5-4_3" << endl;
				cerr << "~/workspace/red/src/ReD_exec -I ~/workspace/red/donnee/ath/data/glint-d2-c15-s25.out.centroFilter -Y 0 -E 6 -R 1 -H 1 -A 4 -m -g -l -G 150000 -t 40000 -u -e -o -p -v -s -F 2 -M 1 -h 0 -T 2 -S ath -L 1 -O ~/workspace/red/res/ath/test" << endl;
			
				exit(EXIT_FAILURE);
			}
		}
		catch(boost::bad_lexical_cast& e) {
			cerr << "ReD -> main.cpp -> : pb cast in option "<<endl;
			cerr << e.what() << endl;
			cerr << "option : " << optarg << endl;
		}
	}
	
	if(type[TYPE_EXECUTION]==6){
		booParam[BPARAM_SAME_SPECIE] = true;
	}
	
	if(type[TYPE_FILTER]>0 && !booParam[BPARAM_SAME_SPECIE]){
		type[TYPE_FILTER] = 0;
	}
	
	if (ratioEdge!=1) {
		double dif = param[PARAM_MEAN_SCORE_ANCHOR] * ratioEdge;
		dif = fabs(param[PARAM_MEAN_SCORE_ANCHOR]-dif)/2;
		param[PARAM_MEAN_SCORE_ANCHOR] = (ratioEdge<1)? param[PARAM_MEAN_SCORE_ANCHOR]-dif:param[PARAM_MEAN_SCORE_ANCHOR]+dif;
		param[PARAM_MEAN_SCORE_TRANSITION] = param[PARAM_MEAN_SCORE_ANCHOR]/ratioEdge;
	}
	
	param[PARAM_SCORE_MIN_CHAIN] = param[PARAM_MEAN_SCORE_ANCHOR] * param[PARAM_NUMBER_MIN_ALIGN] - param[PARAM_MEAN_SCORE_TRANSITION] * (param[PARAM_NUMBER_MIN_ALIGN]-1);
	param[PARAM_SCORE_MIN_CHAIN] = param[PARAM_SCORE_MIN_CHAIN] * ratioScore;
	param[PARAM_SCORE_MIN_SUBCHAIN] = param[PARAM_SCORE_MIN_CHAIN];
	
	
	cout << "Chain min score : " << param[PARAM_SCORE_MIN_CHAIN] << endl;
	cout << "Anchor mean score : " << param[PARAM_MEAN_SCORE_ANCHOR] << endl;
	cout << "Transition mean score : " << param[PARAM_MEAN_SCORE_TRANSITION] << endl;
		
	map<int, Alignement*> mapAlign;
	vector<Alignement*> vecAlign;
	vector<Chain*> vecT;
	vector<double> vecCost;
	map< string,vector<Alignement*> > mapAlignChrom;
	map<string,int> maxChromQuery;
	map<string,int> maxChromSubject;
	
	int cutaligne = 10;
	
	cout << "start read HSP" << endl;
	ofstream file_overlap;
	ifstream ifIn(fileIn, ios::in);
	int maxY = backAlign(&vecAlign, ifIn, cutaligne, file_overlap, false, verifOver, booParam, type, param, espece);
	ifIn.close();
	cout << "end read HSP" << endl;
	
	if (type[TYPE_EXECUTION]==7){
		ifstream ifIn2(fileIn, ios::in);
		vector<int> vec;
		getUnitDupliQuery(ifIn2, vec);
		ifIn2.close();

		cout << "1 : " << vecAlign.size() << endl;
		sort(vecAlign.begin(), vecAlign.end(), compareQueryAlign);
		cout << "2" << endl;
		int debutU = vecAlign.at(0)->getQueryMin();
		cout << "3" << endl;
		sort(vecAlign.begin(), vecAlign.end(), compareQueryMaxAlign);
		cout << "4" << endl;
		int finU = vecAlign.at(0)->getQueryMax();
		
		cout << finU - debutU << " ## " << vec.at(0) << endl;
		
		param[PARAM_UNIT_LENGTH] = vec.at(0)*55/100; 
		param[PARAM_UNIT_START] = vec.at(1); 
		param[PARAM_UNIT_END] = vec.at(2); 
		if (vec.at(3) == 1) booParam[BPARAM_UNIT_TANDEM_QUERY] = true;
		else booParam[BPARAM_UNIT_TANDEM_QUERY] = false;
		cutaligne = vec.at(0)*10/100;
		
		param[PARAM_DIST_MAX_QUERY] = vec.at(0);
		param[PARAM_DIST_MAX_SUBJECT] = vec.at(0);
	}
	
	
	
	if (type[TYPE_EXECUTION]==6){
		sort(vecAlign.begin(), vecAlign.end(), compareScoreAlign);
		param[PARAM_SCORE_MAX_ALIGN] = - boost::numeric_cast<int>(vecAlign.at(vecAlign.size()*9/10)->getScore());
		cout << "score max align = " << - param[PARAM_SCORE_MAX_ALIGN] << endl;
	}
	
	
	cout << "start sort HSP" << endl;
	sort(vecAlign.begin(), vecAlign.end(), compareQueryAlign);
	cout << "end sort HSP" << endl;
		
	
	
	map< string,vector<Alignement*> >::iterator itMapChrom;
	pair< map< string,vector<Alignement*> >::iterator, bool> newit;
	map<string,int>::iterator itChromMax;
	
	cout << "start pre-processing" << endl;
	for (vector<Alignement*>::iterator it=vecAlign.begin(); it!=vecAlign.end(); ++it){
		mapAlign.insert(pair<int, Alignement*>(mapAlign.size(), (*it)));
		(*it)->setIdInGraph(mapAlign.size()-1);
		
		itMapChrom = mapAlignChrom.find( ((*it)->getQueryChrom() + (*it)->getSubjectChrom()) );
		if( itMapChrom == mapAlignChrom.end() ) {
			
			itMapChrom = mapAlignChrom.find( ((*it)->getSubjectChrom() + (*it)->getQueryChrom()) );
			
			if( itMapChrom == mapAlignChrom.end() ) {
				newit = mapAlignChrom.insert(pair< string,vector<Alignement*> >( ((*it)->getQueryChrom() + (*it)->getSubjectChrom()), vector<Alignement*>() ) );
				if (newit.second == false){
					cerr << "error : n'a pas pu creer de vector : dans le main()" << endl;
					exit(EXIT_FAILURE);
				}
				itMapChrom = mapAlignChrom.find( ((*it)->getQueryChrom() + (*it)->getSubjectChrom()) );
			}
			
		}
		(*itMapChrom).second.push_back((*it));
		
		if (booParam[BPARAM_DIST_MAX_AUTO]) {
			itChromMax = maxChromQuery.find((*it)->getQueryChrom());
			if (itChromMax==maxChromQuery.end()){
				maxChromQuery.insert ( pair<string,int>((*it)->getQueryChrom(),(*it)->getQueryMax()) );	
			} else {
				if(itChromMax->second < (*it)->getQueryMax()) {
					itChromMax->second = (*it)->getQueryMax();
				}
			}
			
			if (booParam[BPARAM_SAME_SPECIE] == false) {
				itChromMax = maxChromSubject.find((*it)->getSubjectChrom());
				if (itChromMax==maxChromSubject.end()){
					maxChromSubject.insert ( pair<string,int>((*it)->getSubjectChrom(),(*it)->getSubjectMax()) );	
				} else {
					if(itChromMax->second < (*it)->getSubjectMax()) {
						itChromMax->second = (*it)->getSubjectMax();
					}
				}	
			} else {
				itChromMax = maxChromQuery.find((*it)->getSubjectChrom());
				if (itChromMax==maxChromQuery.end()){
					maxChromQuery.insert ( pair<string,int>((*it)->getSubjectChrom(),(*it)->getSubjectMax()) );	
				} else {
					if(itChromMax->second < (*it)->getSubjectMax()) {
						itChromMax->second = (*it)->getSubjectMax();
					}
				}
			}
		}
	}
	
	
	if (booParam[BPARAM_DIST_MAX_AUTO] && type[TYPE_EXECUTION]!=7) {
		calcDistMoy(vecAlign, param, booParam);
		cout << "DIST_MAX_QUERY : " << param[PARAM_DIST_MAX_QUERY] << endl;
		cout << "DIST_MAX_SUBJECT : " << param[PARAM_DIST_MAX_SUBJECT] << endl;
	}
	
		
	cout << "end pre-processing" << endl;
	
	cout << "DIST_MAX_QUERY : " << param[PARAM_DIST_MAX_QUERY] << endl;
	cout << "DIST_MAX_SUBJECT : " << param[PARAM_DIST_MAX_SUBJECT] << endl;
	
	if (booParam[BPARAM_OVERLAP_BONUS]){
		if (booParam[BPARAM_DIST_MAX_AUTO]) {
			optionOverlap(vecAlign,false, type, booParam, param[PARAM_DIST_MAX_QUERY], param[PARAM_DIST_MAX_SUBJECT]);
		} else {
			double dist = 70000;
			optionOverlap(vecAlign,false, type, booParam, dist, dist);
		}
		 
	}
	
	
	if (type[TYPE_EXECUTION]==1){
		double score = param[PARAM_SCORE_MIN_CHAIN]; 
		cout << "-------------- type d'exc flot score -------------" << endl;
		cout << "Score : " << score << endl;
		excecutionScore (mapAlign, vecT, vecCost, cSource, cSink, type, booParam, param, false);
		cout << endl;
	} else if (type[TYPE_EXECUTION]==2){
		cout << "-------------- type d'exc flot num chain -------------" << endl;
		cout << "number chain : " << numChain << endl;
		excecutionNumChain(mapAlign, vecT, vecCost, cSource, cSink, type, booParam, param, false, numChain);
		cout << endl;
	} else if (type[TYPE_EXECUTION]==3){
		cout << "-------------- type d'exc glouton score -------------" << endl;
		double score = param[PARAM_SCORE_MIN_CHAIN];
		cout << "Score : " << score << endl;
		
		excecutionGloutonScore (mapAlign, vecT, vecCost, cSource, cSink, type, booParam, param, score, false);
		cout << endl;
	} else if (type[TYPE_EXECUTION]==4){
		cout << "-------------- type d'exc glouton num chain -------------" << endl;
		cout << "number chain : " << numChain << endl;
		excecutionGloutonNumChain (mapAlign, vecT, vecCost, cSource, cSink, type, booParam, param, false, numChain);
		cout << endl;
	} else if (type[TYPE_EXECUTION]==5){
		cout << "-------------- type les gènes les plus dupliqués -------------" << endl;
		cout << "number chain : " << numChain << endl;
		excecutionDuplication (mapAlign, vecT, vecCost, cSource, cSink, type, booParam, param, false);
		cout << endl;
	} else if (type[TYPE_EXECUTION]==6){
		cout << "-------------- type tandem duplication -------------" << endl;
		excecutionDuplicationByComposantConnexe (mapAlign, vecT, vecCost, cSource, cSink, type, booParam, param, false);
		cout << endl;
	} else if (type[TYPE_EXECUTION]==7){
		cout << "-------------- type tandem duplication after tblastx -------------" << endl;
		excecutionDuplicationAfterTblastX (mapAlign, vecT, vecCost, type, booParam, param);
		cout << endl;
	} else {
		cerr << "error in paramatre -E" << endl;
		exit(0);		
	}
	
	
	if (booParam[BPARAM_REVERSE_MODE]) {  
		cout << "reverse" << endl;
		
		char fileOut2[2000];
		sprintf(fileOut2, "%s%s", fileOut, ".reverse");
		
		for (vector<Alignement*>::iterator it=vecAlign.begin(); it!=vecAlign.end(); ++it){
			(*it)->restartAlign(maxY);
		}
		
		for (vector<Alignement*>::iterator it=vecAlign.begin(); it!=vecAlign.end(); ++it){
			mapAlign.insert(pair<int, Alignement*>(mapAlign.size(), (*it)));
			(*it)->setIdInGraph(mapAlign.size());
		}
	
		if (type[TYPE_EXECUTION]==1){
			double score = param[PARAM_SCORE_MIN_CHAIN]; 
			cout << "-------------- type d'exc flot score -------------" << endl;
			cout << "Score : " << score << endl;
			excecutionScore (mapAlign, vecT, vecCost, cSource, cSink, type, booParam, param, true);
			cout << endl;
		} else if (type[TYPE_EXECUTION]==2){
			cout << "-------------- type d'exc flot num chain -------------" << endl;
			excecutionNumChain(mapAlign, vecT, vecCost, cSource, cSink, type, booParam, param, true, numChain);
			cout << endl;
		} else if (type[TYPE_EXECUTION]==3){
			cout << "-------------- type d'exc glouton score -------------" << endl;
			double score = param[PARAM_SCORE_MIN_CHAIN];
			cout << "Score : " << score << endl;
			
			excecutionGloutonScore (mapAlign, vecT, vecCost, cSource, cSink, type, booParam, param, score, true);
			cout << endl;
		} else if (type[TYPE_EXECUTION]==4){
			cout << "-------------- type d'exc glouton num chain -------------" << endl;
			excecutionGloutonNumChain (mapAlign, vecT, vecCost, cSource, cSink, type, booParam, param, true, numChain);
			cout << endl;
		} else {
			cerr << "error in paramatre -E" << endl;
			exit(0);		
		}
				
	}
		
	if (type[TYPE_EXECUTION]==7){
		if (booParam[BPARAM_UNIT_TANDEM_QUERY]) {
			vecT.push_back(new Chain(param[PARAM_UNIT_START], param[PARAM_UNIT_END], vecT.at(0)->getQueryChrom()));
		}
		else {
			vecT.push_back(new Chain(param[PARAM_UNIT_START], param[PARAM_UNIT_END], vecT.at(0)->getSubjectChrom()));
		}
	}
	
	if (lengthMinAncorChain > 0) {
		selectAlignUse(vecAlign, vecT);
		addAlignChain(vecAlign, vecT, lengthMinAncorChain, param);
	}
		
	killInvalidChain(vecT, type, booParam, param);
	bool booPrint = true;
	if (type[TYPE_FILE_OUT]==0)	printChain(fileOut, vecT, type, booParam, param, vecCost);
	else {
		if (type[TYPE_FILE_OUT]==1)	{
			if (type[TYPE_EXECUTION]==7){
				if (vecT.size()> 1) booPrint = true;
				else booPrint = false;
			}
			if(booPrint) {				
				int dist = 0;
				printAlign(fileOut, vecAlign, type, booParam, param);
				printAlignUse(fileOut, vecT, type, booParam, param);
				printChain(fileOut, vecT, type, booParam, param, vecCost);
				if (type[TYPE_EXECUTION]==7) {
					dist = printRegion(fileOut, vecT, type, booParam, param);
					printLongRegion(fileOut, vecT, type, booParam, param, dist);
				}
			}
		}
	}
	
	
	for (vector<Alignement*>::iterator it=vecAlign.begin(); it!=vecAlign.end(); ++it){
		
		delete *it;
		*it = 0;
	}
	
	for (vector<Chain*>::iterator it=vecT.begin(); it!=vecT.end(); ++it){
		delete *it;
	}
	
	cout << "end " << endl;			
}
