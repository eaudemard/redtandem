#include <string>
#include <iostream>
#include <fstream>
#include <getopt.h>

#include "./data/Region.h"
#include "./data/Anchor.h"

#include <boost/lexical_cast.hpp>
#include <boost/numeric/conversion/cast.hpp>

#include <glibmm/spawn.h>

// Added by Thomas F
#include <map>
#include <vector>
#include <sstream>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>

using namespace std;
using namespace boost;
using namespace data;

//---------------------------------------------------------------------------------------------------------------
/**
 * 
 * Added by Thomas F to handle the karyotype information
 * 
 */

/**
 * \brief Get the sequence name from the fasta header line
 * \pre The header is in the following format : >SeqName[ \t]
 * \param line the header line
 * \return The sequence name
 */
string GetSeqName(const string& line)
{
	size_t start  = line.find_first_not_of(">"); // Strip leading '>'
	size_t end = line.find_first_of(" \t\n",start); // Keep all characters up to the first space or \n, if any
	return line.substr(start,end-start);	
}

/**
 * \brief Get the chromosome name from the SeqName
 * \pre SeqName is in the following format specieschrom_start-end
 * \param SeqName the sequence name
 * \return The chromosome name (first word after the species name)
 */
string GetChrom(const string espece, const string& SeqName)
{
	size_t start =strlen(espece.c_str());
	size_t end  = SeqName.find_first_of("_",start); // Getting
	return SeqName.substr(start,end-start);	
}

/**
 * \brief Get the chromosome start from the SeqName
 * \pre SeqName is in the following format specieschrom_start-end
 * \param SeqName the sequence name
 * \return The chromosome start as an integer
 */
size_t GetChromStart(const string& SeqName)
{
	size_t start = SeqName.find_first_of("_") + 1; // Skip eveything before the first "_"	
	size_t end  = SeqName.find_first_of("-",start); // Getting first "-"
	return atoi(SeqName.substr(start,end-start).c_str());	
}

/**
 * \brief Get the chromosome end from the SeqName
 * \pre SeqName is in the following format specieschrom_start-end
 * \param SeqName the sequence name
 * \return The chromosome end as an integer
 */
size_t GetChromEnd(const string& SeqName)
{
	size_t start  = SeqName.find_first_of("-") + 1; // // Skip eveything before the first "-"		
	return atoi(SeqName.substr(start).c_str());	
}

/**
 * \brief Open the karyotype file and extract the last position of the chromosomal region (field named "end") !
 * \param filename The file name (ex: karyotype)
 */

void GetChromosomesInfos( const string& espece,const string& file, map< int, int >& karyo )
{	
	string chrom, line, SeqName;	
	size_t chromEnd;
	
	ifstream ifDNA( file.c_str(), ifstream::in );
	
	while ( getline( ifDNA, line, '\n' ) )
	{	
		if ( line[0] != '>' )
			continue;					
		SeqName = GetSeqName(line);
		chrom  = GetChrom( espece, SeqName ); 	
		chromEnd = GetChromEnd( SeqName ); 
		karyo[ atoi( chrom.c_str() ) ] = chromEnd;	
	}	
	ifDNA.close();		
}

//----------------------------------------------------------------------------------------------------------------------


bool compareRegion (const Region& R1, const Region& R2) {
	try
    {
		if ( boost::lexical_cast<int>(R1.getChrom()) < boost::lexical_cast<int>(R2.getChrom()) )
			return true;
		if ( boost::lexical_cast<int>(R1.getChrom()) > boost::lexical_cast<int>(R2.getChrom()) )
			return false;
			
    }
    catch(boost::bad_lexical_cast &) {
		cerr << "RegionTandem -> main.cpp -> compareRegion : pb cast"<<endl;
    }
	
	if (R1.getRegionStart() < R2.getRegionStart())
		return true;
		
	return false;
	
}

void readFilesRegion (ifstream& ifReg, vector<Region>& vecRegion) {

	string line;
	getline(ifReg, line);
	vector<string> col;
	vector<int> indiceCol (5,-1);
	
	split(col, line, is_any_of("\t"),algorithm::token_compress_off);
		
	if (!col.empty()){
		//r_chrom	r_start	r_end	u_start	u_end
		for (unsigned int i=0; i<col.size(); i++){
			if (col.at(i) == "r_chrom"){ 
				indiceCol.at(0) = i;
			} else if (col.at(i) == "r_start"){ 
				indiceCol.at(1) = i;
			} else if (col.at(i) == "r_end"){ 
				indiceCol.at(2) = i;
			} else if (col.at(i) == "u_start"){ 
				indiceCol.at(3) = i;
			} else if (col.at(i) == "u_end"){ 
				indiceCol.at(4) = i;
			} 
		}
	}
	
	while(getline(ifReg, line)){
			if (!line.empty()) { 
				vecRegion.push_back(Region(line,indiceCol));
			}
	}
}

bool readFilesGlint (const char* fileGlint, unsigned int& startUnit, unsigned int& endUnit, unsigned int pOverlapAnchor, char* fileOutUnite) {

	ifstream ifGlint (fileGlint, ios::in);

	string line;
	vector<int> indiceCol (15,-1);
	vector<string> col;
	indiceCol.at(2) = 6;
	indiceCol.at(3) = 7;
	indiceCol.at(4) = 8;
	indiceCol.at(5) = 9;
	indiceCol.at(6) = 11;
	indiceCol.at(13) = 0;
	indiceCol.at(14) = 1;
	
	vector<Anchor> vecAnchor;
	int cptAnchor = 0;
	
	while(getline(ifGlint, line)){
		if (!line.empty()) { 
			vecAnchor.push_back(Anchor(line,indiceCol, cptAnchor));
			++cptAnchor;
		}
	}
	
	vector<unsigned int> vecUnit (endUnit-startUnit+1+1,0);
	vector<Anchor>::const_iterator itAnchorMin = vecAnchor.begin();
	
	for(vector<Anchor>::const_iterator itAnchor = vecAnchor.begin(); itAnchor != vecAnchor.end(); ++itAnchor) {
			for (int i = itAnchor->getSubjectPosMin(); i <= itAnchor->getSubjectPosMax(); ++i){
				vecUnit.at(i) = 1;
			}
			
			for (int i = itAnchor->getQueryPosMin(); i <= itAnchor->getQueryPosMax(); ++i){
				vecUnit.at(i) = 1;
			}
			
			if ( itAnchor->getScore() < itAnchorMin->getScore() ) {
				itAnchorMin = itAnchor;
			}	
	}
	
	unsigned int sumNucInAnchor = 0;
	for(vector<unsigned int>::const_iterator itUnit = vecUnit.begin(); itUnit != vecUnit.end(); ++itUnit) {
		sumNucInAnchor += (*itUnit);
	}
		
	if ((sumNucInAnchor*100)/(endUnit-startUnit+1)>=pOverlapAnchor){
		unsigned int distStart = itAnchorMin->getQueryPosMax();
		unsigned int distEnd = endUnit - (startUnit + itAnchorMin->getSubjectPosMin()) + 1;
		unsigned int nbNucKeep = 0;
		unsigned int nbNucForget = 0;
			
		if (distStart>=distEnd) {
			nbNucKeep = itAnchorMin->getSubjectPosMin();
			nbNucForget = 0;
		} else {
			nbNucForget = itAnchorMin->getQueryPosMax();
			nbNucKeep = (endUnit - startUnit + 1) - nbNucForget;
		}
		
		startUnit += nbNucForget;
		endUnit = startUnit + nbNucKeep-1;
		
		if (endUnit < startUnit) {
			cerr << "regionTandem -> main.cpp -> readFilesGlint() pb update position unit : " << endl;
			cerr << fileOutUnite << endl;
			exit(EXIT_FAILURE);
		}
		
		cout << startUnit << " _ " << endUnit << "( "<< endUnit - startUnit <<" )"<<endl;
		
		ifstream ifUnite(fileOutUnite, ios::in);
		char fileOutUnite2[2000];
		sprintf(fileOutUnite2, "%s%i", fileOutUnite, 2);
		ofstream file_Unite(fileOutUnite2);
		size_t found_start = 0;
		size_t found_end = string::npos;
		
		getline(ifUnite, line);
		if (line.empty()) {
			cerr << "regionTandem -> main.cpp -> readFilesGlint() not line to read in : " << endl;
			cerr << fileOutUnite << endl;
			exit(EXIT_FAILURE);
		}
		
		found_end = line.find("_");
		if (found_end==string::npos){
			cerr << "regionTandem -> main.cpp -> readFilesGlint() 1 : caractere not found" << endl;
			cerr << line << endl;
			exit(EXIT_FAILURE);	
		}
		
		file_Unite << line.substr(found_start,found_end) << "_" << startUnit << "-" << endUnit << "\n";
		while(getline(ifUnite, line)){
			
			if (!line.empty()) {
								
				if (nbNucForget==0 && nbNucKeep!=0) {
					if 	(nbNucKeep>=line.size()){
						file_Unite << line << "\n";
						nbNucKeep -= line.size();
					} else {
						found_start = 0;
						found_end = nbNucKeep;
						nbNucKeep = 0;
						file_Unite << line.substr(found_start,found_end) << "\n";
						break;
					}
				}
				
				if (nbNucForget!=0) {
					if (nbNucForget >= line.size()) {
						nbNucForget -= line.size();
					} else {
						found_start = nbNucForget;
						found_end = string::npos;
						file_Unite << line.substr(found_start,found_end) << "\n";
						nbNucForget = 0;
					}
				}
								
				if 	(nbNucForget==0 && nbNucKeep==0)	break;
			}
			
		}
		
	
		ifUnite.close();
		file_Unite.close();
		
		std::remove(fileOutUnite);
		ofstream file_Unite2(fileOutUnite);
		ifstream ifUniteLight(fileOutUnite2, ios::in);
		
		while(getline(ifUniteLight, line)){
			if (!line.empty()) {
				file_Unite2 << line << "\n";
			}
		}
		
	
		file_Unite2.close();
		ifUniteLight.close();
		
		std::remove(fileOutUnite2);
		
	} else {
		return false;	
	}
	
	return true;
	
}


void findRegion(ifstream& ifDNA, int chrom, int startRegion, int endRegion, int& numNucByLine, int& numNucCopy, int& numLine, int& cptLine, int& startNuc, int& endChrom, int& nLine, string& espece) {

		
		bool changeChrom = false;
		string line;
		
		size_t found_start = 0;
		size_t found_end = 0;
		
		size_t foundBegin = 0;
		size_t foundEnd = 0;
		
		int indiceChrom = -1;
		
		while (indiceChrom!=chrom && getline(ifDNA, line)){
			++nLine;
			if ( numeric_cast<int>(line.size())>0 && line[0] == '>') {
				++indiceChrom;
				
				foundBegin = line.find(espece);
				foundEnd = line.find('_');
				if (foundBegin!=string::npos && foundEnd!=string::npos) {
					foundBegin += espece.length();
					try
	        		{
						indiceChrom = boost::lexical_cast<int>(line.substr(foundBegin,foundEnd-foundBegin));
	        		}
			        catch(boost::bad_lexical_cast &) {
						cout << "regionTandem -> main.cpp -> findRegion() 1 : chrom not int" << endl;
						cout << line.substr(foundBegin,foundEnd-foundBegin) << endl;
						exit(EXIT_FAILURE);
								
					}
					
				} else {
					cerr << "regionTandem -> main.cpp -> findRegion() 2 : name of chrom not found" << endl;
					cerr << line << endl;
					cerr << espece << endl;
					cerr << foundBegin << " # " << foundEnd << endl;
					exit(EXIT_FAILURE);	
				}
			}
			changeChrom = true;
		}
		
		if (changeChrom){
			cptLine = 1;
			cerr << "Found sequence of chrom "<<chrom<<" line :" << nLine << endl;
			
			found_start = line.find("-");
			if (found_start==string::npos){
				cerr << "regionTandem -> main.cpp -> main() 1 : caractere not found" << endl;
				cerr << line << endl;
				exit(EXIT_FAILURE);	
			}
			++found_start;
			
			found_end = line.find(" ", found_start);
			
			if (found_end == string::npos)	endChrom = lexical_cast<int>(line.substr(found_start,string::npos));
			else							endChrom = lexical_cast<int>(line.substr(found_start,found_end-found_start));
			
			if (endRegion>endChrom) endRegion = endChrom;
			
			getline(ifDNA, line);
			++nLine;
			
			numNucByLine =  numeric_cast<int>(line.size());
		}
		
		numNucCopy = endRegion - startRegion + 1; 
		numLine = startRegion/numNucByLine;
		startNuc = startRegion - (numNucByLine*numLine); 
		
		if (startNuc!=0){
			++numLine;
			--startNuc;
		} else {startNuc=numNucByLine-1;}
		
		while(cptLine<=numLine){
			getline(ifDNA, line);
			++cptLine;
			++nLine;
		}
		
		cerr << "end findRegion" << endl;
			
}

int main(int argc, char **argv) {

	char* fileReg = 0;
	char* fileDNA = 0;
	string rep_work;
	string espece;
	int margeR = 10000;
	bool keep = false;
	
	int opt;
	while ((opt = getopt(argc, argv, "R:D:W:S:M:k")) != -1) {
		switch (opt) {
 		case 'R': 
			fileReg = optarg;
			break;
		case 'D': 
			fileDNA = optarg;
			break;
		case 'W': 
			rep_work = boost::lexical_cast<string>(optarg);
			break;
		case 'S': 
			espece = boost::lexical_cast<string>(optarg);
			break;
		case 'M': 
			margeR = boost::lexical_cast<int>(optarg);
			break;
		case 'k': 
			keep = true;
			break;
		default: 
			cerr << "Usage: -R fileName -D fileName -W path -S specieName -M int -k" << endl;
			cerr << "-R fileName : file region" << endl;
			cerr << "-D fileName : file DNA" << endl;
			cerr << "-W fileName : path of rep work" << endl;
			cerr << "-S fileName : short name (ex : arabidopsis = ath)" << endl;
			cerr << "-M int : marge left and right of tandem region" << endl;
			cerr << "-k keep tblastx files" << endl;
			exit(EXIT_FAILURE);
		}
	}
	
	cerr << "end of reading param" << endl;
	
	vector<Region> vecRegion;
	
	ifstream ifReg(fileReg, ios::in);
	readFilesRegion(ifReg, vecRegion);
	ifReg.close();
	
	cerr << "end of reading Region file" << endl;
	
	std::sort(vecRegion.begin(), vecRegion.end(), compareRegion);
	
	cerr << "end sort Region" << endl;

	//Getting the karyotype information (added by ThomasF)
	map< int, int > mKaryotype;
	GetChromosomesInfos( espece, fileDNA, mKaryotype );	
	
	cerr << "begin reading of file " << fileReg << endl;
	ifstream ifDNA; 
	
	int indiceChrom = 0;
	int chrom = 0;
	int startRegion = 0;
	int endRegion = 0;
	int startUnite = 0;
	int endUnite = 0;
	int numNucByLine = 0;
	int numNucCopy = 0;
	int numLine = 0;
	int cptLine = 0;
	int startNuc = 0;
	int cptNuc = 0;
	int endChrom = 0;
	int nLine = 0;
	string line;
	string newLineRegion;
	string newLineUnite;
	string newLineTemp;
	
	int cptMinimiseUnite = 0;
	bool booMinimiseUnite = false;
	
	int cpt = 0;
	int margeU = 0;
	
	std::remove((rep_work+"/tandem.align.out").c_str());
	std::remove((rep_work+"/tandem.alignUse.out").c_str());
	std::remove((rep_work+"/tandem.chain.out").c_str());
	std::remove((rep_work+"/tandem.dupli.out").c_str());
	
	/* Now examining each region to detect the candidate units */
	/* The regions are sorted by chromosomes and by position */	
	for (vector<Region>::const_iterator itRegion = vecRegion.begin(); itRegion < vecRegion.end(); ++itRegion)
	{
		booMinimiseUnite = false;
		
		++cpt;
		
		cout << "###########################################################################################" << endl;
		cout<< "Chrom "<<itRegion->getChrom()<<" : "<<itRegion->getRegionStart()<<"-"<<itRegion->getRegionEnd()<<" size ("<<itRegion->getRegionEnd()-itRegion->getRegionStart()+1<<" bp)";
		cout<<", unite : "<<itRegion->getUniteStart()<<"-"<<itRegion->getUniteEnd()<<" size ("<<itRegion->getUniteEnd()-itRegion->getUniteStart()+1<<" bp)   region nb "<<cpt<<endl;
		cout<< "###########################################################################################" << endl;	
					
		chrom = 0;
		startRegion = 0;
		endRegion = 0;
		startUnite = 0;
		endUnite = 0;
				
		chrom = boost::lexical_cast<int>(itRegion->getChrom());
		startRegion = itRegion->getRegionStart() - margeR;
		endRegion = itRegion->getRegionEnd() + margeR;
		startUnite = itRegion->getUniteStart() - margeU;
		endUnite = itRegion->getUniteEnd() + margeU;

		int lastPos = 	mKaryotype[ chrom ];
		endRegion = ( endRegion > lastPos ) ? lastPos : endRegion;
		endUnite = 	( endUnite > lastPos ) ? lastPos : endUnite;			
				
		cerr<<"-----------------------"<<endl;
		cerr<<"Now performing region "<<cpt<<endl;
		cerr<<chrom<<":"<<startRegion<<"-"<<endRegion<<endl;
		cerr<<"-----------------------"<<endl;
						
		char * cmd_tblastx = new char[1024];
		//int cmd_ok  = snprintf(cmd_tblastx, 1024, "blastall -p tblastx -e 1e-5 -m 8 -i %s.unite.tmp%d -d %s.region.tmp%d -o %s/tblastx.tandem.out%d -F F",fileDNA,cpt,fileDNA,cpt,rep_work.c_str(),cpt);
		int cmd_ok  = snprintf(cmd_tblastx, 1024, "tblastx -db %s.region.tmp%d -query %s.unite.tmp%d -evalue 1e-5 -out %s/tblastx.tandem.out%d -outfmt 6 -seg no",fileDNA,cpt,fileDNA,cpt,rep_work.c_str(),cpt);
		if (cmd_ok >= 1024) {
			cerr << "Tblastx command line too long, please shorten paths." << endl;
        	exit(EXIT_FAILURE);
		}
		
		std::list<std::string> tblastx;
		tblastx.push_back("blastall");
		tblastx.push_back("-p");
		tblastx.push_back("tblastx");
		tblastx.push_back("-e");
		tblastx.push_back("1e-5");
		tblastx.push_back("-m");
		tblastx.push_back("8");
		tblastx.push_back("-i");
		tblastx.push_back(boost::lexical_cast<string>(fileDNA)+".unite.tmp"+boost::lexical_cast<string>(cpt));
		tblastx.push_back("-d");
		tblastx.push_back(boost::lexical_cast<string>(fileDNA)+".region.tmp"+boost::lexical_cast<string>(cpt));
		tblastx.push_back("-o");
		tblastx.push_back(rep_work+"/tblastx.tandem.out"+boost::lexical_cast<string>(cpt));
		tblastx.push_back("-F");
		tblastx.push_back("F");
		
		//Trying to replace tblastx by a very sensible glint (an online glint)
		//glint -M 
		char * cmd_glintx = new char[1024];
		cmd_ok  = snprintf(cmd_glintx, 1024, "./linux-64/glint --mismatch=-2 --gapop=-4 --gapext=-1 --strict -M 1301102110121 -m 8 -o %s/glintx.tandem.out%d %s.region.tmp%d %s.unite.tmp%d",rep_work.c_str(),cpt,fileDNA,cpt,fileDNA,cpt);
		if (cmd_ok >= 1024) {
			cerr << "GlintX command line too long, please shorten paths." << endl;
        	exit(EXIT_FAILURE);
		}
		
		std::list<std::string> glintx;
		glintx.push_back("./otherSoftware/glint/src/glint");
		glintx.push_back("--mismatch=-2");
		glintx.push_back("--gapop=-4");
		glintx.push_back("--gapext=-1");
		glintx.push_back("--strict");
		glintx.push_back("-M");
		glintx.push_back("1301102110121");
		glintx.push_back("-m");
		glintx.push_back("8");
		glintx.push_back("-o");
		glintx.push_back(rep_work+"/glintx.tandem.out"+boost::lexical_cast<string>(cpt));
		glintx.push_back(boost::lexical_cast<string>(fileDNA)+".region.tmp"+boost::lexical_cast<string>(cpt));
		glintx.push_back(boost::lexical_cast<string>(fileDNA)+".unite.tmp"+boost::lexical_cast<string>(cpt));
		
		char * cmd_glint = new char[1024];
		cmd_ok  = snprintf(cmd_glint, 1024, "./linux-64/glint -S --tandem 400000 --strict -C 2 -w 2 -d 2 -c 15 -X 20 -s 25 -m 8 --silent -o %s.unite.glint.tmp%d %s.unite.tmp%d",fileDNA,cpt,fileDNA,cpt);
		if (cmd_ok >= 1024) {
			cerr << "Glint command line too long, please shorten paths." << endl;
        	exit(EXIT_FAILURE);
		}
		std::list<std::string> glint;
		glint.push_back("./otherSoftware/glint/src/glint");
		glint.push_back("-S");
		glint.push_back("--tandem");
		glint.push_back("400000");
		glint.push_back("--strict");
		glint.push_back("-C");
		glint.push_back("2");
		glint.push_back("-w");
		glint.push_back("2");
		glint.push_back("-d");
		glint.push_back("2");
		glint.push_back("-c");
		glint.push_back("15");
		glint.push_back("-X");
		glint.push_back("20");
		glint.push_back("-s");
		glint.push_back("25");
		glint.push_back("-m");
		glint.push_back("8");
		glint.push_back("--silent");
		glint.push_back("-o");
		glint.push_back(boost::lexical_cast<string>(fileDNA)+".unite.glint.tmp"+boost::lexical_cast<string>(cpt));
		glint.push_back(boost::lexical_cast<string>(fileDNA)+".unite.tmp"+boost::lexical_cast<string>(cpt));
		
		//formatdb -p F -i dna.fa.region.tmp
		char * cmd_formatdb = new char[1024];
		//cmd_ok  = snprintf(cmd_formatdb, 1024, "formatdb -pF -i %s.region.tmp%d",fileDNA,cpt);
		cmd_ok  = snprintf(cmd_formatdb, 1024, "makeblastdb -dbtype nucl -in %s.region.tmp%d",fileDNA,cpt);
		if (cmd_ok >= 1024) {
			cerr << "Formatdb command line too long, please shorten paths." << endl;
        	exit(EXIT_FAILURE);
		}

		std::list<std::string> formatdb;
		formatdb.push_back("formatdb");
		formatdb.push_back("-p");
		formatdb.push_back("F");
		formatdb.push_back("-i");
		formatdb.push_back(boost::lexical_cast<string>(fileDNA)+".region.tmp"+boost::lexical_cast<string>(cpt));
		
		// ReD_exec -I files -E 7 -S ath -A 3 -F 4 -C 0 -H 0 -R 1 -M 1 -T 2 -v -m -e -n -s -p -L 1 -O  
		//./build/src/ReD_exec -I ./arathTest/tblastx.tandem.forRed1 -E 7 -S arath -A 3 -F 4 -C 0 -H 0 -R 1.5 -M 1 -T 3 -u -v -m -n -s -p -L 1 -O ./arathTest/tandem
		char * cmd_red = new char[1024];
		cmd_ok  = snprintf(cmd_red, 1024, "./linux-64/ReD_exec -I %s/tblastx.tandem.forRed%d -E 7 -S %s -A 3 -F 4 -C 0 -H 0 -R 1.5 -M 1 -T 3 -u -v -m -n -s -p -L 1 -O %s/tandem",rep_work.c_str(),cpt,espece.c_str(),rep_work.c_str());
		if (cmd_ok >= 1024) {
			cerr << "ReD command line too long, please shorten paths." << endl;
        	exit(EXIT_FAILURE);
		}
			
		std::list<std::string> red;
		red.push_back("./build/src/ReD_exec");
		red.push_back("-I");
		red.push_back(rep_work+"/tblastx.tandem.forRed"+boost::lexical_cast<string>(cpt));
		red.push_back("-E");
		red.push_back("7");
		red.push_back("-S");
		red.push_back(espece);
		red.push_back("-A");
		red.push_back("3");
		red.push_back("-F");
		red.push_back("4");
		red.push_back("-C");
		red.push_back("0");
		red.push_back("-H");
		red.push_back("0");
		red.push_back("-R");
		red.push_back("1.5");
		red.push_back("-M");
		red.push_back("-1");
		red.push_back("-T");
		red.push_back("3");
		//red.push_back("-o");
		red.push_back("-u");
		red.push_back("-v");
		red.push_back("-m");
		red.push_back("-n");
		//red.push_back("-e");
		red.push_back("-s");
		red.push_back("-p");
		red.push_back("-L");
		red.push_back("1");
		red.push_back("-O");
		red.push_back(rep_work+"/tandem");
				
		if (startRegion<1) startRegion = 1;
		
		indiceChrom = 0;
		numNucByLine = 0;
		numNucCopy = 0;
		numLine = 0;
		cptLine = 0;
		startNuc = 0;
		cptNuc = 0;
		endChrom = 0;
		nLine = 0;
		ifDNA.open(fileDNA, ios::in);
		findRegion(ifDNA, chrom, startRegion, endRegion, numNucByLine, numNucCopy, numLine, cptLine, startNuc, endChrom, nLine, espece);
		
		char fileOutUnite[2000];
		sprintf(fileOutUnite, "%s%s%i", fileDNA, ".unite.tmp", cpt);
		ofstream chain_file_Unite(fileOutUnite);
		
		chain_file_Unite << ">" << espece << chrom << "_" << startUnite << "-" << endUnite << "\n";
		
		cptNuc = startRegion;
		newLineUnite.erase();
		if(cptNuc<startUnite && cptNuc+(numNucByLine-startNuc) < startUnite){
			
		} else {
			if (cptNuc>=startUnite && cptNuc+(numNucByLine-startNuc) <= endUnite){
				newLineUnite.insert(0,line.substr(startNuc,string::npos));
			} else {
				for(int cpt = 0;cpt<=numNucByLine-startNuc; ++cpt){
					if (cptNuc+cpt>=startUnite && cptNuc+cpt<=endUnite) {
						newLineUnite.insert(newLineUnite.end(), line[cpt]);
					} 
				}
			}
		}
		
		cptNuc += numNucByLine-startNuc;
		
		while (cptNuc+numNucByLine <= endRegion) {
			getline(ifDNA, line);
			++nLine;
			if ( (cptNuc<startUnite && cptNuc+numNucByLine<startUnite) || cptNuc>endUnite) {
				
			} else {
				if ( cptNuc>=startUnite && cptNuc+numNucByLine<=endUnite) {
					newLineUnite.insert(newLineUnite.size(),line);
				} else {
					for(int cpt = 1;cpt<=numNucByLine; ++cpt){
						if (cptNuc+cpt>=startUnite && cptNuc+cpt<=endUnite) {
							newLineUnite.insert(newLineUnite.end(), line[cpt-1]);
						} 
					}
				}
			}
			
			cptNuc += numNucByLine;
			
			while (newLineUnite.size()>=lexical_cast<size_t>(numNucByLine)){
				newLineTemp.erase();
				newLineTemp.insert(0, newLineUnite, 0,numNucByLine);
				chain_file_Unite << newLineTemp << "\n";
				newLineTemp.erase();
				newLineTemp.insert(newLineTemp.size(), newLineUnite, numNucByLine,string::npos);
				newLineUnite.erase();
				newLineUnite.insert(0, newLineTemp);
			}			
		}
		
		if(cptNuc>endUnite){
			
		} else {
			if (endRegion==endUnite){
				newLineUnite.insert(newLineUnite.size(),line.substr(0,endRegion-cptNuc+1));
			} else {
				for(int cpt = 0;cpt<=endRegion-cptNuc+1; ++cpt){
					if (cptNuc+cpt>=startUnite && cptNuc+cpt<=endUnite) {
						newLineUnite.insert(newLineUnite.end(), line[cpt]);
						
					} 
				}
			}
		}
		
		while (newLineUnite.size()>0){
			newLineTemp.erase();
			newLineTemp.insert(0, newLineUnite, 0,numNucByLine);
			chain_file_Unite << newLineTemp << "\n";
			newLineTemp.erase();
			if (newLineUnite.size()>lexical_cast<size_t>(numNucByLine)){
				newLineTemp.insert(0, newLineUnite, numNucByLine,string::npos);
				newLineUnite.erase();
				newLineUnite.insert(0, newLineTemp);
			}else{
				newLineUnite.erase();
			}
		}
		
		chain_file_Unite.close();
		ifDNA.close();
		
		cmd_ok = system(cmd_glint);
		if (cmd_ok != 0) {
			cerr << "glint 1: cannot execute." << endl;
			exit(EXIT_FAILURE);
		}
		
		unsigned int sUnit;
		unsigned int eUnit;
		
		try{
			 sUnit = numeric_cast<unsigned int>(startUnite);
			 eUnit = numeric_cast<unsigned int>(endUnite);
		} catch (boost::bad_numeric_cast& e) {
			cerr << "regionTandem -> main.cpp -> main() bad numeric cast position unit : " << endl;
			cerr << e.what() << endl;
			cerr << startUnite << " # " << endUnite << endl;
			exit(EXIT_FAILURE);
		}
		
		while ( readFilesGlint ((boost::lexical_cast<string>(fileDNA)+".unite.glint.tmp"+boost::lexical_cast<string>(cpt)).c_str(), 
								sUnit, eUnit, 50, fileOutUnite)) {
			cout << cpt << " : duplication unit reduction" << endl;

			cmd_ok = system(cmd_glint);
			if (cmd_ok != 0) {
				cerr << "glint 2: cannot execute." << endl;
				exit(EXIT_FAILURE);
			}
			
			if (!booMinimiseUnite) {
				cout << "diminue : " << endl;
				cout << chrom << "_" << startUnite << "-" << endUnite << endl;
				booMinimiseUnite = true;
				++cptMinimiseUnite;
			}
		}
		
		if (booMinimiseUnite) {
			cout << chrom << "_" << sUnit << "-" << eUnit << endl;
			startUnite = numeric_cast<int>(sUnit);
			endUnite = numeric_cast<int>(eUnit);
		}
				
		std::remove((boost::lexical_cast<string>(fileDNA)+".unite.glint.tmp"+boost::lexical_cast<string>(cpt)).c_str());
		
		//##################### write region fasta
		
		indiceChrom = 0;
		numNucByLine = 0;
		numNucCopy = 0;
		numLine = 0;
		cptLine = 0;
		startNuc = 0;
		cptNuc = 0;
		endChrom = 0;
		nLine = 0;
		ifDNA.open(fileDNA, ios::in);
		findRegion(ifDNA, chrom, startRegion, endRegion, numNucByLine, numNucCopy, numLine, cptLine, startNuc, endChrom, nLine, espece);
				
		char fileOutRegion[2000];
		sprintf(fileOutRegion, "%s%s%i", fileDNA, ".region.tmp", cpt);
		ofstream chain_file_Region(fileOutRegion);
		
		chain_file_Region << ">" << espece << chrom << "_" << startRegion << "-" << endRegion << "\n";
		
		cptNuc = startRegion;
		newLineRegion.erase();
		
		if(cptNuc<startUnite && cptNuc+(numNucByLine-startNuc) < startUnite){
			newLineRegion.insert(0,line.substr(startNuc,string::npos));
		} else {
			if (cptNuc>=startUnite && cptNuc+(numNucByLine-startNuc) <= endUnite){
				
			} else {
				for(int cpt = 0;cpt<=numNucByLine-startNuc; ++cpt){
					if (cptNuc+cpt>=startUnite && cptNuc+cpt<=endUnite) {
						newLineRegion.insert(newLineRegion.end(), 'N');
					} else {
						newLineRegion.insert(newLineRegion.end(), line[cpt]);
					}
				}
			}
		}
		
		cptNuc += numNucByLine-startNuc;
		
		while (cptNuc+numNucByLine <= endRegion) {
			getline(ifDNA, line);
			++nLine;
			if ( (cptNuc<startUnite && cptNuc+numNucByLine<startUnite) || cptNuc>endUnite) {
				newLineRegion.insert(newLineRegion.size(),line);
			} else {
				if ( cptNuc>=startUnite && cptNuc+numNucByLine<=endUnite) {
					newLineRegion.insert (newLineRegion.size(),numNucByLine,'N');
				} else {
					for(int cpt = 1;cpt<=numNucByLine; ++cpt){
						if (cptNuc+cpt>=startUnite && cptNuc+cpt<=endUnite) {
							newLineRegion.insert(newLineRegion.end(), 'N');
						} else {
							newLineRegion.insert(newLineRegion.end(), line[cpt-1]);
						}
					}
				}
			}
			
			cptNuc += numNucByLine;
			
			while (newLineRegion.size()>= lexical_cast<size_t>(numNucByLine)){
				newLineTemp.erase();
				newLineTemp.insert(0, newLineRegion, 0,numNucByLine);
				chain_file_Region << newLineTemp << "\n";
				newLineTemp.erase();
				newLineTemp.insert(newLineTemp.size(), newLineRegion, numNucByLine,string::npos);
				newLineRegion.erase();
				newLineRegion.insert(0, newLineTemp);
			}
						
		}
		
		if(cptNuc>endUnite){
			
		} else {
			if (endRegion==endUnite){
				
			} else {
				for(int cpt = 0;cpt<=endRegion-cptNuc+1; ++cpt){
					if (cptNuc+cpt>=startUnite && cptNuc+cpt<=endUnite) {
						newLineRegion.insert(newLineRegion.end(), 'N');
					} else {
						newLineRegion.insert(newLineRegion.end(), line[cpt]);
					}
				}
			}
		}
		
		while (newLineRegion.size()>0){
			newLineTemp.erase();
			newLineTemp.insert(0, newLineRegion, 0,numNucByLine);
			chain_file_Region << newLineTemp << "\n";
			newLineTemp.erase();
			if (newLineRegion.size()>lexical_cast<size_t>(numNucByLine)){
				newLineTemp.insert(0, newLineRegion, numNucByLine,string::npos);
				newLineRegion.erase();
				newLineRegion.insert(0, newLineTemp);
			}else{
				newLineRegion.erase();
			}
		}
		
		chain_file_Region.close();
		ifDNA.close();
		
		//##################### execute tblastx
		cerr<<"starting tblastx and glintx"<<endl;
		cmd_ok = system(cmd_formatdb);
		cmd_ok |= system(cmd_tblastx);
		if (cmd_ok != 0) {
			cerr << "formatdb/tblastx: cannot execute." << endl;
			exit(EXIT_FAILURE);
		}
		
		cmd_ok = system(cmd_glintx);
		if (cmd_ok != 0) {
			cerr << "glintx: cannot execute." << endl;
			exit(EXIT_FAILURE);
		}
				
		ofstream ofBlast((rep_work+"/tblastx.tandem.forRed"+boost::lexical_cast<string>(cpt)).c_str());
		ofBlast << "Query id\tSubject id\tpIdentity\tlength\tmismatches\tgaps\tq_start\tq_end\ts_start\ts_end\te-value\tscore\n";
		
		ifstream ifBlast((rep_work+"/tblastx.tandem.out"+boost::lexical_cast<string>(cpt)).c_str());
		while(getline(ifBlast, line)){ ofBlast << line << "\n";}
		ifBlast.close();
			
		ifstream ifGlintX((rep_work+"/glintx.tandem.out"+boost::lexical_cast<string>(cpt)).c_str());
		while(getline(ifGlintX, line)){ ofBlast << line << "\n";}
		ifGlintX.close();
		
		ofBlast.close();
			
		cout<<"red command"<<endl;
		for (list<string>::iterator i = red.begin(); i!= red.end(); i++ )
		{
			cout<<(*i)<<" ";	
		}
		cout<<"\n";	

		cerr << "starting ReD" << endl;
		cmd_ok = system(cmd_red);
		if (cmd_ok != 0) {
			cerr << "ReD: cannot execute." << endl;
			exit(EXIT_FAILURE);
		}
		cerr << "ending ReD" << endl;
		
		std::remove((boost::lexical_cast<string>(fileDNA)+".unite.tmp"+boost::lexical_cast<string>(cpt)).c_str());
		std::remove((boost::lexical_cast<string>(fileDNA)+".unite.glint.tmp"+boost::lexical_cast<string>(cpt)).c_str());
		std::remove((boost::lexical_cast<string>(fileDNA)+".region.tmp"+boost::lexical_cast<string>(cpt)).c_str());
		std::remove((boost::lexical_cast<string>(fileDNA)+".region.tmp"+boost::lexical_cast<string>(cpt)+".nhr").c_str());
		std::remove((boost::lexical_cast<string>(fileDNA)+".region.tmp"+boost::lexical_cast<string>(cpt)+".nin").c_str());
		std::remove((boost::lexical_cast<string>(fileDNA)+".region.tmp"+boost::lexical_cast<string>(cpt)+".nsq").c_str());
		std::remove((boost::lexical_cast<string>(fileDNA)+".region.tmp"+boost::lexical_cast<string>(cpt)+".phr").c_str());
		std::remove((boost::lexical_cast<string>(fileDNA)+".region.tmp"+boost::lexical_cast<string>(cpt)+".psq").c_str());
		std::remove((rep_work+"/tblastx.tandem.out"+boost::lexical_cast<string>(cpt)).c_str());
		std::remove((rep_work+"/glintx.tandem.out"+boost::lexical_cast<string>(cpt)).c_str());
		if (!keep) std::remove((rep_work+"/tblastx.tandem.forRed"+boost::lexical_cast<string>(cpt)).c_str());
				
	}
	
	cout << "number of minimise unite : " << cptMinimiseUnite << endl;
	
}
