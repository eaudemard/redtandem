#ifndef UTIL_C_
#define UTIL_C_

#include "./data/Region.h"
#include <boost/lexical_cast.hpp>

using namespace boost;
using namespace std;

bool compareRegion(const Region& R1, const Region& R2) {
	
	int R1_Chrom = -1;
	int R2_Chrom = -1;
	
	try {
    	R1_Chrom = boost::lexical_cast<int>(R1->getChrom());
    } catch(boost::bad_lexical_cast &) { 
    	if (R1->getChrom().compare("X") == 0) R1_Chrom = 50;
    	if (R1->getChrom().compare("Y") == 0) R1_Chrom = 51;
    	if (R1->getChrom().compare("MT") == 0) R1_Chrom = 52;
    	
    	if (R1_Chrom == -1) R1_Chrom = 100;
    }
    
    try {
    	R2_Chrom = boost::lexical_cast<int>(R2->getChrom());
    } catch(boost::bad_lexical_cast &) { 
    	if (R2->getChrom().compare("X") == 0) R2_Chrom = 50;
    	if (R2->getChrom().compare("Y") == 0) R2_Chrom = 51;
    	if (R2->getChrom().compare("MT") == 0) R2_Chrom = 52;
    	
    	if (R2_Chrom == -1) R2_Chrom = 100;
    }
	
	if ( R1_Chrom < R2_Chrom )
			return true;
	if ( R1_Chrom > R2_Chrom )
			return false;
	
	if (R1.getRegionStart() < R2.getRegionStart())
		return true;
		
	return false;
	
}

#endif /*UTIL_C_*/