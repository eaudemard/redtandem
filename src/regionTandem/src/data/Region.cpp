#include "Region.h"

namespace data{
	
	Region::Region(const string& l, const vector<int>& indice){

		vector<string> v;
		boost::split(v, l, boost::is_any_of("\t"),boost::algorithm::token_compress_off);
		
		chrom = (indice[0]!=-1)?v.at(indice[0]):"-1";
		
		try
        {
        	r_start = (indice[2]!=-1)? boost::lexical_cast<int>(v.at(indice[1])):-1;
			r_end = (indice[3]!=-1)? boost::lexical_cast<int>(v.at(indice[2])):-1;
			
			u_start = (indice[4]!=-1)? boost::lexical_cast<int>(v.at(indice[3])):-1;
			u_end = (indice[5]!=-1)? boost::lexical_cast<int>(v.at(indice[4])):-1;
			
        }
        catch(boost::bad_lexical_cast &) {
			cout << "regionTandem -> region.cpp -> Region : le fichier est dans un mauvais format. Exception sur un cast" << endl;
			cout << l << endl;
			exit(EXIT_FAILURE);
					
		}
		
		if (r_start>r_end){
			std::swap(r_start,r_end);
			r_swap = true;
		} else { r_swap = false;}
		
		if (u_start>u_end){
			std::swap(u_start,u_end);
			u_swap = true;
		} else { u_swap = false;}
		
	}
	

	Region::~Region()
	{
	}
}

