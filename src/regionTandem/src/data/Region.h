#ifndef REGION_H_
#define REGION_H_

#include <string>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>

using namespace std;

namespace data{
	class Region
	{
	public :
		string chrom;
		int r_start;
		int r_end;
		int u_start;
		int u_end;
		
		bool r_swap;
		bool u_swap;
		
	public:
		Region(const string& l, const vector<int>& indice);
		
		inline string getChrom() const		{	return chrom;	}
		inline int getRegionStart() const	{	return r_start;	}
		inline int getRegionEnd() const		{	return r_end;	}
		inline int getUniteStart() const	{	return u_start;	}
		inline int getUniteEnd() const		{	return u_end;	}
		
		virtual ~Region();
	};
}

#endif /*REGION_H_*/
