#ifndef ANCHOR_H_
#define ANCHOR_H_

#include <string>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include "./Region.h"

using namespace std;

namespace data{
	
	class Anchor
	{
		unsigned int id;
		
		string q_name;
		string q_prot;
		string q_chrom;
		int q_start;
		int q_end;
		int q_cov;
		
		string s_name;
		string s_prot;
		string s_chrom;
		int s_start;
		int s_end;
		int s_cov;
		
		int size;
		
		bool swapSubject;
		bool swapQuery;
		
		double score;
		double identity;
		double evalue;
		
		int compConnexe;
		
		Region* regionQuery;
		Region* regionSubject;
		
		unsigned int idGraph;
		unsigned int numVertexGraph;
		
	public:
		Anchor(const string& l, const vector<int>& indice, const unsigned int idAnchor, unsigned int numVertex = 0);
		Anchor(const string& sQ_name, const string& sQ_chrom, int iQ_start, int iQ_end, double dQ_cov, 
				const string& sS_name, const string& sS_chrom, int iS_start, int iS_end, double dS_cov,
				double dIdentity, double dEvalue, int iScore, bool bSwap, int idAnchor);
				
		void changeAnchor(const string& sQ_name, const string& sQ_chrom, int iQ_start, int iQ_end, double dQ_cov, 
							const string& sS_name, const string& sS_chrom, int iS_start, int iS_end, double dS_cov,
							double dIdentity, double dEvalue, int iScore, bool bSwap);
		
		inline void setId(unsigned int i)		{	id = i;					}
		inline unsigned int getId() const		{	return id;				}
		inline string getQueryChrom() const		{	return q_chrom;			}
		inline string getQueryProt() const		{	return q_prot;			}
		inline string getQueryName() const		{	return q_name;			}
		inline int getQueryStart() const		{	return q_start;			}
		inline int getQueryEnd() const			{	return q_end;			}
		inline bool getQuerySwap() const		{	return swapQuery;		}
		inline double getIdentity() const		{	return identity;		}
		inline double getEvalue() const			{	return evalue;			}
		inline int getCoverage() const			{	return (s_cov+q_cov)/2;	}
		inline int getSize() const				{	return size;			}
		
		inline string getSubjectChrom() const	{	return s_chrom;			}
		inline string getSubjectProt() const	{	return s_prot;			}
		inline string getSubjectName() const	{	return s_name;			}
		inline int getSubjectStart() const		{	return s_start;			}
		inline int getSubjectEnd() const		{	return s_end;			}
		inline bool getSubjectSwap() const		{	return swapSubject;		}
		
		inline string getStrand() const			{	return (swapQuery==swapSubject)? "+":"-";		}
		inline double getScore() const			{	return score;									}
		inline void setScore(double s) 			{	score = s;										}
		
		inline int getQueryPosMin() const		{	return (q_start<q_end)?	 q_start: q_end;		}
		inline int getQueryPosMax() const		{	return (q_start<q_end)?	 q_end: q_start;		}
		
		inline int getLengthQuery() const		{	return getQueryPosMax() - getQueryPosMin() + 1;		}
		inline int getLengthSubject() const		{	return getSubjectPosMax() - getSubjectPosMin() + 1;	}
		
		
		inline int getSubjectPosMin() const		{	return (s_start<s_end)?	 s_start: s_end;		}
		inline int getSubjectPosMax() const		{	return (s_start<s_end)?	 s_end: s_start;		}
		
		inline Region* getRegionQuery() const	{	return regionQuery;		}
		inline Region* getRegionSubject() const	{	return regionSubject;	}
		
		inline void setRegionQuery(Region* region)				{ regionQuery = region;					}
		inline void setRegionSubject(Region* region)			{ regionSubject = region;				}
		
		inline int getDistDiag() const 			{	return abs(getQueryPosMin()-getSubjectPosMin());	}
		int getDistBetweenAnchor() const;
		
		
		inline int getIdGraph() const							{	return idGraph;				}
		inline void setIdGraph(unsigned int id)					{	idGraph = id;				}
		
		inline unsigned int getNumVertexGraph() const			{	return numVertexGraph;		}
		inline void setNumVertexGraph(unsigned int numV)		{	numVertexGraph = numV;		}
		
		double pOverlapSubject (const Anchor& A) const;
		double pOverlapSubjectQuery (const Anchor& A) const;
		double pOverlapQuery (const Anchor& A) const;
		double pOverlapQuerySubject (const Anchor& A) const;
		double getPente () const;
		
		void affiche() const;
		void printAnchor(ofstream& ofBlastp) const;
		
		virtual ~Anchor();
	};
}

#endif /*ANCHOR_H_*/
