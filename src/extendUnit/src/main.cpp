#include <iostream>
#include <fstream>
#include <vector>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>

#include <boost/algorithm/string.hpp>
#include "data/Region.h"
#include <boost/lexical_cast.hpp>
#include <boost/numeric/conversion/cast.hpp>


using namespace std;
using namespace boost;
using namespace data;

void testVecRegion (vector<Region> vecReg){
	for (vector<Region>::iterator itR = vecReg.begin(); itR < vecReg.end(); ++itR){
		cout << itR->getId() << " _ " << itR->getChrom() << " _ " << itR->getStart() << " _ " << itR->getEnd() << endl; 
	}
}

bool compareRegion(const Region& R1, const Region& R2) {
		
	if (R1.getPosMin() > R2.getPosMin())
		return false;
	
	return true;
}


int castStringToInt(string text){

	int i = 0;

	while (text.at(0)==' '){
		text.erase(0,1);	
	}

	try 
	{
		i = lexical_cast<int>(text);
	} 
	catch(boost::bad_lexical_cast &) {
		cout << "resultat -> main.cpp -> castStringToInt()" << endl;
		cout << text << endl;
		exit(EXIT_FAILURE);		
	}
	
	return i;
			
}

string castIntToString(int i){

	string text = "";
	try 
	{
		text = lexical_cast<string>(i);
	} 
	catch(boost::bad_lexical_cast &) {
		cout << "resultat -> main.cpp -> castIntToString()" << endl;
		cout << i << endl;
		exit(EXIT_FAILURE);		
	}
	
	return text;
			
}

double castStringToDouble(string text){

	double d = 0;

	while (text.at(0)==' '){
		text.erase(0,1);	
	}

	try 
	{
		d = lexical_cast<double>(text);
	} 
	catch(boost::bad_lexical_cast &) {
		cout << "resultat -> main.cpp -> castStringToDouble()" << endl;
		cout << text << endl;
		exit(EXIT_FAILURE);		
	}
	
	return d;
			
}

void initIndiceCol(vector<int>& indiceCol, int nbCol){

	indiceCol.clear();
	for (int i=0; i<nbCol; ++i){
		indiceCol.push_back(-1);	
	} 
	
}

void headerRegion (vector<string>& col, vector<int>& indiceCol){
	initIndiceCol(indiceCol,7);
	
	for (int i=0; i<lexical_cast<int>(col.size()); i++){
			if (col.at(i) == "chrom"){ 
				indiceCol.at(0) = i;
			} else if (col.at(i) == "start"){ 
				indiceCol.at(1) = i;
			} else if (col.at(i) == "end"){ 
				indiceCol.at(2) = i;
			} else if (col.at(i) == "u_start"){ 
				indiceCol.at(3) = i;
			} else if (col.at(i) == "u_end"){ 
				indiceCol.at(4) = i;
			} else if (col.at(i) == "numDupli"){ 
				indiceCol.at(5) = i;
			} else if (col.at(i) == "dupli"){ 
				indiceCol.at(6) = i;
			}
		}
}

int findDist(vector<Region>& vecRegion)	{ 
	cout << "\t start find dist" << endl;
	int dist = std::numeric_limits<int>::max();
	int longestUnit = 0;
	
	int precCenter = 0;
	int center = 0;
		
	for(vector<Region>::const_iterator it = vecRegion.begin(); it!=vecRegion.end(); ++it){
		center = it->getCenter();
		if ( it->getLength() > longestUnit ) longestUnit = it->getLength();
		if (precCenter!=0 && abs(center-precCenter)<dist) dist = abs(center-precCenter);
		precCenter = center;
	}
	
	if (dist > (longestUnit+longestUnit/2) ) {
		dist = longestUnit + longestUnit/2 ;
	} 

	cout << "\t end find dist" << endl;
	return dist;	
}


void extendUnit (ifstream& ifReg, ofstream& ofExtend) {
	
	string line;
	getline(ifReg, line);

	vector<string> col;
	vector<string> col2;
	vector<string> col3;
	vector<int> indiceCol (7,-1);
	
	int idReg = -1;
	int center = 0;
	int dist = 0;
	
	split(col, line, is_any_of("\t"),algorithm::token_compress_off);
		
	if (!col.empty()){
		headerRegion(col, indiceCol);
		cout << "end read header region" << endl;	

		ofExtend << "#chrom\tstart\tend\tu_start\tu_end\tnumDupli\tdupli\n";
	
		while(getline(ifReg, line)){
			split(col, line, is_any_of("\t"),algorithm::token_compress_off);
			split(col2, col.at(indiceCol.at(6)), is_any_of(","),algorithm::token_compress_off);
			
			vector<Region> vecRegion;

			for (unsigned int i=0; i<col2.size(); ++i){
				if (col2.at(i).size()>1){
					split(col3, col2.at(i), is_any_of(".."),algorithm::token_compress_on);
						
					try
		        		{
						vecRegion.push_back(Region(boost::lexical_cast<int>(col3.at(0)), boost::lexical_cast<int>(col3.at(1)), col.at(0), idReg));
						++idReg;
		        		}
		        		catch(boost::bad_lexical_cast &) {
		        			cout << "resultat -> main.cpp -> readFiles() : le fichier est dans un mauvais format. Exception sur un cast au niveau des region tandem"<<endl;
		        			cout << col3.size() << endl;
		        			cout << col2.at(i) << endl;
		        			cout << col3.at(0) << endl;
		        			cout << col3.at(1) << endl;
						exit(EXIT_FAILURE);	
		        		}
				}
			}

			sort(vecRegion.begin(), vecRegion.end(), compareRegion);

			dist = findDist(vecRegion);
			
			ofExtend << col.at(0) << "\t"; 

			center = vecRegion.at(0).getCenter();
			if (center-dist/2<1) center = 1;
			else center = center-dist/2;
			ofExtend << center << "\t";
		
			center = (vecRegion.at(vecRegion.size()-1)).getCenter();
			center = center+dist/2;
			ofExtend << center << "\t" << col.at(3) << "\t" << col.at(4) << "\t" << col.at(5) << "\t";
		
			for(vector<Region>::const_iterator it = vecRegion.begin(); it!=vecRegion.end(); ++it){
				center = it->getCenter();
				if (center-dist/2<1) ofExtend << 1 << ".." << center+dist/2 << ",";
				else ofExtend << center-dist/2 << ".." << center+dist/2 << ",";
			}
			
			ofExtend << "\n";

		}
					
	} else {
		cerr << "error bad numer on parameter -T" << endl;
		exit(EXIT_FAILURE);
	}

}
	



int main(int argc, char **argv) {

	cout << "read extend unit" << endl;
	char* fileIn = 0;
	char* fileOut = 0;
	cout << "start read param" << endl;
	
	int opt;
	while ((opt = getopt(argc, argv, "I:O:")) != -1) {
		switch (opt) {
 		case 'I': 
			fileIn = optarg;
			break;
		case 'O': 
			fileOut = optarg;
			break;
		default: 
			cerr << "Usage: -I name -O name " << endl;
			cerr << "-I name : file in" << endl;
			cerr << "-O name : file out" << endl;
			exit(EXIT_FAILURE);
		}
	}
	cout << "end read param" << endl;
	
	ifstream ifReg(fileIn, ios::in);
	ofstream ofExtend(fileOut);

	extendUnit(ifReg, ofExtend);
		
	ifReg.close();
	ofExtend.close();
	
	cout << "end extend unit" << endl;
	
}
	
