#ifndef REGION_H_
#define REGION_H_

#include <string>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>

using namespace std;

namespace data{
	
	class Region
	{
	
	int id;
	string chrom;
		
	int start;
	int end;
			
	public:
		Region(int iStart, int iEnd, const string sChrom, int idRegion);
		
		inline int getId() const		{	return id;					}
		inline string getChrom() const		{	return chrom;					}
		inline int getStart() const		{	return start;					}
		inline int getEnd() const		{	return end;					}
		inline int getLength() const		{	return getPosMax()-getPosMin()+1;		}
		inline int getCenter() const		{	return (end+start)/2;				}
		
		inline int getPosMax() const		{	return (start > end)? start : end;		}
		inline int getPosMin() const		{	return (start < end)? start : end;		}
		
		double pOverlap (const Region& R) const;
		
		void affiche() const;
		void print(ofstream& sortie) const;
		
		virtual ~Region();
		
	};
}

#endif /*REGION_H_*/
