#include "Region.h"

namespace data{
	
		
	Region::Region(int iStart, int iEnd, const string sChrom, int idRegion){

		//cout << "Region bis" << endl;
		
		id = idRegion;

		chrom = sChrom;
		start = iStart;
		end = iEnd;
		
	}
	
	double Region::pOverlap (const Region& R) const {
		
		int start = 0;
		int end = 0;
		
		if (R.getChrom().compare(getChrom()) == 0) {
			//cout << "toto" << endl;
			start = (R.getStart()<getStart()) ?  getStart() : R.getStart();
			end =  (R.getEnd()>getEnd()) ? getEnd() : R.getEnd();
			if( (getEnd()-getStart())!=0 )	return ((double)(end-start))/(getEnd()-getStart());
			else {
				cout << "1 div 0" << endl;
				affiche();
				R.affiche();
				return 0;
			}
		}
		
		return 0;	
	}
	
	void Region::affiche() const {
		cout << "Region " << id;
		cout << " - chrom " << chrom;
		cout << " - start " << start;
		cout << " - end " << end;
		
		cout << endl;
	}
	
	void Region::print(ofstream& sortie) const {
		sortie << "Region " << id;
		sortie << " - chrom " << chrom;
		sortie << " - start " << start;
		sortie << " - end " << end;
				
		sortie << "\n";
	}
	

	Region::~Region()
	{
		
	}

}
