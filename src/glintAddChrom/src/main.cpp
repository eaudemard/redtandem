#include <iostream>
#include <fstream>
#include <vector>
#include <getopt.h>
#include <string.h>
#include <stdlib.h>

#include <boost/algorithm/string.hpp>


using namespace std;
using namespace boost;

int main(int argc, char **argv) {

	char* fileGlint = 0;
	char* fileGlintOut = 0;
	char* espece = 0;
	cout << "read param" << endl;
	
	int opt;
	while ((opt = getopt(argc, argv, "G:O:S:")) != -1) {
		switch (opt) {
 		case 'G': 
			fileGlint = optarg;
			break;
		case 'O': 
			fileGlintOut = optarg;
			break;
		case 'S': 
			espece = optarg;
			break;
		default: 
			cerr << "Usage: -G name -O name -S espece" << endl;
			cerr << "-G name : file glint in input" << endl;
			cerr << "-O name : file glint with chrom in output" << endl;
			cerr << "-S name : name of espece use in glint file. Exemple : arabidopsis thaliana is ath" << endl;
			exit(EXIT_FAILURE);
		}
	}
	cout << "end read param" << endl;
	
	cout << "begin change file" << endl;
	ifstream ifGlint(fileGlint, ios::in);
	ofstream ofGlint(fileGlintOut);
	string line;
	vector<string> col;
	size_t start = 0;
	size_t end = 0;
	string q_chrom = "";
	string s_chrom = "";
	
	ofGlint << "q_chrom\tq_name\tq_start\tq_end\ts_chrom\ts_name\ts_start\ts_end\tpIdentity\tevalue\tscore\n";
	while(getline(ifGlint, line)){		
		if (!line.empty()){
			split(col, line, is_any_of("\t"),algorithm::token_compress_off);
			
			start = col.at(0).find(espece);
			if (start == string::npos) {
				cout << "glintAddChrom -> main.cpp -> main() 1 : mauvais format, pas réussit à trouver : "<< espece << endl;	
				cout << col.at(0) << endl;
				ifGlint.close();
				ofGlint.close();
				exit(EXIT_FAILURE);
			}
			
			start += strlen(espece);
			end = col.at(0).find("_", start);
			if (end == string::npos) {
				end = col.at(0).length();
			}

			if (end == string::npos) {
				cout << "glintAddChrom -> main.cpp -> main() 2 : mauvais format, pas réussit à trouver _" << endl;	
				cout << col.at(0) << endl;
				ifGlint.close();
				ofGlint.close();
				exit(EXIT_FAILURE);
			}
			
			q_chrom = col.at(0).substr(start,end-start);
			
			if (q_chrom.at(0)=='M' || q_chrom.at(0)=='m') q_chrom = "100";
			
			if (q_chrom.at(0)=='C' || q_chrom.at(0)=='c') q_chrom = "101";
			
			ofGlint << q_chrom << "\t" << col.at(0) << "\t" << col.at(6) << "\t" << col.at(7);
			
			start = col.at(1).find(espece);
			if (start == string::npos) {
				cout << "glintAddChrom -> main.cpp -> main() 3 : mauvais format, pas réussit à trouver : "<< espece << endl;	
				cout << col.at(1) << endl;
				ifGlint.close();
				ofGlint.close();
				exit(EXIT_FAILURE);
			}
			
			start += strlen(espece);
			end = col.at(1).find("_", start);
			if (end == string::npos) {
				end = col.at(1).length();
			}
			
			if (end == string::npos) {
				cout << "glintAddChrom -> main.cpp -> main() 4 : mauvais format, pas réussit à trouver _" << endl;	
				cout << col.at(1) << endl;
				ifGlint.close();
				ofGlint.close();
				exit(EXIT_FAILURE);
			}
			
			s_chrom = col.at(1).substr(start,end-start);
			
			if (s_chrom.at(0)=='M' || s_chrom.at(0)=='m') s_chrom = "100";
	
			if (s_chrom.at(0)=='C' || s_chrom.at(0)=='c') s_chrom = "101";
		
			ofGlint << "\t" << s_chrom << "\t" << col.at(1) << "\t" << col.at(8) << "\t" << col.at(9);
			
			ofGlint << "\t" << col.at(2) << "\t" << col.at(10) << "\t" << col.at(11) << "\n";
		}
	}
	
	ifGlint.close();
	ofGlint.close();
	
	cout << "end change file" << endl;
	
}
	
