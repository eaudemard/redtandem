/*
   $Id: utils.cpp 263 2009-09-21 16:10:48Z tfaraut $
*/

#include <iostream>
#include <sstream>
#include <string>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <time.h>
#include <list>
#include <vector>
using namespace std;

#include "glint.h"
#include "utils.h"

static string g_strListFileName;
static bool g_bListFileAppend = false;

/****************************************************************************************/
/*                    Some utility functions                                            */
/****************************************************************************************/

Uint4 String2Uint4(const string& value)
{
	Uint4 temp;
	stringstream ss;
	ss<<value;
	ss>>temp;

	return temp;
}

Uint2 String2Uint2(const string& value)
{
	Uint2 temp;
	stringstream ss;
	ss<<value;
	ss>>temp;

	return temp;
}

char String2Char(const string& value)
{
	char temp;
	stringstream ss;
	ss<<value;
	ss>>temp;

	return temp;
}

FILE * open_file(const char *prefixe_name, const char *extension, const char* mode)
{
	FILE *pf;
	char *filename;

	filename=(char*)malloc((strlen(prefixe_name)+strlen(extension)+1)*sizeof(char));
	filename=strcpy(filename,prefixe_name);
	strcat(filename,extension);

	if ((pf=fopen(filename,mode))==NULL) {
		fprintf(stderr,"Impossible d'ouvrir le fichier %s!!!\n",filename);
        exit(1);
	}

	return pf;
}

/* A Changer, remplacer par des exceptions ? (MANU) */

void SysDie (int rvl, const char *s)
{
    if (rvl!=0)
    {
        perror(s);
        exit(1);
    };
}

void SetListFileName(const string& ListFileName, bool bAppend)
	{
	g_strListFileName=ListFileName;
	g_bListFileAppend = bAppend;
	}

void Log(const char szFormat[], ...)
	{
	if (0 == g_strListFileName[0])
		return;

	static FILE *f = NULL;
	string mode;
	if (g_bListFileAppend)
		mode = "a";
	else
		mode = "w";
	if (NULL == f)
		f = fopen(g_strListFileName.c_str(), mode.c_str());
	if (NULL == f)
		{
		perror(g_strListFileName.c_str());
		exit(EXIT_NotStarted);
		}

	char szStr[4096];
	va_list ArgList;
	va_start(ArgList, szFormat);
	vsprintf(szStr, szFormat, ArgList);
	fprintf(f, "%s", szStr);
	fflush(f);
	}

const char *GetTimeAsStr()
	{
	static char szStr[32];
	time_t t;
	time(&t);
	struct tm *ptmCurrentTime = localtime(&t);
	strcpy(szStr, asctime(ptmCurrentTime));
	assert('\n' == szStr[24]);
	szStr[24] = 0;
	return szStr;
	}

// Exit immediately with error message, printf-style.
void Quit(const char szFormat[], ...)
	{
	va_list ArgList;
	char szStr[4096];

	va_start(ArgList, szFormat);
	vsprintf(szStr, szFormat, ArgList);

	fprintf(stderr, "\n*** ERROR ***  %s\n", szStr);

	Log("\n*** FATAL ERROR ***  ");
	Log("%s\n", szStr);
	Log("Stopped %s\n", GetTimeAsStr());

	exit(EXIT_FatalError);
	}

void GetLineTokens(std::vector<std::string>& v, const string& s)
	{
		stringstream ss;
		string tmp;
		ss<<s;
		while ( getline( ss, tmp, '\t' )) v.push_back( tmp );
	}

/*
 Name		:  ValidCutoffParam
 Usage		:  if ( !ValidCutoffParam( cutoff_as_string ) ) take care of the error
 Function	:  checks the format or the cutoff parameters
 Arg		:  cutoffs : a string that represents the cutoffs formated as explained below (see also the help)
				The cutoff values are given for each level as 3 values
				using the following format CMD-CS-CCV :
					CMD : cutoff value for the manhattan Distance separating anchors (integer, a strict maximum value)
						(will be used for the construction of connected components)
					CS  : cutoff score for a cluster to be part of the cluster partition (integer, a strict minimum value)
					CCV : cutoff score for the covariation of anchors within a cluster (float, a strict minimum value)
 Access		:  public function
 Return		:  false if one of the cutoff parameters are not correctly read
 Error		:  none
*/

bool ValidCutoffParam( const string cutoffs )
	{
		bool ValiParam = true;

		list<string> cutoffs_list;

		std::istringstream iss( cutoffs );
		std::string level_token;
		while ( getline(iss,level_token, ':') )
			{
			cutoffs_list.push_back( level_token );
			}

		for( list<string>::iterator i = cutoffs_list.begin() ; i!=cutoffs_list.end() ; ++i )
			{
			std::istringstream iss_cutoff( *i );
			std::string cutoff_token;
			vector<string> cutoff_vector;
			while ( getline(iss_cutoff,cutoff_token, '-') )
				{
					cutoff_vector.push_back( cutoff_token );
				}
				if ( cutoff_vector.size() != 3 )
					return false;
				// Checks if all the cutoff parameters are OK
				ValiParam&=ValidUint4(cutoff_vector[0]) && ValidUint4(cutoff_vector[1]) && ValidCovar(cutoff_vector[2]);
			}

		return ValiParam;
	}

/*
 Name		:  ValidUint4
 Usage		:  if ( !ValidUint4( integer_as_string ) ) { do something }
 Function	:  checks if the string is a valid Uint4 (unsigned int)
 Arg		:  value : a string that represents the integer
 Access		:  public function
 Return		:  false if the conversion from string to a positive integer fails or if the integer is negative
*/

bool ValidUint4(const string& s,  bool failIfLeftoverChars)
{
	long l;
	istringstream iss(s);
	char c;
	if ( (iss >> l).fail() ||  ( failIfLeftoverChars && iss.get(c) ))
		return false;
	if (l<0)  // checks now if the integer is positive
		return false;
	return true;
}

/*
 Name		:  ValidCovar
 Usage		:  if ( !ValidCovar( covar_as_string ) ) { do something }
 Function	:  checks if the string is a valid covariance threshold ( a float bewteen 0 and 1)
 Arg		:  value : a string that represents the covariance threshold
 Access		:  public function
 Return		:  false if the conversion from string to a float fails of if the float is larger than 1
*/

bool ValidCovar(const string& s,  bool failIfLeftoverChars)
{
	float f;
	istringstream iss(s);
	char c;
	if ( (iss >> f).fail() || ( failIfLeftoverChars && iss.get(c) ) )
		return false;
	if ( f > 1 ) // checks now if the float is between 0 and 1
		return false;

	return true;
}

void bin_print(long n, int taille)
	{
	unsigned long mask= 1 << ((taille * CHAR_BIT) - 1);

	printf("%ld: ", n);
	while (mask > 0)
    {
      putchar(n & mask ? '1' : '0');
      mask>>= 1;
    }
	putchar('\n');
	fflush(stdout);
	}

void bin_print_char(char n)
	{
	bin_print(n, sizeof(n));
	}

void bin_print_short(short n)
	{
	bin_print(n, sizeof(n));
	}

void bin_print_int(int n)
	{
	bin_print(n, sizeof(n));
	}

void bin_print_unlong(unsigned long n)
	{
	bin_print(n, sizeof(n));
	}

