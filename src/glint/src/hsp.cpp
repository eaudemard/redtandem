/*
   $Id: alignment.cpp 222 2008-05-28 06:35:44Z tfaraut $
*/


/* alignment.c  */

#include <ctype.h>
#include <iostream>
#include "utils.h"
#include "hsp.h"
#include "sequence_io.h"

using namespace std;

extern RealLogger* logger;


/*

     The methods from the HspPile and HitPile classes

*/

/* Commit:
         -inserts the current hsp (ie the top) in l
         -increments top, WITHOUT increasing the size of p
         -If top is at the highest, p is dumped and p and l are cleared */


void HspPile::Commit()
{
	 _Insert((*hsp_current)[top]);             // insert the top in l
    if (top < hsp_current->size()-1)
    {
        top++;
    }
    else
    {
		if (display.query.compare.Linking)
		{
			//Linking();
		}
		else if (!display.query.compare.OutputStream)
		{
			cerr<<"Warning: hsps are being displayed because the hsp pile is full."<<endl;
			cerr<<"Warning: The hsps may not be correctly sorted."<<endl;
			SortAndDisplayHsps(display.dsp_file);
		}
        hsp_list.clear();
        SwitchPiles();
    };
}

void HspPile::SwitchPiles()
{
	vector<hsp*>* hv = hsp_prev;

	cerr<<"Hsp pile full => here switching pile \n";

	hsp_prev = hsp_current;
	hsp_current = hv;
	hsp_prev_empty = false;
	top=0;

}

void HspPile::_Insert(hsp* h)
{
    for (list<hsp*>::iterator i=hsp_list.begin(); i != hsp_list.end(); ++i)
    {
        hsp ii = **i;
        /* Si la diagonale est superieure a la derniere diagonale ou si
           sur la meme diagonale que le premier element de la liste mais avec une position plus en aval on insere  */
        if (h->max_diag > ii.max_diag || (h->max_diag == ii.max_diag && h->s_end > ii.s_end)) {
            hsp_list.insert(i,h);
            return;
        };
    };
    hsp_list.insert(hsp_list.end(),h);
}

/*---------------------------------------------*/
/**            The Lookup routines            **/
/*---------------------------------------------*/

/* The Lookup routines test if the corresponding match or hsp falls into an already   */
/* selected region as defined by the current and previous hsp list                    */
/* The already detected region are defined by min_diag, max_diag, s_start, s_end of a */
/* registered hsp, hence a parallelogram in the dotplot matrix                        */

/*
 Name:      Lookup
 Usage:     if (Lookup(m))
 Function:  returns true if both _LookupCurrent and _LookupPrevious are true
 Arg:       a pointer on a match
 Return:    a boolean
*/

bool HspPile::Lookup (const match& m) {
    return ( _LookupCurrent(m.diag,m.spos) || _LookupPrevious(m.qpos,m.spos) );

	/*bool b_current = _LookupCurrent(m.diag,m.spos);
	bool b_previous = _LookupPrevious(m.qpos,m.spos);
	bool b = ( b_current|| b_previous );

	if ( b )
	{
		cout << "Match: A similar hsp has already been identified. current :"<<b_current<<" previous :"<<b_previous<<"\n";
		cout << m ;
		cout << "\n";
	}

	return b;*/
}

/*
 Name:      Lookup
 Usage:     if (Lookup(h))
 Function:  returns true if  _LookupCurrent or _LookupPrevious is true
 Arg:       a pointer on a hsp
 Return:    a boolean
*/

bool HspPile::Lookup (const hsp& h) {
	return (_LookupCurrent(h.max_diag,h.s_end) || _LookupPrevious(h.q_start,h.s_start));

	/*bool b_current = _LookupCurrent(h.max_diag,h.s_end);
	bool b_previous = _LookupPrevious(h.q_start,h.s_start);
	bool b = ( b_current|| b_previous );

	if (b)
	{
		cout << "Hsp: A similar hsp has already been identified. current :"<<b_current<<" previous :"<<b_previous<<"\n";
		cout << h << "\n";
	}

	return b;*/
}


/*
 Name:		_LookupCurrent
 Usage:     _LookupCurrent(diag,spos)
 Function:  Returns true if (diag,pos) is contained in the parallelogram (sstart,min_diag) <-> (send,max_diag) of one of the hsps of a list.
            The list of hsp searched is the current list hsp_list (sorted by min_diag)
 Arg:       a diagonal and a position on the subject
 Return:    a boolean
 TODO:      Should be modified in order to check for both s_start and s_end when an hsp is tested !!!!
*/

bool HspPile::_LookupCurrent(Int4 diag, Uint4 spos)
{

	list<hsp*>::iterator i=hsp_list.begin();
    while (i!=hsp_list.end() && (diag<(**i).max_diag || ( diag==(**i).max_diag && spos<=(**i).s_end )))
    {    															 // while there is a potential overlap
        ++i;
    };

	list<hsp*>::reverse_iterator j(i); // Warning j points now to the hsp preceding i, because i is out of bounds :
	                                   // diag>(**i).max_diag or diag==(**i).max_diag && spos > (**i).s_end

	while (j!=hsp_list.rend() &&  diag<=(**j).max_diag)
    {  																// while there is overlap, considering only the diagonals
    	if (diag>=(**j).min_diag && spos>=(**j).s_start && spos<=(**j).s_end)
        {
		    return true;     										// Overlap detected !!!
        };
        ++j;
    };

    return false;
}

/*
 Name:		_LookupPrevious
 Usage:     _LookupPrevious(qpos,spos)
 Function:  Returns true is (qpos,pos) is contained in the parallelogram (qstart,sstart) <-> (qend,send) of one of the hsps of a list.
            The list of hsp searched is the previous list hsp_prev. In this list, hsps are sorted according to qstart (...).
 Arg:       a psoition on the query and a position on the subject
 Return:    a boolean
*/

bool HspPile::_LookupPrevious(Uint4 qpos, Uint4 spos)
{
	hsp *h=(*hsp_prev)[0];

	/*
    if (Debug_Here) {
        fprintf(stdout,"%d %d %d  in %d  %d  %d  %d  ? :  %d\n",hsp_prev->size()i,qpos,spos,h->q_start,h->q_end,h->s_start,h->s_end,1);
		for (int i=0;i<hsp_prev->size();i++) {
			display_hsp_tabular(hsp_prev[i],stdout);
		}
        fprintf(stdout,"----------------\n");
    };
    */

	if (hsp_prev_empty || qpos > h->q_end)
        return false;

    Uint4 i=1;
	while ( i<hsp_prev->size() && qpos < h->q_end ) {
		if (qpos > h->q_start) {
			if (spos >= h->s_start && spos <= h->s_end) {
                //fprintf(stdout,"poverlap: %d %d  in %d  %d  %d  %d\n",qpos,spos,h->q_start,h->q_end,h->s_start,h->s_end);
                return true;
            }
		}
		h=(*hsp_prev)[i++];
	}
    return false;
}



void HspPile::PrintSortedPile()
{
	for (list<hsp*>::iterator i=hsp_list.begin(); i != hsp_list.end(); ++i)
    {
		logger->SetLogType("H")<<(**i).max_diag<<"  "<<(**i).q_start<<"  "<<(**i).q_end<<"  "<<(**i).s_start<<"  "<<(**i).s_end<<"\n";
	}
	logger->SetLogType("H")<<"--------------------------------\n";
}

void HspPile::PrintPile()
{
	for (list<hsp*>::iterator i=hsp_list.begin(); i != hsp_list.end(); ++i)
    {
		cout<<(**i).max_diag<<"  "<<(**i).min_diag<<"  "<<(**i).q_start<<"  "<<(**i).q_end<<"  "<<(**i).s_start<<"  "<<(**i).s_end<<"\n";
	}
	cout<<"--------------------------------\n";
}

/*
     operator< for the hsp, used for sorting the hsps
        1st key: Name (increasing order)
        2nd key: score (decreasing order)

*/
bool operator<(const hsp& p, const hsp& q)
{
    if (p.true_seq.Name==q.true_seq.Name)
        return (p.score > q.score);
    else
        return (p.true_seq.Name < q.true_seq.Name);
}


/* This operator< is used for sorting the hits
   The sort key is the score, the hit are sorted in decreasing order of score !! */
bool operator< (const hit& p, const hit& q)
{
    return p.score > q.score;
}

/*

     Printing an HspPile:
         1/ Sort the pile
         2/ Call PrintVectorHspPtr to print hsp_current

*/

void HspPile::SortAndDisplayHsps(FILE *pf)
{
    if (top!=0)
    {
        display.DisplayHits(*hsp_current,top);
        logger->SetLogType("P") << "Printing new hsps \n";
    };
}

void HspPile::PrintDebug(FILE *pf)
{
	list<hsp*>::const_iterator i=hsp_list.begin();

	 if (top!=0)
    {
    	while (i!=hsp_list.end()) {
        fprintf(pf,"%u %u %u %u  %u\n",(**i).max_diag,(**i).q_start,(**i).q_end,(**i).s_start,(**i).s_end);
		  i++;
    	};
    };
}

#ifdef BLABLA
void HspPileLight::PrintDebug(FILE *pf)
{
	list<hsp*>::const_iterator i=hsp_list.begin();

	 if (!hsp_list.empty())
    {
    	while (i!=hsp_list.end()) {
        if (!(**i).excluded)
		  	fprintf(pf,"%u %u %u %u %u %u\n",(**i).q_start,(**i).q_end,(**i).s_start,(**i).s_end,(**i).order[QUERY],(**i).order[SUBJECT]);
		  i++;
    	};

    };
}
#endif


/* A very basic ostream operator for ostream */

ostream & operator<<(ostream& os, const hsp& h)
{
	const char *char_sign=(h.strand >0) ? OUTPUT_PLUS:OUTPUT_MINUS;
	os<<h.min_diag<<"\t"<<h.max_diag<<"\t"<<h.q_start<<"\t"<<h.q_end<<"\t"<<h.s_start<<"\t"<<h.s_end<<"\t"<<char_sign<<"\t"<<h.score<<"\t"<<h.size;
	return os;
}

/*

     The methods for displaying an hsp or a list of hsps

*/

/*

     The methods from the Display hierarchy: Displaying the results, with different formats
     The Display:: class implements the generic methods, common to all data formats

*/

/*
 Name:      DisplayHits
 Usage:     obj->Displayhits()
 Function:	displays all th hits for the query sequence
 Comment:   this method is defined in order to be overloaded by DisplayM13 for example
 Arg:       a vector of hsp pointers, the number of hsp
 Return:    none
*/
void Display::DisplayHits(vector<hsp*>& v,int len) const
{
	SortAndDisplayHits(v,len);
}

/*
 Name:      SortAndDisplayHits
 Usage:     obj->SortAndDisplayHits(v,l)
 Function:	Sort and organizes the hsps into hits, sort the corresponding hits and calls DisplayResult with the resulting hitpile as argument
 Arg:       a vector of hsp pointers, the number of hsp
 Return:    none

     Printing a vector of Hsp*:
         1/ If len==-1 (default), print the whole vector
            If len==0, return
            Else, print only len hsp*
         2/ Create an HitPile from the HspPile and sort the HitPile
            In the HitPile, we have as many hits and different subject sequence names can be found in the hsps
         3/ Print the hit pile
*/

void Display::SortAndDisplayHits(vector<hsp*>& v,int len) const
{
    hit *ht=NULL;
    HitPile hp(*this);

    if (len==-1)
        len = v.size();
    if (len==0)
        return;

    sort(v.begin(),v.end(),CompHspPtr);

    for (int i=0; i<len; i++)
    {
		if (ht==NULL || v[i]->true_seq.Name!=ht->Name)
        {
            ht = hp.NewHit();
            ht->Name = v[i]->true_seq.Name;
            ht->score= v[i]->score;
        };
        if (!query.compare.Self) {
            v[i]->true_seq_query.Name = query.GetSeqId();
            v[i]->true_seq_query.start=0;
        }
        ht->hsps.push_back(v[i]);
    };
    hp.Sort(); //Sorting the different hits according to the score (best hsp score for this hit) 
    DisplayResult(hp);
}

/*
 Name:      DisplayResult
 Usage:     obj->DisplayResult(hp)
 Function:	Displays the hip pile for a query sequence, begins by displaying general information about the query and the database
 Arg:       a sorted hitpile
 Return:    none
 */
void Display::DisplayResult(const HitPile& hp) const
{
    hsp* first_hsp = *(hp.begin()->hsps.begin());

    DisplayHeader(first_hsp->true_seq_query.Name,query.database.GetName());
    DisplayQueryInfo(first_hsp->true_seq_query.Name);
    for (HitPile::const_iterator i=hp.begin(); i!=hp.end(); ++i) {
    	hit j=*i;
    	DisplayHit(&j);
    }
}

/*
 Name:      Displayhit
 Usage:     obj->DisplayHit(h)
 Function:	Displays all the hsps of a hit
 Arg:       a reference on a hit
 Return:    none
 */
void Display::DisplayHit(const hit* h) const
{
	DisplaySubjectInfo(h->Name,h->score);
    for (vector<hsp*>::const_iterator j=h->hsps.begin(); j != h->hsps.end(); ++j) {
            DisplayHsp(*j);
    }
}

/*
 Obscure function for the DisplayM1

 */


void Display::_InitIncrDecr(void)
{
	int i;
	Incr=(int*)malloc(256*sizeof(int));
	Decr=(int*)malloc(256*sizeof(int));

	for (i=0;i<256;i++) {
		Incr[i]=1;
		Decr[i]=-1;
	}
	Incr['-']=0;
	Decr['-']=0;
}

/*

     The DisplayM1:: class implements the methods specific to the M1 ouput format  (see documentation)

*/

/*
 Name:      DisplayHspTopHeader
 Usage:     obj->DisplayTopHeader()
 Function:	displays outputfile file header if any
 Arg:       none
 Return:    none
*/
void DisplayM1::DisplayTopHeader() const
{
	// no top header for the M type display
}

void DisplayM1::DisplayHeader(const string& query,const string& bank) const
{
	fprintf(dsp_file,"-------------------------------------------\n");
	fprintf(dsp_file,"Query %s\n",query.c_str());
	fprintf(dsp_file,"Searching bank %s \n",bank.c_str());
	fprintf(dsp_file,"-------------------------------------------\n");
}

void DisplayM1::DisplayQueryInfo(const string& query) const
{
	fprintf(dsp_file,"Query=%s\n",query.c_str());
}

void DisplayM1::DisplaySubjectInfo(const string& subject,int score) const
{
	fprintf(dsp_file,">%s\t%d\t",subject.c_str(),score);
}

/*
 Name:      DisplayHsp
 Usage:     obj->DisplayHsp()
 Function:	displays the hsp in the m1 format (hsp info + full alignment)
 Arg:       a pointer on a hsp
 Return:    the size of the hsp
*/
Uint2 DisplayM1::DisplayHsp(hsp *h) const
{
    _DisplayHspInfo(h);
    _DisplayFullHsp(h);

	return h->size;
}

void DisplayM1::_DisplayVerboseHspInfo(hsp *h) const
{
    const char *char_sign=(h->strand >0) ? OUTPUT_PLUS : OUTPUT_MINUS;

	fprintf(dsp_file,"Query : %s   Subject : %s \n",h->true_seq_query.Name.c_str(),h->true_seq.Name.c_str());
	fprintf(dsp_file,"Score : %d\tSize : %d\t%s\n%u\t%u\t%u\t%u\n",h->score,h->size,char_sign,h->q_start-h->true_seq_query.start,h->q_end-h->true_seq_query.start,h->s_start-h->true_seq.start,h->s_end-h->true_seq.start);
	fprintf(dsp_file,"Gap : %d  %d \n",h->q_gap.GetSize(),h->s_gap.GetSize());
	fprintf(dsp_file,"Diagonals : %d  %d\n",h->min_diag,h->max_diag);
}

/*
 Name:      _DisplayHspInfo
 Usage:     called by DisplayHsp
 Function:	displays the hsp summary information (start,end ...)
 Arg:       a pointer on a hsp
*/
void DisplayM1::_DisplayHspInfo(hsp *h) const
{
    const char *char_sign=(h->strand >0) ? OUTPUT_PLUS : OUTPUT_MINUS;

	fprintf(dsp_file,"Score : %d\tSize : %d\t%s\n%u\t%u\t%u\t%u\n",h->score,h->size,char_sign,h->q_start-h->true_seq_query.start,h->q_end-h->true_seq_query.start,h->s_start-h->true_seq.start,h->s_end-h->true_seq.start);
	fprintf(dsp_file,"Gap : %d  %d \n",h->q_gap.GetSize(),h->s_gap.GetSize());
}

Uint2 DisplayM1::_DisplayFullHsp(hsp *h) const
{
	Uint4 l=0, L=0;
	const unsigned char *nQuery=query.GetSeq()+h->q_start;
	const unsigned char *nBase=(h->strand>0) ? query.ctree.GetSeq()+h->s_start : query.ctree.GetSeq()+h->s_end;

	display_line.query_ind=h->q_start-h->true_seq_query.start;
	display_line.strand=h->strand;
	if (h->strand>0) {
		display_line.subject_ind=h->s_start-h->true_seq.start;
		display_line.sincr=Incr;
	} else {
		display_line.subject_ind=h->s_end-h->true_seq.start;
		display_line.sincr=Decr;
	}

    h->q_gap.Sort();
    h->s_gap.Sort();
    GapPile_const_iterator qgap = h->q_gap.begin();
    GapPile_const_iterator sgap = h->s_gap.begin();

	while (  l < h->size ) {
		if (qgap!=h->q_gap.end() && *qgap==l) {
			display_line.alignment[0][L]='-';
			display_line.alignment[1][L]=' ';
			display_line.alignment[2][L]=(h->strand>0) ? *nBase : Rev[*nBase];
			nBase+=h->strand;
			qgap++;
		} else if (sgap!=h->s_gap.end() && *sgap==l) {
			display_line.alignment[0][L]=*nQuery++;
			display_line.alignment[1][L]=' ';
			display_line.alignment[2][L]='-';
			sgap++;
		} else {
			display_line.alignment[0][L]=*nQuery++;
			display_line.alignment[2][L]=(h->strand>0) ? *nBase : Rev[*nBase];
			display_line.alignment[1][L]=(toupper(display_line.alignment[0][L])==toupper(display_line.alignment[2][L])) ? '|' : ' ';
			nBase+=h->strand;
		}
		l++;
		L++;
		if (L==DISPLAY_LINE_SIZE) {
			_DisplayPrettyAlignmentLine(L);
			L=0;
		}
	}

	if (L>0) _DisplayPrettyAlignmentLine(L);

	return h->size;
}

void DisplayM1::_DisplayPrettyAlignmentLine(Uint2 max) const
{
	int j;

	fprintf(dsp_file,"Query:  %-10u   ",display_line.query_ind);
	for (j=0;j<max;j++) {
		fprintf(dsp_file,"%c",display_line.alignment[0][j]);
		display_line.query_ind+=Incr[(unsigned short)display_line.alignment[0][j]];
	}
	fprintf(dsp_file,"   %-10u\n",display_line.query_ind-1);
	fprintf(dsp_file,"%21s","");
	for (j=0;j<max;j++) {
		fprintf(dsp_file,"%c",display_line.alignment[1][j]);
	}
	fprintf(dsp_file,"\n");
	fprintf(dsp_file,"Subjt:  %-10u   ",display_line.subject_ind);
	for (j=0;j<max;j++) {
		fprintf(dsp_file,"%c",display_line.alignment[2][j]);
		display_line.subject_ind+=display_line.sincr[(unsigned short)display_line.alignment[2][j]];
	}
	fprintf(dsp_file,"   %-10u\n",display_line.subject_ind-display_line.strand);
	fprintf(dsp_file,"\n");
}

/*
     The DisplayM2:: class implements the methods specific to the M2 output format  (see documentation)
*/

void DisplayM2::DisplayTopHeader() const
{
	fprintf(dsp_file,"%s\n",M2_HEADER);
}

Uint2 DisplayM2::DisplayHsp(hsp *h) const
{
	char sign=(h->strand>0) ? '+' : '-';

	fprintf(dsp_file,"%s\t%u\t%u\t%s\t%u\t%u\t%c\t%d\t%d\n",h->true_seq_query.Name.c_str(),h->q_start-h->true_seq_query.start,h->q_end-h->true_seq_query.start,h->true_seq.Name.c_str(),h->s_start-h->true_seq.start,h->s_end-h->true_seq.start,sign,h->score,h->size);

	return h->size;
}

/*
     The DisplayM3:: class implements the methods specific to the M3 ouput format  (see documentation)
*/

void DisplayM3::DisplayTopHeader() const
{
	fprintf(dsp_file,"%s\n",M3_HEADER);
}

Uint2 DisplayM3::DisplayHsp(hsp *h) const
{

   fprintf(dsp_file,"%s\t%s\t",h->true_seq_query.Name.c_str(),h->true_seq.Name.c_str());

   _DisplayHspInfo(h);

   fprintf(dsp_file,"\n");

   return h->size;
}

void DisplayM3::_DisplayHspInfo(hsp *h) const
{
    const char *char_sign=(h->strand >0) ? OUTPUT_PLUS:OUTPUT_MINUS;
	float pident;

	//_RecordHspScript(h); Deprecated TO REMOVE
	pident=100*(1-(float)(h->mismatch.GetSize()+h->q_gap.GetSize()+h->s_gap.GetSize())/h->size);

	fprintf(dsp_file,"[%u-%u]->[%u-%u](%u,%s,%d,,%2.0f%c)",h->q_start-h->true_seq_query.start,h->q_end-h->true_seq_query.start,h->s_start-h->true_seq.start,h->s_end-h->true_seq.start,h->size,char_sign,h->score,pident,'%');

	fprintf(dsp_file,"[");
    h->mismatch.Print(dsp_file);
	fprintf(dsp_file,"]");
	fprintf(dsp_file,"{");
    h->q_gap.Print(dsp_file);
	fprintf(dsp_file,"}");
	fprintf(dsp_file,"{");
    h->s_gap.Print(dsp_file);
	fprintf(dsp_file,"}:");

}

#ifdef DEPRECATED
/*
 Name:      _RecordHspScript
 Usage:     cammed by _DisplayHspInfo
 Function:  Records the smallest script for describing the hsp
 Arg:       an hsp pointer
 Return:    the size of the alignment
*/
Uint2 DisplayM3::_RecordHspScript(hsp *h) const
{
	Uint4 l=0;
	const unsigned char *nQuery=query.GetSeq()+h->q_start;
	const unsigned char *nBase=(h->strand>0) ? query.ctree.GetSeq()+h->s_start : query.ctree.GetSeq()+h->s_end;
	char base;
	AlignStatus align_status=ALIGNED;

	display_line.query_ind=h->q_start-h->true_seq_query.start;
	display_line.strand=h->strand;
	if (h->strand>0) {
		display_line.subject_ind=h->s_start-h->true_seq.start;
		display_line.sincr=Incr;
	} else {
		display_line.subject_ind=h->s_end-h->true_seq.start;
		display_line.sincr=Decr;
	}
	h->q_gap.Sort();
	h->s_gap.Sort();
    GapPile_const_iterator qgap = h->q_gap.begin();
    GapPile_const_iterator sgap = h->s_gap.begin();

	while (  l < h->size ) {
		if (qgap!=h->q_gap.end() && *qgap==l) {
			if (align_status == ALIGNED)
			{
				h->gap_opening.Push(l);
				align_status = GAPED;
			}
			nBase+=h->strand;
			qgap++;

		} else if (sgap!=h->s_gap.end() && *sgap==l) {
			if (align_status == ALIGNED)
			{
				h->gap_opening.Push(l);
				align_status = GAPED;
			}
			nQuery++;
			sgap++;
		} else {
			base=(h->strand>0) ? *nBase : Rev[*nBase];
			if (toupper(*nQuery)!=toupper(base)) h->mismatch.Push(l);
			nBase+=h->strand;
			nQuery++;
			align_status = ALIGNED;
		}
		l++;
	}

	return h->size;
}
#endif

/*
     The DisplayM4:: class implements the methods specific to the M4 output format  (see documentation in ptypes.h)
*/

void DisplayM4::DisplayTopHeader() const
{
	// no top header for the M4 type display
}

Uint2 DisplayM4::DisplayHsp(hsp *h) const
{
	cout<<*h<<endl;

	return 1;
}

/*
     The DisplayM8:: class implements the methods specific to the M8 output format  (see documentation)
*/

void DisplayM8::DisplayTopHeader() const
{
	// no top header for the M8 type display
}

Uint2 DisplayM8::DisplayHsp(hsp *h) const
{

	fprintf(dsp_file,"%s\t%s\t",h->true_seq_query.Name.c_str(),h->true_seq.Name.c_str());

	_DisplayHspInfo(h);

	return h->size;
}

void DisplayM8::_DisplayHspInfo(hsp *h) const
{
	float pident;
	Uint4 s_start,s_end;

	//_RecordHspScript(h);  Deprecated TO REMOVE
	pident=100*(1-(float)(h->mismatch.GetSize()+h->q_gap.GetSize()+h->s_gap.GetSize())/h->size);

	if (h->strand>0)
	{
		s_start=h->s_start-h->true_seq.start;
		s_end=h->s_end-h->true_seq.start;
	}
	else
	{
		s_start=h->s_end-h->true_seq.start;
		s_end=h->s_start-h->true_seq.start;
	}

	fprintf(dsp_file,"%2.2f\t%d\t%d\t%u\t%u\t%u\t%u\t%d\t%s\t%d\n",pident,h->size,h->mismatch.GetSize(),h->gap_opening.GetSize(),h->q_start-h->true_seq_query.start,h->q_end-h->true_seq_query.start,s_start,s_end,"1e-0",h->score);

}

/*
     The DisplayM9:: class implements the methods specific to the M9 ouput format  (see documentation)
*/
/*
 Name:      DisplayHeader
 Usage:     DisplayHeader()
 Function:  displays header information à la blast -m9
 Arg:       a pointer on a hsp
 Return:    nothing
*/

void DisplayM9::DisplayHeader(const string& query_name, const string& bank) const
{
	fprintf(dsp_file,"# glint rel $Rev: 222 $ - Compiled on %s at %s\n", __DATE__, __TIME__);
	fprintf(dsp_file,"# Query: %s\n",query_name.c_str());
	fprintf(dsp_file,"# Database: %s\n",bank.c_str());
	fprintf(dsp_file,"# Fields: Query id, Subject id, %c identity, alignment length, mismatches, gap openings, q. start, q. end, s. start, s. end, e-value, bit score\n",'%');
}

/*
     The DisplayM13:: class implements the methods specific to the M13 ouput format  (see documentation)
*/
/*
 Name:      DisplayHits
 Usage:     DisplayHits(v,l)
 Function:  simply displays the fasta sequence if there is at least a hit
 Arg:       a pointer on a hsp
 Return:    nothing
*/

void DisplayM13::DisplayResult(const HitPile& hp) const
{	
	hsp* h = *(hp.begin()->hsps.begin()); //The first hsp
	string SeqId =query.GetSeqId();
	fprintf(dsp_file,">%s\tS=%d\tL=%d\tMM=%d\tG=%d\n",SeqId.c_str(),h->score,h->size,h->mismatch.GetSize(),h->q_gap.GetSize()+h->s_gap.GetSize());
	fprintf(dsp_file,"%s",query.GetSeqAsFasta());
}


