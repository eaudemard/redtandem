/*
   $Id: scoring.cpp 263 2009-09-21 16:10:48Z tfaraut $
*/


/* scoring.c */

/*===========================================================================
                         scoring  -  description
                             -------------------
    begin				: septembre 2005
	 objet				: Functions for the initilizations of the scoring structures intended for dynamic programmming alignment-> alignment.c

=============================================================================*/

#include <stdio.h>
#include "string.h"
#include "scoring.h"

/*
 Name:      Constructor
 Usage:     Constructor
 Function:  the scores for the dropoff and the dyn programming algorithm
 Arg:       match (reward match), mismatch (penality for a mismatch), nmatch (match against an N), alpha (gap opening), beta (pag extension),
            X1 (min score to
			 default parameters (ptypes.h) ALIGN_CONSTANTS_DEF_PRMS 1,-3,-1,-5,-2,15,25
*/
AlignConstants::AlignConstants(int match, int mismatch,int nmatch, int alpha, int beta, int X1, int X2)
{
	//struct ALIGN_CONSTANTS* scoring=(struct ALIGN_CONSTANTS*)malloc(sizeof(struct ALIGN_CONSTANTS));

	/*if ((scoring=(struct ALIGN_CONSTANTS*)malloc(sizeof(struct ALIGN_CONSTANTS)))==NULL) {
		fprintf(stderr,"Impossible to allocate ALIGN_CONSTANTS \n");
		exit(1);
	} else {
		fprintf(stderr,"adress %p \n",scoring);
	}*/

	this->alpha=alpha;
	this->beta=beta;
	this->X1=X1;
	this->X2=X2;

	this->match = match;
	this->mismatch = mismatch;
	this->nmatch = nmatch;

	score_fwd=_InitScoringScheme(match,mismatch,nmatch, 1);
	score_bwd=_InitScoringScheme(match,mismatch,nmatch,-1);

	score_fwd_severe=_InitScoringSchemeSevere(match,mismatch,nmatch, 1);
	score_bwd_severe=_InitScoringSchemeSevere(match,mismatch,nmatch,-1);

	score_indel=_InitScoringIndels(alpha,beta);
	score=score_fwd;

}

/*
 Name:      _InitScoringScheme
 Usage:     called by the constructor
 Function:  stores the scoring matrix for dynamic programming
 Arg:       rmatch (reward match), rmismatch (reward, penality for a mismatch), nmatch (match against an N), strand (+1 or -1)
*/
int **AlignConstants::_InitScoringScheme(int rmatch, int rmismatch, int nmatch, int strand)
{
	int i,j;
	int **score_matrix=(int**)malloc(256*sizeof(int*));

	for (i=0;i<256;i++) {
		score_matrix[i]=(int*)malloc(256*sizeof(int));
		for (j=0;j<256;j++) {
			score_matrix[i][j]=rmismatch;
		}
	}
	for (i=0;i<256;i++) {
		score_matrix[i]['$']=N_INFTY;
		score_matrix['$'][i]=N_INFTY;
	}

	if (strand > 0) {
		score_matrix['A']['A']=rmatch;
		score_matrix['C']['C']=rmatch;
		score_matrix['G']['G']=rmatch;
		score_matrix['T']['T']=rmatch;
		score_matrix['a']['A']=rmatch;
		score_matrix['c']['C']=rmatch;
		score_matrix['g']['G']=rmatch;
		score_matrix['t']['T']=rmatch;
		score_matrix['A']['a']=rmatch;
		score_matrix['C']['c']=rmatch;
		score_matrix['G']['g']=rmatch;
		score_matrix['T']['t']=rmatch;
		score_matrix['a']['a']=rmatch;
		score_matrix['c']['c']=rmatch;
		score_matrix['g']['g']=rmatch;
		score_matrix['t']['t']=rmatch;
	} else {
		score_matrix['A']['T']=rmatch;
		score_matrix['T']['A']=rmatch;
		score_matrix['C']['G']=rmatch;
		score_matrix['G']['C']=rmatch;
		score_matrix['A']['t']=rmatch;
		score_matrix['T']['a']=rmatch;
		score_matrix['C']['g']=rmatch;
		score_matrix['G']['c']=rmatch;
		score_matrix['a']['T']=rmatch;
		score_matrix['t']['A']=rmatch;
		score_matrix['c']['G']=rmatch;
		score_matrix['g']['C']=rmatch;
		score_matrix['a']['t']=rmatch;
		score_matrix['t']['a']=rmatch;
		score_matrix['c']['g']=rmatch;
		score_matrix['g']['c']=rmatch;
	}

	/*score_matrix['N']['A']=0;
	*/
	score_matrix['A']['N']=score_matrix['N']['A']=nmatch;
	score_matrix['C']['N']=score_matrix['N']['C']=nmatch;
	score_matrix['G']['N']=score_matrix['N']['G']=nmatch;
	score_matrix['T']['N']=score_matrix['N']['T']=nmatch;
	score_matrix['N']['N']=nmatch;

	return score_matrix;
}

/*
 Name:      _InitScoringSchemeSevere
 Usage:     called by the constructor
 Function:  stores a severe scoring matrix for dynamic programming (used when the Strict option is set to true)
            lower case letters matches are treated as N letter matches (no reward)
 Arg:       rmatch (reward match), rmismatch (reward, penality for a mismatch), nmatch (match against an N), strand (+1 or -1)
*/
int **AlignConstants::_InitScoringSchemeSevere(int rmatch, int rmismatch, int nmatch, int strand)
{
	int i,j;
	int **score_matrix=(int**)malloc(256*sizeof(int*));

	for (i=0;i<256;i++) {
		score_matrix[i]=(int*)malloc(256*sizeof(int));
		for (j=0;j<256;j++) {
			score_matrix[i][j]=rmismatch;
		}
	}
	for (i=0;i<256;i++) {
		score_matrix[i]['$']=N_INFTY;
		score_matrix['$'][i]=N_INFTY;
	}

	if (strand > 0) {
		score_matrix['A']['A']=rmatch;
		score_matrix['C']['C']=rmatch;
		score_matrix['G']['G']=rmatch;
		score_matrix['T']['T']=rmatch;
		score_matrix['a']['A']=nmatch;
		score_matrix['c']['C']=nmatch;
		score_matrix['g']['G']=nmatch;
		score_matrix['t']['T']=nmatch;
		score_matrix['A']['a']=nmatch;
		score_matrix['C']['c']=nmatch;
		score_matrix['G']['g']=nmatch;
		score_matrix['T']['t']=nmatch;
		score_matrix['a']['a']=nmatch;
		score_matrix['c']['c']=nmatch;
		score_matrix['g']['g']=nmatch;
		score_matrix['t']['t']=nmatch;
	} else {
		score_matrix['A']['T']=rmatch;
		score_matrix['T']['A']=rmatch;
		score_matrix['C']['G']=rmatch;
		score_matrix['G']['C']=rmatch;
		score_matrix['A']['t']=nmatch;
		score_matrix['T']['a']=nmatch;
		score_matrix['C']['g']=nmatch;
		score_matrix['G']['c']=nmatch;
		score_matrix['a']['T']=nmatch;
		score_matrix['t']['A']=nmatch;
		score_matrix['c']['G']=nmatch;
		score_matrix['g']['C']=nmatch;
		score_matrix['a']['t']=nmatch;
		score_matrix['t']['a']=nmatch;
		score_matrix['c']['g']=nmatch;
		score_matrix['g']['c']=nmatch;
	}

	/*score_matrix['N']['A']=0;
	*/
	score_matrix['A']['N']=score_matrix['N']['A']=nmatch;
	score_matrix['C']['N']=score_matrix['N']['C']=nmatch;
	score_matrix['G']['N']=score_matrix['N']['G']=nmatch;
	score_matrix['T']['N']=score_matrix['N']['T']=nmatch;
	score_matrix['N']['N']=nmatch;

	return score_matrix;
}

int **AlignConstants::_InitScoringIndels(int alpha, int beta)
{
	int i,j;
	int **score_matrix=(int**)malloc(256*sizeof(int*));

	for (i=0;i<256;i++) {
		score_matrix[i]=(int*)malloc(256*sizeof(int));
		for (j=0;j<256;j++) {
			score_matrix[i][j]=alpha;
		}
	}
	for (i=0;i<256;i++) {
		score_matrix[i]['$']=N_INFTY;
		score_matrix['$'][i]=N_INFTY;
	}

	return score_matrix;
}


void AlignConstants::SetSevereScoringSheme()
{
	free(score_fwd);
	free(score_bwd);

	score_fwd=score_fwd_severe;
	score_bwd=score_bwd_severe;
}


short*** init_overlap_score(const Mask& m)
{
	int i,j,k;
	short ***overlap_score=(short***)malloc((m.GetLeafLevel()+1)*sizeof(short**));

	//cout<<"mask : "<<m.GetString()<<" taille "<<m.GetLength()<<"\n";

	for (i=ROOT_LEVEL;i<=m.GetLeafLevel();i++) {
		overlap_score[i]=(short**)malloc((m.GetLeafLevel()+1)*sizeof(short*));
		for (j=ROOT_LEVEL;j<=m.GetLeafLevel();j++) {
			overlap_score[i][j]=(short*)malloc(m.GetLength()*sizeof(short));
			for (k=0;k<m.GetLength();k++) {
				overlap_score[i][j][k]=overlap(m,i,j,k);
				//fprintf(stdout,"hit(i)=%d hit(i+1)=%d decalage=%d score=%d\n",i,j,k,overlap_score[i][j][k]);
			}
		}
	}

	return overlap_score;
}

short*** init_overlap_score_bw(const Mask& m)
{
	int i,j,k;
	short ***overlap_score=(short***)malloc((m.GetLeafLevel()+1)*sizeof(short**));

	//fprintf(stdout,"%s length=%d\n",m->StringMask,(int)strlen(m->StringMask));

	for (i=ROOT_LEVEL;i<=m.GetLeafLevel();i++) {
		overlap_score[i]=(short**)malloc((m.GetLeafLevel()+1)*sizeof(short*));
		for (j=ROOT_LEVEL;j<=m.GetLeafLevel();j++) {
			overlap_score[i][j]=(short*)malloc(m.GetLength()*sizeof(short));
			for (k=0;k<m.GetLength();k++) {
				overlap_score[i][j][k]=overlap_bw(m,i,j,k);
				//fprintf(stdout,"hit(i)=%d hit(i+1)=%d decalage=%d score=%d\n",i,j,k,overlap_score[i][j][k]);
			}
		}
	}

	return overlap_score;
}


int overlap(const Mask& m, int i, int j, int k)
{
	int r;
	int score=0;

	for (r=k;r<m.GetLength();r++) {
		if (m.pos[r-k]<=j && m.pos[r]>i) score++;
	}
	for (r=m.GetLength();r<m.GetLength()+k;r++) {
		if (m.pos[r-k]<=j) score++;
	}

	return score;
}

int overlap_bw(const Mask& m, int i, int j, int k)
{
	int r;
	int score=0;

	for (r=0;r<k;r++) {
		if (m.pos[r]<=i) score++;
	}

	for (r=k;r<m.GetLength();r++) {
		if (m.pos[r]<=i && m.pos[r-k]>j) score++;
	}

	return score;
}

