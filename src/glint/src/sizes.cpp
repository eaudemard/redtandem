/*
   $Id: sizes.cpp 230 2008-09-12 13:16:00Z tfaraut $
*/


/*
  $Id: sizes.cpp 230 2008-09-12 13:16:00Z tfaraut $
*/

#include <iostream>
#include <limits>
using namespace std;

#include "ptypes.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/mman.h>

/*

Function: Maxsize
Parameters:
Return value:

*/

void SysDie (int rvl, const char *s)
{
    if (rvl==-1)
    {
        perror(s);
        exit(1);
    };
};

size_t MaxSize(int part)
{
    size_t max_size;
    numeric_limits<ssize_t> limit_ssize_t;

    struct rlimit memory_limit;

    int rvl = getrlimit(RLIMIT_DATA,&memory_limit);
    SysDie(rvl,"MaxSize");

    if (memory_limit.rlim_cur==RLIM_INFINITY) /* No limit specified: try to know the total physical memory */
    {
        long page_size   = sysconf(_SC_PAGESIZE);
        long nb_of_pages = sysconf(_SC_PHYS_PAGES);
        long physical_memory = nb_of_pages / 100;
        physical_memory *= page_size;
        if (physical_memory < limit_ssize_t.max()/part)
        {
            physical_memory *= part;
        }
        else
        {
            physical_memory = limit_ssize_t.max();
        }
        max_size = physical_memory;
    }
    else
    {
        max_size = memory_limit.rlim_cur/100;
        max_size *= part;
    };

    /* Now, we ask the system to give us those pages */

    void * base = malloc(max_size);
    if (base==0) rvl=-1;
    SysDie(rvl,"MaxSize");
    return max_size;
};

int main(int argc,char* argv[])
{
	system("uname -a");

    cout << "\n----\n";
    cout << "Int1\n";
    cout << "----\n";
	cout << "size:   " << sizeof(Int1) << '\n';
    numeric_limits<Int1> limit_Int1;
    cout << "signed: " << (limit_Int1.is_signed ? "Yes" : "No ") << "\n";
    cout << "min:    " << (int) limit_Int1.min() << '\n';
    cout << "max:    " << (int) limit_Int1.max() << '\n';

    cout << "\n----\n";
    cout << "Int2\n";
    cout << "----\n";
	cout << "size:   " << sizeof(Int2) << '\n';
    numeric_limits<Int2> limit_Int2;
    cout << "signed: " << (limit_Int2.is_signed ? "Yes" : "No") << '\n';
    cout << "min:    " << limit_Int2.min() << '\n';
    cout << "max:    " << limit_Int2.max() << '\n';

    cout << "\n----\n";
    cout << "Int4\n";
    cout << "----\n";
	cout << "size:   " << sizeof(Int4) << '\n';
    numeric_limits<Int4> limit_Int4;
    cout << "signed: " << (limit_Int4.is_signed ? "Yes" : "No") << '\n';
    cout << "min:    " << limit_Int4.min() << '\n';
    cout << "max:    " << limit_Int4.max() << '\n';

    cout << "\n-----\n";
    cout << "Uint1\n";
    cout << "-----\n";
	cout << "size:   " << sizeof(Uint1) << '\n';
    numeric_limits<Uint1> limit_Uint1;
    cout << "signed: " << (limit_Uint1.is_signed ? "Yes" : "No") << '\n';
    cout << "min:    " << (int) limit_Uint1.min() << '\n';
    cout << "max:    " << (int) limit_Uint1.max() << '\n';

    cout << "\n-----\n";
    cout << "Uint2\n";
    cout << "-----\n";
	cout << "size:   " << sizeof(Uint2) << '\n';
    numeric_limits<Uint2> limit_Uint2;
    cout << "signed: " << (limit_Uint2.is_signed ? "Yes" : "No") << '\n';
    cout << "min:    " << limit_Uint2.min() << '\n';
    cout << "max:    " << limit_Uint2.max() << '\n';

    cout << "\n-----\n";
    cout << "Uint4\n";
    cout << "-----\n";
	cout << "size:   " << sizeof(Uint4) << '\n';
    numeric_limits<Uint4> limit_Uint4;
    cout << "signed: " << (limit_Uint4.is_signed ? "Yes" : "No") << '\n';
    cout << "min:    " << limit_Uint4.min() << '\n';
    cout << "max:    " << limit_Uint4.max() << '\n';

    cout << "\n-----\n";
    cout << "Uint8\n";
    cout << "-----\n";
	cout << "size:   " << sizeof(Uint8) << '\n';
    numeric_limits<Uint8> limit_Uint8;
    cout << "signed: " << (limit_Uint8.is_signed ? "Yes" : "No") << '\n';
    cout << "min:    " << limit_Uint8.min() << '\n';
    cout << "max:    " << limit_Uint8.max() << '\n';

    cout << "\n-----\n";
    cout << "ssize_t\n";
    cout << "-----\n";
	cout << "size:   " << sizeof(ssize_t) << '\n';
    numeric_limits<ssize_t> limit_ssize_t;
    cout << "signed: " << (limit_ssize_t.is_signed ? "Yes" : "No") << '\n';
    cout << "min:    " << limit_ssize_t.min() << '\n';
    cout << "max:    " << limit_ssize_t.max() << '\n';

    cout << "\n-----\n";
    cout << "size_t\n";
    cout << "-----\n";
	cout << "size:   " << sizeof(size_t) << '\n';
    numeric_limits<size_t> limit_size_t;
    cout << "signed: " << (limit_size_t.is_signed ? "Yes" : "No") << '\n';
    cout << "min:    " << limit_size_t.min() << '\n';
    cout << "max:    " << limit_size_t.max() << '\n';

    cout << "\n-----\n";
    cout << "SSIZE_MAX " << SSIZE_MAX << "\n";

    long page_size   = sysconf(_SC_PAGESIZE);
    long nb_of_pages = sysconf(_SC_PHYS_PAGES);
    double physical_memory = (double) page_size * (double) nb_of_pages;
    cout << "\n-----\n";
    cout << "Page size       " << page_size << endl;
    cout << "nb of pages     " << nb_of_pages << endl;
    cout << "physical memory  (b) " << physical_memory << endl;
    cout << "physical memory  (Kb)" << physical_memory/1024 << endl;
    cout << "physical memory  (Mb)" << physical_memory/(1024*1024) << endl;

    cout << "\n-----\n";
    cout << "Now asking a lot of memory\n";
    size_t mem = MaxSize(50);
    cout << "Got " << mem << " Bytes\n";
    sleep(10);
	return 0;
}



