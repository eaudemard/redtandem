/*
   $Id: compare.h 2889 2009-09-21 16:04:11Z tfaraut $
*/


#ifndef __COMPARE_H__
#define __COMPARE_H__

#include "ptypes.h"
#include "masque.h"

struct Compare_Parameters {
    Compare_Parameters():Cutoff_Slide(DEFAULT_CUTOFF_SLIDE),Cutoff_Dyn(DEFAULT_CUTOFF_DYN),
                         Cutoff_Hsp(DEFAULT_CUTOFF_HSP),s_start4fill(0),s_end4fill(0),complexity(DEFAULT_COMPLEXITY),
                         Fill(false),Genome(false),Linking(false),OutputStream(false),Pipe(false),Self(false),Strict(false),
                         Dyn(true),Verbose(false),Silent(false),msk(DEFAULT_MASK),
                         lower_level(DEFAULT_LOWER_LEVEL),deepest_level(DEFAULT_DEEPEST_LEVEL),
                         output_format(DEFAULT_OUTPUT_FORMAT),step(DEFAULT_STEP)
                         {};
   Compare_Parameters(int, char**, void(*)());            // Initializing the object from the command line
   Compare_Parameters(const Compare_Parameters& compare);

	Uint2 Cutoff_Slide;
	Uint2 Cutoff_Dyn;
	Uint2 Cutoff_Hsp;
	mutable Uint4 s_start4fill;
	mutable Uint4 s_end4fill;
    Uint2 complexity;

    bool Fill;                     // Do we want to fill the gaps between hsp (not implemented)
    bool Genome;                   // If true the query genome is seen as a database (should have been previously indexed by glint)
 	bool Linking;                  // Do we want to link the hsp (not implemented)
	bool OutputStream;             // If true the hsp are displayed once they are computed
	bool Pipe;                     // If true the query sequence is provided by stdin
	bool Self;                     // If true the genome is compared against itself
    bool Strict;                   // If true a severe scoring sheme is used for the alignment (dyn prog) see scoring.cpp
    bool Dyn;                      // If false do not perform a dynamic programming (much faster but less sensitive)
	bool Verbose;				   // If true the program is more verbose
	bool Silent;				   // If true the program is silent

    Uint8 Tandem;                  // Maximum distance between seeds for self comparison dedicated to tandem repeat detection
    Int2 Match;                    // Reward for a match 
    Int2 MisMatch;                 // Reward (penality) for a mismatch
    Int2 NMatch;                   // Reward (penality) for a matching with an N
    Int2 GapOp;                    // Gap opening penality
    Int2 GapExt;                   // Gap extension penality
    Uint2 UngapDropoff;            // Dropoff for ungapped alignment
    Uint2 GapDropoff;              // Dropoff for gapped alignment
    Int4 MaxMisMatch;              // Maximum allowed mismatch to record an hsp
    Int4 MaxGap;                   // Maximum allowd gap to record an hsp
    Int4 SizeMin;                  // Minimum allowed size of an hsp to record it
    float MinRatioOfMaxScore;      // Minimum score, expressed in percentage of max possible score for the query sequence, to record an hsp
    float MinRatioOfMaxLength;      // Minimum score, expressed in percentage of max possible score for the query sequence, to record an hsp

	//When using auto_indexing mode we need to allow additional parameters
	Mask msk;         /* The mask used for indexation */
    Uint2 Max_Count;  /* Maximum allowed occurence of a word, otherwise => black list */

    Uint1 lower_level;
	Uint1 deepest_level;
    Uint1 output_format;
    Uint1 step;
    string results_filename;
    string query_filename;
    string bank_filename;
    string log_level;

	void Validation();     /* If the parameters are validated, OK - else, die */
	void __LogParameters();  /* For debugging purpose only */
};

#endif

