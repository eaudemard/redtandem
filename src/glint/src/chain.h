/*
   $Id: alignment.h 222 2008-05-28 06:35:44Z tfaraut $
*/

#ifndef __CHAIN_H__
#define __CHAIN_H__

#include <string>
#include <vector>

#include "anchor.h"
#include "ptypes.h"

#define FACTICE_CUTOFFS "_FACTICE_CUTOFFS_"
#define INITIAL_LEVEL   0
#define INTERMEDIATE_LEVEL   1
#define LAST_LEVEL   2

// AnchorVectBase type is defined as being a vector of pointers on AnchorNode
// this type, especially AnchorVect an object deriving from it, will be used to
// manage all the hsps produced by glint
typedef std::vector<AnchorNode*> AnchorVectBase;

/*

    Classe AnchorVect,
	AnchorVect is a class for managing the vector of hsps implemented as a vector of AnchorNode pointers

*/

class AnchorVect : public AnchorVectBase
	{
public:
	AnchorVect(): _view_type(QUERY),level(0),MaxDist(MAX_DIST),MinScore(MIN_SCORE),MinCovar(MIN_COVAR) {};
private:
       // Not implemented; prevent use of copy c'tor and assignment.
	AnchorVect(const AnchorVect &);
	AnchorVect &operator=(const AnchorVect &);
public:
	//Methods to load data from file
	void LoadDataFromFile(const std::string& InputFile, MolType moltype=DNA);

	//Managing the different views
	void SetView(ViewType v);
	ViewType GetView() const { return _view_type; };

	//Managing the orders
	void SetAnchorsOrder();
	void OrderLabel();

	//The actual cluster (chain construction)

	Uint4 DetectChains();

	void InitGraphStructure();
	void InitGraphStructure_Self();
	void ConnectedComponents();
	void ConstructClustersInfo();
	void TrivialChaining();
	void ClusterChaining();

	Uint4 __ManhattanSegments(const Anchor& u, const Anchor& v);
	Uint4 __ManhattanPoints(Point& a, Point& b);

	Uint4 PartitionSize() { return partition.size(); };
	const vector<Cluster*>& GetPartition() { return partition; };
	AnchorVect& operator=(const vector<Cluster*>& part);

	void RemoveRepeats();
	void _RemoveRepeats();
	void SelectClusters();
	void SetClusteringParameters(Uint4 md, Uint4 mc, float mcv);
	void SetLevel(Uint2 l) { level = l; };

	void Rbh(Uint4 maxoverlap);
	void PartitionRbh();
	void __DetectAndRemoveSegmentOverlap(ViewType side, Uint4 maxoverlap);
	void __DetectAndRemoveOverlap(ViewType side);
	void PropagateSelection();
	void UpdateClusterProperties();

	//void CheckDiagonalClusters(); Onsolete

	//Printing the clusters and anchors
	void Print(const string& filename);
	void PrintClusters();
	void PrintPartition(const string& filename);

	//For debugging purpose
	void PrintDebug();

	friend ostream & operator<<(ostream& os,  AnchorVect& av);

private:
	std::map<AnchorNode*,Cluster*> clusters;
	vector<Cluster*> partition;
	ViewType _view_type;
	Uint2 level;
	Uint4 MaxDist;
	Uint4 MinScore;
	float MinCovar;
	};

class SyntenyLevel {
public:
	SyntenyLevel(string cutoff,Uint1 t, Uint2 l);
	void FirstClustering();
	void Clustering( );
	void LastClustering();
	void ChainingSegments();
	void Rbh(Uint4 maxoverlap);
	void GetSegmentsFromLowerLevel(SyntenyLevel* A );
	Uint4 GetSize() { return segments.size(); };
	Uint2 GetLevel() { return level; };
	string CutoffParameters();
	friend class Syntenies;
	friend ostream & operator<<(ostream& os,  SyntenyLevel& cl);
private:
	AnchorVect segments;
	Uint4 ManhattanCutoff;
	Uint4 ScoreCutoff;
	float CovarCutoff;
	Uint2 level;
	Uint1 type;
};

typedef vector<SyntenyLevel*>::iterator SyntenyLevelIterator;
typedef vector<SyntenyLevel*> SyntenyLevelVectBase;

class Syntenies: public SyntenyLevelVectBase
{
public:
	Syntenies(const string& chains, const string& glintoutput, const string& blastpoutput, const string& anchorsoutput, const string& basename, bool rbh,Uint4 maxoverlap);
	void InitialClustering();
	void LoadAlignments( );
	void ConstructSyntenyLevels();
	void Print();
	friend ostream & operator<<(ostream& os, Syntenies& s);
private:
	string BaseOutputFilename;
	const string GlintOutputFilename;
	const string BlastpOutputFilename;
	const string AnchorsOutputFilename;
	bool RBH;
	Uint4 MaxOverlap;
};

#endif


