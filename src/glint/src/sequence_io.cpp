/* sequence_io.c */

#include <iostream>
#include <ctype.h>
#include <assert.h>
#include <iomanip>
#include <time.h>
#include <fstream>

#include "utils.h"
//#include "memory.h"
#include "alignment.h"
#include "sequence_io.h"

extern RealLogger* logger;

/*
   Function: GetSeqHeader
   Usage:    sequence_name = GetSeqHeader(f)
   Precondition: f is an already opened fasta file
                 f must be positioned to the First ('>') character of a header line, OR just after '>'
   Postcondition:f is positioned to the FIRST character of the sequence (start of next line)
   Parameter:    fp   A file descriptor
   Return value: The sequence header (string after '>') or "" if nothing read (end of file, blank line, etc)
   Error:        Call SysDie

*/

string GetSeqHeader(FILE *fp)
{
    static char* charbuffer = NULL;
    if (!charbuffer)
    {
        charbuffer = (char*) malloc(MAX_FASTA_HEADER_SIZE+1);   // sizeof(char=1). We alloc 1 one byte for the \0
        if (charbuffer==NULL)
        {
            SysDie(errno,"ERROR - Cannot allocate memory for a buffer\n");
        };
    };

	char* rvl=fgets(charbuffer,MAX_FASTA_HEADER_SIZE+1,fp);
    if (rvl==NULL)
        return (string) "";     // Nothing read (may be we are at end of file)
    if (strlen(rvl)==MAX_FASTA_HEADER_SIZE)
    {
        SysDie(1,"ERROR - Fasta header line too long - Please change MAX_FASTA_HEADER_SIZE and recompile");
    };

    string bfr = charbuffer;
    size_t strt  = bfr.find_first_not_of("> "); // Strip leading '>' and spaces
    if (strt==string::npos)
        SysDie(1,"ERROR - Bad file format\n");

    size_t end = bfr.find_first_of("\n",strt); // Keep all characters up to the first \n, if any
    return bfr.substr(strt,end-strt);
}


/*
   Function: GetSeqName
   Usage:    sequence_name = GetSeqName(f)
   Precondition: f is an already opened fasta file
                 f must be positioned to the First ('>') character of a header line, OR just after '>'
   Postcondition:f is positioned to the FIRST character of the sequence (start of next line)
   Parameter:    fp   A file descriptor
   Return value: The sequence name (first word after '>') or "" if nothing read (end of file, blank line, etc)
   Error:        Call SysDie

*/

string GetSeqName(FILE *fp)
{
    static char* charbuffer = NULL;
    if (!charbuffer)
    {
        charbuffer = (char*) malloc(MAX_FASTA_HEADER_SIZE+1);   // sizeof(char=1). We alloc 1 one byte for the \0
        if (charbuffer==NULL)
        {
            SysDie(errno,"ERROR - Cannot allocate memory for a buffer\n");
        };
    };

	char* rvl=fgets(charbuffer,MAX_FASTA_HEADER_SIZE+1,fp);
    if (rvl==NULL)
        return (string) "";     // Nothing read (may be we are at end of file)
    if (strlen(rvl)==MAX_FASTA_HEADER_SIZE)
    {
        SysDie(1,"ERROR - Fasta header line too long - Please change MAX_FASTA_HEADER_SIZE and recompile");
    };

    string bfr = charbuffer;
    size_t strt  = bfr.find_first_not_of("> "); // Strip leading '>' and spaces
    if (strt==string::npos)
        SysDie(1,"ERROR - Bad file format\n");

    size_t end = bfr.find_first_of(" \n",strt); // Keep all characters up to the first space or \n, if any
    return bfr.substr(strt,end-strt);
}


/*
   Procedure: SBuffer::_NewPosition
              Called by SBuffer::_ReadWord(l,pos)
              If the word has already too many occurrences, black list it and delete the corresponding leaf
              Else, call Leaf::NewPos
   Parameter: l:   *l is the leaf in which the new position will be inserted
              pos: the position to add to this leaf

*/

void SBuffer::_NewPosition(Leaf* l,Uint4 pos)
{
	if (l->GetNbPos()>database.Max_Count) {
        black_list.Insert(window.GetWord());
		//cerr << "Frequent word, deleted " << window.GetWord().Convert() << " !! " << database.Position << '\n';
		//cerr << "Frequent word, deleted " << window.GetWord().Convert() << '\n';
		/*  On pourrait ici remettre � 0 la fen�tre !! (attention � dinucleotide counter)  */
        index_tree.Delete(window);
		/*   DataBase->Words-=f->nb_pos;  */
		database.Words--;
	} else {
		/*  DataBase->Words++;  */
        l->NewPos(pos,window.GetMask().GetLength());
	}
}

/*
   Procedure: SBuffer::_ReadWord
              Called by SBuffer::_Index() or SBuffer:_IndexDinucleotides()
              If the word is not already black listed, retrieve the corresponding leaf, inserting a new leaf if necessary.
              However, nothing is inserted if we are not in the good phylum (see the Tree class)
              If a new leaf was inserted, call NewPosition
   Parameter: pos: The position of the read word

*/

void SBuffer::_ReadWord(Uint4 pos)
{
    if (black_list.LookUp(window.GetWord())==false) {
        Leaf *l = index_tree.Insert(window);    /* insert the new word or update count if the word already exists  */
        if (l !=NULL)                      // may be null if wrong pass treated
            _NewPosition(l,pos);
    };
}

/*
   Procedures: SBuffer::_IndexDinucleotides() and SBuffer::_Index()
               _IndexDinucleotides() is called by SBuffer::_Syncr() when database.complexity == COMPLEXITY_DINUCLEOTIDES
               _Index()              is called by SBuffer::_Syncr() when database.complexity == COMPLEXITY_NONE or COMPLEXITY_DUST
               We call SBuffer::ReadWord when a word is found
   Arg:        none
*/

void SBuffer::_IndexDinucleotides()
{
    if (IsEmpty())
        return;

    Uint4 pos = position - (fa_bfr->size()-1);   // The position, in the sequence, of the buffer's base
   	size_t i = 0;
    while(i < fa_bfr->size())
    {
        char c = *((char*)fa_bfr->GetBase() + i++);
        if (Word::IsCode(c))
        {                             /* If not a nucleotide: Reset the window */
			window.ShiftComp(c);        // Shift the window, computing the complexity (dinucleotides)
            if (window.IsFull()) {
                if (window.IsComplex()) {
                    _ReadWord(pos+i);
                } else {
                    _MaskLowComplexity();
                    window.ResetComp();
                }
            };
        }
        else
        {
            window.Reset();
        };
    };
}

void SBuffer::_Index()
{
    if (IsEmpty())
        return;

    Uint4 pos = position - (fa_bfr->size()-1);   // The position, in the sequence, of the buffer's base

    size_t i = 0;
    while(i < fa_bfr->size())
    {
        char c = *((char*)fa_bfr->GetBase() + i++);
        if (Word::IsCode(c)) {     /* Not a nucleotide: Reset the window */
			window.Shift(c);        // Shift the window, computing the complexity (dinucleotides)
            if (window.IsFull()) {
                _ReadWord(pos+i);
            };
        }
        else
        {
            window.Reset();
        };
    };
}

/*
   Procedure: SBuffer::_Syncr
              Called by SBuffer::Putc(char) or by the destructor of SBuffer, _Syncr works differently
              in memory_mode and not memory_mode:
              If NOT in memory_mode:
                 Call dust if complexity == COMPLEXITY_DUST
                 Call _IndexDinucleotides() or _Index()
                 Call _Dump() if fa_file not NULL
                 Clear fa_bfr
              If in memory_mode:
                 Call dust if complexity == COMPLEXITY_DUST
                 Call _IndexDinucleotides() or _Index()
*/

void SBuffer::_Syncr()
{
    //if (locked)
    //    SysDie(0,"Internal ERROR - The object is locked\n");

    if (fa_bfr.get()!=NULL && !IsEmpty())
    //if (!IsEmpty())
    {
        if (!memory_mode)       // memory_mode false: writing the index to files
        {
            if (fa_file != NULL && database.complexity == COMPLEXITY_DUST)
                _Dust();   // Writing to fa: dust the buffer if necessary;

            if (fa_file != NULL && database.complexity == COMPLEXITY_DINUCLEOTIDES)
            {
                unsigned char * base = (unsigned char *) fa_bfr->GetBase();
                size_t length = fa_bfr->size();
                char sav = *(base+length-1);
                _IndexDinucleotides();      // Start the indexation, with complexity management
                if (sav=='$')
                    *(base+length-1) = sav; // If the end of buffer is masked by the complexity stuff, the last $ is masked too
            }
            else
                /* We reach this point is no complexity was selected, OR if the complexity was already computed
            (when fa_file==NULL, ie the pass number is higher than 1) */
            _Index();                   // Start the indexation, without complexity management

            if (fa_file != NULL)             // Dump to the fa file
                _Dump();

            Clear();                        // Clear the buffer
        }
        else                    // memory_mode true: writing the index to memory
        {
            if (database.complexity == COMPLEXITY_DUST)
                _Dust();    // dust the buffer if necessary
            if (database.complexity == COMPLEXITY_DINUCLEOTIDES)
            {
                unsigned char * base = (unsigned char *) fa_bfr->GetBase();
                size_t length = fa_bfr->size();
                char sav = *(base+length-1);
                _IndexDinucleotides();      // Start the indexation, with complexity management
                if (sav=='$')
                    *(base+length-1) = sav; // If the end of buffer is masked by the complexity stuff, the last $ is masked too
            }
            else
                _Index();                   // Start the indexation, without complexity management
        };
    };
}

/*
   Function:  SBuffer::ReadSeq
              This function is called by main to start the indexing process
              An error is generated if locked is true
   Arg:
   Return Value: The number of positions recorded in the tree
*/

Uint4 SBuffer::ReadSeq() {
//    if (locked)
//        SysDie(0,"Internal ERROR - The object is locked\n");
    return memory_mode ? __ReadSeqMemory() : __ReadSeqFile();
}


/*
   Function:  SBuffer::__ReadSeqMemory
              This function is called by ReadSeq when in memory_mode
              Only ONE pass is possible (the -p switch cannot be used when in this mode)

   Arg:       none (every parameter is taken from database)
   Return Value: The number of positions recorded in the tree
*/

Uint4 SBuffer::__ReadSeqMemory()
{
    string filename = database.GetFileName();
    FILE* in;         // The input file, in fasta format

    in = fopen(filename.c_str(),"r");
    if (in == NULL)
    {
        string msg = "Cannot open file " + filename;
        SysDie(errno,msg.c_str());
    };

    fprintf(stderr,"Reading the sequence \n");

    //PutSeparator();     // Writing $ at beginining of fa_bfr
    //fa_bfr.get()->Put('$'); // Writing $ at beginining of fa_bfr... without incrementing position
    size_t positions = __ReadSeq(in);
    fclose(in);

    /*cerr << "Memory used = " << MemoryPool::Instance()->MemoryUsed()/1048576 << "Mb ";
    cerr << "(allocated " << MemoryPool::Instance()->MemoryAllocated()/1048576 << "Mb)\n";
    cerr << "       Nodes     = " << Node::MemoryUsed()/1048576 << "Mb\n";
    cerr << "       Leaves    = " << Leaf::MemoryUsed()/1048576 << "Mb\n";
    cerr << "       Positions = " << Pos::MemoryUsed()/1048576 << "Mb\n";*/

    index_tree.WriteCompressed(bi_bfr.get());
/*
    FILE* out = fopen("debug.bind.bi","w");
    fwrite(bi_bfr.get()->GetBase(), sizeof(char),bi_bfr.get()->size(), out);
    fclose(out);
    fa_bfr.get()->Put('\n');
    out = fopen("debug.bind.bi.fa","w");
    fwrite(fa_bfr.get()->GetBase(), sizeof(char),fa_bfr.get()->size(), out);
    fclose(out);
    ofstream o("debug.bind.bi.he");
    o<<database;
*/
    // Give the ownership of bi_bfr and fa_bfr to database
    // WARNING - If you eecute ReadSeq 2 times, plantage !
    // This should be solved with putting the next line in the destructor, but then I have very poor performances

    database.SetBfr(bi_bfr,fa_bfr);
    //locked = true;                    // Preventing from any other indexation

    //cerr << database.GetNbWords() << ' ' << database.GetLength() << ' ' << database.GetNbPos() << '\n';
    return positions;
}

/*
   Function:  SBuffer::__ReadSeqFile
              This function is called ReadSeq when not in memory_mode
              ReadSeq opens the input file, and calls __Readseq for every pass
              The fa_file file descriptor is opened and written at the first pass

   Arg:       none (every parameter is taken from database)
   Return Value: The number of positions recorded in the tree
*/

Uint4 SBuffer::__ReadSeqFile()
{
    string filename = database.GetFileName();
    FILE* in;         // The input file, in fasta format
    FILE *bi_file;    // The binary index file
    char c;

    string bi_filename = filename + BI_EXTENSION;
    bi_file = fopen(bi_filename.c_str(),"w");
    if (bi_file == NULL)
    {
        string msg = "Cannot open file " + bi_filename + " for writing";
        SysDie(errno,msg.c_str());
    };

    in = fopen(filename.c_str(),"r");
    if (in == NULL)
    {
        string msg = "Cannot open file " + filename;
        SysDie(errno,msg.c_str());
    };

    int pass_nb=1;
    size_t positions=0;
    do
    {
		fprintf(stderr,"Reading the sequence : pass %d/%d\n",pass_nb,database.GetNbPass());
        //SBuffer bfr(index_tree,black_list,database,brute_pf); // Creating a new SBuffer object

        if (pass_nb==1)
        {
            string fa_filename = filename + FA_EXTENSION;
            fa_file = fopen(fa_filename.c_str(),"w");
            if (fa_file == NULL)
            {
                string msg = "Cannot open file " + fa_filename + " for writing";
                SysDie(errno,msg.c_str());
            };

            /*
               Writing $ at beginining of fa_file file, as a separator between sequences in a multifasta file
               This is done when opening the file, because it is not counted as a 'position'
            */
           /* char c='$';
            fwrite(&c,sizeof(char), 1, fa_file);*/

            positions += __ReadSeq(in);  // Index for this pass, reading the fasta file and dumping to fa

            fclose(in);     // For the other passes, read from the fa file

            c='\n';
            fwrite(&c,sizeof(char), 1, fa_file); // Better terminating the file by \n
            fclose(fa_file);

            fa_file=NULL;
            in = fopen(fa_filename.c_str(),"r");
            if (in == NULL)
            {
                string msg = "Cannot open file " + fa_filename + " for reading";
                SysDie(errno,msg.c_str());
            };
        }
        else
        {
            rewind(in);
            //fgetc(in);      // Skipping the first '$'
            ResetPos();
            positions += __ReadSeq(in);  // Index for this pass, reading the fa file (now opened through in)
        };

        /*cerr << "Memory used for this pass = " << MemoryPool::Instance()->MemoryUsed()/1048576 << "Mb ";
        cerr << "(allocated " << MemoryPool::Instance()->MemoryAllocated()/1048576 << "Mb)\n";
        cerr << "                Nodes     = " << Node::MemoryUsed()/1048576 << "Mb\n";
        cerr << "                Leaves    = " << Leaf::MemoryUsed()/1048576 << "Mb\n";
        cerr << "                Positions = " << Pos::MemoryUsed()/1048576 << "Mb\n";*/

        index_tree.WriteCompressed(bi_file);

        //cerr << pass_nb << ' ' << database.GetNbWords() << ' ' << database.GetLength() << ' ' << database.GetNbPos() << '\n';
	} while ((pass_nb=index_tree.NextPass())!=0);
	fclose(bi_file);
	//fprintf(stderr,"Leaving the passes \n");


   return positions;
}

/*
   Function:  SBuffer::__ReadSeq
              This function is called by SBuffer::Readseq() when indexing the bank fasta file.
              If the fa_file file descriptor is != NULL, the sequence file is reformatted and written to out:
                 1/ The headers and the \n are removed
                 2/ Sequences in a multifasta file are separated by $$$$$$$$$$
                 3/ The first character of the file is $, as well as the last character
                 4/ The low complexity regions are written in low case
                 5/ Dust is called if necessary

   Arg:       in         => An opened file descriptor used for reading
              out        => An opened file descriptor used for writing.
                            If out==NULL, nothing written
              index_tree => The indexation tree (used for input AND output)
              black_list => The black list (used for input AND output)
              database   => The DataBase parameters (used for input AND output)

   Return Value: The number of positions recorded in the tree
*/

Uint4 SBuffer::__ReadSeq(FILE *in)
{
	int  c;
	int i;

	size_t Sequence_Length = 0;  /* Size of the sequences that will be indexed */
    size_t length;           /* Size of the resulting buffer including the separators */
    {
        while ((c=fgetc(in))!=EOF)
        {
            if (c=='>')                                         // Next sequence
            {
                string sequence_name = GetSeqName(in);
                if (sequence_name=="")
                    break;                                      // Job finished
                for ( i = 0 ; i < SEQEND_BUFFER_SIZE ; i++ ) {                            // write 10 '$'
                    PutSeparator();

                if (GetPosition() != 0 && GetPosition()%5000000==0)
                        cerr<<"Database position "<<GetPosition()<<endl;
                };
                if (index_tree.GetPass()==1)
                    database.PushSeq(sequence_name,GetPosition()+database.GetMsk().GetLength());    // dirty trick again TODO tenter de modifier ce principe
            }
            else
            {
				Putc(c);
				if ( index_tree.GetPass()==1 && c!='\n')
					Sequence_Length++;
                if (GetPosition() != 0 && GetPosition()%5000000==0)
                    cerr<<"Database position "<<GetPosition()<<endl;
            };
        };
		if (index_tree.GetPass()==1)
		{
		  	for ( i = 0 ; i < SEQEND_BUFFER_SIZE ; i++ ) {                            // write 10 '$'
         		PutSeparator();
			}
			database.SetSequenceLength( Sequence_Length );
		}
        length = GetPosition()+1+database.GetMsk().GetLength(); // do not forget the dirty trick
    };

    _Syncr();
		
	cerr<<"Sequence length = "<<database.GetSequenceLength()<<endl;
	database.SetLength( length );

	return length;
}

void SBuffer::_MaskLowComplexity()
{
    size_t size = fa_bfr->size();
    char*  base = (char*) fa_bfr->GetBase();
    int min=(size-database.GetMsk().GetLength()>0) ? size-database.GetMsk().GetLength() : 0;
    for (size_t i=min; i<size;i++)
    {
        //base[i] = tolower(base[i]);
        base[i] = 'N';
    };
}

/* QBUffer constructor */

QBuffer::QBuffer(const DataBase_Parameters& db, const Compare_Parameters& cp):
database(db), compare(cp),ctree(_init_tree(db,cp)),query_database(NULL),
Length(0),window(db.GetMsk()),query_bfr(__InitBfrPtr()),out(_OpenOutputFile()),
match_results(MATCH_PILE_SIZE,*this,out)  /* Changé par Thomas le 29/09/06  en   window(db.GetMsk()) */
{
    Ind_Match[0]=0;
    Ind_Match[1]=0;
    Clear();
}

/*
   Procedure: QBuffer::ReadSeqQuery
              This function is called to read the query sequence from a multifasta file (1st version) or from
              an already indexed databank, using the .fa reformatted sequence (switch -G).
              The sequence is deposited in the Qbuffer, and when the sequence is read it is processed

   Arg:       fp         => An opened file descriptor used for reading
              qbfr       => The QBuffer object
   or:        db         => A DataBase_Parameters object
              qbfr       => The QBuffer object

   Return Value: none
   Globals: none

*/

void QBuffer::ReadSeqQuery(const string& file_query)
{
    FILE* fp = (file_query=="-" || compare.Pipe) ? stdin : open_file(file_query.c_str(),"","r");

    if (!compare.Self)
    {
        signed char c;
        while((c=fgetc(fp))!=EOF)
        {
            if (c=='>') {
                ProcessMatches();         // Process the previous sequence, or make nothing if first seq
				PutSeparator();
                string nom=GetSeqName(fp);
                //cerr<<"processing :"<<nom<<"\n";
				logger->SetLogType("L") << "Query name : "<<nom<<"\n";
                if (nom=="")
                    break;
                SetSeqId(nom);
            }
            else
            {
				Putc(c);
                if (GetPosition()%5000000==0)
                    cerr<<"Here query position "<<GetPosition()<<endl;
            };
        };
        ProcessMatches();
    }
    else
    {
        ProcessMatches();
    };
}

void QBuffer::ReadSeqQuery(const DataBase_Parameters& query)
{
    string file_name = query.GetFileName() + FA_EXTENSION;
    FILE * fp        = open_file(file_name.c_str(),"","r");
    signed char c;
    Uint2 nb_seq = query.GetNbSeq();
    const vector<Sequence_Positions> seq_info = query.GetSeqInfo();
    Uint2 ct_seq = 0;
	
	if(!compare.Silent)
    	cerr << file_name << "  nb seq="<<query.GetNbSeq()<<"\n";

    if(!compare.Self)
    {
    	while((c=fgetc(fp))!=EOF)
    	{
        if (c=='$') {
	    	ProcessMatches();         // Process the previous sequence, or make nothing if first seq
	     if (ct_seq==nb_seq)
                break;
            SetSeqId(seq_info[ct_seq++].Name);
			if(!compare.Silent)
	   			cerr<<"SeqId : "<<GetSeqId()<<"\n";
				while(c=='$') {
					c=fgetc(fp);               // Skip $$$$$

				}
            if (c==EOF)                    // should not happen
                break;
				PutSeparator();
        };
			//Putc(toupper(c));                 /* Supprimé pour avoir une compatibilité avec softmasking  */
			Putc(c);
		  if (GetPosition()%5000000==0 && !compare.Silent)
            cerr<<"Here query position "<<GetPosition()<<endl;
   	  };
     } else {
		ProcessMatches();
     }
}

CTree& QBuffer::_init_tree(const DataBase_Parameters& db, const Compare_Parameters& cp)
{
	CTree* ctree_ptr;

	if (cp.Self) {
		ctree_ptr=new CTreeSelf(db,cp);
	} else {
		ctree_ptr=new CTree(db,cp);
	}

	return *ctree_ptr;
}

Buffer& QBuffer::__InitBfrPtr()
{
    // In Self mode, the object Buffer is used to wrap an already existing memory area
    if (compare.Self)
    {
        query_bfr_ptr = Buffer_ptr(new Buffer((unsigned char*) ctree.GetSeq(),database.GetLength()));
    }
    // In non Self mode, we alloc a memory area, it will be managed by Buffer
    else
    {
        query_bfr_ptr = Buffer_ptr(new Buffer(QBUFFER_INITIAL_SIZE));
    }
    return *(query_bfr_ptr.get());
}

/*
   Procedure: ProcessMatches
              This function is called when the QBuffer is filled with a complete sequence, to begin processing the matches:
                 1/ dust is called if necessary
                 2/ _CompareDinucleotides or _Compare is called (depends on compare.complexity)
                 3/ compute_matches TODO CHANGER
                 4/ The temporary variables are cleared TODO
                 5/ The QBuffer is cleared

   Arg:       none
*/

void QBuffer::ProcessMatches()
{
    unsigned char * Seq;

	if(!compare.Self)
    {
       if(query_bfr.IsEmpty())      // Nothing to process
            return;
          Length = query_bfr.size() - 1;   // Seq[0] is not counted
		  PutSeparator();                 // '$' at first and last position are useful to avoid overflows later
		  Seq = (unsigned char*) query_bfr.GetBase();
    }
    else
    {
        Length = ctree.database.GetLength();
        Seq = (unsigned char*) ctree.GetSeq();
    };

    if (Length < window.GetLength()) {
		if (!compare.Silent)
		{	
			cerr <<"Warning : sequence length "<< Length<<" smaller than mask length "<< (int)window.GetLength()<<", sequence "<<GetSeqId()<< " ignored \n";
    	}
		return;
    }

    if (compare.complexity==COMPLEXITY_DUST)
        dust(Length,Seq+1);

    if (compare.complexity==COMPLEXITY_DINUCLEOTIDES)
        _CompareDinucleotides();
    else
        _Compare();

#ifdef DEBUGLOG
    logger->SetLogType("L") << "Now computing hsp  \n";
#endif

    match_results.ComputeHsp();

	if (compare.Linking)
	{
		 //SortingAndLinking();
	}
	else if (!compare.OutputStream && !compare.Fill)
	{
		match_results.PrintAllHsp(GetSeqId(),database.GetName());
	}

    match_results.HspReset();
    //match_results.Status();

    Clear();
}

void QBuffer::_CompareDinucleotides()
{
    window.Reset();

    for (Uint4 pos=1; pos <= Length; pos++,Position++)  // Starting at 1 because skipping 0 ('$')
    {
        char c=*((char*)query_bfr.GetBase() + pos);
        if (!Word::IsCode(c)) {     // Masked region
            if (compare.Self && c=='$' && !window.IsEmpty())
               match_results.GetCurrentMatchPile().ComputeSimpleMatches();
            window.Reset();
        }
        else
        {
            window.ShiftComp(c);        // The complexity is filtered out on the request
            if (window.IsFull()) {
                //cerr << window.GetWord().Convert() << '\n';
                if (window.IsComplex() && pos % compare.step == 0) //compare_window_compact(position,p,window);
                ctree.CompareWindow(window,Position,match_results);
            }
        }
    }
}

void QBuffer::_Compare()
{
    window.Reset();

	 for (Uint4 pos=1; pos <= Length; pos++,Position++)  // Starting at 1 because skipping 0 ('$')
    {
        char c=*((char*)query_bfr.GetBase() + pos);

        if (!Word::IsCode(c)) {     // Masked region
            window.Reset();
        }
        else
        {
            window.Shift(c);        // The complexity is filtered out on the request
            if (window.IsFull()) {
				if ( pos % compare.step == 0 )
					ctree.CompareWindow(window,Position,match_results);
            }
        }
    }
}

const unsigned char * QBuffer::GetSeqAsFasta() const
{
	string seq;
	Uint4 pos;
	for ( pos=1; pos <= Length ; pos++)  // Starting at 1 because skipping 0 ('$')
	{
		char c=*((char*)query_bfr.GetBase() + pos);
		seq.append(1,c);
		if ( pos%60 == 0)
			seq.append(1,'\n');
	}
	if ( (pos-1)%60 != 0)
		seq.append(1,'\n');
	return (unsigned char *)seq.c_str();
}


QBuffer* QBuffer::Duplicate()
{
	Compare_Parameters cp(compare);

	cp.Fill=true;
	cp.lower_level=ROOT_LEVEL;
	cp.deepest_level=ROOT_LEVEL;
	cp.Cutoff_Hsp=20;
	cp.OutputStream=true;
	cp.Linking=false;

	return new 	QBuffer(database,cp);

}

#ifdef BLABLA
void QBuffer::SortingAndLinking()
{
	QBuffer* query_son=this->Duplicate();
	char* Query_begin=(char*)query_bfr.GetBegin();
	Uint4 qgapsize,sgapsize;
	Uint4 sg_start, sg_end;

	for (int i=0; i<2; i++) {
		match_results.SetStrand(i);
		MatchPile& mp = match_results.GetCurrentMatchPile();
		mp.Linking();
		mp.GetHspPile()->SwitchPiles();
		for (list<chain*>::const_iterator i=mp.GetHspPile()->GetTheChains()->begin();i!=mp.GetHspPile()->GetTheChains()->end();i++)
		{
			if ((**i).hsp_synt.size()>4 && (**i).concorient) {
				list<hsp*>::const_iterator previous=(**i).hsp_synt.begin();
				(**i).strand=(**previous).strand;
				fprintf(stdout,"Real chain orientation : %c \n",((**i).strand==PLUS) ? '+' : '-');
				for (list<hsp*>::const_iterator j=++(**i).hsp_synt.begin();j!=(**i).hsp_synt.end();j++)
				{
					qgapsize=(**j).q_start-(**previous).q_end+1;
					if ((**i).strand==PLUS) {
						sg_start=(**previous).s_end;
						sg_end=(**j).s_start;
					} else {
						sg_start=(**j).s_start;
						sg_end=(**previous).s_end;
					}
					sgapsize=sg_end-sg_start+1;
					if (qgapsize>6000 && 0) {
						fprintf(stdout,"gap size query : %d  subject : %d \n",qgapsize,sgapsize);
						query_son->query_bfr.SetBegin((void*)(Query_begin+(**previous).q_end));
						query_son->compare.s_start4fill=sg_start;
						query_son->compare.s_end4fill=sg_end;
						//logger->SetLogType("L") << "Trying to fill a new gap \n";
						//DisplayQuerySeq(qgapsize,(**previous).q_end,(**j).q_start);
						query_son->ProcessMatches();
					}
					previous=j;
				}
				fprintf(stdout,"\n");
			}
		}
	}
	query_bfr.SetBegin((void*)(Query_begin));
}
#endif

void QBuffer::DisplayQuerySeq(Uint4 length,int start, int end)
{
	fprintf(stdout,"Sequence length %u subsequence length %d: start = %d    end = %d %d\n",Length,length,start,end,window.GetSize());

	for (Uint4 pos=1; pos + window.GetLength() <= length ; pos++)  // Starting at 1 because skipping 0 ('$')
   {
		char c=*((char*)query_bfr.GetBase() + pos);
		fprintf(stdout,"%c",c);
	}
	fprintf(stdout,"\n");

}


/*void QBuffer::FillingTheGaps()
{

	for (list<chain*>::const_iterator i=hsp_chains.begin();i!=hsp_chains.end();i++)
	{
		if ((**i).hsp_synt.size()>4 && (**i).concorient) {
			fprintf(stdout,"Real chain \n");
			list<hsp*>::const_iterator previous=(**i).hsp_synt.begin();
			for (list<hsp*>::const_iterator j=++(**i).hsp_synt.begin();j!=(**i).hsp_synt.end();j++)
			{
				fprintf(stdout,"%c:",((**j).strand==PLUS) ? '+' : '-');
			}
			fprintf(stdout,"\n");
		}
	}

}*/






/*
     The output operator of the class BlackList
*/

ostream & operator<<(ostream& os, const BlackList& bl)
{
    for (int i=0;i<bl.length;i++)
    {
        const set<Word> s = bl.hash_table[i];
        for (set<Word>::const_iterator j=s.begin(); j!= s.end(); ++j)
        {
            os << j->Convert() << '\n';
        };
    };
    return os;
}





