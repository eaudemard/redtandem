/*
   $Id: database.h 1424 2008-03-06 09:07:57Z tfaraut $
*/


#ifndef __DATABASE_H__
#define __DATABASE_H__

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

#include "ptypes.h"
#include "buffer.h"
#include "masque.h"
#include "compare.h"

/*
    DataBase_Parameters - this class encapsulates the parameters used for the database indexation.
    Only 2 constructors are provided:
         -From the comand line (argv, argc)
         -From a file (the file name is passed by parameter)

    Tree and SBuffer are friends, they are used to index the database
    The operators << and >> are useful to store/retrieve the database parameters in a .he file

*/

class DataBase_Parameters;
class SBuffer;
class Tree;
class CTree;
ostream & operator<<(ostream&, DataBase_Parameters const &);
istream& operator>>(istream& is, DataBase_Parameters&);

struct Sequence_Positions
{
    Sequence_Positions(Uint4 s=0, string n=""):start(s),Name(n){};
    Sequence_Positions(const Sequence_Positions &sp) {start=sp.start;Name=sp.Name;};
    Uint4 start;       // changed by manu: Int4 ==> Uint4
    string Name;
};

class DataBase_Parameters: NonCopyable
{
    public:
    DataBase_Parameters(const string&);                     // Initializing the object from a file
    DataBase_Parameters(int, char**, void(*)());            // Initializing the object from the command line
	DataBase_Parameters(const Compare_Parameters& compare);  //Initilaizing from a Compare_Parameters object
	
    /* Accessors */
    string GetName() const {return Name;};
    string GetFileName() const {return FileName;};
    Buffer * GetBiBfr() const {return bi_bfr.get();};
    Buffer * GetFaBfr() const {return GetBiBfr()==NULL ? NULL : fa_bfr.get();};    // If bi_bfr is NULL, we return NULL also
    // After execution of SetBfr, the object is the new owner of bi_bfr and of fa_bfr - The original owner can be destroyed
    void SetBfr(Buffer_ptr bi, Buffer_ptr fa) {bi_bfr=bi; fa_bfr=fa;};
    Uint4 GetNbWords() const {return Words;};
    Uint8 GetNbSeq() const {return nb_seq;};
    Uint2 GetNbPass() const {return nb_pass;};
    Uint4 GetNbPos() const {return Positions;};
    const vector<Sequence_Positions> & GetSeqInfo() const {return seq_info;};
    Uint4 GetLength() const {return Length;};
    Uint4 GetSequenceLength() const {return SequenceLength;};
    ssize_t GetSize() const {return Size;};
    Uint4 GetPositions() const {return Positions;};
    Uint2 GetMaxCount() const {return Max_Count;};
    int GetNodeWidth(int level) const {return node_width[level];};
    const Mask& GetMsk() const {return msk;};
    string GetLogLevel() const {return log_level;};

    Sequence_Positions GetTrueSeqPos(Uint4) const;    // Given a position in the database, build the true corresponding SequencePosition

    /* Some mutators */
    void SetMsk(const string & s) { msk=s;};
	 //void ChangeMsk(const string & s) { msk.Change(s,blastzbehavior); };
	 //void SetBlastzBehavior(const bool& b) { blastzbehavior=b; msk.SetBlastzbBehavior(b);};
    void SetLength(Uint4 l) { Length=l;};
    void SetSequenceLength(Uint4 l) { SequenceLength=l;};
    void SetName(const string & n) {        // Set the Name, stripping every directory information
        string::size_type slash=n.find_last_of('/');
        if (slash==string::npos)            // no / found
            Name=n;
        else
            Name=n.substr(slash+1);
    };
    void PushSeq(const string& seq_name, size_t pos) {seq_info.push_back(Sequence_Positions(pos,seq_name)); nb_seq++;};

    friend istream& operator>>(istream& is, DataBase_Parameters&);
    friend ostream & operator<<(ostream&, DataBase_Parameters const &);
    friend class Tree;
    friend class SBuffer;
    friend int ReadComLine(int , char **, DataBase_Parameters& , int & );


    private:
    void _init_node_width()     // used by the constructors
    {
        //node_width[1]=5;    // sons + shift = 1 + 4
		node_width[1]=1+sizeof(RootLevelOffsetType);    // sons + shift = 1 + 8
        node_width[2]=6;    // hash_code + sons + shift = 1 + 1 + 4
        for(int i=3;i<=MAX_LEAF_LEVEL;i++) node_width[i]=4; // hash_code + sons + shift = 1 + 1 + 2
    };
    void Validation();     /* If the parameters are validated, OK - else, die */

    string Name;      /* database name (output display purpose)  */
    string FileName;  /* database filename (for the purpose of accessing the different file extensions)  */

    Uint4 Words;      /* nb of different words in the tree  */
    Uint4 Length;     /* Length (in nucleotides) of the database   */
    Uint4 SequenceLength;   /* Length (in nucleotides) of the database  sequence (shorter than the Length because here the separators are not counted) */
    size_t Size;      /* size in bytes of the compressed index .bind.bi */
    Uint4 Positions;  /* Total number of positions stored in the tree */
    Uint8 nb_seq ;    /* number of sequences of the database (mulifasta)*/
    vector<Sequence_Positions> seq_info;  /* correspondance between positions in the database (i.e in .bind.bi.fa) and the true position in the sequences */

    Mask msk;         /* The mask used for indexation */
    Uint2 Max_Count;  /* Maximum allowed occurence of a word, otherwise => black list */
    int node_width[MAX_LEAF_LEVEL+1];   /*The width, in bytes, for the node/leaf info at each mask level */

    int nb_pass;      /* number of passes in the indexation process */

    Uint2 complexity;   /* The complexity algorithm used for indexation (0/1/2) */

    Buffer_ptr bi_bfr; /* The binary index is stored in a Buffer in auto_index mode */
    Buffer_ptr fa_bfr;
    string log_level;   /* Used only in DEBUG mode - given by the -D option */
};
ostream & operator<<(ostream&, Sequence_Positions const &);
istream& operator>>(istream& is, Sequence_Positions&);

#endif
