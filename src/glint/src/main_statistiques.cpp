/*
   $Id: main_statistiques.cpp 230 2008-09-12 13:16:00Z tfaraut $
*/


/*main_compare.c*/

#include "index_io.h"
#include "utils.h"

extern MatchConstants* MatchScoring;

int ReadComLine(int, char**,bool&);
int Usage();
int Description();
STrivialLogger* logger;

/*////////////// Main //////////////*/
int main(int argc,char* argv[])
{
    bool read_only = false;
	int nbre_opt=ReadComLine(argc,argv,read_only);
	int i;

	/* aide */
	if (argc-nbre_opt<2) {
		Usage();
		exit(1);
	}

    /* Initializing logger: we use an auto_ptr to be sure that the destructor is called at end of program */
    /* Nothing logged by thi sprogram (using STrivialLogger) */
    auto_ptr<STrivialLogger> logger_ptr = auto_ptr<STrivialLogger> (new STrivialLogger("file","level"));
    logger = logger_ptr.get();

	string filename=argv[nbre_opt+1];
    fprintf(stderr,"Lecture du fichier index\n");
    fprintf(stderr,"Remplissage du tableau de hachage\n");

    DataBase_Parameters database(filename);
    Compare_Parameters compare;
    CTree comp_index_tree(database,compare);

	 MatchScoring=new MatchConstants(database.GetMsk(),(float)database.GetNbWords());

	 for (i=ROOT_LEVEL;i<=database.GetMsk().GetLeafLevel();i++)
    {
	 	cerr<< "Hit value at level "<<i <<" : "<< MatchScoring->score_matrix->s[i] <<"\n";
	 }

    if (read_only==false)
        cout << comp_index_tree << "===============\n";
	return 0;
}


int ReadComLine(int argc, char* argv[],bool& read_only)
{
	int nbre_opt=argc;

	while (--argc>0 && (*++argv)[0]== '-'){
		switch (*++argv[0]) {
			case 'r':
				read_only = true;;
				break;
			case 'h':
				Description();
				exit(0);
				break;
			default :
				fprintf(stderr,"\n*******************OPTION INTERDITE******************\n");
				break;
		}
	}
	return nbre_opt-argc-1;

}

/*Utilisation du programme */
int Usage()
{
	fprintf(stderr,"dnastat: Programme de test pour dnaindex\n");
	fprintf(stderr,"Usage: dnastat [options] fichier_sequence \n\n");
	fprintf(stderr,"\tfichier_index : nom du fichier qui a fait l'objet d'une indexation par dnaindex\n");
	fprintf(stderr,"\tOptions:\n");
	fprintf(stderr,"\t\t -r  read only (permet de tester uniqsuement le chargement de l'index)\n");
	return 1;
}

/* description de l'utilisation du programme*/
int Description()
{
	Usage();
	fprintf(stderr,"Description:\n");
	fprintf(stderr,"\tL'index est charg en mmoire et affich dans sa totalit. \n");
	fprintf(stderr,"\tL'option -r permet d'viter l'affichage de l'index et de tester ainsi uniqsuement le chargement de l'index\n");


	return 1;
}

