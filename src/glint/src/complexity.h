/*
   $Id: complexity.h 230 2008-09-12 13:16:00Z tfaraut $
*/


/* Complexity.h */

#ifndef _COMPLEXITY_H
#define _COMPLEXITY_H

#include "word.h"
#include <iostream>

/*

   The object Complexity manages the complexity of the sequence
   Main interface functions:
        Complexity(int)             The constructor, initializing the threshold
        void SetThreshold(int)      Set the threshold for detecting high/low complexity

        void Decrease(const Word &) Update the object just before pushing a new character in the word, decreasing the count
        void Increase(const Word &) Update the object just after pushing a new character in the word, thus increasing the count.
                                    Those 2 functions are thus an incremental update of the object.
        void Reset()                Reset the counter
        void SetThreshold(int)      Set the threshold for detecting high/low complexity
        bool IsComplex () const     Returns true if the complexity is enough high

   There are several flavours of the Complexity object:

        TrivialComplexity           No complexity managed at all
        DinucleotideComplexity      See the code for the details

   All flavours of Complexity are friends of Word - The flavour is chosen when the code is compiled (see the defines xxx_COMPLEXITY)

*/

/*
   TrivialComplexity: This trivial object does not make anytihng and returns always true to the IsComplex() function
*/

class TrivialComplexity: private NonCopyable {
    public:
    TrivialComplexity(int th) {};
    void  SetThreshold(int th){};

    void Reset(){};
    void Decrease(const Word &w){};
    void Increase(const Word &w){};

    bool IsComplex() const {return true;};
};

/*
   DinucleotideComplexity: We count the number of pairs of nucleotides in the word to decide if the complexity is high or not
*/

class DinucleotideComplexity: private NonCopyable {
    public:
    DinucleotideComplexity(int t) : threshold(t) {Reset();};
    void Reset() { count=0;for (int i=0; i<= 15; i++) dinucleotide[i]=0;};
    void SetThreshold(int t) {threshold=t;};
    bool IsComplex() const {return (count>threshold);};     // If count <= threshold, the word is considered as 'low complexity'

    /*
    Procedure:  Decrease
    Parameters: a word
    Usage:     To be called by Window JUST BEFORE Pushing the word
               The 4 msb of the word are considered as an OLD pair of nucleotides and are substracted
               from the count. This function should be called just BEFORE shifting the window
               Getc is not used for performance reasons.
               There are two versions of Decrease, related to the two versions of Word.
    */

    void Decrease (const Word &w);

    /*
    Procedure:  Increase
    Parameters: a word
    Usage:      To be called by Window's Shift JUST AFTER Pushing the word
                The 4 lsb of the word are considered as a NEW pair of nucleotides and are added
                There are two versions of Update, related to the two versions of Word.
    */

    void Increase (const Word &w);

    private:
    int count;
    int dinucleotide[16];   /* There are 4x4 = 16 possible pairs of nucleotides */
    int threshold;
};

#ifdef TRIVIAL_COMPLEXITY
typedef TrivialComplexity Complexity;
#else
typedef DinucleotideComplexity Complexity;
#endif

/* THE inline METHODS OF THE OBJECT: DinucleotideComplexity
   Decrease and Increase access the private section of Word (Complexity is a friend of Word)

*/

#ifdef WORD_IN_TWO_PARTS

inline void DinucleotideComplexity::Decrease (const Word &w)
{
    int l,m;
    int shift_count=w.size-4;
    if (shift_count>=REGISTER_SIZE) {    // Working with r[1]
        shift_count-=REGISTER_SIZE;
        m=15<<shift_count;
        l=w.r[1];
    } else {                             // Working with r[0]
        m=15<<shift_count;
        l=w.r[0];
    }
    l&=m;
    (l>>=shift_count)&15;
    if (--dinucleotide[l] == 0)
        count--;
}

inline void DinucleotideComplexity::Increase (const Word &w)
{
    if (dinucleotide[15&w.r[0]]++ == 0)
        count++;
};

#else

inline void DinucleotideComplexity::Decrease (const Word &w)
{
    int shift_count=w.size-4;
    int m=15<<shift_count;
    int l=m&w.r;
    if (--dinucleotide[(l>>=shift_count)&15] == 0)
        count--;
}

inline void DinucleotideComplexity::Increase (const Word &w)
{
    if (dinucleotide[15&w.r]++ == 0)
        count++;
}

#endif   // WORD_IN_TWO_PARTS

#endif   // COMPLEXITY_H

