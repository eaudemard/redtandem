/*
   $Id: chain.cpp 222 2008-05-28 06:35:44Z tfaraut $
*/

/* chain.cpp  */

#include <ctype.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <functional>
#include <sstream>
#include <algorithm>
#include <limits>
#include <math.h>

#include "chain.h"
#include "glint.h"
#include "utils.h"

/*
 Name:   AnchorVect::fromFile
 usage:  results.fromFile(filename);
 Procedure: Reads the anchors from a tab formatted file (in the Narcisse format: header using the HEADER_MARKUP as first character)
 */
void AnchorVect::LoadDataFromFile(const std::string& InputFile, MolType moltype)
{
	ifstream is(InputFile.c_str());
	string header, s;

	getline( is, header, '\n');

	if (  HEADER_MARKUP != header[0])
		{
		Quit("The first line should correspond to the header of glint -m 2 output format (see documentation) \n");
		}
	else
		{
		header.erase(0,1);
		}

	// Convert the header line of text into a list of tokens
	vector<string> vFieldName;
	GetLineTokens(vFieldName,header);

	// Reading the actual data
	while ( getline( is, s, '\n' ) )
		{
		AnchorNode* a=new AnchorNode();
		vector<string> v;
		GetLineTokens(v,s);
		if ( v.size() != vFieldName.size())
		{
			fprintf(stderr,"Error in data file %s \n", InputFile.c_str());
			fprintf(stderr,"Number of columns in line \n%s\ndifferent from the header\n",s.c_str());
			exit(EXIT_FatalError);
		}
		for (unsigned int i = 0; i < v.size(); i++)
			{
				a->SetField(vFieldName[i],v[i]);
			}
		a->Set_Centers();
		a->Set_Span( a->Size() ); //At the anchor level the span and size have identical values
		a->Set_Molecule(moltype);
		a->Select();

		//Warning, we check the orientation according to the order of start and end
		a->Check_Orientation();
		push_back(a);
		}
}

//--------------------------------------------------------------------------------------------
// Managing the views

/*
 Name:   AnchorVect::SetView
 usage:  results.SetView(VIEW)
 Procedure: Sets the view for all anchors (VIEW is QUERY or SUBJECT)
 */

void AnchorVect::SetView(ViewType v)
{
	//cerr<<"Changing view from "<<View2String(_view_type)<<" to "<<View2String(v)<<endl;
	if ( v == _view_type )
		return;
	_view_type=v ;
	for ( AnchorNodeIterator i = begin() ; i != end() ; ++i )
	{
		AnchorNode *a=*i;
		a->SetView(_view_type);
	}
	for ( vector<Cluster*>::iterator i = partition.begin() ; i != partition.end() ; ++i )
	{
		Cluster *a=*i;
		a->SetView(_view_type);
	}

}

//--------------------------------------------------------------------------------------------
// Managing the orders

/*
 Name:   AnchorVect::OrerLabel
 usage:  results.OrderLabel()
 Procedure: Orders the anchors according to their start position along the QUERY or the SUBJECT according to the current view
 */
void  AnchorVect::OrderLabel()
{
	int k=1;
	sort(begin(),end(),CmpAnchorNodePtr());  // The vector elements (AnchorNode *) are first sorted
	for ( AnchorNodeIterator i = begin() ; i != end() ; ++i )
	{
		AnchorNode *a=*i;
		a->SetOrder(k++);
	}
}

/*
 Name:   AnchorVect::SetAnchorsOrder
 usage:  results.SetAnchorsOrder()
 Procedure: Set the anchors order label on both side (QUERY and SUBJECT)
 */
void AnchorVect::SetAnchorsOrder()
{
	ViewType saved_view=_view_type;

    SetView(QUERY);
    OrderLabel();
    SetView(SUBJECT);
    OrderLabel();

    SetView(saved_view);

}


//The actual cluster (chain construction)


/*
 Name:   AnchorVect::DetectChains
 usage:  results.DetectChains()
 Procedure: Detects the conserved chains (conserved syntenies ?)
 */

Uint4 AnchorVect::DetectChains()
{

	if ( size() == 0 )
		return 0;

	cerr<<"\tConstructing the graph structure (max_dist = "<<MaxDist<<") "<<size()<<" elements "<<endl;
	InitGraphStructure();

	cerr<<"\tDetecting connected components "<<endl;
	ConnectedComponents();

	cerr<<"\tSetting cluster properties "<<endl;
	ConstructClustersInfo();

	//cerr<<PartitionSize()<<" clusters "<<endl;

	cerr<<"\tSelecting clusters "<<endl;
	SelectClusters();

	return PartitionSize();
}

bool NonZeroCurrentCluster(Uint4 first, Uint4 second)
{
	if (first==0 || second==0)
		return false;
	if ( first == second )
		return true;
	else
		return false;
}

/*
 Name:   AnchorVect::RemoveRepeats
 usage:  RemoveRepeats()
 Procedure: Remove highly repeated segments of both the query and the subject sequences

 */
void AnchorVect::RemoveRepeats()
{
	ViewType saved_view=_view_type;

	SetView(QUERY);
	_RemoveRepeats();
	SetView(SUBJECT);
    _RemoveRepeats();

    SetView(saved_view);
}

/*
 Name:   AnchorVect::_RemoveRepeats
 usage:  _RemoveRepeats()
 Procedure: For each overlapping anchors, store them in an overlap object.
            Deselect all the anchors if the overlap depth is greater than MAX_REPEATS

*/
void AnchorVect::_RemoveRepeats()
{
	Overlap o;

	sort(begin(),end(),CmpAnchorNodePtr());  // The vector elements are first sorted according to the query start)
	AnchorNodeIterator  i=begin();
	do {
		AnchorNodeIterator j = i;
		o.ReSet( *j );
		do {
			j++;
		}
		while ( j!= end() && o.Merging(*j) );

		if ( o.Depth()> MAX_REPEATS )
			o.DeselectMembers();
		i = j;
	}
	while ( i != end() );
}


/*
 Name:   AnchorVect::ClusterChaining
 usage:  ClusterChaining()
 Procedure: Chain the anchors according to a clustering information provided in the file (this applies only to PROT matches)
 Principle: The genome PROT matches were obtained by projecting each protein hsp onto the genome, so each hsp is represented
            as a chain (according to the different implied exons)
            The score of the chain is precisely the score of the corresponding protein hsp.

 */
void AnchorVect::ClusterChaining()
{
	std::map<Uint4,Cluster*> m_clusters;

	sort(begin(),end(),CmpAnchorNodeClusterPtr());  // The vector elements are first sorted according to the cluster id

	for (AnchorNodeIterator  i=begin(); i!=end(); i++) {

		if ((*i)->Molecule() != PROT || !(*i)->Selected() )
			continue;

		AnchorNode *ai=*i;
		map<Uint4,Cluster*>::iterator it;
		it=m_clusters.find(ai->ClusterId());
		if( it==m_clusters.end() )
		{
			Cluster* c= new Cluster();
			it=m_clusters.insert ( it, pair<Uint4,Cluster*>(ai->ClusterId(),c) );
		}
		it->second->members.push_back(ai);
	}

	Uint4 k=1;
	for ( map<Uint4,Cluster*>::iterator i = m_clusters.begin(); i!=m_clusters.end(); ++i, k++)
	{
		Cluster* c=i->second;
		c->SetClusterNum(k);
		c->SetClusterProperties();
		//We correct here for the score (each hsp-segment has the same score as the initial hsp)
		AnchorNode *first =c->members.front();
		c->Set_Score(first->Score());
		partition.push_back(c);
		c->Select();
	}
}

bool ConsecutiveAnchors(AnchorNode* first, AnchorNode* second)
{
	if (first == NULL || second == NULL)
		return false;
	if ( first->Q_Name() == second->Q_Name() && first->S_Name() == second->S_Name() && std::abs((long)first->S_Order()-(long)second->S_Order()) == 1 )
		return true;
	else
		return false;
}

/*
 Name:   AnchorVect::TrivialChaining
 usage:  TrivialChaining()
 Procedure: Consecutive DNA, or simply ANCHOR, anchors are simply chained

 */
void AnchorVect::TrivialChaining()
{
	AnchorNode* previous_anchor=NULL;
	Cluster* c;
	vector<Cluster*>::iterator FirstDNACluster = partition.begin();

	Uint4 NbProtCluster = partition.size();

	sort(begin(),end(),CmpAnchorNodeQOrderPtr());  // The vector elements are first sorted according to the query start)

	for (AnchorNodeIterator  i=begin()+1; i!=end(); i++) {

		if ((*i)->Molecule() == PROT || !(*i)->Selected() ) // We do not want to chain proteins in the first pass
			continue;

		AnchorNode *ai=*i;
		if ( ! ConsecutiveAnchors( ai, previous_anchor ) )
		{
			c=new Cluster();
			c->Set_Molecule(DNA);
			partition.push_back(c);
		}
		c->members.push_back(ai);
		previous_anchor = ai;
	}

	vector<Cluster*>::iterator start = partition.begin();
	advance( start, NbProtCluster);
	int k= NbProtCluster + 1;
	for (vector<Cluster*>::iterator i= start; i!=partition.end() ; ++i, k++ )
	{
		Cluster* ci=*i;
		c->SetClusterNum(k);
		ci->SetClusterProperties();
		ci->Select();
	}
}

/*
 Name:   AnchorVect::InitGraphStructure
 usage:  InitGraphStructure()
 Procedure: Constructs and edge between any pair of anchors verifying a (Manhattan) distance constraint
            The graph is oriented with  i-> j where i<j according to the order on anchors defined by the '<' operator
 */
void AnchorVect::InitGraphStructure()
{
	int k=0;

	sort(begin(),end(),CmpAnchorNodePtr());  // The vector elements are first sorted
	for (AnchorNodeIterator  i=begin();i!=end()-1;i++, k++) {
		AnchorNode *ai=*i;
		if ( !ai->Selected() )
			continue;
		for (AnchorNodeIterator  j=i+1;j!=end() && (*j)->Q_Start() < (*i)->Q_End() + MaxDist;j++ )
		{
			AnchorNode *aj=*j;
			if ( !aj->Selected() )
				continue;
			edge e;
			e.score= __ManhattanSegments(*ai,*aj);
			//fprintf(stdout,"d(%d,%d)=%u (qstart= %u and q_start=%u)\n",ai->Q_Order(),aj->Q_Order(), e.score,ai->Q_Start(),aj->Q_Start());
			if ( e.score < MaxDist )
			{
				e.n = &(*aj);
				ai->AddNeighbour(e);
			}
		}
	}
}

/*
 Name:   AnchorVect::ConnectedComponents
 usage:  ConnectedComponents()
 Procedure: Constructs the connected component of the graph (Cormen, Leiserson, Rivest, Introduction to Algorithms)
 */
void AnchorVect::ConnectedComponents()
{
	for (AnchorNodeIterator i=begin();i!=end(); ++i) {
		AnchorNode *ai=*i;
		ai->father=&(*ai);
	}

	for (AnchorNodeIterator i=begin();i!=end();i++) {
		AnchorNode *ai=*i;
		for (vector<edge>::iterator j=ai->neighbours.begin();j!=ai->neighbours.end(); ++j) {
			if (ai->father != j->n->father)  // If i an j belongs to to different sets
				j->n->father=ai->father;   //Union of i and j => they both point on i
		}
	}

	map<AnchorNode*,Cluster*>::iterator it;
	for (AnchorNodeIterator i=begin();i!=end(); ++i) {
		AnchorNode *ai=*i;
		it=clusters.find(ai->father);
		if( it==clusters.end() )
		{
			Cluster* c= new Cluster();  /* Ceci n'est pas très propre, on pourrait envisager une gestion dynamique  */
			it=clusters.insert ( it, pair<AnchorNode*,Cluster*>(ai->father,c) );
		}
		it->second->members.push_back(ai);
	}
}
/*
 Name:   AnchorVect::ConstructClustersInfo
 usage:  ConstructClustersInfo()
 Procedure: Set the properties of the different clusters
 */

void AnchorVect::ConstructClustersInfo()
{
	Uint4 k=1;
	for ( map<AnchorNode*,Cluster*>::iterator i = clusters.begin(); i!=clusters.end(); ++i, k++)
	{
		Cluster* c=i->second;
		c->SetClusterNum(k);
		c->SetClusterProperties();
		partition.push_back(c);
	}
}

/*
 Name:      AnchorVect::Rbh
 Usage:     Rbh()
 Function:  Detects and removes overlapping anchors (repeats or simple overlapping anchors) in a reciprocal best hit fashion
 Arg:       none
 Returns:   ideally a list of anchors such as an object of type chain
*/

void AnchorVect::PartitionRbh()
{

	__DetectAndRemoveOverlap(QUERY);
	__DetectAndRemoveOverlap(SUBJECT);

}

/*
 Name:      AnchorVect::Rbh
 Usage:     Rbh()
 Function:  Detects and removes overlapping anchors (repeats or simple overlapping anchors) in a reciprocal best hit fashion
 Arg:       none
 Returns:   ideally a list of anchors such as an object of type chain
*/

void AnchorVect::Rbh(Uint4 maxoverlap)
{

	__DetectAndRemoveSegmentOverlap(QUERY, maxoverlap);
	__DetectAndRemoveSegmentOverlap(SUBJECT, maxoverlap);

}


/*
 Name		:  AnchorVect::__DetectAndRemoveSegmentOverlap
 Usage		:  __DetectAndRemoveSegmentOverlap(side)
 Function	:  Detects and removes overlapping segments on the specified side
 Return		:  none
 Arg		:  the side that has to be considered
 Error		:  none
*/

void AnchorVect::__DetectAndRemoveSegmentOverlap(ViewType side, Uint4 MaxOverlap)
{
	vector<AnchorPair> overlapping;
	AnchorPair o;

	ViewType saved_view=_view_type;

	SetView(side);
	sort(begin(),end(),CmpAnchorNodePtr());
	for (AnchorNodeIterator  i=begin() ; i!= end()-1 ; ++i )
	{
		for (AnchorNodeIterator  j = i+1; j != end() ; ++j )
		{
			if ( OverlapSize(*i,*j) > MaxOverlap )
			{
				o.i=*i;
				o.j=*j;
				overlapping.push_back(o);
			}
		}
	}

	for (vector<AnchorPair>::iterator k=overlapping.begin(); k !=overlapping.end(); k++)
	{
		if (  k->i->Selected() && k->j->Selected() )
		{
			if ( k->i->Score() > k->j->Score() )
			{
				//cerr<<"rbh : "<<*(k->j)<<endl;
				k->j->Deselect() ;
			}
			else
			{
				//cerr<<"rbh : "<<*(k->j)<<endl;
				k->i->Deselect() ;
			}
		}
	}

	 SetView(saved_view);

}


/*
 Name		:  AnchorVect::__DetectAndRemoveOverlap
 Usage		:  __DetectAndRemoveOverlap(side)
 Function	:  Detects and removes overlapping anchors on the specified side
 Return		:  none
 Arg		:  the side that has to be considered
 Error		:  none
*/

void AnchorVect::__DetectAndRemoveOverlap(ViewType side)
{
	vector<AnchorPair> overlapping;
	AnchorPair o;

	ViewType saved_view=_view_type;

	SetView(side);
	sort(partition.begin(),partition.end(),CmpAnchorNodePtr());
	for (vector<Cluster*>::iterator i=partition.begin() ; i!= partition.end()-1 ; ++i )
	{
		for (vector<Cluster*>::iterator j = i+1; j != partition.end() ; ++j )
		{
			if (OverlappingAnchors(*i,*j))
			{
				o.i=*i;
				o.j=*j;
				overlapping.push_back(o);
			}
		}
	}

	for (vector<AnchorPair>::iterator k=overlapping.begin(); k !=overlapping.end(); k++)
	{
		if (  k->i->Selected() && k->j->Selected() )
		{
			//cerr<<"The overlap is "<<OverlapSize(k->i,k->j)<<endl;
			if ( k->i->Score() > k->j->Score() )
			{
				k->j->Deselect() ;
				//cerr<<"Deselect "<<*(k->j)<<endl;
			}
			else
			{
				k->i->Deselect() ;
				//cerr<<"Deselect "<<*(k->i)<<endl;
			}
		}
	}

	 SetView(saved_view);

}

void AnchorVect::PropagateSelection()
{
	for (vector<Cluster*>::iterator i=partition.begin() ; i!=partition.end() ; ++i )
		{
			if ( true == (*i)->Selected() )
			{
				(*i)->__DetectAndRemoveOverlap(QUERY);
				(*i)->__DetectAndRemoveOverlap(SUBJECT);
			}
			else
			{
				(*i)->DeselectedMembers();
			}
		}
}

/*
 Name:   AnchorVect::__ManhattanSegments
 Usage:  d=__ManhattanSegments(u,v)
 Function: Calculates the Manhattan distance between two segments defined as the minimum Manhatan distance between their extremities
 Args:  Reference on the two segments (anchors)
 Returns: The Manhattan distance as an Uint4
 */

Uint4 AnchorVect::__ManhattanSegments(const Anchor& u, const Anchor& v)
{
	if ( u.Q_Name() != v.Q_Name() || u.S_Name() != v.S_Name() )
	{
		return numeric_limits<Uint4>::max();
	}
	Point U_0, U_1;
	Point V_0, V_1;

	if ( u.Orientation() == Plus )
	{
		U_0.x = u.Q_Start();
		U_0.y = u.S_Start();
		U_1.x = u.Q_End();
		U_1.y = u.S_End();
	}
	else
	{
		U_0.x = u.Q_Start();
		U_0.y = u.S_End();
		U_1.x = u.Q_End();
		U_1.y = u.S_Start();
	}
	if ( v.Orientation() == Plus )
	{
		V_0.x = v.Q_Start();
		V_0.y = v.S_Start();
		V_1.x = v.Q_End();
		V_1.y = v.S_End();
	}
	else
	{
		V_0.x = v.Q_Start();
		V_0.y = v.S_End();
		V_1.x = v.Q_End();
		V_1.y = v.S_Start();
	}

	Uint4  a = __ManhattanPoints( U_0, V_0 );  // d(U(0),V(0))
	Uint4  b = __ManhattanPoints( U_0, V_1 );  // d(U(0),V(1))
	Uint4  c = __ManhattanPoints( U_1, V_0 );  // d(U(1),V(0))
	Uint4  d = __ManhattanPoints( U_1, V_1 );  // d(U(1),V(1))

	Uint4 min1 = ( a < b ) ? a : b;
	Uint4 min2 = ( c < d ) ? c : d;

	return ( min1 < min2 ) ? min1 : min2;
}

/*
 Name:   GlintOutput::__ManhattanPoints
 Usage:  d=__ManhattanPoints(x0,y0,x1,y1)
 Function: Calculates the Manhattan distance between two points: a positive integer representing the move along the axes to get
           from one point to the other.
 Args:  The two point coordinates
 Returns: The Manhattan distance as an Uint4
 */

Uint4 AnchorVect::__ManhattanPoints(Point& U, Point& V)
	{

	Uint4 y=( V.y > U.y ) ? V.y - U.y : U.y - V.y ;
	Uint4 x=( V.x > U.x ) ? V.x - U.x : U.x - V.x ;

	return y+x ;
	}

void AnchorVect::Print(const string& filename)
{
	ofstream output;
	output.open(filename.c_str());

	sort(begin(),end(),CmpAnchorNodePtr());
	output<<"#"<<ANCHOR_HEADER<<endl;

	sort(begin(),end(),CmpAnchorNodePtr());
	for ( AnchorNodeIterator i = begin() ; i != end() ; ++i )
	{
		if ( (*i)->selected == true)
			output<<**i<<endl;
	}
}

/*
 Name:   PrintClusters
 usage: PrintClusters()
 Procedure: Print the clusters
 */

void AnchorVect::PrintClusters()
{
	sort(partition.begin(),partition.end(),CmpAnchorNodePtr());
	cout<<"#"<<CLUSTER_HEADER<<endl;
	for (vector<Cluster*>::iterator i=partition.begin(); i!=partition.end() ; ++i) {
		if ( (*i)->Score() > MinScore ) cout<<**i<<endl;
	}
}

/*
 Name:  PrintPartition()
 usage: PrintPartition()
 Procedure: Print the clusters
 */

void AnchorVect::PrintPartition(const string& filename)
{
	if (partition.size() == 0)
	{
		cerr<<"Warning you are trying to print an empty partition "<<endl;
	}

	ofstream output;
	output.open(filename.c_str());
	sort(partition.begin(),partition.end(),CmpAnchorNodePtr());
	output<<"#"<<CLUSTER_HEADER<<endl;
	for (vector<Cluster*>::iterator i=partition.begin();i!=partition.end(); ++i) {
		if ( (*i)->selected == true)
			output<<**i<<endl;
	}
	output.close();
}

/*
 Name:   AnchorVect::SelectClusters()
 usage:  o_xxx->SelectClusters()
 Procedure: Select (keep) the clusters that satisfy the conditions:
		 (i) score > ScoreCutoff (CS in the help)
		 (ii) covar > CovarCutoff (CCV in the help)
Comment : The anchors are by default selected, so are the clusters

 */

void AnchorVect::SelectClusters()
{
	for (vector<Cluster*>::iterator i=partition.begin();i!=partition.end(); ++i)
	{
		if ( (*i)->Score() < MinScore || fabs((*i)->CoVar()) < MinCovar) {
			//cerr<<"Deselecting "<<**i<<endl;
			(*i)->Deselect();
		}

	}
}

/*
 Name:   AnchorVect::UpdateClusterProperties()
 usage:  o_xxx->UpdateClusterProperties()
 Procedure: When RBH (Reciprocal Best Hits) have been selected within each cluster the cluster
            properties need to be updated (q_start, a_end ....)
 */

void AnchorVect::UpdateClusterProperties()
{
	for (vector<Cluster*>::iterator i=partition.begin(); i!=partition.end() ; ++i )
	{
		if ( (*i)->Selected() ) (*i)->SetClusterProperties() ;
	}
}

void AnchorVect::SetClusteringParameters(Uint4 max_dist, Uint4 min_score, float min_covar)
{
	MaxDist=max_dist;
	MinScore=min_score;
	MinCovar=min_covar;
}

/*
 Name:   AnchorVect::operator=(const vector<Cluster*>& part)
 usage:  a=c
 Procedure: affectation operator from cluster to Anchornode
 */

AnchorVect& AnchorVect::operator=(const vector<Cluster*>& part)
{
	clear();
	for (vector<Cluster*>::const_iterator i=part.begin();i!=part.end(); ++i) {
		if ( (*i)->selected == true ) push_back( (AnchorNode*) (*i) );
	}

	return *this;
}

/*------------------------------------------------------
 *             For debugging purpose only
 */


void AnchorVect::PrintDebug()
{
	for (vector<Cluster*>::iterator i=partition.begin();i!=partition.end(); ++i) {
			cerr<<**i<<endl;
			(*i)->PrintDebug();
	}
}

/*------------------------------------------------------
 *             The SyntenyLevel methods
 */

/*
 Name:   SyntenyLevel::SyntenyLevel( string cutoffs )
 usage:  new SyntenyLevel( cutoffs_str );
 Procedure: The constructor
 */
SyntenyLevel::SyntenyLevel( string cutoffs, Uint1 t, Uint2 l )
{
	std::istringstream iss( cutoffs );
	std::string token;

	 // The Manhattan distance cutoff
	 getline(iss, token, '-');
	 ManhattanCutoff = atoi( token.c_str() );
	 // The score cutoff
	 getline(iss, token, '-');
	 ScoreCutoff = atoi( token.c_str() );
	 // The covar cutoff
	 getline(iss, token, '-');
	 CovarCutoff = atof( token.c_str() );

	 type = t;
	 level = l;
}

void SyntenyLevel::FirstClustering( )
{
	/* Removing the highly involved parts defined by MAX_REPEATS */
	segments.RemoveRepeats();
	/* Ordering the anchors */
	segments.SetAnchorsOrder();
    /* Chaining the protein anchors (hsp) belonging to the same blastp match */
	segments.ClusterChaining();
	/* Chaining the consecutive dna anchors */
	segments.TrivialChaining();
}

void SyntenyLevel::Clustering( )
{
	/* Set the clusturing parameters */
	segments.SetClusteringParameters(ManhattanCutoff,ScoreCutoff,CovarCutoff);
	/* Ordering segments */
	segments.SetAnchorsOrder();
    /* Detecting clusters */
	segments.DetectChains();

	//segments.PrintDebug();

}

void SyntenyLevel::LastClustering( )
{
	/* Ordering segments */
	segments.SetAnchorsOrder();
    /* Detecting clusters */
}

void SyntenyLevel::ChainingSegments( )
{

	if ( type == INITIAL_LEVEL )
	{
		FirstClustering( );
	}
	else if (type == INTERMEDIATE_LEVEL )
	{
		Clustering( );
	}
	else // Last level nothing has to be done
	{
		//nothing to do
	}
}

void SyntenyLevel::Rbh( Uint4 maxoverlap )
{
	segments.Rbh( maxoverlap );
}


void SyntenyLevel::GetSegmentsFromLowerLevel(SyntenyLevel* A )
{
	if ( type == INITIAL_LEVEL)
	{
		//Nothing to do
	}
	else
	{
		segments=A->segments.GetPartition();
		segments.SetAnchorsOrder();
		segments.SetLevel(level);
	}
}

string SyntenyLevel::CutoffParameters()
{
	stringstream sst;
	sst<<ManhattanCutoff<<" (manhattan)\t"<<ScoreCutoff<<" (score)\t"<<CovarCutoff<<" (co-linearity)";

	return sst.str();
}


/*------------------------------------------------------
 *             The Syntenies methods
 */

Syntenies::Syntenies( const string& cutoffs, const string& glint_output, const string& blastpgenome_output, const string& anchors_output, const string& baseoutputfile, bool rbh,Uint4 maxoverlap):
	BaseOutputFilename( baseoutputfile ),
	GlintOutputFilename( glint_output ),
	BlastpOutputFilename( blastpgenome_output ),
	AnchorsOutputFilename( anchors_output ),
	RBH(rbh),
	MaxOverlap(maxoverlap)
{

	// The cutoff values are given for each level as 3 values
	// using the following format CMD-CS-CCV :
	// CMD : cutoff value for the manhattan Distance separating anchors (integer, a strict maximum value)
	//        (will be used for the construction of connected components)
	// CS  : cutoff score for a cluster to be part of the cluster partition (integer, a strict minimum value)
	// CCV : cutoff score for the covariation of anchors within a cluster (float, a strict minimum value)

	Uint2 level = 0;
	//First synteny level is trivial, the cutoff values are never used
	push_back( new SyntenyLevel( FACTICE_CUTOFFS, INITIAL_LEVEL, level++ ) );

	std::istringstream iss( cutoffs );
	std::string token;
	while ( getline(iss, token, ':') )
	{
		push_back( new SyntenyLevel( token, INTERMEDIATE_LEVEL, level++ ) );
	}
	//Last synteny level is trivial, the cutoff values are never used
	push_back( new SyntenyLevel( FACTICE_CUTOFFS, LAST_LEVEL, level++ ) );

	cerr<<"Will try to build "<<size()<<" levels from 0 to "<<size()-1<<" with cutoff parameters :"<<endl;
	cerr<<"level 0: raw data "<<endl;
	cerr<<"level 1: strict consecutive chaining "<<endl;
	for ( SyntenyLevelIterator i = begin()+1 ; i != end()-1 ; ++i)
	{
		cerr<<"level "<<(*i)->GetLevel()+1<<": "<<(*i)->CutoffParameters()<<endl;
	}

}

void Syntenies::LoadAlignments( void )
{
	SyntenyLevel* level0 = front();

	cerr<<"Loading data"<<endl;

	if ( AnchorsOutputFilename != "")
		level0->segments.LoadDataFromFile(AnchorsOutputFilename,MARKER);
	if ( GlintOutputFilename != "")
		level0->segments.LoadDataFromFile(GlintOutputFilename,DNA);
	if ( BlastpOutputFilename != "")
		level0->segments.LoadDataFromFile(BlastpOutputFilename,PROT);

}

void Syntenies::ConstructSyntenyLevels( void )
{
	bool Incident = false;
	SyntenyLevel* PreviousSyntenyLevel = NULL;
	SyntenyLevel* CurrentSyntenyLevel;

	for ( SyntenyLevelIterator i = begin() ; i != end() ; ++i )
	{
		CurrentSyntenyLevel = *i;
		CurrentSyntenyLevel->GetSegmentsFromLowerLevel( PreviousSyntenyLevel );

		if ( CurrentSyntenyLevel->GetSize() == 0 )
		{
			fprintf(stderr,"warning. No more segments at level %d.\n",CurrentSyntenyLevel->GetLevel());
			fprintf(stderr,"warning. You should probably consider modifying the cutoffs parameters.\n");
			Incident = true;
			break;
		}
		cerr<<"Constructing  level "<<CurrentSyntenyLevel->GetLevel()<<" chains"<<endl;
		CurrentSyntenyLevel->ChainingSegments();
		PreviousSyntenyLevel = CurrentSyntenyLevel;
	}

	if ( RBH && Incident == false )
	{
/*TODO This will have to be done more neately */
		cerr<<"Detecting reciprocal best hits "<<endl;
		SyntenyLevel* LastLevel = back();
		LastLevel->Rbh( MaxOverlap );
		//LastLevel->segments.Print("rbh.out");

		//Now going back down the levels and propagating the selection
		for ( vector<SyntenyLevel*>::reverse_iterator i = rbegin()+1 ; i != rend()-1 ; ++i)
		{
			cerr<<"Propagating selection at level "<<(*i)->GetLevel()<<endl;
			(*i)->segments.PropagateSelection();
		}
		for ( SyntenyLevelIterator i = begin() ; i != end()-1 ; ++i)
		{
			(*i)->segments.UpdateClusterProperties();
		}
	}

   // Adjusting cluster span and orientation
}

void Syntenies::Print( void )
{
	SyntenyLevel* FirstLevel = front();
	FirstLevel->segments.Print( BaseOutputFilename +  "0" );

	for ( SyntenyLevelIterator i = begin() ; i != end()-1 ; ++i )
	{
		if ( (*i)->GetSize() == 0 )
			break;
		stringstream sst;
		sst<<(*i)->GetLevel()+1; //We are printing the partitions hence in fact level+1
		//cerr<<"Printing level "<<(*i)->GetLevel()+1<<endl;
		(*i)->segments.PrintPartition( BaseOutputFilename +  sst.str() );
	}
}

/* IO operators  */

ostream& operator<<(ostream& os,const Overlap& o)
{
	os << o.Start() << "\t" << o.End() << "\t" << o.Depth() ;

	return os;
}

ostream& operator<<(ostream& os,const Anchor& a)
{
	os << a.Q_Name() << "\t" << a.Q_Start() << "\t" << a.Q_End() << "\t";
	os << a.S_Name() << "\t" << a.S_Start() << "\t" << a.S_End() << "\t";
	os << a.Strand() << "\t" << a.Score() << "\t" << a.Size() << "\t";
	os << a.Molecule()<< "\t"<< a.ClusterId();

	return os;
}

ostream& operator<<(ostream& os,const AnchorNode& a)
{
	os << a.Q_Order() << "\t" << a.Q_Order() << "\t" << a.Q_Name() << "\t" << a.Q_Start() << "\t" << a.Q_End() << "\t";
	os << a.Strand() << "\t" << a.S_Name() << "\t" << a.S_Start() << "\t" << a.S_End() << "\t";
	os << a.S_Order() << "\t" << a.Score() << "\t"  << a.Size() << "\t" << a.MoleculeType();

	return os;
}

ostream& operator<<(ostream& os,const Cluster& a)
{
	os << a.Q_Order() << "\t" << a.Q_Order() << "\t" << a.Q_Name() << "\t" << a.Q_Start() << "\t" << a.Q_End() << "\t";
	os << a.Strand() << "\t" << a.S_Name() << "\t" << a.S_Start() << "\t" << a.S_End() << "\t";
	os << a.S_Order() << "\t" << a.Score() << "\t"  << a.Size() << "\t"<<a.Q_Chain()<<"\t";
	os << a.S_Chain() << "\t" << a.MoleculeType()<<"\t"<<a.CoVar();

	return os;
}

ostream & operator<<(ostream& os,  AnchorVect& av)
{
	os<<ANCHOR_HEADER<<endl;
	for ( AnchorNodeIterator i = av.begin() ; i != av.end() ; ++i )
	{
		cout<<**i<<endl;
	}

	return os;
}

#ifdef BLABLA
// Obsolete methods, TODO => remove
ostream & operator<<(ostream& os,  ChainingLevel& cl)
{
	os<<"Cutoffs: "<<endl;
	os<<"Manhattan: "<<cl.ManhattanCutoff<<endl;
	os<<"Score: "<<cl.ScoreCutoff<<endl;
	os<<"Covar: "<<cl.CovarCutoff<<endl;

	return os;
}

#endif

ostream & operator<<(ostream& os,  SyntenyLevel& cl)
{
	os<<"Cutoffs: "<<endl;
	os<<"Manhattan: "<<cl.ManhattanCutoff<<endl;
	os<<"Score: "<<cl.ScoreCutoff<<endl;
	os<<"Covar: "<<cl.CovarCutoff<<endl;

	return os;
}


ostream & operator<<(ostream& os,  Syntenies& s)
{
	os<<"Glint filename: "<<s.GlintOutputFilename<<endl;
	os<<"Blastp filename: "<<s.BlastpOutputFilename<<endl;
	os<<"Base filename: "<<s.BaseOutputFilename<<endl;

	int k=0;
	for ( SyntenyLevelIterator i = s.begin() ; i != s.end() ; ++i, k++ )
	{
		os<<"level "<<k<<endl;
		os<< **i ;
	}

	return os;
}

