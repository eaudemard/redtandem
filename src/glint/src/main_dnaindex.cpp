/*
   $Id: main_dnaindex.cpp 259 2009-08-05 08:59:30Z tfaraut $
*/

/*===========================================================================
                          dnaindex  -  description
                             -------------------
    begin				: septembre 2005
	 objet				: The core program for constructing an index

=============================================================================*/

using namespace std;
#include <iostream>
#include <fstream>

#include "sequence_io.h"
#include "alignment.h"
#include "utils.h"
#include "ptypes.h"
#include "memory.h"

void Usage();
RealLogger* logger;

/*////////////// Main ///////////////*/
int main(int argc,char* argv[])
{
    cout <<"glintindex rel $Rev: 259 $ - Compiled on " << __DATE__ << " at " << __TIME__;
#ifdef __INTEL_COMPILER
    cout << " using the intel compiler " << __INTEL_COMPILER_BUILD_DATE << '\n';
#elif __GNUG__
    cout << " using the gcc compiler " << __VERSION__ << '\n';
#endif
#ifdef WORD_IN_TWO_PARTS
    cout << "WORD OBJECT IN TWO PARTS ==> SLOWER\n";
#endif

    cout << "Command : ";
    for (int p=0; p<argc;p++)
        cout << argv[p] << ' ';
    cout << '\n';

    BlackList black_list(BLACK_LIST_SIZE);

    DataBase_Parameters database(argc,argv,Usage);
    string filename = database.GetFileName();
	cerr << "Reading the sequence file " << filename << '\n';
    cout << "Mask = " <<  database.GetMsk() << '\n';

    /* Initializing logger: we use an auto_ptr to be sure that the destructor is called at end of program */
    auto_ptr<RealLogger> logger_ptr = auto_ptr<RealLogger> (new RealLogger(filename + ".log",database.GetLogLevel(),LOGGER_OPT_AUTOFLUSH));
    logger = logger_ptr.get();

    /* Initializing Tree, with the singleton semantics */
    Tree * t_ptr = Tree::Instance();
    Tree & index_tree = *t_ptr;
    index_tree.Initialize(&database);

    SBuffer bfr(index_tree,black_list,database); // Creating a new SBuffer object
    bfr.ReadSeq();          // Read and index the sequence

    /* Writing the database object to the .he file */
    string file_name = (string) filename + (string) HE_EXTENSION;
    ofstream out(file_name.c_str());
    out << database;
    out.close();

    /* Writing the deleted words inside the black list */
    {
        string black_file_name = (string) filename + BL_EXTENSION;
        ofstream black_file(black_file_name.c_str());
        black_file << black_list;
	}

    //cout << index_tree << '\n';
	return 0;

}

/*Utilisation du programme */
void Usage()
{

	fprintf(stderr,"dnaindex: Construct the index of a nucleotide sequence (this index is used by the compare program)\n");
	fprintf(stderr,"Usage: dnaindex [options] sequence_file \n\n");
	fprintf(stderr,"\tsequence_file : file of the nucleotide sequence to index (a chromosome for example) in multi-fasta format.\n");
	fprintf(stderr,"\tOptions:\n");
	fprintf(stderr,"\t\t -N Name\tName of the bank (defaults to file name)\n");
	fprintf(stderr,"\t\t -M M\t\tM is the mask structure (default  %s, max 32 characters), may contain 1,..,%d characters.\n",DEFAULT_MASK,MAX_LEAF_LEVEL);
	fprintf(stderr,"\t\t -p P\t\tnumber of passes to create the index (default 1, the more passes the less memory is used). \n");
    fprintf(stderr,"\t\t\t\tThis is useful for large sequence files (> 50 Mb)\n");
	fprintf(stderr,"\t\t -n count\tMax_Count The maximum number of allowed occurences for a word otherwise deleted (default %d, max 256). \n",DEFAULT_MAX_COUNT);
	fprintf(stderr,"\t\t -C 0/1/2\tmask low complexity sequence using dinucleotides count (1) or dust (2), or do not mask (0) (default %d)\n",DEFAULT_COMPLEXITY);
    #ifdef DEBUGLOG
    fprintf(stderr,"\t\t -D T/TV\tlog some messages useful for debugging the program\n");
    #endif
}



