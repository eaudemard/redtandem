/*
   $Id: masque.cpp 230 2008-09-12 13:16:00Z tfaraut $
*/


/* masque.c */

/*===========================================================================
                          masque  -  description
                             -------------------
    begin				: septembre 2005
	 objet				: Everything for the definition and initilization of the Mask : see description ine the main

=============================================================================*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
using namespace std;
#include <iostream>
#include "ptypes.h"
#include "masque.h"
#include "memory.h"
#include "word.h"

/* The private initialization function, called by the constructors and operator= */

void Mask::__Init (const string& str_mask)
{
	int j,k;
	size_t i;
	Uint4 b;
	int a[2];
	a[0]=1;
	a[1]=0;

	//cerr<<"Here init stringmask "<<str_mask<<"\n";

	if (str_mask=="")
    {
        length = 0;
        string_mask = "";
    }
    else
    {
        string_mask=str_mask;
        length=string_mask.length();
    };

    if (length == 0)
    {
        pos = NULL;
        leaf_level = 0;
        level_pos = NULL;
        return;
    };

    /* Initialize the level_length array */
	for (i=0;i<MAX_LEAF_LEVEL;i++) {
		level_length[i]=0;
	}

    /* Create and init the pos and level_length arrays, also leaf_level */
    pos=(short*)memory_survey(length*sizeof(short),(char*)"int*");
    //pos=(short*) malloc(length*sizeof(short));
	int min=length;
	int max=0;
	for (i=0;i<length;i++) {
		pos[i]=str_mask[i]-'1' + 1;            /* converts char 1 (2,3,...) to the corresponding integer value*/
		min=(pos[i]<min && pos[i]!=0) ? pos[i] : min;   /* Here allowing for 0 in the mask   */
		max=(pos[i]>max) ? pos[i] : max;
		level_length[pos[i]]++;
	};
    leaf_level=max;

    /* Check for consistency */
	if (min != ROOT_LEVEL || max > MAX_LEAF_LEVEL) {
		fprintf(stderr,"Levels of mask %s out of range ([1-%d]). Please see help (-h)\n",str_mask.c_str(),MAX_LEAF_LEVEL);
		exit(1);
	}

    /* We refuse a mask with level_length higher than 4, except for the root: if level_length is more than 4, the corresponding
       code has more more thant 8 bytes, and there will be some overflow in the index binary file */

    for (i=ROOT_LEVEL+1; i<=leaf_level; i++)
    {
        if (level_length[i] > 4) {
            cerr << "ERROR - The level " << i << " has a length higher than 4 (" << level_length[i] << ")\n";
            exit(1);
        }
    }

    bm=(Uint8*)memory_survey((max+1)*sizeof(Uint8),(char*)"unsigned long*");

	for (j=min;j<=max;j++)
	{
		bm[j]=0;
		for (i=0;i<length;i++)
		{
			b=(pos[i]<=j) ? 1 : 0;
			bm[j]<<=1;
			bm[j]|=b;
		}
	}
	//fprintf(stderr,"le masque est de taille %d : min=%d  max=%d  %d  \n",length,min,max,leaf_level);


	level_pos=(short**)memory_survey((max+1)*sizeof(short*),(char*)"short**");

	/*fprintf(stderr,"Le masque est %s \n",pos);
	*/

	for (j=min;j<=max;j++) {
		level_pos[j]=(short*)memory_survey(level_length[j]*sizeof(short),(char*)"short*");
		k=0;
		for (i=0;i<length;i++) {
			if (pos[i]==j) level_pos[pos[i]][k++]=i;
		}
	}

}

/*
  Name:      Mask::Reconstruct
  Usage:     string s = m.Reconstruct(s,h_code,level)
  Function:  Partially reconstruct the word
  Arg:  s      ==> The word to be reconstructed (input/output)
        h_code ==> The hash code used for reconstruction
        level  ==> The mask level of the reconstruction
  Return: s
*/

string Mask::Reconstruct (const string & w,Uint4 l,int level) const
{
    string mot=w;
	if (level==ROOT_LEVEL) {
        mot = "";
		for (int k=0;k<length;k++)
            mot += '.';
	}

	for (int k=0;k<level_length[level];k++) {
		unsigned long c=l & 3;
		l>>=2;
		mot[level_pos[level][level_length[level]-k-1]]=Word::decode(c);
	}
	//if (level==m->leaf_level) mot->m[m->taille]='\0';
	return mot;
}

ostream & operator<<(ostream& os, const Mask& m)
{
	os << m.string_mask;
/*
    for (int i=0;i<m.length;i++)
        os << m.pos[i];
    os << "\n\n";
    */
    return os;
}

