/*
   $Id: match.cpp 261 2009-08-14 13:18:25Z tfaraut $
*/


/*match.c*/

/*===========================================================================
                         match  -  description
                             -------------------
    begin				: 24 jui 2002
	 objet				: Functions to handle the matching seeds before turning to dynamic programming

=============================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iomanip>
#include <time.h>
#include <limits>

#include "ptypes.h"
#include "index_io.h"
#include "alignment.h"
#include "match.h"
#include "utils.h"
#include "sequence_io.h"
#include "bitcount.h"
#include "iqsort.h"

/*DiagInt Last_Diag;   Use to be global variables now in the MatchPile object
Uint4 Subject_End;*/

MatchConstants* MatchScoring;

extern RealLogger* logger;

/* A very basic ostream operator for match */
ostream & operator<<(ostream& os, const match& m)
{
	os<<m.diag<<"\t"<<m.qpos<<"\t"<<m.spos<<"\t"<<m.score<<"\t"<<m.hit;
	return os;
}

/*
    The MatchResults object
*/

/* The constructor */
MatchResults::MatchResults(Uint4 size, const QBuffer& q,FILE* f): strand(0),display(_init_display(q,f)) {
        match_pile_strand[0]=new MatchPile(size,1,q,display);
        match_pile_strand[1]=new MatchPile(size,-1,q,display);
}

/*
   Initializing the Display object: we create an object derived of Display
*/

Display & MatchResults::_init_display(const QBuffer& q, FILE* f)
{
    Display * dsp_ptr;
    if (q.compare.output_format == OUTPUT_FORMAT_M1)
        dsp_ptr = new DisplayM1(q,f);
    else if (q.compare.output_format == OUTPUT_FORMAT_M2)
        dsp_ptr = new DisplayM2(q,f);
    else if (q.compare.output_format == OUTPUT_FORMAT_M3)
        dsp_ptr = new DisplayM3(q,f);
    else if (q.compare.output_format == OUTPUT_FORMAT_M4)
        dsp_ptr = new DisplayM4(q,f);
    else if (q.compare.output_format == OUTPUT_FORMAT_M8)
        dsp_ptr = new DisplayM8(q,f);
    else if (q.compare.output_format == OUTPUT_FORMAT_M9)
        dsp_ptr = new DisplayM9(q,f);
    else if (q.compare.output_format == OUTPUT_FORMAT_M13)
        dsp_ptr = new DisplayM13(q,f);
/*    else if (q.compare.output_format == OUTPUT_FORMAT_BLASTM9)
           dsp_ptr = new DisplayBlastTabularM9(q,f);*/
    else
    {
        /* TODO - TRAITER L'ERREUR AILLEURS QUE LA - MANU */
        cerr << "ERROR - " << q.compare.output_format << " is not a correct output format\n";
        exit(1);
    };

    dsp_ptr->DisplayTopHeader();

    return *dsp_ptr;
}

/*
    The matches stored in the match_pile objects, it is time to compute the hsp
    We just call ComputeSimpleMatches from the MatchPile objects

*/
void MatchResults::ComputeHsp(void)
{

	if (match_pile_strand[0]->IsNotEmpty())
	{
		match_pile_strand[0]->ComputeSimpleMatches();
		match_pile_strand[0]->Reset();
	}
	if (match_pile_strand[1]->IsNotEmpty())
	{
		match_pile_strand[1]->ComputeSimpleMatches();
		match_pile_strand[1]->Reset();
	}
}

/*
    Print hsps of ALL MatchPiles, sorting them first
*/

void MatchResults::PrintAllHsp(const string& query_name, const string& database_name)
{
	vector<hsp*> hspl;

    for (int i=0; i<2; i++) {
        this->SetStrand(i);
        MatchPile& ml =  this->GetCurrentMatchPile();
        for (HspIterator k = ml.begin(); k !=  ml.end(); ++k) {
            hspl.push_back(*k);
        }
    }
    display.DisplayHits(hspl);
}

/*
   Function:	Prints the header to the predefined output stream once for all

   Arg: none
   Return Value: none
*/

void MatchResults::DisplayResultsHeader()
{
	display.DisplayTopHeader();
}

/*
    The MatchPile object
*/

/* The constructor, some iterators, Reset etc */

HspIterator MatchPile::begin()  {return HP.begin();}
HspIterator MatchPile::end()    {return HP.end();}
void MatchPile::HspReset() {HP.Reset();}

/*
   Function  :	Constructor of the MatchPile
				The MatchResults has two match piles, one for each strand
   Args      : 
				sz : size of the pile
				st : strand under inspection
				q : a reference on teh QBuffer object
				d : a reference on the Dsiplay object
*/
MatchPile::MatchPile(int sz,int st, const QBuffer & q, const Display& d):size(sz),strand(st),top(0),HP(HSP_PILE_SIZE,d),align_results(MAX_SEQ_SIZE,q),display(d)
{
    l = new match*[size];
    p = new match[size];
    for (Uint4 i=0; i < size;i++)
    {
        l[i] = &(p[i]);
    }
}

void MatchPile::Reset()
{
    top=0;
}

/*
Function	:	Adds an element to the pile, compute the matches is the pie is full
Args		: 
				spos : the position of the seed in the query
				qpos : the position of the seed in the subject
				hit  : the hit value of the seed (TODO to be explained elswhere)
Retruns		: 	the current size of the pile	
*/
int MatchPile::Push(Uint4 spos,Uint4 qpos, Uint1 hit)
{
    if (top==size)
    {
    	ComputeSimpleMatches();
        Reset();
    }
    else
    {
        match *m = l[top++];
        m->spos=spos;
        m->hit=hit;
        m->qpos = qpos;
        m->score=MatchScoring->score_matrix->s[hit];
        m->diag=(DiagInt)m->spos-strand*(DiagInt)m->qpos;
    }
    return top;
}


/*
Method		: Compute the hsps: (i) sort the matches, according to diagonals first and then subject
							    (ii) slide across the diagonals to identify regions with a concentration of seeds
*/
void MatchPile::ComputeSimpleMatches()
{
	seed_qsort();
	ScoreSlidingDiagonal();
}

/*
Method		: Sliding accross diagonals
				(i) the score of overlapping seeds are calculated using bitwise operations 
					precomputed16_bitcount gives the number of 1s in the bscore bit array
				(ii) when the overlapping seeds extend the initial window size, bitwise operation enable to keep track of previous seed scores
				(iii) when they are not overlapping but close, we performe a temptative alignment and if unsuccessful we keep track of the score of the previous seeds
				(iv)  when they are not overlapping and far away, we performe a temptative alignment and the score is reset to zero
				(v)   when a new diagonal is visited, we performe a temptative alignment and the score is reset to zero
*/
void MatchPile::ScoreSlidingDiagonal()
{
	match **m=l;
	Uint4 n=top;
	
	Uint1 Window_Size=display.query.database.GetMsk().GetLength();
	const Mask &M=display.query.database.GetMsk();	

	// Initialization of the global variables LastDiag, SubjectStart and SubjectEnd
	LastDiag = numeric_limits<DiagInt>::min();
	SubjectEnd   = 0;
    SubjectStart = numeric_limits<Uint4>::max();

	Uint4 start		= 0;
	Uint4 ChunkStart	= start;

	Uint8 bscore	= M.bm[m[start]->hit] << Window_Size;
	DiagInt	CurrentDiag= m[start]->diag;
	Uint4	score=0;

	int offset	= 0;
	Uint4 j		= 0;
	bool found	= 0;	
	
	while ( start < n-1 ) 
	{
		j=start+1;
		found=0;
		
		// Adding all overlapping seeds to bscore (i)
		while ( j < n && m[j]->diag == CurrentDiag && (( offset=m[j]->spos - m[start]->spos ) < Window_Size)) 
		{
				bscore |= ( M.bm[m[j]->hit]<<(Window_Size-offset) );
				j++;
		};
		// Transforming the bscore into a score
		score += precomputed16_bitcount( bscore );
		
		if ( j == n )
		{		
			if ( score >= display.query.compare.Cutoff_Slide )
			{ 
				found=LocateMatchesChunk( m, ChunkStart , j-1 , score);
			}
			break;
		}
		
		if (  m[j]->diag != CurrentDiag )// (v)
		{
			if ( score >= display.query.compare.Cutoff_Slide ) 
			{	
				found=LocateMatchesChunk( m, ChunkStart, j-1, score);
			}
			
			if ( found ) {
				while ( j < n  &&  m[j]->diag <= LastDiag  && m[j]->spos >= SubjectStart && m[j]->spos <= SubjectEnd ) 
				{   /* Jumping to the next chunk  */
					j++;
				}
				if (j==n) break; // There is no further chunk !!
			}
			start = j;
			ChunkStart = start;
			score = 0;
			bscore = M.bm[m[start]->hit] << Window_Size;
			CurrentDiag = m[start]->diag;
		}
		else if ( m[j]->spos - m[j-1]->spos >= Window_Size )
		{
			if ( score >= display.query.compare.Cutoff_Slide ) 
			{
				found=LocateMatchesChunk( m, ChunkStart, j-1, score);
			}

			if ( found ) 
			{
				while ( j < n  &&  m[j]->diag <= LastDiag  && m[j]->spos >= SubjectStart &&  m[j]->spos <= SubjectEnd ) 
				{     /* Jumping to the next chunk  */		
					j++;
				}
				if (j==n) break;    // There is no further chunk !!

				start = j;
				ChunkStart = start;
				score=0;			
			} else {
				if ( m[j]->spos - m[j-1]->spos > 100 ) //(iv) 
				{
					start = j;
					ChunkStart = start;
					score=0;
				} else //(iii) 
				{
					ChunkStart = start;
					start = j;
				}
			}

			bscore = M.bm[m[start]->hit] << Window_Size;
		}
		else  //(ii)
		{	
			// We keep track of the score of the previous window except the one overlapping the current window 
			score += precomputed16_bitcount( bscore >> Window_Size );
			ChunkStart = start;
			start = j-1;
			bscore <<= Window_Size;
		
		}
	}

}

/*
Function	: Given a set of seeds in a defined region, tries to comute a local alignment
	On a segment of a diagonal, tries to identify an aligment in several steps
     i) Identify the match (seed) with the best score 
    ii) Starting from this word (seed) a check is made if it doesn't correspond to a previous alignment (HP.Lookup)
   iii) If ok extend on the right (look_right) and on the left (look_left)
    iv) If the resulting ungapped alignment has a score > display.query.compare.Cutoff_Dyn, a dynamic programming alignment is tried on the right and on the left
     v) If the resulting possibly gapped alignment has a score > display.query.compare.Cutoff_Hsp one checks if it is not a previous recorded alignment (HP.Lookup again)
    vi) If ok record it or print it depending on OutputStream
*/

bool MatchPile::LocateMatchesChunk( match **m, int min, int max, Int2 score)
{
	int max_score=0;
	int ind_maxscore=0;

	bool found=0;
	hsp *h;

	int query_move=1;
	DiagInt max_diago=MIN_DIAG;

	char sign;
	unsigned char bstrand;

	if ( strand > 0 ) 
	{
		sign='+';
		bstrand=0;
	} else {
		sign='-';
		bstrand=1;
	}

	/* step i) Selecting the seed with the best score */
	for (int i=min;i<=max;i++) 
	{
		if ( m[i]->hit > max_score ) 
		{
			max_score=m[i]->hit;
			ind_maxscore=i;
		}
	}

#ifdef DEBUGLOG
	_DebugPrintSelMatches( m, min, max , score);
#endif

	/* step ii)   Checking if the seed m is not already part of an existing hsp*/
	if (HP.Lookup(*m[ind_maxscore]))    // Return 0 if we detect an overlap with another hsp
	{	
		return false;
	}
	
	/* step iii) Performing an ungapped alignment */
	align_results.size=0;
	query_move=(strand >0)	? 1 : -1;


#ifdef DEBUGLOG
	_DebugPrintTopMatch( m, ind_maxscore);
#endif

	align_results.score_align=align_results.LookRight(m,m[ind_maxscore]->qpos,m[ind_maxscore]->spos,strand);
	align_results.score_align+=align_results.LookLeft(m,m[ind_maxscore]->qpos-query_move,m[ind_maxscore]->spos-1,strand);

	h=HP.GetTop();
    h->strand=strand;

#ifdef DEBUGLOG
	printf("score : %d\t%d\t%d\t%d\t%d\n", align_results.GetScore(),align_results.bestSi_left,align_results.bestSi_right,align_results.bestSj_left,align_results.bestSj_right);
#endif		

	/* step iv) If Dyn is true and the score is good enough perform a gapped alignment (dynamic programming)  */
	if ( display.query.compare.Dyn && align_results.GetScore()	>= display.query.compare.Cutoff_Dyn ) 
	{
		
#ifdef DEBUGLOG
	_DebugPrintAlign( &align_results,(char*)"before");
#endif		
		align_results.strand=strand;
		align_results.query_gap=0;
		align_results.subject_gap=0;

		/* Alignment on the left side    */
		align_results.SetAlignLeft(strand);
		align_results.LocalAlign(h);

#ifdef DEBUGLOG
	_DebugPrintAlign( &align_results,(char*)"left");
#endif	

		align_results.size+=align_results.bestSi_right-align_results.bestSi_left+1;

		/* Alignment on the right side    */
		align_results.SetAlignRight(strand);
		align_results.LocalAlign(h);

#ifdef DEBUGLOG
	_DebugPrintAlign( &align_results,(char*)"right");
#endif

	}

	/* step v) If the score is good enough with store (commit) this alignment in an hsp  */
	if ( align_results.GetScore() >=  display.query.compare.Cutoff_Hsp )
	{
		max_diago = align_results.ToHsp(*h);

		if (! align_results.ValidHsp(*h) )
			return false;

		if (HP.Lookup(*h))  // Checking if the hsp is not already part of an existing hsp
			return false;

		if (display.query.compare.OutputStream)
		{
			display.DisplayHsp(h);
		}

		LastDiag     = ( max_diago > LastDiag ) ? 	max_diago	:	LastDiag;
		SubjectStart = align_results.GetSubjectStart();
		SubjectEnd   = align_results.GetSubjectEnd();

		found=1;
		HP.Commit();

#ifdef DEBUGLOG
	_DebugPrintSelHsp(h);
#endif	
		//HP.PrintSortedPile();
	}

	return found;
}

/*
Method		: Sorting seeds using an in-line qsort implementation ( iqsort.h )
*/
void MatchPile::seed_qsort()
{
#define elt_sort(a,b) ((*a)->diag)==((*b)->diag) ? (((*a)->spos)<((*b)->spos)) : (((*a)->diag)<((*b)->diag))
 QSORT(struct match*,l,top,elt_sort);
}

void MatchPile::seed_display()
{
#define voir(a,b) printf(" L[%d]=%u \n",b,a->diag)
 Uint4 i;
 for (i=0;i<top;i++) {
 	voir((int)l[i],i);
 };

}

/***------------------------------------------------------------------------------------------------------***/
/*                                                                                                          */
/* 									    Methods for sorting matches                                         */
/*                                                                                                          */
/****-----------------------------------------------------------------------------------------------------***/

int compare_match(const void *premier,const  void *second)
{
	match **pp;
	match **qq;
	match *p;
	match *q;

	pp=(match **)premier;
	qq=(match **)second;

	p=*pp;
	q=*qq;

	if (p->spos<q->spos)
		return -1;
	else if (p->spos>q->spos)
		return 1;
	else
		return 0;

}

int compare_match_qpos(const void *premier,const  void *second)
{
	match **pp;
	match **qq;
	match *p;
	match *q;

	pp=(match **)premier;
	qq=(match **)second;

	p=*pp;
	q=*qq;

	if (p->qpos<q->qpos)
		return -1;
	else if (p->qpos>q->qpos)
		return 1;
	else
		return 0;
}

int compare_match_diag(const void *premier,const  void *second)
{
	match **pp;
	match **qq;
	match *p;
	match *q;

	pp=(match **)premier;
	qq=(match **)second;

	p=*pp;
	q=*qq;

	if (p->diag<q->diag)
		return -1;
	else if (p->diag>q->diag)
		return 1;
	else
		return 0;
}

int compare_match_diag_spos(const void *premier,const  void *second)
{
	match **pp;
	match **qq;
	match *p;
	match *q;

	pp=(match **)premier;
	qq=(match **)second;

	p=*pp;
	q=*qq;

	if (p->diag<q->diag)
		return -1;
	else if (p->diag>q->diag)
		return 1;
	else if (p->spos<q->spos)
		return -1;
	else if (p->spos>q->spos)
		return 1;
	else
		return 0;
}

#ifdef DEBUGLOG
void _DebugPrintSelMatches(match **m, int min, int max,Int2 score)
{
	printf("------\n");
	for ( int i = min ; i <= max ; i++ )
	{
		printf("lookup\t%d\t%d\t%d\t%d\n",m[i]->hit,m[i]->diag,m[i]->qpos,m[i]->spos);	
	}
}

void _DebugPrintTopMatch(match **m, int ind_max)
{

	printf("lookup_max\t%d\t%d\t%d\t%d\n",m[ind_max]->hit,m[ind_max]->diag,m[ind_max]->qpos,m[ind_max]->spos);	
}
void _DebugPrintAlign(AlignEngine* align,char* type)
{
	printf("dyn_%s\t%d - %d  -->  %d - %d\n",type,align->GetQueryStart(),align->GetQueryEnd(),align->GetSubjectStart(),align->GetSubjectEnd());
}
void _DebugPrintSelHsp(hsp *h)
{	
	printf("hsp\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n",h->max_diag,h->q_start,h->q_end,h->s_start,h->s_end,h->strand,h->score);
}
#endif

#ifdef BLABLA


#ifdef DEBUGLOG
	logger->SetLogType("L") << "Locating match chunks  with q_pos "<<m[ind_maxscore]->qpos<<" and score = "<<align_results.GetScore()<<"\n";
#endif


#ifdef DEBUGLOG
	logger->SetLogType("L") << "Align results with score = "<< align_results.GetScore() <<"    ";
	logger->SetLogType("L") << "["<<align_results.query_start <<"-"<<align_results.query_end<<"]->";
	logger->SetLogType("L") << "["<<align_results.subject_start <<"-"<<align_results.subject_end<<"]\n";
#endif

#endif


