/*
   $Id: test_logger.cpp 1190 2008-01-12 11:19:26Z tfaraut $
*/



#include "logger.h"
#include <iostream>
using namespace std;

void WaitALittle(void)
{
    for (int j=0; j<1000000000;j++)
        for (int k=0; j<10000;j++);
};

/*////////////// Main //////////////*/
int main(int argc,char* argv[])
{

    /* Should not compile */

    //Logger l((string)"test.log",1);

    /* Testing ILogger */

    try
    {
        ILogger il("/test.log",1);
    }
    catch(exception&e)
    {
        cerr << "Exception received " << e.what() << "\n";
    };

    /* Logging log level 1 */
    {
        ILogger il1((string)"test.log",1,LOGGER_OPT_AUTOFLUSH);

        try
        {
            il1.StartTimer("");
        }
        catch(exception&e)
        {
            cerr << "Exception received " << e.what() << "\n";
        };

        sleep(2);
        il1.StartTimer("T1");
        il1.Trace("Logging level 1\n",1);
        sleep(2);
        WaitALittle();
        for (int j=0; j<110; ++j)
        {
            il1.Trace("Logging level 1\n",1);
        };

        il1.Trace("Logging level 1\n",1,point);
        il1.Trace("Logging level ",1);          // logging in 2 times
        il1.Trace("1\n",1,no_stamp);
        il1.Trace("Logging level 1\n",1,timer,"T1");

        //il1.SetLogType(1).SetLogFormat(normal) << (string) "Logging level " << (string) "1" << (string) "with <<\n";

        int ii=5;
        il1.SetLogType(1) << (string) "Logging level ";
        il1 << (string) "1" << (string) " with << " << (string) "int ";
        il1 << ii << (string) "\n";

        /* Logging log level 2 */

        il1.Trace("Logging level 2\n",2);

        ILogger il2("test2.log",2);
        il2.Trace("Logging level 2\n",2);
    }

    /* Testing SLogger */


    /* Logging log level D */
    SLogger sl1((string)"test.log","D",LOGGER_OPT_AUTOFLUSH);

    sleep(2);
    sl1.Trace("Logging level 1\n","D");
    sleep(2);
    WaitALittle();

    sl1.Trace("Logging level DD\n","DD",point);
    sl1.Trace((string)"Logging level D\n",(string)"D");

    /* Logging log level DD */

    sl1.Trace("Logging level DD\n","DD");

    SLogger sl2("test2.log","DD");
    sl2.Trace("Logging level DD\n","DD");

};


