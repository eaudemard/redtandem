/*
   $Id: anchor.h 222 2008-05-28 06:35:44Z tfaraut $
*/

#ifndef __ANCHOR_H__
#define __ANCHOR_H__

#include <string>
#include <vector>

#include "ptypes.h"

typedef enum view {QUERY, SUBJECT} ViewType;
typedef enum mol {DNA, PROT, MARKER, COMB} MolType;

#define ANCHORS_HEADER_O "query\tq_start\tq_end\tsubject\ts_start\ts_end\tstrand\tscore\tsize\tcluster"
#define CLUSTERS_HEADER_O "query\tq_start\tq_end\tsubject\ts_start\ts_end\tstrand\tscore\tspan\tsize\tcovar\tcluster"

#define ANCHOR_HEADER "q_order\telement\tq_chrom\tq_start\tq_end\tstrand\ts_chrom\ts_start\ts_end\ts_order\tscore\tsize\tmol"
#define CLUSTER_HEADER "q_order\telement\tq_chrom\tq_start\tq_end\tstrand\ts_chrom\ts_start\ts_end\ts_order\tscore\tsize\tq_chain\ts_chain\tmol\tcovar"

#define HEADER_MARKUP '#'

#define MAX_DIST 500000    //To put somewhere else
#define MIN_SCORE 400
#define MIN_COVAR 0.8

using std::vector;

/*

    Classes AnchorNode, AnchorData, AnchorView, ..., Anchor
	Anchor* classes are designed to manage the glint output data which consists in hsps.

	AnchorData is the base class storing the raw data according to the glint output format

	An hsp is defined by the following attributes:
		q_name q_start q_end s_name s_start s_end strand score size
	For the purpose of chaingin anchors some new attributes are defined :
		q_center, s_center : respectively the mean of (q_start, q_end) and (s_start, s_end)
		q_order, s_order   : the order of the hsp along the query (resp subject) genome (all chromosomes are concatenated arbitrarily)

	AnchorView is an abstract class (a virtual class) containing almost exclusively virtual methods.
	The AnchorQueryView and AnchorSubjectView inherit from AnchorView and implement the virtual methods of AnchorView
	   according to the particular View.
	In AnchorSubjectView for example, the Q_Start method returns the s_start attribute of the hsp because if one adopts the subject
	view, the subject considers himself as the query and the other organism as the subject.
	Defining these two different views enables to work with generic methods that will use only the AnchorView methods to access the data.
	A simple change in the point of view, from Query to Subject, will enable to perform the same operation, using the same code, on both
	organism.

*/

class AnchorNode;
struct edge {
	AnchorNode* n;
	Uint4 score;
};

class AnchorData {
public:
	AnchorData();
	AnchorData(const std::string&);

	void Set_Centers();

	friend std::istream& operator>>(std::istream& , AnchorData& );

	friend class AnchorQueryView;
	friend class AnchorSubjectView;
	friend class AnchorView;

	friend class Anchor;
protected:
	std::string q_name,s_name;
	Uint4 q_start, q_end, s_start, s_end;
	Uint4 q_center, s_center;
private:
	Uint4 q_order,s_order;
};

/*
 * The Abstract class AnchorView
 *
 */

class AnchorView {
public:
	AnchorView() { data=NULL; };
	AnchorView(AnchorData* );
	virtual std::string Q_Name() const=0;
	virtual Uint4 Q_Start() const=0;
	virtual Uint4 Q_End() const=0;
	virtual std::string S_Name() const=0;
	virtual Uint4 S_Start() const=0;
	virtual Uint4 S_End() const=0;
	virtual Uint4 Q_Center() const=0;
	virtual	Uint4 S_Center() const=0;
	virtual Uint4 Q_Order() const=0;
	virtual	Uint4 S_Order() const=0;
	virtual void SetOrder(Uint4)=0;

	void Set_Centers() { data->Set_Centers(); };

	virtual ~AnchorView() {};

    friend bool operator<(const AnchorView&, const AnchorView&);

	friend class Anchor;
	friend class Cluster;

protected:
	AnchorData* data;
};


/*
 * The class AnchorQueryView :  viewing the data from the' query point of view
 *
 */


class AnchorQueryView: public AnchorView {
public:
	AnchorQueryView() : AnchorView() {};
	AnchorQueryView(AnchorData* a) : AnchorView(a) {};

	std::string Q_Name() const { return data->q_name; };
	Uint4 Q_Start() const { return data->q_start; };
	Uint4 Q_End() const { return data->q_end; };
	std::string S_Name() const { return data->s_name; };
	Uint4 S_Start() const { return data->s_start; };
	Uint4 S_End() const { return data->s_end; };

	Uint4 Q_Center() const { return data->q_center; };
	Uint4 S_Center() const { return data->s_center; };

	Uint4 Q_Order() const { return data->q_order; };
	Uint4 S_Order() const { return data->s_order; };

	void SetOrder(Uint4 k) { data->q_order=k;};

};

/*
 * The class AnchorQueryView :  viewing the data from the subject point of view
 *
 */

class AnchorSubjectView: public AnchorView {
public:
	AnchorSubjectView() : AnchorView() {};
	AnchorSubjectView(AnchorData* a) : AnchorView(a) {};

	std::string Q_Name() const { return data->s_name; };
	Uint4 Q_Start() const { return data->s_start; };
	Uint4 Q_End() const { return data->s_end; };
	std::string S_Name() const { return data->q_name; };
	Uint4 S_Start() const { return data->q_start; };
	Uint4 S_End() const { return data->q_end; };

	Uint4 Q_Center() const { return data->s_center; };
	Uint4 S_Center() const { return data->q_center; };

	Uint4 Q_Order() const { return data->s_order; };
	Uint4 S_Order() const { return data->q_order; };

	void SetOrder(Uint4 k) { data->s_order=k;};

};


/*
 * The class Anchor :  the central class for manipulating the anchors data
 *    Attributes :
 *        the important attribute:
 *          view : a pointer on an AnchorView Class (Query or Subject)
 *        some additional attributes:
 *          score, size, span and orientation that are relevant to each hsp and have a meaning irrespective to the view
 */

class Anchor {
public:
	Anchor();
	Anchor(const Anchor& a);
	/*Anchor(const std::string&);
	Anchor(const Anchor& a);*/
	//~Anchor() { delete view->data;};
	void Destroy() { delete view->data;};

	std::string Q_Name() const { return view->Q_Name(); };
	Uint4 Q_Start() const { return view->Q_Start(); };
	Uint4 Q_End() const { return view->Q_End(); };
	std::string S_Name() const { return view->S_Name(); };
	Uint4 S_Start() const { return view->S_Start(); };
	Uint4 S_End() const { return view->S_End(); };

	Uint4 Q_Center() const { return view->Q_Center(); };
	Uint4 S_Center() const { return view->S_Center(); };

	Uint4 Q_Order() const { return view->Q_Order(); };
	Uint4 S_Order() const { return view->S_Order(); };

	Uint4 Score() const { return score;};
	Uint4 Size() const { return size;};
	Uint4 Span() const { return span; };

	OrientType Orientation() const { return orientation; };
	char Strand() const { return ( PLUS == orientation ) ? '+' : '-';};
	const string MoleculeType() const ;

	Uint4 ClusterId() const { return clusterid; };
	MolType Molecule() const { return molecule; };

	void SetOrder(Uint4 k) { view->SetOrder(k);};

	virtual void SetView(ViewType v);
	ViewType GetView() const { return _view_type; };

	void SetField(const std::string& name,const string& value);

	void Set_Q_Name(const string& s ) { view->data->q_name=s; };
	void Set_Q_Start(Uint4 q_start ) { view->data->q_start=q_start; };
	void Set_Q_End(Uint4 q_end ) { view->data->q_end=q_end; };
	void Set_S_Name(const string& s) {view->data->s_name=s; };
	void Set_S_Start(Uint4 s_start) {view->data->s_start=s_start; };
	void Set_S_End(Uint4 s_end) { view->data->s_end=s_end; };

	void Set_Centers() { view->Set_Centers(); }

	void Set_Score(Uint4 sc) { score=sc;};
	void Set_Size(Uint4 sz) { size=sz;};
	void Set_Span(Uint4 sp) { span=sp; };
	void Set_Orientation(OrientType orient) { orientation=orient; };
	void Set_ClusterId(Uint4 c) { clusterid=c; };
	void Set_Molecule(MolType m) { molecule=m; };

	void Check_Orientation();

	Anchor& operator=(const Anchor& a);
	friend bool operator<(const Anchor&, const Anchor&);

	AnchorView* view;

	friend std::ostream& operator<<(std::ostream&,const Anchor&);
    friend class AnchorVect;
    friend class Cluster;

protected:
	Uint4 clusterid;

private:
	AnchorQueryView query;
	AnchorSubjectView subject;

	OrientType orientation;
	Uint4 score;
	Uint4 size;
	Uint4 span;

	/*Anchor* father;
	vector<edge> neighbours;*/
	ViewType _view_type;
	MolType molecule;
};

inline AnchorView::AnchorView(AnchorData* a) : data(a) {}

/*
 * The class AnchorNode :  the class for manipulating the anchors within the graph representation
 *          Attributes
 *          father, neighbours that will be used for the clustering steps
 *          selected : 0|1 wether the node is selected for further processing
 */

class AnchorVect;
class AnchorNode : public Anchor
	{
public:
	AnchorNode(): Anchor(), selected(1) {};
	void AddNeighbour(const edge& e);
	bool Selected() { return selected;};
	void Select() {  selected = true; };
	void Deselect() { selected = false; };
	void ReverseSegment();
	friend class AnchorVect;

private:

	bool selected;
	AnchorNode* father;
	vector<edge> neighbours;

	};

//A simple type definition for managing AnchorNode pointers
typedef AnchorNode* AnchorNodePtr;

//A type definition to simplify the code for iterators
typedef vector<AnchorNode*>::iterator AnchorNodeIterator;

ostream& operator<<(ostream& os, const AnchorNode& a);

//Determines if the awo anchors overlap on the current point of view (the so-called Query side)
bool OverlappingAnchors(AnchorNode* a,AnchorNode* b);
Uint4 OverlapSize(AnchorNode* a,AnchorNode* b);

struct AnchorPair
	{
	AnchorNode* i;
	AnchorNode* j;
	};

/* class Overlap :  the class for manipulating overlapping anchors and clusters
 *          Attributes
 *          start, end (always on a specific genome)
 *			anchorstack :  a vector of pointer of AnchorNodes belonging (overlapping) to the overlap
 */

class Overlap
{
public:
	Overlap(): start(0),end(0),name("") {};
	void ReSet(AnchorNode * m);
	bool Updating(AnchorNode* m );
	bool Merging(AnchorNode* m );
	void DeselectMembers();

	Uint2 Depth() const { return anchorstack.size(); };
	Uint4 Start() const { return start; };
	Uint4 End() const { return end; };
	Uint4 Size() const { return end-start+1; };
private:
	Uint4 start, end;
	string name;
	vector<AnchorNode*> anchorstack;
};

ostream& operator<<(ostream& os,const Overlap& o);

/* class Cluster :  inherits from AnchorNode
 *          A cluster is a collection of anchors
 *          Attributes
 *          start, end (always on a specific genome)
 *			anchorstack :  a vector of pointer of AnchorNodes belonging (overlapping) to the overlap
 */

class Cluster : public AnchorNode
	{
public:
	Cluster(): AnchorNode(), num(0) { };
	void SetClusterProperties(void);
	void SetClusterNum(Uint2 k);
	vector<AnchorNode*> members;

	void SetView(ViewType v);

	Uint2 Num() const { return num; };

	void Set_CoVar(float cv) { covar=cv; };

	string Q_Chain() const;
	string S_Chain() const;
	double Q_Var() const { return q_var;};
	double S_Var() const { return s_var;};
	double CoVar() const { return covar;};
	void SortMembers();
	void DeselectedMembers();
	bool IsDiagonal();

	void __DetectAndRemoveOverlap(ViewType side);

	void PrintDebug();
private:
	Uint2 num;
	double q_mean, s_mean;
	double q_var, s_var, covar;
	};

ostream& operator<<(ostream& os, const Cluster& c);

struct Point
{
	Point(): x(0),y(0) {};
	Uint4 x;
	Uint4 y;
};

struct CmpAnchorNodePtr : public binary_function<AnchorNode*, AnchorNode*, bool>
{
  bool operator()(AnchorNode* a, AnchorNode* b)
  {
	  if (a->Q_Name() == b->Q_Name())
	  		return a->Q_Start() < b->Q_Start();
	  	else
	  		return a->Q_Name() < b->Q_Name();
  }
};

struct CmpAnchorNodeQOrderPtr : public binary_function<AnchorNode*, AnchorNode*, bool>
{
  bool operator()(AnchorNode* a, AnchorNode* b)
  {
	  return a->Q_Order() < b->Q_Order();
  }
};

struct CmpAnchorNodeClusterPtr : public binary_function<AnchorNode*, AnchorNode*, bool>
{
  bool operator()(AnchorNode* a, AnchorNode* b)
  {
	  return a->ClusterId() < b->ClusterId();
  }
};

//For debugging purpose only

inline string View2String(ViewType v)
{
	return ( v == QUERY ) ? "Query" : "Subject";
}

#endif
