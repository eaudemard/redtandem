/*
   $Id: bitcount.cpp 263 2009-09-21 16:10:48Z tfaraut $
*/

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "bitcount.h"

/* Iterated bitcount iterates over each bit. The while condition sometimes helps
   terminates the loop earlier */
int iterated_bitcount (unsigned int n)
{
    int count=0;
    while (n)
    {
        count += n & 0x1u ;
        n >>= 1 ;
    }
    return count ;
}


/* Here is another version of precomputed bitcount that uses a precomputed array
   that stores the number of ones in each short. */

static char bits_in_16bits [0x1u << 16] ;

void compute_bits_in_16bits (void)
{
    unsigned int i ;
    for (i = 0; i < (0x1u<<16); i++)
        bits_in_16bits [i] = iterated_bitcount (i) ;
    return ;
}

#ifdef PROC64
int precomputed16_bitcount (Uint8 n)
{
    // works only for 64-bit int

    return bits_in_16bits [n         & 0xffffu]
        +  bits_in_16bits [(n >> 16) & 0xffffu]
		  +  bits_in_16bits [(n >> 32) & 0xffffu]
		  +  bits_in_16bits [(n >> 48) & 0xffffu];
}
#else
int precomputed16_bitcount (unsigned int n)
{
    // works only for 32-bit int

    return bits_in_16bits [n         & 0xffffu]
        +  bits_in_16bits [(n >> 16) & 0xffffu] ;
}
#endif




