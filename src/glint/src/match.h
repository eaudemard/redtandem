/*
   $Id: match.h 260 2009-08-07 15:35:54Z tfaraut $
*/


/* match.h */


#ifndef _MATCH_H
#define _MATCH_H

#include <limits>
#include <climits>
#include "memory.h"
#include "word.h"
#include "masque.h"
#include "alignment.h"
#include "ptypes.h"
#include "bitcount.h"
#include <string.h>
#include <math.h>

#define POW24 0x01000000
#define POW20 0x00100000
#define MIN_DIAG LONG_MIN       /* Attention à la compatibilité 32 bits 46 bits */

#define Strandboo(X) ((X>0) ? 1 : 0)


/*

    A Seed is a word found in the index tree
    The hit can be kept at different levels in the tree.
    The deeper the level, the more specific the match

*/

struct Seed {
    Seed(): strand(0),level(0),node(NULL){};
    void Reset(short s) {
        strand=s;
        level=0;
        node=NULL;
    };
    short strand;
    int level;			// the level in the CTree
    const unsigned char* node;			// the node at which a match was detected
};

/*

    A Match is created when a seed found in the tree matches with the request.
    The following are in the Match:
        -The position in the sequence and in the query (spos, qpos)
        -The score (c'est quoi deja ?)
        -The level of the seed in the tree (c'est hit ?)
        -The diagonal of the match: <0, 0, >0
*/

struct match {
    match():diag(0),score(0),hit(0),spos(0),qpos(0){};
	DiagInt diag;
	Uint2 score;
	Uint1 hit;
	Uint4 spos;
	Uint4 qpos;

	friend ostream & operator<<(ostream& os, const match &);
};

/*

    A MatchPile is a set of matches - The matches are preallocated, and there are no more than 'size'
    matches in the pile.
    There is an HspPile inside it to store the hsp computed from the matches
    You can Push a new match in the pile, or compute the alignments from the set of matches

    Mechanisms for computing alignments:
    -----------------------------------
		* ComputeSimpleMatches sorts the matches according to the diagonal ( diag = spos - strand*qpos ), smallest diagonals first
        * ScoreSlidingDiagonal iterates through the matches
             - adding the scores of matches belonging to the same diagonal that are within 100 base pairs
             - a mechanism enables to compute exact scores of overlapping seeds ( bscore stores an array of digits: the fused overlapping seeds)
             - if the score is greater than the parameter Cutoff_Slide LocateMatchesChunk is called
        * LocateMatchesChunk implements a seed extension algorithm followed by a dynamic programming algorithm if the score is high enough
		* If an hsp is found, the upper diagonal is stored in Last_Diag and the subject end of the hsp enabling to skip subsequent matches that
		  would lead to the same hsp

    Associations:
         MatchPile ---> HspPile
         MatchPile ---> AlignEngine
         MatchPile ---> Display

*/

class MatchPile {
    public:
    MatchPile(int sz,int st, const QBuffer& q, const Display & d);
    int Push(Uint4 pos_s, Uint4 q_pos,Uint1 hit);
    void ComputeSimpleMatches();
    void Reset();
    void HspReset();
    /*void LinkingVect() { HP.LinkingVect();}
    void Linking() { HP.Linking();}*/
    HspPile* GetHspPile() { return &HP; };

    /* Iterators */

    HspIterator begin();
    HspIterator end();

    int TheTop() { return top;};
    bool IsNotEmpty() { return (top>0); };

    private:
    void ScoreSlidingDiagonal();
    void ScoreSlidingDiagonalNew();

    bool LocateMatchesChunk(match **m, int min, int max,Int2 score);
    void seed_qsort(void);
    void seed_display(void);

    /* Variables recording information on the previously detected hsp */
    DiagInt LastDiag;   // The last diagonal on which an hsp has been detected
    Uint4 SubjectEnd;   // The subject end on this last diagonal
    Uint4 SubjectStart; // The subject start on this last diagonal

    Uint4 size;
    int strand;     // The strand, common to this match list (-1/1)
	Uint4 top;
	HspPile HP;
	AlignEngine align_results;
	const Display& display;

	match **l;
	match *p;
};

/*

     A MatchResults is a set of (currently 2) MatchPile
     You can set the current strand, retrieve the current MatchPile, compute the hsp
     mixing the results of both MatchPiles
     You can also display all Hsp

     Associations:
        MatchResults --> (2) MatchPile
        MatchResults --> Display

*/

class MatchResults {
    public:
    MatchResults(Uint4 size, const QBuffer& q, FILE* f);

    void SetStrand(Uint2 s) {
        strand = (s!=0)? 1:0;
    };
    void Reset() {match_pile_strand[0]->Reset();match_pile_strand[1]->Reset();};
    void HspReset(){match_pile_strand[0]->HspReset();match_pile_strand[1]->HspReset();};
    MatchPile& GetCurrentMatchPile() {return *match_pile_strand[strand];};
    void ComputeHsp();
	void PrintAllHsp(const string&, const string&);
	//void SortingAndLinking();
	void DisplayResultsHeader();
    void Status();

    private:
	MatchPile* match_pile_strand[2];
    int strand;
    const Display& display;
    /*
       Initializing the Display object: we create an object derived from Display */
    Display & _init_display(const QBuffer& q, FILE* f);
};

/*

   THOMAS

*/
struct ScoreMatrix {
    ScoreMatrix(const Mask& msk,float etotal)
    {
        int i,size=0;
        for (i=ROOT_LEVEL;i<=msk.GetLeafLevel();i++)
        {
            size += msk.GetLength(i);
            m[i] = etotal*pow(0.25,size);
            s[i] = size;
        }
    }
    double m[MAX_LEAF_LEVEL];
    short s[MAX_LEAF_LEVEL];
};

struct MatchConstants {
    MatchConstants (const Mask& m,float etotal) {
        score_matrix=new ScoreMatrix(m,etotal);
        Overlap_score=init_overlap_score(m);
        Overlap_score_bw=init_overlap_score_bw(m);
		  compute_bits_in_16bits();
    }
	ScoreMatrix* score_matrix;
	short*** Overlap_score;
	short*** Overlap_score_bw;
};


int compare_match(const void *,const void *);
int compare_match_diag(const void *,const  void *);
int compare_match_diag_spos(const void *,const  void *);
int compare_match_qpos(const void *premier,const  void *second);


#ifdef DEBUGLOG
void _DebugPrintSelMatches(match **m, int min, int max,Int2 score);
void _DebugPrintTopMatch(match **m, int ind_max);
void _DebugPrintAlign(AlignEngine *,char* type);
void _DebugPrintSelHsp(hsp *);
#endif

#endif

