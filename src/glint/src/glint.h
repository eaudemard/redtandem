/*
   $Id: glint.h 222 2008-05-28 06:35:44Z tfaraut $
*/


#ifndef __GLINT_H__
#define __GLINT_H__

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <vector>

#include "ptypes.h"

#define DEFAULT_CHAINING_CUTOFFS "200000-500-0.9:700000-1000-0.8:1300000-1000-0.7"
#define UNDEFINED_STRING "__undefined_string"

void ProcessArgVect(int argc, char *argv[]);

void SetListFileName(const string& ListFileName, bool bAppend);
void Log(const char szFormat[], ...);
const char *GetTimeAsStr();
void Quit(const char szFormat[], ...);
void GetLineTokens(std::vector<std::string>&,const string&);




#endif


