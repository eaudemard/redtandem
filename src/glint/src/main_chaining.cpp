/*
   $Id: main_statistiques.cpp 190 2007-11-20 14:33:37Z faraut $
*/

#include <iostream>
#include <fstream>

#include "glint.h"
#include "chain.h"
#include "utils.h"
#include "options.h"

using namespace std;

void Usage();
void FirstClustering(AnchorVect& A, AnchorVect& B );
void ChainLowerLevel(AnchorVect& A, AnchorVect& B, Uint4 md, Uint4 mc, float mcv );
void GetOptions(int argc, char** argv, void(*help)());
void LogParameters(string file, int argc,char** argv);
void __OptionsValidation();
extern CSimpleOpt::SOption g_GlintOptions[];

string g_GlintOutputFilename = "";
string g_BlastpOutputFilename = "";
string g_AnchorsOutputFilename = "";
string g_OutputFilename;
string g_cutoffs = UNDEFINED_STRING;
string g_targetdir="";
Uint4  g_maxoverlap = DEFAULT_MAX_CHAIN_OVERLAP;
bool g_RBH=true;
bool g_SELF=false;

//For old compiler
RealLogger* logger;

int main(int argc,char** argv)
{
	string output = "chain_output";

	GetOptions(argc,argv,Usage);

	string baseoutputfile = (  g_targetdir == ""  ) ? "output_chain" :  g_targetdir + "/" + "chain";

	if ( g_SELF == true )
	{
		cerr<<"Self chaining has been asked"<<endl;
	}

	Syntenies synt( g_cutoffs, g_GlintOutputFilename, g_BlastpOutputFilename, g_AnchorsOutputFilename, baseoutputfile, g_RBH, g_maxoverlap);

	synt.LoadAlignments();
	synt.ConstructSyntenyLevels();

	synt.Print();

	string baselogfile = baseoutputfile + ".log";
	LogParameters( baselogfile, argc, argv );

	return 0;
}

void GetOptions(int argc, char** argv, void(*help)())
{
	// declare our options parser, pass in the arguments from main
	// as well as our array of valid options.
	if (argc == 1) {
		help();
		exit(0);
	}

	CSimpleOpt args(argc, argv, g_GlintOptions);
	string cutoffs;
	// while there are arguments left to process
	while (args.Next()) {
		if (args.LastError() == SO_SUCCESS) {
			if (args.OptionId() == OPT_HELP)
			{
				help();
				exit(0);
			}
			else if ( args.OptionId() == OPT_GLINTOUT )
			{
				g_GlintOutputFilename =(string) args.OptionArg();
			}
			else if ( args.OptionId() == OPT_BLASTPOUT )
			{
				g_BlastpOutputFilename =(string) args.OptionArg();
			}
			else if ( args.OptionId() == OPT_ANCHORSOUT )
			{
				g_AnchorsOutputFilename =(string) args.OptionArg();
			}
			else if (args.OptionId() == OPT_CUTOFFS )
			{
				g_cutoffs = (string) args.OptionArg();
			}
			else if (args.OptionId() == OPT_MAXOVERLAP )
			{
				g_maxoverlap = atoi(args.OptionArg());
			}
			else if (args.OptionId() == OPT_TARGETDIR )
			{
				g_targetdir = (string) args.OptionArg();
			}
			else if (args.OptionId() == OPT_NORBH )
			{
				g_RBH = false;
			}
			else if (args.OptionId() == OPT_SELF )
			{
				g_SELF = true;
			}
			else if (args.OptionId() == OPT_RESULTSFILENAME )
			{
				g_OutputFilename = args.OptionArg();
			}
			else
			{
				cerr<<"Unknown option "<< args.OptionText() <<endl;
			}
		}
	}

	__OptionsValidation();
}

void __OptionsValidation()
{

	if ( g_GlintOutputFilename =="" && g_BlastpOutputFilename=="" && g_AnchorsOutputFilename=="" )
	{
		fprintf(stderr,"At least one output filename must be provided.\n");
		Usage();
		exit(EXIT_FatalError);
	}

    if ( g_AnchorsOutputFilename !="" && ( g_GlintOutputFilename != "" || g_BlastpOutputFilename != "" ) )
    {
    	fprintf(stderr,"You must choose between anchors or glint and blastp.\n");
    	fprintf(stderr,"Please see program usage (-h option).\n");
    	exit(EXIT_FatalError);
    }

    if ( g_cutoffs == UNDEFINED_STRING)
    {
    	fprintf(stderr,"Using default cutoff parameters.\n");
    	g_cutoffs=DEFAULT_CHAINING_CUTOFFS;
    }

    if ( !ValidCutoffParam( g_cutoffs ) )
    {
    	fprintf(stderr,"Warning. Something wrong with the cutoff parameters.\n");
    	fprintf(stderr,"Please see program usage (-h option).\n");
    	exit(EXIT_FatalError);
    }
}

void LogParameters(string file_name, int argc,char** argv)
{
	ofstream out(file_name.c_str());

	out << "Command: ";
    for (int p=0; p<argc;p++)
        out << argv[p] << ' ';
    out << '\n';

	if ( g_GlintOutputFilename.size() > 0)
		out << "glintout: "<< g_GlintOutputFilename <<endl;
	if ( g_BlastpOutputFilename.size() > 0  )
		out << "blaspout: "<<  g_BlastpOutputFilename <<endl;
	if ( g_AnchorsOutputFilename.size() > 0 )
		out << "anchors: "<<  g_AnchorsOutputFilename <<endl;
	out<<"Rbh: "<<g_RBH<<endl;
	out<<"Self: "<<g_SELF<<endl;
	out<<"targetdir: "<<g_targetdir<<endl;
	out<<"cutoffs: "<<g_cutoffs<<endl;

	out.close();
}



/*Utilisation du programme */
void Usage()
{
	fprintf(stderr,"glintchain: chaining program for glint hsps\n");
	fprintf(stderr,"Usage: glintchain --glintout glint_output --blastpout blastp_output --cutoffs CMD-CS-CCV\n");
	fprintf(stderr,"    or glintchain --anchors anchors_output --cutoffs CMD-CS-CCV\n\n");
	fprintf(stderr,"\tOptions:\n");
	fprintf(stderr,"\t\t --glintout glint.out : the file with the glint ouput in -m 2 format \n");
	fprintf(stderr,"\t\t --blastpout blastp.out : the file with the protein blastp output in ?? format \n");
	fprintf(stderr,"\t\t --cutoffs CMD-CS-CCV : the chaining parameters see below \n");
	fprintf(stderr,"\t\t --targetdir dir : directory for the results files (default local directory)\n");
	fprintf(stderr,"\tThe cutoff values are given for each level as 3 values using the following format CMD-CS-CCV : \n");
	fprintf(stderr,"\tCMD : cutoff value for the manhattan Distance separating anchors (integer, a strict maximum value)\n");
	fprintf(stderr,"\t\tThis parameter will be used for the construction of connected components.\n");
	fprintf(stderr,"\tCS  : cutoff score for a cluster to be part of the cluster partition (integer, a strict minimum value)\n");
	fprintf(stderr,"\tCCV : cutoff score for the covariation of anchors within a cluster (float (0<CCV<1), a strict minimum value)\n");

}

