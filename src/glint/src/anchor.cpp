/*
   $Id: anchor.cpp 222 2008-05-28 06:35:44Z tfaraut $
*/


/* anchor.cpp  */

#include <ctype.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <functional>
#include <sstream>
#include <algorithm>
#include <limits>
#include <math.h>
#include <assert.h>

#include "anchor.h"
#include "glint.h"
#include "utils.h"

#define INFTY_DIST 1000000000

//#define View2String(a) ( a == QUERY ) ? "Query" : "Subject"

/* The methods of the AnchorData class */

/*
  Name:      The AnchorData default constructor
  Parameter: none
  Function:  Initialize an object of class AnchorData to default values
*/
AnchorData::AnchorData() :
	q_name(""),s_name(""),q_start(0),q_end(0),s_start(0),s_end(0),q_center(0),s_center(0),q_order(0),s_order(0)
{

}

void AnchorData::Set_Centers()
{
	q_center=(Uint4)floor((q_start+q_end)/2);
	s_center=(Uint4)floor((s_start+s_end)/2);
}

/* The methods of the Anchor class */

/*
  Name:      The default anchor constructor
  Parameter: none
  Function:  Initialize an object of class anchor to default values
*/

Anchor::Anchor() :
	clusterid(0),orientation(Plus),score(0),size(0),_view_type(QUERY),molecule(DNA)
{
	AnchorData* a=new AnchorData();
	query.data=a;
	subject.data=a;

	SetView(_view_type);

}

Anchor::Anchor(const Anchor& a) :
	clusterid(a.clusterid),orientation(a.orientation),score(a.score),size(a.size),_view_type(a._view_type),molecule(a.molecule)
{
	query.data=a.query.data;
	subject.data=a.subject.data;

	SetView(a.GetView());
}


/*
bool operator<(const Anchor& a, const Anchor& b)
	{
	if (a.Q_Name() == b.Q_Name())
			return a.Q_Start() < b.Q_Start();
		else
			return a.Q_Name() < b.Q_Name();
	}


ostream& operator<<(ostream& os, const Anchor& a)
	{
	os << a.q_name <<"\t"<< a.q_start <<"\t" << a.q_end  <<"\t";
	os<< a.s_name  <<"\t"<< a.s_start <<"\t"<<  a.s_end  <<"\t";
	os<< a.score  <<"\t"<<  a.strand  <<"\t"<<  a.size;
    return os;
	}

ostream& operator<<(ostream& os, const AnchorNode& a)
	{
	os<<(Anchor) a<<"\t"<<a.cluster;
    return os;
	}
*/

enum AF
	{
	Q_NAME,
	Q_START,
	Q_END,
	S_NAME,
	S_START,
	S_END,
	SCORE,
	SIZE,
	STRAND,
	CLUSTER
	};

static Int1 GetFieldOrder(const string& sFieldName)
	{
	static string FieldNames[10] =
		{
		"q_name",
		"q_start",
		"q_end",
		"s_name",
		"s_start",
		"s_end",
		"score",
		"size",
		"strand",
		"cluster"
		};
	for (int i=0; i < 10 ; i++)
		{
			if ( sFieldName == FieldNames[i])
				return i;
		}
	return -1;
	}

OrientType String2Orient(const string& value)
{
	return ( value == "+" ) ? Plus : Minus;
}


void Anchor::SetField(const std::string& name,const string& value)
{
	switch ( GetFieldOrder( name ) )
			{
			case Q_NAME:
				Set_Q_Name( value );
				break;
			case Q_START:
				Set_Q_Start( String2Uint4( value ) );
				break;
			case Q_END:
				Set_Q_End( String2Uint4(value) );
				break;
			case S_NAME:
				Set_S_Name( value );
				break;
			case S_START:
				Set_S_Start( String2Uint4( value ) );
				break;
			case S_END:
				Set_S_End( String2Uint4( value ) );
				break;
			case SCORE:
				Set_Score( String2Uint4( value ) );
				break;
			case SIZE:
				Set_Size( String2Uint4( value ) );
				break;
			case STRAND:
				Set_Orientation( String2Orient( value ) );
				break;
			case CLUSTER:
				Set_ClusterId( String2Uint4( value ) );
				break;
			default :
				1;
#ifdef DEBUGLOG
				cerr<<"Ignored field :"<<name<<endl;
#endif
			}
	}

Anchor& Anchor::operator=(const Anchor& a)
{
	query.data=a.query.data;
	subject.data=a.subject.data;

	SetView(a.GetView());

	return *this;
}

//--------------------------------------------------------------------------------------------
// Managing the views

/*
 Name:   Anchor::SetView
 usage:  a.SetView(VIEW)
 Procedure: Sets the view for the current anchor (VIEW is QUERY or SUBJECT)
 */

void Anchor::SetView(ViewType side)
{
	if ( side == QUERY )
	{
		view=&query;
	}
	else
	{
		view=&subject;
	}
	_view_type = side;
}

const string Anchor::MoleculeType() const
{
	string mol;

	switch ( molecule )
	{
	case DNA:
		mol = "dna";
		break;
	case PROT:
		mol = "prot";
		break;
	case COMB:
		mol = "comb";
		break;
	default :
		mol = "anchor";
	}
	return mol;
}

void Anchor::Check_Orientation()
{
	if ( S_End() < S_Start() )
	{
		Uint4 ls_start = S_Start();
		Set_S_Start( S_End() );
		Set_S_End( ls_start );
		Set_Orientation(Minus);
	}

}

bool operator<(const Anchor& a, const Anchor& b)
{
	if (a.Q_Name() == b.Q_Name())
		return a.Q_Start() < b.Q_Start();
	else
		return a.Q_Name() < b.Q_Name();
}

void AnchorNode::AddNeighbour(const edge& e)
	{
	neighbours.push_back(e);
	}

void  AnchorNode::ReverseSegment()
{
	Uint4 ls_start=S_Start();
	Set_S_Start( S_End() );
	Set_S_End( ls_start );
}

bool OverlappingAnchors(AnchorNode* a,AnchorNode* b)
{
	if (*b<*a)
	{
		AnchorNode* c;
		c=a;a=b;b=c;
	}

	if ( ( a->Q_Name() == b->Q_Name() ) && ( b->Q_Start() < a->Q_End() ) )
		return true;
	else
		return false;
}

Uint4 OverlapSize(AnchorNode* a,AnchorNode* b)
{
	if (*b<*a)
	{
		AnchorNode* c;
		c=a;a=b;b=c;
	}

	// No need to check for the positivity of the next quantity because it was forced to be positive by
	// the reordering step just above

	if ( ( a->Q_Name() == b->Q_Name() ) && ( b->Q_Start() < a->Q_End() ) )
		return ( a->Q_End()-b->Q_Start() );
	else
		return 0;

}

/*
Name:  Set
usage: Set(m)
Function: sets the overlap interval to the one defined by m
Args: m an AnchorNode pointer
return: none
*/
void Overlap::ReSet(AnchorNode* m)
{
	anchorstack.clear();
	name = m->Q_Name();
	start = m->Q_Start();
	end = m->Q_End();
	anchorstack.push_back(m);
}


/*
Name:  Updating
usage: Updating(m)
Function: returns true if m is overlapping the current interval, updating the overlap, and false otherwise
Args: m an AnchorNode pointer
return: a boolean
TODO : obsolete
*/
bool Overlap::Updating(AnchorNode* m)
{
	if ( m->Q_Name() == name && m->Q_Start() < end )
	{
		end = ( m->Q_End() > end ) ?  m->Q_End() : end;
		anchorstack.push_back(m);
		return true;
	}
	else
	{
		return false;
	}
}

/*
Name:  Merging
usage: Merging(m)
Function: merge the anchor m to the current interval if m is overlapping, returning true. Returns false if m does not overlap o
Args: m an AnchorNode pointer
return: a boolean
*/
bool Overlap::Merging(AnchorNode* m)
{
	if ( m->Q_Name() == name && m->Q_Start() < end )
	{
		end = ( m->Q_End() > end ) ?  m->Q_End() : end;
		anchorstack.push_back(m);
		return true;
	}
	else
	{
		return false;
	}
}


void Overlap::DeselectMembers()
{
	cerr<<"Repeat region detected "<<name<<":"<<start<<"-"<<end<<" ("<<anchorstack.size()<<" elements)"<<endl;
	while ( !anchorstack.empty() )
	{
		AnchorNode *a=anchorstack.back();
		if ( a->Size() < REPEAT_THRESHOLD_SIZE ) a->Deselect();  //Only small anchors are considered as repeats
		//a->Deselect();
		anchorstack.pop_back();
	}

}

void Cluster::SetView(ViewType side)
{
	Anchor::SetView(side);

	for ( vector<AnchorNode*>::iterator j = members.begin() ; j != members.end() ; j++ )
	{
		AnchorNode *a=*j;
		a->SetView(side);
	}
}

void Cluster::DeselectedMembers()
{
	for ( vector<AnchorNode*>::iterator j = members.begin() ; j != members.end() ; j++ )
	{
		AnchorNode *a=*j;
		a->Deselect();
	}
}

bool Cluster::IsDiagonal()
{
	if ( S_Start() < Q_End())
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Cluster::SetClusterNum( Uint2 k)
{
	num = k;
}

void Cluster::SetClusterProperties()
{
	Uint4 min_x = 0, max_x = 0, min_y = 0, max_y = 0;
	Uint4 score = 0,  span = 0;

	double q_sum = 0, s_sum = 0;
	double q_sum2  = 0, s_sum2 = 0;
	double co_sum = 0, co_mean;
	double q_mean2, s_mean2;

	MolType cluster_moltype;

	/* Temporary variables to keep track of the largest cluster */
	Uint4 max_span;
	AnchorNode *max_span_anchor;

	Uint4 n=0;
	for ( vector<AnchorNode*>::iterator j = members.begin() ; j != members.end() ; j++ )
	{
		AnchorNode *a=*j;
		if ( a->Selected() )
		{
			if ( 0 == n)
			{
				cluster_moltype = a->Molecule();
				max_span = a->Span();
				max_span_anchor	= a;
			}
			n++;
			min_x = ( min_x > 0 && min_x < a->Q_Start() ) ? min_x : a->Q_Start() ;
			max_x = ( max_x > a->Q_End() ) ? max_x : a->Q_End() ;
			min_y = ( min_y > 0 && min_y < a-> S_Start() ) ? min_y : a->S_Start() ;
			max_y = ( max_y > a->S_End() ) ? max_y : a->S_End() ;
			score+=a->Score();
			size+=a->Size();

			//We work in kb in order to prevent from overflow errors
			double q_center=a->Q_Center()/1000;
			double s_center=a->S_Center()/1000;
			q_sum += q_center;
			s_sum += s_center;
			q_sum2 += (q_center*q_center);
			s_sum2 += (s_center*s_center);
			co_sum += (q_center*s_center);

			/* Keeping track of the largest sub-chain */
			if ( a->Span() > max_span )
			{
				max_span = a->Span();
				max_span_anchor = a;
			}

			/* Detecting the cluster molecule type */
			if ( cluster_moltype != a->Molecule() )
						cluster_moltype=COMB;
		}
	}

	span = ( (max_y- min_y) > (max_x - min_x) ) ? (max_y- min_y) : (max_x - min_x);

	Set_Q_Name(members.front()->Q_Name());
	Set_S_Name(members.front()->S_Name());

	 //TODO A serious correction will have to be made on the cluster span (? Why ?)
	Set_Size(n);
	Set_Span(span);
	Set_Score(score);
	Set_Q_Start( min_x );
	Set_Q_End( max_x );
	Set_S_Start( min_y );
	Set_S_End( max_y );
	Set_Molecule( cluster_moltype );

	Set_Centers();

	q_mean = q_sum/n;
	s_mean = s_sum/n;

	q_mean2 = q_sum2/n;
	s_mean2 = s_sum2/n;

	co_mean=co_sum/n;

	//  n = 1 or 2 : we just take the orientation from the best or the only node
	if ( n <= 2 )
	{
		covar=1;
		AnchorNode* first =  members.front();
		AnchorNode* second =  members.back();
		AnchorNode* DominantSegment=( first->Score() >= second->Score() ) ? first : second;
		orientation=DominantSegment->Orientation();
	}
	// We have here a dominant segment in the cluster, we use its orientation for the cluster orientation
	else if  ( max_span > span/2 )
	{
		covar=1; //We consider that the dominant segment (already selected) satisfies the covariation threshold
		orientation = max_span_anchor->Orientation();
	}
	// We can calculate the variances
	else
	{
		q_var=((q_mean2-q_mean*q_mean)*n)/(n-1);
		s_var=((s_mean2-s_mean*s_mean)*n)/(n-1);
		//printf("%d : %f and  %f   %f \n",n,q_mean2,q_mean*q_mean,q_mean2-q_mean*q_mean);
		//printf("%d : %f and  %f   %f \n",n,s_mean2,s_mean*s_mean,s_mean2-s_mean*s_mean);
		assert(q_var >= 0 && s_var >= 0);

		if ( q_var == 0 || s_var == 0 )  // One of the variance is null
		{
			//cerr<<"One or the other variance is 0"<<endl;
			covar=std::numeric_limits<float>::quiet_NaN();
			orientation=Plus;
		}
		// The covariation coefficient is calculated and orientation is decided accordingly
		else
		{
			covar=( (co_mean-q_mean*s_mean)*n/(n-1) )/( sqrt(q_var) * sqrt(s_var));
			orientation=( covar > 0 ) ? Plus : Minus;
		}
	}

	Set_CoVar( covar );
	Set_Orientation( orientation );

}

string Cluster::Q_Chain() const
{
	stringstream ss;
	string s="";
	vector<AnchorNode*> temp_members;

	for ( vector<AnchorNode*>::const_iterator j = members.begin() ; j != members.end() ; j++ )
	{
		if ( (*j)->Selected() ) temp_members.push_back(*j);
	}

	sort(temp_members.begin(),temp_members.end(),CmpAnchorNodePtr());
	for ( vector<AnchorNode*>::iterator j = temp_members.begin() ; j != temp_members.end() ; j++ )
	{
			AnchorNode *a=*j;
			ss<<a->Q_Order()<<":";
	}

	ss>>s;

	return s;
}

string Cluster::S_Chain() const
{
	stringstream ss;
	string s="";


	for ( vector<AnchorNode*>::const_iterator j = members.begin() ; j != members.end() ; j++ )
	{
		AnchorNode *a=*j;
		if ( a->Selected() ) ss<<a->S_Order()<<":";
	}
	ss>>s;

	return s;
}

void Cluster::PrintDebug(void)
{
	cerr<<"Is selected : "<<this->Selected()<<endl;
	cerr<<"Now members "<<endl;
	for ( vector<AnchorNode*>::iterator j = members.begin() ; j != members.end() ; j++ )
	{
		cerr<<(**j)<<endl;
	};
	cerr<<"---------------"<<endl;
}

/*
 Name:      Cluster::__DetectAndRemoveOverlap
 Usage:		__DetectAndRemoveOverlap(side)
 Function:  Detects and removes overlapping anchors on the specified side
            returns a pointer on a list of anchors : the reciprocal best hit sets
 Arg:       the side that has to be considered
 Error:     none
*/

void Cluster::__DetectAndRemoveOverlap(ViewType side)
{
	vector<AnchorPair> overlapping;
	AnchorPair o;
	bool modified = false;

	ViewType saved_view=_view_type;

	SetView(side);
	sort(members.begin(),members.end(),CmpAnchorNodePtr());
	for ( AnchorNodeIterator i = members.begin() ; i!=members.end()-1 ; ++i )
	{
		for (AnchorNodeIterator j = i+1; j != members.end() ; ++j )
		{
			if (OverlappingAnchors(*i,*j))
			{
				o.i=*i;
				o.j=*j;
				overlapping.push_back(o);
			}
		}
	}

	for (vector<AnchorPair>::iterator k=overlapping.begin(); k !=overlapping.end(); k++)
	{
		if (  k->i->Selected() && k->j->Selected() )
		{
			if ( k->i->Score() > k->j->Score() )
				k->j->Deselect();
			else
				k->i->Deselect() ;
		}
		modified = true;
	}

	SetView(saved_view);
}

