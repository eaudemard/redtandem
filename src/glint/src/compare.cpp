/*
   $Id: compare.cpp 2889 2009-09-21 16:04:11Z tfaraut $
*/

#include <errno.h>
#include <stdlib.h>
#include <string>
#include <iostream>

using namespace std;

#include "compare.h"
#include "options.h"

extern RealLogger* logger;

extern CSimpleOpt::SOption g_GlintOptions[];


/*
  Name:      The Compare_Parameters constructors
  Parameters: const Compare_Parameters& compare
  Function:  Initialize an object of class Compare_Parameters from the command line
*/
Compare_Parameters::Compare_Parameters(const Compare_Parameters& compare):
msk(compare.msk)
{
	*this=compare;
}

Compare_Parameters::Compare_Parameters(int argc, char** argv, void(*help)()):
Cutoff_Slide(DEFAULT_CUTOFF_SLIDE),
Cutoff_Dyn(DEFAULT_CUTOFF_DYN),
Cutoff_Hsp(DEFAULT_CUTOFF_HSP),
complexity(DEFAULT_COMPLEXITY),
Fill(false),
Genome(false),
Linking(false),
OutputStream(false),
Pipe(false),
Self(false),
Strict(false),
Dyn(true),
Verbose(false),
Silent(false),
Tandem(DEFAULT_TANDEM_SIZE),
Match(DEFAULT_MATCH),
MisMatch(DEFAULT_MISMATCH),
NMatch(DEFAULT_NMATCH),
GapOp(DEFAULT_GAP_OPENING),
GapExt(DEFAULT_GAP_OPENING), 
UngapDropoff(DEFAULT_UNGAP_DROPOFF),
GapDropoff(DEFAULT_GAP_DROPOFF),
MaxMisMatch(-1),
MaxGap(-1),
SizeMin(-1),
MinRatioOfMaxScore(-1),
MinRatioOfMaxLength(-1),
msk(DEFAULT_MASK),
Max_Count(DEFAULT_MAX_COUNT),
lower_level(DEFAULT_LOWER_LEVEL),
deepest_level(DEFAULT_DEEPEST_LEVEL),
output_format(DEFAULT_OUTPUT_FORMAT),
step(DEFAULT_STEP),
results_filename("")
{
		// declare our options parser, pass in the arguments from main
	    // as well as our array of valid options.
	    if (argc == 1) {
			help();
			exit(0);
	    }

	    CSimpleOpt args(argc, argv, g_GlintOptions);

	    // while there are arguments left to process
	    while (args.Next()) {
	        if (args.LastError() == SO_SUCCESS) {
	            if (args.OptionId() == OPT_HELP) {
	                help();
	                exit(0);
	            }
	            else if (args.OptionId() == OPT_CUTOFF_SLIDE )
	            {
	            	Cutoff_Slide = atoi(args.OptionArg());
	            }
	            else if (args.OptionId() == OPT_CUTOFF_DYN )
	            {
	            	Cutoff_Dyn = atoi(args.OptionArg());
	            }
	            else if (args.OptionId() == OPT_CUTOFF_HSP )
	            {
	            	Cutoff_Hsp = atoi(args.OptionArg());
	            }
	            else if (args.OptionId() == OPT_LOWER_LEVEL )
	            {
	            	lower_level = atoi(args.OptionArg());
	            }
	            else if (args.OptionId() == OPT_DEEPEST_LEVEL )
	            {
	            	deepest_level = atoi(args.OptionArg());
	            }
	            else if (args.OptionId() == OPT_COMPLEXITY )
	            {
	            	complexity = atoi(args.OptionArg());
	            }
	            else if (args.OptionId() == OPT_OUTPUTFORMAT)
	            {
	            	output_format = atoi(args.OptionArg());
	            }
	            else if (args.OptionId() == OPT_RESULTSFILENAME )
	            {
	            	results_filename = (string) args.OptionArg();
	            }
	            else if (args.OptionId() == OPT_GENOME )
	            {
	            	Genome = true;
	            }
	            else if (args.OptionId() ==  OPT_FILL)
	            {
	            	Linking = true;
	            }
	            else if (args.OptionId() == OPT_OUTPUTSTREAM )
	            {
	            	OutputStream = true;
	            }
	            else if (args.OptionId() ==  OPT_PIPE )
	            {
	            	Pipe = true;
	            }
	            else if (args.OptionId() ==  OPT_SELF )
	            {
	            	Self = true;
	            }
	            else if (args.OptionId() ==  OPT_TANDEM )
	           	{
	           	    Tandem = atoi(args.OptionArg());
	           	} 
	           	else if (args.OptionId() ==  OPT_MATCH )
	            {
	            	Match = atoi(args.OptionArg());
	            }
	            else if (args.OptionId() ==  OPT_MISMATCH )
	            {	            	
	            	MisMatch = atoi(args.OptionArg());
	            }
	            else if (args.OptionId() ==  OPT_GAP_OPENING )
	            {
	            	GapOp = atoi(args.OptionArg());
	            }
	            else if (args.OptionId() ==  OPT_GAP_EXTENSION )
	            {
	            	GapExt = atoi(args.OptionArg());
	            }	
	            else if (args.OptionId() ==  OPT_STRICT )
	            {
	            	Strict = true;
	            }
	            else if (args.OptionId() ==  OPT_NOGAP )
	            {
	            	Dyn = false;
	            }
	            else if (args.OptionId() ==   OPT_MAX_MIS )
	            {
	            	MaxMisMatch = atoi(args.OptionArg()); //_AtoI
	            }
	            else if (args.OptionId() ==   OPT_MAX_GAP )
	         	{
	         		MaxGap = atoi(args.OptionArg());
	         	}
	            else if (args.OptionId() ==   OPT_SIZE_MIN )
	            {
	            	SizeMin = atoi(args.OptionArg());
	            }
	            else if (args.OptionId() ==   OPT_SCORE_RATIO_MIN )
	            {
	            	MinRatioOfMaxScore = atof(args.OptionArg());
	            }
	            else if (args.OptionId() ==   OPT_LENGTH_RATIO_MIN )
	            {
	            	MinRatioOfMaxLength = atof(args.OptionArg());
	            }
	            else if (args.OptionId() ==  OPT_STEP )
	          	{
	            	step = atoi(args.OptionArg());
	          	}	           
				else if (args.OptionId() ==  OPT_VERBOSE )
	          	{
	            	Verbose = true;
	          	}
				else if (args.OptionId() ==  OPT_SILENT )
	          	{
	            	Silent = true;
	          	}
	          	//For auto_indexing mode
	          	else if (args.OptionId() == OPT_MAX_COUNT )
				{
					Max_Count = atoi(args.OptionArg());
				}
				else if (args.OptionId() == OPT_MASK)
				{
					msk = (string) args.OptionArg();
				}
#ifdef DEBUGLOG
	            else if (args.OptionId() ==  OPT_LOG_LEVEL)
	            {
	            	log_level = (string) args.OptionArg();
	            }
#endif
	            ;
	        }
	        else {
	            fprintf(stderr,"Invalid argument: %s\n", args.OptionText());
	            fprintf(stderr,"Please use option --help to see the usage\n");
	            exit(1);
	        }
	    }

	     if ( Self )  /* Self Implies Genome */
	     {
	    	 if (args.FileCount()!=1)
	    	 {
	    	 	 fprintf(stderr,"Exactly one filename is required when option Self is activated\n");
	    	 	 fprintf(stderr,"Please use option --help to see the usage\n");
	    	 	 exit(1);
	    	 }
	    	 bank_filename = (string) args.File(0);

	    	 complexity=COMPLEXITY_NONE;
	    	 Pipe = false;
	    	 Genome = true;
	    	 query_filename = bank_filename;
	     }
	     else if (Pipe)
	     {
	    	 if (args.FileCount()!=1)
	    	 {
	    		 fprintf(stderr,"Exactly one filename is required when option Pipe is activated\n");
	    		 fprintf(stderr,"Please use option --help to see the usage\n");
	    		 exit(1);
	    	 }
	    	 bank_filename = (string) args.File(0);
	    	 query_filename = "-";
	     }
	     else if (args.FileCount()!=2)
	     {
	    	 fprintf(stderr,"Exactly two filenames are required for bank and query \n");
	    	 fprintf(stderr,"Please use option --help to see the usage\n");
	    	 exit(1);
	     }
	     else
	     {
		     bank_filename = (string) args.File(0);
		     query_filename = (string) args.File(1);
	     }
		
	 	if ( Genome )
	 	{
	 		complexity=COMPLEXITY_NONE;
	 		OutputStream=true;
	 		Pipe=false;
	 	};

#ifdef DEBUGLOG
	    	__LogParameters();
#endif
	 	Validation();
}

/*
  Name:      Validation
  Parameters: none
  Function:  exit if something wrong with the parameters
*/

void Compare_Parameters::Validation()
{
    bool ok = true;
    if (complexity>2)
    {
        cerr << "ERROR - Complexity should be 0,1,2\n";
        ok = false;
    };
    if (!ok)
        exit(1);
}

/*
  Name:      __CheckingParameters
  Parameters: none
  Function:  prints the available parameters
*/

void Compare_Parameters::__LogParameters()
{
	cerr <<"Ouput format : "<<(int)output_format<<endl;
    cerr <<"Cutoff slide : "<<Cutoff_Slide<<endl;
    cerr <<"Cutoff dyn   : "<<Cutoff_Dyn<<endl;
    cerr <<"Cutoff hsp   : "<<Cutoff_Hsp<<endl;
    cerr <<"Lower level  : "<<(int)lower_level<<endl;
    cerr <<"Deepest level : "<<(int)deepest_level<<endl;
    string outputstream=( results_filename!="" ) ? results_filename : "stdout";
    cerr <<"Ouput file   : "<<outputstream<<endl;
    cerr <<"Follow  : "<< OutputStream <<endl;
    cerr <<"Complexity  : "<< complexity <<endl;
    cerr <<"Self  : " << Self <<endl;
    cerr <<"Genome  : "<< Genome <<endl;
    cerr <<"Pipe  : "<<Pipe<<endl;
    cerr <<"Strict  : "<<Strict<<endl;
    cerr <<"Bank filename  : "<<bank_filename<<endl;
    cerr <<"Query filename  : "<< query_filename<<endl;
    cerr <<"Log level  : "<<log_level<<endl;

}


