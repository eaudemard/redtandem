/*
   $Id: buffer.cpp 1190 2008-01-12 11:19:26Z tfaraut $
*/


#include <errno.h>
#include <iostream>

#include "utils.h"
#include "buffer.h"



/* Member functions of the class Buffer */

/*
 Name:      MemroyArray::__Realloc()
 Usage:     if (__Realloc()) ...
 Function:  If 2 * cur_capacity is still ower than max_capacity, or if there is no max_capacity defined,
            increase by 100% the capacity of the array
            Else, make nothing
 Arg:       none
 Return:    true if success, false if not success (error in realloc, max_capacity reached)
*/

bool Buffer::__Realloc() const
{
    if (begin==NULL)
    {
        cur_capacity = min_capacity;
        begin = malloc(cur_capacity);
        if (begin==NULL)
        {
            SysDie(errno,"Buffer, pb in realloc");
        }
        else
        {
            end = begin;
            //cout << "Buffer::_Realloc - Initial buffer " << cur_capacity << '\n';
            return true;
        }
    }
    else
    {
        size_t wanted_capacity = 2*cur_capacity;
        size_t old_size = size();

        if (max_capacity==0 || wanted_capacity<=max_capacity)
        {
            void* temp = realloc(begin, wanted_capacity);
            if (temp==NULL)
            {
                SysDie(errno,"Buffer, pb in realloc");
            }
            else
            {
                begin = temp;
                end = (void*)((unsigned char*)temp + old_size);
                cur_capacity = wanted_capacity;
                //cout << "Buffer::_Realloc - realloc now capacity " << cur_capacity << "\n";
                return true;
            }
        }
        else
            return false;
    }
	return true; // To avoid a warning !!
}


