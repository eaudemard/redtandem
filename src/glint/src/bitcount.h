/*
   $Id: bitcount.h 263 2009-09-21 16:10:48Z tfaraut $
*/

/* ==========================================================================
   Bit Counting routines

   Author: Gurmeet Singh Manku    (manku@cs.stanford.edu)
   Date:   27 Aug 2002
   ========================================================================== */


#ifndef __BITCOUNT_H__
#define __BITCOUNT_H__

#include "ptypes.h"

int iterated_bitcount (unsigned int n);
void compute_bits_in_16bits (void);

#ifdef PROC64
int precomputed16_bitcount (Uint8 n);
#else
int precomputed16_bitcount (unsigned int n);
#endif

#endif
