/*
   $Id: buffer.h 1424 2008-03-06 09:07:57Z tfaraut $
*/


#ifndef __BUFFER_H__
#define __BUFFER_H__

#include <stdlib.h>
#include <memory>
using namespace std;

#include "ptypes.h"

/*
     Buffer is a low-level buffer, used for storing sequences, binary indexes, etc.
     You may specify a minimum size (default 500000), a maximal size (default 0, ie no maximal size)for the buffer.
     The functions Putc adjust the buffer size when necessary - and when possible
     IsFull returns true only if the buffer cannot be adjusted any more.
     The memory is allocated by Buffer, using malloc/realloc
     The memory is freed by the destructor

     If Buffer is constructed with 2nd constructor, it is only used to wrap some memory area
     It is then marked as 'full' and does not own the wrapped data, thus:
        -The constructor does not allocate the data area
        -Put does not work
        -Clear does not work as well
        -The destructor does free the buffer
*/

class Buffer: NonCopyable
{
    public:
    Buffer(size_t min=500000,size_t max=0);
    Buffer(void* begin, size_t size);
    ~Buffer() {if (begin!=NULL && owner) free(begin);};
    bool Put(char);
    bool Put(Uint2);
    bool Put(Uint4);
	bool Put(Uint8);
    void Clear() {if(owner) end = begin;};  // Warning ! If  not owner, Clear makes nothing but says nothing
    void* GetBase() const { return begin;};
	 void* GetBegin() const {return begin;};
	 void* GetEnd() const { return end;}
    size_t capacity() const {return cur_capacity;};
    size_t size() const {return (char*)end - (char*)begin;};
    bool IsFull() const {return size()==capacity() && !__Realloc();};   // The array is full if we cannot reallocate memory
    bool IsEmpty() const {return begin==end;};

	 void SetBegin(void *nbegin) {begin=nbegin;};
	 void SetEnd(void *nend) {end=nend;};

    private:
    size_t min_capacity;
    size_t max_capacity;
    mutable size_t cur_capacity;

    mutable void* begin;    // points to the first character of the array
    mutable void* end;      // points AFTER the LAST character of the array

    bool owner;
    bool __Realloc() const;
};

/*
   A Buffer_ptr is a smart pointer, it will be usedful to transfer the responsability of fa_bfr and bi_bfr between
   SBuffer and DataBase_Parameters
*/

typedef auto_ptr<Buffer> Buffer_ptr;

/* The inline functions of the class Buffer
*/

/*
   The constructors:
   1/ When the buffer is created with the (size_t,size_t) constructor, it manages a memory array.
   Thus:
       -The array is allocated when necessary (at first Put call)
       -Put, (thus Realloc), Clear are available

   2/ When the buffer is created with the (void*, void*), it is used to wrap an already allocated memory array.
   Thus:
       -Buffer is not the owner of the data.
       -No Put, No Realloc, No Clear available
       -max_capacity is set to the array size
*/

inline Buffer::Buffer(size_t min,size_t max): min_capacity(min),max_capacity(max),cur_capacity(0),begin(NULL),end(NULL),owner(true){}
inline Buffer::Buffer(void* b, size_t s): min_capacity(0),max_capacity(size()),cur_capacity(size()),begin(b),end((char*)b+s),owner(false){}

/* The Put functions:
   Performance trick: write the most often used instruction just after the if
   Return true if pushed, false if capacity problem
*/

inline bool Buffer::Put(char c)
{
    if (size()+sizeof(char) <= cur_capacity)
    {
        char* tmp = (char*)end;
        *(tmp++)=c;
        end = (void*)tmp;
        return true;
    }
    else
    {
        if (__Realloc())
        {
            char* tmp = (char*)end;
            *(tmp++)=c;
            end = (void*)tmp;
            return true;
        }
        else
            return false;
    }
}

inline bool Buffer::Put(Uint2 i)
{
    if (size()+sizeof(Uint2) <= cur_capacity)
    {
        Uint2* tmp = (Uint2*)end;
        *(tmp++)=i;
        end = (void*)tmp;
        return true;
    }
    else
    {
        if (__Realloc())
        {
            Uint2* tmp = (Uint2*)end;
            *(tmp++)=i;
            end = (void*)tmp;
            return true;
        }
        else
            return false;
    }
}

inline bool Buffer::Put(Uint4 i)
{
    if (size()+sizeof(Uint4) <= cur_capacity)
    {
        Uint4* tmp = (Uint4*)end;
        *(tmp++)=i;
        end = (void*)tmp;
        return true;
    }
    else
    {
        if (__Realloc())
        {
            Uint4* tmp = (Uint4*)end;
            *(tmp++)=i;
            end = (void*)tmp;
            return true;
        }
        else
            return false;
    }
}

inline bool Buffer::Put(Uint8 i)
{
    if (size()+sizeof(Uint8) <= cur_capacity)
    {
        Uint8* tmp = (Uint8*)end;
        *(tmp++)=i;
        end = (void*)tmp;
        return true;
    }
    else
    {
        if (__Realloc())
        {
            Uint8* tmp = (Uint8*)end;
            *(tmp++)=i;
            end = (void*)tmp;
            return true;
        }
        else
            return false;
    }
}

#endif
