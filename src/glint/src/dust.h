/*
   $Id: dust.h 230 2008-09-12 13:16:00Z tfaraut $
*/


/* dust.h */

#ifndef _DUST_H
#define _DUST_H

#define TRUE   1
#define FALSE  0

void set_dust_level(int );
void set_dust_window(int );
void set_dust_word(int );

void __wo1(int ,unsigned char * ,int );
int __wo(int , unsigned char *, int *, int *);
void dust(int len,unsigned char *s);
void init_zeros(void);
int __code(char );


#endif
