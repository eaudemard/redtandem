/*
   $Id: alignment.cpp 266 2010-02-16 08:44:32Z tfaraut $
*/


/* alignment.cpp  */

#include <ctype.h>
#include "utils.h"
#include "alignment.h"
#include "sequence_io.h"

Uint2 I;
Uint2 J;

Uint1 **Trace;
Uint1 **Trace_E;
Uint1 **Trace_F;

extern RealLogger* logger;
bool Debug_here;

/*
      Some utility functions:

		Functions defined for the purpose of dynamic programming and more specifically for the traceback (which cell realized the maximum)

*/

inline int max3(int a, int b, int c)
{
	int max;

	if ( a > b ) {
		max = a;
		Trace[I][J]=DIAG;
	} else {
		max=b;
		Trace[I][J]=LEFT;
	}
	if ( max < c ) {
		max=c;
		Trace[I][J]=ABOVE;
	}

	return max;
}

inline int max2_E(int a, int b)
{
	int max;

	if ( a > b ) {
		max = a;
		Trace_E[I][J]=OPEN_Q;
	} else {
		max=b;
		Trace_E[I][J]=CONT_GQ;
	}

	return max;
}

inline int max2_F(int a, int b)
{
	int max;

	if ( a > b ) {
		max = a;
		Trace_F[I][J]=OPEN_S;
	} else {
		max=b;
		Trace_F[I][J]=CONT_GS;
	}

	return max;
}

/*

     The methods from the AlignEngine class

*/

/*
     Constructors and other initialisation functions
*/

AlignEngine::AlignEngine(int size, const QBuffer& q):
	align_size_max(0),bestScore(0),
	scoring(q.compare.Match,q.compare.MisMatch,q.compare.NMatch,q.compare.GapOp,q.compare.GapExt,q.compare.UngapDropoff,q.compare.GapDropoff),
	align_direction(*this),query(q)
{
	max_seq_size = size;
	
	_InitScoreMatrix();
	_InitTraceMatrix();
	Trace=T;
	Trace_E=TE;
	Trace_F=TF;

	Rev=(int*)malloc(256*sizeof(int));
	for (int i=0;i<256;i++) {
	   Rev[i]=i;
	}
	Rev['A']='T';
	Rev['T']='A';
	Rev['C']='G';
	Rev['G']='C';
	Rev['a']='t';
	Rev['t']='a';
	Rev['c']='g';
	Rev['g']='c';

	Rev['N']='N';

	if (q.compare.Strict) scoring.SetSevereScoringSheme();

}

void AlignEngine::_InitScoreMatrix()
{
	Uint4 size = max_seq_size;
	
	S=(Int2**)malloc((size+1)*sizeof(int*));
	for (int i=0;i<=size;i++) {
		S[i]=(Int2*)calloc((size+1),sizeof(int));
	}

	for (int i=1;i<=size;i++) {
		S[i][0]=scoring.alpha+(i-1)*scoring.beta;
		S[0][i]=scoring.alpha+(i-1)*scoring.beta;
	}
	S[0][0]=0;

	F=(Int2*)malloc((size+1)*sizeof(Int2));
}

void AlignEngine::_InitTraceMatrix()
{
	Uint4 size = max_seq_size;
	
	T=(Uint1**)malloc((size+1)*sizeof(Uint1*));
	TE=(Uint1**)malloc((size+1)*sizeof(Uint1*));
	TF=(Uint1**)malloc((size+1)*sizeof(Uint1*));
	for (int i=0;i<=size;i++) {
		T[i]=(Uint1*)calloc((size+1),sizeof(Uint1));
		TE[i]=(Uint1*)calloc((size+1),sizeof(Uint1));
		TF[i]=(Uint1*)calloc((size+1),sizeof(Uint1));

		T[0][i]=LEFT;
		TE[0][i]=CONT_GQ;
		T[i][0]=ABOVE;
		TF[i][0]=CONT_GS;
	}

	TE[0][1]=OPEN_Q;
	TF[1][0]=OPEN_S;

	TF[0][0]=DONE;
	TE[0][0]=DONE;
	T[0][0]=DONE;
}

/*  DEPRECATED
void AlignEngine::_ResetTraceMatrix( )
{
	for (int i=0;i<= max_seq_size ;i++) {
		T[0][i]=LEFT;
		TE[0][i]=CONT_GQ;
		T[i][0]=ABOVE;
		TF[i][0]=CONT_GS;
	}

	TE[0][1]=OPEN_Q;
	TF[1][0]=OPEN_S;

	TF[0][0]=DONE;
	TE[0][0]=DONE;
	T[0][0]=DONE;
	
	Trace=T;
	Trace_E=TE;
	Trace_F=TF;	
}
*/

/*
    GetHsp: Fill the hsp passed by argument, using the data found in AlignEngine
            max_diago, the largest diagonal implied in the alignment, is returned                                (c'est quoi ca THOMAS ? )
*/

DiagInt AlignEngine::ToHsp(hsp & h)
{
    h.q_start=query_start;
    h.q_end=query_end;
    h.size=h.q_end-h.q_start+1+h.q_gap.GetSize();

    h.s_start=subject_start;
    h.s_end=subject_end;
    h.score=score_align;

#ifdef DEBUGLOG
   logger->SetLogType("A") << "Storing hsp h : ["<<h.q_start <<"-"<<h.q_end<<"]->["<<h.s_start<<"-"<<h.s_end<<"]\n";
#endif

	 /*      Display align_results. and alignment      */

    DiagInt diago1=(strand >0) ? (long) subject_start-query_start : subject_start+query_end-query.database.GetMsk().GetLength()+1;
    DiagInt diago2=(strand >0) ? (long) subject_end-query_end : subject_end+query_start-query.database.GetMsk().GetLength()+1;

    DiagInt max_diago;

    if (diago1>diago2) {
        h.min_diag=diago2-h.s_gap.GetSize();
        h.max_diag=diago1+h.q_gap.GetSize();
        max_diago= diago1;
    } else {
        h.min_diag=diago1-h.s_gap.GetSize();
        h.max_diag=diago2+h.q_gap.GetSize();
        max_diago= diago2;
    }

	//Getting the actual sequence using information from the .he file
    _GetSeqInfo(h);

    //Recording the hsp shortest edit script
    _RecordHspScript(h);
    
    return max_diago;
}

/*
 Name:      _RecordHspScript
 Usage:     cammed by _DisplayHspInfo
 Function:  Records the smallest script for describing the hsp
 Arg:       an hsp pointer
 Return:    the size of the alignment
*/
Uint2 AlignEngine::_RecordHspScript(hsp& h) const
{
	Uint4 l=0;
	const unsigned char *nQuery=query.GetSeq()+h.q_start;
	const unsigned char *nBase=(h.strand>0) ? query.ctree.GetSeq()+h.s_start : query.ctree.GetSeq()+h.s_end;
	char base;
	AlignStatus align_status=ALIGNED;

	h.q_gap.Sort();
	h.s_gap.Sort();
    GapPile_const_iterator qgap = h.q_gap.begin();
    GapPile_const_iterator sgap = h.s_gap.begin();

	while (  l < h.size ) {
		if (qgap!=h.q_gap.end() && *qgap==l) {
			if (align_status == ALIGNED)
			{
				h.gap_opening.Push(l);
				align_status = GAPED;
			}
			nBase+=h.strand;
			qgap++;

		} else if (sgap!=h.s_gap.end() && *sgap==l) {
			if (align_status == ALIGNED)
			{
				h.gap_opening.Push(l);
				align_status = GAPED;
			}
			nQuery++;
			sgap++;
		} else {
			base=(h.strand>0) ? *nBase : Rev[*nBase];
			if (toupper(*nQuery)!=toupper(base)) h.mismatch.Push(l);
			nBase+=h.strand;
			nQuery++;
			align_status = ALIGNED;
		}
		l++;
	}

	return h.size;
}


/*
 Name:      AlignEngine::_GetSeqInfo(hsp & h)
 Usage:     _GetSeqInfo(h);
 Function:  The sequences from .fa file being different from the true sequence,
            names and coordinates have to be changed accordingly
 Arg:       a reference on a hsp
 Return:    none
*/
void AlignEngine::_GetSeqInfo(hsp & h)
{
	h.true_seq=query.database.GetTrueSeqPos(h.s_start);
	if (query.compare.Self)
	{
		h.true_seq_query=query.database.GetTrueSeqPos(h.q_start);
		//if ((h.q_end==h.s_end || h.q_start==h.s_start) && (h.strand>0)) { Why h.strand>0, this doesn't seem mandatory !!
		if (h.q_end==h.s_end || h.q_start==h.s_start) {
				//fprintf(stderr,"Warning Here main diagonal [%u-%u]-->[%u-%u]\n",h.q_start,h.q_end,h.s_start,h.s_end);
		}
	}
	else
	{
		h.true_seq_query.Name=(char*)query.GetSeqId().c_str();
		h.true_seq_query.start=0;
	}
}

bool AlignEngine::ValidHsp(hsp& h)
{
	bool IsValid = true;

	if ( query.compare.MaxMisMatch >=0 && h.mismatch.GetSize()>query.compare.MaxMisMatch )
		IsValid = false;
	if ( query.compare.SizeMin >=0 && h.size < query.compare.SizeMin )
		IsValid = false;
	if ( query.compare.MaxGap >=0 && (h.q_gap.GetSize()+h.s_gap.GetSize())>query.compare.MaxGap )
		IsValid = false;
	if ( query.compare.MinRatioOfMaxScore >=0 &&  h.score < (query.compare.MinRatioOfMaxScore * query.MaxScore( scoring.Match() )))
		IsValid = false;
	if ( query.compare.MinRatioOfMaxLength >=0 &&  h.size < (query.compare.MinRatioOfMaxLength * query.GetLength()))
			IsValid = false;

	return IsValid;
}

void AlignDirection::SetDirection(int align_move, int strand)
{
	int move=align_move*strand;

	this->move=align_move;

	this->query_start=(align_move>0) ? results.query_end+1 : results.query_start-1;

	if (move>0) {
		this->subject_start=results.subject_end+move;
	} else {
		this->subject_start=results.subject_start+move;
	}

	this->query_move=move;
	this->subject_move=move;
	this->strand=strand;

#ifdef DEBUGLOG
	logger->SetLogType("AA") << "q_start : "<<this->query_start<<" s_start : "<<this->subject_start;
	logger->SetLogType("AA") <<" q_move : "<<(int)this->query_move<<" s_move : "<<(int)this->subject_move <<" strand : "<<strand<<"\n";
	//fprintf(stdout,"%u %u %d %d %d \n",this->query_start,this->subject_start,this->query_move,this->subject_move,strand);
#endif

}

Int2 AlignEngine::LocalAlign(hsp* h)
{
	Int2 *sdiag, *scurrent, *sleft, *sabove, *fabove;
	const unsigned char *nQuery, *nBase;
	int i=0;
	int min_j=1, max_j=0;
	int size_max;
	int max_align_score=0;
	int dropoff;
	int query_move=align_direction.query_move*align_direction.strand;
	int E;
	Uint8 Sequence_Length;

	Debug_here = 0;

	scoring.score=(align_direction.strand>0) ? scoring.score_fwd : scoring.score_bwd;

	this->bestScore=0;
	this->bestSi=0;
	this->bestSj=0;
	dropoff=this->bestScore-scoring.X2;

	sdiag=this->S[0];
	sleft=this->S[1];
	sabove=sdiag+1;
	scurrent=sleft+1;

	//_ResetTraceMatrix();
	
	// The actual sequence begins at position 1 in the array just as I below so we have to add an extra 1
	// so that nQuery=align_direction.query_start+(I-1) can visit first and last nucleotides
	size_max=(query_move>0) ? query.GetLength()-align_direction.query_start + 2 : align_direction.query_start + 1;

	i=0;
	while(this->S[0][i++]> dropoff-1) max_j++;

	for (i=min_j;i<=MAX_SEQ_SIZE;i++) {
		this->F[i]=N_INFTY;
	}

	nQuery=query.GetSeq()+align_direction.query_start;
	I=1;

	Sequence_Length=(align_direction.subject_move>0) ? query.database.GetLength()-align_direction.subject_start : align_direction.subject_start;
	if ( Sequence_Length > MAX_SEQ_SIZE ) Sequence_Length=MAX_SEQ_SIZE;
	if ( size_max > MAX_SEQ_SIZE ) size_max=MAX_SEQ_SIZE;

	  while (max_align_score > dropoff && I<size_max)  {

		nBase=query.ctree.GetSeq()+align_direction.subject_start+align_direction.subject_move*(min_j-1);

		E=N_INFTY;
		fabove=&this->F[min_j];
		max_align_score=dropoff-1;
		Uint8 q_pos = align_direction.query_start+(I-1)*query_move;
		J=min_j;
		do
		{				
			if ( *nBase == SEQEND ) // SEQEND ($) is never included in an alignment
			{
				max_j = J-1;				
			}
			
			if ( J > max_j ) *sabove=N_INFTY;

			E=max2_E(*sleft+scoring.alpha,E+scoring.beta);					
			*fabove=max2_F(*sabove+scoring.alpha,*fabove+scoring.beta);
			*scurrent=max3(*sdiag+scoring.score[*nQuery][*nBase],E,*fabove);
								
			if (*scurrent < dropoff) {
				*scurrent=N_INFTY;
			} else if (*scurrent >= this->bestScore) {
				this->bestScore=*scurrent;
				this->bestSi=I;
				this->bestSj=J;
				max_align_score=this->bestScore;
				dropoff=	this->bestScore-scoring.X2;
			} else if (*scurrent > max_align_score) {
				max_align_score=*scurrent;
			}
			nBase+=align_direction.subject_move;
			J++,sdiag++,sabove++,sleft++,scurrent++,fabove++;
		} while ( (*(scurrent-1) > dropoff || J <= max_j) && J < Sequence_Length);

#ifdef DEBUGLOG		
		int min_i = ( I-20 > 0 ) ? I-20 : 0;  
		show_score_matrix(min_i,I,min_j,max_j,stdout);
#endif
		
			max_j=(J<Sequence_Length) ?  J : Sequence_Length;

			E=max2_E(*sleft+scoring.alpha,E+scoring.beta);
			this->F[max_j] =	N_INFTY;
			*scurrent = max3(*sdiag+scoring.score[*nQuery][*nBase],E,*fabove);
			if (*scurrent < dropoff) *scurrent=N_INFTY;

		if (this->S[I][min_j] < dropoff) {
			this->S[I+1][min_j] = N_INFTY;
			min_j++;
		}

		if (min_j>1) {
			this->S[I+1][min_j-1] = N_INFTY;
		}
		sdiag=this->S[I]+min_j-1;
		sleft=this->S[I+1]+min_j-1;
		sabove=sdiag+1;
		scurrent=sleft+1;
		
		//if (query.compare.Self &&	align_direction.strand>0) { Why align_direction.strand>0 see above !!
		if ( query.compare.Self )
		{
			Uint4 BestScoreSubjectIndex = align_direction.subject_start + align_direction.subject_move * (this->bestSj-1);	
			Uint4 BestScoreQueryIndex   = align_direction.query_start + query_move * (this->bestSi-1);

			if ( BestScoreQueryIndex == BestScoreSubjectIndex ) //Here the main diagonal
			{
				max_align_score=N_INFTY;
			}
			
			Uint4 MinSubject = ( align_direction.subject_move > 0 ) ? min_j - 1 : 1 - max_j; 		
			MinSubject += align_direction.subject_start;

			Uint4 MaxQuery = ( query_move > 0 ) ? I -1 : 0;
			MaxQuery += align_direction.query_start	;

			if ( MinSubject - 2 <= this->query_end || MaxQuery + 2 >= this->subject_start ) //Overlapping alignment +1 and -1 in order to be strict (not even a single nucleotide overlap)
			{			
				//printf("Overlapping !! : %d < %d %d > %d\n", BestScoreSubjectIndex,this->query_end,BestScoreQueryIndex, this->subject_start );
				max_align_score=N_INFTY;	
			}
	
		}

		nQuery+=query_move;
		I++;
	}
		   		    
	if (this->bestScore>0) {
		if (align_direction.subject_move>0) {
			this->subject_end=align_direction.subject_start+this->bestSj-1;
		} else {
			this->subject_start=align_direction.subject_start-this->bestSj+1;
		}
		if (query_move>0) {
			this->query_end=align_direction.query_start+this->bestSi-1;
		} else {
			this->query_start=align_direction.query_start-this->bestSi+1;
		}

		this->size=RecordAlignment(h);
		this->score_align+=this->bestScore;
		
		if ( max_j == MAX_SEQ_SIZE ) {

			align_direction.SetDirection(align_direction.move,align_direction.strand);
			this->bestScore+=LocalAlign(h);

		}
	}

	return this->score_align;
}

/*

     Trying to align on the right side, not allowing gaps, using a dropoff principle (continue while the score does not dropp X away from the current best score).
	  																												THOMAS
*/
int AlignEngine::LookRight(match **m,Uint4 i, Uint4 j,int strand)
{
	Int2 score=0;
    int X = scoring.X1;

	this->bestScore=0;

	if ( query.compare.Self )
	{
		Uint4 start_i = i;
		Uint4 start_j = j;		
	
		if ( strand > 0 ) 
		{
			this->bestSi_right=i;
			this->bestSj_right=j;

			while(score>this->bestScore-X) 
			{
				if ( i >= start_j - 1 ) //Stop before the overlap
				{
					score = -10000;			
				}
				if (score>=this->bestScore) {
					this->bestScore=score;
					this->bestSi_right=i-1;
					this->bestSj_right=j-1;
				}
				score+=scoring.score_fwd_severe[query.Getc(i++)][query.ctree.Getc(j++)];
			}
			this->query_start=0;
			this->query_end=this->bestSi_right;
			this->subject_start=0;
			this->subject_end=this->bestSj_right;
		} 
		else 
		{
			i+=query.database.GetMsk().GetLength()-1;
			this->bestSi_left=i;
			this->bestSj_right=j;

			while(score>this->bestScore-X) {
				if (score>=this->bestScore) {
					this->bestScore=score;
					this->bestSi_left=i+1;
					this->bestSj_right=j-1;
				}
				score+=scoring.score_bwd_severe[query.Getc(i--)][query.ctree.Getc(j++)];
			}
			this->query_start=this->bestSi_left;
			this->query_end=0;
			this->subject_start=0;
			this->subject_end=this->bestSj_right;
		}
	} 
	else
	{
		if ( strand > 0 ) 
		{
			this->bestSi_right=i;
			this->bestSj_right=j;

			while(score>this->bestScore-X) 
			{
				if (score>=this->bestScore) {
					this->bestScore=score;
					this->bestSi_right=i-1;
					this->bestSj_right=j-1;
				}
				score+=scoring.score_fwd_severe[query.Getc(i++)][query.ctree.Getc(j++)];
			}
			this->query_start=0;
			this->query_end=this->bestSi_right;
			this->subject_start=0;
			this->subject_end=this->bestSj_right;
		} 
		else 
		{
			i+=query.database.GetMsk().GetLength()-1;
			this->bestSi_left=i;
			this->bestSj_right=j;

			while(score>this->bestScore-X) 
			{
				if (score>=this->bestScore) 
				{
					this->bestScore=score;
					this->bestSi_left=i+1;
					this->bestSj_right=j-1;
				}
				score+=scoring.score_bwd_severe[query.Getc(i--)][query.ctree.Getc(j++)];
			}
			this->query_start=this->bestSi_left;
			this->query_end=0;
			this->subject_start=0;
			this->subject_end=this->bestSj_right;
		}
	}

	return this->bestScore;
}

/*

 Trying to align on the left side, not allowing gaps, using a dropoff principle.
     THOMAS

*/

int AlignEngine::LookLeft(match **m,Uint4 i, Uint4 j,int strand)
{
	int score=0;
	int X = scoring.X1;

	this->bestScore=0;

	if ( query.compare.Self )
	{
		Uint4 start_i = i;
		Uint4 start_j = j;

		if ( strand > 0 ) 
			{
				this->bestSi_left=i;
				this->bestSj_left=j;
				while(score>this->bestScore-X) 
				{
					if ( j <= this->query_end + 1 )//  /Stop before the overlap
					{
						score = -10000;			
					}
					if (score >= this->bestScore) 
					{
						this->bestScore=score;
						this->bestSi_left=i+1;
						this->bestSj_left=j+1;
					}
				score+=scoring.score_fwd_severe[query.Getc(i--)][query.ctree.Getc(j--)];
				}
				this->query_start=this->bestSi_left;
				this->subject_start=this->bestSj_left;
			} 
			else 
			{
				i+=query.database.GetMsk().GetLength()-1;
				this->bestSi_right=i;
				this->bestSj_left=j;
				while(score>this->bestScore-X) 
				{
					if (score>=this->bestScore) 
					{
						this->bestScore=score;
						this->bestSi_right=i-1;
						this->bestSj_left=j+1;
					}
				score+=scoring.score_bwd_severe[query.Getc(i++)][query.ctree.Getc(j--)];
				}
				this->query_end=this->bestSi_right;
				this->subject_start=this->bestSj_left;
			}

	
	}
	else
	{
		if ( strand > 0 ) 
		{
			this->bestSi_left=i;
			this->bestSj_left=j;
			while(score>this->bestScore-X) 
			{
				if (score>=this->bestScore) 
				{
					this->bestScore=score;
					this->bestSi_left=i+1;
					this->bestSj_left=j+1;
				}
			score+=scoring.score_fwd_severe[query.Getc(i--)][query.ctree.Getc(j--)];
			}
			this->query_start=this->bestSi_left;
			this->subject_start=this->bestSj_left;
		} 
		else 
		{
			i+=query.database.GetMsk().GetLength()-1;
			this->bestSi_right=i;
			this->bestSj_left=j;
			while(score>this->bestScore-X) 
			{
				if (score>=this->bestScore) 
				{
					this->bestScore=score;
					this->bestSi_right=i-1;
					this->bestSj_left=j+1;
				}
			score+=scoring.score_bwd_severe[query.Getc(i++)][query.ctree.Getc(j--)];
			}
			this->query_end=this->bestSi_right;
			this->subject_start=this->bestSj_left;
		}
	}

	return this->bestScore;
}

/*

     The dynamic programming allows to compute the best score. Here the traceback is undertaken in order to recover the (one of the) alignemnt exhibiting the best score.

*/


int AlignEngine::RecordAlignment(hsp* h)
{
	int i=this->bestSi;
	int j=this->bestSj;
	
	int query_move=align_direction.query_move*align_direction.strand;
	int orient=	query_move;
	
	int current_hsp_size = this->size;
	
	int k = 0;				// Relative position of the gaps starting at the alignment end
	int move;                   // The traceback atomic mouvement
	Uint1 **t;					// A pointer on the trace matrices
	
	GapPile q_gap;      // Local query and subject gap piles
	GapPile s_gap;
	
	t=Trace;
	move=t[i][j];
	
	while(move!=DONE) {
		switch(move) {
			case DIAG	: 	  /* Nothing to do */
							  i--; j--;  /* TODO should we check the mismatch here ? */
					 		  break;
			case ABOVE	:     s_gap.Push( k );
							  t=Trace_F;
				 			  break;
			case OPEN_S:      k--;
							  t=Trace;
							  i--;
				 			  break;
			case CONT_GS:     s_gap.Push( k );
							  i--;
				 			  break;
			case LEFT	:     q_gap.Push( k );
							  t=Trace_E;
				 			  break;
			case OPEN_Q:  	  k--;
							  t=Trace;
							  j--;
				 			  break;
			case CONT_GQ:     q_gap.Push( k );
							  j--;
				 			  break;
			default    : 	  fprintf(stderr,"It looks like there is a problem in here %d %d\n",i,j);
							  exit(1);
		}
		k++;
		move=t[i][j];
	}
	
	//k is now the size of the alignment
	int align_size = k;
	
	// We have now to correct the gap positions
	// Reporting the absolute positions of gaps
	if ( orient < 0 ) {
		
		// Previous reported gaps have to be incremented first by the size of the current local alignment
		h->s_gap.Increment( align_size );
		h->q_gap.Increment( align_size );
		
		for(Uint2 i=0;i<q_gap.GetSize();i++) h->q_gap.Push( q_gap.gaps[i] );
		for(Uint2 i=0;i<s_gap.GetSize();i++) h->s_gap.Push( s_gap.gaps[i] );	
		
	}
	else {
		// Newly reported gaps have to be adjusted according to the starting point of the alignment and its size
		
		int offset = current_hsp_size + align_size + -1;
		
		for(Uint2 i=0;i<q_gap.GetSize();i++) h->q_gap.Push( offset - q_gap.gaps[i] );
		for(Uint2 i=0;i<s_gap.GetSize();i++) h->s_gap.Push( offset - s_gap.gaps[i] );		
	}

	int new_size = align_size + current_hsp_size;
	return new_size;
}

#ifdef OLDSTUFF
int AlignEngine::RecordAlignment(hsp* h, int start)
{
	int i=this->bestSi;
	int j=this->bestSj;
	int query_move=align_direction.query_move*align_direction.strand;
	int orient=	query_move;
	int max_i_j = ( i > j ) ? i : j;
	int current_size = this->size;
	int new_size = max_i_j + start;    
	
	int l;
	int move;
	Uint1 **t;
	
	// Previous reported gaps have to be incremented
	if (orient<0) {	
		h->s_gap.Increment(l);
        h->q_gap.Increment(l);
	};

	cerr<<"Here  max_i_j="<<max_i_j<<" (i,j)="<<i<<","<<j<<"  start="<<start<<endl;
		
	l= ( orient > 0 ) ? new_size -1 : start - current_size; //l is the size of the alignment

	cerr<<"Starting with l="<<l<<" start "<<start<<" and with size "<<this->size<<" orient "<<orient<<endl;
	
	t=Trace;

	move=t[i][j];
	while(move!=DONE) {
		switch(move) {
			case DIAG	: 	  /* Nothing to do */
							  i--; j--;  /* TODO should we check the mismatch here ? */
					 		  break;
			case ABOVE	:     h->s_gap.Push(l);
							  cerr<<"Here above "<<l<<" and "<<i<<endl;
							  /*this->subject_gap++;
							  */
							  t=Trace_F;
				 			  break;
			case OPEN_S:      l+=orient;
							  t=Trace;
							  i--;
				 			  break;
			case CONT_GS:     h->s_gap.Push(l);
							  /*this->subject_gap++;
							  */
							  i--;
				 			  break;
			case LEFT	:     h->q_gap.Push(l);
							  cerr<<"Here left "<<l<<" and "<<i<<endl;
							  /*this->query_gap++;
							  */
							  t=Trace_E;
				 			  break;
			case OPEN_Q:  	  l+=orient;
							  t=Trace;
							  j--;
				 			  break;
			case CONT_GQ:     h->q_gap.Push(l);
							  /*this->query_gap++;
							  */
							  j--;
				 			  break;
			default    : 	  fprintf(stderr,"It looks like there is a problem in here %d %d\n",i,j);
							  exit(1);
		}
		cerr<<"l="<<l<<" (i,j)="<<i<<","<<j<<endl;
		l-=orient;
		move=t[i][j];
	}

	this->size=new_size;

	return new_size;
}
#endif


/* The static members */

list<hit> HitPile::reserve;   // THOMAS Who is really using this stuff !!

//#ifdef DEBUGLOG
void AlignEngine::show_score_matrix(int l_inf, int l_max, int c_inf, int c_max, FILE *pf)
{
	int i,j;
	char d;
	int query_move=align_direction.query_move*align_direction.strand;

	fprintf(pf,"   \t   \t");
	for (j=c_inf;j<=c_max;j++) {
		fprintf(pf,"%3d\t",j);
	}
	fprintf(pf,"\n");

	for (j=c_inf;j<=c_max;j++) {
		if (j==0) {
			fprintf(pf,"   \t - \t");
		} else if (j==c_inf) {
			fprintf(pf,"   \t   \t");
		}
		fprintf(pf,"%3c\t",*(query.ctree.GetSeq()+align_direction.subject_start+align_direction.subject_move*(j-1)));
	}
	fprintf(pf,"\n");

	for (i=l_inf;i<=l_max;i++) {
		fprintf(pf,"%3d\t",i);
		d=(i==0) ? '-' : *(query.GetSeq()+align_direction.query_start+query_move*(i-1));
		fprintf(pf,"%3c\t",d);
		for (j=c_inf;j<=c_max;j++) {
			fprintf(pf,"%3d\t",this->S[i][j]);
		}
		fprintf(pf,"\n");
	}
	fprintf(pf,"\n");
}
//#endif


