/*
   $Id: memory.cpp 230 2008-09-12 13:16:00Z tfaraut $
*/


/* memory.c */

/*===========================================================================
                          memory  -  description
                             -------------------
    begin				: septembre 2005
	 objet				: To assist the debugging of the dynamic allocation

=============================================================================*/

#include "memory.h"
#include "utils.h"
#include <errno.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/mman.h>
#include <limits>
using namespace std;

static double MEM;
/*static double TOTAL_MEM;
static unsigned long MEGA=1<<20;*/

void* memory_survey(int taille,char* message)
{

	void *p;

	MEM+=taille;

	if ((p=(void*)malloc(taille))==NULL) {
		fprintf(stderr,"--------------------------------------------- ERROR  ------------------  Attention, probl�me de m�moire !\n");
		/*fprintf(stdout,"memoire allouee %d: (message: %s)\n",MEM*8,message);
		*/
		return NULL;
	};
	/*fprintf(stdout,"Instantiation de %s memoire+%d=%foctets\n",message,taille,MEM);
	fprintf(stdout,"Instantiation de %s memoire+%d=%fMoctets\n",message,taille,MEM/MEGA);
	*/

	return p;

}

void* MemoryPool::Malloc(size_t sze)
{
    if (pool_top[current_pool]+sze >= pool[current_pool] + pool_size)        // We must change our pool to alloc more data
    {
        current_pool++;
        //void* base = memory_survey(pool_size,"");
        void* base = malloc(pool_size);
        if (base==NULL)
        {
            SysDie(errno,"Cannot allocate memory for the memory pool");
        };
        mem_alloc += pool_size;
        pool.push_back((Uint1*)base);
        pool_top.push_back((Uint1*)base);
    };
    Uint1* rvl = pool_top[current_pool];
    pool_top[current_pool] += sze;
    mem_used += sze;
    return (void*)rvl;
}

void MemoryPool::FreeAll()
{
    while (current_pool>0) {
        free(pool[current_pool--]);
        pool.pop_back();
        pool_top.pop_back();
    }
    pool_top[current_pool]=pool[current_pool];
    mem_used = 0;
    mem_alloc = pool_size;
}


#ifdef BLABLA
/*

Function: Maxsize
Parameters:
Return value:

*/

size_t MaxSize(int part=0)
{
    size_t max_size;
    numeric_limits<ssize_t> limit_ssize_t;

    const int part_limit = 80;            /* default when using limit to discover the available memory */
    const int part_phys_mem=50;           /* default when using sysconf to discover the available memory */

    struct rlimit memoryuse,datasize;

    int rvl = getrlimit(RLIMIT_DATA,&datasize);
    SysDie(rvl,"MaxSize");
    rvl = getrlimit(RLIMIT_RSS,&memoryuse);
    SysDie(rvl,"MaxSize");

    /* No limit specified: try to know the total physical memory */
    if (datasize.rlim_cur==RLIM_INFINITY && memoryuse.rlim_cur==RLIM_INFINITY)
    {
        if (part==0)
        {
            part = part_phys_mem;
        };
        long page_size   = sysconf(_SC_PAGESIZE);
        long nb_of_pages = sysconf(_SC_PHYS_PAGES);
        long physical_memory = nb_of_pages / 100;
        physical_memory *= page_size;
        if (physical_memory < limit_ssize_t.max()/part)
        {
            physical_memory *= part;
        }
        else
        {
            physical_memory = limit_ssize_t.max();
        }
        max_size = physical_memory;
    }
    else
    {
        if (part==0)
        {
            part = part_limit;
        };
    };

    if (datasize.rlim_cur==RLIM_INFINITY && memoryuse.rlim_cur!=RLIM_INFINITY)  /* Limits for only 1 parameter */
    {
        max_size = memoryuse.rlim_cur/100;

        max_size *= part;
    };
    if (datasize.rlim_cur!=RLIM_INFINITY && memoryuse.rlim_cur==RLIM_INFINITY)
    {
        max_size = datasize.rlim_cur/100;
        max_size *= part;
    };
    if (datasize.rlim_cur!=RLIM_INFINITY && memoryuse.rlim_cur!=RLIM_INFINITY)  /* Limits for both parameters: take the lowest */
    {
        max_size = (datasize.rlim_cur < memoryuse.rlim_cur) ? datasize.rlim_cur/100 : memoryuse.rlim_cur/100;
        max_size *= part;
    };

    /* Now, we ask the system to give us those pages, then we free them */

    void * base = malloc(max_size);
    if (base==0) rvl=-1;
    SysDie(rvl,"MaxSize");
    free(base);
    return max_size;
};

#endif
