/*
   $Id: logger.h 1424 2008-03-06 09:07:57Z tfaraut $
*/


#ifndef __LOGGER_H__
#define __LOGGER_H__

//#include <stdlib.h>
#include <math.h>
#include <string>
#include <map>
#include <stdexcept>
#include <fstream>
#include <sstream>
#include <sys/time.h>
#include <time.h>
using namespace std;


#define LOGGER_OPT_NORMAL 0
#define LOGGER_OPT_QUIET  1
#define LOGGER_OPT_COMPRESSION 2
#define LOGGER_OPT_AUTOFLUSH 4

typedef enum log_format_type {no_stamp,normal,timer,point} log_format_type;


/*

    Classes Logger, SLogger, ILogger
    Logger is a general purpose class to log some events to a log file
    A log_level is passed to the constructor. log_level is typically set at runtime.
    A log_type is passed to the Trace function, used for logging. The log_type is matched agains log_level,
    the message passed to Trace is logged is the match is true.

    log_type, log_level
    -------------------

    There are 2 possibilities for log_type, log_level: they can be char* (SLogger) or int (ILogger)

    1/ char* log_type and log_level

    The message is logged ONLY IF log_type is a substring of log_level

    For example:
          log_level = ABV
          log_type  = A  ==> Logged
                      AV ==> Not Logged
                      B  ==> Logged
                      BV ==> Logged

    2/ int log_type and log_level

    The message is logged ONLY IF log_type is <= log_level

          log_level = 1
          log_type  = 1  ==> logged
                      2  ==> logged
                      3  ==> not logged

     The time stamp:
     ---------------

     #06#12#25#13#16#27#01# is a stamp meaning: Dec 12, 2006 13h16mn27sec, serial number 1 (1st number)
     If several timestamps are computed in less than a second, the serial number is incremented. If more than 99, 00 is displayed,
     showing the stamps are not unique because of an overflow problem

     The timers:
     -----------

     timers can be created and started with the StartTimer function, and stopped with the StopTimer function. When a timer is stopped, a timer stamp
     is generated, formed like this:

     #00#00#00#13#16#27#36# means the elapsed time was 13hours, 16 minutes, 27.36 seconds (resolution = 1/100 s)

     The log line format:
     -------------------

     The stamp, if used, is always computed when Trace is called.
     The format depends on the stamp_type parameter:

     1/ If log_format==normal, the line has the format:

     #06#12#25#13#16#27#01#    _XXX_    message

     2/ If log_format==no_stamp, the line has the format:

     message

     3/ If log_format==timer, the timer parameter must be passed with a started timer, and the line has the format:
     #00#00#00#13#16#27#36#    _XXX_    message

     4/ If log_format==point, the line has the format:
     #..#..#..#..#..#..#..#    _XXX_    message

     If msg DOES NOT END with \n, the object switches to multiline mode, so that the next Trace call
     is done on the same line. this makes possible logging a line in several times.

     USING THE OPERATOR<<
     --------------------

     It is possible to use the operator<< to log something., but you must set the log format and the log type, and eventually the timer,
     BEFORE using the operator<<, with the SetLogFormat, SetTimer and SetLogType mutators:

     logger.SetLogFormat(timer);
     logger.SetTimer("T1");
     logger << "some message i= " << i << "f = " << f << "\n";

     NOTE - There are private variables to keep the format, the timer, the type between the calls to operator<<,
            BUT please note that the Trace function DO NOT use those variables: they MUST BE passed by parameters if you use the Trace function

*/

class Logger {
    public:

    /* The Logger exceptions */
    class DoesNotExist: public exception {
        public:
        DoesNotExist(const string& t) throw():timer(t){};
        ~DoesNotExist() throw() {};
        virtual const char * what() const throw() {
            string err_msg = "Logger object - The timer " + timer + " does not exist";
            return err_msg.c_str();
        };
        private:
        string timer;
    };

    protected:
    /* The constructor: opening the log file - The constructor is protected, so we have some sort of abstract tpye */
    Logger(const string& file_name, const string& log_level, int opt=LOGGER_OPT_NORMAL) throw(exception);

    public:
    ~Logger();
    /* Logging to some other file */
    void SetLogFile(const string&) throw(exception);

    /* Setting the log format and the timer for the operator<< */
    void SetLogFormat(log_format_type fmt);
    void SetTimer(const string &);

    /* The timers: start, stop, reset timers */
    void StartTimer(const string&) throw (DoesNotExist);
    string StopTimer(const string&) throw (DoesNotExist);
    void ResetTimer(const string&) throw (DoesNotExist);


    protected:
    void _Trace(const string& msg, const string& log_type, log_format_type log_format=normal, const string& timer="") throw(DoesNotExist);

    /* Used by the operator<< (in derived classes) */
    log_format_type log_format;
    string current_timer;

    private:

    struct Timer {
        Timer(): running(false),duration(0),start(0) {};
        bool running;       // true = The timer is running, false the timer is stopped
        clock_t duration;   // The accumulated duration (in ticks)
        unsigned long start;  // If running, the start time (in centiseconds from the object birth)
    };

    map<string,Timer> timers;
    string log_file_name;
    ofstream log_file;
    int option;

    /*
       computing the stamps
    */

    string __CalcTimerStamp(unsigned long) throw();
    string __CalcStamp() throw();

    /*
       managing a counter, useful if several lines are logged in 1 s
    */

    struct Counter {
        Counter():cnt(0),tme(0),stamp(""){};
        int cnt;
        time_t tme;
        string stamp;
    };
    static Counter serial;

    /*
       When the object is built, we measure the time of day, useful for the timers
    */

    timeval creation_date;

    /*
       Detect the _Trace calls which do not end with \n
    */

    bool multiline;

};

/*

    ILogger derives from Logger, it implements the log level integer - style

*/

class ILogger: public Logger {
    public:
    ILogger(const string& file_name, int log_lvl, int option=LOGGER_OPT_NORMAL) throw(exception);
    void Trace(const string& msg, int log_type, log_format_type log_format=normal, const string& timer="") throw(DoesNotExist);
    ILogger& SetLogType(int type);

    friend ILogger& operator<< (ILogger&, const string&);

    private:
    int log_level;
    int log_type;
    string __itoa(int) const;
};

/* Calling Trace in a more user-friendly manner */
ILogger& operator<< (ILogger&, const string&);
template<typename T> ILogger& operator<< (ILogger& l, T msg);

/*

    SLogger derives from Logger, it implements the log level string - style

*/

class SLogger: public Logger {
    public:
    SLogger(const string& file_name, const string& log_lvl, int option=LOGGER_OPT_NORMAL) throw(exception);
    void Trace(const string& msg, const string& log_type, log_format_type log_format=normal, const string& timer="") throw(DoesNotExist);
    SLogger& SetLogType(const string& type);

    friend SLogger& operator<< (SLogger&, const string&);

    private:
    string log_level;
    string log_type;
};

/* Calling Trace in a user-friendly manner */
SLogger& operator<< (SLogger&, const string&);
template<typename T> SLogger& operator<< (SLogger& l, T msg);


/*
    TrivialLogger, ITrivialLogger and STrivialLogger have the same interface as SLogger/ILogger, but makes nothing

*/

class TrivialLogger {
    protected:
    TrivialLogger() {};

    public:
    void SetLogFile(const string&) {};
    void SetLogFormat(log_format_type fmt) {};
    void SetTimer(const string &) {};
    void StartTimer(const string&) {};
    string StopTimer(const string&) {return "timer stopped";};
    void ResetTimer(const string&) {};

    protected:
    void _Trace(const string& msg, const string& log_type, log_format_type log_format=normal, const string& timer="") {};
};

class ITrivialLogger: public TrivialLogger {
    public:
    ITrivialLogger(const string& file_name, int log_lvl, int option=LOGGER_OPT_NORMAL) {};
    void Trace(const string& msg, int log_type, log_format_type log_format=normal, const string& timer="") {};
    ITrivialLogger& SetLogType(int type) {return *this;};
    friend ITrivialLogger& operator<< (ITrivialLogger&, const string&);
};

/* Calling Trace in a more user-friendly manner */
inline ITrivialLogger& operator<< (ITrivialLogger& l, const string& s){return l;}
template<typename T> ITrivialLogger& operator<< (ITrivialLogger& l, T msg){return l;}

class STrivialLogger: public TrivialLogger {
    public:
    STrivialLogger(const string& file_name, const string& log_lvl, int option=LOGGER_OPT_NORMAL){};
    void Trace(const string& msg, const string& log_type, log_format_type log_format=normal, const string& timer=""){};
    STrivialLogger& SetLogType(const string& type){ return *this;};
    friend STrivialLogger& operator<< (STrivialLogger&, const string&);
};

/* Calling Trace in a user-friendly manner */
inline STrivialLogger& operator<< (STrivialLogger& l, const string& s){return l;}
template<typename T> STrivialLogger& operator<< (STrivialLogger& l, T msg){ return l;}


/* Template and inline functions */

template<typename T> ILogger& operator<< (ILogger& l, T msg)
{
    ostringstream tmp;
    tmp << msg;
    l << tmp.str();
    return l;
}

template<typename T> SLogger& operator<< (SLogger& l, T msg)
{
    ostringstream tmp;
    tmp << msg;
    l << tmp.str();
    return l;
}


inline void Logger::SetLogFormat(log_format_type fmt) {log_format=fmt;}
inline void Logger::SetTimer(const string & timer) {current_timer=timer;}
inline ILogger& ILogger::SetLogType(int type) {log_type=type;return *this;}
inline SLogger& SLogger::SetLogType(const string& type) {log_type=type;return *this;}

#endif

