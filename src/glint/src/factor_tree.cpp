/*
   $Id: factor_tree.cpp 240 2009-02-18 14:27:15Z tfaraut $
*/


/* factor_tree.c */

/*===========================================================================
                          factor tree  -  description
                             -------------------
    begin				: 24 jui 2002
	 objet				: The multilevel hashing tree for the words according to the mask

=============================================================================*/

using namespace std;
#include <iostream>
#include <deque>

#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include "factor_tree.h"
#include "ptypes.h"
#include "utils.h"     /* PROVISOIRE (pour SysDie) */

numeric_limits<Uint1> limit_Uint1;
numeric_limits<Uint4> limit_Uint4;
numeric_limits<Uint8> limit_Uint8;

extern RealLogger* logger;

template <class T> T * SingleLinkedList<T>::PopBack() {
    if (IsEmpty())
        return NULL;

    T * rvl = first;
    first = first->brother;
    if (first == NULL)
        last = NULL;          // Empty object

    return rvl;
}

template <class T> void SingleLinkedList<T>::PushFront(T * p) {
    if (p==NULL) return;

    p->brother=NULL;        // Pushing only one element, marking the end of the chain
    if (IsEmpty())
    {
        first = p;
    }
    else
    {
        last->brother = p;
    };
    last = p;
    return;
}

template <class T> void SingleLinkedList<T>::PushFrontList(T * p) {
    if (p==NULL) return;

    if (IsEmpty())
    {
        first = p;
        last = p;
    }
    else
    {
        last->brother = p;
    };

    while(p->brother!=NULL)     // Pushing a whole chain, many elements together
    {
        p=p->brother;
    }
    last = p;

    return;
}


/*
 Name:      Tree::Tree (THE CONSTRUCTOR)
 Usage:     Tree t()
 Function:  initialize a Tree
 Arg:       none
            The top-level array is initialized
*/

Tree::Tree(): database(NULL), initialized(false), offset(0), ct_pass(1), inf_code(0) {

}

/*
 Name:      Tree::Initialize (database initialization)
 Usage:     Tree t()
 Function:  initialize a Tree (the end !)
            This function was necessary because Tree is a singleton, it has only a default constructor
 Arg:       none
            The top-level array is initialized
 Error:     If initialized is true, we exit (initializing the object 2 times is an error)
*/

void Tree::Initialize(DataBase_Parameters* d) {
    if (initialized)
        SysDie(0,"Tree is already initialized");
    else
    {
        database = d;
        const Mask& mask =database->GetMsk();

        /* Initializing the number of nodes = depends on the mask */
        Uint1 n = mask.GetLength(ROOT_LEVEL);
        nb_top=1<<2*n;     // 4**n

        /* Initializing the top nodes */
        top = new Node[nb_top];
        for(Uint4 i=0; i<nb_top; i++)
            top[i].hash_code = i;

        /* Initializing the number of passes from database */
        SetNbPass(database->GetNbPass());

        /* Initializing the serialize_shift table (see the DataBase definition) */
        for (int i=0;i<=9;i++) {
            switch(i) {
				case 4:	serialize_shift[i] = new SerializeShift<Uint2>; break;
                case 5:
                case 6:	serialize_shift[i] = new SerializeShift<Uint4>; break;
				case 9:	serialize_shift[i] = new SerializeShift<Uint8>; break;
                default:  serialize_shift[i] = NULL; break;
            }
        }

        /* Initializing the copy of the masque's leaf level */
        leaf_level = mask.GetLeafLevel();
        initialized=true;
    };
}

void Tree::__FreeAll() {
    MemoryPool::Instance()->FreeAll();  // Deleting the nodes alltogether
    Node::Clear();                      // Resetting the memory consumption indicators in Node and Leaf
    Leaf::Clear();
    Pos::Clear();
    for(Uint4 i=0; i<nb_top; i++)         // Top nodes are not deleted, we just reset their sons
        top[i].son = NULL;
}

 /*
 Name:      Tree::SetNbPass
 Usage:     index_tree.SetNbPass(n)
 Procedure: Initialize the data structures for setting the pass numbers
            offset and top_info are reset
 Arg:       n => The number of passes to set
 */

void Tree::SetNbPass(Uint1 n)
{
    nb_pass = n;
    ct_pass = 1;
    lg_pass = nb_top/n;
    inf_code = 0;
    sup_code = lg_pass;
    offset = 0;
    top_info.clear();
    __FreeAll();
}

 /*
 Name:      Tree::NextPass
 Usage:     if (!index_tree.NextPass()==0) ... finished;
 Function:  Increment ct_pass if possible
            Free all memory pools
 Arg:       None
 Return:    If we are already at the last pass, does nothing except returning 0
            If the pass number is correctly incremented, return ct_pass
 */

Uint1 Tree::NextPass()
{
    if (ct_pass!=nb_pass)
    {
        ct_pass++;
        inf_code = sup_code;
        sup_code = (ct_pass==nb_pass) ? nb_top : inf_code + lg_pass;
        __FreeAll();
        return ct_pass;
    }
    else
        return 0;
}

  /*
 Name:      Tree::Phylum
 Usage:     if (index_tree.Phylum(w))...
 Function:  Checks to know if the hash code of the window at level ROOT_LEVEL is in the current interval
 Arg:       w => a window
 Return:    true/false
 */

 bool  Tree::Phylum(const Window &w) const {          // Return true if the word is in the good interval, false if not
     Uint4 code=w.TreeHash(ROOT_LEVEL);
     return (code>=inf_code && code <sup_code);
 }



/*========================================================================
           Inserting a leaf in the tree, from a word included in a window
==========================================================================*/
/*
 Name:      Insert
 Usage:     Leaf* leaf= insert(w);
 Function:  Insert a leaf in the tree, or retrieve the leaf if already inserted
            If the hash_code at the root level is outside the current pass, NULL is returned
 Arg:       w   : The window encapsulating the word to insert
 Return :   The inserted or retrieved leaf, or NULL if the phylum is outside the current interval
*/

Leaf* Tree::Insert(const Window & w)
{
    if (Phylum(w))
    {
        Uint4 code=w.TreeHash(ROOT_LEVEL);
        Node* father = top+code;
        for (Uint1 l=ROOT_LEVEL+1;l<leaf_level;l++)
        {
            code = w.TreeHash(l);
            Node* new_node;
            if (father->son!=NULL)
            {
                Node* strt = (Node*)father->son;
                Node* prev;
                new_node = __HorizNodeSearch(strt,code,&prev);
                if (new_node==NULL)                 // Not found - We must create this node
                {
                    if (prev==NULL)                 // Must be created at front of list
                        new_node = new Node(code,father,1);
                    else
                        new_node = new Node(code,prev);
                };
            }
            else
            {
                new_node = new Node(code,father,1); // New branch created
            }
            father = new_node;                      // for the next iteration
        };

        Leaf* strt = (Leaf*) father->son;
        code = w.TreeHash(leaf_level);
        Leaf* prev;
        Leaf* leaf = __HorizLeafSearch(strt,code,&prev);
        if (leaf == NULL)
        {
            if (prev==NULL)
                leaf = new Leaf(code,father,1);
            else
                leaf = new Leaf(code,prev);
            database->Words++;
        }
        return leaf;
    }
    else
    {
        return NULL;
    }
}

/*========================================================================
           Deleting a leaf of the tree, from a word included in a window
==========================================================================*/
/*
 Name:      Delete
 Usage:     int rvl = Delete(w);
 Function:  Delete a leaf of the tree
            If possible, delete also every intermediate nodes
 Arg:       w   : The window encapsulating the word to delete
 Return :   0 ==> OK, the leaf was deleted
            1 ==> The leaf was not in the tree
 Global:

 Name:      __Delete
 Usage:     int rvl = __Delete(p,w,level)
 Function:  Delete a leaf, starting the path to p's son
            All the intermediate nodes will be deleted if possible
 Arg:       p: The father of the starting node
            w: The window encapsulating the word to delete
            level: The level at which the delete is started
 Return :   0 ==> OK, the leaf was deleted
            1 ==> The leaf was not in the current interval, or not in the tree
*/

int Tree::Delete (const Window& w)
{
	Uint4 code=w.TreeHash(ROOT_LEVEL);
    if (Phylum(w) && top[code].son!=NULL) {    // If there is something in this phylum, search here
		return __Delete(top+code,w,ROOT_LEVEL+1);
	} else {                                // Else, nothing found
        return 1;
	};
}

int Tree::__Delete(Node * father, const Window& w, Uint1 level)
{
    int rvl=0;
    Uint4 code = w.TreeHash(level);
    if (level<leaf_level)              // We have nodes at this level
    {
        Node* p = (Node*)father->son;
        Node* q;
        Node* r = __HorizNodeSearch(p,code,&q);
        if (r!=NULL)                // The node was found (should be)
        {
            rvl = __Delete(r,w,level+1);  // Recursive call for the next level
            if (rvl !=0)
                return rvl;         // An error was found under
            if(r->son==NULL)  // This node does not have any son, we delete it
            {
                if (q!=NULL)
                    q->brother=r->brother;
                else
                    father->son=r->brother;    // May be NULL if last node for this branch
                delete r;
            }
        }
        else
        {
            rvl = 1;
        };
    }
    else
    {
        Leaf* p = (Leaf*)father->son;
        Leaf* q;
        Leaf* r = __HorizLeafSearch(p,code,&q);
        if (r!=NULL)                        // The leaf was found (should be): delete it
        {
            if (q!=NULL)
                q->brother=r->brother;
            else
                father->son=r->brother;
            delete r;
            database->Words--;
        }
        else
        {
            rvl = 1;
        };
    }
    return rvl;
}


/*
 Name:      Leaf::NewPos
 Usage:     Uint1 nb_pos = leaf.NewPos(p)
 Function:  Add a position to the leaf, allocating memory if necessary
 Arg:       new_pos  : The position to add
            min_dist : The minimum distance between the new position and the last position
                       already stored, thus avoiding any overlap
 Return :   The new count
*/

Uint1 Leaf::NewPos(Uint4 new_pos, Uint1 min_dist) {
    if (pos==NULL) {
        pos = new Pos(new_pos);
        nb_pos=1;
    }
    else
    {
        /* Find the last position recorded */
        Pos* last_pos=pos;
        int i;
        for(i=1; i< nb_pos; i++)
            last_pos = last_pos->brother;
        //cerr << "coucou " << i << '\n';
        /* Append the new position only if there is no overlap */
        if (new_pos-last_pos->position > min_dist)
        {
            last_pos->brother = new Pos(new_pos);
            nb_pos++;
            logger->SetLogType("TV") << "Adding a new position to the tree - pos= " << new_pos << " nb of pos= " << pos << "\n";
        }
        else
        {
            logger->SetLogType("T") << "Overlapping here: " << last_pos->position << "...." <<  new_pos << " (min " << (int) min_dist << ")\n";
        }
    };

	assert(nb_pos<=limit_Uint1.max());

    return nb_pos;
}


/*========================================================================
           Searching a leaf in the tree, from a word included in a window
==========================================================================*/
/*
 Name:      Search
 Usage:     Leaf* leaf= Search(w);
 Function:  Insert a leaf in the tree
            Calls _insertion_directe, using the TreeHash of the word at root level to know the phylum
 Arg:       w   : The window encapsulating the word to search
 Return :   The leaf of the searched word, or NULL if not found (may be not in the current interval)
 Global:

 Name:      __Search
 Usage:     Leaf* leaf= __Search(p,w,level)
 Function:  Search a node or a leaf corresponding to a word at a given level
            If level != leaf level, call __Search
            Calls _insertion_directe, using the TreeHash of the word at root level to know the phylum
 Arg:       w   : The window encapsulating the word to search
 Return :   The leaf of the searched word, or NULL if not found
 Global:
*/


Leaf* Tree::Search (const Window& w)
{
	Uint4 code=w.TreeHash(ROOT_LEVEL);
    if (Phylum(w) && top[code].son) {           // If there is something in this phylum, search here
		return __Search(top+code,w,ROOT_LEVEL+1);
	} else {                                // Else, nothing found
		return NULL;
	};
}

/*
 Name:      __Search
 Usage:     Leaf* l = __Search(p,w,level)
 Function:  Search a leaf whose hash_code is equal to code
            Return a pointer to the leaf if found, NULL if not found
 Arg:       p:    The father of the starting node
            w:    The window encapsulating the word we search for
            level:The level in the tree
 Return :   A pointer to the found node, of NULL if not found
*/

Leaf* Tree::__Search(Node* p, const Window& w, Uint1 level)
{
    void* n = p->son;
    Uint4 code = w.TreeHash(level);
    if (level==leaf_level)        // It is a leaf, end of recursivity
    {
        return __HorizLeafSearch((Leaf*)n,code);
    }
    else
    {
        n=__HorizNodeSearch((Node*)n,code);
        if (n==NULL)
            return (Leaf*)n;
        else
            return __Search((Node*)n,w,level+1);
    }
}

/*
 Name:      __HorizNodeSearch
 Usage:     Node* node = __HorizNodeSearch(p,code)
            Node* node = __HorizNodeSearch(p,code,prev)
 Function:  Search a node whose hash_code is equal to code
            Return a pointer to the node if found, NULL if not found
 Arg:       p:    The starting node
            code: The code to search for
            prev: returns the node just before the found node
                  (used for delete)
 Return :   A pointer to the found node, of NULL if not found
            nd form: return a pointer to node previous to the searched node
            If the searched node is not found, return the position at which to insert it
*/

Node* Tree::__HorizNodeSearch(Node * p, Uint4 code)
{
    for(Node* n=p;n != NULL; n=n->brother)
    {
        if (code==n->hash_code)
            return n;
        if (code<n->hash_code)
            return NULL;
    };
    return NULL;
}

Node* Tree::__HorizNodeSearch(Node * p, Uint4 code, Node ** prev)
{
    *prev=NULL;
    for(Node* n=p;n != NULL; n=n->brother)
    {
        if (code==n->hash_code)
            return n;
        if (code < n->hash_code)
            return NULL;
        *prev = n;
    };
    return NULL;
}

/*
 Name:      __HorizLeafSearch
 Usage:     Leaf* leaf = __HorizLeafSearch(p,code)
            Leaf* leaf = __HorizLeafSearch(p, code, prev)
 Function:  Search a leaf whose hash_code is equal to code
            Return a pointer to the leaf if found, NULL if not found
 Arg:       p:    The starting leaf
            code: The code to search for
            prev: Used for returning the node just before the found node
 Return :   A pointer to the found node, of NULL if not found
            The previous node if found (2nd form)
*/

Leaf* Tree::__HorizLeafSearch(Leaf * p, Uint4 code)
{
    for(Leaf* n=p;n != NULL; n=n->brother)
    {
        if (code==n->hash_code)
            return n;
        if (code < n->hash_code)
            return NULL;
    };
    return NULL;
}

Leaf* Tree::__HorizLeafSearch(Leaf * p, Uint4 code, Leaf ** prev)
{
    *prev=NULL;
    for(Leaf* n=p;n != NULL; n=n->brother)
    {
        if (code==n->hash_code)
            return n;
        if (code < n->hash_code)
            return NULL;
        *prev = n;
    };
    return NULL;
}


/*=====================================================================
       Printing the tree
=======================================================================*/




/*
  Name:     operator<<
  Usage:    cout << t << '\n';
  Print formatted the complete tree, mainly for debugging purposes
*/

ostream & operator<<(ostream& os, const Tree& t) {
    if (!t.initialized)
        SysDie(0,"ERROR - The tree cannot be written because it is not initialized");

    const Mask& msk = t.database->GetMsk();
    for (Uint4 code=0;code<t.nb_top;code++)
        t.top[code].Print(os,msk);
    return os;
}

/*
  Name:      Node::Print
  Usage:     node.Print(cout,level,m,word)
  Procedure: If the node does not have any son, return
             Else print the node, then its sons, then its brothers.
  Arg:  os    ==> An output stream
        m     ==> The mask (used for reconstruction of the word)
        level ==> The level at which the node lives in the tree (default=ROOT_LEVEL)
        word  ==> The reconstructed word (default = "")
        indent==> The number of spaces to insert before any printed character
*/

void Node::Print(ostream& os, const Mask& m, Uint1 level, const string& word, int indent)
{
    if (son==NULL)
        return;

    string begin="";
    for(int i=0;i<indent;i++) begin+=' ';

    // First, print the node itself
    string reconstructed = m.Reconstruct(word,hash_code,level);
    os << begin << reconstructed << ' ' << hex << hash_code << " (" << (int)level << '/' << m.GetLeafLevel() << ")\n";

    // Second, print the son - It should work

    if (m.GetLeafLevel()-level>1)     // The son is still a Node*
    {
        if (son != NULL)
        {
            Node* s = (Node*) son;
            s->Print(os,m,level+1,reconstructed,indent+m.GetLength()-1);
        }
        else
        {
            os << begin << "THERE SHOULD BE A NODE HERE " << (int)level+1 << '/' << m.GetLeafLevel() << '\n';
        }
    }
    else                        // The son is a Leaf*
    {
        if (son != NULL)
        {
            Leaf *s = (Leaf*) son;
            s->Print(os,m,reconstructed,indent+m.GetLength()-1);
        }
        else
        {
            os << begin << "THERE SHOULD BE A LEAF HERE " << (int)level+1 << '/' << m.GetLeafLevel() << '\n';
        }
    }

    // Third, and if possible only, print the brother

    if (brother != NULL)
    {
        brother->Print(os,m,level,word,indent);
    };
}

/*
  Name:      Leaf::Print
  Usage:     leaf.Print(cout,level,m,word)
  Procedure: Print the leaf, then its brothers.
  Arg:  os    ==> An output stream
        m     ==> The mask (used for reconstruction of the word)
        word  ==> The reconstructed word
        indent==> The number of spaces to insert before any printed character

*/

void Leaf::Print(ostream& os, const Mask& m, const string& word, int indent)
{
    // First, print the leaf
    Uint1 level = m.GetLeafLevel();     // The leaves live only at this level
    string begin="";
    for(int i=0;i<indent;i++) begin+=' ';
    string reconstructed = m.Reconstruct(word,hash_code,level);
    os << begin << reconstructed << ' ' << hex << hash_code << " (" << (int)level << '/' << m.GetLeafLevel() << ") ";
    os << dec << GetNbPos() << " positions - ";
    if (pos !=NULL) {
        Pos* p = pos;
        int dsp_pos = (nb_pos < 10) ? nb_pos : 10;     // Do not display all positions
        for (int i=0;i<dsp_pos;i++) {
            os << dec << p->position << ' ';
            p=p->brother;
        };
        if (dsp_pos==nb_pos)
        {
            os << '\n';
        }
        else
        {
            os << "...\n";
        };
    };

    // Second, print the  brothers, if any
    if (brother != NULL)
    {
        brother->Print(os,m,word,indent);
    };
}


/*            Version avec compression                */

/*
 Name:      Tree::WriteCompressed (2 versions)
 Usage:     int j = tree.WriteCompressed(pf)
            int j = tree.Writecompressed(m)
 Function:  Write the tree to the opened file descriptor pf /to the Buffer m
 Arg:       pf = A file descriptor
            m  = A Buffer
 Return:    The number of phyla written
 Global:
*/

int Tree::WriteCompressed(FILE * pf)
{
    if (!initialized)
        SysDie(0,"ERROR - The tree cannot be written because it is not initialized");

    //cerr << inf_code << ' ' << sup_code-1 << '\n';
    for (Uint4 i=inf_code; i< sup_code; i++)
    //for (i=0; i< 2; i++)  // for debugging only
    {
        if (top[i].son != NULL)
        {
			TopInfo e;
			e.sons = __WriteSons(pf,top[i].son,ROOT_LEVEL+1);
	    	assert((offset - database->node_width[ROOT_LEVEL+1])<limit_Uint8.max());
	    	e.start = offset - database->node_width[ROOT_LEVEL+1];
			top_info.push_back(e);
        }
        else
        {
            top_info.push_back(enul);
        }
    };

    // We now write all top levels, but ONLY if we are working in the last pass

    if (ct_pass==nb_pass)
    {
        while(!top_info.empty())
        {
            TopInfo e = top_info.back();
            top_info.pop_back();
            Uint1 s = e.sons;
            size_t t = e.start;
            offset += sizeof(Uint1)*fwrite(&s, sizeof(Uint1), 1, pf);
			offset += __CheckAndWriteOffset(pf,t,ROOT_LEVEL);
        };
    };
    database->Size = offset;
    cout << "INDEX SIZE " << offset << "\n";

    return nb_top;
}

int Tree::WriteCompressed(Buffer* m)
{
    if (!initialized)
        SysDie(0,"ERROR - The tree cannot be written because it is not initialized");

    for (Uint4 i=inf_code; i< sup_code; i++)
    //for (i=0; i< 2; i++)  // for debugging only
    {
        if (top[i].son != NULL)
        {
            TopInfo e;
            e.sons = __WriteSons(*m,top[i].son,ROOT_LEVEL+1);
			assert((offset - database->node_width[ROOT_LEVEL+1])<limit_Uint8.max());
            e.start = offset - database->node_width[ROOT_LEVEL+1];
            top_info.push_back(e);
        }
        else
        {
            top_info.push_back(enul);
        }
    };

    // We now write all top levels, but ONLY if we are working in the last pass

    if (ct_pass==nb_pass)
    {
        while(!top_info.empty())
        {
            TopInfo e = top_info.back();
            top_info.pop_back();
            Uint1 s = e.sons;
            size_t t = e.start;
            offset += sizeof(Uint1) * (Uint1) m->Put((char)s);
            offset += __CheckAndWriteOffset(*m,t,ROOT_LEVEL);

        };
    };
    database->Size = offset;
    return nb_top;
}
/*
 Name:      Tree::__WriteSons (2 versions)
 Usage:     int son = __WriteSons(f,strt,level)
            int son = __WriteSons(m,strt,level)
 Function:  Write the subtree starting at strt (with level level) to the file f. / the Buffer m
            If level < GetLeafLevel(), strt is a node and WriteSons is called first for the son, second for the brother
	        If level == GetLeafLevel(), strt is a leaf: the positions are written, the WriteSons is called for the brother
            The private member offset is updated
 Arg:       f     = A file descriptor
            m     = A Buffer
            strt  = The subtree to write (if NULL, nothing is written)
	        level = The level at which strt lives
 Return:    The number of brothers at the current level
 Global:    Tree_var, Masque
*/

Uint1 Tree::__WriteSons(FILE* pf, void* strt, Uint1 level)
{
	Uint1 brothers=0;
	Uint1 sons=0;
	size_t my_offset;

	if (strt!=NULL){
		if (level<leaf_level) {	/* It is a node */
			Node * n = (Node*) strt;
			sons=__WriteSons(pf,(void*)n->son,level+1);            // First, write the son
            my_offset = offset - database->node_width[level+1];
            brothers=__WriteSons(pf,(void*)n->brother,level)+1;    // Then, write the first brother

			if (sons)                                                   // Write the node himself
                __SerializeNode(pf,n,sons,offset-my_offset,level);

		} else {						/* It is a leaf */
			Leaf*f=(Leaf*) strt;
			if (!f->IsEmpty()) {        // If there are positions for this leaf
				database->Positions+=f->GetNbPos();
				my_offset=offset;
				__WriteLeafPositions(pf,f);
				brothers=__WriteSons(pf,(void*)f->brother,level)+1;
				__SerializeLeaf(pf,f,offset-my_offset,level);
			}
		}

		return brothers;
	}
    else
    {
        return 0;
    };
}

Uint1 Tree::__WriteSons(Buffer& m, void* strt, Uint1 level)
{
	Uint1 brothers=0;
	Uint1 sons=0;
	size_t my_offset;

	if (strt!=NULL){
		if (level<leaf_level) {	/* It is a node */
			Node * n = (Node*) strt;
			sons=__WriteSons(m,(void*)n->son,level+1);            // First, write the son
            my_offset = offset - database->node_width[level+1];
            brothers=__WriteSons(m,(void*)n->brother,level)+1;    // Then, write the first brother

			if (sons)                                                   // Write the node himself
                __SerializeNode(m,n,sons,offset-my_offset,level);

		} else {						/* It is a leaf */
			Leaf*f=(Leaf*) strt;
			if (!f->IsEmpty()) {        // If there are positions for this leaf
				database->Positions+=f->GetNbPos();
				my_offset=offset;
				__WriteLeafPositions(m,f);
				brothers=__WriteSons(m,(void*)f->brother,level)+1;
				__SerializeLeaf(m,f,offset-my_offset,level);
			}
		}
		return brothers;
	}
    else
    {
        return 0;
    };
}

/*
 Name:      Tree::__SerializeNode (2 versions)
 Usage:     __SerializeNode(f,node,sons,shift_count,depth)
            __SerializeNode(m,node,sons,shift_count,depth)
 Procedure: Serialize and write a node to the descriptor f  / the Buffer m
 Arg:       f     = A file descriptor
            m     = A Buffer
            node  = A node
			sons  = The number of sons this node has
	        shift_count = A shift count to write
			depth = The depth of the node in the tree
 Global:    Tree_var
*/

void Tree::__SerializeNode(FILE *pf,Node* n, Uint1 sons, size_t shift, Uint1 level)
{
    Uint1 code = (Uint1) n->hash_code;

	offset += sizeof(Uint1)*fwrite(&code, sizeof(Uint1), 1, pf);
	offset += sizeof(Uint1)*fwrite(&sons, sizeof(Uint1), 1, pf);
    offset += __CheckAndWriteOffset(pf,shift,level);
}

void Tree::__SerializeNode(Buffer& m,Node* n, Uint1 sons, size_t shift, Uint1 level)
{
    Uint1 code = (Uint1) n->hash_code;
    offset += sizeof(Uint1) * (Uint1) m.Put((char)code);
    offset += sizeof(Uint1) * (Uint1) m.Put((char)sons);
	offset += __CheckAndWriteOffset(m,shift,level);
}

/*
 Name:      Tree::__SerializeLeaf (2 versions)
 Usage:     __SerializeLeaf(f,leaf,shift_count)
 Procedure: __Serialize and write a leaf to the descriptor pf / the Buffer m
 Arg:       pf    = A file descriptor
            m     = A Buffer
            l     = A leaf
	        shift = A shift count to write
			level = The depth of the leaf in the tree
 Global:    Tree_var
*/

void Tree::__SerializeLeaf(FILE *pf, Leaf* l, size_t shift, Uint1 level)
{
    Uint1 code   = (Uint1) l->hash_code;
    Uint1 nb_pos = (Uint1) l->GetNbPos();
    offset += sizeof(Uint1)*fwrite(&code, sizeof(Uint1), 1, pf);
	offset += sizeof(Uint1)*fwrite(&nb_pos, sizeof(Uint1), 1, pf);
    offset += __CheckAndWriteOffset(pf,shift,level);
}

void Tree::__SerializeLeaf(Buffer &m, Leaf* l, size_t shift, Uint1 level)
{
    Uint1 code   = (Uint1) l->hash_code;
    Uint1 nb_pos = (Uint1) l->GetNbPos();
    offset += sizeof(Uint1) * (Uint1) m.Put((char)code);
    offset += sizeof(Uint1) * (Uint1) m.Put((char)nb_pos);
	offset += __CheckAndWriteOffset(m,shift,level);
}

/*
 Name:      Tree::__WriteLeafPositions (2 versions)
 Usage:     __ WriteLeafPositions(f,leaf)
            __ WriteLeafPositions(m,leaf)
 Function:  Write the array of leaf positions to the descriptor f / the Buffer m
 Arg:       pf    = A file descriptor
            m     = A Buffer
            f     = A leaf
 Global:    Tree_var
 Return:    The number of bytes written
*/

int Tree::__WriteLeafPositions(FILE* pf, Leaf* f)
{
    int bytes=0;
    Pos* p=f->pos;
    //cerr << "P ";
    for (int i=0;i<f->nb_pos;i++) {
        //cerr << p->position << ' ';
		assert(p->position<=GetDatabaseParameters().GetLength());
        bytes += sizeof(Uint4) * fwrite((void *)&(p->position),sizeof(Uint4),1,pf);
        p=p->brother;
    }
    offset += bytes;
    return bytes;
    //cerr << '\n';
}

int Tree::__WriteLeafPositions(Buffer& m, Leaf* f)
{
    int bytes=0;
    Pos* p=f->pos;
    //cerr << "P ";
    for (int i=0;i<f->nb_pos;i++) {
        //cerr << p->position << ' ';
        bytes += sizeof(Uint4) * m.Put(p->position);
        p=p->brother;
    }
    offset += bytes;
    return bytes;
    //cerr << '\n';
}


/*
 Name:      Tree::__CheckAndWriteOffset  (2 versions)
 Usage:     __CheckAndWriteOffse(pf,shift)
            __CheckAndWriteOffse(m,shift)
 Function:  Write the array of leaf positions to the descriptor f / the Buffer m
 Arg:       pf    = A file descriptor
            m     = A Buffe
            shift = A shift count to write
			level = The depth in the tree
 Global:    Tree_var
 Return:    The number of bytes written
*/

int Tree::__CheckAndWriteOffset(FILE* pf,size_t shift,Uint1 level)
{
	BaseSerializeShift &fct = *serialize_shift[database->node_width[level]];
    Uint1 ofs = fct(pf,shift);    // Calling the good SerializeShift function
    if (ofs != 0)
    {
        return ofs;
    }
    else
    {
        cerr << shift << " out of range at level "<<(int)level<<" (see manual for details)\n";
        exit(1);
    };

}

int Tree::__CheckAndWriteOffset(Buffer& m,size_t shift,Uint1 level)
{
	BaseSerializeShift &fct = *serialize_shift[database->node_width[level]];
    Uint1 ofs = fct(m,shift);    // Calling the good SerializeShift function
    if (ofs != 0)
    {
        return ofs;
    }
    else
    {
        cerr << shift << " out of range at level "<<(int)level<<" (see manual for details)\n";
        exit(1);
    };


}

/* Creating the static members of those objects */

size_t NBaseNode::mem_used = 0;
SingleLinkedList<Node> NBaseNode::reserve;

size_t NBaseLeaf::mem_used = 0;
SingleLinkedList<Leaf> NBaseLeaf::reserve;

size_t NBasePos::mem_used = 0;
SingleLinkedList<Pos> NBasePos::reserve;


