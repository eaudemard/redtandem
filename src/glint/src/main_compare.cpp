/*
   $Id: main_compare.cpp 267 2010-02-19 15:28:48Z tfaraut $
*/

/*main_compare.c*/


#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>
#include <string>
using namespace std;


#include "ptypes.h"
#include "index_io.h"
#include "utils.h"
#include "alignment.h"
#include "sequence_io.h"
#include "match.h"
#include "bitcount.h"


extern struct MatchConstants* MatchScoring;

void Usage();
RealLogger* logger;
bool FindIndex(const string &);
bool ValidQueryFile(const string &);

/*////////////// Main ///////////////*/
int main(int argc,char* argv[])
{

    Compare_Parameters compare(argc,argv,Usage);
	
	 if ( !compare.Silent ) 
	{
    	cerr <<"glint rel $Rev: 267 $ - Compiled on " << __DATE__ << " at " << __TIME__;
    	#ifdef __INTEL_COMPILER
    	cerr << " using the intel compiler " << __INTEL_COMPILER_BUILD_DATE << '\n';
    	#elif __GNUG__
    	cerr << " using the gcc compiler " << __VERSION__ << '\n';
    	#endif
    	#ifdef WORD_IN_TWO_PARTS
    	cerr << "WORD OBJECT IN TWO PARTS ==> SLOWER\n";
    	#endif
	}
	

    /* Initializing logger */
    string log_filename;
    if (compare.query_filename == "-")
    	log_filename="glint_pipe.log";
    else
    	log_filename=compare.query_filename + ".log";

    auto_ptr<RealLogger> logger_ptr = auto_ptr<RealLogger>(new RealLogger(log_filename,compare.log_level,LOGGER_OPT_AUTOFLUSH));
    logger = logger_ptr.get();

    // Checking is the query file is a valid fasta file before loading the database in memory
    if ( !ValidQueryFile(compare.query_filename) )
    {
    	fprintf(stderr,"The query file %s is not a non-empty fasta file\n",compare.query_filename.c_str());

    	return EXIT_NotStarted;
    }

    bool auto_indexing_mode = !FindIndex(compare.bank_filename);        // If the index is not found, we switch to auto_indexing_mode

    /* auto_indexing_mode: dnaindex did not ran, we index the data on the fly */
    if (auto_indexing_mode)
    {
        fprintf(stderr,"Indexing the database\n");
        DataBase_Parameters database(compare);

        /* Initializing the indexation tree, with the singleton semantics */

        Tree * index_tree_ptr = Tree::Instance();
        index_tree_ptr->Initialize(&database);

        BlackList* black_list = new BlackList(1000);              // There should not be a too long black list
        SBuffer* bfr = new SBuffer(*index_tree_ptr,*black_list,database,true); // Creating a new SBuffer object, in memory mode

        bfr->ReadSeq();                                   // Read and index the sequence

        delete bfr;
        delete black_list;
        delete index_tree_ptr;

        if (compare.Self && !compare.Silent)
            fprintf(stderr,"Self comparison has been asked !\n");

        QBuffer query(database,compare);
#ifdef DEBUGLOG
        fprintf(stderr,"DataBase: %s\nRequest: %s\nOutput: %s\nWord size: %d\n",
                compare.bank_filename.c_str(),compare.query_filename.c_str(),compare.results_filename.c_str(),database.GetMsk().GetLength());
#endif
        MatchScoring=new MatchConstants(database.GetMsk(),(float)database.GetNbWords());
		if ( !compare.Silent )
        	fprintf(stderr,"Processing the request\n");
        query.ReadSeqQuery(compare.query_filename);
        if ( !compare.Silent )
			fprintf(stderr,"Leaving read query\n");
    }
    /* normal mode: the data were indexed by dnaindex */
    else
    {
        if (compare.Self && !compare.Silent)
            fprintf(stderr,"Self comparison has been asked !\n");
		  if (compare.Linking)
            fprintf(stderr,"The progam will try to link the hsps !\n");

        if (compare.Verbose)
			fprintf(stderr,"Reading the database\n");
        DataBase_Parameters database(compare.bank_filename);
        QBuffer query(database,compare);
#ifdef DEBUGLOG
        fprintf(stderr,"DataBase: %s\nRequest: %s\nOutput: %s\nWord size: %d\n",
                compare.bank_filename.c_str(),compare.query_filename.c_str(),compare.results_filename.c_str(),database.GetMsk().GetLength());
#endif
        MatchScoring=new MatchConstants(database.GetMsk(),(float)database.GetNbWords());
		
		if ( !compare.Silent )
		  fprintf(stderr,"Processing the request\n");

        if (compare.Genome)
        {
	    		DataBase_Parameters indexed_query(compare.query_filename);
	    		query.SetQueryDatabase(&indexed_query);
	    		query.ReadSeqQuery(indexed_query);
        }
        else
        {
            query.ReadSeqQuery(compare.query_filename);
        };
        
		if ( !compare.Silent )
        	fprintf(stderr,"Leaving read query\n");
    };
	return 0;
}

/*
   Function:  FindIndex
              Search an index, and return false if all files are not found

   Arg:       The name of the database
   Return Value: true if the index files are found, else false
*/

bool FindIndex(const string & name)
{

    struct stat buf_stat;
    string file_bi = name + BI_EXTENSION;
    string file_fa = name + FA_EXTENSION;
    string file_he = name + HE_EXTENSION;

    bool found = true;

    if (stat(file_bi.c_str(),&buf_stat)==-1)
        found = false;
    if (stat(file_fa.c_str(),&buf_stat)==-1)
        found = false;
    if (stat(file_he.c_str(),&buf_stat)==-1)
        found = false;

    return found;
}

/*
   Function: ValidQueryFile

   Arg: The name of the query file
   Return Value: true if the query fail is a valid (multi)fasta file, else false
*/

bool ValidQueryFile(const string & file_query)
{
	bool valid = true;

	if (   file_query=="-" ) // Checking stdin as a valid fasta file is not implemented yet
	{
		return valid;
	}

	ifstream infile;
    string s;
	infile.open (file_query.c_str(), ifstream::in);

	 getline(infile, s); //The first line
	 if ( s[0] != '>' )
		 valid = false;
	 getline(infile, s); //The second line
	 if (s.length() == 0 )
		 valid = false;

	return valid;
}

/*Utilisation du programme */
void Usage()
{
	fprintf(stderr,"glint: Comparing a nucleic sequence versus a bank.\n");
	fprintf(stderr,"Usage: glint [options] bank query \n");
	fprintf(stderr,"    or glint bank query [options] \n\n");
	fprintf(stderr,"\tbank: this fasta or multifasta file has been indexed by dnaindex and the index files must be in the same directory\n");
    fprintf(stderr,"\t            The indexation step may be avoided if the bank file is small enough\n");
	fprintf(stderr,"\tquery: fasta or multifasta file \n\n");
	fprintf(stderr,"\tOptions :\n");
	fprintf(stderr,"\t\t -c c: where c is the minimum score to trigger an ungapped alignment (default %d) \n",DEFAULT_CUTOFF_SLIDE);
	fprintf(stderr,"\t\t -X x: where x is the minimum score to trigger a gapped alignment (dynamic programming) (default %d)\n",DEFAULT_CUTOFF_DYN);
	fprintf(stderr,"\t\t -s s: where s is the minimum score ro report an alignment (default %d) \n",DEFAULT_CUTOFF_HSP);
	fprintf(stderr,"\t\t -w w: where w is the starting point at which the seeds are reported (similar to -W in blast) (default %d)\n",DEFAULT_LOWER_LEVEL);
	fprintf(stderr,"\t\t -d d: where d is the ending point at which the seeds are reported (default %d)\n",DEFAULT_DEEPEST_LEVEL);
	fprintf(stderr,"\t\t -f : flag, fast output without sorting hsps\n");
	fprintf(stderr,"\t\t -C 0/1/2: mask low complexity sequence using dinucleotides count (1), dust (2), or do not mask (0) (default %d)\n", DEFAULT_COMPLEXITY);
	fprintf(stderr,"\t\t -S : flag, self comparison of database if specified \n");
	fprintf(stderr,"\t\t --tandem dist : maximum size allowed for tandem repeats (this makes sens only for self-comparison, hence with the -S flag\n");
    fprintf(stderr,"\t\t -G : flag, if specified, the request must have been indexed through dnaindex, too - Faster for complete genome comparisons\n");
    fprintf(stderr,"\t\t --strict : lower case letter in the query sequence will be considered as non reliable nucleotides (=> they always lead to a mismatch)\n");
    fprintf(stderr,"\t\t --no_gap : do not trigger gapped alignment \n");
    fprintf(stderr,"\t\t --step s : stepping value for the window on the query sequence in the process of identifying seeds (default %d)\n",DEFAULT_STEP);
    fprintf(stderr,"\t\t --match=r :reward for a match (default %d)\n",DEFAULT_MATCH);
    fprintf(stderr,"\t\t --mismatch=p : reward (penality) for a mismatch (default %d)\n",DEFAULT_MISMATCH);
	fprintf(stderr,"\t\t --gapop=p : reward (penality) for a gap opening (default %d)\n",DEFAULT_GAP_OPENING);
    fprintf(stderr,"\t\t --gapext=p : reward( penality) for a gap extension (default %d)\n",DEFAULT_GAP_EXTENSION);
    fprintf(stderr,"\t\t --mmis m : maximum number of mismatches allowed (default none)\n");
    fprintf(stderr,"\t\t --mgap g : maximum number of gaps allowed (default none)\n");
    fprintf(stderr,"\t\t --lmin l : minimum length of the hsp (default none)\n");
    fprintf(stderr,"\t\t --srmin r : minimum score, expressed in terms of percentage of the maximum possible score given the query (default none)\n");
    fprintf(stderr,"\t\t --lrmin r : minimum length, expressed in terms of percentage of the maximum possible length given the query (default none)\n");
    fprintf(stderr,"\t\t -m 1-3 and 8-9 : output format (default m=%d)\n",DEFAULT_OUTPUT_FORMAT);
	fprintf(stderr,"\t\t\t1 : complete alignement (default)\n");
	fprintf(stderr,"\t\t\t2 : tabular output \n");
	fprintf(stderr,"\t\t\t3 : shortest edit script \n");
	fprintf(stderr,"\t\t\t8 : the blastall -m 8 ouput format \n");
	fprintf(stderr,"\t\t\t9 : the blastall -m 9 ouput format \n");
	fprintf(stderr,"\t\t -o filename : output file (default stdout)\n");
	fprintf(stderr,"\t\t --silent somehow less verbose output\n");
   fprintf(stderr,"\t\t --verbose  somehow more verbose output\n");

    fprintf(stderr,"\tIf the indexation step is avoided, you may also use the following options:\n");
	fprintf(stderr,"\t\t -N Name : Name of the bank (defaults to file name)\n");
	fprintf(stderr,"\t\t -M M    : M is the mask structure (default  %s, max 32 characters), may contain 1,..,%d characters.\n",DEFAULT_MASK,MAX_LEAF_LEVEL);
	fprintf(stderr,"\t\t -n %4d : Max_Count The maximum number of allowed occurences for a word otherwise deleted (max 256). \n",DEFAULT_MAX_COUNT);
	fprintf(stderr,"\t\t -C 0/1/2: the same complexity is ued for indexing and for querying\n");
    #ifdef DEBUGLOG
    fprintf(stderr,"\t\t -D T/TV\tlog some messages useful for debugging the program\n");
    #endif

    fprintf(stderr,"\nDescription:\n");
	fprintf(stderr,"\tThe program works in the following manner : \n");
	fprintf(stderr,"\t\t- the index file is uploaded in memory as a factor tree like structure of k-words (k=size of the mask).\n");
    fprintf(stderr,"\t\t  If the indexation step is avoided, the indexation is performed on the fly, and the indexes are not kept.\n");
	fprintf(stderr,"\t\t- the query sequence is scanned in orderr to detect seeds using the factor tree like structure as an index.\n");
	fprintf(stderr,"\t\t- when a group of seed meets a given criterion, a dropoff alignment is performed in the neigborhood.\n");
}

