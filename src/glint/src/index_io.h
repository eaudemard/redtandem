/*
   $Id: index_io.h 259 2009-08-05 08:59:30Z tfaraut $
*/



#ifndef __INDEX_IO_H__
#define __INDEX_IO_H__

#include <stdio.h>
#include <stdlib.h>
#include "utils.h"
#include "match.h"
#include "window.h"
#include "compare.h"

#define MAX_LIGNE_SIZE 1000

/*

    TreePath is a temporary variable used by CTree.
    We keep the whole path to the found match (ancestor_nodes, an array of scores)
    and the deepest found match.

*/

struct TreePath {
    void Reset(short s) {   // Resetting the scores
        for (int i=0;i<MAX_LEAF_LEVEL+1;i++)
            ancestor_nodes[i].Reset(s);
        Deepest.Reset(s);
    };
	Uint1 Hit;
	Seed ancestor_nodes[MAX_LEAF_LEVEL+1];
	Seed Deepest;
};

class CTree;
ostream & operator<<(ostream&, const CTree&);
class CTree {
    public:
    CTree(const DataBase_Parameters& db, const Compare_Parameters & cp);
    virtual ~CTree();
    /*
       Retrieving the top nodes
       All those functions return a char*, ie an address in the index memory area. This address should point the a node definition

       GetTopXXX are used to retrieve the main phyla (top nodes)
          GetTop:       parameter = hash_code    - return the corresponding node
          GetTopNbSons: parameter = hash_code    - return the number of sons this top node has, top node retrieved from the code
          GetTopNbSons: parameter = const char * - return the number of sons this top node has, top node pointed to by parameter
          GetTopSons:   parameter = hash_code    - return the first son this top node has, top node retrieved from the code
          GetTopSons:   parameter = const char * - return the first son this top node has, top node pointed to by parameter

       GetXXX are used to retrieve internal nodes or leafs - Note that the leafs are treated exactly like the nodes. The "sons" of the
       leaves are the positions, and the positions do not have any son.
          GetCode:      parameter = const char * - return the hash code of this node
          GetNbSons:    parameter = const char * - return the number of sons this node has
          GetSons:      parameters= const char *, int - return the first son this node has, the function called depends on the level
          GetBrother:   parameter = const char *, int - return the next brother of this node, the shift depends on the level
          GetPos:       parameter = const char * - return the position pointed to by p

    */
    const unsigned char* GetTop(Uint4 code) const {return index - code*database.GetNodeWidth(ROOT_LEVEL);};
    Uint1 GetTopNbSons(Uint4 code) const {return *(GetTop(code));};
    Uint1 GetTopNbSons(const unsigned char* p) const {return *p;};
    const unsigned char* GetTopSons(Uint4 code) const    { return base_index + _Ptr[ROOT_LEVEL](GetTop(code)+1);}
    const unsigned char* GetTopSons(const unsigned char* p) const {return base_index + _Ptr[ROOT_LEVEL](p+1);};

    inline Uint1 GetCode(const unsigned char* p) const {return p[0];};   // Return the code, assuming p is a correctly initialized ptr
    Uint1 GetNbSons(const unsigned char* p) const {return p[1];};   // Return the number of sons, assuming p is a correctly initialized ptr
    const unsigned char*  GetSons(const unsigned char *p, int level) const {return p-_Ptr[level](p+2);};
    const unsigned char*  GetBrother(const unsigned char* p, int level) const {return p-database.GetNodeWidth(level);};
    Uint4 GetPos(const unsigned char* p) const {return *(Uint4*)p;};
    const unsigned char*  GetIndex(Uint4 code) const    { return base_index;};


    const unsigned char* GetSeq() const {return base_seq;};
    unsigned char Getc(Uint4 i) const {return base_seq[i];};

    friend ostream & operator<<(ostream&, const CTree&);

    void PrintPhylum(ostream&,Uint4) const;

    void CompareWindow(const Window&, Uint4 qpos, MatchResults&) const;

    const DataBase_Parameters & database; // Those objects are public, but const
    const Compare_Parameters  & compare;

    protected:
	 mutable TreePath tree_var;

	 private:
    void __InitFromBfr();              // Called by the constructor
    void __InitFromFiles();            // Called by the constructor
    const unsigned char* base_index;   // The base address of the allocated memory for the binary index
    const unsigned char* index;        // The start point (near the end of the area)
    const unsigned char* base_seq;     // The base address of the allocated memory for the sequence
    const unsigned long nb_top;        // The number of top nodes
    bool InitedFromBuffer; 				// True if the dababase has been inited from the buffer
    RootLevelOffsetType (*_Ptr[MAX_LEAF_LEVEL+1])(const unsigned char*);   // An array of functions, initialized by the constructor
    mutable Uint4 NbPos;               // Temporary - ONLY FOR DEBUG


    int _Search(const Window &) const;
    int _Search(const Window&, const unsigned char*, Uint1, Uint1) const;
    int _ReverseSearch(const Window &) const;
    int _ReverseSearch(const Window&, const unsigned char*, Uint1, Uint1) const;
    const unsigned char * _HorizSearch(Uint4, const unsigned char*, Uint1, Uint1) const;
    void _RetrievePos(const unsigned char *, Uint1, Uint4,MatchPile&) const;
    virtual void _RetrievePosTab(const unsigned char *, Uint1, Uint4,MatchPile&) const;

    static RootLevelOffsetType _Ptr2(const unsigned char *p) {return (RootLevelOffsetType)(*((Uint2 *)p));};
    static RootLevelOffsetType _Ptr4(const unsigned char *p) {return (RootLevelOffsetType)(*((Uint4 *)p));};
    static RootLevelOffsetType _Ptr8(const unsigned char *p) {return (*((RootLevelOffsetType *)p));};

    void _PrintBranch(ostream&, const unsigned char*, Uint1, Uint1, const string&, int) const;

};

class CTreeSelf: public CTree {
	public:
	CTreeSelf(const DataBase_Parameters& db, const Compare_Parameters & cp) : CTree(db,cp) {};
	void _RetrievePosTab(const unsigned char *, Uint1, Uint4,MatchPile&) const;

};


#endif
