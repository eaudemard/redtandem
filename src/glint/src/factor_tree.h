/*
   $Id: factor_tree.h 230 2008-09-12 13:16:00Z tfaraut $
*/


/* factor_tree.h */

/*===========================================================================
                          hachage  -  description
                             -------------------
    begin				: 24 jui 2002
    copyright			: Thomas Faraut
	 objet				: Implement un tableau de hachage
=============================================================================*/

#ifndef __FACTOR_TREE_H__
#define __FACTOR_TREE_H__

#include <stdio.h>
#include <limits>
#include <deque>

#include "ptypes.h"
//#include "utils.h"
#include "memory.h"
#include "window.h"
#include "buffer.h"
#include "database.h"


/*
    The template class SingleLinkedList defines a single linked list, used to temporary store objects like
    Node, Leaf, Pos (see later).
    Prerequisite: The object must have a field brother, and SingleLinkedList<T> must be a friend of T

    The objects are pushed through the front, and popped from the back
    You pop only One object, but you can push a single object, OR a whole list of objects

*/

template <class T> class SingleLinkedList {
    public:
    SingleLinkedList(): first(NULL),last(NULL) {};

    T * PopBack();
    void PushFrontList(T * p);
    void PushFront(T * p);

    bool IsEmpty() const {return first==NULL;};
    //bool IsEmpty() const {return true;};
    void Clear() { first = NULL; last = NULL;};

    private:
    T * first;
    T * last;
};

/*
    Those three little classes are base classes for the Node, Leaf and Pos classes.

    The new operator is overloaded, so that the memory is taken:
        -From a "reserve" static object of class SingleLinkedList if the reserve is not empty
        -From the single instance of MemoryPool if the reserve is empty

    The delete operator is overloaded, so that the memory is not freed, but stored to the reserve
    Clear() clears the reserve, and MemoryUsed returns the quantitiy of memory taken from the pool
*/

class Node;
class Leaf;
class Pos;

class NBaseNode {
    public:
    static void * operator new (size_t sz);
    static void operator delete(void * l);
    static size_t MemoryUsed() { return mem_used;};
    static void Clear() { mem_used = 0; reserve.Clear(); };

    private:
    static size_t mem_used; /* The number of bytes in memory occupied by the positions */
    static SingleLinkedList<Node> reserve;
};


class NBaseLeaf {
    public:

    static void * operator new (size_t sz);
    static void operator delete(void * l);
    static size_t MemoryUsed() { return mem_used;};
    static void Clear() { mem_used = 0; reserve.Clear(); };

    private:
    static size_t mem_used; /* The number of bytes in memory occupied by the positions */
    static SingleLinkedList<Leaf> reserve;
};

class NBasePos {
    public:
    /* WARNING - Here, the delete operator pushes a whole chain of elements, thus allowing for the deletion of
    many Pos objects together - Used by the destructor od Leaf
    */
    static void * operator new (size_t sz);
    static void operator delete(void * l);
    static size_t MemoryUsed() { return mem_used;};
    static void Clear() { mem_used = 0; reserve.Clear(); };

    private:
    static size_t mem_used; /* The number of bytes in memory occupied by the positions */
    static SingleLinkedList<Pos> reserve;
};

/*
    The class Node defines a node in the indexation tree (see the object Tree)
    The new and delete operators are overloaded for this class, thus allowing for very fast memory allocation.
    From time to time, the Tree object will free the whole memory pool, thus destroying all Node objects altogether.
    Tree and Leaf are friends of Node, they Tree may set/get the brother and hash_code private members

*/
class Tree;
class Node: public NBaseNode {
    public:
    /* The constructors of Node:
           -Node()  Default constructor, for initializng the array top
                    hash_code is not used here, the index is the hash_code
           -Node(code,prev_brother)   Used to insert a new node inside an horizontal branch
                                      PRECONDITION (not tested): prev_brother != NULL
           -Node(code,father,1)       Used to insert a new node at front of an horizontal branch
                                      PRECONDITION (not tested): father != NULL
                                      Last parameter (1) is dummy, only used to correctly overload the function
    */

    Node();
    Node(Uint4 code, Node * prev_brother);
    Node(Uint4 code, Node* father, int);

    /* Printing a node */
    void Print(ostream&, const Mask&, Uint1 level=ROOT_LEVEL, const string& word="", int indent=0);

    friend class Tree;
    friend class Leaf;
    friend class SingleLinkedList<Node>;

    private:
	Uint4 hash_code;	/* The hash code, at the correct level, of the words managed by this node */
	Node* brother;      /* Next node (same level)   */
    void* son;          /* Next node (level + 1) - The node may be a Node or a Leaf object */
};

class Pos : public NBasePos {
    public:

    /* The constructor of Pos */

    Pos(Uint4 p):position(p),brother(NULL){};

    friend class Leaf;
    friend class Tree;
    friend class SingleLinkedList<Pos>;

    private:
    Uint4 position; /* The position to keep */
    Pos* brother;   /* Next position (same leaf) */
};


/*
    The class Leaf defines a leaf in the indexation tree (see the object Tree)
    The new and delete operators are overloaded for this class, thus allowing for very fast memory allocation.
    From time to time, the Tree object will free the whole memory pool, thus destroying all Leaf objects altogether.
    Each leaf contains an array of positions, corresponding to the positions of the word corresponding to the leaf.

    Tree is friend of Leaf, thus Tree may set/get the brother and hash_code private members
    -Wall
*/

class Leaf : public NBaseLeaf {
    public:
    /* The constructors of Leaf:
           -Leaf(code,prev_brother)   Used to insert a new leaf inside an horizontal branch
                                      PRECONDITION (not tested): prev_brother != NULL
           -Leaf(code,father,1)       Used to insert a new leaf at front of an horizontal branch
                                      PRECONDITION (not tested): father != NULL
                                      Last parameter (1) is dummy, only used to correctly overload the function
    */

    Leaf(Uint4 code, Leaf* prev_brother);
    Leaf(Uint4 code, Node* father,int);

    /*
      Leaf has a destructor, because when a leaf is destroyed we have to delete the Positions it encapsulates
      However, we delete ONLY the first pos object, because the delete operator is overloaded for the Pos class,
      so that the whole linked list will be recycled (see the Pos class)
    */

    ~Leaf() {
        if (pos != NULL)
            delete pos;
    };

    /* Some accessors */

    int   GetNbPos() const {return nb_pos;};
    bool IsEmpty() const {return !(nb_pos);};

    /* Printing a leaf */
    void Print(ostream&, const Mask&, const string& word="", int indent=0);

    /* Storing positions */

    Uint1 NewPos(Uint4,Uint1);

    friend class Tree;
    friend class SingleLinkedList<Leaf>;

    private:
    Uint4 hash_code;	/* The hash code, at leaf level, of the word */
    Leaf* brother;      /* Next leaf (same level) */
	Pos*  pos;			/* pointeur sur les positions   */
    Uint1 nb_pos;       /* Number of cells in *pos array. Should be == nb_pos, but is not always */
};


/*
    The class BaseSerializeShift is an abstract functor, used only through the () operator.
    Its derived classes (the template SerializeShift) define the operator(), writing the argument shift
    to the argument pf (1st version) or to the MemArray m (2nd version)
    A type cast is first performed, thus leading to 1, 2 or 4 bytes written to the file, depending upon T
    Note that this should work ONLY for the Uin1, Uint2, Uint4 data types, that's all.
    The SerializeShift objects are stored in a private array in the class Tree, this array
    is initialized by the constructor of Tree

*/

class BaseSerializeShift {

	public:
	virtual ~BaseSerializeShift() {}
    virtual Uint1 operator() (FILE* pf, size_t shift) = 0;
    virtual Uint1 operator() (Buffer& m, size_t shift) = 0;

};

template<class T> class SerializeShift: public BaseSerializeShift {
    public:
    virtual Uint1 operator() (FILE* pf, size_t shift) {
        T s = (T) shift;
        if (shift>limit.max())
            return 0;
        else
            return sizeof(T)*fwrite(&s,sizeof(T), 1, pf);
    };
    virtual Uint1 operator() (Buffer& m, size_t shift) {
        T s = (T) shift;
        if (shift>limit.max())
            return 0;
        else
            return sizeof(T) * (Uint1) m.Put(s);    // m.Put returns true/flase, casted to 1/0
    };
    private:
    numeric_limits<T> limit;
};

/*
    The class Tree defines an indexation tree. The tree manages:
    -Top nodes, stored in an array of length 2*n where n is the length of the ROOT_LEVEL (see the Mask class)
    -Mask.GetLevel(leaf_level)-2 levels of nodes, the number of levels thus depends on the mask
    -A level of leafs.

    Tree handles a 'pass' mechanism, in which only some slices of the top nodes (thus of the tree) are considered for inserting,
    searching of deleting the objects. This mechanism enables the indexation of very large files, with a lot of words and a lot of positions,
    without using too much memory.

    The memory pools are flushed at the end of each pass, thus destroying all nodes and leaves.
    Tree is implemented as a singleton. It has only a default constructor, so that database and mask must be initialized
    AFTER Tree (see InitDatabase)

*/


class Tree: public Singleton<Tree> {
    protected:
    Tree();    // The constructor is protected (because we are a singleton)

    public:
    void Initialize(DataBase_Parameters* d);
    friend class Singleton<Tree>;

    void SetNbPass(Uint1);         // Setting the number of passes for creating and writing the index files
    Uint1 NextPass();
    Uint1 GetPass() { return ct_pass;};
    bool  Phylum(const Window &w) const;          // Return true if the word is in the good interval, false if not
    DataBase_Parameters& GetDatabaseParameters() const {return *database;};

    Leaf* Insert(const Window & w);
    Leaf* Search(const Window& w);
    int   Delete(const Window &w);

    /* Printing or binary writing the tree */
    friend ostream & operator<<(ostream&, const Tree&);

    int WriteCompressed(FILE * pf);
    int WriteCompressed(Buffer*);

    private:

    Node* top;      /* The top of the tree: an array of Node */
    Uint4 nb_top;     /* The number of phyla */
    DataBase_Parameters* database;
    int leaf_level; /* The result of database->GetMsk().GetLeafLevel() */
    bool initialized;   /* If false, we have to init database */

    /* A stack to temporary store the info about the top nodes, used by WriteCompressed */
    struct TopInfo {   // Some info to store in a stack about the top nodes
        TopInfo():sons(0),start(0){};
        Uint1 sons;    // The nb of sons of this top node
        size_t start;   // The start position of this top node
    };
    TopInfo enul;      // A null element
    deque<TopInfo> top_info;

    /* The offset, used by WriteCompressed */
    size_t offset;  // Used as a temporary storage by the __Write functions

    /* The shift written in the binary file, may be written by several functions */
    BaseSerializeShift* serialize_shift[10];

    /* Managing several passes */
    Uint1 nb_pass;  // Number of passes
    Uint1 ct_pass;  // Current pass number
    Uint4 lg_pass;  // The "length" of a pass
    Uint4 inf_code; // For a given pass, we consider only the ROOT_LEVEL codes in the interval [inf_code,sup_code[
    Uint4 sup_code;

    void __FreeAll();   // Freeing all memory in one call

    Leaf*  __Search(Node *, const Window&, Uint1);
    Node * __HorizNodeSearch(Node *, Uint4);
    Node * __HorizNodeSearch(Node *, Uint4, Node **);
    Leaf*  __HorizLeafSearch(Leaf *, Uint4);
    Leaf*  __HorizLeafSearch(Leaf *, Uint4, Leaf ** prev);

    int  __Delete(Node *, const Window& w, Uint1 level);

    /* Low level functions to write data to a file */
    Uint1 __WriteSons(FILE*, void*,Uint1);
    void  __SerializeNode(FILE *,Node*, Uint1, size_t, Uint1);
    void __SerializeLeaf(FILE *, Leaf*, size_t, Uint1);
    int __WriteLeafPositions(FILE*, Leaf*);
    int __CheckAndWriteOffset(FILE*,size_t ,Uint1 );

    /* Low level functions to write the data to a memory array */
    Uint1 __WriteSons(Buffer&, void*,Uint1);
    void  __SerializeNode(Buffer&,Node*, Uint1, size_t, Uint1);
    void __SerializeLeaf(Buffer&, Leaf*, size_t, Uint1);
    int __WriteLeafPositions(Buffer&, Leaf*);
	int __CheckAndWriteOffset(Buffer&,size_t ,Uint1 );

};


/* the inline methods of the classes NBaseNode, NBaseLeaf, NBasePos, Node, Leaf */

inline void * NBaseNode::operator new (size_t sz)
{
    if (reserve.IsEmpty())
    {
        mem_used += sz;
        return MemoryPool::Instance()->Malloc(sz);
    }
    else
    {
        return (void *) reserve.PopBack();
    };
}
inline void NBaseNode::operator delete(void * l)
{
    reserve.PushFront((Node*)l);
}

inline void * NBaseLeaf::operator new (size_t sz)
{
    if (reserve.IsEmpty())
    {
        mem_used += sz;
        return MemoryPool::Instance()->Malloc(sz);
    }
    else
    {
        return (void *) reserve.PopBack();
    };
}
inline void NBaseLeaf::operator delete(void * l)
{
    reserve.PushFront((Leaf*)l);
}

inline void * NBasePos::operator new (size_t sz)
{
    if (reserve.IsEmpty())
    {
        mem_used += sz;
        return MemoryPool::Instance()->Malloc(sz);
    }
    else
    {
        //cerr << "coucou recyclage\n";
        return (void *) reserve.PopBack();
    };
}
inline void NBasePos::operator delete(void * l)
{
    //cerr << "coucou delete\n";
    reserve.PushFrontList((Pos*)l);
}

inline Node::Node() : hash_code(0),brother(NULL),son(NULL) {}
inline Node::Node(Uint4 code, Node * prev_brother):hash_code(code),brother(prev_brother->brother),son(NULL)
{
    prev_brother->brother = this;
}
inline Node::Node(Uint4 code, Node* father, int):hash_code(code),brother((Node*)father->son),son(NULL)
{
    father->son = this;    // father must be != 0
}

inline Leaf::Leaf(Uint4 code, Leaf* prev_brother):hash_code(code),brother(prev_brother->brother),pos(NULL),nb_pos(0)
{
    prev_brother->brother = this;
}

inline Leaf::Leaf(Uint4 code, Node* father,int):hash_code(code),brother((Leaf*)father->son),pos(NULL),nb_pos(0)
{
    father->son=this;    // father must be != 0
}
#endif
