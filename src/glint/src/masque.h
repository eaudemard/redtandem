/*
   $Id: masque.h 230 2008-09-12 13:16:00Z tfaraut $
*/


/* masque.h */

#ifndef _MASQUE_H
#define _MASQUE_H

using namespace std;
#include <string>
#include <iostream>
#include <stdlib.h>
#include "ptypes.h"


class Mask;
int overlap(Mask* m, int i, int j, int k);
int overlap_bw(Mask* m, int i, int j, int k);
class Word;
class Window;

/* The Mask object - operator= and copy constructor are private, to disable them */

class Mask {
    public:
    /*
        There is no default constructor
        The constructor/destructor, copy constructor and affectation operator.
        All job is made in the private functions __Init and __Free
    */
    Mask(const string& s) { __Init(s);};
    ~Mask() {__Free();};
    Mask(const Mask&m){  __Init(m.string_mask);};
    Mask& operator=(const Mask& m){ __Free(); __Init(m.string_mask);return *this;};

    string Reconstruct(const string &, Uint4, int) const;

    /* Some accessors... */
    Uint1 GetLength() const { return length;};                          // Get the total length of the mask
    Uint1 GetLength(Uint1 level) const { return level_length[level];};     // Get the length of the mask at a given level
    string GetString() const {return string_mask;};
    int GetLeafLevel() const { return leaf_level;};
	 bool GetBlastzBehavior() const { return blastzlike;};
	 Uint1 GetBitCount(Uint1 level)const { return bm[level];};

	/* Some mutators  */
	/* void SetBlastzbBehavior(const bool& b) { blastzlike=b; __Init(string_mask);};
	 void Change(const string& s, const bool &b) { blastzlike=b; __Init(s);};*/
	 friend class Window;
    friend int overlap(const Mask& m, int i, int j, int k);
    friend int overlap_bw(const Mask& m, int i, int j, int k);

    friend ostream & operator<<(ostream&, const Mask&);

	 Uint8* bm;     /* bitscore mask for bicount scoring  */

    private:
    void __Init(const string&);
    void __Free() {
        if (length==0)  // Nothing was allocated, thus nothing to free
            return;
        for(int i=ROOT_LEVEL;i<=leaf_level;i++)
            free(level_pos[i]);
        free(level_pos);
        free(pos);
    };

    Uint1 length;               /* The length of the mask */
    string string_mask;         /* The string representation */

    int level_length[MAX_LEAF_LEVEL]; /* taille du masque pour chacun des niveaux */
	short **level_pos;          /*                                         */
	short* pos;				    /*positions dans le masque = an array of 'length' short (0..length-1) */
    Uint2 leaf_level;
	bool blastzlike;
};


#endif


/* MANU PORTAGE C++
masque* new_masque                              ==> Constructeur Mask(const char*)
string reconstruct (masque*,string&, Uint4,int) ==> string Reconstruct (const string &, Uint4, int);
void carac_masque(masque*)                      ==> operator<<
Uint1 GetLength() cons                          ==> NEW
int GetLeafLevel() const                        ==> NEW
int code_size(int etape)                        ==> SUPPRIME
string masquer(masque*,string,int)              ==> SUPPRIME;
unsigned long  masquer_entier(masque* ,unsigned long) ==> SUPPRIME
int max_masque(masque*)                         ==> SUPPRIME
int min_masque(masque*)                         ==> SUPPRIME
unsigned long* transition                       ==> SUPPRIME
*/



