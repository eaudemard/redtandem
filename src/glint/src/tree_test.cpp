/*
   $Id: tree_test.cpp 230 2008-09-12 13:16:00Z tfaraut $
*/

#include <math.h>
#include <string>
#include <iostream>
#include <map>
using namespace std;
#include "utils.h"
#include "factor_tree.h"

/* Init a "genome". The number of words generated is passed by parameter */
void InitGenome(string & g, string & d, int n)
{
    Word w(8);
    int nb_words = (int) pow(4,8);
    double factor = (double)nb_words/RAND_MAX;
    for (int i=0;i<n;i++)
    {
        long int r = random();
        r = (long int)(r *factor);
        g += w.Convert(r,8) + "N\n";
        if (i%10 ==0)
            d += w.Convert(r,8) + "D\n";
        //g += w.Convert(r+32768,8) + "N\n";
        //if (i%20 ==0)
        //    d += w.Convert(r+32768,8) + "N\n";
    };
};

void FillTree(Tree & t, Window & w, const string & g)
{
    for (int i=0; i<g.length();i++)
    {
        if (Word::code(g[i]) >3)
            continue;
        w.Shift(g[i]);
        if (w.IsFull()) {
            Leaf* l = t.Insert(w);
            l->NewPos(i,w.GetMask().GetLength());
            w.Reset();
        }
    };
};

void DelTree(Tree & t, Window& w, const string & d, map<string,int>& m)
{
    w.Reset();
    for (int i=0; i<d.length();i++)
    {
        if (Word::code(d[i]) >3)
            continue;
        w.Shift(d[i]);
        if (w.IsFull()) {
            int rvl = t.Delete(w);
            if (rvl==1)
                cerr << "Warning the word is not there " << w.GetWord().Convert() << '\n';
            else
                m[w.GetWord().Convert()]=1;
            w.Reset();
        }
    };
};

void SrchTree(Tree & t, Window& w, const string & d, map<string,int>& m)
{
    bool good=true;
    int cnt=0;
    w.Reset();
    for (int i=0; i<d.length();i++)
    {
        if (Word::code(d[i]) >3)
            continue;
        w.Shift(d[i]);
        if (w.IsFull()) {
            cnt++;
            //cout << w.GetWord().Convert() << "SRCH\n";
            map<string,int>::iterator i = m.find(w.GetWord().Convert());
            Leaf* leaf = t.Search(w);
            if (leaf!=NULL)
            {
                if (i!=m.end())
                {
                    good=false;
                    cout << "ERROR - word " << w.GetWord().Convert() << " FOUND, SHOULD HAVE BEEN DELETED\n";
                }
            }
            else
            {
                if (i==m.end())
                {
                    good=false;
                    cout << "ERROR - word " << w.GetWord().Convert() << " NOT FOUND, BUT NOT DELETED\n";
                }
            };
            w.Reset();
        };
    };
    cout << dec << cnt << " WORDS PROCESSED\n";
    if (good)
        cout << "ALL WORDS WERE FOUND, EXCEPT THE DELETED WORDS\n";
    else
        cout << "ERROR WERE FOUND\n";
};

int main() {

    DataBase_Parameters DataBase(MAX_COUNT);
    DataBase.str_msk = "11112323";   /* A tiny mask, just for testing */

    /* Initializing Tree, with the singleton semantics */

    Tree * t_ptr = Tree::Instance(DataBase);
    Tree & tree  = *t_ptr;

    Window w(DataBase.str_msk);

    /* We simulate a genome for filling the mask, another to delete some words */
    string genome="";
    string to_del="";

    InitGenome(genome,to_del,100); // Genome initialization
    cout << "GENOME \n" << genome << "\n=====\n";
    cout << "TO DEL \n" << to_del << "\n=====\n";

    /* Fill and print the tree */
    //genome="AGCCGGCCTCGACCGTNACGGTCGAGGCCGGCT";

    FillTree(tree,w,genome);      // Fill the tree
    cout << tree << "=========\n";       // Print the tree

    /* Delete some words and print the tree */
    map<string,int> deleted;
    DelTree(tree,w,to_del,deleted);
    cout << tree << '\n';

    /* Search the words of the genome - The deleted words will not be found */
    SrchTree(tree,w,genome,deleted);

    /* Write the tree to some file */
    FILE * index = fopen("tree_test.bin","w");
    tree.WriteCompressed(index);
    fclose(index);

    return 0;
}


