/*
   $Id: index_io.cpp 267 2010-02-19 15:28:48Z tfaraut $
*/

/* index_io.c */

/*===========================================================================
                          index_io  -  description
                             -------------------
    begin				: 24 jui 2002

	 objet				: Interface to the compact index
=============================================================================*/

#define INLINE

using namespace std;
#include <iostream>
#include <fstream>
#include <errno.h>
#include <math.h>
#include <fcntl.h>
#include <assert.h>
#include <unistd.h>
#include "index_io.h"
#include "masque.h"
//#include "utils.h"
#include "compare.h"
#include "sequence_io.h"
#include "factor_tree.h"

const Int1 SStrand[2]={1,-1};

extern RealLogger* logger;

/*
read_patch
Usage:    same as the read function (see man 2 read)
Function: Cut a read operation in several slices if count is too high
          I think there is a bug in read on some systems, at least for linux ia64

*/

ssize_t read_patch(int fd, const void *buf, size_t count)
{
	size_t max_count = 100000000;
	ssize_t rvl=0;
	while (count>0)
	{
		ssize_t r;
		size_t c;
		char * b;
		c = count>=max_count ? max_count : count;
		r = read(fd,(void*)buf,c);
		if (r==-1)
		{
			break;
		}
		else
		{
			rvl += r;
		};
		if (count<max_count)
		{
			break;
		}
		else
		{
			count -= max_count;
		};
		b = (char*)buf + max_count;
		buf = (void*) b;
		cerr<<rvl<<"\r";
	};
	return rvl;
}

/* From this point to the end of this file, all read calls will be replaced with read_patch */

#define read read_patch


/*=====================================================================
							chargement de l'index
=======================================================================*/

/*

    The CTree (Compact Tree) object: reading the indexation tree from a stored file

*/
CTree::CTree(const DataBase_Parameters& db, const Compare_Parameters& cp): database(db), compare(cp),
//2**n
nb_top(1<<(2*db.GetMsk().GetLength(ROOT_LEVEL))),InitedFromBuffer(false) // const vire provisoirement
{
    /*
        We check to know if database maintains buffers for the binary index and for the data
    */

    if (database.GetBiBfr() != NULL)  // Yes... Use those buffers
        __InitFromBfr();
    else
        __InitFromFiles();      // No... Read the index files

    /*
        Init the _ptr array of functions, using the info found in database.node_width
    */

    _Ptr[0] = NULL;     // Not used
    //_Ptr[ROOT_LEVEL] = (database.GetNodeWidth(ROOT_LEVEL) == 5) ? _Ptr4 : _Ptr2;  // nb_son(1b + ptr (4b or 2b)

    //cout<<"Root level width  : "<<database.GetNodeWidth(ROOT_LEVEL)<<"\n";

    switch(database.GetNodeWidth(ROOT_LEVEL)) {
    	case 9 : _Ptr[ROOT_LEVEL] = _Ptr8;break;
	case 5 : _Ptr[ROOT_LEVEL] = _Ptr4;break;
	case 3 : _Ptr[ROOT_LEVEL] = _Ptr2;break;
    }

    for (int i=ROOT_LEVEL+1; i<MAX_LEAF_LEVEL+1; i++)
    {
        switch(database.GetNodeWidth(i))
        {
            case 6: _Ptr[i] = _Ptr4; break; // node = hash_code (1b) + nb_son(1b) + ptr(4b)
            case 4: _Ptr[i] = _Ptr2; break; // node = hash_code (1b) + nb_son(1b) + ptr(2b)
        };
    }
}

/*
    CTree::__InitFromBfr() Init CTree from a buffer, useful in auto_indexation mode
*/

void CTree::__InitFromBfr()
{
    base_index = (unsigned char*) database.GetBiBfr()->GetBase();
    index=base_index + database.GetSize() - database.GetNodeWidth(ROOT_LEVEL);   /*    First go to the end !!   */
    base_seq   = (unsigned char*) database.GetFaBfr()->GetBase();
    InitedFromBuffer=true;
}

/*
    CTree::__InitFromFiles() Init CTree from a buffer, useful in normal (ie NOT auto_indexation) mode
*/

void CTree::__InitFromFiles()
{
	int fd;
	string index_name = database.GetFileName();
    string bin_file = index_name + BI_EXTENSION;

    /* Try to open the .bi file */
	fd=open(bin_file.c_str(),O_RDONLY,0);
	if (fd==-1)
	{
        string msg = "ERROR in opening file " + bin_file + " for reading";
        SysDie(errno,msg.c_str());
	}
    else
    {
        base_index=new unsigned char[database.GetSize()];
        if (base_index==NULL)
            SysDie(errno,"Cannot allocate data for reading the .bi file\n");

        ssize_t lus=read(fd,base_index,database.GetSize());
        close(fd);
        if (lus==-1)
        {
            string msg = "ERROR in reading file " + bin_file;
            SysDie(errno,msg.c_str());
        };
        if (lus != database.GetSize())
        {
            cerr << "ERROR reading file " + bin_file << " read " << lus << " should be " << database.GetSize() << '\n';
            exit(1);
        }; 
		if (!compare.Silent)
        	cerr<<lus<<" read and "<<database.GetSize()<<" to read \n";
    }
	index=base_index + database.GetSize() - database.GetNodeWidth(ROOT_LEVEL);   /*    First go to the end !!   */

    string seq_file = index_name + FA_EXTENSION;
    base_seq = new unsigned char[database.GetLength()+1];
	if (base_seq==NULL)
		SysDie(errno,"Cannot allocate data for reading the .fa file\n");

    fd=open(seq_file.c_str(),O_RDONLY,0);
	if (fd==-1)
	{
        string msg = "ERROR in opening file " + seq_file + " for reading";
        SysDie(errno,msg.c_str());
	}

	ssize_t lus=read(fd,base_seq,database.GetLength());
	close(fd);
    if (!compare.Silent)
		cerr<<lus<<" read and "<<database.GetLength()<<" to read \n";

	if (lus==-1)
	{
        string msg = "ERROR in reading file " + seq_file;
        SysDie(errno,msg.c_str());
    }
}

/*
    The destructor
*/

CTree::~CTree()
{
	if (InitedFromBuffer==false)
	{
		delete[] base_index;
		delete[] base_seq;
	}
}



/*=====================================================================
							recherche  dans l'arbre compact
=======================================================================*/
/*
   rechercher un élément classé avec score
   YO-SC void recherche_compact_root(char *p, const Window& f)
   recherche_compact_root appelle recherche_compact

   2 versions = reverse and direct
                Those two versions are identical, except for calling the member function
                TreeHash or ReverseTreeHash
*/

/*
 Name:      CTree::_Search, CTree:_ReverseSearch
 Usage:     int hit = _Search(const window& w)
 Function:  Search a word in the tree, starting at the root of the tree
 Arg:       f     = The window in which the word to search, and the mask are encapsulated
 Return:    0 if no match is found, or the level at which a match is found
 Access:    private
 Global:    TreeVar
*/

int CTree::_Search(const Window& window) const
{
	Uint4 code=window.TreeHash(ROOT_LEVEL);
    const unsigned char *p = GetTop(code);
    Uint1 sons= GetTopNbSons(p);
    Uint1 hit = 0;

    bool found = false;
	if (sons) {
		tree_var.ancestor_nodes[ROOT_LEVEL].level=ROOT_LEVEL;
		tree_var.ancestor_nodes[ROOT_LEVEL].node=p;
		found=true;
 #ifdef DEBUG
	logger->SetLogType("L") << "Word : "<<window.GetWord().Convert()<<"\n";
        //fprintf(stdout,"%s + %d\t%p\n",window.GetWord().Convert(),ROOT_LEVEL,tree_var.ancestor_nodes[ROOT_LEVEL].node);
#endif
		hit = _Search(window,GetTopSons(p),sons,ROOT_LEVEL+1);
	}
	hit=(hit>ROOT_LEVEL) ? hit : ROOT_LEVEL;

	return (found) ? hit : 0;
}

int CTree::_ReverseSearch(const Window& window) const
{
	Uint4 code=window.ReverseTreeHash(ROOT_LEVEL);
    const unsigned char *p = GetTop(code);
    Uint1 sons= GetTopNbSons(p);
    Uint1 hit = 0;

    bool found = false;
	if (sons) {
		tree_var.ancestor_nodes[ROOT_LEVEL].level=ROOT_LEVEL;
		tree_var.ancestor_nodes[ROOT_LEVEL].node=p;
		found=true;

        /*
        m=mot->Convert();
		fprintf(stdout,"%s + %d\t%p\n",m->m,ROOT_LEVEL,tree_var.ancestor_nodes[ROOT_LEVEL].nd);
		*/
		hit = _ReverseSearch(window,GetTopSons(p),sons,ROOT_LEVEL+1);
	}
	hit=(hit>ROOT_LEVEL) ? hit : ROOT_LEVEL;

	return (found) ? hit : 0;
}

/*
 Name:      CTree::_Search, CTree:_ReverseSearch
 Usage:     int hit = _Search(const window& window, const char* p, Uint1 sons, Uint1 level)
 Function:  Search a word in the tree, starting at any level higher than ROOT_LEVEL
 Arg:       window     = The window in which the word to search, and the mask are encapsulated
            p       = The starting node
            sons    = The number of brothers (including p)
            level   = The level in the tree (min ROOT_LEVEL+1)
 Return:    The last level in which the hit is found (ROOT_LEVEL..leaf_level)
 Access:    private
 Global:    tree_var, Masque
*/

int CTree::_Search(const Window & window, const unsigned char* p, Uint1 brothers, Uint1 level) const
{
    int leaf_level = window.GetMask().GetLeafLevel();
	bool in=true;

	while (level <= leaf_level && in==true) {
        Uint4 code=window.TreeHash(level);
        p = _HorizSearch(code,p,brothers,level);
        if (p==NULL)
        {
            in=false;
        }
        else
        {
			tree_var.ancestor_nodes[level].node=(const unsigned char*)p;
			tree_var.ancestor_nodes[level].level=level;
            brothers=GetNbSons(p);
            p=GetSons(p,level);
            level++;
        };
    };
    return level-1;     // last level in which the word was found
}
int CTree::_ReverseSearch(const Window & window, const unsigned char* p, Uint1 brothers, Uint1 level) const
{
    int leaf_level = window.GetMask().GetLeafLevel();
	bool in=true;

	while (level <= leaf_level && in==true) {
        Uint4 code=window.ReverseTreeHash(level);
        p = _HorizSearch(code,p,brothers,level);
        if (p==NULL)
        {
            in=false;
        }
        else
        {
			tree_var.ancestor_nodes[level].node=(const unsigned char*)p;
			tree_var.ancestor_nodes[level].level=level;
            brothers=GetNbSons(p);
            p=GetSons(p,level);
            level++;
        };
    };
    return level-1;     // last level in which the word was found
}

/*
 Name:      CTree::_HorizSearch
 Usage:     char * p = _HorizSearch(Uint4 code, const char* p, Uint1 brothers, Uint1 level)
 Function:  Search a code in the horizontal branch pointed to by p
 Arg:       code    = A code to search
            p       = The starting node of the branch
            brothers= The number of nodes in the branch
            level   = The level of the branch
 Return:    A ptr to the found node, or NULL if nothing found
 Access:    private
 Global:
*/

const unsigned char* CTree::_HorizSearch(Uint4 code, const unsigned char* p, Uint1 brothers, Uint1 level) const
{
    //for(Uint1 i=0; i<brothers && GetCode(p)<code;i++)   // TODO DICHOTOMIE !
    //    p=GetBrother(p,level);

    Uint1 i=0;
    while(i++<brothers)
    {
        if (GetCode(p)<code)
        {
            p=GetBrother(p,level);
            continue;
        };
        if (GetCode(p)==code)
            return p;
        return NULL;
    };
    return NULL;
}


/*

   procedure CTree::Compare
   Usage:    ct.Compare(w)
   Parameter: w, a window encapsulating the word to compare to the compressed index
              qpos, the position of the word in the query
   Access: public

*/

void CTree::CompareWindow(const Window &w,Uint4 qpos,MatchResults& match_results) const
{
    for (int bstrand=0; bstrand<=1; bstrand++)
    {
        match_results.SetStrand(bstrand);
        tree_var.Reset(SStrand[bstrand]);
        int hit = (bstrand==0)?_Search(w):_ReverseSearch(w);
        int min=w.GetMask().GetLeafLevel();
        NbPos=0;                        // for debugging only, counting the nb of positions found

		if ( hit >= compare.lower_level )
		{
			int idx=( hit < compare.deepest_level ) ? hit : compare.deepest_level ;
			tree_var.Deepest=tree_var.ancestor_nodes[idx];
			min=(idx<min) ? idx : min;
			_RetrievePos((const unsigned char*)tree_var.ancestor_nodes[min].node,min,qpos,match_results.GetCurrentMatchPile());
		}
	}
}


/*=====================================================================
					Retrieving positions in the index tree
=======================================================================*/

/*

    Walk in the tree levels, retrieving all positions under the p,
    living at the level passed by parameter
    The retrieved positions are stored in the MatchPile passed by parameter

*/

void CTree::_RetrievePos(const unsigned char * p, Uint1 level, Uint4 qpos,MatchPile& match_pile) const
{
	int i;
	unsigned char sons;
	const unsigned char *q;

    sons = GetNbSons(p);

	if ( p == tree_var.ancestor_nodes[level].node ) 
		tree_var.Hit=level;    // ancestor_nodes = ancestor Node attribue le veritable hit au match trouve ???  TODO explain better

	if ( level == ROOT_LEVEL )
	{
        sons = GetTopNbSons(p);

        p=GetTopSons(p);
		for (i=0;i<sons;i++)
		{
			_RetrievePos(p,level+1,qpos,match_pile);
            p=GetBrother(p,level+1);
		}
	}
	else if ( level == database.GetMsk().GetLeafLevel() )
	{

        q=GetSons(p,level);

		_RetrievePosTab(q,sons,qpos,match_pile);
	}
	else
	{

        p=GetSons(p,level);
		for (i=0;i<sons;i++)
		{
			_RetrievePos(p,level+1,qpos,match_pile);
            p=GetBrother(p,level+1);
		}
	}
}

/*

    For each retrieved position, call Push, to keep the match in the match_pile passed by parameter
    q = The leaf
    sons = Number of positions to retrieve
    position = The position of the match in the query

*/

void CTree::_RetrievePosTab(const unsigned char * q, Uint1 sons, Uint4 qpos, MatchPile& match_pile) const
{
	int j=0;
	Uint4 pos=0;
	const Uint4 *p;

	p=(Uint4 *)q;
	for (j=0;j<sons;j++)
	{
		pos=*p++;

		/* This could be modified to bypass a call to the function Push */

		/*if (compare.Fill) {
				if ( pos > compare.s_start4fill && pos < compare.s_end4fill) {
						logger->SetLogType("L") << "Trying to add a new position in the matchpile "<<pos<<"\t("<<compare.s_start4fill<<"-"<<compare.s_end4fill<<")\n";
					match_pile.Push(pos,qpos,tree_var.Hit);
				}
		} else  {*/
			match_pile.Push(pos,qpos,tree_var.Hit);
			NbPos++;
        /*}*/
	}
}

void CTreeSelf::_RetrievePosTab(const unsigned char * q, Uint1 sons, Uint4 qpos, MatchPile& match_pile) const
{
	int j=0;
	Uint4 pos=0;
	const Uint4 *p;

	p=(Uint4 *)q;
	for (j=0;j<sons;j++)
	{
		pos=*p++;

		/* This could be modified to bypass a call to the function Push */
		if ( pos > qpos && (pos-qpos) < compare.Tandem ) {		// Only the first half of the matrix has to be studied
                match_pile.Push(pos,qpos,tree_var.Hit);
		}
	}
}

/*=====================================================================
   Printing the index (for debugging purpose, used by dnastat)
=======================================================================*/

/*
  Function:     operator<<
  Usage   :    cout << t << '\n';
  Print formatted the complete tree, mainly for debugging purposes
*/

ostream & operator<<(ostream& os, const CTree& ct) {
    for (Uint4 code=0;code<ct.nb_top;code++) {
        if (ct.GetTopNbSons(code))
        {
            ct.PrintPhylum(os,code);
        };
    };
    return os;
}

/*

  procedure: CTree::PrintPhylum
  Usage:    PrintPhylum(cout,code);
  Print formatted the phylum whose code is passed by parameter
  Parameters: cout An output stream
              code A code, used to identify the phylum to print

*/

void CTree::PrintPhylum(ostream& os, Uint4 code) const
{
   // os << code << ' ' << Word::Convert(code,Masque->GetLength(ROOT_LEVEL)) << '\n';
    string w="";
    w = database.GetMsk().Reconstruct(w,code,ROOT_LEVEL);
    os << w << ' ' << hex << code << " (" << ROOT_LEVEL << '/' << (int)database.GetMsk().GetLeafLevel() << ")\n";
    //fprintf(stdout,"adresse : %p base index : %p  \n",GetTopSons(code), GetIndex(code));
    _PrintBranch(os,GetTopSons(code),GetTopNbSons(code),ROOT_LEVEL+1,w,database.GetMsk().GetLength()-1);
    fprintf(stdout,"Number of sons : %d \n",GetTopNbSons(code));
}

/*

  procedure: CTree::_PrintBranch
  Usage:    _PrintBranch(cout,GetSons(node,level),GetNbSons(node),level,reconstructed,indent)
  Print formatted a node, his brothers and his sons
  Parameters: cout   An output stream
              branch   A pointer to the starting node
              brothers The number of brothers, including branch
              level    The level of branch in the tree
              recontructed A partially reconstructed word
              indent   An indentation number

*/

void CTree::_PrintBranch(ostream& os, const unsigned char* branch, Uint1 brothers, Uint1 level, const string& word, int indent) const
{
    brothers--;     // Do not count myself as a "brother"
    string begin="";
    for(int i=0;i<indent;i++) begin+=' ';

    // First, print the node
    int code = GetCode(branch);
    string reconstructed = database.GetMsk().Reconstruct(word,code,level);
    os << begin << reconstructed << ' ' << hex << code << " (" << (int)level << '/' << (int)database.GetMsk().GetLeafLevel() << ")";
    //os << dec << (int) brothers+1;

    // Second, print the son(s) if whe are still a node...

        os << (int)brothers << " brothers" << '\n';
    if (level<database.GetMsk().GetLeafLevel())     // This is still a node
    {
        os << '\n';
        _PrintBranch(os, GetSons(branch,level+1), GetNbSons(branch), level+1, reconstructed, indent+database.GetMsk().GetLength()-1);
    }
    // ...or print the positions, if we are a leaf
    else
    {
        int nb_pos = GetNbSons(branch);
        const unsigned char * pos = GetSons(branch,level);
        os << ' ' << dec << nb_pos << " positions - ";
        int dsp_pos = (nb_pos < 10) ? nb_pos : 10;     // Do not display all positions
        for (int p=0; p<dsp_pos; p++)
        {
            os << dec << GetPos(pos) << ' ';
            pos += 4;
        };

        if (dsp_pos==nb_pos)
        {
            os << '\n';
        }
        else
        {
            os << "...\n";
        };
    };

    // Then, print the brothers
    const unsigned char* p = branch;
    for (int b=0; b<brothers; b++)    //
    {
        p = GetBrother(p,level);
        _PrintBranch(os,p,1,level,word,indent);
    };
}

#undef read_patch

