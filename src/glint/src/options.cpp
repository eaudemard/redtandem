/*
   $Id: options.cpp 1424 2008-03-06 09:07:57Z tfaraut $
*/


#include "options.h"

CSimpleOpt::SOption g_GlintOptions[] = {
    // ID       TEXT          TYPE
	//	Specific options of the glint executable
    { OPT_CUTOFF_SLIDE,  (char*)"-c",     SO_REQ_SEP  }, // "-c"
    { OPT_CUTOFF_DYN,  (char*)"-X",     SO_REQ_SEP    }, // "-X"
    { OPT_CUTOFF_HSP,  (char*)"-s",     SO_REQ_SEP    }, // "-s"
    { OPT_LOWER_LEVEL,  (char*)"-w",     SO_REQ_SEP   }, // "-w"
    { OPT_DEEPEST_LEVEL, (char*) "-d",     SO_REQ_SEP }, // "-d"
    { OPT_COMPLEXITY, (char*) "-C",     SO_REQ_SEP    }, // "-C"
    { OPT_OUTPUTFORMAT,  (char*)"-m",     SO_REQ_SEP  }, // "-m"
    { OPT_RESULTSFILENAME,  (char*)"-o",     SO_REQ_SEP    }, // "-C"
    { OPT_GENOME, (char*) "-G",     SO_NONE    }, // "-G"
    { OPT_OUTPUTSTREAM, (char*) "-f",     SO_NONE    }, // "-f"
    //{ OPT_PIPE,  (char*)"-p",     SO_NONE    }, // "-p"
    { OPT_PIPE, (char*) "-",     SO_NONE    }, // "-"
    { OPT_SELF,  (char*)"-S",     SO_NONE    }, // "-S"
    { OPT_SELF,  (char*)"--self",     SO_NONE    }, // "--self"
    { OPT_TANDEM,  (char*)"--tandem",     SO_REQ_SEP  }, // "--tandem"
    { OPT_STRICT,  (char*)"--strict",     SO_NONE    }, //
    { OPT_STEP,  (char*)"--step",     SO_REQ_SEP   }, //
    { OPT_SILENT,  (char*)"--silent",   SO_NONE    }, //
    { OPT_VERBOSE,  (char*)"--verbose",   SO_NONE    }, //
    { OPT_VERBOSE,  (char*)"-v",   SO_NONE    }, //
    { OPT_SLICESIZE,  (char*)"--slice-size",     SO_REQ_SEP   }, //
    // Options for the alignments socring scheme 
    { OPT_MATCH, (char*)"--match", SO_REQ_CMB   }, // 
	{ OPT_MISMATCH, (char*)"--mismatch", SO_REQ_CMB   }, //
	{ OPT_GAP_OPENING, (char*)"--gapop", SO_REQ_CMB   }, // 
	{ OPT_GAP_EXTENSION, (char*)"--gapext", SO_REQ_CMB   }, //
    { OPT_NOGAP,  (char*)"--no_gap",     SO_NONE    }, //
    { OPT_MAX_MIS, (char*)"--mmis",   SO_REQ_SEP       }, //
    { OPT_MAX_GAP, (char*)"--mgap",   SO_REQ_SEP       }, //
    { OPT_SIZE_MIN, (char*)"--lmin",   SO_REQ_SEP       }, //
    { OPT_SCORE_RATIO_MIN, (char*)"--srmin",   SO_REQ_SEP       }, //
    { OPT_LENGTH_RATIO_MIN, (char*)"--lrmin",   SO_REQ_SEP       }, //
#ifdef DEBUGLOG
    { OPT_LOG_LEVEL,  (char*)"-D",     SO_REQ_SEP    }, // "-D"
#endif
    //	Specific options of the dnaindex executable
    { OPT_MAX_COUNT, (char*) "-n",     SO_REQ_SEP    }, // "-n"
    { OPT_MASK,  (char*)"-M",     SO_REQ_SEP    }, // "-M"
    { OPT_BANK_NAME,  (char*)"-N",     SO_REQ_SEP   }, // "-w"
    { OPT_NB_PASS,  (char*)"-p",     SO_REQ_SEP    }, // "-p"
    //For the chaining algorithm
    { OPT_CUTOFFS,  (char*)"--cutoffs",     SO_REQ_SEP    },
    { OPT_MAXOVERLAP,  (char*)"--maxoverlap",     SO_REQ_SEP    },
    { OPT_GLINTOUT,  (char*)"--glintout",     SO_REQ_SEP    },
    { OPT_BLASTPOUT,  (char*)"--blastpout",     SO_REQ_SEP    },
    { OPT_ANCHORSOUT,  (char*)"--anchorsout",     SO_REQ_SEP    },
    { OPT_TARGETDIR,  (char*)"--targetdir",     SO_REQ_SEP    },
    { OPT_NORBH,  (char*)"--noRBH",     SO_NONE    }, //
    // To get some help
    { OPT_HELP, (char*)"-?",     SO_NONE    }, // "-?"
    { OPT_HELP, (char*)"-h",     SO_NONE    }, // "-?"
    { OPT_HELP, (char*)"--help", SO_NONE    }, // "--help"
    SO_END_OF_OPTIONS                       // END
};

