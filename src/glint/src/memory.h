/*
   $Id: memory.h 230 2008-09-12 13:16:00Z tfaraut $
*/


/* memory.h */


#ifndef _MEMORY_H
#define _MEMORY_H

#include <stdio.h>
#include <stdlib.h>
#include <vector>
using namespace std;

#include "ptypes.h"

void* memory_survey(int,char*);
void* rememory_survey(void *,int,int,char*);
void memory_print(void);

/*
    The class MemoryPool defines a very simplistic object used for allocating/releasing memory
    When needed, the memory is allocated by pools of 100 Mb
    Malloc reserves a few bytes on top of current pool
    Free does nothing, thus organizing massive memory leaks...
    FreeAll frees all the pools: memory allocation/deallocation is thus very fast,
    but not very flexible....
    There can be only ONE object of class MemoryPool in the program, it si a singleton (see ptypes.h)

*/

class MemoryPool: public Singleton<MemoryPool> {
    public:
    void* Malloc(size_t);
    void Free(void*ptr) {};
    void FreeAll();
    size_t MemoryUsed() { return mem_used;};
    size_t MemoryAllocated() { return mem_alloc;};
    friend class Singleton<MemoryPool>;

    protected:
    MemoryPool():pool_size(100*1048576),mem_used(0),mem_alloc(pool_size),current_pool(0) { //Size of a pool = 100Mb
        void* base = memory_survey(pool_size,(char*)"");
        pool.push_back((Uint1*)base);
        pool_top.push_back((Uint1*)base);
    };

    private:
    const size_t pool_size;
    size_t mem_used,mem_alloc;  // mem_used = memory used so far. mem_alloc = higher than mem_use, memory given by malloc calls
    int current_pool;
    vector<Uint1*>pool;         // The base address of each pool
    vector<Uint1*>pool_top;     // The top of each pool
};

/*
     The function MaxSize tries to determine the size of the memory (RAM, not virtual) of this computer
     It returns the number of bytes we may probably use
     We call malloc/free to really retrieve the wanted memory
     Parameters:
          -If 0, returns 80% of the total memory
          -If > 0, specifies the fraction of memory we are allowed to use (50 = the program may use half of the available memory).
     Retun value: The size of allocatable memory

*/
size_t MaxSize(int);

#endif
