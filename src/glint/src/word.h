/*
   $Id: word.h 263 2009-09-21 16:10:48Z tfaraut $
*/


/*   word.h   */

#ifndef _WORD_H
#define _WORD_H

#include <cstring>
#include <string>
#include "glint.h"
#include "masque.h"

using std::string;

/*

   WordDnaCode: All Dna Word classes derive from this tiny object
                Our genetic code is encapsulated here (...)

*/

class WordDnaCode {
    public:
    static int code(char c);
    static char decode(int c);
    static bool IsCode(char c) {return (code(c) < N);};

    private:
    enum {A,C,G,T,N};
};

/*

    Word: Deriving from WordDnacode
          This object is used to manipulate the characters read by Window
          There are 2 versions of the implementation:
                -Built upon a 32 bit memory word, for 32 bits architecture
                -Built upon a 64 bit memory word, more performant on 64 bits architecture

          Both versions have the same interface, but their implementations are completely different
          This was implemented with #define rather than with inheritance, for performance reasons.
          (we avoid the use of virtual functions in such a crticial part of the application)

*/

class DinucleotideComplexity;
class Window;

class Word: public WordDnaCode {
    public:

    /*

    The constructors

    1/ From an Uint1: initialize a word to 0 from a length information.
    2/ From a char *: The size is taken from the char * length
    3/ From another Word: the system's constructor is OK here

    */

    Word(Uint1 lg);
    Word(const char* w);

    /*
    Some accessors

    1/ GetSize returns the size of the Word (number of bytes)
    2/ GetLength returns the length of the word (number of char)
    3/ Getc returns the character whose position is passed by the parameter (0 = First character)
    */

    char GetSize() const { return size;};
    char GetLength() const {return length;};
    char Getc(int i) const;

    /*

    The Pushc, Popc, Getc functions

    Pushc: insert a new letter in the word by pushing everything to the left and adding with OR
    Since two Uint4 are used, a shift and push mechanism is needed: last character in r[0] (2 MSB)
    is extracted before shifting everything to the left, and added (ORed) to r[1]
    Popc:  The reverse of Pushc

    */

    void Pushc(char c);
    char Popc();

    /*

            The Conversion functions:
                1/ string Convert():             convert a word to a string
                2/ string Convert(Uint4,length): convert an Uint4 to a 'chaine', the length is passed by parameter
                          This latter function is static, because it is not related to any object
    */

    string Convert() const;
    static string Convert(Uint4 l,int length);

    /*
        The hashing functions:
        Hash            ==> Very simple, used for black_listing some words
    */

    int Hash(int sz) const;

    /*
             The Complement, ReverseComplement functions

             Those functions return a new word, which is the DNA complement or reverse complement of the current word.
             They return a Word*
             The coding of the nucleotides A:00 T:11 C:01 G:10 was devised in order to enable such an easy complementation
    */

    Word* Complement() const;
    Word* ReverseComplement() const;

    /*
       Some operators, very useful for using with containers
    */

    friend bool operator==(const Word&, const Word&);
    friend bool operator<(const Word&, const Word&);

    /* The friend classes */

    friend     class DinucleotideComplexity;
    friend     class Window;

    /* P R I V A T E     S E C T I O N */

    /* The private section is different for 32 bits and 64 bits impementation */
    /* THE FOLLOWING IS FOR A 32 BITS PROCESSOR, ALLOWING FOR MASKS AS LONG AS 32 CHARACTERS */

#ifdef WORD_IN_TWO_PARTS
#define REGISTER_SIZE 32

    private:
	Uint4 r[2];        /* A word of more than 16 letters in the {A,T,G,C} alphabet needs at least 2 unsigned long  */
	Uint4 m[2];        /* If less than 32 letters are wished a mask must be used for the remaining bits */
    Uint1 size;        /* size = The size of the word in bits */
    Uint1 length;      /* length = The number of characters of the word */
    void InitMask() { /* Called by the constructors */
        m[0]=0;
        m[1]=0;
        int j=0;
        for (int i=0;i<size;i++) {         /* The mask is 1 for every bit except for the discarded bits : i.e  00011111111 */
            if (i>=REGISTER_SIZE) j=1;
            m[j]<<=1;
            m[j]|=1;
        }
    };
};

#else

#define REGISTER_SIZE 64

    private:
	Uint8 r;        /* A 64 bits word, allowing for masks as long as 32 characters */
	Uint8 m;        /* If less than 32 letters are wished a mask must be used for the remaining bits */
    Uint1 size;        /* size = The size of the word in bytes */
    Uint1 length;      /* length = The number of characters of the word */
    void InitMask() { /* Called by the constructors */
        m=0;
        for (int i=0;i<size;i++) {         /* The mask is 1 for every bit except for the discarded bits : i.e  00011111111 */
            m <<=1;
            m|=1;

        }
    };
};

#endif

/* INLINE FUNCTIONS OR METHODS - 2 VERSIONS, 32 bits OR 64 bits */

/* The constructors */

#ifdef WORD_IN_TWO_PARTS
inline Word::Word(Uint1 lg): length(lg),size(lg*2) {r[0]=0;r[1]=0;InitMask();};   /* The parameter is a length information, init to 0 */
inline Word::Word(const char* w)
{
    length = strlen(w);
    size = 2 * length;                 /* 2 bits / letter */
    InitMask();
    for(int i=0;i<length;i++)
    {
        Pushc(w[i]);
    };
}
#else
inline Word::Word(Uint1 lg): r(0),size(lg*2),length(lg) {InitMask();}   /* The parameter is a size information, init to 0 */
inline Word::Word(const char* w)
{
    length = strlen(w);
    size = 2 * length;                 /* 2 bits / letter */
    InitMask();
    for(int i=0;i<length;i++)
    {
        Pushc(w[i]);
    };
}
#endif

/* The accessors */

#ifdef WORD_IN_TWO_PARTS

inline char Word::Getc(int i) const
{
    Uint4 m,l;
    int shift_count=size-2*(i+1);

    /* First computing the mask */
    if (shift_count>=REGISTER_SIZE) {    // Working with r[1]
        shift_count-=REGISTER_SIZE;
        m=3<<shift_count;
        l=r[1];
    } else {                 // Working with r[0]
        m=3<<shift_count;
        l=r[0];
    }
    l&=m;
    l>>=shift_count;

    return (char)l;
}

#else

inline char Word::Getc(int i) const
{
    int shift_count=size-2*(i+1);
    Uint8 mask = 3<<shift_count;
    Uint8 rvl = mask&r;
    rvl >>= shift_count;
    return (char)(rvl&3);
}

#endif

/* The Pushc, Popc functions */

#ifdef WORD_IN_TWO_PARTS

inline void Word::Pushc(char c)
{
    Uint4 l=r[0]>>(REGISTER_SIZE-2);
    r[1]<<=2;
    r[1]|=l;
    r[0]<<=2;
    r[0]|=code(c);
}

inline char Word::Popc()
{
    Uint4 b=r[0]&3;
    Uint4 c=r[1]&3;

    r[1]&=m[1];

    r[0]>>=2;
    r[1]>>=2;

    c<<=(REGISTER_SIZE-2);
    r[0]|=c;

    return decode((char)b);
}

#else

inline void Word::Pushc(char c) {
    r<<=2;
    r|=code(c);
}

inline char Word::Popc() {
    int b=r&3;
    r>>=2;
    return decode(b);
}

#endif

/* The Conversion functions */

#ifdef WORD_IN_TWO_PARTS

inline string Word::Convert() const
{
    string s="";
    for (int k=0;k<length;k++) {
        char c=Getc(k);
        s += decode(c);
    }
    return s;
}
inline string Word::Convert(Uint4 l,int length)
{
    int m=3;        // A mask
    string s="";
    for (int k=0;k<length;k++,l>>=2) {
        Uint4 c=m & l;
        s += decode(c);
    };
    return s;
}

#else

inline string Word::Convert() const
{
    string s="";
    for (int k=0;k<length;k++) {
        char c=Getc(k);
        s += decode(c);
    }
    return s;
}

inline string Word::Convert(Uint4 l,int length)
{
    int m=3;        // A mask
    string s="";
    for (int k=0;k<length;k++,l>>=2) {
        Uint4 c=m & l;
        s += decode(c);
    }
    return s;
}

#endif

/* The hashing functions */

#ifdef WORD_IN_TWO_PARTS

inline int Word::Hash(int sz) const {  /* A hashing function */
    return ((r[0]&m[0])+(r[1]&m[1])) % sz;
}

#else

inline int Word::Hash(int sz) const {  /* A hashing function */
    return (r&m) % sz;
}

#endif

#ifdef WORD_IN_TWO_PARTS

inline Word* Word::Complement() const {
    Word *w=new Word(size);
    w->r[0]=~r[0];
    w->r[1]=~r[1];
    return w;
}

inline Word* Word::ReverseComplement() const {
    Word *rw = new Word(size);
    Word *w  = Complement();
    int cnt = length;
    while (cnt--)
    {
        rw->Pushc(w->Popc());
    };
    delete w;
    return rw;
}

#else

inline Word* Word::Complement() const {
    Word *w=new Word(size);
    w->r=(~r)&m;
    return w;
}

inline Word* Word::ReverseComplement() const {
    Word *rw = new Word(length);
    Word *w  = Complement();
    int cnt = length;
    while (cnt--)
    {
        rw->Pushc(w->Popc());
    };
    delete w;
    return rw;
}

#endif

/* The operator==, <, != */

#ifdef WORD_IN_TWO_PARTS

inline bool operator==(const Word& a, const Word& b)
{
    return (((a.r[0]&a.m[0])==(b.r[0]&b.m[0])) && ((a.r[1]&a.m[1])==(b.r[1]&b.m[1])));
}
inline bool operator<(const Word& a, const Word& b)
{
    if (a.r[0]&a.m[0] != b.r[0]&b.m[0])
        return (a.r[1]&a.m[1] < b.r[1]&b.m[1]);
    else
        return (a.r[0]&a.m[0] < b.r[0]&b.m[0]);
}

#else

inline bool operator==(const Word& a, const Word& b)
{
    return ((a.r&a.m)==(b.r&b.m));
}
inline bool operator<(const Word& a, const Word& b)
{
    return ((a.r&a.m) < (b.r&b.m));
}

#endif

inline bool operator!=(const Word& a, const Word& b) { return !operator==(a,b);}

/*
   The WordDnacode methods

*/

inline int WordDnaCode::code(char c){
    switch (c)
    {
        case 'A' : return A;
        case 'C' : return C;
        case 'G' : return G;
        case 'T' : return T;
        default :  return N;
    }
}
inline char WordDnaCode::decode(int c){
    switch (c)
    {
        case A : return 'A';
        case C : return 'C';
        case G : return 'G';
        case T : return 'T';
        default :  return 'N';
    }
}


#endif
