/*
   $Id: utils.h 263 2009-09-21 16:10:48Z tfaraut $
*/


#ifndef __UTILS_H__
#define __UTILS_H__

#include "glint.h"

#include <stdexcept>
#include <stdio.h>

void SysDie (int, const char *);

Uint4 String2Uint4(const string& value);
Uint2 String2Uint2(const string& value);
char String2Char(const string& value);

FILE * open_file(const char *, const char *, const char* );

void bin_print(long n, int taille);
void bin_print_char(char n);
void bin_print_short(short n);
void bin_print_int(int n);
void bin_print_unlong(unsigned long n);

bool ValidCutoffParam( const string cutoffs );
bool ValidUint4(const string& s, bool failIfLeftoverChars = true);
bool ValidCovar(const string& s, bool failIfLeftoverChars = true);

class BadConversion : public std::runtime_error {
 public:
   BadConversion(const std::string& s)
     : std::runtime_error(s)
     { }
 };

inline Uint4 convertToUint4(const std::string& s)
{
  std::istringstream i(s);
  Uint4 x;
  if (!(i >> x))
    throw BadConversion("convertToUint4(\"" + s + "\")");
  return x;
}

inline float convertTofloat(const std::string& s)
{
  std::istringstream i(s);
  float x;
  if (!(i >> x))
    throw BadConversion("convertfloat(\"" + s + "\")");
  return x;
}


#endif
