/*
   $Id: ptypes.h 267 2010-02-19 15:28:48Z tfaraut $
*/


/* ptypes.h  */
/* Personal types for compatibility */


#ifndef _PTYPES_H
#define _PTYPES_H

typedef signed char Int1;
typedef short Int2;
typedef int Int4;

typedef unsigned char Uint1;
typedef unsigned short Uint2;
typedef unsigned int Uint4;
typedef unsigned long Uint8;

typedef long DiagInt;

enum EXIT
	{
	EXIT_Success = 0,
	EXIT_NotStarted = 1,
	EXIT_FatalError = 2,
	EXIT_Except = 3
	};

enum STRAND
	{
	WATSON = 0,
	CRICK = 1
	};

typedef enum orient {Minus, Plus} OrientType;

#define MAX_FASTA_HEADER_SIZE 1000                    /* The max number of characters allowed for the header line of the fasta files */
#define QBUFFER_INITIAL_SIZE  2000000                 /* The initial size of QBuffer, we realloc if necessary */

/*#define SBUFFER_INITIAL_SIZE   2000000*/                 /* The initial size of Sbuffer (used for indexation), we realloc if necessary */
#define SBUFFER_INITIAL_SIZE   62500                   /* The initial size of Sbuffer (used for indexation), we realloc if necessary */
#define SBUFFER_MAX_SIZE       20000000                /* The maximal size of Sbuffer, we dump the sbuffer if necessary */
/*#define SBUFFER_MAX_SIZE       4000000000*/                 /* The maximal size of Sbuffer, we dump the sbuffer if necessary */

#define DEFAULT_SLICE_SIZE 10000000

#define DEFAULT_TANDEM_SIZE 10000000000

#define SMALL_BUFFER_SIZE 255

#define SEQEND_BUFFER_SIZE 10
#define SEQEND '$'

/* Some data structure dimensions and other parameters */
#define BLACK_LIST_SIZE 60000                         /* The number of slots in the black list */

#ifndef HSP_PILE_SIZE
#define HSP_PILE_SIZE 50000      					/* The size of an hsp pile */
#endif

#define MATCH_PILE_SIZE 800000                        /* The size of a MatchPile */

#define ROOT_LEVEL 1                                  /* A lot of things will hang if ROOT_LEVEL != 1 */
#define MAX_LEAF_LEVEL 8                              /* Should be possible (but useless) to change this value */

#define ALIGN_CONSTANTS_DEF_PRMS 1,-3,-1,-5,-2,15,25  /* AlignConstants constructor parameters in the following order : match, mismatch, mismatch(match) with an N, gap opening penalty, gap extension penalty, dropoff for ungapped aligments, cutoff for gapped alignments  */

#define DEFAULT_MATCH 1
#define DEFAULT_MISMATCH -3
#define DEFAULT_NMATCH -1
#define DEFAULT_GAP_OPENING -5
#define DEFAULT_GAP_EXTENSION -2
#define DEFAULT_UNGAP_DROPOFF 15
#define DEFAULT_GAP_DROPOFF 25

#define MAX_SEQ_SIZE 1000
#define DISPLAY_LINE_SIZE 60

/* The complexity management */

#define COMPLEXITY_NONE          0
#define COMPLEXITY_DINUCLEOTIDES 1
#define COMPLEXITY_DUST          2
#define DEFAULT_COMPLEXITY       COMPLEXITY_DINUCLEOTIDES

/* glintindex */
#define DEFAULT_MAX_COUNT        100
//#define DEFAULT_MASK             "13112411211341"    /* The default Mask is now of 14 letters  */
//#define DEFAULT_MASK             "13112111211311"    /* The default Mask is now of 14 letters  */
#define DEFAULT_MASK             "130110211012110311"    /* The default Mask is now of 14 letters but of size 19 (0 are not counted)*/
#define FA_EXTENSION             ".fa"
#define BI_EXTENSION             ".bi"
#define HE_EXTENSION             ".he"
#define BL_EXTENSION             ".bl"

/*                          The different output formats
 *
 *   M1 : standard output format including a description and the full alignment
 *   M2 : a simple tabular output with the following header line:
 *         #query\tq_start\tq_end\tsubject\ts_start\ts_end\tstrand\tscore\tsize
 *   M3 : an improved tabular format with an editing script
 *   M4 : a format dedicated to analysis of the output
 *   M8 : a blast-like -m 8 output format (without header): a tab separated tabular ouput with the following fields (in thart order)
 *       q_name s_name pident size #mismatch #gaps  q_start q_end s_start s_end "1e-0" score
 *   M9 : a blast-like -m 9 output format (same as M8 but with a header)
 */

#define OUTPUT_FORMAT_M1  1
#define OUTPUT_FORMAT_M2  2
#define OUTPUT_FORMAT_M3  3
#define OUTPUT_FORMAT_M4  4
#define OUTPUT_FORMAT_M8  8
#define OUTPUT_FORMAT_M9  9
#define OUTPUT_FORMAT_M13  13
#define DEFAULT_OUTPUT_FORMAT    OUTPUT_FORMAT_M1

#define M2_HEADER "#q_name\tq_start\tq_end\ts_name\ts_start\ts_end\tstrand\tscore\tsize"
#define M3_HEADER "#query\tsubject\tescript([q_start-q_end]->[s_start-s_end](size,strand,score,evalue,pident)"

#define PLUS 1
#define MINUS -1

#define OUTPUT_PLUS  "Plus/Plus"
#define OUTPUT_MINUS "Plus/Minus"

/*
    Default values for Compare_Parameters
*/

#define DEFAULT_STEP 1

#define DEFAULT_CUTOFF_SLIDE  20
#define DEFAULT_CUTOFF_DYN    25
#define DEFAULT_CUTOFF_HSP    30
#define DEFAULT_LOWER_LEVEL    2
#define DEFAULT_DEEPEST_LEVEL  2

#define DEFAULT_BLASTZ_BEHAVIOR 0

/*
    Default values for Linking option of compare
*/

#define MAX_REPEATS 1000
#define DEFAULT_MAX_CHAIN_OVERLAP 50000
#define REPEAT_THRESHOLD_SIZE 1000

#define MAX_OVERLAP_HSP 100

#ifdef PROC64
typedef Uint8 RootLevelOffsetType;
#else
typedef Uint4 RootLevelOffsetType;
#endif


/*
    Classe SLogger = If not in DEBUG, use the trivial logger for speed
*/

#include "logger.h"
#ifdef DEBUGLOG
   typedef SLogger RealLogger;
#else
   typedef STrivialLogger RealLogger;
#endif

/*
    Class NonCopyable : The classes who declare a private inheritance from this class cannot
    redefine any operator=, nor any copy constructor - This is a useful security measure in several situations
*/

class NonCopyable
{
    public:
    NonCopyable(){};
    private:
    NonCopyable(const NonCopyable&) {};
    void operator=(const NonCopyable&) {};
};

/*
     Class Singleton: Some classes in this application are implemented as singletons
                      To have a class toto as a singleton, you must write:
                              class toto: public Singleton<toto> {
                                 protected:
                                     toto();
                                 public:
                                     friend class Singleton<toto>;
                                 ...
                              }
*/

template<typename T> class Singleton
{
    protected:
    Singleton () {};
//    ~Singleton () {}

    public:
    static T* Instance ()
    {
        if (self==NULL)
            self = new T;
        return (static_cast<T*> (self));
        //return (T*) self;
    }

    private:
    static T* self;
};

template <typename T> T* Singleton<T>::self = NULL;

#endif
