/*
   $Id: dust.cpp 230 2008-09-12 13:16:00Z tfaraut $
*/


/* dust.c */

#include <stdio.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#include "dust.h"

#define TRUE   1
#define FALSE  0

enum {A,C,G,T,N};

static int word = 3;
static int window = 64;
static int window2 = 32;
static int level = 20;

static int* zeros;
static int sizeof_zeros;
static int *counts;

static int mv, iv, jv;

void set_dust_level(int value)
{
	level = value;
}

void set_dust_window(int value)
{
	window = value;
	window2 = window / 2;
}

void set_dust_word(int value)
{
	word = value;
}

int __code(char c)
{
		switch (c)
		{
			case 'A' : return A;
			case 'C' : return C;
			case 'G' : return G;
			case 'T' : return T;
			default :  return N;
		}
}

void init_zeros(void)
{
	int i;

	sizeof_zeros=64*sizeof(int);

	counts=(int*)malloc(sizeof_zeros);
	zeros=(int*)malloc(sizeof_zeros);

	for (i=0;i<64;i++) zeros[i]=0;
}


void __wo1(int len,unsigned char* s,int ivv)
{
	static int n1=63;
	int c;
	int i, ii, j, v, t, sum;

	memcpy(counts,zeros,sizeof_zeros);

	i = 0;
	ii = 0;
	sum = 0;
	v = 0;
	for (j=0; j < len; j++, s++) {
		ii <<= 2;
		if ((c=__code(*s))<N) {
			ii|=c;
		} else {
			i = 0;
			continue;
		}
		ii &= n1;
		i++;
		if (i >= word) {
			if ((t = counts[ii]++) > 0) {
				sum += t;
				v = 10 * sum / j;
				if (mv < v) {
					mv = v;
					iv = ivv;
					jv = j;
				}
			}
		}
	}

}

/*

global modified : mv

*/

//static int wo(int len, unsigned char *s, int *beg, int *end)
int __wo(int len, unsigned char *s, int& beg, int& end)
{
	int i, l1;

	l1 = len - word + 1;
	if (l1 < 0) {
		//*beg = 0;
        beg = 0;
		//*end = len - 1;
        end = len - 1;
		return 0;
	}
	mv = 0;
	iv = 0;
	jv = 0;
	for (i=0; i < l1; i++) {
		__wo1(len-i, s+i, i);
	}
	//*beg = iv;
    beg = iv;
	//*end = iv + jv;
    end = iv + jv;
	return mv;
}

void dust(int len,unsigned char *s)
{
	int i, j, l, from, to, a, b, v;
	static int first_time=1;

	if (first_time) init_zeros();

	//fprintf(stderr,"Here dusting !\n");

	from = 0;
	to = -1;
	for (i=0; i < len; i += window2) {
		from -= window2;
		to -= window2;
		l = (len > i+window) ? window : len-i;
		//v = wo(l, s+i, &a, &b);
        v = __wo(l, s+i, a, b);
		for (j = from; j <= to; j++) {
			s[i+j] = tolower(s[i+j]);
			//s[i+j] = 'N';
		}
		if (v > level) {
			for (j = a; j <= b && j < window2; j++) {
				s[i+j] = tolower(s[i+j]);
				//s[i+j] = 'N';
			}
			from = j;
			to = b;
		} else {
			from = 0;
			to = -1;
		}
	}

	first_time=0;
}

#ifdef BLABLA
/*
  Old wo1 not any more in use
*/
static void old_wo1(int len,unsigned char * s,int ivv)
{
	//int i, ii, j, v, t, n, n1, sum;
    int i, ii, j, v, t, sum;
	static int counts[32*32*32];    // The letters are taken by groups of 3
	static int iis[32*32*32];
	int js, nis;

	//n = 32 * 32 * 32;
	static int n1 = 32*32*32 - 1;                // n1 = 2^15 - 1
	nis = 0;
	i = 0;
	ii = 0;
	sum = 0;
	v = 0;
	for (j=0; j < len; j++, s++) {
		ii <<= 5;               // * 32
		//if (isalpha(*s)) {
            if (*s!='$') {
			//if (islower(*s)) {
			//	ii |= *s - 'a';
			// } else {
				ii |= *s - 'A';
			//}
		} else {
			i = 0;
			continue;
		}
		ii &= n1;              // keep only last 3 iterations
		i++;
		if (i >= word) {        // skip the following for the first 2 iterations
			for (js=0; js < nis && iis[js] != ii; js++) ;
			if (js == nis) {
				iis[nis] = ii;
				counts[ii] = 0;
				nis++;
			}
			if ((t = counts[ii]) > 0) {
				sum += t;
				v = 10 * sum / j;
				if (mv < v) {
					mv = v;
					iv = ivv;
					jv = j;
				}
			}
			counts[ii]++;
		}
	}
}
#endif

