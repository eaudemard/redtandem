/*
   $Id: database.cpp 1437 2008-03-10 08:35:57Z tfaraut $
*/


#include "utils.h"
#include "database.h"
#include "options.h"


extern CSimpleOpt::SOption g_GlintOptions[];

/*
  Name:      The DataBase_Parameter constructors
  Parameters: Argc, Argv (ie the command line)
              A  help function
  Function:  Initialize an object of class DataBase_Parameters from the command line
*/
DataBase_Parameters::DataBase_Parameters(int argc, char** argv, void(*help)()):
Words(0),Length(0),Positions(0),nb_seq(0),msk(DEFAULT_MASK),Max_Count(DEFAULT_MAX_COUNT),
nb_pass(1),complexity(DEFAULT_COMPLEXITY),bi_bfr(NULL)
{
    if (argc == 1) {
		help();
		exit(0);
    }

	_init_node_width();

    CSimpleOpt args(argc, argv, g_GlintOptions);

	// while there are arguments left to process
	while (args.Next()) {
		if (args.LastError() == SO_SUCCESS) {
			if (args.OptionId() == OPT_HELP) {
				help();
				exit(0);
			}
			else if (args.OptionId() == OPT_NB_PASS )
			{
				nb_pass = atoi(args.OptionArg());
			}
			else if (args.OptionId() == OPT_MAX_COUNT )
			{
				Max_Count = atoi(args.OptionArg());
			}
            else if (args.OptionId() == OPT_COMPLEXITY )
            {
            	complexity = atoi(args.OptionArg());
            }
			else if (args.OptionId() == OPT_MASK)
			{
				msk = (string) args.OptionArg();
			}
			else if (args.OptionId() == OPT_BANK_NAME )
			{
				Name = (string) args.OptionArg();
			}
#ifdef DEBUGLOG
	        else if (args.OptionId() ==  OPT_LOG_LEVEL )
	        {
	        	log_level = (string) args.OptionArg();
	        }
#endif
            ;
		 }
		else {
			fprintf(stderr,"Invalid argument: %s\n", args.OptionText());
			fprintf(stderr,"Please use option --help to see the usage\n");
			exit(1);
		}
	}

	/* Commented out to enable auto-indexing mode
	 *
	 * if (args.FileCount()!=1)
	{
		fprintf(stderr,"Exactly one filename is required for indexing\n");
		fprintf(stderr,"Please use option --help to see the usage\n");
		exit(1);
	}*/

	FileName = (string) args.File(0);

	if (Name=="")
	{
		string::size_type pt = FileName.find_last_of('.');
		if (pt != string::npos && pt != 0)
			Name = FileName.substr(0,pt);
		else
			Name = FileName;
	}
	Validation();

}

/*
  Name:      DataBase_Parameter constructor from compare parameters
  Parameters: a reference on a Compare_Parameters object
  Function:  Initialize an object of class DataBase_Parameters from a Compare_Parameters object, mandatory for auto_indexing mode
*/
DataBase_Parameters::DataBase_Parameters(const Compare_Parameters& compare):
FileName(compare.bank_filename),Words(0),Length(0),Positions(0),nb_seq(0),msk(compare.msk),Max_Count(compare.Max_Count),
nb_pass(1),complexity(compare.complexity),bi_bfr(NULL)
{
	_init_node_width();
	
	Validation();
}

/*
Methods for the class DataBase_Parameters: given a position in the database (.fa file), return
the corresponding SequencePosition
*/

Sequence_Positions DataBase_Parameters::GetTrueSeqPos(Uint4 pos) const
{
	vector<Sequence_Positions>::const_iterator left,right,middle;
	Uint8 size=GetNbSeq();

	left=GetSeqInfo().begin();
	right=left+size-1;

	while (size>1) {
		middle=left+(Uint8)(size/2);
		if (pos < middle->start) {
			right = middle;
		} else {
			left = middle;
		}
		size=(Uint8)(size+1)/2;
	}
	return *left;
}

/*
  Name:      Validation
  Parameters: none
  Function:  exit if something wrong with the parameters
*/

void DataBase_Parameters::Validation()
{
    bool ok = true;
    if (nb_pass<1 || nb_pass >10)
    {
        cerr << "ERROR - nb_pass should be in the interval 1..9\n";
        ok = false;
    };
    if (Max_Count<2 || Max_Count>255)
    {
        cerr << "ERROR - Max_Count should be in the interval 2..255\n";
        ok = false;
    };
    if (FileName=="")
    {
        cerr << "ERROR - Illegal file name\n";
        ok = false;
    };
    if (complexity>2)
    {
        cerr << "ERROR - Complexity should be 0,1,2\n";
        ok = false;
    };
    if (!ok)
        exit(1);
}


/*
  Name:      The DataBase_Parameter constructors
  Parameter: A file
  Function:  Initialize an object of class DataBase_Parameters from a .he file
*/

DataBase_Parameters::DataBase_Parameters(const string& index_name): msk(DEFAULT_MASK)
{
    _init_node_width();
    string file_name = (string)index_name + (string) HE_EXTENSION;
    string tmp, str_msk;
    ifstream is(file_name.c_str());

    this->FileName=(string)index_name;

	 /* Initializing the fields from the file -
       WARNING - We do not use the NAME of the field, so the ORDER is important in the .he file !!!
       TODO - Arranger cela MANU
    */

    is >> tmp >> this->Name;
    is >> tmp >> str_msk;
    if (str_msk != DEFAULT_MASK)
        this->msk = str_msk;      // Initialize the mask from str_msk if not already correct
    is >> tmp >> this->Size;
    is >> tmp >> this->Words;
    is >> tmp >> this->Length;
    is >> tmp >> this->nb_seq;
    is >> tmp >> this->complexity;
    is >> tmp;
    for (int i=ROOT_LEVEL;i<=MAX_LEAF_LEVEL;i++)
        is >> this->node_width[i];
    for (int i=0; i < this->nb_seq; i++)
    {
        Sequence_Positions sp;
        is >> tmp >> sp;
        this->seq_info.push_back(sp);
    }

    /*fprintf(stderr,"Il y a %u mots pour %u bases et %d sequence(s), index size %lu\n",this->GetNbWords(),this->GetLength(),this->GetNbSeq(),this->GetSize());
	 fflush(stderr);*/
}   // closing the file

/*
  Name:      The DataBase: operator<<
  Usage:     cout << dabase;
  Function:  The standard output operator of the struct DataBase_Parameters
*/


ostream & operator<<(ostream& os, DataBase_Parameters const & db) {
    os << "NAME " << db.Name << '\n';
    os << "MASK "<< db.GetMsk().GetString() << '\n';
    os << "INDEX_SIZE "<< db.Size << '\n';
    os << "WORDS "<< db.Words << '\n';
    os << "LENGTH " << db.Length << '\n';
    os << "SEQ " << db.nb_seq <<'\n';
    os << "COMPLEXITY " << db.complexity << '\n';
    os << "NODE_WIDTH ";
    for (int i=ROOT_LEVEL;i<=MAX_LEAF_LEVEL;i++)
        os << db.node_width[i] << ' ';
    os << '\n';
    for (int i=0; i < db.nb_seq; i++)
        os << "SEQUENCE " << db.seq_info[i] << '\n';
    return os;
}

ostream & operator<<(ostream& os, Sequence_Positions const & sp) {
    os << sp.start << ' ' << sp.Name;
    return os;
}
istream& operator>>(istream& is, Sequence_Positions& sp) {
    is >> sp.start >> sp.Name;
    return is;
}


