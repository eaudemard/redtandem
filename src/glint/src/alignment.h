/*
   $Id: alignment.h 266 2010-02-16 08:44:32Z tfaraut $
*/


#ifndef __ALIGN_H__
#define __ALIGN_H__

#include <stdio.h>
#include <stdlib.h>
#include "ptypes.h"
#include "scoring.h"
#include <algorithm>
#include <list>
//PROVISOIRE
#include <iostream>

#include "database.h"
#include "compare.h"
#include "logger.h"
#include "hsp.h"

enum {NONE, DIAG, LEFT, ABOVE, OPEN_Q, OPEN_S, CONT_GQ, CONT_GS, DONE};

/*
    The objects related to the alignment
        Associations:
            AlignDirection     <---> AlignEngine
            AlignEngine ---> QBuffer
            AlignEngine ---> Alignconstants

        Dependencies:
             AlignDirection     <---> AlignEngine

*/

class AlignEngine;
struct AlignDirection {
    AlignDirection(const AlignEngine &a):results(a){};
    void SetDirection(int align_move, int strand);

    friend class AlignEngine;
    private:
	Uint4 query_start;
	Uint4 subject_start;
	Int1 move;
	Int1 query_move;
	Int1 subject_move;
	Int1 strand;

    private:
    const AlignEngine & results;
};

class match;
struct AlignEngine {
    AlignEngine(int size,const QBuffer & q);

    friend class MatchPile;
    friend class AlignDirection;

    Int2 LocalAlign(hsp*);
    DiagInt ToHsp(hsp&);
    void _GetSeqInfo(hsp&);
    Uint2 _RecordHspScript(hsp& h) const;
    bool ValidHsp(hsp& );

    Uint2 GetScore() { return score_align;};
	Uint4 GetQueryStart() { return query_start;};
    Uint4 GetQueryEnd() { return query_end;};
	Uint4 GetSubjectStart() { return subject_start;};
    Uint4 GetSubjectEnd() { return subject_end;};

    int RecordAlignment(hsp* h);
    void SetAlignLeft(int strandness) { align_direction.SetDirection(-1,strandness);};
    void SetAlignRight(int strandness) { align_direction.SetDirection(1,strandness);};

    int LookRight(match **,Uint4, Uint4 ,int);
    int LookLeft(match **,Uint4, Uint4 ,int);

    void show_score_matrix(int , int , int , int , FILE *);

private:
	Int2 **S;
	Uint1 **T;
	Int2 *F;
	Uint1 **TE;
	Uint1 **TF;
	Uint2 align_size_max;
	Int4 bestScore;
	Uint2 bestSi;
	Uint2 bestSj;
	Uint4 bestSi_left;
	Uint4 bestSi_right;
	Uint4 bestSj_left;
	Uint4 bestSj_right;
	Uint4 query_start;
	Uint4 query_end;
	Uint4 subject_start;
	Uint4 subject_end;
	Uint2 query_gap;
	Uint2 subject_gap;
	Int1 strand;
	Uint4 score_align;
	Uint4 size;
	
	Uint4 max_seq_size;
	
    void _InitScoreMatrix( );
    void _InitTraceMatrix( );
    //void _ResetTraceMatrix( ); Deprecated
    
    AlignConstants scoring;
    AlignDirection align_direction;
    const QBuffer & query;

    // Used by _RecordHspScript (stores the Watson-Crick relation A <->T etc.)
    int *Rev;

};


// An Idea to think about if we want to separate self alignments from others
/*class AlignEngineSelf:  public AlignEngine {
	public:
		AlignEngineSelf(int size,const QBuffer & q) : AlignEngine(size,q);
};*/



#endif
