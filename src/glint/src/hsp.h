/*
   $Id: alignment.h 222 2008-05-28 06:35:44Z tfaraut $
*/


#ifndef __HSP_H__
#define __HSP_H__

#include <stdio.h>
#include <stdlib.h>
#include "ptypes.h"
#include "scoring.h"
#include <algorithm>
#include <list>
//PROVISOIRE
#include <iostream>

#include "database.h"
#include "compare.h"
#include "logger.h"

enum _AlignStatus {ALIGNED,GAPED};
typedef enum _AlignStatus AlignStatus;

/*

     A GapPile is a "stack" of gaps.
     It encapsulates a vector of Int2 (the gap positions)
     A new gap can be added with the Push function
     All already stored gaps can be incremented with the Increment function
     The gaps can be stored with the Sort() function
     With begin() and end(), you can iterate throug the object (const iterators only)

*/


typedef Uint4 Gap;
typedef vector<Gap>::const_iterator GapPile_const_iterator;

class GapPile {
    public:
    void Push(Uint4 g);
    void Increment(Uint4 l);
    void Clear();
    void Sort();

    void Print(FILE *pf);
    Uint2 GetSize() const;

    typedef vector<Uint4>::const_iterator const_iterator;
    const_iterator begin() const;
    const_iterator end() const;

    void PrintDebug(FILE *pf);

	friend class AlignEngine;

    private:
    vector<Gap> gaps;
};

/*
      An hsp
      The well-known Highest Segment Pair concept : a local alignment between a query and a subject
      with the following information [q_start:q_end]-->[s_start:s_end] a score, etc
*/

struct hsp {
    hsp(): excluded(false)  {};
	Uint4 q_start, q_end, s_start, s_end;
	Int4 min_diag,max_diag;
	Int1 strand;
	Uint4 score;
	Uint4 size;
    GapPile q_gap, s_gap, gap_opening, mismatch;
    Sequence_Positions true_seq;
    Sequence_Positions true_seq_query;
	bool excluded;
	Uint2 order[2];
/* For debugging purpose only */
	Uint2 seed_number;
	Uint4 maxscore;

	friend ostream & operator<<(ostream& os, const hsp &);
};



/*
     An HspPile encapsulates a lot of pre-allocated hsp objects.
     The number of hsp is passed by the constructor.

     GetTop may be used to retrieve the highest stored hsp
     Commit is used to "record" the top hsp in the object, and to propose a new top
     Lookup is used to check overlaps between  new hsps and already committed hsps
     When committing, if every preallocated is committed, AND if we are working in OutputStream mode,
     the hsp are printed

     Associations:
        HspPile ---> Display

*/

class match;
class Display;
typedef vector<hsp*>::iterator HspIterator;            // const_iterator SERAIT PLUS LOGIQUE, MAIS ON PEUT PAS
class HspPile {
    public:

    HspPile(Uint2 size,const Display& d);


    /* Commit increments top, WITHOUT increasing the size of p
       If top is at the highest, p and h_list are dumped and cleared */

     void Commit();
	 void SwitchPiles();
     void Sort();
	 void SortQue();
	 void SortSub();
	 void SortQue1();
	 void SortSub1();
     void SortAndDisplayHsps(FILE *pf);    // was display_all_hsp
     void PrintDebug(FILE *pf);  //For debugging purpose

	void PrintDebugNumbering(const vector<hsp*>& v,int len,FILE *pf) const;
    void Reset();
	void PrintDebugDisplay();
    bool Lookup (const match&);
    bool Lookup (const hsp&);

	 void PrintOuput();

	 /* For debugging purpose only  */

	 void PrintSortedPile();
	 void PrintPile();

    /* return the top of the pile, slightly clearing the returned hsp */
    hsp* GetTop();

    HspIterator begin();
    HspIterator end();

	HspIterator beginhsp();
    HspIterator endhsp();

    private:
    bool _LookupCurrent(Int4, Uint4);
    bool _LookupPrevious(Uint4, Uint4);
    void _Insert(hsp*);         // Insert an hsp in the list hsp_list
	 Uint2 top;
    bool hsp_prev_empty;        // if true, there is nothing interesting in hsp_prev
    vector<hsp*>*hsp_prev;       // The 'old' preallocated hsps
    vector<hsp*>*hsp_current;     // The 'current' preallocated hsps
    list<hsp*> hsp_list;         // The committed hsps, sorted as specified by _Insert
	list<hsp*> hsp_valid;         // The valid hsps, selected by DetectOverlappingHsps
    //list<chain*> hsp_chains;     // The chained hsps, as constructed by Chaining

	 const Display& display;
};

/*
	overlap

	A structure for handling overlapping hsps
*/


typedef vector<hsp*>::iterator HspIterator;

struct Overlap {
	Overlap(hsp *f,hsp *s) : first(f),second(s) {};
	hsp* first;
	hsp* second;
};

typedef vector<Overlap*>::iterator OverlapIterator;

inline void ExcludeOverlappingHsp(const Overlap* o)
{
	if (!o->first->excluded && !o->second->excluded) o->second->excluded=1;
}

inline void ExcludeRepeatHsp(const Overlap* o)
{
	o->first->excluded=1;
	o->second->excluded=1;
}

/*
	HspPileLight

	A light HspPile object for linking purpose
*/


class HspPileLight {
	public:

	bool Lookup (const hsp&);
	void _InsertQueryEndSorted(hsp*);              // Insert an hsp in the list hsp_list
	void PrintDebug(FILE *pf);                     //For debugging purpose
	void LookupOverlap(hsp *h,list<Overlap*>& o);

	private:


	list<hsp*>hsp_list;         // The hsps, sorted as specified by _Insert
};

/*
     hit, HitPile: A hit is a set of hsp belonging to the same query sequence
     THOMAS ET LE SCORE ???
     HitPile is a "pile" of hits. The destructor keeps the already allocated hits
     in a "reserve", thus avoiding repeated calls to alloc/free memory
*/

struct hit {
    hit() : score(0),Name("") {};
	Int2 score;
	string Name;
    vector<hsp*> hsps;
};
bool operator< (const hit&, const hit&);        // Used to sort the hits

/*
      HitPile: a "pile" of hits, used to print several HspPiles together
               The destructor keeps the already allocated hits
               in a "reserve", thus avoiding repeated calls to alloc/free memory
      Associations:
           -HitPile  --> Display
*/
class Display;
class HitPile {
    public:
    HitPile(const Display& d): display(d){};
    /* Constructor/destructor */

    /* Adding a new hit to the pile */
    hit* NewHit();
	Uint4 Size() const { return p.size(); };
    /* Iterating through the hits */
    typedef list<hit>::const_iterator const_iterator;
    const_iterator begin() const;
    const_iterator end() const;

    /* Sorting the hit pile */
    void Sort();

    private:
    const Display& display;
	list<hit> p;
    static list<hit>reserve;
};


/* The inline methods of those classes */

inline void GapPile::Push(Uint4 g) {gaps.push_back(g);}
inline void GapPile::Increment(Uint4 l) { for(Uint2 i=0;i<gaps.size();i++) gaps[i]+=l;}
inline void GapPile::Clear() {gaps.clear();}
inline void GapPile::Sort() { sort(gaps.begin(),gaps.end());}
inline void GapPile::Print(FILE *pf)
{
    for(Uint2 i=0;i<gaps.size();i++)
    {
        if (i>0) fprintf(pf,",");
        fprintf(pf,"%d",gaps[i]);
    }
}
inline Uint2 GapPile::GetSize() const { return gaps.size();}
inline GapPile::const_iterator GapPile::begin()   const {return gaps.begin();}
inline GapPile::const_iterator GapPile::end()     const {return gaps.end();}
inline void GapPile::PrintDebug(FILE *pf)
{
    fprintf(pf,"gap pile \n");
    for(Uint2 i=0;i<gaps.size();i++) {
        fprintf(pf,"%d \t",gaps[i]);
    }
    fprintf(pf,"\n");
}

/*
      The hsp comparison operator and functions
*/

bool operator<(const hsp& p, const hsp& q);

inline bool CompHspPtr(const hsp* p, const hsp* q) {return *p<*q;}

inline bool CompHspQuePtr(const hsp* p, const hsp* q)
{
 return 	(p->q_start<q->q_start);
}

inline bool CompHspSubPtr(const hsp* p, const hsp* q)
{
 return 	(p->s_start<q->s_start);
}

inline bool CompHspScorePtr(const hsp* p, const hsp* q)
{
 return 	(p->score>q->score);
}


inline Uint4 StartHsp(const hsp* p,const unsigned char i)
{
	return (i==0) ? p->q_start : p->s_start;
}

inline Uint4 EndHsp(const hsp* p,const unsigned char i)
{
	return (i==0) ? p->q_end : p->s_end;
}

inline HspPile::HspPile(Uint2 size,const Display& d) : top(0),hsp_prev_empty(true),display(d)  {
    hsp_prev = new vector<hsp*>;
    hsp_current = new vector<hsp*>;
    for(int j=0; j<size; j++) {
        hsp_current->push_back(new hsp);
        hsp_prev->push_back(new hsp);
    };
}

inline void HspPile::Sort() {sort(hsp_current->begin(),hsp_current->begin()+top,CompHspPtr);}
inline void HspPile::SortQue1() {sort(hsp_current->begin(),hsp_current->begin()+top,CompHspQuePtr);}
inline void HspPile::SortSub1() {sort(hsp_current->begin(),hsp_current->begin()+top,CompHspSubPtr);}
inline void HspPile::SortQue() {hsp_valid.sort(CompHspQuePtr);}
inline void HspPile::SortSub() {hsp_valid.sort(CompHspSubPtr);}
inline void HspPile::Reset()  { top=0; hsp_list.clear();}
inline hsp* HspPile::GetTop() {
    hsp *h=(*hsp_current)[top];
    h->q_gap.Clear();
    h->s_gap.Clear();
    h->mismatch.Clear();
    h->gap_opening.Clear();


    return h;
}

inline HspIterator HspPile::begin() {return hsp_current->begin();}
inline HspIterator HspPile::end()   {return hsp_current->begin()+top;}

inline hit* HitPile::NewHit() {             // Was new_hit
    if (reserve.empty()) {  // If the reserve is empty, create a new hit
        hit* h = new hit();
        p.push_back(*h);     // WARNING !!! We store a copy of *h, but we DO NOT have any operator=
        delete(h);           // Stupid, but provisoire
        return &(p.back());
    }
    else                    // If the reserve is not empty, w<e take a hit from the reserve
    {
        p.splice(p.end(),reserve,reserve.begin());
        return &(p.back());
    };
}
inline HitPile::const_iterator HitPile::begin() const {return p.begin();}
inline HitPile::const_iterator HitPile::end() const {return p.end();}
inline void HitPile::Sort() {p.sort();}


/*----------------------------------------------------------------------------------

                   Classes and methods for displaying results

-----------------------------------------------------------------------------------*/


/*
    The struct AlignDisplayLine is used by the friends DisplayFull and displayScript to display an aligned line

*/

class DisplayM1;
class DisplayScript;
struct AlignDisplayLine {
    public:
    AlignDisplayLine();

    friend class DisplayM1;
    friend class DisplayM3;

    private:
	Uint4 query_ind, subject_ind;
	int *sincr;
	Int1 strand;
	char * alignment[3];
};

/*

    The class Display is an abstract class, it is the base class of several DisplayXXX classes

*/

class QBuffer;
class Display {

	public:
    Display(const QBuffer& q,FILE* f);
    // A class with virtual methods must have a virtual destructor
    virtual ~Display(){};

    //The hierarchy of methods called to display hsps
    virtual void DisplayTopHeader() const {};
    virtual void DisplayHeader(const string& query,const string& bank) const { };
    virtual void DisplayQueryInfo(const string& query) const { };
    virtual void DisplaySubjectInfo(const string& subject,int score) const { };
    virtual Uint2 DisplayHsp(hsp *h) const = 0;
    virtual void DisplayHits(vector<hsp*>& v,int len=-1) const;
    virtual void DisplayResult(const HitPile&) const;
    
    void DisplayHit(const hit*) const;

    void SortAndDisplayHits(vector<hsp*>& v,int len=-1) const;

    const QBuffer& query;
    FILE *dsp_file;

    protected:
    void _InitIncrDecr();
    int *Rev;
    int *Incr, *Decr;

};


class DisplayM1: public Display {

public:
    DisplayM1(const QBuffer& q,FILE* f) : Display(q,f), display_line() {};

    /*The hierarchy of methods called to display hsps for the M1 class  */
    virtual void DisplayTopHeader() const;
    virtual void DisplayHeader(const string& query,const string& bank) const;
    virtual void DisplayQueryInfo(const string& query) const;
    virtual void DisplaySubjectInfo(const string& subject,int score) const ;
    virtual Uint2 DisplayHsp(hsp *h) const;

    void DisplayHitPile(const HitPile&) const {}; //Should be deprecated .....

    private:
    Uint2 _DisplayFullHsp(hsp *h) const;
    void _DisplayVerboseHspInfo(hsp *h) const;
    void _DisplayPrettyAlignmentLine(Uint2 max) const;

    protected:
    virtual void _DisplayHspInfo(hsp *h) const;
    mutable AlignDisplayLine display_line;
};

class DisplayM2: public Display {
    public:
    DisplayM2(const QBuffer& q,FILE* f) : Display(q,f){ };

    /*The hierarchy of methods called to display hsps for the M2 class  */
    virtual void DisplayTopHeader() const;
    virtual void DisplayHeader(const string& query,const string& bank) const { };
    virtual void DisplayQueryInfo(const string& query) const { };
    virtual void DisplaySubjectInfo(const string& subject,int score) const { };
    virtual Uint2 DisplayHsp(hsp *h) const;

};

class DisplayM3: public DisplayM1 {
    public:
    DisplayM3(const QBuffer& q,FILE* f) : DisplayM1(q,f){ };

    /*The hierarchy of methods called to display hsps for the M3 class  */
    virtual void DisplayTopHeader() const;
    virtual void DisplayHeader(const string& query,const string& bank) const { };
    virtual void DisplayQueryInfo(const string& query) const { };
    virtual void DisplaySubjectInfo(const string& subject,int score) const { };
    virtual Uint2 DisplayHsp(hsp *h) const;

    protected:
    virtual void _DisplayHspInfo(hsp *h) const;
    //Uint2 _RecordHspScript(hsp *h) const;

};

class DisplayM4: public DisplayM2 {
    public:
    DisplayM4(const QBuffer& q,FILE* f) : DisplayM2(q,f){};

    void DisplayTopHeader() const;

    protected:
    Uint2 DisplayHsp(hsp *h) const;
};

class DisplayM8: public DisplayM3 {
    public:
    DisplayM8(const QBuffer& q,FILE* f): DisplayM3(q,f){};

    void DisplayTopHeader() const;
    virtual void DisplayHeader(const string& query,const string& bank) const { };
    Uint2 DisplayHsp(hsp *h) const;
    void _DisplayHspInfo(hsp *h) const;
};

class DisplayM9: public DisplayM8 {
    public:
    DisplayM9(const QBuffer& q,FILE* f): DisplayM8(q,f){};

    void DisplayHeader(const string& query,const string& bank) const;
};

class DisplayM13: public Display {
	public:
    DisplayM13(const QBuffer& q,FILE* f) : Display(q,f){};
	
	void DisplayResult(const HitPile&) const;  
    Uint2 DisplayHsp(hsp *h) const {};
};

#ifdef BLABLA
class DisplayAnalysis: public DisplayTabular {
    public:
    DisplayAnalysis(const QBuffer& q,FILE* f): DisplayTabular(q,f){};

	 private:
	 virtual void DisplayHspTabular(hsp *h) const;
};
#endif

inline AlignDisplayLine::AlignDisplayLine(): query_ind(0),subject_ind(0),sincr(NULL),strand(0){
    for (int i=0;i<3;i++) {
        alignment[i]=new char[DISPLAY_LINE_SIZE];
    }
}

inline Display::Display(const QBuffer& q,FILE* f): query(q),dsp_file(f)
{
    Rev=(int*)malloc(256*sizeof(int));
    for (int i=0;i<256;i++) {
        Rev[i]=i;
    }

    Rev['A']='T';
    Rev['T']='A';
    Rev['C']='G';
    Rev['G']='C';
    Rev['a']='t';
    Rev['t']='a';
    Rev['c']='g';
    Rev['g']='c';

    Rev['N']='N';

    _InitIncrDecr();
}

#endif
