/*
   $Id: window.h 230 2008-09-12 13:16:00Z tfaraut $
*/


/* window.h */


#ifndef _WINDOW_H
#define _WINDOW_H

#include "ptypes.h"
#include "complexity.h"
#include "word.h"

class Window: private NonCopyable {
    public:
    /* The constructor - Parameter = The StringMask, from which the length of the window (number of characters) is computed */
    Window(const Mask& msk);
    /*
    Shifting or resetting the window:
       -Shift shifts the window
       -ShiftComp shifts the window and computes the complexity
       -Reset resets the window
       -ResetComp resets the window AND the comp object
    */

    void Shift(char c);
    void ShiftComp(char c);

    void Reset();
    void ResetComp();

    /* Checking the window: is it full or not ? is there enough complexity or not ? What is the length ?*/
    bool IsFull() const{ return (pos==size);};
    bool IsEmpty() const {return pos==0;};
    bool IsComplex() const{return comp.IsComplex();};
    Uint1 GetLength() const {return length;};
    Uint1 GetSize() const {return size;};

    /* Retrieve the Word and the mask, but only for using the accessor functions (the objects retrieved are const) */
    const Word& GetWord() const {return w;};
    const Mask& GetMask() const {return m;};

    /*
        The hashing functions used to store and retrieve the "words" in the tree:

        -TreeHash        ==> Used in conjunction with the mask for storing and retrieving entries in the tree, based upon the word
                             handled by the window
        -ReverseTreeHash ==> Same as TreeHash, but for the reverse complement word

    */

    Uint4 TreeHash(Uint1 level) const;
    Uint4 ReverseTreeHash(Uint1 level) const;

    private:

    Mask m;             /* The mask, used by the hashing functions */
    Word w;             /* The word encapsulated by the window */
    Uint1 length;         /* Length in characters */
    Uint1 size;           /* Size (in bits) */
    int pos;            /* The position of the window. In bits */
    Complexity comp;    /* The complexity object */

    mutable Uint4 tree_hash[MAX_LEAF_LEVEL+1];     // Caching the results of ThreeHash for all the levels
    mutable bool valid_tree_hash;
    mutable Uint4 rev_tree_hash[MAX_LEAF_LEVEL+1]; // Caching the results of ReverseThreeHash for all the levels
    mutable bool valid_rev_tree_hash;
};


/* THE inline METHODS OF THE OBJECT: Window */

inline Window::Window(const Mask& msk): m(msk),w(msk.GetLength()),
length(msk.GetLength()),size(2*msk.GetLength()),pos(0),comp(4),
valid_tree_hash(false),valid_rev_tree_hash(false)
{
    for(int i =0; i<=MAX_LEAF_LEVEL;i++)
    {
        tree_hash[i]=0;rev_tree_hash[i]=0;
    };
}

inline void Window::Shift(char c) {
    valid_tree_hash     = false;
    valid_rev_tree_hash = false;
    w.Pushc(c);
    if (!IsFull())
        pos += 2;
}

inline void Window::ShiftComp(char c) {
    valid_tree_hash     = false;
    valid_rev_tree_hash = false;
    if (IsFull()) comp.Decrease(w);   // Update the complexity object only if the window is full
    else pos += 2;                     // Widen the windonw if not full
    w.Pushc(c);
    if (pos >= 4)                      // Update the complexity object only if the window is not empty
        comp.Increase(w);
}

inline void Window::Reset() {
    pos=0;
    comp.Reset();
    valid_tree_hash=false;
    valid_rev_tree_hash=false;
}   // Clear position and reset the Complexity tool

inline void Window::ResetComp() {
    pos=0;
    valid_tree_hash=false;
    valid_rev_tree_hash=false;
}   // Clear position and reset the Complexity tool

/* There is a dependance between Window and Word (Window is a friend of Word)
   We have 2 versions for the hashing functions
*/


#ifndef WORD_IN_TWO_PARTS

inline Uint4 Window::TreeHash(Uint1 level) const
{
    if (valid_tree_hash) return tree_hash[level];
    Uint8 r    = w.r;
    int count[MAX_LEAF_LEVEL+1];
    for (int i=0; i<=m.leaf_level; i++) {
        tree_hash[i]=0;
        count[i]=0;
    };

    for (int i=length-1; i>=0; i--)
    {
        int level = m.pos[i];      // The level of the mask at this position
        int c=r&3;                 // keep the last 2 bits of r
        r>>=2;                     // rotate r
        c <<= 2*count[level]++;    // shift c, incrementing the count
        tree_hash[level] |= c;     // Update the hash for this level
    };
    valid_tree_hash = true;

    /*JUST FOR DEBUG
    cerr << w.Convert() << '(' << (int)level << ")  ";
    for (int i=1; i<=m.leaf_level; i++)
    {
        cerr << ' ' << hex << tree_hash[i];
    }
    cerr << '\n';
    *//*
    cerr << w.Convert() << '*';
    for (int i=1; i<=m.leaf_level; i++)
    {
        cerr << ' ' << hex << TreeHash_OLD(i);
    }
    cerr << '\n' << '\n';

    ReverseTreeHash(level);

    cerr << w.Convert() << ' ' << (int)level << ' ' << hex << tree_hash[level] << '\n';
    */
    return tree_hash[level];       // return the hash at the good level
}

inline Uint4 Window::ReverseTreeHash(Uint1 level) const
{
    if (valid_rev_tree_hash) return rev_tree_hash[level];
    Uint8 r   = ~w.r;
    Uint8 msk = (((Uint8)3)<<(w.size-2));

    int count[MAX_LEAF_LEVEL];
    for (int i=0; i<=m.leaf_level; i++) {
        rev_tree_hash[i]=0;
        count[i]=0;
    };


    for (int i=0; i<length; i++)
    {
        int level = m.pos[length-i-1];      // The level of the mask at this position
        Uint8 c=((r&msk)>>(w.size-2))&3; // keep the first 2 useful bits of ~r
        r<<=2;                     // rotate r
        c <<= 2*count[level]++;    // shift c, incrementing the count
        rev_tree_hash[level] |= c; // Update the hash for this level
    };
    valid_rev_tree_hash = true;

    /*JUST FOR DEBUG
    cerr << w.ReverseComplement()->Convert() << '(' << (int)level << "R) ";
    for (int i=0; i<=MAX_LEAF_LEVEL; i++)
    {
        cerr << ' ' << hex << rev_tree_hash[i];
    }
    cerr << '\n';
    */
    /*cerr << w.ReverseComplement()->Convert() << '!';
    for (int i=1; i<=m.leaf_level; i++)
    {
        cerr << ' ' << hex << ReverseTreeHash_OLD(i);
    }
    cerr << '\n' << '\n';
    */

    return rev_tree_hash[level];       // return the hash at the good level
}

#else

inline Uint4 Window::TreeHash(int level) const
{
    if (valid_tree_hash) return tree_hash[level];
    Uint4 r[2];
    r[0] = w.r[0];
    r[1] = w.r[1];

    int count[MAX_LEAF_LEVEL+1];
    for (int i=0; i<=m.leaf_level; i++) {
        tree_hash[i]=0;
        count[i]=0;
    };

    for (int i=length-1; i>=0; i--)
    {
        int lvl = m.pos[i];      // The level of the mask at this position
        int c=r[0]&3;              // keep the last 2 bits of r
        c <<= 2*count[lvl]++;    // shift c, incrementing the count
        tree_hash[lvl] |= c;     // Update the hash for this level

        r[0]>>=2;                     // rotate r[0],r[1]
        c=r[1]&3;
        r[0] |= c<<(REGISTER_SIZE-2);
        r[1]>>=2;
    };
    valid_tree_hash = true;

    /*JUST FOR DEBUG
    cerr << w.Convert() << '(' << (int)level << ")  ";
    for (int i=1; i<=m.leaf_level; i++)
    {
        cerr << ' ' << hex << tree_hash[i];
    }
    cerr << '\n';
    *//*
    cerr << w.Convert() << '*';
    for (int i=1; i<=m.leaf_level; i++)
    {
        cerr << ' ' << hex << TreeHash_OLD(i);
    }
    cerr << '\n' << '\n';

    ReverseTreeHash(level);
    */
    //cerr << w.Convert() << ' ' << (int)level << ' ' << hex << tree_hash[level] << '\n';

    return tree_hash[level];       // return the hash at the good level
}


inline Uint4 Window::ReverseTreeHash(Uint1 level) const
{
    if (valid_rev_tree_hash) return rev_tree_hash[level];
    Uint4 r[2];
    r[0] = ~w.r[0];
    r[1] = ~w.r[1];

    int count[MAX_LEAF_LEVEL];
    for (int i=0; i<=m.leaf_level; i++) {
        rev_tree_hash[i]=0;
        count[i]=0;
    };

    if (size>REGISTER_SIZE)
    {
        int size = w.size-REGISTER_SIZE-2;
        Uint4 msk = 3<<size;
        for (int i=0; i<length; i++)
        {
            int lvl = m.pos[length-i-1];      // The level of the mask at this position
            int c=((r[1]&msk)>>size)&3; // keep the first 2 useful bits of ~r
            c <<= 2*count[lvl]++;    // shift c, incrementing the count
            rev_tree_hash[lvl] |= c; // Update the hash for this level
            r[1]<<=2;                     // rotate r[0], r[1]
            c = r[0]&(3<<(REGISTER_SIZE-2));
            r[1]&=(c>>(REGISTER_SIZE-2));
        };
    }
    else
    {
        Uint8 msk = 3<<(w.size-2);
        for (int i=0; i<length; i++)
        {
            int lvl = m.pos[length-i-1];      // The level of the mask at this position
            int c=((r[0]&msk)>>(w.size-2))&3; // keep the first 2 useful bits of ~r[0]
            c <<= 2*count[lvl]++;    // shift c, incrementing the count
            rev_tree_hash[lvl] |= c; // Update the hash for this level
            r[0]<<=2;                     // rotate r[0], r[1] not used
        };
    };
    valid_rev_tree_hash = true;

    /*JUST FOR DEBUG
    cerr << w.ReverseComplement()->Convert() << '(' << (int)level << "R) ";
    for (int i=0; i<=MAX_LEAF_LEVEL; i++)
    {
        cerr << ' ' << hex << rev_tree_hash[i];
    }
    cerr << '\n';
    *//*
    cerr << w.ReverseComplement()->Convert() << '!';
    for (int i=1; i<=m.leaf_level; i++)
    {
        cerr << ' ' << hex << ReverseTreeHash_OLD(i);
    }
    cerr << '\n' << '\n';
    */

    return rev_tree_hash[level];       // return the hash at the good level
}

#endif    // WORD_IN_TWO_PARTS

#endif    // WINDOW_H

