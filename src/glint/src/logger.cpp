/*
   $Id: logger.cpp 1424 2008-03-06 09:07:57Z tfaraut $
*/


#include <errno.h>
#include <time.h>
#include <sstream>
#include <iomanip>
#include <unistd.h>
#include "glint.h"
#include "logger.h"
using namespace std;

#include <iostream>

/* Member functions of the class Logger */

/*
 Name:      Logger::Logger
 Usage:     The constructor of the base class
 Function:  Open the log file
 Arg:       file_name: The Logger file name, opened by the constructor
            option:    see logger.h for the details
 Access:    protected (making impossible to allocate an object of class Logger)
 Error:     throw an ios::failure exception
*/

Logger::Logger(const string& file_name, const string& log_level, int opt) throw(exception):
	log_format(normal),log_file_name(file_name),option(opt),multiline(false)
{
    log_file.exceptions(ios_base::goodbit);         // _Trace will throw an exception if there is a problem
    if (option&LOGGER_OPT_AUTOFLUSH)
        log_file.setf(ios::unitbuf);

    log_file.open(file_name.c_str(),ofstream::app);
    if(! log_file.is_open())
    {
      ios::failure e("Logger object - Could not open " + log_file_name);
      throw(e);
    };
 	//cout<<file_name<<"\n";
    gettimeofday(&creation_date,NULL);              // Measuring the time when created is useful for the timers

    if ((option&LOGGER_OPT_QUIET)==0)
    {
        string name;
        char* nme = (char*)calloc(20,sizeof(char)); // allocating and resetting a char* 20 characters long
        if (gethostname(nme, 19)==-1)               // keeping at least 1 byte for the \0
            name="unknown";
        else
            name=nme;

        pid_t pid = getpid();

        log_file << __CalcStamp() << " - ";
        log_file << "STARTING LOGGING (" << pid << "." << name << ") - LEVEL " << log_level << " - NO COMPRESSION ";
        if (option&LOGGER_OPT_AUTOFLUSH)
            log_file << "AUTOFLUSH ON\n";
        else
            log_file << "AUTOFLUSH OFF\n";

   }
}

/*
 Name:      Logger::~Logger
 Usage:     The destructor of the base class
 Function:  write a line, unless the option QUIET is selected
*/

Logger::~Logger()
{
    if ((option&LOGGER_OPT_QUIET)==0)
    {
        log_file << __CalcStamp() << " - ";
        log_file << "STOPPED LOGGING\n";
   }
}

/*
 Name:      Logger::SetLogFile
 Usage:     logger.SetLogFile("some_file.log");
 Function:  Change the log file name: If the new fle cannot be opened, an exception is thrown and the old log file is still opened
 Arg:       file_name: The new Logger file name, opened by the constructor
 Error:     throw an ios::failure exception
*/

void Logger::SetLogFile(const string& file_name) throw(exception)
{
    ofstream tmp(file_name.c_str(),ofstream::out);
    if (!tmp.is_open())
    {
      ios::failure e("Logger object - Could not open " + log_file_name);
      throw(e);
    };

    log_file.close();
    log_file.open(file_name.c_str(),ofstream::out);
}

/*
 Name:      Logger::StartTimer
 Usage:     logger.StartTimer("some_timer");
 Function:  Create if necessary and Start the timer called "some_timer"
            If already started and still running, does nothing
            If stopped, restart it
 Arg:       timer: the name of the timer to start/restart
 Error:     Throw a timer exception if the timer is not correctly specified
*/

void Logger::StartTimer(const string& timer) throw(DoesNotExist)
{
    if (timer=="")
        throw(DoesNotExist("<blank>"));

    Timer& t = timers[timer];
    if (t.running)  // Already running - nothing to do
        return;

   timeval now;
   gettimeofday(&now,NULL);    // TODO  Error management not done here
   //cerr << creation_date.tv_sec << "  " << creation_date.tv_usec << '\n';
   //cerr << now.tv_sec << "  " << now.tv_usec << "\n";
   t.start =  (now.tv_sec - creation_date.tv_sec) * 100;
   t.start += (now.tv_usec - creation_date.tv_usec) / 10000;
   t.running = true;
   //cerr << "start " << t.start << "\n";
}

/*
 Name:      Logger::StopTimer
 Usage:     logger.StopTimer("some_timer");
 Function:  Stop the timer if necessary
            Return the field "duration" converted to a timer stamp
 Arg:       timer: the name of the timer to start/restart
 Error:     Throw a timer exception if the timer does not exist
*/

string Logger::StopTimer(const string& timer) throw(DoesNotExist)
{
    map<string,Timer>::iterator i = timers.find(timer);

    if (i==timers.end())                // timer not found
        throw(DoesNotExist(timer));

    Timer & t = i->second;
    if (t.running)
    {
        timeval now;
        gettimeofday(&now,NULL);    // TODO  Error management not done here
        unsigned long stop = (now.tv_sec - creation_date.tv_sec) * 100;
        stop += (now.tv_usec - creation_date.tv_usec) / 10000;
        //cerr << "stop " << stop << "\n";
        t.duration += (stop - t.start);
        t.running = false;
    };

    return __CalcTimerStamp(t.duration);
}

/*
 Name:      Logger::ResetTimer
 Usage:     logger.ResetTimer("some_timer");
 Function:  Reset (ie set duration=0) the timer
            Throw an exception if the timer does not exist
            Reset the duration and stop the timer
 Return     Nothing
 Arg:       timer: the name of the timer to start/restart
 Error:     Throw a timer exception if the timer does not exist
*/

void Logger::ResetTimer(const string & timer) throw(DoesNotExist)
{
    map<string,Timer>::iterator i = timers.find(timer);

    if (i==timers.end())                // timer not found
        throw(DoesNotExist(timer));

    Timer & t = i->second;
    t.running = false;
    t.duration = 0;
}

/*
 Name:      Logger::__CalcTimerStamp
 Usage:     string stamp = logger.__CalctimerStamp(clk);
 Function:  Convert the number of ticks returned by the timer in a timer stamp
 Return     the stamp
 Arg:       a number of ticks
 Access:    private
 Error:     none
*/

string Logger::__CalcTimerStamp(unsigned long tme) throw()
{

    //cerr << "koukou " << tme << '\n';
    int c = tme % 100;   // number of centi-seconds
    int s = tme / 100;   // total number of seconds

    int m  = s / 60;    // total number of minutes
    s  =  s % 60;       // number of seconds

    int h = m / 60;     // total number of hours
    m = h % 60;

    int d = h / 24;     // total number of days
    h = h % 24;         // number of hours

    ostringstream tmp;
    tmp << "#00#00#";
    tmp.fill('0');
    tmp << setw(2) << d << setw(1) << '#';
    tmp << setw(2) << h << setw(1) << '#';
    tmp << setw(2) << m << setw(1) << '#';
    tmp << setw(2) << s << setw(1) << '#';
    tmp << setw(2) << c << setw(1) << '#';
    return tmp.str();
}

/*
 Name:      Logger::__CalcStamp
 Usage:     string stamp = logger.__CalcStamp();
 Function:  Convert the current time to a stamp
 Return     the stamp
 Arg:       none
 Access:    private
 Error:     none
*/

string Logger::__CalcStamp() throw()
{
    time_t now = time(NULL);
    if (now == serial.tme)
    {
        serial.cnt++;
        if (serial.cnt>=100)
            return serial.stamp + "00#";

        ostringstream tmp(serial.stamp);
        tmp << serial.stamp << setfill('0') << setw(2) << serial.cnt << setw(1) << '#';
        return tmp.str();
    }
    else
    {
        serial.cnt = 1;
        serial.tme = now;
        ostringstream tmp;
        struct tm *now_st = gmtime(&now);
        tmp << '#';
        tmp.fill('0');
        tmp << setw(2) << now_st->tm_year + 1900 - 2000 << setw(1) << '#';
        tmp << setw(2) << now_st->tm_mon + 1 << setw(1) << '#';
        tmp << setw(2) << now_st->tm_mday << setw(1) << '#';
        tmp << setw(2) << now_st->tm_hour << setw(1) << '#';
        tmp << setw(2) << now_st->tm_min << setw(1) << '#';
        tmp << setw(2) << now_st->tm_sec << setw(1) << '#';
        serial.stamp = tmp.str();
        tmp << setw(2) << serial.cnt << setw(1) << '#';
        return tmp.str();
    };
}

Logger::Counter Logger::serial;

/*
 Name:      Logger::_Trace
 Usage:     stamp = logger._Trace(...)
 Function:  Log a correctly formatted line
 Return     nothing
 Arg:       msg: A message to log (should be terminated by "\n" if this is the last line to log)
            log_type: The log_type, in a string representation (just for logging)
            log_format: the log format
            timer: the timer, used only with log_format=timer
 Access:    protected
 Error:     none
*/

void Logger::_Trace(const string& msg, const string& log_type, log_format_type log_format, const string& tmr) throw(DoesNotExist)
{
    if (multiline)
    {
        log_file << msg;
    }
    else
    {
        switch (log_format)
        {
            case no_stamp: {
                log_file << msg;
                break;
            }
            case normal: {
                log_file << __CalcStamp() << "   _" + log_type + "_   " << msg;
                break;
            }
            case point: {
                log_file << "#....................#" << "   _" + log_type + "_   " << msg;
                break;
            }
            case timer: {
                log_file << StopTimer(tmr) << "   _" + log_type + "_   " << "(" << tmr << ") " << msg;
                break;
            }
        }
    };
    multiline = (msg[msg.length()-1]!='\n');   // we switch to multiline if last char of msg is not \n
}

/* Utility functions to convert an integer */

string __to_string(int i)
{
    ostringstream tmp;
    tmp << i;
    return tmp.str();
}

/* operator<< for ILogger and SLogger, used when msg is a string - The other operator<< use this one */

ILogger& operator<< (ILogger& l, const string& msg)
{
    l.Trace(msg,l.log_type,l.log_format,(l.log_format==timer) ? l.current_timer : "");
    return l;
}


SLogger& operator<< (SLogger& l, const string& msg)
{
    l.Trace(msg,l.log_type,l.log_format,(l.log_format==timer) ? l.current_timer : "");
    return l;
}

/* Member functions of the class ILogger */

ILogger::ILogger(const string& file_name, int log_lvl, int option) throw(exception):
Logger(file_name,__to_string((long)log_lvl),option),log_level(log_lvl){}

/*
 Name:      ILogger::Trace
 Usage:     stamp = logger.Trace(...)
 Function:  Log a correctly formatted line only if log_level is enough high
 Return     nothing
 Arg:       msg: A message to log (should be terminated by "\n" if this is the last line to log)
            log_type: The log_type (compared to log_level)
            log_format: the log format
            timer: the timer, used only with log_format=timer
 Access:    public
 Error:     Timer exception
*/

void ILogger::Trace(const string& msg, int log_type, log_format_type log_format, const string& timer) throw(DoesNotExist)
{
    if (log_type>log_level)
        return;
    else
        _Trace(msg,__to_string((long)log_type),log_format,timer);
}

/* Member functions of the class SLogger */

SLogger::SLogger(const string& file_name, const string& log_lvl, int option) throw(exception):
Logger(file_name,log_lvl,option),log_level(log_lvl){}

/*
 Name:      ILogger::Trace
 Usage:     stamp = logger.Trace(...)
 Function:  Log a correctly formatted line only if log_level is enough high
 Return     nothing
 Arg:       msg: A message to log (should be terminated by "\n" if this is the last line to log)
            log_type: The log_type (a string, matched to log_level)
            log_format: the log format
            timer: the timer, used only with log_format=timer
 Access:    public
 Error:     Timer exception
*/

void SLogger::Trace(const string& msg, const string& log_type, log_format_type log_format, const string& timer) throw(DoesNotExist)
{
    if (log_level.find(log_type) == string::npos)
        return;
    else
        _Trace(msg,log_type,log_format,timer);
}


