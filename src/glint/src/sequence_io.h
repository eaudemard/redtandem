/*
   $Id: sequence_io.h 263 2009-09-21 16:10:48Z tfaraut $
*/


/* sequence_io.h */

#ifndef _SEQUENCE_IO_H_
#define _SEQUENCE_IO_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

//STL library
#include <set>
#include <vector>

//Personal includes
#include "ptypes.h"
#include "window.h"
#include "factor_tree.h"
#include "index_io.h"
#include "database.h"
#include "dust.h"

#define MAX_INT3 16777215

/*
 * Common prototypes used by most or all of the strmat files.
 */
#define PREMATURE_EOF -3

/* shared return values */
#define FORMAT_ERROR -1

/*
    The class BlackList handles the 'black-listed' words (words deleted because they were too frequent)
    This is a sery simplistic hash table, built upon a vector and a lot of set objects
    You can only insert some words to the table, and ask the table to know if some given word is
    in the table or not. That's all we need yet
*/

class BlackList {
    public:

    /* The constructors/destructor - We specifye the size of the hash at start */
    BlackList(int);

    /* Inserting a new word in the list */
    void Insert(const Word&);

    /* Searching a word - return true if the word is in the list, false if not */
    bool LookUp(const Word&) const;

    /* Printing the list */
    friend ostream & operator<<(ostream& os, const BlackList &);

    private:
    vector< set<Word> > hash_table;
    int length;     // The number of sets
};

/*
     The class SBuffer defines a buffer, to be filled with characters from the sequence to be indexed.

     The constructor accepts the following parameters:
         t ==> The tree to used for the indexation
         b ==> The blacklist
         d ==> The DataBase_Parameters
         m ==> true if using MemoryMode, default false
      If in MemoryMode, the .bi file and the .fa files are replaced by Buffer objects

     Putc puts a character inside the buffer. If the buffer gets full, the process of:
          -dusting
          -masking the low complexity regions
          -indexing
          -dumping (only if out != NULL)
          is started, then the buffer is emptied.
*/

class SBuffer {
    public:

    /* The constructors/destructor */
    SBuffer(Tree&, BlackList&, DataBase_Parameters&, bool memory_mode=false);
    ~SBuffer();

    /* Reading and indexing the sequence whose filename is maintained
       by database - Looping through the passes, calling __ReadSeqFile() or __ReadSeqMemory()
    */

    Uint4 ReadSeq();

    /* Filling the buffer: Putc puts a character, PutSeparator puts some $ characters
       Each one calls _Syncr() when the buffer is full
    */
    private:
    void Putc(char);
    void PutSeparator() {Putc('$');};

    /* Clearing the buffer */
    void Clear();

	/* Resetting the position using the trick  */
	 void ResetPos();

    /* querying the buffer
       IsFull tries to increase the buffer size until it reaches SBUFFER_MAX_SIZE
       Only when the buffer cannot be resized, true is returned
    */
    bool IsFull() const {return fa_bfr->IsFull();};
    bool IsEmpty() const { return fa_bfr->IsEmpty();};
    size_t GetPosition() const {return position;};

    Buffer_ptr GetBinaryIndexSBuffer() const {return memory_mode ? (Buffer_ptr)bi_bfr : Buffer_ptr(NULL);}; // Strange here : (Buffer_ptr)bi_bfr was necessary for an icc compilation
    Buffer_ptr GetFaSBuffer() const {return memory_mode ? (Buffer_ptr)fa_bfr : Buffer_ptr(NULL);};

    Int4 position;       // Position IN THE DATABASE of the top character
    bool memory_mode;    // if true, we don-t write the index to the files
    bool locked;         // If true, it is impossible to call ReadSeq

    mutable Buffer_ptr fa_bfr; // The buffer used to store the fa file
    mutable Buffer_ptr bi_bfr; // The binary index, only used in memory_mode
    FILE* fa_file;       // The sequence in internal 'fa' format - written during first pass

    Window window;
    Tree & index_tree;
    BlackList & black_list;
    DataBase_Parameters& database;

    /* Reading and indexing a sequence
       Calling Putc or PutSeparator
       Calling _Syncr() just before returning
    */

    Uint4 __ReadSeqFile();          // Called by ReadSeq when memory_mode==false
    Uint4 __ReadSeqMemory();        // Called by ReadSeq when memory_mode==true
    Uint4 __ReadSeq(FILE *);

    Buffer* __InitFaBfr();     // Used by the constructor
    Buffer* __InitBiBfr();     // Used by the constructor

    void _Syncr();           // SBuffer synchronization: dusting, indexing, dumping
    void _MaskLowComplexity();                 // Masking the window width, from the top, because a low complexity was detected
    void _Dust();
    void _Index();           // Index the buffer, filling the tree and may be the blacklist
    void _IndexDinucleotides();
    void _Dump() {fwrite((char *)fa_bfr->GetBase(),sizeof(char),fa_bfr->size(),fa_file);};  // Dump the buffer to fa_file
    void _ReadWord(Uint4);   // Called by _Index or _IndexDinucleotides
    void _NewPosition(Leaf*,Uint4); // Called by _Readword

};

/*
     The class QBuffer defines a buffer, to be filled with characters from the query sequence

     Public methods:

     The constructor accepts the following parameters:
         DataBase_Parameters db The parameters for database
         Compare_Parameters cp  The comparison options

     ReadSeqQuery is the main entry point: it is used to read a multifasta file... or a .fa file from an already
     indexed file (-G switch)

     The Getxxx functions are accessors

     Private methods:
     Putc puts a character inside the buffer. If the buffer gets full, the process of:
          -dusting
          -masking the low complexity regions (???????????)
          -indexing
          -dumping (only if out != NULL)
          is started, then the buffer is emptied.

*/

class QBuffer {
    public:

    /* The constructors/destructor */
    QBuffer(const DataBase_Parameters&, const Compare_Parameters&);
    ~QBuffer();

    /* Reading and comparing a multifasta file against the database
       Calling Putc, and when each sequence is read, ProcessMatches()
       The first form is generally used, the second form with the switch -G
    */

    void ReadSeqQuery(const string& file_query);
    void ReadSeqQuery(const DataBase_Parameters& db);

    /* Querying some stuff */

    size_t GetPosition() const      {return Position;};
    size_t GetLength() const        {return Length;};
    const string & GetSeqId() const {return Sequence_ID;};
    const unsigned char * GetSeq() const {return (unsigned char *) query_bfr.GetBase();};
    const unsigned char * GetSeqAsFasta() const ;
    unsigned char Getc(Uint4 i) const    {return *((unsigned char *) query_bfr.GetBase() + i);};
    void SetQueryDatabase(const DataBase_Parameters* db) {query_database=db;};

    Uint8 MaxScore(int matchscore) const { return ( Length*matchscore ); }; //Maximum possible score of the query sequence given its length.
                                                                            // matchscore is the atomic reward for a nucleotide match

    private:

    /* Putting a new character in the buffer - The buffer is resized if necessary */
    void Putc(char);
	void PutSeparator() {Putc('$');};

    /* Setting the sequence name */
    void SetSeqId(const string & n)      {Sequence_ID=n;};

    /* Clearing the buffer for a new sequence, putting a $ at start */
    void Clear();

    /* Processing and displaying everything */
    void DisplayAllHsp(FILE *pf);
    void ProcessMatches();
	//void SortingAndLinking();

	 /* For debuging purpose   */
	 void DisplayQuerySeq(Uint4 length, int start, int end);
	 QBuffer* Duplicate();

    public:
    const DataBase_Parameters& database;        // They are const, no problem to make them public
    const Compare_Parameters& compare;          // Not too clean however...
    const CTree&  ctree;
    const DataBase_Parameters* query_database;

    private:
    string Sequence_ID;
    size_t Length;      // Number of nucleotides (was top)
    Int4 Position;      // Position IN THE QUERY FILE of the top character (0 = Beginning of current sequence)
	Uint4 Ind_Match[2];
    Window window;

    Buffer_ptr query_bfr_ptr;   // an auto_ptr
    Buffer& query_bfr;
    Buffer& __InitBfrPtr();     // initializing the auto_ptr, init depends on Self mode

	CTree& _init_tree(const DataBase_Parameters& db, const Compare_Parameters& cp); // Enables different ctree objects to be initialized

	void _CompareDinucleotides();
    void _Compare();
    FILE* _OpenOutputFile();   // Called by the constructor: open the output file
    FILE* out;
    MatchResults match_results;
};

/*
     The inline member functions of the class: BlackList
*/

inline BlackList::BlackList(int d): length(d)
{
    set<Word> s;
    for (int i=0;i<length;i++)
        hash_table.push_back(s);
}

inline void BlackList::Insert(const Word &w)
{
    int code=w.Hash(length);
    set<Word>&s = hash_table[code];
    s.insert(w);
}

inline bool BlackList::LookUp(const Word & w) const
{
    int code=w.Hash(length);
    const set<Word>& s = hash_table[code];
    return (s.find(w)!=s.end());
}

/*
     The inline member functions of the class: SBuffer
*/

inline SBuffer::SBuffer(Tree& t, BlackList& b, DataBase_Parameters& d,bool mode):
memory_mode(mode),locked(false),fa_bfr(__InitFaBfr()),bi_bfr(__InitBiBfr()),fa_file(NULL),
window(d.GetMsk().GetString()),index_tree(t),black_list(b),database(d) {
    //fa_bfr = new Buffer(SBUFFER_INITIAL_SIZE,SBUFFER_MAX_SIZE);
	ResetPos();
}

/*
     Init SeqBfr and bi_bfr - The initialization depends on memory_mode
*/
inline Buffer* SBuffer::__InitFaBfr()
{
    size_t max_size = memory_mode ? 0 : SBUFFER_MAX_SIZE;
    return new Buffer(SBUFFER_INITIAL_SIZE,max_size);
}
inline Buffer* SBuffer::__InitBiBfr()
{
    if (memory_mode)
        return new Buffer(SBUFFER_INITIAL_SIZE);
    else
        return NULL;
}

inline SBuffer::~SBuffer() {
    _Syncr();       // Synchronize the buffer before removing it
    // Give the ownership of bi_bfr and fa_bfr to database
    // This was WITHDRAWN because we had perf pbs in g++ - See alse ReadSeqMemory
    //database.SetBfr(bi_bfr,fa_bfr);
}

inline void SBuffer::Clear()
{
    fa_bfr->Clear();
	 //fprintf(stderr,"Clearing the buffer \n");
    //  position = -database.GetMsk().GetLength();  /* Here again the dirty trick !!      A VERIFIER     THOMAS*/
}

inline void SBuffer::ResetPos()
{
    position = -database.GetMsk().GetLength()-1;  /* Here again the dirty trick !! TODO Change this !! */
}

inline void SBuffer::Putc(char c)
{
    if (c!='\n' && (signed char) c!= EOF)
    {
        // fa_bfr->Put must always be successfull, because we test the status with IsFull just after

        fa_bfr->Put(c);
        position++;
        if (fa_bfr->IsFull())
            _Syncr();
    }
}

// Execute the dust algorithm on the buffer before indexation
inline void SBuffer::_Dust()
{
    unsigned char * base = (unsigned char *) fa_bfr->GetBase();
    size_t length = fa_bfr->size();
    char sav = *(base+length-1);
    dust(length,base);
    if (sav=='$')
        *(base+length-1) = sav; // If the end of buffer is masked by dust, the last $ is masked too
}

/*
     The inline member functions of the class: QBuffer
*/

inline QBuffer::~QBuffer()
{
    if (out != stdout && out != NULL)
        fclose(out);
}

inline void QBuffer::Clear()
{
    query_bfr.Clear();
	 //PutSeparator();
    Position = -database.GetMsk().GetLength()+2;  /* Here again the dirty trick !!  */
}

inline void QBuffer::Putc(char c)
{
    if (c!='\n' && (signed char) c!= EOF)
    {
        // The Put is always successfull, because there is no max capacity specified (see the constructor)
        query_bfr.Put(c);
    }
}

inline FILE* QBuffer::_OpenOutputFile()
{
   FILE* o;
   if (compare.results_filename=="")
        o=stdout;
    else
    {
        o = fopen(compare.results_filename.c_str(),"w");
        if (o==NULL)
        {
                string msg = "Cannot open file " + compare.results_filename + " for writing";
                SysDie(errno,msg.c_str());
        };
    }
    return o;
}


#endif

