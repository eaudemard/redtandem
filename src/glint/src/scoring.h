/*
   $Id: scoring.h 263 2009-09-21 16:10:48Z tfaraut $
*/


/* scoring.h */


#ifndef __SCORING_H__
#define __SCORING_H__

#define N_INFTY -1000    //Could be much lower but useless

#include "masque.h"

class AlignEngine;
class AlignConstants {

    public:
    AlignConstants(int , int , int, int , int , int, int);  // See ptypes.h for the default parameters set
    friend class AlignEngine;

    int Match() { return match;};

    private:
	int X1,X2;
	int alpha,beta;
	int match,mismatch,nmatch;
	int **score_fwd;
	int **score_bwd;
	int **score_fwd_severe;
	int **score_bwd_severe;
	int **score_indel;
	int **score;
    int ** _InitScoringScheme(int, int, int, int );
    int ** _InitScoringSchemeSevere(int, int, int, int );
    int ** _InitScoringIndels(int , int );
    void SetSevereScoringSheme();
};

short*** init_overlap_score(const Mask&);
int overlap(const Mask& , int , int , int );
short*** init_overlap_score_bw(const Mask &);
int overlap_bw(const Mask& , int , int , int );


#endif
