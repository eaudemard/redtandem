#
# $Id: ParamParser.pm 3496 2010-09-16 13:32:55Z manu $
#

package ParamParser;

# Copyright INRA/CNRS

# Emmanuel.Courcelle@toulouse.inra.fr
# Jerome.Gouzy@toulouse.inra.fr

# This software is a perl module whose purpose is to help you writing
# your own scripts

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

=head1 NAME

ParamParser - parse parameters from different sources (CGI.pm, GetOpt, cgi-lib, configuration file, ARGV, ENV)

=head1 DESCRIPTION

	1. parameter source defined from a configuration file

	use ParamParser;
	$rh_param = New ParamParser($filename);

		------ example.cfg -------
		# lines starting with # are ignored
		OPTION=value of the option
		--------------------------

	2. from ARGV

	use ParamParser;
	$rh_param = New ParamParser('ARGV');

		% program OPTION1="value of the option" OPTION2=value

	3. from environment variables

	use ParamParser;
	$rh_param = New ParamParser('ENV');
	or
	$rh_param = New ParamParser('ENV','prefix'); to add a tag to environment variables

	4. from CGI object

	use CGI;
	use ParamParser;
	$rh_param = New ParamParser('CGIPM');

	5. from CGI-LIB data structure (version 2)

	require "cgi-lib2.pl";
	use ParamParser;
	$rh_param = New ParamParser('CGILIB');

	6. from Getopt::Std object

	use Getopt::Std;
	use ParamParser;
	$rh_param = New ParamParser('GETOPTSTD',"list_of_singlet-character_switches");

	run the command man Getopt::Std to see what is "list_of_singlet-character_switches"
	to use  the same options with the current module you must write
	$rh_param = New ParamParser('GETOPTSTD',"oif:");
	$rh_param = New ParamParser('GETOPTSTD',"oDI");

	7. from Getopt::Long object

	use Getopt::Long;
	use ParamParser;
	$rh_param = New ParamParser('GETOPTLONG',(list_of_getoptlong_option));

	run the command man Getopt::Long to see what is a "list_of_getoptlong_option"
	to use the same options with the current module you must write
	$rh_param = New ParamParser('GETOPTLONG',("length=i","file=s","verbose"));

    8. from another ParamParser object
 	use ParamParser;
	$rh_param = New ParamParser('PARAMPARSER',$rh_other_param);

    9. from a hash
    use ParamParser;
    $rh_param = New ParamParser('HASH',\%some_hash);

=head1 HOW TO SPECIFY THE USAGE FUNCTION ?

 The Usage function should print some help to the command line.
 It is automatically called when the switch --help is specified
 
 You have three ways to specify the Usage function:
     1/ From the constructor (best method, but new in Aug 2010):
     my $o_param = New ParamParser('GETOPTLONG',\&Usage,'help');
     
     2/ Using a default value (not so nice)
     ParamParser::SetDefaultUsage(\&Usage);
     my $o_param = New ParamParser('GETOPTLONG','help');
     
     3/ Using the SetUsage function (does not work well)
     my $o_param = New ParamParser('GETOPTLONG','help');
     $o_param -> SetUsage(\&Usage);
     WARNING - The latter is UNABLE to trap errors related to the command line, so it is relatively useless.
     
=head1 EXAMPLE1

 use CGI qw/:standard/;
 use ParamParser;

 my $o_param =  New ParamParser("CGIPM");

 # attach an usage fonction to the parser
 # the best way would be to reference a real fonction $o_param->SetUsage(my $usage=sub { &UsageFct(); } );
 $o_param->SetUsage(my $usage=sub { print "\nPlease read the documentation\n"; } );

 # add a single variable to the data structure
 $o_param->Set('TIMEOUT','10000');

 # append all environment variables in overwrite mode (overwrite duplicates)
 $o_param->Update('ENV',"O");

 # check that the value of the parameter CFG is an existing file, print the usage and exit if it is not.
 $o_param->AssertFileExists('CFG');

 # add all variables contained in the configuration file in append mode (do not overwrite duplicates)
 $o_param->Update($o_param->Get('CFG'),"A");

 print header;
 $o_param->Print('html');
 
 # Pass a filename through parameter -infile, specifying that STDIN will be used as a default value
 # If a file is specified, it may be compressed using one of many compression algorithms (cf. GENERAL.pm)
 $o_param -> SetUnlessDefined('infile','stdin');
 my $fh_in = $o_param -> GetStreamIn( 'infile' );
 while (my $line = <$fh_in>){...};

=cut

=head1 EXAMPLE2

 use Getopt::Long;
 use ParamParser;

 my $o_param =  New ParamParser('GETOPTLONG',("help:s","min=i","max=i","inputfile=s","what=s"));

 # attach an usage fonction to the parser
 # the best way is to reference a real fonction $o_param->SetUsage(my $usage=sub { &UsageFct(); } );
 $o_param->SetUsage(my $usage=sub { print "\nPlease read the documentation\n"; } );

 # append all environment variables in append mode (do not overwrite duplicates)
 $o_param->Update('ENV',"A");

 # check that the value of the parameter inputfile is an existing file, print the usage and exit if it is not.
 $o_param->AssertFileExists('inputfile');

 # check that the value of the parameters are integers, print the usage and exit if one of them is not.
 $o_param->AssertInteger('max','min');

 # check that the value of the parameter what is a correct value
 $o_param->AssertAllowedValue('what','yes','no','maybe');

 # check that the value of the parameter what is a correct value (more restrictive: only 1 char)
 $o_param->AssertAllowedValue('what','[yYnN01]');

 # check that the value of the parameters is a correct value, matching one of those patterns
 $o_param->AssertAllowedPattern('^[wW]hat$','^[yY]es', '^[nN]o','^maybe$');

 # check each key's value for a list of allowed characters
 $o_param->AssertAllowedValueForEachKey('[0-9a-z]+');

 # check that each key's value starts with a lower-case letter
 $o_param->AssertAllowedPatternForEachKey('^[a-z]');

 $o_param->Print();

=cut

=head1 BEHAVIOURS

 The following behaviours may be set/unset (see the SetBehaviour, UnsetBehaviour, GetBehaviour functions and also
 SetDefaultBehaviour, UnsetDefaultBehaviour, GetDefaultBehaviour).
 Have a look to the $H_DEFBEHAVIOUR initializations to know default values for those behaviours
 => By default only assert_strict is set.

 assert_value_secure
   NOTE - Forced and hardcoded to 1 when the source is CGIPM or CGILIB
   Before Setting a parameter, its value is matched against the list of authorized characters (see SetAuthorizedCharacters)

 assert_strict
   When set, the assertions (see the methods AssertXXX) fail if the parameter is not defined

 assert_empty_file_allowed
   When set, AssertfileExists will NOT throw an exception if the file exists but is empty

 ignore_space
   When set, separators are allowed on each side of the = sign, so that toto   =   3 affects 3 to toto
   When unset, toto   =   3 means that the parameter 'toto   ' gets the value '   3'

 use_substitution_table
   When set, special characters (ie %0-%9, %a-%z, %A-%Z) found in the parameters' VALUE may be substituted by variables
   or by function calls.
   Have a look to the SetSubstitution method

 exit_on_getopt_error
   When set, the program throws an exception if there is a getopt error (GETOPTLONG or GETOPT parameter sources only)
   When unset, the program only warns a message if there is a getopt error. This message may be suppressed
   with the --quiet switch
   If using GETOPTLONG AND exit_on_getopt_error is unset, you may authorize unknown parameters

 lock_file
   When set, updates or dumps from or to a file are lokc-protected against concurrent access

 use_exceptions
   When set, LipmError based exceptions are thrown instead of calling die/carp
   THIS BEHAVIOUR IS NOW IGNORED AND WILL BE SOON REMOVED

=head1 PUBLIC METHODS

=cut

# Avoiding warning due to a conflict name with General
use warnings;
no warnings "redefine";

use strict;
use Carp;
use Fcntl;
use IO::File;
use General;

# We protect this use, so that if one of those modules is not installed nothing wrong happens
# however, some functionalities will be lost

eval("use Convert::UU qw(uudecode uuencode)");
eval("use JSON");
#eval("use warnings");

our %H_DEFBEHAVIOUR;

#
# Init the default behaviour for each recorded behaviour
#

$H_DEFBEHAVIOUR{'assert_value_secure'}       = 0;
$H_DEFBEHAVIOUR{'assert_strict'}             = 1;
$H_DEFBEHAVIOUR{'assert_empty_file_allowed'} = 0;
$H_DEFBEHAVIOUR{'ignore_space'}              = 0;
$H_DEFBEHAVIOUR{'exit_on_getopt_error'}      = 0;
$H_DEFBEHAVIOUR{'use_substitution_table'}    = 0;
$H_DEFBEHAVIOUR{'lock_file'}                 = 0;
$H_DEFBEHAVIOUR{'use_exceptions'}            = 0;


#
# Init the R_DEFUSAGE variable
#

our $R_DEFUSAGE = sub {print "\nWarning: something is wrong but the program usage is not yet described\n"};

use constant SEPARATOR             => ' ';
use constant HTTP_ERROR_SECURITY   => 888;
use constant AUTHORIZED_CHARACTERS => '[>/0-9a-zA-Z:_.@+\#\-\s=,\?\"\'\[\]]';    # do not use \w

BEGIN
{
    our $VERSION = do {my @r = (q$Rev: 3496 $ =~ /\d+/g); $r[0]};
}

=head2 function New

	Title      :	New
	Usage      :	my $o_param = New ParamParser();
                    my $o_param = New ParamParser('CGIPM');
                    my $o_param = New ParamParser('GETOPTLONG','some_switch','some_int=i','some_string=s');
                    my $o_param = New ParamParser('GETOPTLONG',\&Usage,'some_switch','some_int=i','some_string=s');
                    (perldoc Getopt::Long for the details)
                    my $o_param = New ParamParser('some_file');
                    my $o_param = New ParamParser('PARAMPARSER',$o_another_param);
                    my $o_param = New ParamParser('HASH',\%h_parameters);
	Function   :	constructor of the object
	Returns    :	a valid object
	Args       :	$source    (optional) The source of the parameters
                    CGIPM|CGILIB|GetOptStd|GetOptLong|ARGV|$filename|HASH|ENV
                    other prms (optional) following $source
	Globals    :	none

=cut

sub New
{
    my ($pkg, $source, @a_opt) = @_;
    
    if (ref ($a_opt[0]) eq 'CODE')
    {
		my $rf_usage = shift @a_opt;
		&ParamParser::SetDefaultUsage( $rf_usage );
	}

    #
    # Set the behaviour for this objet to the default behaviour
    #
    my %__h_behaviour = %H_DEFBEHAVIOUR;

    my $self = {
                __h_opt                 => {},
                __h_behaviour           => \%__h_behaviour,
                __nb                    => 0,
                __mode                  => "",
                __name_space            => "",
                __authorized_characters => &AUTHORIZED_CHARACTERS,
                __possible_sources      => "",
                __last_source           => $source,
		__o_appli		=> '@'
                };

    bless($self, $pkg);

    $self->SetUsage($R_DEFUSAGE);
    $self->__InitPossibleSources();

    # More security concern in CGI-related modes
    if (defined($source) && $source =~ /CGI/)
    {
        $$self{'__h_behaviour'}{'assert_value_secure'} = 1;

        # See perldoc perlsec
        $ENV{'PATH'} = '/bin:/usr/bin';
        delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'};
    }

    $self->Update($source, 'I', @a_opt);


    return ($self);
}

=head2 procedure Update

	Title     :	Update
	Usage     :	$o_param->Update($source,$mode,@options);
	Procedure :	Updates the parameters
	Args      :	$source  The source to update from
                         INIT|CGIPM|CGILIB|GetOptStd|GetOptLong|ARGV|$filename|HASH|ENV
                If $source if not defined, we try to automaticcaly detect a data source.
                On a given configuration the behaviour is always the same, but the behaviour is unpredictable from machine to machine
                The consequence is that New ParamParser() may be somewhat unpredictable, you may rather want New ParamParser('INIT');
                $mode
		           I init     : clean the data structure first
		           A Append   : preserve the previous value of duplicate keys
		           O Overwrite: replace the value of a duplicate key
                @options other prms (optional) following $source
	Globals   :	none

=cut
sub Update
{
    my ($self, $source, $mode, @a_opt) = @_;
    my $opt = (defined($a_opt[0])) ? $a_opt[0] : "";
    my $lock_flg = $self->GetBehaviour('lock_file');

    $$self{'__mode'} = $mode;

    if ($mode eq 'I')
    {
        $self->Init();
    }

    if (defined($source) and -e $source)
    {
        if ((!-z $source) or ($lock_flg == 1))    # If locking enable, $source may be empty and locked
        {
            &__FromFile($self, $source);
            $$self{'__last_source'} = "$source";
            return;
        }
    }

    if (!defined($source))
    {

        # the module tries to find automatically the source of parameter
        # (the source cannot be neither GetOpt* nor filename)
        if ($$self{'__possible_sources'} =~ /ARGV/)
        {
            $source = "ARGV";
        }
        elsif ($$self{'__possible_sources'} =~ /CGIPM/)
        {
            $source = "CGIPM";
        }
        elsif ($$self{'__possible_sources'} =~ /CGILIB/)
        {
            $source = "CGILIB";
        }
        else
        {
            $source = "ENV";
        }
    }

    if (!defined($source) || $$self{'__possible_sources'} !~ / $source /)
    {
        $$self{'__last_source'} = "";
        return;
    }

    $$self{'__last_source'} = "\U$source";
    if ($source =~ /CGILIB/i)
    {
        my (@a_backup) = @_;    # this backup is needed because cgi-lib uses @_ as parameter input source
        &__FromCGILIB($self, @a_backup);
    }
    elsif ($source =~ /CGIPM/i)
    {
        &__FromCGIPM($self,$opt);
    }
    elsif ($source =~ /ENV/i)
    {
        &__FromENV($self);
    }
    elsif ($source =~ /GETOPTSTD/i)
    {
        &__FromGetOptStd($self, $opt);
    }
    elsif ($source =~ /GETOPTLONG/i)
    {
        &__FromGetOptLong($self, @a_opt);
    }
    elsif ($source =~ /ARGV/i)
    {
        &__FromARGV($self);
    }
    elsif ($source =~ /PARAMPARSER/i)
    {
        &__FromPARAMPARSER($self, @a_opt);
    }
    elsif ($source =~ /HASH/i)
    {
        &__FromHASH($self, @a_opt);
    }
    elsif ($source =~ /INIT/i)
    {
        $self->__CallUsageIfNeeded();
    }

	$self->__CallUsageIfNeeded();

    return;
}

=head2 procedure Dump

	Title     :	Dump
	Usage     :	$o_param->Dump($filename [,$prefix]);
                $o_param->Dump('ENV' [,$prefix]);
                $o_param->Dump('GETOPTLONG'[,$prefix]);
                $o_param->Dump('HASH',$rh_output [,$prefix]);
                $o_param->Dump('CGIPM',$r_output [,$prefix]);
                $o_param->Dump('STRING',$r_output [,$prefix]);
                $o_param->Dump('HTML',$r_output [,$prefix]);
                $o_param->Dump('JSON',$r_output [,$prefix] [,\%h_json_keys]);
	Procedure :	Dumps the parameters to some target (a file for example)
	Args      :	$target (required) The target used for dumping
                                   $filename|ENV|GetOptLong|HASH
                $rh_output (required if target eq 'HASH') The hash used to dump to
                $prefix (optional) A prefix to write BEFORE each parameter name
	Globals   :	none

=cut
sub Dump
{
    my ($self, $target, @a_opt) = @_;

    if ($$self{'__possible_destinations'} =~ / $target /)
    {
        if ($target =~ /ENV/)
        {
            my $prefix = (defined($a_opt[0])) ? $a_opt[0] : "";
            &__ToENV($self, $prefix);
        }
        if ($target =~ /GETOPTLONG/)
        {
            my $prefix = (defined($a_opt[0])) ? $a_opt[0] : "";
            &__ToGetOptLong($self, $prefix);
        }
        if ($target =~ /HASH/)
        {
            my $rh_output = $a_opt[0];
            my $prefix = (defined($a_opt[1])) ? $a_opt[1] : "";
            &__ToHASH($self,$rh_output,$prefix);
        }
        if ($target =~ /CGIPM/)
        {
            my $r_output = $a_opt[0];
            my $prefix = (defined($a_opt[1])) ? $a_opt[1] : "";
            &__ToCGI($self,$r_output,$prefix);
        }
        if ($target =~ /STRING/)
        {
            my $r_output = $a_opt[0];
            my $prefix = (defined($a_opt[1])) ? $a_opt[1] : "";
            &__ToSTRING($self,$r_output,$prefix,"");
        }
        if ($target =~ /HTML/)
        {
            my $r_output = $a_opt[0];
            my $prefix = (defined($a_opt[1])) ? $a_opt[1] : "";
            &__ToSTRING($self,$r_output,$prefix,"html");
        }
        if ($target =~ /JSON/)
        {
            my $r_output = $a_opt[0];
            my $prefix = (defined($a_opt[1])) ? $a_opt[1] : "";
            &__ToSTRING($self,$r_output,$prefix,"json",$a_opt[2]);
        }

    }
    else    # the parameter is assumed to be a filename
    {
        my $prefix = (defined($a_opt[0])) ? $a_opt[0] : "";
        &__ToFile($self, $target, $prefix);
    }
    return;
}

=head2 procedure SelectNameSpace

    Title     :	SelectNameSpace
    Usage     :	$o_param->SelectNameSpace('SOME_PREFIX');
                $o_param->SelectNameSpace();
    Procedure :	Select a prefix for each parameter name, it will be used for Get/Set operations
    Args      :	$ns   the name space to use from this point
                      If not specified, do not use any namespace
    Globals   : none

=cut
sub SelectNameSpace
{
    my $self = shift;
    my ($ns) = @_;
    $ns = "" if (!defined($ns));

    $$self{'__name_space'} = $ns;
    return;
}

=head2 procedure Init

    Title     :	Init
    Usage     :	$o_param->Init();
                $o_param->Update('I');
    Procedure :	Initialize the parameters
    Args      :	none

=cut
sub Init
{
    my $self = shift;

    $$self{'__nb'}          = 0;
    $$self{'__last_source'} = "";
    $$self{'__mode'}        = "I";
    $$self{'__name_space'}  = "";

    foreach my $key (keys(%{$$self{'__h_opt'}}))
    {
        delete($$self{'__h_opt'}{$key});
    }
}

=head2 procedure Set

    Title     :	Set
    Usage     :	$o_param->Set($key,$value);
    Procedure :	Set the value of a single parameter
    Args      :	$key    Parameter name - If using a name space, Set will prefix the name with the name space
                $value  Parameter value - Will be subsituted and checked for security if necessary
    TODO      : Passer plusieurs valeurs et utiliser &SEPARATOR (cf. __UndateIfPossible)
                APPELER __SecurityControl
                CORRIGER LE BUG

=cut
sub Set
{
    my $self = shift;
    my ($opt, $value) = @_;

    $$self{'__last_source'} = "INLINE";

    my $key = $$self{'__name_space'} . $opt;
    $$self{'__nb'}++ if (!defined($$self{'__h_opt'}{$key}));
    $$self{'__h_opt'}{$key} = $value;
    $self->__SubstituteKey($key) if ($self->GetBehaviour('use_substitution_table'));
    return;
}

=head2 procedure SetUnlessDefined

    Title     :	SetUnlessDefined
    Usage     :	$o_param->SetUnlessDefined($opt,$value);
    Procedure :	Call Set only if the opt is NOT already defined
    Args      :	$opt    Parameter name - If using a name space, SetUnlessDefined will prefix the name with the name space
                $value  Parameter value - Will be subsituted and checked for security if necessary

=cut
sub SetUnlessDefined
{
    my $self = shift;
    my ($opt, $value) = @_;

    unless ($self->IsDefined($opt))
    {
        $self->Set($opt,$value);
    };
    return;
}

=head2 procedure Delete

    Title     :	Delete
    Usage     :	$o_param->Delete($opt)
    Procedure :	Delete the parameter
    Args      :	$opt    Parameter name - If using a name space, Delete will prefix the name with the name space

=cut
sub Delete
{
    my $self = shift;
    my ($opt) = @_;

    $$self{'__nb'}--;
    my $key = $$self{'__name_space'} . $opt;
    if (defined($$self{'__h_opt'}{$key}))
    {
        delete($$self{'__h_opt'}{$key});
    }
}

=head2 function Get

    Title     :	Get
    Usage     :	my $value = $o_param->Get($opt);
                my @value = $o_param->Get($opt);
                my @value = $o_param->Get($opt1,$opt2,...);
    function  :	1- Return the value of the $opt key
                2- Return the value of the $opt key as a singleton array
                3- Return the value of the keys as an array
    Args      :	$opt,... Parameter(s) name(s) - If using a name space, Get will prefix the name with the name space
    return    : the parameter value(s) if defined or "" (NEVER return undef)

=cut
sub Get
{
    my $self = shift;
    my (@a_opt) = @_;

    my $name_space = $$self{'__name_space'};
    my @rvl=();
    foreach my $opt (@a_opt)
    {
        my $key = $name_space . $opt;
        push (@rvl, (defined($$self{'__h_opt'}{$key})) ? $$self{'__h_opt'}{$key} : "");
    };
    return ( @a_opt==1 and !wantarray() ) ? $rvl[0] : @rvl;
}

=head2 function GetInteger

    Title     :	GetInteger
    Usage     :	my $value = $o_param->GetInteger($opt);
                my @value = $o_param->GetInteger($opt);
                my @value = $o_param->GetInteger($opt1,$opt2,...);
    function  :	1- Call AssertInteger and return the value of the $opt key
                2- Call AssertInteger and return the value of the $opt key as a singleton array
                3- Call AssertInteger and return the value of the keys as an array
    Args      :	$opt,... Parameter(s) name(s) - If using a name space, Get will prefix the name with the name space
    return    : the parameter value(s) as integers if defined or 0 (NEVER return undef)
    behaviours: assert_strict

=cut
sub GetInteger
{
    my $self = shift;
    my (@a_opt) = @_;

    $self->AssertInteger(@a_opt);
    my $name_space = $$self{'__name_space'};
    my @rvl=();
    foreach my $opt (@a_opt)
    {
        my $key = $name_space . $opt;
        my $val;
        if (defined($$self{'__h_opt'}{$key}) and $$self{'__h_opt'}{$key} ne "")
        {
            $val = $$self{'__h_opt'}{$key} + 0;
        }
        else
        {
            $val = 0;
        }
        push (@rvl, $val);
    };
    return ( $#a_opt==0 and !wantarray() ) ? $rvl[0] : @rvl;
}

=head2 function GetStreamIn,GetStreamOut

    Title     :	GetStreamIn,GetStreamOut 
    Usage     :	my $fh_in   = $o_param->GetStreamIn($opt);
                my @a_fh_in = $o_param->GetStreamIn($opt);
                my @a_fh_in = $o_param->GetStreamIn($opt1,$opt2);
                my $fh_out  = $o_param->GetStreamOut($opt);
                my @a_fh_out= $o_param->GetStreamOut($opt);
                my @a_fh_out= $o_param->GetStreamOut($opt1,$opt2); 
   function   : 1- Returns an INPUT stream corresponding to the file whose path is the value of  
                   parameter $opt (STDIN if the value is '-' or 'stdin')
                2- Same thing, but as a singleton array
                3- Returns an array of streams
                4- Returns an OUTPUT stream corresponding to the file whose path is the value of  
                   parameter $opt (STDOUT if the value is '-' or 'stdout')
                The files (in or out) may be compressed, the compression algorithm is guessed from the extension
    Prerequisite : The parameters are expected to correspond to filename(s), else an input error will be thrown
                   If the special value 'stdin/stdout' or '-' is used, STDIN/STDOUT is considered 
                   (and should NOT be a compressed stream)
    Args      :	One or several keys
    return    : stream(s) corresponding to (compressed) filenames

=cut
sub GetStreamIn
{
    my $self = shift;
    my (@a_opt) = @_;
    
    my (@a_filenames) = $self -> Get ( @a_opt );
    my @a_fh;
    foreach my $filename (@a_filenames)
    {
		next if $filename eq '';
		push @a_fh, General::GetStreamIn( $filename );
	}
	return ( @a_filenames==1 and !wantarray() ) ? $a_fh[0] : @a_fh;
}
sub GetStreamOut
{
    my $self = shift;
    my (@a_opt) = @_;
    
    my (@a_filenames) = $self -> Get ( @a_opt );
    my @a_fh;
    foreach my $filename (@a_filenames)
    {
		next if $filename eq '';
		push @a_fh, General::GetStreamOut( $filename );
	}
	return ( @a_filenames==1 and !wantarray() ) ? $a_fh[0] : @a_fh;
}

=head2 function GetKeys

    Title     :	GetKeys
    Usage     :	my @keys = $o_param->GetKeys('pattern');
    function  :	Return the the list of keys matching with:
                     -The namespace prefix is a namespace is defined
                     -The pattern if a pattern is passed
    Args      :	$pattern (optional) The pattern - If not specified each key is considered to match
    return    : An array of matching keys, WITHOUT THEIR NAMESPACES

=cut
sub GetKeys
{
    my $self = shift;
    my ($pattern) = @_;

    my @a_keys = ();
    my $cpt    = 0;

    my $ns = $$self{'__name_space'};
    foreach my $key (sort keys(%{$$self{'__h_opt'}}))
    {
        if ($key =~ /^$ns/)
        {
            my $nkey = $key;
            $nkey =~ s/^$ns// unless ($ns eq '');
            if (!defined($pattern) or $nkey =~ /$pattern/)
            {
                $a_keys[$cpt++] = $nkey;
            }
        }
    }
    return (@a_keys);
}

=head2 function IsDefined

    Title     :	IsDefined
    Usage     :	if ($o_param->IsDefined($opt)...
    function  :	Return true if the opt is defined IN THE NAMESPACE, false if not
    Args      :	$opt    The opt to test for definition
    return    : true/false

=cut
sub IsDefined
{
    my $self = shift;
    my ($opt) = @_;

    my $key = $$self{'__name_space'} . $opt;
    my ($bool) = (defined($$self{'__h_opt'}{$key})) ? 1 : 0;
    return $bool;
}

=head2 function HowMany

    Title     :	HowMany
    Usage     :	my $nb = $o_param->HowMany();
    function  :	Return the number of parameters
    Args      :	none
    return    : the number of parameters

=cut
sub HowMany
{
    my $self = shift;

    return $$self{'__nb'};
}

=head2 function GetSource

    Title     :	GetSource
    Usage     :	my $srce = $o_param->GetSource();
    function  :	Return the last source used for updating the parameters
    Args      :	none
    return    : the last source

=cut
sub GetSource
{
    my $self = shift;

    return $$self{'_last_source'};
}

=head2 procedure SetSubstitution

    Title     :	SetSubstitution
    Usage     :	$o_param->SetSubstitution($pattern,$ref)
    procedure : Add en entry in the substitution table
    Args      :	$pattern The pattern (%a-%z,%A-%Z,%0-%9) to substitute
                $ref     The ref (to a scalar or to a function) or value to subsitute with

=cut
sub SetSubstitution
{
    my ($self, $to_substitute, $ref) = @_;
    if ($to_substitute !~ /\A%[a-z0-9]\Z/i)
    {
        &Carp::croak("\n" . '=>You can declare as substitution strings ONLY %0..%9, %A..%Z, %a,..%z','parameter');
    }
    $$self{'__substitution_table'}{$to_substitute} = $ref;
}

=head2 procedure Print

    Title     :	Print
    Usage     :	$o_param->Print();
                $o_param->Print('html');
    procedure : Print all the parameters and their values
    Args      :	'html' (optional) Print through an html table

=cut
sub Print
{
    my ($self, $format) = @_;

    my $output="";
    if ($format eq  "html")
    {
        $self->Dump('HTML',\$output,"");
    }
    else
    {
        $self->Dump('STRING',\$output,"");
    }

    print $output;
}

=head2 procedure SetBehaviour

    Title     :	SetBehaviour
    Usage     :	$o_param->SetBehaviour('some_behaviour')
    procedure : Set a behaviour
    Args      :	$behaviour  The behaviour to set
                NOTES:
                  -If the behaviour passed by parameter does not exist, the method is silently ignored
                  -If the behaviour is 'use_substitution_table', the substitutions are automatically performed

=cut

sub SetBehaviour
{
    my ($self, $behaviour) = @_;

    return unless (__ValidBehaviour($behaviour));
    $$self{'__h_behaviour'}{$behaviour} = 1;
    if ($behaviour eq 'use_substitution_table')
    {
        $self->__SubstituteAll();
    }
}

=head2 procedure UnsetBehaviour

    Title     :	UnsetBehaviour
    Usage     :	$o_param->UnsetBehaviour('some_behaviour')
    procedure : Unset a behaviour
    Args      :	$behaviour  The behaviour to unset
                NOTE If the behaviour passed by parameter does not exist,
                the method is silently ignored (thus nothing happens)

=cut
sub UnsetBehaviour
{
    my ($self, $behaviour) = @_;

    return unless (__ValidBehaviour($behaviour));
    $$self{'__h_behaviour'}{$behaviour} = 0;
}

=head2 procedure GetBehaviour

    Title     :	GetBehaviour
    Usage     :	if($o_param->GetBehaviour('some_behaviour'))...
    procedure : Get the status of some behaviour
    Args      :	$behaviour  The behaviour to get
                NOTE If the behaviour passed by parameter does not exist,
                the method returns FALSE

=cut
sub GetBehaviour
{
    my ($self, $behaviour) = @_;
    return 0 unless (__ValidBehaviour($behaviour));
    return $$self{'__h_behaviour'}{$behaviour};
}

=head2 procedure SetDefaultBehaviour

    Title     :	SetDefaultBehaviour
    Usage     :	ParamParser::SetBehaviour('some_behaviour');
    procedure : This is NOT a method, this is an ordinary function
                Set the default status of some behaviour
    Args      :	$behaviour  The behaviour to set
                NOTE If the behaviour passed by parameter does not exist,
                the method is silently ignored (thus nothing happens)

=cut
sub SetDefaultBehaviour
{
    my $behaviour = shift;
    return unless (__ValidBehaviour($behaviour));
    $H_DEFBEHAVIOUR{$behaviour} = 1;
}

=head2 procedure UnsetDefaultBehaviour

    Title     :	UnsetDefaultBehaviour
    Usage     :	ParamParser::UnsetDefaultBehaviour('some_behaviour')
    procedure : This is NOT a method, this is an ordinary function
                Unset the default status of some behaviour
    Args      :	$behaviour  The behaviour to unset
                NOTE If the behaviour passed by parameter does not exist,
                the method is silently ignored (thus nothing happens)

=cut
sub UnSetDefaultBehaviour
{
    my $behaviour = shift;
    return unless (__ValidBehaviour($behaviour));
    $H_DEFBEHAVIOUR{$behaviour} = 0;
}

=head2 procedure GetDefaultBehaviour

    Title     :	GetDefaultBehaviour
    Usage     :	ParamParser::GetDefaultBehaviour('some_behaviour'))...
    procedure : Get the Default status of some behaviour
    Args      :	$behaviour  The behaviour to get
                NOTE If the behaviour passed by parameter does not exist,
                the method returns FALSE

=cut
sub GetDefaultBehaviour
{
    my $behaviour = shift;
    return 0 unless (__ValidBehaviour($behaviour));
    return $H_DEFBEHAVIOUR{$behaviour};
}

=head2 procedure AssertFullPath

    Title     :	AssertFullPath
    Usage     :	$o_param->AssertFullPath(@a_opt);
    procedure : throw an exception unless every element of the array @a_opt is the full path of an existing file or dir
    Args      :	@a_opt  A list of parameters to check

=cut
sub AssertFullPath
{
    my $self = shift;

    my (@a_opt) = @_;

    foreach my $opt (@a_opt)
    {
        my $key = $$self{'__name_space'} . $opt;
        my ($lfile) = $$self{'__h_opt'}{$key};
        if (defined($lfile) && $lfile !~ /^\//)
        {
            &__PrintUsage($self);
            $lfile = &__DefinedIfNot($lfile);
            &Carp::croak("\n=>The value of the parameter $opt is >$lfile< which is not full path file|dir name",'parameter');
        }
    }
    return 1;
}

=head2 procedure AssertFileExists

    Title     :	AssertFileExists
    Usage     :	$o_param->AssertFileExists(@a_opt);
    procedure : throw an exception unless every element of the array @a_opt is the name of an existing file
    Args      :	@a_opt  A list of parameters to check
    Behaviours: assert_strict and assert_empty_file_allowed

=cut
sub AssertFileExists
{
    my ($self, @a_opt) = @_;

    foreach my $opt (@a_opt)
    {
        my $key = $$self{'__name_space'} . $opt;
        my ($lfile) = $$self{'__h_opt'}{$key} =~ /(\S+)/ if (defined $$self{'__h_opt'}{$key});
        next if (!defined($lfile) && !$$self{'__h_behaviour'}{'assert_strict'});
        if (!defined($lfile) || !-e $lfile || (-z $lfile && !$$self{'__h_behaviour'}{'assert_empty_file_allowed'}))
        {
            &__PrintUsage($self);
            $lfile = &__DefinedIfNot($lfile);
            &Carp::croak(
                "\n=>The value of the parameter $opt is >$lfile< which is not a name of an existing and non empty file",'parameter'
                );
        }
    }

    return 1;
}

=head2 procedure AssertNonEmptyFile(@a_opt)

    Title     :	AssertNonEmptyFile
    Usage     :	$o_param->AssertNonEmptyFile(@a_opt);
    procedure : throw an exception unless every element of the array @a_opt refers to a non empty file
    Args      :	@a_opt  A list of parameters to check
    Behaviours: none

=cut
sub AssertNonEmptyFile
{
    my ($self, @a_file) = @_;

    foreach my $file (@a_file)
    {
        my $file = $$self{'__name_space'} . $file;
        if (!defined($file) || !-e $file || -z $file)
        {
            &__PrintUsage($self);
            $file = &__DefinedIfNot($file);
            &Carp::croak("\nAssertNonEmptyFile failed for $file","parameter");
        }
    }

    return 1;
}

=head2 procedure AssertDirExists

    Title     :	AssertDirExists
    Usage     :	$o_param->AssertDirExists(@a_opt);
    procedure : throw an exception unless every element of the array @a_opt is the name of an existing dir
    Args      :	@a_opt  A list of parameters to check
    Behaviours: assert_strict

=cut
sub AssertDirExists
{
    my ($self, @a_opt) = @_;

    foreach my $opt (@a_opt)
    {
        my $key = $$self{'__name_space'} . $opt;
        my ($lfile) = $$self{'__h_opt'}{$key};
        next if (!defined($lfile) && !$$self{'__h_behaviour'}{'assert_strict'});
        if (!defined($lfile) || !-d $lfile)
        {
            &__PrintUsage($self);
            $lfile = &__DefinedIfNot($lfile);
            &Carp::croak("\n=>The value of the parameter $opt is >$lfile< which is not a name of an existing directory","parameter");
        }
    }

    return 1;
}

=head2 procedure AssertInteger

    Title     :	AssertInteger
    Usage     :	$o_param->AssertInteger(@a_opt);
    procedure : throw an exception unless every element of the array @a_opt is an integer
    Args      :	@a_opt  A list of parameters to check
    Behaviours: assert_strict

=cut
sub AssertInteger
{
    my ($self, @a_opt) = @_;

    foreach my $opt (@a_opt)
    {
        my $key = $$self{'__name_space'} . $opt;
        my ($lopt) = $$self{'__h_opt'}{$key};
        next if (!defined($lopt) && !$$self{'__h_behaviour'}{'assert_strict'});
        if (!defined($lopt) || $lopt !~ /^[\+\-]*\d+$/)
        {
            &__PrintUsage($self);
            $lopt = &__DefinedIfNot($lopt);
            &Carp::croak("\n=>The value of the parameter $opt is >$lopt< which is not a valid integer value","parameter");
        }
    }
    return 1;
}

=head2 procedure AssertDefined

    Title     :	AssertDefined
    Usage     :	$o_param->AssertDefined(@a_opt);
    procedure : throw an exception unless every element of the array @a_opt is defined
    Args      :	@a_opt  A list of parameters to check
    Behaviours: none

=cut
sub AssertDefined
{
    my ($self, @a_opt) = @_;

    foreach my $opt (@a_opt)
    {
        my $key = $$self{'__name_space'} . $opt;
        my ($lopt) = $$self{'__h_opt'}{$key};
        if (!defined($lopt))
        {
            &__PrintUsage($self);
            &Carp::croak("=>The parameter $opt must be provided");
        }
    }
    return 1;
}

=head2 procedure AssertAllowedValue

    Title     :	AssertallowedValue
    Usage     :	$o_param->AssertAllowedValue($a_opt,@a_regex);
    procedure : throw an exception unless the value of the passed parameter matches at least 1 *anchored* regex
    Args      :	$opt     The parameter to check (ONLY ONE)
                @a_regex The regular expressions used for the match
    Behaviours: assert_strict
    NOTE      : We test using a regex match, but the values entered ARE ANCHORED, so that this function is convenient
                to test a parameter agains a value, or a set of allowed characters, etc.
                If you want to test only if some value starts with some character, you should use AssertAllowedPattern instead


=cut
sub AssertAllowedValue
{
    my ($self, $opt, @a_list_of_allowed_values) = @_;
    my $key = $$self{'__name_space'} . $opt;
    my ($lvalue) = $$self{'__h_opt'}{$key};
    if (defined($lvalue))
    {
        foreach my $one_value (@a_list_of_allowed_values)
        {
            if ($lvalue =~ /^$one_value$/)
            {
                return 1;
            }
        }
    }
    else
    {
        if (!$$self{'__h_behaviour'}{'assert_strict'})
        {
            return 1;
        }
    }
    &__PrintUsage($self);
    my ($allowed) = join(',', @a_list_of_allowed_values);
    $lvalue = &__DefinedIfNot($lvalue);

    #ce carp n'envoye rien dans le fichier de logs d'apache !
    &Carp::croak(
         "=>The current value of the parameter $opt is >$lvalue< which is not in the set of allowed values [$allowed]",
         'parameter'
         );
}

=head2 procedure AssertAllowedValueForAllKeys

    Title     :	AssertAllowedValueForAllKeys
    Usage     :	$o_param->AssertAllowedValueForAllKeys(@a_regex);
    procedure : Call AssertAllowedValue for every parameter
    Args      :	@a_regex The regular expressions used for the match
    Behaviours: assert_strict

=cut
sub AssertAllowedValueForAllKeys
{
    my ($self, @a_list_of_allowed_patterns) = @_;

    foreach my $key ($self->GetKeys())
    {
        $self->AssertAllowedValue($key, @a_list_of_allowed_patterns);
    }
}

=head2 procedure AssertAllowedPattern

    Title     :	AssertallowedPattern
    Usage     :	$o_param->AssertAllowedValue($a_opt,@a_regex);
    procedure : throw an exception unless the value of the passed parameter matches at least 1 regex
    Args      :	$opt     The parameter to check (ONLY ONE)
                @a_regex The regular expressions used for the match
    Behaviours: assert_strict
    NOTE      : This sub is *NEARLY* the same as AssertAllowedValue, EXCEPT THAT here we do not anchor the
                regex. You can use AssertAllowedPattern to check that some parameter STARTS WITH something

=cut
sub AssertAllowedPattern
{
    my ($self, $value, @a_list_of_allowed_patterns) = @_;

    my $key = $$self{'__name_space'} . $value;
    my ($lvalue) = $$self{'__h_opt'}{$key};
    if (defined($lvalue))
    {
        foreach my $one_pattern (@a_list_of_allowed_patterns)
        {
            if ($lvalue =~ /$one_pattern/)
            {
                return 1;
            }
        }
    }
    else
    {
        if (!$$self{'__h_behaviour'}{'assert_strict'})
        {
            return 1;
        }
    }
    &__PrintUsage($self);
    my ($allowed) = join(',', @a_list_of_allowed_patterns);
    $lvalue = &__DefinedIfNot($lvalue);
    &Carp::croak(
        "=>The current value of the parameter $value is >$lvalue< which is not in the set of allowed patterns [$allowed]",
        'parameter'
        );
}

=head2 procedure AssertAllowedPatternsForAllKeys

    Title     :	AssertAllowedPatternsForAllKeys
    Usage     :	$o_param->AssertAllowedPatternsForAllKeys(@a_regex);
    procedure : Call AssertAllowedPatterns for every parameter
    Args      :	@a_regex The regular expressions used for the match
    Behaviours: assert_strict

=cut
sub AssertAllowedPatternForAllKeys
{
    my ($self, @a_list_of_allowed_patterns) = @_;

    foreach my $key ($self->GetKeys())
    {
        $self->AssertAllowedPattern($key, @a_list_of_allowed_patterns);
    }
}

=head2 procedure SetUsage

    Title    : SetUsage
    Usage    : $o_param->SetUsage(my $usage= sub { &my_usage_fct();} );
               $o_param->SetUsage(\&main::Usage);
	           $o_param->SetUsage('USAGE_DELAYED');
    procedure: Attach an usage fonction to the ParamParser object (1st, 2nd call).
               Attach the private function UsageDelayed (3rd call). If called, this function just sets a flag;
               If, somewhat later, SetUsage is called with a real function reference, this function will be immediately called.
	           This way, the call of the Usage function is somewhat delayed. This can be useful when some other objects
	           need to be built before calling Usage.
    args     : $f_fct_usage a ref to an usage function OR the string 'USAGE_DELAYED'

=cut
sub SetUsage
{
    my ($self, $r_fct_usage) = @_;

    if ($r_fct_usage eq 'USAGE_DELAYED')
    {
        $$self{'__usage_delayed'} = 1;
        $$self{'__usage'}         = \&__UsageDelayed;
    }
    else
    {
        $$self{'__usage_delayed'} = 0;
        $$self{'__usage'}         = $r_fct_usage;
        $self->__CallUsageIfNeeded();
    }
}

=head2 procedure SetDefaultUsage

    Title    : SetDefaultUsage
    Usage    : $o_param->SetUsage$o_param->SetUsage(my $usage= sub { &my_usage_fct();} );
               $o_param->SetUsag$o_param->SetUsage(\&main::Usage);
    procedure: Attach a default usage fonction to the ParamParser module
               This function will be automagically set to the usage function for the new objects created from now

=cut
sub SetDefaultUsage
{
    my $r_fct_usage = shift;
    $R_DEFUSAGE = $r_fct_usage;
}

=head2 procedure Usage

    Title     :	Usage
    Usage     :	$o_param->Usage();
                $o_param->Usage('html');
    procedure : Print the usage of the program, calling the attached procedure, and exit with code 1
    Args      :	$format   If html, print a mini but complete html page
    Behaviours: none

=cut
sub Usage
{
    my ($self, $format) = @_;
    my ($head) = "";
    my ($tail) = "";

    return if (exists $$self{'_usage_delayed'});    # Nothing to do if the usage is delayed
    if (defined($format) && $format =~ /html/i)
    {
        $head = "<html><head><title>$0</title></head><body><br><pre>";
        $tail = "<br></pre></body></html>";
    }
    print $head;
    &__PrintUsage($self);
    print $tail;
    exit 1;
}

=head2 function Encode

	Title      :	Encode
	Usage      :	&ParamParser::Encode($parameters);
	Prerequiste:	uuencode must be installed
	Function   :	Encode a $param if required
                    THIS FUNCTION IS NOT A METHOD
	Returns    :	$params, encoded or not
	Args       :	$param,	a string, generally an url formatted parameters
	globals    :	none

=cut

sub Encode
{
    my ($parameters) = @_;

    my $str = uuencode($parameters);
    $str =~ s/^begin 644 uuencode.uu\s//o;
    $str =~ s/`\send\s$//o;
    $str =~ s/\=/code1/go;
    $str =~ s/\&/code2/go;
    $str =~ s/\?/code3/go;
    $str =~ s/\n/code4/go;
    $str =~ s/\"/code5/go;
    $str =~ s/ /code6/go;
    $str =~ s/;/code7/go;
    $str =~ s/([^A-Za-z0-9])/sprintf("%%%02X", ord($1))/seg;
    $str =~ s/%/code8/go;
    $str = reverse($str);
    return '' if ($str eq '');
#print STDERR "ENC: $str\n";
    return "__wb_url=$str";
}

=head2 function Decode

	Title      :	Decode
	Usage      :	$query_string = &ParamParser::Decode();
	Prerequiste:	uudecode must be installed
	Function   :	Decode the $ENV{'QUERY_STRING'} if needed
                    THIS FUNCTION IS NOT A METHOD
	Returns    :	the $ENV{'QUERY_STRING'} decoded
	Args       :	none
	globals    :	$ENV{QUERY_STRING} modified

=cut

sub Decode
{
    return '' unless (defined($ENV{'QUERY_STRING'}));
    return $ENV{'QUERY_STRING'} if ($ENV{'QUERY_STRING'} !~ /__wb_url/);

    my ($encoded_substr) = $ENV{'QUERY_STRING'} =~ /__wb_url=([^\&]+)/;
    my $encoded_ori = $encoded_substr;
    $encoded_substr = reverse($encoded_substr);
    $encoded_substr =~ s/code1/=/go;
    $encoded_substr =~ s/code2/\&/go;
    $encoded_substr =~ s/code3/\?/go;
    $encoded_substr =~ s/code4/\n/go;
    $encoded_substr =~ s/code5/\"/go;
    $encoded_substr =~ s/code6/ /go;
    $encoded_substr =~ s/code7/;/go;
    $encoded_substr =~ s/code8/%/go;
    $encoded_substr =~ s/\%([A-Fa-f0-9]{2})/pack('C', hex($1))/seg;

    $encoded_substr = "begin 644 uuencode.uu\n$encoded_substr\`\nend\n";

    my $str = uudecode($encoded_substr);

    my $tmp = "__wb_url=$encoded_ori";
    $ENV{'QUERY_STRING'} =~ s/$tmp/$str/m;

    return $ENV{'QUERY_STRING'};

}

=head2 procedure SetAuthorizedCharacters

    Title     :	SetAuthorizedCharacters
    Usage     :	$o_param->SetAuthorizedCharacters('[A-Za-z0-9_]');
    procedure : Set the behaviour assert_value_secure and change the authorized characters
                For CGI programs, assert_value_secure is activated by default and used at the parameter parsing level
                so to modify the set of AuthorizedCharacters you must do it in several steps
                my $o_param = New ParamParser();                # first init the object
                $o_param->SetAuthorizedCharacters('[A-Za-z]');  # then modify the list
                $o_param->Update('CGIPM','A');                  # then read the parameters

	The more common usage
                my $o_param = New ParamParser('CGIPM');
                requires a set of allowed values and uses the default set of characters
    Args      : $pattern  A perl regex
    Behaviours: assert_value_secure is set

=cut

sub SetAuthorizedCharacters
{
    my ($self, $perlpattern) = @_;

    $$self{'__authorized_characters'} = $perlpattern;
    $$self{'__h_behaviour'}{'assert_value_secure'} = 1;

}

=head1 PRIVATE METHODS

=cut

=head2 procedure __SecurityControl

    Title     :	__SecurityControl
    Usage     :	$self->__Securitycontrol($opt,\@values);
    procedure : If $opt is some reserved parameter, just return
                If behaviour 'assert_value_secure' unset, return
                Else, check every value agains the authorized characters.
                If a mismatch is found, throw an exception or (in CGIPM only) return with a fake http code
    Args      :	$opt       The parameter name
                $ra_values The array of values to check
    Access    : private
    Behaviours: assert_value_securenone

=cut
sub __SecurityControl
{
    my ($self, $item, $ra_values) = @_;

    return if ($item =~ /__wb_url|__wb_cookie/);    # related to WebBuilder
    if ($$self{'__h_behaviour'}{'assert_value_secure'})
    {
        my $secure_char = $$self{'__authorized_characters'};
        foreach my $val (@$ra_values)
        {
            if ($val !~ /^($secure_char*)$/)
            {
                if ($$self{'__last_source'} =~ /CGI/)
                {
                    my $error = &HTTP_ERROR_SECURITY;
                    $secure_char =~ s/\///g;
                    print "Status: $error\nContent-Type: text/html; charset=utf-8\n\n<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
                    print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
                    print "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n<head></head><body><h3>";
                    print "ERROR $error : The request is not processed due to insecure character in<br>key=$item<br>value=$val<br>allowed characters are $secure_char</body></html>";
                }
                &Carp::croak("SECURITY ISSUE: Fatal error: the parameter >$item< is not secure enough (value=$val)\n",'parameter');
            }
        }
    }
}


=head2 procedure __CallUsageIfNeeded

    Title     :	__CallUsageIfNeeded
    Usage     :	$self->__CallUsageIfNeeded()
    procedure : Call Usage if the 'help' parameter is defined
    Args      :	none
    Access    : private

=cut
sub __CallUsageIfNeeded
{
    my $self = shift;
    if ($self->IsDefined('help') or $self->IsDefined('HELP'))
    {
        return if (defined($$self{'__usage_delayed'}) && $$self{'__usage_delayed'} == 1);
        if ($$self{'__last_source'} =~ /CGI/i)
        {
            $self->Usage('html');
        }
        else
        {
            $self->Usage();
        }
    }
}

=head2 procedure __UsageDelayed

    Title     :	__UsageDelayed
    Usage     :	$self->__UsageDelayed()
    procedure : Set the internal flag '__usage_needed'
    Args      :	none
    Access    : private

=cut
sub __UsageDelayed
{
    my $self = shift;
    $$self{'__usage_needed'} = 1;    # We shall call Usage when possible
}

=head2 procedure __PrintUsage

    Title     :	__PrintUsage
    Usage     :	$self->__PrintUsage()
    procedure : Call the registered usage function
    Args      :	none
    Access    : private

=cut
sub __PrintUsage
{
    my $self = shift;
    &{$$self{'__usage'}}($self);

}

=head2 procedure __UpdateIfPossible

    Title     :	__UpdateIfPossible
    Usage     :	$self->__UpdateIfPossible($opt,@values);
    procedure : Update the $opt parameter with 1 or several values
                If several values are specified, they are joined, using the constant &SEPARATOR, before
                updating the parameter value
                The parameter is updated or not, depending on the mode
    Args      :	$opt The parameter to update
                @values The parameter value(s) - MAY BE UNDEF !
    Access    : private
    Behaviours: use_substitution_table

=cut
sub __UpdateIfPossible
{
    my ($self, $item, @values) = @_;

    $self->__SecurityControl($item, \@values);

    my $how = ($$self{'__mode'} eq "") ? "A" : $$self{'__mode'};

    $item = $$self{'__name_space'} . $item;
    if (
        !defined($$self{'__h_opt'}{$item})    # the key doesn't already exist
        || (defined($$self{'__h_opt'}{$item}) && $how eq 'O')
      )                                       # or the key already exists but the mode is 'O'verwrite
    {
        $$self{'__nb'}++;
        if (defined($values[0]))              # at least one value
        {
            if (defined($values[1]))          # more than one
            {
                if (!ref($values[1]))         # only simple values that can be merged
                {
                    $$self{'__h_opt'}{$item} = join(&SEPARATOR, @values);
                }
                else                          # but do not try merging complex data types
                {
                    $$self{'__h_opt'}{$item} = \@values;
                }
            }
            else
            {
                $$self{'__h_opt'}{$item} = $values[0];
            }
        }
        else
        {
            $$self{'__h_opt'}{$item} = undef;
        }
    }

    if ($self->GetBehaviour('use_substitution_table'))
    {
        $self->__SubstituteKey($item);
    }
    return;
}

=head2 function __ValidBehaviour

    Title     :	__ValidBehaviour
    Usage     :	if (&ValidBehaviour($behaviour)) ...
    function  : Return true if the behaviour name is valid
                throw an exceptnio if the behaviour name is invalid
                THIS IS NOT A METHOD, THIS IS AN ORDINARY FUNCTION
    Args      :	$behaviour  The behaviour to validate
    Access    : private

=cut
sub __ValidBehaviour
{
    my $behaviour = shift;
    return 1 if (exists $H_DEFBEHAVIOUR{$behaviour});
    &ParamParser::__Die('',"\n=>The behaviour $behaviour is unknown",'parameter');
    return 0;
}

=head2 procedure __SubstituteKey

    Title     :	__SubstituteKey
    Usage     :	$self->SubstituteKey($key);
    procedure : Try to make the substitutions for the key passed by parameter
    Args      :	$key  The key whose value will be substituted
    Access    : private

=cut
sub __SubstituteKey
{
    my ($self, $key) = @_;
    return unless (defined($self->{'__h_opt'}{$key}));         # If value not defined, nothing to substitute
    return unless (exists $self->{'__substitution_table'});    # If no table, nothing to substitute

    my $rh_sub_table = $self->{'__substitution_table'};
    my $to_subst     = $self->{'__h_opt'}{$key};
    return unless ($to_subst =~ /%/);                          # If no %, nothing to substitute

    foreach my $s (keys(%$rh_sub_table))
    {
        next unless ($to_subst =~ /$s/);
        my $r = $rh_sub_table->{$s};
        if (ref($r) eq '')                                     # Substitute if not a ref
        {
            $to_subst =~ s/$s/$r/g;
        }
        elsif (ref($r) eq 'SCALAR')                            # Substitute if ref to a scalar
        {
            $to_subst =~ s/$s/$$r/g;
        }
        elsif (ref($r) eq 'CODE')                              # Substitute, calling the sub, if ref to a sub
        {
            my $subst = &$r($self, $key);
            $to_subst =~ s/$s/$subst/g;                        # N.B. May be several substitutions, but only 1 call
        }
    }

    $self->{'__h_opt'}{$key} = $to_subst;
    return;
}

=head2 procedure __SubstituteAll

    Title     :	__SubstituteAll
    Usage     :	$self->SubstituteAll();
    procedure : Call __self->SubstitueKey for each parameter
    Args      :	none
    Access    : private

=cut
sub __SubstituteAll
{
    my $self = shift;
    foreach my $key (sort keys(%{$self->{'__h_opt'}}))
    {
        $self->__SubstituteKey($key);
    }
}

=head2 procedure __FromGetOptStd

    Title     :	__FromGetOptStd
    Usage     :	$self->__FromGetOptStd($optlist);
    procedure : Initialize the ParamParser object using Getopt::Std style as source of param/values
    Args      :	$optlist used by getopts
    Access    : private

=cut
sub __FromGetOptStd
{
    my ($self, $optlist) = @_;

    use Getopt::Std;
    my @a_backup = @ARGV;

    our %options = ();
    &getopts($optlist, \%options);

    #my $getopt_succeed = &getopts($optlist,\%options);
    #if ( ! $getopt_succeed && $$self{'__h_behaviour'}{'exit_on_getopt_error'} )
    #{
    #	&Usage();
    #}
    foreach my $key (keys(%options))
    {
        &__UpdateIfPossible($self, $key, $options{$key});
    }

    @ARGV = @a_backup;    # restore original parameters
                          #	-> can be parsed again is necessary
                          #	-> avoid side effect
}

=head2 procedure __FromGetOptLong

    Title     :	__FromGetOptLong
    Usage     :	$self->__FromGetOptLong(@a_opt);
    procedure : Initialize the ParamParser object using Getopt::Long style as source of param/values
    Args      :	@a_opt used by GetOptions
    Access    : private

=cut
sub __FromGetOptLong
{
    my ($self, @a_opt) = @_;

    use Getopt::Long;

    # Unknown parameters are kept inside @ARGV
    if (! $$self{'__h_behaviour'}{'exit_on_getopt_error'})
    {
    	Getopt::Long::Configure('pass_through');        
    }
	else
	{
		Getopt::Long::Configure('no_pass_through');
	}

	my @a_backup  = @ARGV;
    my %h_options = ();
    my %h_value   = ();

    foreach my $key (@a_opt)
    {
        my $val = undef;
        $h_options{$key} = \$val;
    }
    my $getopt_succeed = &GetOptions(%h_options);

    if (!$getopt_succeed && $$self{'__h_behaviour'}{'exit_on_getopt_error'})
    {
        &Usage($self);
    }

	my @a_known_options = ();
    foreach my $key (keys(%h_options))
    {
		my (@F)        = split(/[:=]/, $key);
		my ($real_key) = $F[0];
		push (@a_known_options,$real_key);
		my $r_tmp      = $h_options{$key};
		if (defined($$r_tmp))
        {
            &__UpdateIfPossible($self, $real_key, $$r_tmp);
        }
    }

    # Unknown parameters are retrieved
    if (! $$self{'__h_behaviour'}{'exit_on_getopt_error'})
    {
    	   &__FromGetOptLongRetrieveUnknownParams($self, \@a_known_options, \@ARGV);
    }

    @ARGV = @a_backup;    # restore original parameters
                          #	-> can be parsed again is necessary
                          #	-> avoid side effect
}
=head2 procedure __FromGetOptLongRetrieveUnknownParams

    Title     :	__FromGetOptLongRetrieveUnknownParams
    Usage     :	&__FromGetOptLong($self, $ra_known_options, $ra_argv);
    procedure : Update the ParamParser object using ARGV options after Getopt::Long treatment
    Args      :	$self ParamParser object
                   $ra_known_options parameters defined in GetOpt::Long constructor
		   $ra_argv ARGV options
    Access    : private

=cut

sub __FromGetOptLongRetrieveUnknownParams
{
	my ($self, $ra_known_options, $ra_argv) = @_;
	 for (my $argpos =0; $argpos <= $#{$ra_argv}; )
	    {
    		my $nextargpos = 2;
			my $arg = $ra_argv->[$argpos];
	
		
		#SC 20100914
		#ajout du ,2 pour ne spliter que sur le premier separateur  sinon si la valeur du parametre commencai par un espace
		#ex: ./test.pl --testparam=' -e 0.0001 ', le -e 0.0001 etait perdu et --testparam etait consideré comme switch
		#my (@a_split)        = split(/[:= ]/, $arg);
		my (@a_split)        = split(/[:= ]/, $arg, 2);
		
	        my ($real_key) = $a_split[0];
	    if (! grep (/-*$real_key$/,@{$ra_known_options}))
		{
			$real_key =~ s/^-+//;
			my $value = undef;
			
			if (defined $a_split[1] and $a_split[1] ne '')
			{
				$value = $arg;
				$value =~ s/^-+$real_key[=: ]//;
				$nextargpos = 1;
				
			}
			else
			{
				#cas des parametre passes avec valeurs mais sans le = ni le :
				#comment les differencier des switches booleens
				#je regarde si dans ARGV, l'argument suivant est defini'
				
				if (defined $ra_argv->[$argpos + 1])
				{
					#si l'argument suivant n'est pas du genre -param , c'est donc la valeur de l'argument actuel


					if ($ra_argv->[$argpos + 1] !~ /^-+\S+/)
					{
						$value = $ra_argv->[$argpos + 1] ;
					}
					#si l'argument suivant ne commence par un -, l'argument actuel est un switch qu'on met a 1
					#et le parametre suivant se trouve a la position +1 et non +2
					else
					{
						$nextargpos = 1;
						$value = 1 ;
					}
				}
				else #si l'argument suivant dans ARGV n'est pas definit, alors on est en bout de ligne et il s'agit donc d'un switch
				{
					$value = 1 ;
				}
				
			}
			print STDERR "Warning: Despite the fact that parameter >$real_key< with value >$value< is not defined in your GetOpt::Long list, ParamParser retrieved it\n" unless ($self->IsDefined('quiet'));
			&__UpdateIfPossible($self, $real_key, $value) if (defined $value);
		}
		$argpos =  $argpos + $nextargpos;
	    }
	    return;
}

=head2 procedure __FromCGILIB

    Title     :	__FromCGILIB
    Usage     :	$self->__FromCGILIB(@a_backup);
    procedure : Initialize the ParamParser object using CGI-LIB2 as source of param/value
    Args      :	@a_backup ???
    Access    : private

=cut
sub __FromCGILIB
{
    my ($self, @a_backup) = @_;

    @_ = @a_backup;
    my ($keyin);

    if (defined(ref(&main::ReadParse)))
    {
        &main::ReadParse;

        foreach $keyin (keys(%main::in))
        {
            &__UpdateIfPossible($self, $keyin, $main::in{$keyin});
        }
    }
}

=head2 procedure __FromCGIPM

    Title     :	__FromCGIPM
    Usage     :	$self->__FromCGIPM(@a_backup);
    procedure : Initialize the ParamParser object using CGI.pm as source of param/value
    Args      :	$cgi (optional): An already built CGI.pm object (will be created if not passed)
    Access    : private

=cut
sub __FromCGIPM
{
    my ($self,$cgi) = @_;

    if (!defined $cgi or $cgi eq '')
    {
        $cgi = new CGI;
    }

    my $original_mode = $self->{'__mode'};
    $self->{'__mode'} = 'M';

    foreach my $key ($cgi->param())
    {
        my @a_value = ();
        my $fh      = &CGI::upload($key);
        if (defined($fh))    # required to not modify the type
        {
            $a_value[0] = $cgi->param($key);    # the value is a filehandle or an array of filehandle
        }
        else                                    # required to manage multiple selection on list
        {
			if ($key eq '__wb_url')
			{
				my ($wb_url) = $cgi->param($key);
				my @a_params = ();
				$wb_url = '__wb_url='.$wb_url if ($wb_url !~ /__wb_url=/);
				$ENV{'QUERY_STRING'} = $wb_url;
				my $decoded_url = &ParamParser::Decode();
				$decoded_url =~ s/amp;//g;
				@a_params = split(/\&/, $decoded_url);
				foreach my $param (@a_params)
				{
					my @a_keyvalue = $param =~ /([^=]+)=(.*)/;
					&__UpdateIfPossible($self, shift(@a_keyvalue), @a_keyvalue);
				}
			}
			else
			{
				@a_value = $cgi->param($key);
			}
        }
        &__UpdateIfPossible($self, $key, @a_value);
    }
    $self->{'__mode'} = $original_mode;
}

=head2 procedure __FromFILE

    Title     :	__Fromfile
    Usage     :	$self->__FromFile($source);
    procedure : Initialize the ParamParser object using a configuration file
    Args      :	$source The file name
    Access    : private

=cut
sub __FromFile
{
    my ($self, $source) = @_;
    my $lock_flg = $self->GetBehaviour('lock_file');

    my ($lign) = "";

    my $lock_file = $source . '.lock';
    my $fh_lock_file;
    if ($lock_flg == 1)
    {
        $fh_lock_file = new IO::File("+>>$lock_file") or &Carp::croak("Cannot open $lock_file");
        fcntl($fh_lock_file, F_SETLKW, pack('ssx32', F_RDLCK, 0)) or &Carp::croak("Can't put a read lock on $lock_file: $!",'io');
    }
    my $fh_source = new IO::File($source) or &Carp::croak("ERROR Cannot open >$source<",'io');
    while ($lign = <$fh_source>)
    {
        next if ($lign =~ /^#/);
        chomp($lign);
        my (@F);
        if ($$self{'__h_behaviour'}{'ignore_space'})
        {
            @F = split(/\s*=\s*/, $lign, 2);
        }
        else
        {
            @F = split('=', $lign, 2);
        }
        next if (!defined($F[0]) || !defined($F[1]));
        &__UpdateIfPossible($self, $F[0], $F[1]);
    }
    $fh_source->close();
    if ($lock_flg == 1)
    {
        fcntl($fh_lock_file, F_SETLKW, pack('ssx32', F_UNLCK, 0)) or &Carp::croak("Can't release the read lock on $lock_file: $!",'io');
        $fh_lock_file->close();
    }
}

=head2 procedure __FromARGV

    Title     :	__FromARGV
    Usage     :	$self->__FromARGV();
    procedure : Initialize the ParamParser object using @ARGV array as source of param/value
    Args      :	none
    Access    : private

=cut
sub __FromARGV
{
    my ($self) = @_;

    foreach my $option (@ARGV)
    {
        my (@F) = split('=', $option, 2);
        next if (!defined($F[0]) || !defined($F[1]));
        &__UpdateIfPossible($self, $F[0], $F[1]);
    }
}

=head2 procedure __FromENV

    Title     :	__FromENV
    Usage     :	$self->__FromENV();
    procedure : Initialize the ParamParser object using the %ENV hash as source of param/value
    Args      :	none
    Access    : private

=cut
sub __FromENV
{
    my ($self) = @_;

    foreach my $option (keys(%ENV))
    {
        next if (!defined($option) || !defined($ENV{$option}));
        &__UpdateIfPossible($self, $option, $ENV{$option});
    }
}

=head2 procedure __FromPARAMPARSER

    Title     :	__FromPARAMPARSER
    Usage     :	$self->__FromPARAMPARSER($o_param);
    procedure : Initialize the ParamParser object using another ParamParser object
    Args      :	$o_param The other ParamParser object
    Access    : private

=cut
sub __FromPARAMPARSER
{
    my $self = shift;
    my $o_p  = shift;
    my ($keyin);

    my $rh_opt = $o_p->{'__h_opt'};    # The parameters from the other ParamParser object
    foreach $keyin (keys(%$rh_opt))
    {
        &__UpdateIfPossible($self, $keyin, $rh_opt->{$keyin});
    }
}

=head2 procedure __FromHASH

    Title     :	__FromHASH
    Usage     :	$self->__FromHASH(@a_backup);
    procedure : Initialize the ParamParser object using a hash
    Args      :	$rh_p the hash
    Access    : private

=cut
sub __FromHASH
{
    my $self = shift;
    my $rh_p = shift;

    foreach my $keyin (keys(%$rh_p))
    {
        &__UpdateIfPossible($self, $keyin, $rh_p->{$keyin});
    }
}

=head2 procedure __ToFile

    Title     :	__ToFile
    Usage     :	$self->__ToFile($target,$prefix)
    procedure : Dump the paramparser into a file
    Args      :	$target  The file name
                $prefix  Add a prefix to the key, unless already added (write to a namespace)
    Access    : private

=cut
sub __ToFile
{
    my ($self, $target, $prefix) = @_;
    my $ns        = $$self{'__name_space'};
    my $lock_file = $target . '.lock';
    my $lock_flg  = $self->GetBehaviour('lock_file');
    my $fh_lock_file;
    if ($lock_flg == 1)
    {
        $fh_lock_file = new IO::File(">>$lock_file") or &Carp::croak ("ERROR - Can't put a read lock on $lock_file: $!",'io');
        fcntl($fh_lock_file, F_SETLKW, pack('ssx32', F_WRLCK, 0)) or die "Can't put a read lock on $lock_file: $!";
    }
    my $fh_target = new IO::File(">$target") or &Carp::croak("ERROR Can't open >$target< for writing\n",'io');
    foreach my $key (sort keys(%{$$self{'__h_opt'}}))
    {
        if (defined($key) && defined($$self{'__h_opt'}{$key}) && $key =~ /^$ns/)
        {
            if ($prefix ne "" && $key !~ /^$prefix/)
            {
                my $nkey = "$prefix$key";
                print $fh_target "$nkey=" . $$self{'__h_opt'}{$key} . "\n";
            }
            else
            {
                print $fh_target "$key=" . $$self{'__h_opt'}{$key} . "\n";
            }
        }
    }
    $fh_target->close();
    if ($lock_flg == 1)
    {
        fcntl($fh_lock_file, F_SETLKW, pack('ssx32', F_UNLCK, 0)) or &Carp::croak ("Can't release the read lock on $lock_file: $!",'io');
        $fh_lock_file->close();
        unlink($lock_file);    # Forcing a cache reload with nfs
    }
}

=head2 procedure __ToENV

    Title     :	__ToENV
    Usage     :	$self->__ToENV($prefix)
    procedure : Dump the paramparser into the environment
    Args      :	$prefix  Add a prefix to the key, unless already added (write to a namespace)
    Access    : private

=cut
sub __ToENV
{
    my ($self, $prefix) = @_;
    my $ns = $$self{'__name_space'};

    foreach my $key (sort keys(%{$$self{'__h_opt'}}))
    {
        next if ($key !~ /^$ns/);
        if (defined($key) && defined($$self{'__h_opt'}{$key}))
        {
            if ($prefix ne "" && $key !~ /^$prefix/)
            {
                my $nkey = "$prefix$key";
                $ENV{$nkey} = "$$self{'__h_opt'}{$key}";
            }
            else
            {
                $ENV{$key} = "$$self{'__h_opt'}{$key}";
            }
        }
    }
}

=head2 procedure __ToHASH

    Title     :	__ToHASH
    Usage     :	$self->__ToHASH($rh_target,$prefix)
    procedure : Dump the paramparser into a hash
    Args      : $rh_target The hash
                $prefix  Add a prefix to the key, unless already added (write to a namespace)
    Access    : private

=cut
sub __ToHASH
{
    my ($self, $rh_target, $prefix) = @_;
    my $ns = $$self{'__name_space'};

    foreach my $key (sort keys(%{$$self{'__h_opt'}}))
    {
        next if ($key !~ /^$ns/);
        if (defined($key) && defined($$self{'__h_opt'}{$key}))
        {
            if ($prefix ne "" && $key !~ /^$prefix/)
            {
                my $nkey = "$prefix$key";
                $rh_target->{$nkey} = "$$self{'__h_opt'}{$key}";
            }
            else
            {
                $rh_target->{$key} = "$$self{'__h_opt'}{$key}";
            }
        }
    }
}

=head2 procedure __ToSTRING

    Title     :	__ToSTRING
    Usage     :	$self->__ToHASH($r_target,$prefix,$format)
    procedure : Dump the paramparser into a string
    Args      : $r_target                    : The string
                $prefix                      : Add a prefix to the key, unless already added (write to a namespace)
                $format                      : If "html", dump to an html table
                $rh_json_formatted (optional): used only with json format
                If "json", dump to one or several json-formatted strings:
                   -First remove parameters starting with __json_
                   -Compute the key with the remaining part (__json_some_key ==> some_key)
                   -Keep the parameter to the $rh_json hash if specified, else it is lost
                This is to avoid re-formatting json for already json parameters, wthis would lead to an error
    Access    : private

=cut
sub __ToSTRING
{
    my ($self, $r_target, $prefix, $format, $rh_json_keys) = @_;

    # Dump the object to a hash
    my %h_tmp;
    $self->__ToHASH(\%h_tmp,$prefix);

    # Dump the hash to a json string, saving the already json-formatted parameters
    if (defined($format) && $format =~ /json/i)
    {
        my $json = new JSON;
        foreach my $k (keys %h_tmp)
        {
            if ($k =~ /^__json_(.+)$/)
            {
                $rh_json_keys->{$1} = $h_tmp{$k} if (defined $rh_json_keys);
                delete $h_tmp{$k};
            }
        }
        $$r_target = $json->encode(\%h_tmp);
    }

    # Dump the temporary hash to a string
    else
    {
        $$r_target    = "";
        my ($header)  = "";
        my ($tail)    = "";
        my ($sep)     = ":";
        my ($newline) = "\n";
        my ($endline) = "";
        my ($bold)    = "";
        my ($endbold) = "";
        if (defined($format) && $format =~ /html/i)
        {
            $header  = "<table>";
            $tail    = "</table>";
            $sep     = "</td><td>";
            $newline = "<tr><td>";
            $endline = "</td></tr>";
            $bold    = "<b>";
            $endbold = "</b>";
        }
        $$r_target .= $header;
        foreach my $key (sort keys %h_tmp)
        {
            $$r_target .= $newline . $bold . $key . $endbold . $sep . $h_tmp{$key} . $endline;
        }
        $$r_target .= "${newline}Total number (all namespaces) of keys$sep " . $$self{'__nb'} . $endline;
        $$r_target .= "${newline}Last source$sep " . $$self{'__last_source'} . $endline;
        $$r_target .= "$tail";
    }
}

=head2 procedure __ToCGI

    Title     :	__ToCGI
    Usage     :	$self->__ToCGI($r_output,$prefix)
    procedure : Dump the paramparser to $r_output (a ref to a string), using the CGI conventions
    Args      :	$r_output A ref to a string, the parameters will be APPENDED to this string
                $prefix  Add a prefix to the key, unless already added (write to a namespace)
    Access    : private

=cut

sub __ToCGI
{
    my ($self, $r_output, $prefix) = @_;
    my $ns = $$self{'__name_space'};

    my @a_tmp=();
    foreach my $key (sort keys(%{$$self{'__h_opt'}}))
    {
        next if ($key !~ /^$ns/);
        if (defined($key) && defined($$self{'__h_opt'}{$key}))
        {
            if ($prefix ne "" && $key !~ /^$prefix/)
            {
                my $nkey = "$prefix$key";
                push (@a_tmp, "$nkey=".$$self{'__h_opt'}{$key});
            }
            else
            {
                push (@a_tmp, "$key=".$$self{'__h_opt'}{$key});
            }
        }
    }
    $$r_output = join('&amp;',@a_tmp);

    return;
}


=head2 procedure __ToGetOptLong

    Title     :	__ToGetOptLong
    Usage     :	$self->__ToGetOptLong($prefix)
    procedure : Dump the paramparser to @ARGV, using OptLong conventions
    Args      :	$prefix  Add a prefix to the key, unless already added (write to a namespace)
    Access    : private

=cut
sub __ToGetOptLong
{
    my ($self, $prefix) = @_;
    my $ns = $$self{'__name_space'};

    @ARGV = ();
    foreach my $key (sort keys(%{$$self{'__h_opt'}}))
    {
        next if ($key !~ /^$ns/);
        if (defined($key) && defined($$self{'__h_opt'}{$key}))
        {
            if ($prefix ne "" && $key !~ /^$prefix/)
            {
                my $nkey = "$prefix$key";
                push(@ARGV, '--' . $nkey, $$self{'__h_opt'}{$key});
            }
            else
            {
                push(@ARGV, '--' . $key, $$self{'__h_opt'}{$key});
            }
        }
    }
}

=head2 function __DefinedIfNot

    Title     :	__ToENV
    Usage     :	$self->__DefinedIfNot($r_var)
    function  : Init a variable if it is not defined (in order to avoid warnings)
    Access    : private

=cut
sub __DefinedIfNot
{
    my ($var) = @_;

    if (!defined($var) || $var eq "")
    {
        return "undef";
    }
    return $var;
}

=head2 Procedure __InitPossibleSources

    Title     :	__InitPossibleSources
    Usage     :	$self->__InitPossiblesources()
    Procedure : Build a list of possible sources depending on loaded modules
                Build also a list of possible destinations (for Dump)
    Access    : private

=cut
sub __InitPossibleSources
{
    my ($self) = @_;
    my (%h_src) = (
                   "CGIPM"       => defined($CGI::VERSION),
                   "GETOPTSTD"   => defined($Getopt::Std::VERSION),
                   "GETOPTLONG"  => defined($Getopt::Long::VERSION),
                   "CGILIB"      => defined($cgi_lib::version),
                   "ARGV"        => defined($ARGV[0]),
                   "INIT"        => 1,
                   "PARAMPARSER" => 1,
                   "HASH"        => 1,
                   "ENV"         =>1
                   );

    $$self{'__possible_sources'} = "";
    foreach my $key (keys(%h_src))
    {
        if ($h_src{$key})
        {
            $$self{'__possible_sources'} .= " $key ";
        }
    }

    my (%h_dst) = (
                   "CGIPM"       => defined($CGI::VERSION),
                   "GETOPTLONG"  => defined($Getopt::Long::VERSION),
                   "JSON"        => defined($JSON::VERSION),
                   "HASH"        => 1,
                   "STRING"      => 1,
                   "HTML"        => 1,
                   "ENV"         => 1
                   );

    $$self{'__possible_destinations'} = "";
    foreach my $key (keys(%h_dst))
    {
        if ($h_dst{$key})
        {
            $$self{'__possible_destinations'} .= " $key ";
        }
    }

}

=head2 function Verbose

 Title        : Verbose
 Usage        : $bool = $o_xx->Verbose( $msg );
 Prerequisite : none
 Function     : will print $msg onto STDERR if verbose param is set
 Returns      : $bool, a boolean, 1 if verbose is set.
                if $msg is undefined, it's equivalent to IsDefined( 'verbose' );
 Args         : $msg, a string, anything to write onto STDERR

 Todo         : make a specification relative to the format like usage ?

=cut

sub Verbose
{
	my $self = shift;
	my ( $msg ) = @_;

	if( $self->IsDefined( 'verbose' ) )
	{
		print STDERR $msg, "\n";
		return 1;
	}

	return 0;
}

=head2 procedure SetAppli

    Title     :	SetAppli
    Usage     :	ParamParser::SetAppli($o_appli);
    procedure : This is NOT a method, this is an ordinary function
                Set an Appli object that could be used to print Usage (see Appli.pm from PlayMOBY)
    Args      :	$o_appli  An Appli object instance


=cut
sub SetAppli
{
    my $self = shift;
    my $o_appli = shift;
    return unless (ref ($o_appli) =~ /appli/i);
    $$self{__o_appli} = $o_appli;
    $R_DEFUSAGE = sub {print $o_appli->GetUsage();};
    $self->SetUsage($R_DEFUSAGE);
    return;
}



=head1 COPYRIGHT NOTICE

This software is governed by the CeCILL license - www.cecill.info

=cut

1;

