#
# 	Sebastien.Letort@toulouse.inra.fr
#	Created: April 05, 2007
# 	$Id: General.pm 3531 2010-10-18 09:09:08Z manu $
#

# Copyright INRA/CNRS

# Emmanuel.Courcelle@toulouse.inra.fr
# Jerome.Gouzy@toulouse.inra.fr
# Thomas.Faraut@toulouse.inra.fr

# This software is a computer program whose purpose is to provide a
# web-based interface for analyzing the different levels of genome
# conservations.

# This software is governed by the CeCILL license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".

# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.

# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.

# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

package General;

BEGIN {
	use Exporter   ();
	use vars       qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

	$VERSION = do { my @r = (q$Rev: 3531 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

	@ISA         = qw(Exporter);
	@EXPORT      = qw( &TRUE &FALSE
						IsDefined SetUnlessDefined IsNotNull
						&SafeIsa );
	%EXPORT_TAGS = ( );     # ex. : TAG => [ qw!name1 name2! ],
	@EXPORT_OK   = qw( &ShowDebug );
}

use Data::Dumper;
use Scalar::Util;

# les constantes
use constant TRUE  => 1;
use constant FALSE => 0;

#les fonctions generales

=head2 function Capitalize

  Title        : Capitalize
  Usage        : $string = &Capitalize( $str );
  Prerequisite : none
  Function     : puts all first letters of a string to upper case
                 It works also if words are separated by '_'.
  Returns      : $string, $str with upper case on the first letters of every word
  Args         : $str, a string to capitalize.
  Globals      : none

=cut

sub Capitalize
{
	my ( $str ) = @_;

	my $string = $str;
# 	$string =~ s/([0-9a-zA-Z]+)/\u$1/g;
	$string =~ s/([[:alnum:]]+)/\u$1/g;

	return $string;
}


=head2 function Camelize

  Title        : Camelize
  Usage        : $string = &Camelize( $str );
  Prerequisite : none
  Function     : puts all first letters of a string to upper case,
                 	like Capitalize.
                 	But it also remove separator '_', ' ' or '''.
                 	For other separators, nothing is done.
  Returns      : $string, a one word string in camelcase style.
  Args         : $str, a string to camelize.
  Globals      : none

  TODO         : est-ce qu'on doit enlever tous les separateurs
                	ou bien seulement ceux entre 2 mots ?
                "salut les gars, ca va ?"
                 => 1) "SalutLesGars,CaVa?"
                 => 2) "SalutLesGars, CaVa ?"

                 Pour le moment c'est le 1)

=cut

sub Camelize
{
	my ( $str ) = @_;

	my $string = $str;
# 	$string =~ s/([0-9a-zA-Z]+)/\u$1/g;
	$string =~ s/([[:alnum:]]+)/\u$1/g;
	$string =~ s/[ _']//g;

	return $string;
}


=head2 Procedure Verbose

 Title        : Verbose
 Usage        : &General::Verbose( $self, $msg );
                or &General::Verbose( { verbose => &TRUE }, $msg );
 Prerequisite : none
 Procedure    : warn a message if verbose is true
               	or is $param->{ _verbose } is true.
 Args         : first arg, object with _verbose attribute
                 		or $hash ref with verbose key
                $msg, string, the message to warn

 Note         : deprecated, try Logger attribute in object context

=cut

sub Verbose
{
	my $param = shift;
	my ( $msg ) = @_;

	my $verbose = &FALSE;
	my $ref = ref $param;
	if( $ref eq 'HASH' )
	{	$verbose = $$param{ verbose };	}
	else
	{	$verbose = $param->{ _verbose };	}

	print STDERR "v-->$msg\n"	if( defined $verbose and &TRUE == $verbose );

	return;
}


=head2 Procedure ShowDebug

	Title      :	ShowDebug
	Usage      :	ShowDebug( deep=>$deep, arg => $ra_args );	<= a faire
	Prerequiste:	none
	Procedure  :	print onto STDERR the name of the function where the function was called
	        	depending on the deep parameter
	        	with the filename and the line number of the current file.
	        	Then it prints the list of arguments with Dumper.
	Args       :	$deep, an integer representing the deep of the call
	        		must be >=1
	        	@a_args, the list of arguments to print out.
	Error      :    none
	Globals    :    none

	Note       :	I used a hash ref to preserved compatibility with previous usage ShowDebug(@a_args)

=cut

sub ShowDebug
{
	my @args = @_;

	my $deep = 1;
# 	if( ref $args[0] eq 'HASH' )
# 	{
# 		$rh_hash = shift @args;
# 		$deep = $$rh_hash{ deep };
# 	}

	my ($package, $file, $line) = caller();

	my $subroutine = ( caller($deep) )[3] || $package;
	warn "In $subroutine ($file:$line) :\n", Data::Dumper->Dump([@args]);

	return;
}


=head2 Function IsDefined

 Usage      : print "\$var n'est pas vide\n"	if( IsDefined( $var ) );
 Prerequiste: only works with string, any ideas to enhaced with interger (!=0) is welcome
 Function   : shortcut to know if a variable is defined and non empty
 Returns    : 1 if var is defined and non empty, 0 otherwise
 Args       : $var, a variable that should contains a string.
 Error      : none
 Globals    : none

=cut

sub IsDefined
{
	my ($var) = @_;

	return( defined $var and $var ne '');
}

=head2 Function IsNotNull

        Title      :    IsNotNull
        Usage      :    print "\$var n'est pas vide\n"  if( IsNotNull( $var ) );
        Prerequiste:    only works with scalars (strings)
        Function   :    shortcut to know if a variable is defined and non empty
        Returns    :    &TRUE if var is defined and non empty, &FALSE otherwise
        Args       :    $var, a variable that should contains a string.
        Error      :    none
        Globals    :    none

=cut

sub IsNotNull
{
        my ($var) = @_;

        return &TRUE if ( defined $var and $var ne '');
        return &FALSE;
}


=head2 Function GetMonth

 Usage      : my $month = &General::GetMonth( $mon );
 Prerequiste: none
 Function   : return the english name of the month.
              January is month 0, and december, month 11
              like localtime->mon() return.
 Returns    : $month, string, the month corresponding to $mon
 Args       : $mon, integer, the number of the month
 
 Error      :    none # TODO ?
 Globals    :    none

=cut

sub GetMonth
{
	my( $mon ) = @_;

	my @a_months = qw( January February March April May June July
						August September October November December );

	return $a_months[ $mon ];
}


=head2 Function SetUnlessDefined

	Title      :	SetUnlessDefined
	Usage      :	SetUnlessDefined( \$var, $val );	# prefer this one
	            	$var = SetUnlessDefined( $var, $val );
	Prerequiste:	none
	Function   :	give a value to a variable if it hasn't got any.
	Returns    :	the value of the variable if a scalar is given as the first arg.
	            	nothing otherwise.
	Args       :	$var, a variable.
	Error      :    none
	Globals    :    none

=cut

sub SetUnlessDefined
{
	my ( $r_var, $val ) = @_;

	if( ref( $r_var ) eq 'SCALAR' )
	{
		$$r_var = $val	unless IsDefined( $$r_var );
		return;
	}
	elsif( ref( $r_var ) eq '' )
	{
		return  $val	unless IsDefined( $r_var );
		return $r_var;
	}

	return; # cosmetique
}


=head2 Procedure PurgeArray

 Title        : PurgeArray
 Note         :	Deprecated --> use Array::Purge

=cut

sub PurgeArray
{
	warn "obsolete use of General::PurgeArray, prefer Array::Purge\n" . caller();
	my ( $ra_array ) = @_;

	my @a_new = ();

	foreach my $value ( @$ra_array )
	{
		push( @a_new, $value )	if( defined( $value ) );
	}

	# destruction de l'ancien tableau et recuperation du nouveau
	@$ra_array = @a_new;

	return;
}



=head2 Procedure InitArray

 Title        : InitArray
 Usage        : &InitArray( $ra_array, val=>$val, nb_val => $nb );
 Prerequisite : none
 Procedure    : this function initializes an array
 Args         : $ra_array, a ref to the array to modify
               	$val, the value to affect to elements of the array, default is 0
               	$nb, the number of value to initialize _usefull if your array is still empty
               		default is the length of the array.

 Note         : will move to Array package.

=cut

sub InitArray
{
	use Carp;
	carp "obsolete use of General::InitArray, prefer Array::Init\n";

	my $ra_array = shift;
	my %h_args = ( val => 0,
				nb_val => scalar( @$ra_array ),
				@_ );

	my $key;
	for( my $i=0; $i<$h_args{ nb_val }; ++$i )
	{
		$$ra_array[ $i ] = $h_args{ val };
	}

	return;
}



=head2 Procedure HashWithoutValues

 Title        : HashWithoutValues
 Usage        : &HashWithoutValues( $rh_hash, @a_values );
 Prerequisite : do not use with string values.
                Works better when there are much more vlaues in the hash than in the list @a_values
 Procedure    : this function will delete keys where values belongs to @a_values
 Args         : $rh_hash, a ref to the hash to modify
                @a_values, a list of values to search

 Note : I suppose that there is another way to do it in one pass
      	like by using grep map or something like that.

=cut

sub HashWithoutValues
{
	my ( $rh_hash, @a_values ) = @_;

	foreach my $val ( @a_values )
	{
		foreach my $key ( keys %$rh_hash )
		{
			delete $$rh_hash{ $key }	if( $$rh_hash{ $key } == $val );
		}
	}

	return;
}



=head2 Procedure SafeIsa

 Usage        : $bool = SafeIsa( $var, $class )
 Function     : check that $var is defined,
                that $var is an object and that $var is a $class instance.
 Args         : $var, any scalar to test,
                $class, string, class name.
 Error        : none.

=cut

sub SafeIsa
{
	my( $var, $class ) = @_;

	if( defined $var &&  &Scalar::Util::blessed( $var ) && $var->isa( $class ) )
	{ return &TRUE;	}

	return &FALSE;
} #SafeIsa


1;	# package General

# -------------------------------------------------------------------
# ----------------------- General::Array ----------------------------
# -------------------------------------------------------------------

package Array;

use General;

=head2 function Array::Includes

  Title        : Includes
  Usage        : @a_indices = &Array::Includes( $ra_list, -values => $ra_values );
               	$indice = &Array::Includes( $ra_list, -val => $val );
  Prerequisite : it use eq to compare values even if it figures.
  Function     : This function checks if the values are into the $ra_list.
  Returns      : @a_indices = a list of indices,
               		the position where the element have been found
               	$indice = the position of $val in $ra_list
               	If one value is not found, its indice will be undef.
  Args         : $ra_list, a ref to the list of reference.
               	%hash, one key
               		-val to seek for one value.
               		-values to seek for many values

=cut

sub Includes
{
	my $ra_list = shift;
	my %h_args = (
			-val => undef,
			-values => [],
# 			-cmp => '==',	# no use since eq works for figures
			@_ );

	my $ra_values = $h_args{ -values };
	if( defined( $h_args{ -val } ) )
	{	@$ra_values = ( $h_args{ -val } );	}

	my @a_out = ();
	foreach my $val ( @$ra_values )
	{
		my $found = FALSE;

		for( my $i=0; $i<scalar( @$ra_list ); ++$i )
		{
			my ( $cmp, $b ) = ( $h_args{ -cmp }, $$ra_list[$i] );
# 			my $ok = eval "\$val $cmp \$b";

			# more simple, i use it until i found a problem
			# works for numbers and strings
			my $ok = ( $val eq $b );
			if( 1 == $ok )
			{
				push( @a_out, $i );
				$found = &TRUE;
				last;
			}
		}

		push( @a_out, undef )	unless( &TRUE == $found );
	}

	return wantarray ? @a_out : $a_out[0];
}


=head2 Procedure Array::Init

 Title        : Array::Init
 Usage        : &Array::Init( $ra_array, val=>$val, nb_val => $nb );
 Prerequisite : none
 Procedure    : this function initializes an array
 Args         : $ra_array, a ref to the array to modify
               	$val, the value to affect to elements of the array, default is 0
               	$nb, the number of value to initialize _usefull if your array as a size of 0 !
               		default is the length of the array.

 TODO         :	I will wait to need it to implement it.
                	I don't like it that much the way it is

=cut

sub Init
{
	my $ra_array = shift;
	my %h_args = ( val => 0,
				nb_val => scalar( @$ra_array ),
				@_ );

	my $key;
	for( my $i=0; $i<$h_args{ nb_val }; ++$i )
	{
		$$ra_array[ $i ] = $h_args{ val };
	}

	return;
}


=head2 Procedure Array::Purge

  Title        :	Array::Purge
  Usage        :	&Array::Purge( $ra_array );
  Prerequisite :	the values will be temporary duplicated
  Procedure    :	erase undef values of $ra_array.
                	will replace the array ref by a one without undef values
  Args         :	$ra_array, an array ref that might contains undef values

=cut

sub Purge
{
	my $ra_array = shift;
	my %h_args = (
			-val => undef,
			@_ );
	my @a_new = ();

	foreach my $value ( @$ra_array )
	{
		push( @a_new, $value )	unless( $h_args{ -val } == $value );
	}

	# destruction de l'ancien tableau et recuperation du nouveau
	@$ra_array = @a_new;

	return;
}

1;

__END__

=pod

=head1 COPYRIGHT NOTICE

This software is governed by the CeCILL license - www.cecill.info

=cut

